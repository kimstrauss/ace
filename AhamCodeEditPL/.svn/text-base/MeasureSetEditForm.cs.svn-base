﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class MeasureSetEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private MeasureSet _measureSetToEdit;

        public MeasureSetEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _measureSetToEdit = (MeasureSet)parameters.EditObject;

            this.Load += new System.EventHandler(SetEditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void SetEditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            measureSetBindingSource.DataSource = _measureSetToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (measureSetNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (measureSetAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_measureSetToEdit);
            _measureSetToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }

    }
}
