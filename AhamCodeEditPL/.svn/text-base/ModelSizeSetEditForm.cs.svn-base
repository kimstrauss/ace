﻿using System;
using System.Collections;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;
using System.Data.Linq;

using System.Configuration;
using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ModelSizeSetEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private ModelSizeSet _modelSizeSetToEdit;

        public ModelSizeSetEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _modelSizeSetToEdit = (ModelSizeSet)parameters.EditObject;

            this.Load += new EventHandler(ModelSizeSetEditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        void ModelSizeSetEditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);
            modelSizeSetBindingSource.DataSource = _modelSizeSetToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = ds.GetDataList(HaiBusinessObjectType.Market);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Market>(marketNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "MarketName", "MarketKey", marketKeyTextBox, _modelSizeSetToEdit.MarketName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit model size sets because market fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObject.HaiBusinessObjectType.ProductDimension);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<ProductDimension>(productDimensionNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ProductDimensionName", "ProductDimensionKey", productDimensionKeyTextBox, _modelSizeSetToEdit.ProductDimensionName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit model size sets because product dimension fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (modelSizeSetNameTextBox.Text.Trim() == string.Empty)
            {
                abort = true;
                MessageBox.Show("A model size set name must be entered.", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if (marketNameComboBox.Text.Trim() == string.Empty && marketNameComboBox.Items.Count>0)
            {
                abort = true;
                MessageBox.Show("A market must be selected.", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if (productDimensionNameComboBox.Text.Trim() == string.Empty && productDimensionNameComboBox.Items.Count>0)
            {
                abort = true;
                MessageBox.Show("A product dimension must be selected.", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if (abort)
                return;

            MarkChangedProperties(_modelSizeSetToEdit);
            _modelSizeSetToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
