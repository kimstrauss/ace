﻿namespace AhamMetaDataPL
{
    partial class ActivityEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label activityAbbreviationLabel;
            System.Windows.Forms.Label activityKeyLabel;
            System.Windows.Forms.Label activityNameLabel;
            System.Windows.Forms.Label activityStockFlowLabel;
            this.activityAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.activityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.activityKeyTextBox = new System.Windows.Forms.TextBox();
            this.activityNameTextBox = new System.Windows.Forms.TextBox();
            this.activityStockFlowTextBox = new System.Windows.Forms.TextBox();
            activityAbbreviationLabel = new System.Windows.Forms.Label();
            activityKeyLabel = new System.Windows.Forms.Label();
            activityNameLabel = new System.Windows.Forms.Label();
            activityStockFlowLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.activityBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            // 
            // activityAbbreviationLabel
            // 
            activityAbbreviationLabel.AutoSize = true;
            activityAbbreviationLabel.Location = new System.Drawing.Point(12, 22);
            activityAbbreviationLabel.Name = "activityAbbreviationLabel";
            activityAbbreviationLabel.Size = new System.Drawing.Size(106, 13);
            activityAbbreviationLabel.TabIndex = 30;
            activityAbbreviationLabel.Text = "Activity Abbreviation:";
            // 
            // activityKeyLabel
            // 
            activityKeyLabel.AutoSize = true;
            activityKeyLabel.Location = new System.Drawing.Point(12, 100);
            activityKeyLabel.Name = "activityKeyLabel";
            activityKeyLabel.Size = new System.Drawing.Size(65, 13);
            activityKeyLabel.TabIndex = 32;
            activityKeyLabel.Text = "Activity Key:";
            // 
            // activityNameLabel
            // 
            activityNameLabel.AutoSize = true;
            activityNameLabel.Location = new System.Drawing.Point(12, 48);
            activityNameLabel.Name = "activityNameLabel";
            activityNameLabel.Size = new System.Drawing.Size(75, 13);
            activityNameLabel.TabIndex = 34;
            activityNameLabel.Text = "Activity Name:";
            // 
            // activityStockFlowLabel
            // 
            activityStockFlowLabel.AutoSize = true;
            activityStockFlowLabel.Location = new System.Drawing.Point(12, 74);
            activityStockFlowLabel.Name = "activityStockFlowLabel";
            activityStockFlowLabel.Size = new System.Drawing.Size(100, 13);
            activityStockFlowLabel.TabIndex = 36;
            activityStockFlowLabel.Text = "Activity Stock Flow:";
            // 
            // activityAbbreviationTextBox
            // 
            this.activityAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "ActivityAbbreviation", true));
            this.activityAbbreviationTextBox.Location = new System.Drawing.Point(131, 19);
            this.activityAbbreviationTextBox.Name = "activityAbbreviationTextBox";
            this.activityAbbreviationTextBox.Size = new System.Drawing.Size(80, 20);
            this.activityAbbreviationTextBox.TabIndex = 0;
            // 
            // activityBindingSource
            // 
            this.activityBindingSource.DataSource = typeof(AhamMetaDataDAL.Activity);
            // 
            // activityKeyTextBox
            // 
            this.activityKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "ActivityKey", true));
            this.activityKeyTextBox.Location = new System.Drawing.Point(131, 97);
            this.activityKeyTextBox.Name = "activityKeyTextBox";
            this.activityKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.activityKeyTextBox.TabIndex = 3;
            this.activityKeyTextBox.TabStop = false;
            // 
            // activityNameTextBox
            // 
            this.activityNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "ActivityName", true));
            this.activityNameTextBox.Location = new System.Drawing.Point(131, 45);
            this.activityNameTextBox.Name = "activityNameTextBox";
            this.activityNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.activityNameTextBox.TabIndex = 1;
            // 
            // activityStockFlowTextBox
            // 
            this.activityStockFlowTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.activityStockFlowTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "ActivityStockFlow", true));
            this.activityStockFlowTextBox.Location = new System.Drawing.Point(131, 71);
            this.activityStockFlowTextBox.Name = "activityStockFlowTextBox";
            this.activityStockFlowTextBox.Size = new System.Drawing.Size(50, 20);
            this.activityStockFlowTextBox.TabIndex = 2;
            // 
            // ActivityEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(activityAbbreviationLabel);
            this.Controls.Add(this.activityAbbreviationTextBox);
            this.Controls.Add(activityKeyLabel);
            this.Controls.Add(this.activityKeyTextBox);
            this.Controls.Add(activityNameLabel);
            this.Controls.Add(this.activityNameTextBox);
            this.Controls.Add(activityStockFlowLabel);
            this.Controls.Add(this.activityStockFlowTextBox);
            this.Name = "ActivityEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Activity";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.activityStockFlowTextBox, 0);
            this.Controls.SetChildIndex(activityStockFlowLabel, 0);
            this.Controls.SetChildIndex(this.activityNameTextBox, 0);
            this.Controls.SetChildIndex(activityNameLabel, 0);
            this.Controls.SetChildIndex(this.activityKeyTextBox, 0);
            this.Controls.SetChildIndex(activityKeyLabel, 0);
            this.Controls.SetChildIndex(this.activityAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(activityAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.activityBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource activityBindingSource;
        private System.Windows.Forms.TextBox activityAbbreviationTextBox;
        private System.Windows.Forms.TextBox activityKeyTextBox;
        private System.Windows.Forms.TextBox activityNameTextBox;
        private System.Windows.Forms.TextBox activityStockFlowTextBox;
    }
}