﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessObject;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class ActivityEditForm : HaiObjectEditForm
    {
        private Activity _activityToEdit;

        public ActivityEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _activityToEdit = (Activity)parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            activityBindingSource.DataSource = _activityToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (activityNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (activityAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (activityStockFlowTextBox.Text.Trim().ToUpper() != "S" && activityStockFlowTextBox.Text.Trim().ToUpper() != "F")
            {
                MessageBox.Show("Stock/Flow must be \"S or \"F.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_activityToEdit);
            _activityToEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
