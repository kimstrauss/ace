﻿namespace AhamMetaDataPL
{
    partial class ActivitySetEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label activitySetAbbreviationLabel;
            System.Windows.Forms.Label activitySetKeyLabel;
            System.Windows.Forms.Label activitySetNameLabel;
            this.activitySetAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.activitySetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.activitySetKeyTextBox = new System.Windows.Forms.TextBox();
            this.activitySetNameTextBox = new System.Windows.Forms.TextBox();
            activitySetAbbreviationLabel = new System.Windows.Forms.Label();
            activitySetKeyLabel = new System.Windows.Forms.Label();
            activitySetNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.activitySetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(385, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(466, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // activitySetAbbreviationLabel
            // 
            activitySetAbbreviationLabel.AutoSize = true;
            activitySetAbbreviationLabel.Location = new System.Drawing.Point(12, 12);
            activitySetAbbreviationLabel.Name = "activitySetAbbreviationLabel";
            activitySetAbbreviationLabel.Size = new System.Drawing.Size(125, 13);
            activitySetAbbreviationLabel.TabIndex = 30;
            activitySetAbbreviationLabel.Text = "Activity Set Abbreviation:";
            // 
            // activitySetKeyLabel
            // 
            activitySetKeyLabel.AutoSize = true;
            activitySetKeyLabel.Location = new System.Drawing.Point(12, 64);
            activitySetKeyLabel.Name = "activitySetKeyLabel";
            activitySetKeyLabel.Size = new System.Drawing.Size(84, 13);
            activitySetKeyLabel.TabIndex = 32;
            activitySetKeyLabel.Text = "Activity Set Key:";
            // 
            // activitySetNameLabel
            // 
            activitySetNameLabel.AutoSize = true;
            activitySetNameLabel.Location = new System.Drawing.Point(12, 38);
            activitySetNameLabel.Name = "activitySetNameLabel";
            activitySetNameLabel.Size = new System.Drawing.Size(94, 13);
            activitySetNameLabel.TabIndex = 34;
            activitySetNameLabel.Text = "Activity Set Name:";
            // 
            // activitySetAbbreviationTextBox
            // 
            this.activitySetAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activitySetBindingSource, "ActivitySetAbbreviation", true));
            this.activitySetAbbreviationTextBox.Location = new System.Drawing.Point(143, 9);
            this.activitySetAbbreviationTextBox.Name = "activitySetAbbreviationTextBox";
            this.activitySetAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.activitySetAbbreviationTextBox.TabIndex = 0;
            // 
            // activitySetBindingSource
            // 
            this.activitySetBindingSource.DataSource = typeof(AhamMetaDataDAL.ActivitySet);
            // 
            // activitySetKeyTextBox
            // 
            this.activitySetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activitySetBindingSource, "ActivitySetKey", true));
            this.activitySetKeyTextBox.Location = new System.Drawing.Point(143, 61);
            this.activitySetKeyTextBox.Name = "activitySetKeyTextBox";
            this.activitySetKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.activitySetKeyTextBox.TabIndex = 2;
            this.activitySetKeyTextBox.TabStop = false;
            // 
            // activitySetNameTextBox
            // 
            this.activitySetNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activitySetBindingSource, "ActivitySetName", true));
            this.activitySetNameTextBox.Location = new System.Drawing.Point(143, 35);
            this.activitySetNameTextBox.Name = "activitySetNameTextBox";
            this.activitySetNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.activitySetNameTextBox.TabIndex = 1;
            // 
            // ActivitySetEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 216);
            this.Controls.Add(activitySetAbbreviationLabel);
            this.Controls.Add(this.activitySetAbbreviationTextBox);
            this.Controls.Add(activitySetKeyLabel);
            this.Controls.Add(this.activitySetKeyTextBox);
            this.Controls.Add(activitySetNameLabel);
            this.Controls.Add(this.activitySetNameTextBox);
            this.Name = "ActivitySetEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ActivitySetEditForm";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.activitySetNameTextBox, 0);
            this.Controls.SetChildIndex(activitySetNameLabel, 0);
            this.Controls.SetChildIndex(this.activitySetKeyTextBox, 0);
            this.Controls.SetChildIndex(activitySetKeyLabel, 0);
            this.Controls.SetChildIndex(this.activitySetAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(activitySetAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.activitySetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource activitySetBindingSource;
        private System.Windows.Forms.TextBox activitySetAbbreviationTextBox;
        private System.Windows.Forms.TextBox activitySetKeyTextBox;
        private System.Windows.Forms.TextBox activitySetNameTextBox;
    }
}