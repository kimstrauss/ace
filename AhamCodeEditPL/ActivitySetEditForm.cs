﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class ActivitySetEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private ActivitySet _activitySetToEdit;

        public ActivitySetEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _activitySetToEdit = (ActivitySet)parameters.EditObject;

            this.Load += new EventHandler(SetEditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        void SetEditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            activitySetBindingSource.DataSource = _activitySetToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (activitySetNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (activitySetAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_activitySetToEdit);
            _activitySetToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
