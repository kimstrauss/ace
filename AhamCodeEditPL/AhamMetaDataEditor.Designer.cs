﻿namespace AhamMetaDataPL
{
    partial class AhamMetaDataEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.databaseDescription = new System.Windows.Forms.Label();
            this.serverDescription = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(689, 560);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Don\'s test button";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // databaseDescription
            // 
            this.databaseDescription.AutoSize = true;
            this.databaseDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseDescription.Location = new System.Drawing.Point(106, 46);
            this.databaseDescription.Name = "databaseDescription";
            this.databaseDescription.Size = new System.Drawing.Size(57, 20);
            this.databaseDescription.TabIndex = 12;
            this.databaseDescription.Text = "label1";
            // 
            // serverDescription
            // 
            this.serverDescription.AutoSize = true;
            this.serverDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverDescription.Location = new System.Drawing.Point(106, 75);
            this.serverDescription.Name = "serverDescription";
            this.serverDescription.Size = new System.Drawing.Size(58, 20);
            this.serverDescription.TabIndex = 13;
            this.serverDescription.Text = "server";
            // 
            // AhamMetaDataEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 596);
            this.Controls.Add(this.serverDescription);
            this.Controls.Add(this.databaseDescription);
            this.Controls.Add(this.button1);
            this.Name = "AhamMetaDataEditor";
            this.Text = "Meta Data Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AhamMetaDataEditor_FormClosing);
            this.Load += new System.EventHandler(this.AhamMetaDataEditor_Load);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.databaseDescription, 0);
            this.Controls.SetChildIndex(this.serverDescription, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

         }


        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label databaseDescription;
        private System.Windows.Forms.Label serverDescription;





    }
}

