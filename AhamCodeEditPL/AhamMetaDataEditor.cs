﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using HaiBusinessObject;
using AhamMetaDataDAL;
using HaiBusinessUI;
using HaiMetaDataDAL;
using HaiInterfaces;


namespace AhamMetaDataPL
{
    public partial class AhamMetaDataEditor : HaiMetaDataEditor
    {
        // constructor
        public AhamMetaDataEditor()
        {
            InitializeComponent();

            _gus.DatabaseContext = Utilities.GetDatabaseContext();

            InitializeComponentSpecial();

            // set the form and the dictionaries of delegates for procedure calls based on business object type
            SetupForm();
            SetupAddProcedures();
            SetupEditProcedures();
            SetupDuplicateProcedures();

            // set parameters needed for instantiating the data service
            DataServiceParameters parameters = new DataServiceParameters();

            // create the data service
            _ds = new AhamDataService(parameters);
        }

        // STEP 6
        #region Setup Procedures

        protected override void SetupForm()
        {
            if (_gus.DatabaseContext == 2)  // OPEI
            {
                cellDisclosureToolStripMenuItem.Enabled = false;
                footnoteToolStripMenuItem.Enabled = false;
                expansionFactorToolStripMenuItem.Enabled = false;
                fsListToolStripMenuItem.Enabled = false;
                manufacturerBrandToolStripMenuItem.Enabled = false;
                inputFormToolStripMenuItem.Enabled = false;
                releaseExceptionToolStripMenuItem.Enabled = false;
                councilToolStripMenuItem.Enabled = false;
                productSetToolStripMenuItem.Enabled = true;
                productMembersToolStripMenuItem.Enabled = true;
                inputColumnToolStripMenuItem.Enabled = false;
                inputRowToolStripMenuItem.Enabled = false;
                outputReportPublicToolStripMenuItem.Enabled = false;
                manufacturerMissingBrandToolStripMenuItem.Enabled = false;
                reportCompareDetailToolStripMenuItem.Enabled = false;
                reportCompareMasterToolStripMenuItem.Enabled = false;
                channelHeirarchySetToolStripMenuItem.Enabled = true;
                channelHeirarchySetMemberToolStripMenuItem.Enabled = true;
            }

            if (_gus.DatabaseContext == 1)  // AHAM
            {
                cellDisclosureToolStripMenuItem.Enabled = true;
                footnoteToolStripMenuItem.Enabled = true;
                expansionFactorToolStripMenuItem.Enabled = true;
                fsListToolStripMenuItem.Enabled = true;
                manufacturerBrandToolStripMenuItem.Enabled = true;
                inputFormToolStripMenuItem.Enabled = true;
                releaseExceptionToolStripMenuItem.Enabled = true;
                councilToolStripMenuItem.Enabled = true;
                productSetToolStripMenuItem.Enabled = false;
                productMembersToolStripMenuItem.Enabled = false;
                inputColumnToolStripMenuItem.Enabled = true;
                inputRowToolStripMenuItem.Enabled = true;
                outputReportPublicToolStripMenuItem.Enabled = true;
                manufacturerMissingBrandToolStripMenuItem.Enabled = true;
                reportCompareDetailToolStripMenuItem.Enabled = true;
                reportCompareMasterToolStripMenuItem.Enabled = true;
                channelHeirarchySetToolStripMenuItem.Enabled = false;
                channelHeirarchySetMemberToolStripMenuItem.Enabled = false;
            }
            databaseDescription.Text =  "Current Association:  " + Utilities.GetDatabaseDisplayName();
            serverDescription.Text =    "Current Server:  " + Utilities.GetServerDisplayName();

        }

        private void SetupEditProcedures()
        {
            _callEditProcedure = new Dictionary<HaiBusinessObjectType, EditProcedure>();

            _callEditProcedure.Add(HaiBusinessObjectType.Catalog, new EditProcedure(CatalogEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.CatalogSet, new EditProcedure(CatalogSetEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.CatalogSetMember, new EditProcedure(CatalogSetMembershipEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Activity_Aham, new EditProcedure(ActivityEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.ActivitySet_Aham, new EditProcedure(ActivitySetEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.ActivitySetMember_Aham, new EditProcedure(ActivitySetMembershipEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Geography, new EditProcedure(GeographyEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.GeographySet, new EditProcedure(GeographySetEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.GeographySetMember, new EditProcedure(GeographySetMembershipEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Market, new EditProcedure(MarketEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.MarketSet, new EditProcedure(MarketSetEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.MarketSetMember, new EditProcedure(MarketSetMembershipEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Channel, new EditProcedure(ChannelEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.ChannelSet, new EditProcedure(ChannelSetEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.ChannelSetMember, new EditProcedure(ChannelSetMembershipEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.ChannelHeirarchySet, new EditProcedure(ChannelHeirarchySetEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.ChannelHeirarchySetMember, new EditProcedure(ChannelHeirarchySetMembersEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Use, new EditProcedure(UseEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.UseSet, new EditProcedure(UseSetEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.UseSetMember, new EditProcedure(UseSetMembershipEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Product_Aham, new EditProcedure(ProductEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.ProductSet_Aham, new EditProcedure(ProductSetEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.ProductSetMember_Aham, new EditProcedure(ProductSetMembershipEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Measure, new EditProcedure(MeasureEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.MeasureSet, new EditProcedure(MeasureSetEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.MeasureSetMember, new EditProcedure(MeasureSetMembershipEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Council, new EditProcedure(CouncilEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Group, new EditProcedure(GroupEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ExpansionFactor, new EditProcedure(ExpansionFactorEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.CellDisclosure, new EditProcedure(CellDisclosureEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Footnote, new EditProcedure(FootnoteEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.HolidayDate, new EditProcedure(HolidayDateEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.HolidayCADate, new EditProcedure(HolidayCADateEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ReleaseException, new EditProcedure(ReleaseExceptionEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ContactRoles, new EditProcedure(ContactRolesEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Manufacturer, new EditProcedure(ManufacturerEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Association, new EditProcedure(AssociationEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.FSList, new EditProcedure(FSListEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ManufacturerBrand, new EditProcedure(ManufacturerBrandEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.InputForm, new EditProcedure(InputFormEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ManufacturerReport, new EditProcedure(ManufacturerReportEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.Dimension, new EditProcedure(DimensionEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ProductDimension, new EditProcedure(ProductDimensionEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ModelSizeSet, new EditProcedure(ModelSizeSetEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ManufacturerMissingBrand, new EditProcedure(ManufacturerMissingBrandEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.SuppressReport, new EditProcedure(SuppressReportEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ManufacturerPLog, new EditProcedure(ManufacturerPLogListEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.OutputReportPublic, new EditProcedure(OutputReportPublicEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.OutputDistributionUser, new EditProcedure(OutputDistributionUserEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.OutputDistributionManufacturer, new EditProcedure(OutputDistributionManufacturerEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ProductMarket, new EditProcedure(ProductMarketEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ProductSum, new EditProcedure(ProductSumEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.InputColumn, new EditProcedure(InputColumnEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.InputRow, new EditProcedure(InputRowEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ModelSizeSetMember, new EditProcedure(ModelSizeSetMembershipEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ReportCompareMaster, new EditProcedure(ReportCompareMasterEdit));

            _callEditProcedure.Add(HaiBusinessObjectType.ReportCompareDetail, new EditProcedure(ReportCompareDetailEdit));
        }

        private void SetupAddProcedures()
        {
            _callAddProcedure = new Dictionary<HaiBusinessObjectType, AddProcedure>();

            _callAddProcedure.Add(HaiBusinessObjectType.Catalog, CatalogAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.CatalogSet, CatalogSetAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Activity_Aham, ActivityAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.ActivitySet_Aham, ActivitySetAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Geography, GeographyAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.GeographySet, GeographySetAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Market, MarketAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.MarketSet, MarketSetAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Channel, ChannelAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.ChannelSet, ChannelSetAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.ChannelHeirarchySet, ChannelHeirSetAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Use, UseAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.UseSet, UseSetAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Product_Aham, ProductAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.ProductSet_Aham, ProductSetAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Measure, MeasureAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.MeasureSet, MeasureSetAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Council, CouncilAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Group, GroupAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.ExpansionFactor, ExpansionFactorAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.CellDisclosure, CellDisclosureAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Footnote, FootnoteAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.HolidayDate, HolidayDateAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.HolidayCADate, HolidayCADateAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.ReleaseException, ReleaseExceptionAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.ContactRoles, ContactRolesAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Manufacturer, ManufacturerAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Association, AssociationAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.FSList, FSListAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.ManufacturerBrand, ManufacturerBrandAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.InputForm, InputFormAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.ManufacturerReport, ManufacturerReportAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.Dimension, DimensionAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.ProductDimension, ProductDimensionAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.ModelSizeSet, ModelSizeSetAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.ManufacturerMissingBrand, ManufacturerMissingBrandAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.SuppressReport, SuppressReportAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.OutputReportPublic, OutputReportPublicAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.OutputDistributionUser, OutputDistributionUserAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.OutputDistributionManufacturer, OutputDistributionManufacturerAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.ProductMarket, ProductMarketAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.InputColumn, InputColumnAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.InputRow, InputRowAdd);

            _callAddProcedure.Add(HaiBusinessObjectType.ReportCompareMaster, ReportCompareMasterAdd);
        }

        private void SetupDuplicateProcedures()
        {
            _callDuplicateProcedure = new Dictionary<HaiBusinessObjectType, DuplicateProcedure>();

            _callDuplicateProcedure.Add(HaiBusinessObjectType.Catalog, CatalogDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.CatalogSet, CatalogSetDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Activity_Aham, ActivityDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.ActivitySet_Aham, ActivitySetDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Geography, GeographyDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.GeographySet, GeographySetDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Market, MarketDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.MarketSet, MarketSetDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Channel, ChannelDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.ChannelSet, ChannelSetDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Use, UseDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.UseSet, UseSetDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Product_Aham, ProductDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.ProductSet_Aham, ProductSetDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Measure, MeasureDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.MeasureSet, MeasureSetDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Council, CouncilDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Group, GroupDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.ExpansionFactor, ExpansionFactorDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.CellDisclosure, CellDisclosureDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Footnote, FootnoteDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.HolidayDate, HolidayDateDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.HolidayCADate, HolidayCADateDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.ReleaseException, ReleaseExceptionDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.ContactRoles, ContactRolesDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Manufacturer, ManufacturerDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Association, AssociationDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.FSList, FSListDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.ManufacturerBrand, ManufacturerBrandDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.ManufacturerReport, ManufacturerReportDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.Dimension, DimensionDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.ProductDimension, ProductDimensionDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.ModelSizeSet, ModelSizeSetDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.SuppressReport, SuppressReportDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.OutputReportPublic, OutputReportPublicDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.OutputDistributionUser, OutputDistributionUserDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.OutputDistributionManufacturer, OutputDistributionManufacturerDuplicate);
            _callDuplicateProcedure.Add(HaiBusinessObjectType.ProductMarket, ProductMarketDuplicate);
        }
        #endregion

        // STEP 5
        #region Duplicate Object Procedures

        private void ActivityDuplicate()
        {
            DuplicateHaiBusinessObject<Activity, ActivityEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void ActivitySetDuplicate()
        {
            DuplicateSet<ActivitySet, ActivitySetEditForm>();
        }

        private void CatalogDuplicate()
        {
            DuplicateHaiBusinessObject<Catalog, CatalogEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void CatalogSetDuplicate()
        {
            DuplicateSet<CatalogSet, CatalogSetEditForm>();
        }

        private void GeographyDuplicate()
        {
            DuplicateHaiBusinessObject<Geography, GeographyEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void GeographySetDuplicate()
        {
            DuplicateSet<GeographySet, GeographySetEditForm>();
        }

        private void MarketDuplicate()
        {
            DuplicateHaiBusinessObject<Market, MarketEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void MarketSetDuplicate()
        {
            DuplicateSet<MarketSet, MarketSetEditForm>();
        }

        private void ChannelDuplicate()
        {
            DuplicateHaiBusinessObject<Channel, ChannelEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void ChannelSetDuplicate()
        {
            DuplicateSet<ChannelSet, ChannelSetEditForm>();
        }

        private void UseDuplicate()
        {
            DuplicateHaiBusinessObject<Use, UseEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void UseSetDuplicate()
        {
            DuplicateSet<UseSet, UseSetEditForm>();
        }

        private void ProductDuplicate()
        {
            DuplicateHaiBusinessObject<Product, ProductEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void ProductSetDuplicate()
        {
            DuplicateSet<ProductSet, ProductSetEditForm>();
        }

        private void MeasureDuplicate()
        {
            DuplicateHaiBusinessObject<Measure, MeasureEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void MeasureSetDuplicate()
        {
            DuplicateSet<MeasureSet, MeasureSetEditForm>();
        }

        private void CouncilDuplicate()
        {
            DuplicateHaiBusinessObject<Council, CouncilEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void GroupDuplicate()
        {
            DuplicateHaiBusinessObject<Group, GroupEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void ExpansionFactorDuplicate()
        {
            DuplicateHaiBusinessObject<ExpansionFactor, ExpansionFactorEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        private void CellDisclosureDuplicate()
        {
            DuplicateHaiBusinessObject<CellDisclosure, CellDisclosureMemberEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        private void FootnoteDuplicate()
        {
            DuplicateHaiBusinessObject<Footnote, FootnoteEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void HolidayDateDuplicate()
        {
            DuplicateHaiBusinessObject<HolidayDate, HolidayDateEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void HolidayCADateDuplicate()
        {
            DuplicateHaiBusinessObject<HolidayCADate, HolidayCADateEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void ReleaseExceptionDuplicate()
        {
            DuplicateHaiBusinessObject<ReleaseException, ReleaseExceptionEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void ContactRolesDuplicate()
        {
            DuplicateHaiBusinessObject<ContactRoles, ContactRolesEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void ManufacturerDuplicate()
        {
            DuplicateHaiBusinessObject<Manufacturer, ManufacturerEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void AssociationDuplicate()
        {
            DuplicateHaiBusinessObject<Association, AssociationEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void FSListDuplicate()
        {
            DuplicateHaiBusinessObject<FSList, FSListEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        private void ManufacturerBrandDuplicate()
        {
            DuplicateHaiBusinessObject<ManufacturerBrand, ManufacturerBrandEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        private void ManufacturerReportDuplicate()
        {
            DateRangeValidationType validationType;
            switch (_gus.DatabaseContext)
            {
                case 1: //  AHAM
                    validationType = DateRangeValidationType.Daily;
                    break;

                case 2: // OPEI
                    validationType = DateRangeValidationType.Monthly;
                    break;

                default:
                    throw new Exception("Unknown database context in AhamMetaDataEditor.ManufacturerReportDuplicate()");
            }

            DuplicateHaiBusinessObject<ManufacturerReport, ManufacturerReportEditForm>(_datagrid, validationType);
        }

        private void DimensionDuplicate()
        {
            DuplicateHaiBusinessObject<Dimension, DimensionEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void ProductDimensionDuplicate()
        {
            DuplicateHaiBusinessObject<ProductDimension, ProductDimensionEditForm>(_datagrid, DateRangeValidationType.Monthly);
        }

        private void ModelSizeSetDuplicate()
        {
            DuplicateHaiBusinessObject<ModelSizeSet, ModelSizeSetEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void SuppressReportDuplicate()
        {
            DuplicateHaiBusinessObject<SuppressReport, SuppressReportEditForm>(_datagrid, DateRangeValidationType.Monthly);
        }

        private void OutputReportPublicDuplicate()
        {
            DuplicateHaiBusinessObject<OutputReportPublic, OutputReportPublicEditForm>(_datagrid, DateRangeValidationType.Monthly);
        }

        private void OutputDistributionUserDuplicate()
        {
            DuplicateHaiBusinessObject<OutputDistributionUser, OutputDistributionUserEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        private void OutputDistributionManufacturerDuplicate()
        {
            DuplicateHaiBusinessObject<OutputDistributionManufacturer, OutputDistributionManufacturerEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        private void ProductMarketDuplicate()
        {
            DuplicateHaiBusinessObject<ProductMarket, ProductMarketEditForm>(_datagrid, DateRangeValidationType.Monthly);
        }

        #endregion

        // STEP 4
        #region Edit Object Procedures

        /* Catalog */
        private void CatalogEdit()
        {
            EditSingleHaiBusinessObject<Catalog, CatalogEditForm>();
        }

        private void CatalogSetEdit()
        {
            EditSingleHaiBusinessObject<CatalogSet, CatalogSetEditForm>();
        }

        private void CatalogSetMembershipEdit()
        {
            List<HaiBusinessObjectBase> objectsToEdit = GetItemsSelectedForEdit<CatalogSet>(_datagrid);
            if (objectsToEdit.Count != 1)
            {
                MessageBox.Show("A single object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Dictionary<string, BrowsableProperty> bpList = CatalogSetMember.GetBrowsablePropertyList();
            if (ContinueAfterCheckForChangedItems(objectsToEdit, bpList))
            {
                string[] itemPropertiesToShow = { "CatalogName", "CatalogAbbreviation" };
                List<string> itemPropertyList = itemPropertiesToShow.ToList<string>();
                string[] memberPropertiesToShow = { "CatalogName", "CatalogAbbreviation", "BeginDate", "EndDate" };
                List<string> memberPropertyList = memberPropertiesToShow.ToList<string>();

                SetManagementForm<CatalogSet, Catalog, CatalogSetMember> theForm =
                    new SetManagementForm<CatalogSet, Catalog, CatalogSetMember>((ISet)objectsToEdit[0], itemPropertyList, memberPropertyList, _gus);
                theForm.ShowDialog();

                theForm.Dispose();
            }
        }

        /* Activity */
        private void ActivityEdit()
        {
            EditSingleHaiBusinessObject<Activity, ActivityEditForm>();
        }

        private void ActivitySetEdit()
        {
            EditSingleHaiBusinessObject<ActivitySet, ActivitySetEditForm>();
        }

        private void ActivitySetMembershipEdit()
        {
            List<HaiBusinessObjectBase> objectsToEdit = GetItemsSelectedForEdit<ActivitySet>(_datagrid);
            if (objectsToEdit.Count != 1)
            {
                MessageBox.Show("A single object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Dictionary<string, BrowsableProperty> bpList = ActivitySetMember.GetBrowsablePropertyList();
            if (ContinueAfterCheckForChangedItems(objectsToEdit, bpList))
            {
                string[] itemPropertiesToShow = { "ActivityName", "ActivityAbbreviation" };
                List<string> itemPropertyList = itemPropertiesToShow.ToList<string>();
                string[] memberPropertiesToShow = { "ActivityName", "ActivityAbbreviation", "BeginDate", "EndDate" };
                List<string> memberPropertyList = memberPropertiesToShow.ToList<string>();

                SetManagementForm<ActivitySet, Activity, ActivitySetMember> theForm =
                    new SetManagementForm<ActivitySet, Activity, ActivitySetMember>((ISet)objectsToEdit[0], itemPropertyList, memberPropertyList, _gus);
                theForm.ShowDialog();
                
                theForm.Dispose();
            }
        }

        /* Geography */

        private void GeographyEdit()
        {
            EditMultipleHaiBusinessObjects<Geography, GeographyEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void GeographySetEdit()
        {
            EditSingleHaiBusinessObject<GeographySet, GeographySetEditForm>();
        }

        private void GeographySetMembershipEdit()
        {
            List<HaiBusinessObjectBase> objectsToEdit = GetItemsSelectedForEdit<GeographySet>(_datagrid);
            if (objectsToEdit.Count != 1)
            {
                MessageBox.Show("A single object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Dictionary<string, BrowsableProperty> bpList = GeographySetMember.GetBrowsablePropertyList();
            if (ContinueAfterCheckForChangedItems(objectsToEdit, bpList))
            {
                string[] itemPropertiesToShow = 
                    {"GeographyNameSuffix", "RegionID", "NationISO", "StateFIPS", "CountyFIPS", "StateVertex", "CountyVertex", 
                    "CensusRegionNumber", "GeographyName", "GeographySuffix", "GeographyID", "GeographyAbbreviation"};
                List<string> itemPropertyList = itemPropertiesToShow.ToList<string>();

                string[] memberPropertiesToShow = 
                    {"GeographyName", "GeographyAbbreviation", "BeginDate", "EndDate", "GeographyNameSuffix", "RegionID", "NationISO", 
                    "StateFIPS", "CountyFIPS", "StateVertex", "CountyVertex", "CensusRegionNumber", "GeographyName", 
                    "GeographySuffix", "GeographyID", "GeographyAbbreviation"};
                List<string> memberPropertyList = memberPropertiesToShow.ToList<string>();

                SetManagementForm<GeographySet, Geography, GeographySetMember> theForm =
                    new SetManagementForm<GeographySet, Geography, GeographySetMember>((ISet)objectsToEdit[0], itemPropertyList, memberPropertyList, _gus);
                theForm.ShowDialog();

                theForm.Dispose();
            }
        }

        /* Market */
        private void MarketEdit()
        {
            EditSingleHaiBusinessObject<Market, MarketEditForm>();
        }

        private void MarketSetEdit()
        {
            EditSingleHaiBusinessObject<MarketSet, MarketSetEditForm>();
        }

        private void MarketSetMembershipEdit()
        {
            List<HaiBusinessObjectBase> objectsToEdit = GetItemsSelectedForEdit<MarketSet>(_datagrid);
            if (objectsToEdit.Count != 1)
            {
                MessageBox.Show("A single object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Dictionary<string, BrowsableProperty> bpList = MarketSetMember.GetBrowsablePropertyList();
            if (ContinueAfterCheckForChangedItems(objectsToEdit, bpList))
            {
                string[] itemPropertiesToShow = { "MarketName", "MarketAbbreviation" };
                List<string> itemPropertyList = itemPropertiesToShow.ToList<string>();
                string[] memberPropertiesToShow = { "MarketName", "MarketAbbreviation", "BeginDate", "EndDate" };
                List<string> memberPropertyList = memberPropertiesToShow.ToList<string>();

                SetManagementForm<MarketSet, Market, MarketSetMember> theForm =
                    new SetManagementForm<MarketSet, Market, MarketSetMember>((ISet)objectsToEdit[0], itemPropertyList, memberPropertyList, _gus);
                theForm.ShowDialog();

                theForm.Dispose();
            }
        }

        /* Channel */
        private void ChannelEdit()
        {
            EditSingleHaiBusinessObject<Channel, ChannelEditForm>();
        }

        private void ChannelSetEdit()
        {
            EditSingleHaiBusinessObject<ChannelSet, ChannelSetEditForm>();
        }

        private void ChannelSetMembershipEdit()
        {
            List<HaiBusinessObjectBase> objectsToEdit = GetItemsSelectedForEdit<ChannelSet>(_datagrid);
            if (objectsToEdit.Count != 1)
            {
                MessageBox.Show("A single object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Dictionary<string, BrowsableProperty> bpList = ChannelSetMember.GetBrowsablePropertyList();
            if (ContinueAfterCheckForChangedItems(objectsToEdit, bpList))
            {
                string[] itemPropertiesToShow = { "ChannelName", "ChannelAbbreviation" };
                List<string> itemPropertyList = itemPropertiesToShow.ToList<string>();
                string[] memberPropertiesToShow = { "ChannelName", "ChannelAbbreviation", "BeginDate", "EndDate" };
                List<string> memberPropertyList = memberPropertiesToShow.ToList<string>();

                SetManagementForm<ChannelSet, Channel, ChannelSetMember> theForm =
                    new SetManagementForm<ChannelSet, Channel, ChannelSetMember>((ISet)objectsToEdit[0], itemPropertyList, memberPropertyList, _gus);
                theForm.ShowDialog();

                theForm.Dispose();
            }
        }

        private void ChannelHeirarchySetEdit()
        {
            EditSingleHaiBusinessObject<ChannelHeirarchySet, ChannelHeirSetEditForm>();
        }
        private void ChannelHeirarchySetMembersEdit()
        {
            EditSingleHaiBusinessObject<ChannelHeirarchySetMember, ChannelHeirMemberEditForm>();
        }

        /* Use */
        private void UseEdit()
        {
            EditSingleHaiBusinessObject<Use, UseEditForm>();
        }

        private void UseSetEdit()
        {
            EditSingleHaiBusinessObject<UseSet, UseSetEditForm>();
        }

        private void UseSetMembershipEdit()
        {
            List<HaiBusinessObjectBase> objectsToEdit = GetItemsSelectedForEdit<UseSet>(_datagrid);
            if (objectsToEdit.Count != 1)
            {
                MessageBox.Show("A single object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Dictionary<string, BrowsableProperty> bpList = UseSetMember.GetBrowsablePropertyList();
            if (ContinueAfterCheckForChangedItems(objectsToEdit, bpList))
            {
                string[] itemPropertiesToShow = { "UseName", "UseAbbreviation" };
                List<string> itemPropertyList = itemPropertiesToShow.ToList<string>();
                string[] memberPropertiesToShow = { "UseName", "UseAbbreviation", "BeginDate", "EndDate" };
                List<string> memberPropertyList = memberPropertiesToShow.ToList<string>();

                SetManagementForm<UseSet, Use, UseSetMember> theForm =
                    new SetManagementForm<UseSet, Use, UseSetMember>((ISet)objectsToEdit[0], itemPropertyList, memberPropertyList, _gus);
                theForm.ShowDialog();

                theForm.Dispose();
            }
        }

        /* Product */
        private void ProductEdit()
        {
            // N.B.  See comment in ProductEditForm for description of special handling needs for Product
            // which requires that the edit form be launched with EditMultipleHaiBusinessObjects() rather than
            // EditSingleHaiBusinessObject() that would normally be used.

            List<HaiBusinessObjectBase> itemsToEdit = GetItemsSelectedForEdit<Product>(_datagrid);
            if (itemsToEdit.Count > 1)
            {
                MessageBox.Show("Only one item can be selected for editing.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            EditMultipleHaiBusinessObjects<Product, ProductEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        private void ProductSetEdit()
        {
            EditSingleHaiBusinessObject<ProductSet, ProductSetEditForm>();
        }

        private void ProductSetMembershipEdit()
        {
            List<HaiBusinessObjectBase> objectsToEdit = GetItemsSelectedForEdit<ProductSet>(_datagrid);
            if (objectsToEdit.Count != 1)
            {
                MessageBox.Show("A single object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Dictionary<string, BrowsableProperty> bpList = ProductSetMember.GetBrowsablePropertyList();
            if (ContinueAfterCheckForChangedItems(objectsToEdit, bpList))
            {
                string[] itemPropertiesToShow = { "ProductName", "ProductAbbreviation" };
                List<string> itemPropertyList = itemPropertiesToShow.ToList<string>();
                string[] memberPropertiesToShow = { "ProductName", "ProductAbbreviation", "BeginDate", "EndDate" };
                List<string> memberPropertyList = memberPropertiesToShow.ToList<string>();

                SetManagementForm<ProductSet, Product, ProductSetMember> theForm =
                    new SetManagementForm<ProductSet, Product, ProductSetMember>((ISet)objectsToEdit[0], itemPropertyList, memberPropertyList, _gus);
                theForm.ShowDialog();

                theForm.Dispose();
            }
        }

        /* Measure */
        private void MeasureEdit()
        {
            EditSingleHaiBusinessObject<Measure, MeasureEditForm>();
        }

        private void MeasureSetEdit()
        {
            EditSingleHaiBusinessObject<MeasureSet, MeasureSetEditForm>();
        }

        private void MeasureSetMembershipEdit()
        {
            List<HaiBusinessObjectBase> objectsToEdit = GetItemsSelectedForEdit<MeasureSet>(_datagrid);
            if (objectsToEdit.Count != 1)
            {
                MessageBox.Show("A single object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Dictionary<string, BrowsableProperty> bpList = MeasureSetMember.GetBrowsablePropertyList();
            if (ContinueAfterCheckForChangedItems(objectsToEdit, bpList))
            {
                string[] itemPropertiesToShow = { "MeasureName", "MeasureAbbreviation" };
                List<string> itemPropertyList = itemPropertiesToShow.ToList<string>();
                string[] memberPropertiesToShow = { "MeasureName", "MeasureAbbreviation", "BeginDate", "EndDate", "GroupNumber" };
                List<string> memberPropertyList = memberPropertiesToShow.ToList<string>();

                // use the constructor form that passes the Type of the form that will be used to edit values in the Set Member
                SetManagementForm<MeasureSet, Measure, MeasureSetMember> theForm =
                    new SetManagementForm<MeasureSet, Measure, MeasureSetMember>((ISet)objectsToEdit[0], itemPropertyList, memberPropertyList, typeof(MeasureSetMemberEditForm), _gus);
                theForm.ShowDialog();

                theForm.Dispose();
            }
        }

        /* Council */
        private void CouncilEdit()
        {
            EditSingleHaiBusinessObject<Council, CouncilEditForm>();
        }

        /* Group */
        private void GroupEdit()
        {
            EditSingleHaiBusinessObject<Group, GroupEditForm>();
        }

        /* ExpansionFactor */
        private void ExpansionFactorEdit()
        {
            EditMultipleHaiBusinessObjects<ExpansionFactor, ExpansionFactorEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        /* CellDisclosure */
        private void CellDisclosureEdit()
        {
            EditSingleHaiBusinessObject<CellDisclosure, CellDisclosureMemberEditForm>();
        }

        /* Footnote */
        private void FootnoteEdit()
        {
            EditSingleHaiBusinessObject<Footnote, FootnoteEditForm>();
        }

        /* HolidayDate */
        private void HolidayDateEdit()
        {
            EditSingleHaiBusinessObject<HolidayDate, HolidayDateEditForm>();
        }

        /* HolidayCADate */
        private void HolidayCADateEdit()
        {
            EditSingleHaiBusinessObject<HolidayCADate, HolidayCADateEditForm>();
        }

        /* ReleaseException */
        private void ReleaseExceptionEdit()
        {
            EditSingleHaiBusinessObject<ReleaseException, ReleaseExceptionEditForm>();
        }

        /* ContactRoles */
        private void ContactRolesEdit()
        {
            EditMultipleHaiBusinessObjects<ContactRoles, ContactRolesEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);
        }

        /* Manufacturer */
        private void ManufacturerEdit()
        {
            EditSingleHaiBusinessObject<Manufacturer, ManufacturerEditForm>();
        }

        /* Association */
        private void AssociationEdit()
        {
            EditSingleHaiBusinessObject<Association, AssociationEditForm>();
        }

        /* FSList */
        private void FSListEdit()
        {
            EditMultipleHaiBusinessObjects<FSList, FSListEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        /* ManufacturerBrand */
        private void ManufacturerBrandEdit()
        {
            EditMultipleHaiBusinessObjects<ManufacturerBrand, ManufacturerBrandEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        /* Input Form */
        private void InputFormEdit()
        {
            EditSingleHaiBusinessObject<InputForm, InputFormEditForm>();
        }

        /* Manufacturer Report */
        private void ManufacturerReportEdit()
        {
            DateRangeValidationType validationType;
            switch (_gus.DatabaseContext)
            {
                case 1: //  AHAM
                    validationType = DateRangeValidationType.Daily;
                    break;

                case 2: // OPEI
                    validationType = DateRangeValidationType.Monthly;
                    break;

                default:
                    throw new Exception("Unknown database context in AhamMetaDataEditor.ManufacturerReportEdit()");
            }

            EditMultipleHaiBusinessObjects<ManufacturerReport, ManufacturerReportEditForm>(_datagrid, validationType);
        }

        /* Dimension */
        private void DimensionEdit()
        {
            EditSingleHaiBusinessObject<Dimension, DimensionEditForm>();
        }

        /* ProductDimension */
        private void ProductDimensionEdit()
        {
            EditMultipleHaiBusinessObjects<ProductDimension, ProductDimensionEditForm>(_datagrid, DateRangeValidationType.Monthly);
        }

        /* Model Size Set */
        private void ModelSizeSetEdit()
        {
            EditSingleHaiBusinessObject<ModelSizeSet, ModelSizeSetEditForm>();
        }

        private void ModelSizeSetMembershipEdit()
        {
            List<HaiBusinessObjectBase> objectsToEdit = GetItemsSelectedForEdit<ModelSizeSet>(_datagrid);
            if (objectsToEdit.Count != 1)
            {
                MessageBox.Show("A single object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Dictionary<string, BrowsableProperty> bpList = ModelSizeSetMember.GetBrowsablePropertyList();
            if (ContinueAfterCheckForChangedItems(objectsToEdit, bpList))
            {
                string[] itemPropertiesToShow = { "SizeName" };
                List<string> itemPropertyList = itemPropertiesToShow.ToList<string>();
                string[] memberPropertiesToShow = { "ModelSizeSetMemberName", "ModelSizeSetName", "SizeName", "BeginDate", "EndDate", "ParentSizeName", "SizeLevel" };
                List<string> memberPropertyList = memberPropertiesToShow.ToList<string>();

                // use the constructor form that passes the Type of the form that will be used to edit values in the Set Member
                // N.B. there is special case code in SetManagementForm to handle details for ModelSizeSet type
                SetManagementForm<ModelSizeSet, AhamMetaDataDAL.Size, ModelSizeSetMember> theForm =
                    new SetManagementForm<ModelSizeSet, AhamMetaDataDAL.Size, ModelSizeSetMember>((ISet)objectsToEdit[0], itemPropertyList, memberPropertyList, typeof(ModelSizeSetMemberEditForm), _gus);
                theForm.ShowDialog();

                theForm.Dispose();
            }
        }

        /* Manufacturer Missing Brand */
        private void ManufacturerMissingBrandEdit()
        {
            EditMultipleHaiBusinessObjects<ManufacturerMissingBrand, ManufacturerMissingBrandEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        /* Suppress Report */
        private void SuppressReportEdit()
        {
            EditSingleHaiBusinessObject<SuppressReport, SuppressReportEditForm>();
        }

        /* Edit PLog List */
        private void ManufacturerPLogListEdit()
        {
            // ensure that a single ManufacturerReport is selected
            List<HaiBusinessObjectBase> objectsToEdit = GetItemsSelectedForEdit<ManufacturerReport>(_datagrid);
            if (objectsToEdit.Count != 1)
            {
                MessageBox.Show("A single object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            ManufacturerReport theManufacturerReport = (ManufacturerReport)objectsToEdit[0];
            int manufacturerReportKey = theManufacturerReport.ManufacturerReportKey;        // get key used to retrieve related PLogs

            // ... get the list of related PLogs
            DataServiceParameters dataServiceParameters = new DataServiceParameters();
            DataAccessResult resultForPLogList = AhamDataServiceReportManagement.ManufacturerPLogListGet(manufacturerReportKey, 0, dataServiceParameters);
            if (!resultForPLogList.Success)
            {
                MessageBox.Show("Unable to load data: " + resultForPLogList.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // N.B. this form is a "one off" that does not follow the pattern for edit forms.
            EditFormParameters editParameters = new EditFormParameters(_gus);   // this object is used to pass GUS into the form to support "Print"
            PLogListEditForm theForm = new PLogListEditForm(theManufacturerReport.ManufacturerName, theManufacturerReport.ReportName, resultForPLogList, editParameters);
            theForm.ShowDialog(this);

            // get user confirmation before executing the delete operation.
            DialogResult yesNo;
            if (theForm.DialogResult == DialogResult.OK)
                if (resultForPLogList.PassBackDataList == null)
                {
                    MessageBox.Show("Nothing to delete!", "Delete notice", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    yesNo = DialogResult.No;
                }
                else
                    yesNo = MessageBox.Show(resultForPLogList.PassBackDataList.Count.ToString() + " items will be deleted.  Continue?", "Confirm action", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            else
                yesNo = DialogResult.No;

            if (yesNo == DialogResult.Yes)
            {
                DataAccessResult deleteResult = AhamDataServiceReportManagement.ManufacturerPLogListDelete((HaiBindingList<ManufacturerPLog>)resultForPLogList.PassBackDataList, dataServiceParameters);
                if (!deleteResult.Success)
                {
                    MessageBox.Show("Deletion(s) failed.\r\n" + deleteResult.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            theForm.Dispose();
        }

        /* Output Report Public */
        private void OutputReportPublicEdit()
        {
            EditSingleHaiBusinessObject<OutputReportPublic, OutputReportPublicEditForm>();
        }

        /* Output Distribution User */
        private void OutputDistributionUserEdit()
        {
            EditMultipleHaiBusinessObjects<OutputDistributionUser, OutputDistributionUserEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        /* Output Distribution Manufacturer */
        private void OutputDistributionManufacturerEdit()
        {
            EditMultipleHaiBusinessObjects<OutputDistributionManufacturer, OutputDistributionManufacturerEditForm>(_datagrid, DateRangeValidationType.Daily);
        }

        /* Product/Market */
        private void ProductMarketEdit()
        {
            EditMultipleHaiBusinessObjects<ProductMarket, ProductMarketEditForm>(_datagrid, DateRangeValidationType.Monthly);
        }

        /* ProductSum */
        private void ProductSumEdit()
        {
            EditFormParameters parameters = new EditFormParameters(_gus);
            List<HaiBusinessObjectBase> itemsToEdit = GetItemsSelectedForEdit<Product>(_datagrid);

            if (itemsToEdit.Count > 1)
            {
                MessageBox.Show("Only one item can be selected for editing.", 
                    "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            parameters.EditObject = itemsToEdit[0];

            HaiBusinessObjectBase anyHaiBusinessObject = System.Activator.CreateInstance<Product>() as HaiBusinessObjectBase;
            Dictionary<string, BrowsableProperty> bpList = anyHaiBusinessObject.GetClassBrowsablePropertyList();

            if (ContinueAfterCheckForChangedItems(itemsToEdit, bpList))
            {
                _datagrid.Refresh();

                ProductSumEditForm theForm = new ProductSumEditForm(parameters);
                theForm.ShowDialog(this);
                Application.DoEvents();

                if (theForm.DialogResult == DialogResult.OK)
                {
                    Cursor = Cursors.WaitCursor;

                    // ProductSum items to be added/deleted were place in ItemsSelectedForEdit
                    foreach (HaiBusinessObjectBase anyHBO in parameters.ItemsSelectedForEdit)       
                    {
                        DataAccessResult result = _ds.Save(anyHBO);
                        if (!result.Success)
                        {
                            MessageBox.Show("Save failed for " + anyHBO.UniqueIdentifier + "\r\n" + result.Message, 
                                "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }

                    Cursor = Cursors.Default;
                }

                theForm.Dispose();
            }
        }

        /* Input column */
        private void InputColumnEdit()
        {
            EditSingleHaiBusinessObject<InputColumn, InputColumnEditForm>();
        }

        /* Input row */
        private void InputRowEdit()
        {
            EditSingleHaiBusinessObject<InputRow, InputRowEditForm>();
        }

        /* Report Compare Master */
        private void ReportCompareMasterEdit()
        {
            EditSingleHaiBusinessObject<ReportCompareMaster, ReportCompareMasterEditForm>();
        }

        /* Report Compare Detail */
        private void ReportCompareDetailEdit()
        {
            EditFormParameters parameters = new EditFormParameters(_gus);
            List<HaiBusinessObjectBase> itemsToEdit = GetItemsSelectedForEdit<ReportCompareMaster>(_datagrid);

            if (itemsToEdit.Count > 1)
            {
                MessageBox.Show("Only one ReportCompareMaster can be selected for retrieving ReportCompareDetail records.",
                    "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            EditFormParameters parametersForSelector = new EditFormParameters(_gus);
            parametersForSelector.EditObject = itemsToEdit[0];
            parametersForSelector.GloballyUsefulStuff = _gus;
            ReportCompareDetailSelectorForm theForm = new ReportCompareDetailSelectorForm(parametersForSelector);

            theForm.ShowDialog(this);

            theForm.Dispose();
        }

        #endregion

        /* STEP 3.1 Use designer to create forms for xxxEditForm and xxxSetEditForm */
        /* STEP 3.2 Add the "New Object Add Procedures" */
        #region New Object Add Procedures

        /* Catalog */
        private void CatalogAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Catalog, CatalogEditForm>(bindingList);
        }

        private void CatalogSetAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<CatalogSet, CatalogSetEditForm>(bindingList);
        }

        /* Activity */
        private void ActivityAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Activity, ActivityEditForm>(bindingList);
        }

        private void ActivitySetAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ActivitySet, ActivitySetEditForm>(bindingList);
        }

        /* Geography */
        private void GeographyAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Geography, GeographyEditForm>(bindingList);
        }

        private void GeographySetAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<GeographySet, GeographySetEditForm>(bindingList);
        }

        /* Market */
        private void MarketAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Market, MarketEditForm>(bindingList);
        }

        private void MarketSetAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<MarketSet, MarketSetEditForm>(bindingList);
        }

        /* Channel */
        private void ChannelAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Channel, ChannelEditForm>(bindingList);
        }

        private void ChannelSetAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ChannelSet, ChannelSetEditForm>(bindingList);
        }

        private void ChannelHeirSetAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ChannelHeirarchySet, ChannelHeirSetEditForm>(bindingList);
        }


        /* Use */
        private void UseAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Use, UseEditForm>(bindingList);
        }

        private void UseSetAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<UseSet, UseSetEditForm>(bindingList);
        }

        /* Product */
        private void ProductAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Product, ProductEditForm>(bindingList);
        }

        private void ProductSetAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ProductSet, ProductSetEditForm>(bindingList);
        }

        /* Measure */
        private void MeasureAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Measure, MeasureEditForm>(bindingList);
        }

        private void MeasureSetAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<MeasureSet, MeasureSetEditForm>(bindingList);
        }

        /* Council */
        private void CouncilAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Council, CouncilEditForm>(bindingList);
        }

        /* Group */
        private void GroupAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Group, GroupEditForm>(bindingList);
        }

        /* ExpansionFactor */
        private void ExpansionFactorAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ExpansionFactor, ExpansionFactorEditForm>(bindingList);
        }

        /* CellDisclosure */
        private void CellDisclosureAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<CellDisclosure, CellDisclosureMemberEditForm>(bindingList);
        }

        /* Footnote */
        private void FootnoteAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Footnote, FootnoteEditForm>(bindingList);
        }

        /* HolidayDate */
        private void HolidayDateAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<HolidayDate, HolidayDateEditForm>(bindingList);
        }

        /* HolidayCADate */
        private void HolidayCADateAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<HolidayCADate, HolidayCADateEditForm>(bindingList);
        }

        /* ReleaseException */
        private void ReleaseExceptionAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ReleaseException, ReleaseExceptionEditForm>(bindingList);
        }

        /* ContactRoles */
        private void ContactRolesAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ContactRoles, ContactRolesEditForm>(bindingList);
        }

        /* Manufacturers */
        private void ManufacturerAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Manufacturer, ManufacturerEditForm>(bindingList);
        }

        /* Associations */
        private void AssociationAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Association, AssociationEditForm>(bindingList);
        }

        /* FSLists */
        private void FSListAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<FSList, FSListEditForm>(bindingList);
        }

        /* Manufacturer Brand */
        private void ManufacturerBrandAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ManufacturerBrand, ManufacturerBrandEditForm>(bindingList);
        }

        /* Input Form */
        private void InputFormAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<InputForm, InputFormEditForm>(bindingList);
        }

        /* Manufacturer Report */
        private void ManufacturerReportAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ManufacturerReport, ManufacturerReportEditForm>(bindingList);
        }

        /* Dimension */
        private void DimensionAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<Dimension, DimensionEditForm>(bindingList);
        }

        /* Product Dimension */
        private void ProductDimensionAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ProductDimension, ProductDimensionEditForm>(bindingList);
        }

        /* Model Size Set */
        private void ModelSizeSetAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ModelSizeSet, ModelSizeSetEditForm>(bindingList);
        }

        /* Manufacturer Missing Brand */
        private void ManufacturerMissingBrandAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ManufacturerMissingBrand, ManufacturerMissingBrandEditForm>(bindingList);
        }

        /* Suppress Report */
        private void SuppressReportAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<SuppressReport, SuppressReportEditForm>(bindingList);
        }

        /* Output Report Public */
        private void OutputReportPublicAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<OutputReportPublic, OutputReportPublicEditForm>(bindingList);
        }

        /* Output Distribution User */
        private void OutputDistributionUserAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<OutputDistributionUser, OutputDistributionUserEditForm>(bindingList);
        }

        /* OutputDistribution Manufacturer */
        private void OutputDistributionManufacturerAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<OutputDistributionManufacturer, OutputDistributionManufacturerEditForm>(bindingList);
        }

        /* Product/Market */
        private void ProductMarketAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ProductMarket, ProductMarketEditForm>(bindingList);
        }

        /* Input column */
        private void InputColumnAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<InputColumn, InputColumnEditForm>(bindingList);
        }

        /* Input row */
        private void InputRowAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<InputRow, InputRowEditForm>(bindingList);
        }

        /* Report Compare Master */
        private void ReportCompareMasterAdd(IUntypedBindingList bindingList)
        {
            AddHaiBusinessObject<ReportCompareMaster, ReportCompareMasterEditForm>(bindingList);
        }

        #endregion

        // STEP 2
        #region Menu Event Handlers

        /* CATALOG */
        // catalog
        void catalogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Catalog);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text ="";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a catalog", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // catalog set
        void catalogSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.CatalogSet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a catalog set", HaiReports.ListType.Set);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // catalog set members
        void catalogMembersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.CatalogSet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manage the members of a catalog set", HaiReports.ListType.Dictionary);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.CatalogSetMember;   // direct edit to set Members
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        /* ACTIVITY */
        // activity
        void activityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Activity_Aham);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define an activity", HaiReports.ListType.Dictionary);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // activity set
        void activitySetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ActivitySet_Aham);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define an activity set", HaiReports.ListType.Set);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // activity set members
        void activityMembersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ActivitySet_Aham);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manage the members of an activity set", HaiReports.ListType.Dictionary);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.ActivitySetMember_Aham;
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        /* GEOGRAPHY */
        // geography
        void geographyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Geography);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                string[] geographyOrder = 
                    {"GeographyNameSuffix", "RegionID", "NationISO", "StateFIPS", "CountyFIPS", "StateVertex", "CountyVertex", 
                        "CensusRegionNumber", "GeographyName", "GeographySuffix", "GeographyID", "GeographyAbbreviation"};
                
                CreateDataGridControl<HaiBusinessObjectBase>(result, new List<string>(geographyOrder), "Meta Data Editor: Define a geographic area", HaiReports.ListType.Dictionary);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // geography set
        void geographySetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.GeographySet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a geography set", HaiReports.ListType.Set);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // geography set members
        void geographyMembersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.GeographySet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manage the members of a geography set", HaiReports.ListType.Dictionary);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.GeographySetMember;
            }
                else
                    MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        /* Market */
        // market definintion
        void marketToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Market);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a market", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // market set definition
        void marketSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.MarketSet);
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a market set", HaiReports.ListType.Set);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // market set members management
        void marketMembersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.MarketSet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manage the members of a market set", HaiReports.ListType.Dictionary);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.MarketSetMember;
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        /* Channel */
        // channel
        void channelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Channel);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a channel", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // channel set
        void channelSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ChannelSet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a channel set", HaiReports.ListType.Set);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // channel heirarchy set
        void cchannelHeirarchySetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ChannelHeirarchySet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a channel heirarchy set", HaiReports.ListType.Set);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
 
        // channel set members
        void channelMembersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ChannelSet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manage the members of a channel set", HaiReports.ListType.Dictionary);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.ChannelSetMember;   // direct edit to set Members
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }
        // channel heirarchy set members
        void channelHeirarchyMembersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ChannelHeirarchySetMember);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manage the members of a channel heirarchy set", HaiReports.ListType.Dictionary);
                //_currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.ChannelHeirarchySetMember;   // direct edit to set Members
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        /* USE */
        // use
        void useToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Use);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a use", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // use set
        void useSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.UseSet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a use set", HaiReports.ListType.Set);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // use set members
        void useMembersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.UseSet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manage the members of a use set", HaiReports.ListType.Dictionary);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.UseSetMember;   // direct edit to set Members
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        /* PRODUCT */
        // product
        void productToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Product_Aham);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define an product", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // product set
        void productSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ProductSet_Aham);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define an product set", HaiReports.ListType.Set);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // product set members
        void productMembersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ProductSet_Aham);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manage the members of an product set", HaiReports.ListType.Dictionary);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.ProductSetMember_Aham;
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        /* MEASURE */
        // measure
        void measureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Measure);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a measure", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // measure set
        void measureSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.MeasureSet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a measure set", HaiReports.ListType.Set);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // measure set members
        void measureMembersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.MeasureSet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manage the members of a measure set", HaiReports.ListType.Dictionary);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.MeasureSetMember;   // direct edit to set Members
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        /* COUNCIL */
        // council
        void councilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Council);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a council", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /* GROUP */
        // group
        void groupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Group);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a group", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /* EXPANSION FACTOR */
        // expansionFactor
        void expansionFactorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ExpansionFactor);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define an expansion factor", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /* CELL DISCLOSURE   */
        void cellDisclosureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.CellDisclosure);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                string[] cols = { "OutputReportID", "IndustryMemberName", "SourceSizeSetMemberName", "DestinationSizeSetMemberName", "Footnote", 
                                    "BeginDate", "EndDate", "OutputReportKey", "IndustryMemberKey", "SourceSizeSetMemberKey", "DestinationSizeSetMemberKey", 
                                    "FootnoteKey", "CellDisclosureKey" };
                CreateDataGridControl<HaiBusinessObjectBase>(result, new List<string>(cols), "Meta Data Editor: Set cell disclosures", HaiReports.ListType.unknown);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /* FOOTNOTE */
        // footnote
        void footnoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Footnote);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Define a report footnote", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /* HOLIDAY */
        // HolidayDate
        void holidayDateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.HolidayDate);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                string[] cols = { "DateOfHoliday", "Name", "HolidayDateKey" };
                CreateDataGridControl<HaiBusinessObjectBase>(result, new List<string>(cols), "Meta Data Editor: Define a holiday date", HaiReports.ListType.Dictionary);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /* HOLIDAYCA */
        // HolidayCADate
        void holidayCADateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.HolidayCADate);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                string[] cols = { "DateOfHoliday", "Name", "HolidayDateKey" };
                CreateDataGridControl<HaiBusinessObjectBase>(result, new List<string>(cols), "Meta Data Editor: Define a holiday date", HaiReports.ListType.Dictionary);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }


        /* RELEASE EXCEPTIONS */
        // ReleaseException
        void releaseExceptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ReleaseException);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                string[] cols = { "OutputReportID", "ReportEndDate", "HideFinal", "HidePublic", "ReleaseExceptionKey", "OutputReportKey" };
                CreateDataGridControl<HaiBusinessObjectBase>(result, new List<string>(cols), "Meta Data Editor: Release exceptions", HaiReports.ListType.Dictionary);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /* CONTACT ROLES */
        // ContactRoles
        void contactRolesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ContactRoles);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Contact Roles", HaiReports.ListType.Dictionary);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /* MANUFACTURER */
        // manufacturer
        void manufacturerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Manufacturer);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                string[] cols = { "ManufacturerName", "ManufacturerAbbreviation", "Billable", "URL", "ManufacturerKey" };
                CreateDataGridControl<HaiBusinessObjectBase>(result, new List<string>(cols), "Meta Data Editor: Manufacturer", HaiReports.ListType.Dictionary);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /* ASSOCIATION */
        // association
        void associationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Association);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Association", HaiReports.ListType.Dictionary);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /* FSLIST */
        // FSList
        void fsListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.FSList);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: FSList", HaiReports.ListType.Dictionary);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /* MANUFACTURER BRAND */
        // ManufacturerBrand
        void manufacturerBrandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ManufacturerBrand);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manufacturer Brand", HaiReports.ListType.Dictionary);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Input Form
        void inputFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.InputForm);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Input form", HaiReports.ListType.unknown);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Manufacturer Report
        void manufacturerReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ManufacturerReport);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manufacturer report", HaiReports.ListType.unknown);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Dimension
        void dimensionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Dimension);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Dimension", HaiReports.ListType.unknown);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Product Dimension
        void productDimensionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ProductDimension);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Product Dimension", HaiReports.ListType.Dictionary);
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        // Model Size Set
        void modelSizeSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ModelSizeSet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Model Size Set", HaiReports.ListType.Set);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        // Model Size Set members
        void modelSizeSetMembersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ModelSizeSet);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manage the members of a model size set", HaiReports.ListType.Dictionary);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.ModelSizeSetMember;   // direct edit to set Members
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        // Manufacturer Missing Brand
        void manufacturerMissingBrandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ManufacturerMissingBrand);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Manufacturer Missing Brands", HaiReports.ListType.unknown);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Suppress Reports
        void suppressReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.SuppressReport);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Suppress Reports", HaiReports.ListType.unknown);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Edit PLog List
        void editPLogListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ManufacturerReport);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            string[] cols = { "ManufacturerName", "ReportName", "BeginDate", "EndDate" };
            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, cols.ToList<string>(), "Meta Data Editor: Edit PLog list for a manufacturer report", HaiReports.ListType.unknown);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.ManufacturerPLog;
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        // Output Report Manufacturer
        void outputReportManufacturerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.OutputDistributionManufacturer);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            string[] cols = { "ManufacturerName", "OutputReportID", "BeginDate", "EndDate", "Disabled", "ManufacturerKey", "OutputReportKey", "OutputDistributionManufacturerKey" };
            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, cols.ToList<string>(), "Meta Data Editor: Edit Output Distribution Manufacturer", HaiReports.ListType.unknown);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Output Report User
        void outputReportUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.OutputDistributionUser);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            string[] cols = { "UserCode", "OutputReportID", "BeginDate", "EndDate", "Disabled", "OutputReportKey", "OutputDistributionUserKey" };
            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, cols.ToList<string>(), "Meta Data Editor: Edit Output Distribution User", HaiReports.ListType.unknown);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Output Report Public
        void outputReportPublicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.OutputReportPublic);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            string[] cols = { "OutputReportID", "BeginDate", "EndDate", "OutputReportKey", "OutputReportPublicKey" };
            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, cols.ToList<string>(), "Meta Data Editor: Edit Output Report Public", HaiReports.ListType.unknown);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Product/Market
        void productMarketToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ProductMarket);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            string[] cols = { "ProductName", "MarketName", "ChannelName", "BeginDate", "EndDate", "ProductKey", "MarketKey", "ChannelKey", "ProductMarketKey" };
            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, cols.ToList<string>(), "Meta Data Editor: Edit Product/Market relationship", HaiReports.ListType.unknown);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        //ProductSum
        void productSumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Product_Aham);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            string[] cols = { "ProductName", "ProductAbbreviation", "ProductKey" };
            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, cols.ToList<string>(), "Meta Data Editor: Edit aggregates for a product", HaiReports.ListType.unknown);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.ProductSum;
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        // Input Column
        void inputColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.InputColumn);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Edit input column", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Input Row
        void inputRowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.InputRow);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Edit input row", HaiReports.ListType.Dictionary);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Report Compare Master
        void reportCompareMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ReportCompareMaster);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Edit report compare master", HaiReports.ListType.unknown);
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        // Report Comapre Detail
        void reportCompareDetailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ReportCompareMaster);
            Cursor = Cursors.Default;
            databaseDescription.Text = "";
            serverDescription.Text = "";

            if (result.Success)
            {
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Meta Data Editor: Select Report Compare Master for detail records", HaiReports.ListType.unknown);
                _currentGridControl.ActionRedirectionObjectType = HaiBusinessObjectType.ReportCompareDetail;    // direct edit to ReportCompareDetail
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            ButtonAddDeleteDuplicateDisable();
        }

        #endregion

        // STEP 1
        #region SPECIAL: Designer-like code and declarations for this form

        // top level menu items
        private ToolStripMenuItem codesToolStripMenuItem;
        private ToolStripMenuItem reportManagementToolStripMenuItem;
        private ToolStripMenuItem relationshipsToolStripMenuItem;
        private ToolStripMenuItem heirarchiesToolStripMenuItem;
        private ToolStripMenuItem contactManagementToolStripMenuItem;

        // code categories
        private ToolStripMenuItem dictionariesToolStripMenuItem;
        private ToolStripMenuItem setDefinitionToolStripMenuItem;
        private ToolStripMenuItem setManagementToolStripMenuItem;

        // dictionaries
        private ToolStripMenuItem catalogToolStripMenuItem;
        private ToolStripMenuItem activityToolStripMenuItem;
        private ToolStripMenuItem geographyToolStripMenuItem;
        private ToolStripMenuItem marketToolStripMenuItem;
        private ToolStripMenuItem channelToolStripMenuItem;
        private ToolStripMenuItem useToolStripMenuItem;
        private ToolStripMenuItem productToolStripMenuItem;
        private ToolStripMenuItem measureToolStripMenuItem;
        private ToolStripMenuItem councilToolStripMenuItem;
        private ToolStripMenuItem groupToolStripMenuItem;
        private ToolStripMenuItem expansionFactorToolStripMenuItem;
        private ToolStripMenuItem footnoteToolStripMenuItem;
        private ToolStripMenuItem holidayDateToolStripMenuItem;
        private ToolStripMenuItem holidayCADateToolStripMenuItem;
        private ToolStripMenuItem manufacturerToolStripMenuItem;
        private ToolStripMenuItem associationToolStripMenuItem;
        private ToolStripMenuItem dimensionToolStripMenuItem;
        private ToolStripMenuItem productDimensionToolStripMenuItem;

        // set definition
        private ToolStripMenuItem catalogSetToolStripMenuItem;
        private ToolStripMenuItem activitySetToolStripMenuItem;
        private ToolStripMenuItem geographySetToolStripMenuItem;
        private ToolStripMenuItem marketSetToolStripMenuItem;
        private ToolStripMenuItem channelSetToolStripMenuItem;
        private ToolStripMenuItem useSetToolStripMenuItem;
        private ToolStripMenuItem productSetToolStripMenuItem;
        private ToolStripMenuItem measureSetToolStripMenuItem;
        private ToolStripMenuItem modelSizeSetToolStripMenuItem;

        // set membership
        private ToolStripMenuItem catalogMembersToolStripMenuItem;
        private ToolStripMenuItem activityMembersToolStripMenuItem;
        private ToolStripMenuItem geographyMembersToolStripMenuItem;
        private ToolStripMenuItem marketMembersToolStripMenuItem;
        private ToolStripMenuItem channelMembersToolStripMenuItem;
        private ToolStripMenuItem useMembersToolStripMenuItem;
        private ToolStripMenuItem productMembersToolStripMenuItem;
        private ToolStripMenuItem measureMembersToolStripMenuItem;
        private ToolStripMenuItem modelSizeSetMembersToolStripMenuItem;

        // report management menu items
        private ToolStripMenuItem cellDisclosureToolStripMenuItem;
        private ToolStripMenuItem manufacturerBrandToolStripMenuItem;
        private ToolStripMenuItem releaseExceptionToolStripMenuItem;
        private ToolStripMenuItem inputFormToolStripMenuItem;
        private ToolStripMenuItem manufacturerReportToolStripMenuItem;
        private ToolStripMenuItem manufacturerMissingBrandToolStripMenuItem;
        private ToolStripMenuItem suppressReportToolStripMenuItem;
        private ToolStripMenuItem editPLogListToolStripMenuItem;
        private ToolStripMenuItem outputReportDistributionManufacturerToolStripMenuItem;
        private ToolStripMenuItem outputReportDistributionUserToolStripMenuItem;
        private ToolStripMenuItem outputReportPublicToolStripMenuItem;
        private ToolStripMenuItem inputColumnToolStripMenuItem;
        private ToolStripMenuItem inputRowToolStripMenuItem;
        private ToolStripMenuItem reportCompareMasterToolStripMenuItem;
        private ToolStripMenuItem reportCompareDetailToolStripMenuItem;

        // contact management menu items
        private ToolStripMenuItem contactRolesToolStripMenuItem;
        private ToolStripMenuItem fsListToolStripMenuItem;


        // relationships
        private ToolStripMenuItem productMarketToolStripMenuItem;
        private ToolStripMenuItem productSumToolStripMenuItem;

        // heirarchies
        private ToolStripMenuItem channelHeirarchySetToolStripMenuItem;
        private ToolStripMenuItem channelHeirarchySetMemberToolStripMenuItem;


        private void InitializeComponentSpecial()
        {

            this.codesToolStripMenuItem = new ToolStripMenuItem();
            this.reportManagementToolStripMenuItem = new ToolStripMenuItem();
            this.contactManagementToolStripMenuItem = new ToolStripMenuItem();
            this.relationshipsToolStripMenuItem = new ToolStripMenuItem();
            this.heirarchiesToolStripMenuItem = new ToolStripMenuItem();

            // dictionaries
            this.dictionariesToolStripMenuItem = new ToolStripMenuItem();
            this.catalogToolStripMenuItem = new ToolStripMenuItem();
            this.activityToolStripMenuItem = new ToolStripMenuItem();
            this.geographyToolStripMenuItem = new ToolStripMenuItem();
            this.marketToolStripMenuItem = new ToolStripMenuItem();
            this.channelToolStripMenuItem = new ToolStripMenuItem();
            this.useToolStripMenuItem = new ToolStripMenuItem();
            this.productToolStripMenuItem = new ToolStripMenuItem();
            this.measureToolStripMenuItem = new ToolStripMenuItem();
            this.councilToolStripMenuItem = new ToolStripMenuItem();
            this.groupToolStripMenuItem = new ToolStripMenuItem();
            this.footnoteToolStripMenuItem = new ToolStripMenuItem();
            this.holidayDateToolStripMenuItem = new ToolStripMenuItem();
            this.holidayCADateToolStripMenuItem = new ToolStripMenuItem();
            this.manufacturerToolStripMenuItem = new ToolStripMenuItem();
            this.associationToolStripMenuItem = new ToolStripMenuItem();
            this.dimensionToolStripMenuItem = new ToolStripMenuItem();
            this.productDimensionToolStripMenuItem = new ToolStripMenuItem();

            // set definition
            this.setDefinitionToolStripMenuItem = new ToolStripMenuItem();
            this.catalogSetToolStripMenuItem = new ToolStripMenuItem();
            this.activitySetToolStripMenuItem = new ToolStripMenuItem();
            this.geographySetToolStripMenuItem = new ToolStripMenuItem();
            this.marketSetToolStripMenuItem = new ToolStripMenuItem();
            this.channelSetToolStripMenuItem = new ToolStripMenuItem();
            this.channelHeirarchySetToolStripMenuItem = new ToolStripMenuItem();
            this.useSetToolStripMenuItem = new ToolStripMenuItem();
            this.productSetToolStripMenuItem = new ToolStripMenuItem();
            this.measureSetToolStripMenuItem = new ToolStripMenuItem();
            this.modelSizeSetToolStripMenuItem = new ToolStripMenuItem();
            
            // set membership
            this.setManagementToolStripMenuItem = new ToolStripMenuItem();
            this.catalogMembersToolStripMenuItem = new ToolStripMenuItem();
            this.activityMembersToolStripMenuItem = new ToolStripMenuItem();
            this.geographyMembersToolStripMenuItem = new ToolStripMenuItem();
            this.marketMembersToolStripMenuItem = new ToolStripMenuItem();
            this.channelMembersToolStripMenuItem = new ToolStripMenuItem();
            this.channelHeirarchySetMemberToolStripMenuItem = new ToolStripMenuItem();
            this.useMembersToolStripMenuItem = new ToolStripMenuItem();
            this.productMembersToolStripMenuItem = new ToolStripMenuItem();
            this.measureMembersToolStripMenuItem = new ToolStripMenuItem();
            this.modelSizeSetMembersToolStripMenuItem = new ToolStripMenuItem();

            // report management
            this.expansionFactorToolStripMenuItem = new ToolStripMenuItem();
            this.cellDisclosureToolStripMenuItem = new ToolStripMenuItem();
            this.releaseExceptionToolStripMenuItem = new ToolStripMenuItem();
            this.manufacturerBrandToolStripMenuItem = new ToolStripMenuItem();
            this.inputFormToolStripMenuItem = new ToolStripMenuItem();
            this.manufacturerReportToolStripMenuItem = new ToolStripMenuItem();
            this.manufacturerMissingBrandToolStripMenuItem = new ToolStripMenuItem();
            this.suppressReportToolStripMenuItem = new ToolStripMenuItem();
            this.editPLogListToolStripMenuItem = new ToolStripMenuItem();
            this.outputReportDistributionManufacturerToolStripMenuItem = new ToolStripMenuItem();
            this.outputReportDistributionUserToolStripMenuItem = new ToolStripMenuItem();
            this.outputReportPublicToolStripMenuItem = new ToolStripMenuItem();
            this.inputColumnToolStripMenuItem = new ToolStripMenuItem();
            this.inputRowToolStripMenuItem = new ToolStripMenuItem();
            this.reportCompareMasterToolStripMenuItem = new ToolStripMenuItem();
            this.reportCompareDetailToolStripMenuItem = new ToolStripMenuItem();

            // contact management
            this.contactRolesToolStripMenuItem = new ToolStripMenuItem();
            this.fsListToolStripMenuItem = new ToolStripMenuItem();

            // relationships
            this.productMarketToolStripMenuItem = new ToolStripMenuItem();
            this.productSumToolStripMenuItem = new ToolStripMenuItem();

            //heirarchies
            this.channelHeirarchySetMemberToolStripMenuItem = new ToolStripMenuItem();
            this.channelHeirarchySetToolStripMenuItem = new ToolStripMenuItem();
            
            // 
            // codesToolStripMenuItem
            // 
            this.codesToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] {
                this.dictionariesToolStripMenuItem,
                this.setDefinitionToolStripMenuItem,
                this.relationshipsToolStripMenuItem,
                this.setManagementToolStripMenuItem
            });
            this.codesToolStripMenuItem.Name = "codesToolStripMenuItem";
            //this.codesToolStripMenuItem.Size = new Size(49, 20);
            this.codesToolStripMenuItem.Text = "&Codes";


            // 
            // dictionariesToolStripMenuItem
            // 
            this.dictionariesToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] {
                this.activityToolStripMenuItem,
                this.associationToolStripMenuItem,
                this.catalogToolStripMenuItem,
                this.channelToolStripMenuItem,
                this.councilToolStripMenuItem,
                this.dimensionToolStripMenuItem,
                this.footnoteToolStripMenuItem,
                this.geographyToolStripMenuItem,
                this.groupToolStripMenuItem,
                this.holidayDateToolStripMenuItem,
                this.holidayCADateToolStripMenuItem,
                this.manufacturerToolStripMenuItem,
                this.marketToolStripMenuItem,
                this.measureToolStripMenuItem,
                this.productToolStripMenuItem,
                this.productDimensionToolStripMenuItem,
                this.useToolStripMenuItem
                });
            this.dictionariesToolStripMenuItem.Name = "dictionariesToolStripMenuItem";
            this.dictionariesToolStripMenuItem.Text = "&Dictionaries";
            // 
            // catalogToolStripMenuItem
            // 
            this.catalogToolStripMenuItem.Name = "catalogToolStripMenuItem";
            this.catalogToolStripMenuItem.Text = "Catalog definition";
            this.catalogToolStripMenuItem.Click += new EventHandler(catalogToolStripMenuItem_Click);
            //
            // activityToolStripMenuItem
            //
            this.activityToolStripMenuItem.Name = "activityToolStringMenuItem";
            this.activityToolStripMenuItem.Text = "Activity definition";
            this.activityToolStripMenuItem.Click += new EventHandler(activityToolStripMenuItem_Click);
            //
            // geographyToolStripMenuItem
            //
            this.geographyToolStripMenuItem.Name = "geographyToolStringMenuItem";
            this.geographyToolStripMenuItem.Text = "Geography definition";
            this.geographyToolStripMenuItem.Click += new EventHandler(geographyToolStripMenuItem_Click);
            //
            // marketToolStripMenuItem
            //
            this.marketToolStripMenuItem.Name = "marketToolStripMenuItem";
            this.marketToolStripMenuItem.Text = "Market definition";
            this.marketToolStripMenuItem.Click += new EventHandler(marketToolStripMenuItem_Click);
            //
            // channelToolStripMenuItem
            //
            this.channelToolStripMenuItem.Name = "channelToolStripMenuItem";
            this.channelToolStripMenuItem.Text = "Channel definition";
            this.channelToolStripMenuItem.Click += new EventHandler(channelToolStripMenuItem_Click);
            // 
            // useToolStripMenuItem
            // 
            this.useToolStripMenuItem.Name = "useToolStripMenuItem";
            this.useToolStripMenuItem.Text = "Use definition";
            this.useToolStripMenuItem.Click += new EventHandler(useToolStripMenuItem_Click);
            //
            // productToolStripMenuItem
            //
            this.productToolStripMenuItem.Name = "productToolStringMenuItem";
            this.productToolStripMenuItem.Text = "Product definition";
            this.productToolStripMenuItem.Click += new EventHandler(productToolStripMenuItem_Click);
            // 
            // measureToolStripMenuItem
            // 
            this.measureToolStripMenuItem.Name = "measureToolStripMenuItem";
            this.measureToolStripMenuItem.Text = "Measure definition";
            this.measureToolStripMenuItem.Click += new EventHandler(measureToolStripMenuItem_Click);
            //
            // councilToolStripMenuItem
            //
            this.councilToolStripMenuItem.Name = "measureToolStripMenuItem";
            this.councilToolStripMenuItem.Text = "Council definition";
            this.councilToolStripMenuItem.Click += new EventHandler(councilToolStripMenuItem_Click);
            //
            // groupToolStripMenuItem
            //
            this.groupToolStripMenuItem.Name = "groupToolStripMenuItem";
            this.groupToolStripMenuItem.Text = "Group definition";
            this.groupToolStripMenuItem.Click += new EventHandler(groupToolStripMenuItem_Click);
            //
            // expansionFactorToolStripMenuItem
            //
            this.expansionFactorToolStripMenuItem.Name = "expansionFactorToolStripMenuItem";
            this.expansionFactorToolStripMenuItem.Text="Expansion factors";
            this.expansionFactorToolStripMenuItem.Click += new EventHandler(expansionFactorToolStripMenuItem_Click);
            //
            //footnoteToolStripMenuItem
            //
            this.footnoteToolStripMenuItem.Name = "footnoteToolStripMenuItem";
            this.footnoteToolStripMenuItem.Text = "Footnote definition";
            this.footnoteToolStripMenuItem.Click += new EventHandler(footnoteToolStripMenuItem_Click);
            //
            //holidayDateToolStripMenuItem
            //
            this.holidayDateToolStripMenuItem.Name = "holidayDateToolStripMenuItem";
            this.holidayDateToolStripMenuItem.Text = "Holiday date definition";
            this.holidayDateToolStripMenuItem.Click += new EventHandler(holidayDateToolStripMenuItem_Click);
            //
            //holidayCADateToolStripMenuItem
            //
            this.holidayCADateToolStripMenuItem.Name = "holidayDateToolStripMenuItem";
            this.holidayCADateToolStripMenuItem.Text = "Holiday date definition - Canada";
            this.holidayCADateToolStripMenuItem.Click += new EventHandler(holidayCADateToolStripMenuItem_Click);
            //
            //manufacturerToolStripMenuItem
            //
            this.manufacturerToolStripMenuItem.Name = "manufacturerToolStripMenuItem";
            this.manufacturerToolStripMenuItem.Text = "Manufacturer definition";
            this.manufacturerToolStripMenuItem.Click += new EventHandler(manufacturerToolStripMenuItem_Click);
            //
            //associationToolStripMenuItem
            //
            this.associationToolStripMenuItem.Name = "associationToolStripMenuItem";
            this.associationToolStripMenuItem.Text = "Association definition";
            this.associationToolStripMenuItem.Click += new EventHandler(associationToolStripMenuItem_Click);
            //
            //dimensionToolStripMenuItem
            //
            this.dimensionToolStripMenuItem.Name = "dimensionToolStripMenuItem";
            this.dimensionToolStripMenuItem.Text = "Dimension definition";
            this.dimensionToolStripMenuItem.Click += new EventHandler(dimensionToolStripMenuItem_Click);
            //
            //productDimensionToolStripMenuItem
            //
            this.productDimensionToolStripMenuItem.Name = "productDimensionToolStripMenuItem";
            this.productDimensionToolStripMenuItem.Text = "Product dimension definition";
            this.productDimensionToolStripMenuItem.Click += new EventHandler(productDimensionToolStripMenuItem_Click);



            //
            // setDefinitionToolStripMenuItem
            //
            this.setDefinitionToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] {
                activitySetToolStripMenuItem,
                catalogSetToolStripMenuItem,
                channelSetToolStripMenuItem,
                geographySetToolStripMenuItem,
                marketSetToolStripMenuItem,
                measureSetToolStripMenuItem,
                modelSizeSetToolStripMenuItem,
                productSetToolStripMenuItem,
                useSetToolStripMenuItem});
            this.setDefinitionToolStripMenuItem.Name = "setDefinitionToolStripMenuItem";
            this.setDefinitionToolStripMenuItem.Text = "&Set definition";
            //
            // catalogSetToolStripMenuItem
            //
            this.catalogSetToolStripMenuItem.Name = "catalogSetToolStripMenuItem";
            this.catalogSetToolStripMenuItem.Text = "Catalog set";
            this.catalogSetToolStripMenuItem.Click += new EventHandler(catalogSetToolStripMenuItem_Click);
            //
            // activitySetToolStripMenuItem
            //
            this.activitySetToolStripMenuItem.Name = "activitySetToolStripMenuItem";
            this.activitySetToolStripMenuItem.Text = "Activity set";
            this.activitySetToolStripMenuItem.Click += new EventHandler(activitySetToolStripMenuItem_Click);
            //
            // geographySetToolStripMenuItem
            //
            this.geographySetToolStripMenuItem.Name = "geographySetToolStripMenuItem";
            this.geographySetToolStripMenuItem.Text = "Geography set";
            this.geographySetToolStripMenuItem.Click += new EventHandler(geographySetToolStripMenuItem_Click);
            //
            // marketSetToolStripMenuItem
            //
            this.marketSetToolStripMenuItem.Name = "marketSetToolStripMenuItem";
            this.marketSetToolStripMenuItem.Text = "Market set";
            this.marketSetToolStripMenuItem.Click += new EventHandler(marketSetToolStripMenuItem_Click);
            //
            // channelSetToolStripMenuItem
            //
            this.channelSetToolStripMenuItem.Name = "channelSetToolStripMenuItem";
            this.channelSetToolStripMenuItem.Text = "Channel set";
            this.channelSetToolStripMenuItem.Click += new EventHandler(channelSetToolStripMenuItem_Click);
            //
            // useSetToolStripMenuItem
            //
            this.useSetToolStripMenuItem.Name = "useSetToolStripMenuItem";
            this.useSetToolStripMenuItem.Text = "Use set";
            this.useSetToolStripMenuItem.Click += new EventHandler(useSetToolStripMenuItem_Click);
            //
            // productSetToolStripMenuItem
            //
            this.productSetToolStripMenuItem.Name = "productSetToolStripMenuItem";
            this.productSetToolStripMenuItem.Text = "Product set";
            this.productSetToolStripMenuItem.Click += new EventHandler(productSetToolStripMenuItem_Click);
            //
            // measureSetToolStripMenuItem
            //
            this.measureSetToolStripMenuItem.Name = "measureSetToolStripMenuItem";
            this.measureSetToolStripMenuItem.Text = "Measure set";
            this.measureSetToolStripMenuItem.Click += new EventHandler(measureSetToolStripMenuItem_Click);
            //
            // modelSizeSetToolStripMenuItem
            //
            this.modelSizeSetToolStripMenuItem.Name = "modelSizeSetToolStripMenuItem";
            this.modelSizeSetToolStripMenuItem.Text = "Model Size set";
            this.modelSizeSetToolStripMenuItem.Click += new EventHandler(modelSizeSetToolStripMenuItem_Click);


            // 
            // setManagementToolStripMenuItem
            // 
            this.setManagementToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] {
                this.activityMembersToolStripMenuItem,
                this.catalogMembersToolStripMenuItem,
                this.channelMembersToolStripMenuItem,
                this.geographyMembersToolStripMenuItem,
                this.marketMembersToolStripMenuItem,
                this.measureMembersToolStripMenuItem,
                this.modelSizeSetMembersToolStripMenuItem,
                this.productMembersToolStripMenuItem,
                this.useMembersToolStripMenuItem});

            this.setManagementToolStripMenuItem.Name = "setManagementToolStripMenuItem";
            this.setManagementToolStripMenuItem.Text = "Set management";

            //
            // catalogMembersToolStripMenuItem
            //
            this.catalogMembersToolStripMenuItem.Name = "catalogMembersToolStripMenuItem";
            this.catalogMembersToolStripMenuItem.Text = "Catalog members";
            this.catalogMembersToolStripMenuItem.Click += new EventHandler(catalogMembersToolStripMenuItem_Click);
            //
            // activityMembersToolStripMenuItem
            //
            this.activityMembersToolStripMenuItem.Name = "activityMembersToolStripMenuItem";
            this.activityMembersToolStripMenuItem.Text = "Activity members";
            this.activityMembersToolStripMenuItem.Click += new EventHandler(activityMembersToolStripMenuItem_Click);
            //
            // geographyMembersToolStripMenuItem
            //
            this.geographyMembersToolStripMenuItem.Name = "geographyMembersToolStripMenuItem";
            this.geographyMembersToolStripMenuItem.Text = "Geography members";
            this.geographyMembersToolStripMenuItem.Click += new EventHandler(geographyMembersToolStripMenuItem_Click);
            //
            // marketMembersToolStripMenuItem
            //
            this.marketMembersToolStripMenuItem.Name = "marketMembersToolStripMenuItem";
            this.marketMembersToolStripMenuItem.Text = "Market members";
            this.marketMembersToolStripMenuItem.Click+=new EventHandler(marketMembersToolStripMenuItem_Click);
            //
            // channelMembersToolStripMenuItem
            //
            this.channelMembersToolStripMenuItem.Name = "channelMembersToolStripMenuItem";
            this.channelMembersToolStripMenuItem.Text = "Channel members";
            this.channelMembersToolStripMenuItem.Click += new EventHandler(channelMembersToolStripMenuItem_Click);
            //
            // useMembersToolStripMenuItem
            //
            this.useMembersToolStripMenuItem.Name = "useMembersToolStripMenuItem";
            this.useMembersToolStripMenuItem.Text = "Use &members";
            this.useMembersToolStripMenuItem.Click += new EventHandler(useMembersToolStripMenuItem_Click);
            //
            // productMembersToolStripMenuItem
            //
            this.productMembersToolStripMenuItem.Name = "productMembersToolStripMenuItem";
            this.productMembersToolStripMenuItem.Text = "Product members";
            this.productMembersToolStripMenuItem.Click += new EventHandler(productMembersToolStripMenuItem_Click);
            //
            // measureMembersToolStripMenuItem
            //
            this.measureMembersToolStripMenuItem.Name = "measureMembersToolStripMenuItem";
            this.measureMembersToolStripMenuItem.Text = "Measure members";
            this.measureMembersToolStripMenuItem.Click += new EventHandler(measureMembersToolStripMenuItem_Click);
            //
            // modelSizeSetMembersToolStripMenuItem
            //
            this.modelSizeSetMembersToolStripMenuItem.Name = "modelSizeSetMembersToolStripMenuItem";
            this.modelSizeSetMembersToolStripMenuItem.Text = "Model size set members";
            this.modelSizeSetMembersToolStripMenuItem.Click += new EventHandler(modelSizeSetMembersToolStripMenuItem_Click);


            //
            // Report Management
            //
            //
            // reportManagementToolStripMenuItem
            //
            this.reportManagementToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { 
                this.cellDisclosureToolStripMenuItem,
                this.editPLogListToolStripMenuItem,
                this.expansionFactorToolStripMenuItem,
                this.inputColumnToolStripMenuItem,
                this.inputFormToolStripMenuItem,
                this.inputRowToolStripMenuItem,
                this.manufacturerBrandToolStripMenuItem,
                this.manufacturerMissingBrandToolStripMenuItem,
                this.manufacturerReportToolStripMenuItem,
                this.outputReportDistributionManufacturerToolStripMenuItem,
                this.outputReportDistributionUserToolStripMenuItem,
                this.outputReportPublicToolStripMenuItem,
                this.releaseExceptionToolStripMenuItem,
                this.reportCompareDetailToolStripMenuItem,
                this.reportCompareMasterToolStripMenuItem,
                this.suppressReportToolStripMenuItem
            });
            this.reportManagementToolStripMenuItem.Name = "reportManagementToolStripMenuItem";
            this.reportManagementToolStripMenuItem.Text = "&Report management";

            //
            // cellDisclosureToolStripMenuItem
            //
            this.cellDisclosureToolStripMenuItem.Name = "cellDisclosureToolStripMenuItem";
            this.cellDisclosureToolStripMenuItem.Text = "Cell disclosures";
            this.cellDisclosureToolStripMenuItem.Click += new EventHandler(cellDisclosureToolStripMenuItem_Click);

            //
            // releaseExceptionToolStripMenuItem
            //
            this.releaseExceptionToolStripMenuItem.Name = "releaseExceptionToolStripMenuItem";
            this.releaseExceptionToolStripMenuItem.Text = "Release exceptions";
            this.releaseExceptionToolStripMenuItem.Click += new EventHandler(releaseExceptionToolStripMenuItem_Click);

            //
            //manufacturerBrandToolStripMenuItem
            //
            this.manufacturerBrandToolStripMenuItem.Name = "manufacturerBrandToolStripMenuItem";
            this.manufacturerBrandToolStripMenuItem.Text = "Manufacturer brands";
            this.manufacturerBrandToolStripMenuItem.Click += new EventHandler(manufacturerBrandToolStripMenuItem_Click);

            //
            //manufacturerReportToolStripMenuItem
            //
            this.manufacturerReportToolStripMenuItem.Name = "manufacturerReportToolStripMenuItem";
            this.manufacturerReportToolStripMenuItem.Text = "Manufacturer reports";
            this.manufacturerReportToolStripMenuItem.Click += new EventHandler(manufacturerReportToolStripMenuItem_Click);

            //
            //inputFormToolStripMenuItem
            //
            this.inputFormToolStripMenuItem.Name = "inputFormToolStripMenuItem";
            this.inputFormToolStripMenuItem.Text = "Input forms";
            this.inputFormToolStripMenuItem.Click += new EventHandler(inputFormToolStripMenuItem_Click);

            //
            // manufacturerMissingBrandToolStripMenuItem
            //
            this.manufacturerMissingBrandToolStripMenuItem.Name = "manufacturerMissingBrandToolStripMenuItem";
            this.manufacturerMissingBrandToolStripMenuItem.Text = "Manufacturer missing brands";
            this.manufacturerMissingBrandToolStripMenuItem.Click += new EventHandler(manufacturerMissingBrandToolStripMenuItem_Click);

            //
            // suppressReportToolStripMenuItem
            //
            this.suppressReportToolStripMenuItem.Name = "suppressReportToolStripMenuItem";
            this.suppressReportToolStripMenuItem.Text = "Suppress reports";
            this.suppressReportToolStripMenuItem.Click += new EventHandler(suppressReportToolStripMenuItem_Click);

            //
            // editPLogListToolStripMenuItem
            //
            this.editPLogListToolStripMenuItem.Name = "editPLogListToolStripMenuItem";
            this.editPLogListToolStripMenuItem.Text = "Edit PLog Lists";
            this.editPLogListToolStripMenuItem.Click += new EventHandler(editPLogListToolStripMenuItem_Click);

            //
            // outputReportManufacturerToolStripMenuItem
            //
            this.outputReportDistributionManufacturerToolStripMenuItem.Name = "outputReportManufacturerToolStripMenuItem";
            this.outputReportDistributionManufacturerToolStripMenuItem.Text = "Output report distribution manufacturer";
            this.outputReportDistributionManufacturerToolStripMenuItem.Click += new EventHandler(outputReportManufacturerToolStripMenuItem_Click);

            //
            // outputReportUserToolStripMenuItem
            //
            this.outputReportDistributionUserToolStripMenuItem.Name = "outputReportUserToolStripMenuItem";
            this.outputReportDistributionUserToolStripMenuItem.Text = "Output report distribution user";
            this.outputReportDistributionUserToolStripMenuItem.Click += new EventHandler(outputReportUserToolStripMenuItem_Click);

            //
            // this.outputReportPublicToolStripMenuItem
            //
            this.outputReportPublicToolStripMenuItem.Name = "outputReportPublicToolStripMenuItem";
            this.outputReportPublicToolStripMenuItem.Text = "Output report public";
            this.outputReportPublicToolStripMenuItem.Click += new EventHandler(outputReportPublicToolStripMenuItem_Click);

            //
            // inputColumnToolStripMenuItem
            //
            this.inputColumnToolStripMenuItem.Name = "inputColumnToolStripMenuItem";
            this.inputColumnToolStripMenuItem.Text = "Input columns";
            this.inputColumnToolStripMenuItem.Click += new EventHandler(inputColumnToolStripMenuItem_Click);

            //
            // inputRowToolStripMenuItem
            //
            this.inputRowToolStripMenuItem.Name = "inputRowToolStripMenuItem";
            this.inputRowToolStripMenuItem.Text = "Input rows";
            this.inputRowToolStripMenuItem.Click += new EventHandler(inputRowToolStripMenuItem_Click);

            //
            // reportCompareMasterToolStripMenuItem
            //
            this.reportCompareMasterToolStripMenuItem.Name = "reportCompareMasterToolStripMenuItem";
            this.reportCompareMasterToolStripMenuItem.Text = "Report compare master";
            this.reportCompareMasterToolStripMenuItem.Click += new EventHandler(reportCompareMasterToolStripMenuItem_Click);

            //
            // reportCompareDetailToolStripMenuItem
            //
            this.reportCompareDetailToolStripMenuItem.Name = "reportCompareDetailToolStripMenuItem";
            this.reportCompareDetailToolStripMenuItem.Text = "Report compare detail";
            this.reportCompareDetailToolStripMenuItem.Click += new EventHandler(reportCompareDetailToolStripMenuItem_Click);


            //
            // Contact Management
            //
            //
            // contactManagementToolStripMenuItem 
            //
            this.contactManagementToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { 
                this.contactRolesToolStripMenuItem,
                this.fsListToolStripMenuItem
            });
            this.contactManagementToolStripMenuItem.Name = "catalogMembersToolStripMenuItem";
            this.contactManagementToolStripMenuItem.Text = "Con&tact management";

            //
            // contactRolesToolStripMenuItem
            //
            this.contactRolesToolStripMenuItem.Name = "contactRolesToolStripMenuItem";
            this.contactRolesToolStripMenuItem.Text = "Contact roles";
            this.contactRolesToolStripMenuItem.Click+=new EventHandler(contactRolesToolStripMenuItem_Click);

            //
            //fsListToolStripMenuItem
            //
            this.fsListToolStripMenuItem.Name = "fsListToolStripMenuItem";
            this.fsListToolStripMenuItem.Text = "FS List";
            this.fsListToolStripMenuItem.Click += new EventHandler(fsListToolStripMenuItem_Click);


            //
            // Relationships
            //
            //
            this.relationshipsToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] {
                this.productMarketToolStripMenuItem,
                this.productSumToolStripMenuItem
            });
            this.Name = "relationshipsToolStripMenuItem";
            this.relationshipsToolStripMenuItem.Text = "Relationships";

            //
            // this.productMarketToolStripMenuItem
            //
            this.productMarketToolStripMenuItem.Name = "productMarketToolStripMenuItem";
            this.productMarketToolStripMenuItem.Text = "Product/Market/Channel";
            this.productMarketToolStripMenuItem.Click += new EventHandler(productMarketToolStripMenuItem_Click);

            //
            // productSumToolStripMenuItem
            //
            this.productSumToolStripMenuItem.Name = "productSumToolStripMenuItem";
            this.productSumToolStripMenuItem.Text = "Product aggregates";
            this.productSumToolStripMenuItem.Click += new EventHandler(productSumToolStripMenuItem_Click);

            //
            //  heirarchies
            //
            this.heirarchiesToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] {
                this.channelHeirarchySetToolStripMenuItem,
                this.channelHeirarchySetMemberToolStripMenuItem,
                });
            this.heirarchiesToolStripMenuItem.Name = "heirarchiesToolStripMenuItem";
            this.heirarchiesToolStripMenuItem.Text = "&Heirarchies";
            //
            // channelHeirarchySetToolStripMenuItem
            //
            this.channelHeirarchySetToolStripMenuItem.Name = "channelHeirarchySetToolStripMenuItem";
            this.channelHeirarchySetToolStripMenuItem.Text = "Channel Heirarchies";
            this.channelHeirarchySetToolStripMenuItem.Click += new EventHandler(cchannelHeirarchySetToolStripMenuItem_Click);
            //
            // channelHeirarchyMembersToolStripMenuItem
            //
            this.channelHeirarchySetMemberToolStripMenuItem.Name = "channelHeirarchySetMemberToolStripMenuItem";
            this.channelHeirarchySetMemberToolStripMenuItem.Text = "Channel Heirarchy members";
            this.channelHeirarchySetMemberToolStripMenuItem.Click += new EventHandler(channelHeirarchyMembersToolStripMenuItem_Click);

            _mainMenu.Items.AddRange(new ToolStripItem[] {
                this.codesToolStripMenuItem,
                this.reportManagementToolStripMenuItem,
                this.relationshipsToolStripMenuItem,
                this.heirarchiesToolStripMenuItem,
                this.contactManagementToolStripMenuItem
            });
        }


        #endregion


        // TESTING SECTION

        private void button1_Click(object sender, EventArgs e)
        {
            EditFormParameters parameters = new EditFormParameters(_gus);
            TestForm theForm = new TestForm(parameters);
            theForm.ShowDialog(this);

            theForm.Dispose();
        }

        private void AhamMetaDataEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult yesNo = MessageBox.Show("Close the application?", "Confirm action", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (yesNo == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
        }

        private void AhamMetaDataEditor_Load(object sender, EventArgs e)
        {

        }

   

    }
}
