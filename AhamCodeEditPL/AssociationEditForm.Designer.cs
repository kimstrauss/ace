﻿namespace AhamMetaDataPL
{
    partial class AssociationEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label associationAbbreviationLabel;
            System.Windows.Forms.Label associationKeyLabel;
            System.Windows.Forms.Label associationNameLabel;
            this.associationAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.associationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.associationKeyTextBox = new System.Windows.Forms.TextBox();
            this.associationNameTextBox = new System.Windows.Forms.TextBox();
            associationAbbreviationLabel = new System.Windows.Forms.Label();
            associationKeyLabel = new System.Windows.Forms.Label();
            associationNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.associationBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(391, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(472, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // associationAbbreviationLabel
            // 
            associationAbbreviationLabel.AutoSize = true;
            associationAbbreviationLabel.Location = new System.Drawing.Point(13, 15);
            associationAbbreviationLabel.Name = "associationAbbreviationLabel";
            associationAbbreviationLabel.Size = new System.Drawing.Size(126, 13);
            associationAbbreviationLabel.TabIndex = 30;
            associationAbbreviationLabel.Text = "Association Abbreviation:";
            // 
            // associationKeyLabel
            // 
            associationKeyLabel.AutoSize = true;
            associationKeyLabel.Location = new System.Drawing.Point(13, 67);
            associationKeyLabel.Name = "associationKeyLabel";
            associationKeyLabel.Size = new System.Drawing.Size(85, 13);
            associationKeyLabel.TabIndex = 32;
            associationKeyLabel.Text = "Association Key:";
            // 
            // associationNameLabel
            // 
            associationNameLabel.AutoSize = true;
            associationNameLabel.Location = new System.Drawing.Point(13, 41);
            associationNameLabel.Name = "associationNameLabel";
            associationNameLabel.Size = new System.Drawing.Size(95, 13);
            associationNameLabel.TabIndex = 34;
            associationNameLabel.Text = "Association Name:";
            // 
            // associationAbbreviationTextBox
            // 
            this.associationAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.associationBindingSource, "AssociationAbbreviation", true));
            this.associationAbbreviationTextBox.Location = new System.Drawing.Point(145, 12);
            this.associationAbbreviationTextBox.Name = "associationAbbreviationTextBox";
            this.associationAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.associationAbbreviationTextBox.TabIndex = 0;
            // 
            // associationBindingSource
            // 
            this.associationBindingSource.DataSource = typeof(AhamMetaDataDAL.Association);
            // 
            // associationKeyTextBox
            // 
            this.associationKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.associationBindingSource, "AssociationKey", true));
            this.associationKeyTextBox.Location = new System.Drawing.Point(145, 64);
            this.associationKeyTextBox.Name = "associationKeyTextBox";
            this.associationKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.associationKeyTextBox.TabIndex = 2;
            this.associationKeyTextBox.TabStop = false;
            // 
            // associationNameTextBox
            // 
            this.associationNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.associationBindingSource, "AssociationName", true));
            this.associationNameTextBox.Location = new System.Drawing.Point(145, 38);
            this.associationNameTextBox.Name = "associationNameTextBox";
            this.associationNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.associationNameTextBox.TabIndex = 1;
            // 
            // AssociationEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 216);
            this.Controls.Add(associationAbbreviationLabel);
            this.Controls.Add(this.associationAbbreviationTextBox);
            this.Controls.Add(associationKeyLabel);
            this.Controls.Add(this.associationKeyTextBox);
            this.Controls.Add(associationNameLabel);
            this.Controls.Add(this.associationNameTextBox);
            this.Name = "AssociationEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AssociationEditForm";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.associationNameTextBox, 0);
            this.Controls.SetChildIndex(associationNameLabel, 0);
            this.Controls.SetChildIndex(this.associationKeyTextBox, 0);
            this.Controls.SetChildIndex(associationKeyLabel, 0);
            this.Controls.SetChildIndex(this.associationAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(associationAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.associationBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource associationBindingSource;
        private System.Windows.Forms.TextBox associationAbbreviationTextBox;
        private System.Windows.Forms.TextBox associationKeyTextBox;
        private System.Windows.Forms.TextBox associationNameTextBox;
    }
}