﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class AssociationEditForm : HaiObjectEditForm
    {
        private Association _associationToEdit;

        public AssociationEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _associationToEdit = (Association)parameters.EditObject;

            associationBindingSource.DataSource = _associationToEdit;
            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (associationAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (associationNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_associationToEdit);
            _associationToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
