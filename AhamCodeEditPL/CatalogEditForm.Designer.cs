﻿namespace AhamMetaDataPL
{
    partial class CatalogEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label catalogAbbreviationLabel;
            System.Windows.Forms.Label catalogKeyLabel;
            System.Windows.Forms.Label catalogNameLabel;
            this.catalogBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.catalogAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.catalogKeyTextBox = new System.Windows.Forms.TextBox();
            this.catalogNameTextBox = new System.Windows.Forms.TextBox();
            catalogAbbreviationLabel = new System.Windows.Forms.Label();
            catalogKeyLabel = new System.Windows.Forms.Label();
            catalogNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.catalogBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // catalogAbbreviationLabel
            // 
            catalogAbbreviationLabel.AutoSize = true;
            catalogAbbreviationLabel.Location = new System.Drawing.Point(12, 15);
            catalogAbbreviationLabel.Name = "catalogAbbreviationLabel";
            catalogAbbreviationLabel.Size = new System.Drawing.Size(108, 13);
            catalogAbbreviationLabel.TabIndex = 30;
            catalogAbbreviationLabel.Text = "Catalog Abbreviation:";
            // 
            // catalogKeyLabel
            // 
            catalogKeyLabel.AutoSize = true;
            catalogKeyLabel.Location = new System.Drawing.Point(12, 67);
            catalogKeyLabel.Name = "catalogKeyLabel";
            catalogKeyLabel.Size = new System.Drawing.Size(67, 13);
            catalogKeyLabel.TabIndex = 32;
            catalogKeyLabel.Text = "Catalog Key:";
            // 
            // catalogNameLabel
            // 
            catalogNameLabel.AutoSize = true;
            catalogNameLabel.Location = new System.Drawing.Point(12, 41);
            catalogNameLabel.Name = "catalogNameLabel";
            catalogNameLabel.Size = new System.Drawing.Size(77, 13);
            catalogNameLabel.TabIndex = 34;
            catalogNameLabel.Text = "Catalog Name:";
            // 
            // catalogBindingSource
            // 
            this.catalogBindingSource.DataSource = typeof(AhamMetaDataDAL.Catalog);
            // 
            // catalogAbbreviationTextBox
            // 
            this.catalogAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.catalogBindingSource, "CatalogAbbreviation", true));
            this.catalogAbbreviationTextBox.Location = new System.Drawing.Point(126, 12);
            this.catalogAbbreviationTextBox.Name = "catalogAbbreviationTextBox";
            this.catalogAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.catalogAbbreviationTextBox.TabIndex = 0;
            // 
            // catalogKeyTextBox
            // 
            this.catalogKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.catalogBindingSource, "CatalogKey", true));
            this.catalogKeyTextBox.Location = new System.Drawing.Point(126, 64);
            this.catalogKeyTextBox.Name = "catalogKeyTextBox";
            this.catalogKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.catalogKeyTextBox.TabIndex = 2;
            this.catalogKeyTextBox.TabStop = false;
            // 
            // catalogNameTextBox
            // 
            this.catalogNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.catalogBindingSource, "CatalogName", true));
            this.catalogNameTextBox.Location = new System.Drawing.Point(126, 38);
            this.catalogNameTextBox.Name = "catalogNameTextBox";
            this.catalogNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.catalogNameTextBox.TabIndex = 1;
            // 
            // CatalogEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(catalogAbbreviationLabel);
            this.Controls.Add(this.catalogAbbreviationTextBox);
            this.Controls.Add(catalogKeyLabel);
            this.Controls.Add(this.catalogKeyTextBox);
            this.Controls.Add(catalogNameLabel);
            this.Controls.Add(this.catalogNameTextBox);
            this.Name = "CatalogEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Catalog";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.catalogNameTextBox, 0);
            this.Controls.SetChildIndex(catalogNameLabel, 0);
            this.Controls.SetChildIndex(this.catalogKeyTextBox, 0);
            this.Controls.SetChildIndex(catalogKeyLabel, 0);
            this.Controls.SetChildIndex(this.catalogAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(catalogAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.catalogBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource catalogBindingSource;
        private System.Windows.Forms.TextBox catalogAbbreviationTextBox;
        private System.Windows.Forms.TextBox catalogKeyTextBox;
        private System.Windows.Forms.TextBox catalogNameTextBox;
    }
}