﻿namespace AhamMetaDataPL
{
    partial class CatalogSetEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label catalogSetAbbreviationLabel;
            System.Windows.Forms.Label catalogSetKeyLabel;
            System.Windows.Forms.Label catalogSetNameLabel;
            this.catalogSetAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.catalogSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.catalogSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.catalogSetNameTextBox = new System.Windows.Forms.TextBox();
            catalogSetAbbreviationLabel = new System.Windows.Forms.Label();
            catalogSetKeyLabel = new System.Windows.Forms.Label();
            catalogSetNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.catalogSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(386, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(467, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // catalogSetAbbreviationLabel
            // 
            catalogSetAbbreviationLabel.AutoSize = true;
            catalogSetAbbreviationLabel.Location = new System.Drawing.Point(10, 15);
            catalogSetAbbreviationLabel.Name = "catalogSetAbbreviationLabel";
            catalogSetAbbreviationLabel.Size = new System.Drawing.Size(127, 13);
            catalogSetAbbreviationLabel.TabIndex = 30;
            catalogSetAbbreviationLabel.Text = "Catalog Set Abbreviation:";
            // 
            // catalogSetKeyLabel
            // 
            catalogSetKeyLabel.AutoSize = true;
            catalogSetKeyLabel.Location = new System.Drawing.Point(10, 67);
            catalogSetKeyLabel.Name = "catalogSetKeyLabel";
            catalogSetKeyLabel.Size = new System.Drawing.Size(86, 13);
            catalogSetKeyLabel.TabIndex = 32;
            catalogSetKeyLabel.Text = "Catalog Set Key:";
            // 
            // catalogSetNameLabel
            // 
            catalogSetNameLabel.AutoSize = true;
            catalogSetNameLabel.Location = new System.Drawing.Point(10, 41);
            catalogSetNameLabel.Name = "catalogSetNameLabel";
            catalogSetNameLabel.Size = new System.Drawing.Size(96, 13);
            catalogSetNameLabel.TabIndex = 34;
            catalogSetNameLabel.Text = "Catalog Set Name:";
            // 
            // catalogSetAbbreviationTextBox
            // 
            this.catalogSetAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.catalogSetBindingSource, "CatalogSetAbbreviation", true));
            this.catalogSetAbbreviationTextBox.Location = new System.Drawing.Point(143, 12);
            this.catalogSetAbbreviationTextBox.Name = "catalogSetAbbreviationTextBox";
            this.catalogSetAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.catalogSetAbbreviationTextBox.TabIndex = 1;
            // 
            // catalogSetBindingSource
            // 
            this.catalogSetBindingSource.DataSource = typeof(AhamMetaDataDAL.CatalogSet);
            // 
            // catalogSetKeyTextBox
            // 
            this.catalogSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.catalogSetBindingSource, "CatalogSetKey", true));
            this.catalogSetKeyTextBox.Location = new System.Drawing.Point(143, 64);
            this.catalogSetKeyTextBox.Name = "catalogSetKeyTextBox";
            this.catalogSetKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.catalogSetKeyTextBox.TabIndex = 2;
            this.catalogSetKeyTextBox.TabStop = false;
            // 
            // catalogSetNameTextBox
            // 
            this.catalogSetNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.catalogSetBindingSource, "CatalogSetName", true));
            this.catalogSetNameTextBox.Location = new System.Drawing.Point(143, 38);
            this.catalogSetNameTextBox.Name = "catalogSetNameTextBox";
            this.catalogSetNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.catalogSetNameTextBox.TabIndex = 0;
            // 
            // CatalogSetEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 216);
            this.Controls.Add(catalogSetAbbreviationLabel);
            this.Controls.Add(this.catalogSetAbbreviationTextBox);
            this.Controls.Add(catalogSetKeyLabel);
            this.Controls.Add(this.catalogSetKeyTextBox);
            this.Controls.Add(catalogSetNameLabel);
            this.Controls.Add(this.catalogSetNameTextBox);
            this.Name = "CatalogSetEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Catalog set";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.catalogSetNameTextBox, 0);
            this.Controls.SetChildIndex(catalogSetNameLabel, 0);
            this.Controls.SetChildIndex(this.catalogSetKeyTextBox, 0);
            this.Controls.SetChildIndex(catalogSetKeyLabel, 0);
            this.Controls.SetChildIndex(this.catalogSetAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(catalogSetAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.catalogSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource catalogSetBindingSource;
        private System.Windows.Forms.TextBox catalogSetAbbreviationTextBox;
        private System.Windows.Forms.TextBox catalogSetKeyTextBox;
        private System.Windows.Forms.TextBox catalogSetNameTextBox;
    }
}