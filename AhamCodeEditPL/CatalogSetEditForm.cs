﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class CatalogSetEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private CatalogSet _catalogSetToEdit;

        public CatalogSetEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _catalogSetToEdit = (CatalogSet)parameters.EditObject;

            this.Load += new System.EventHandler(SetEditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void SetEditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            catalogSetBindingSource.DataSource = _catalogSetToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (catalogSetNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (catalogSetAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_catalogSetToEdit);
            _catalogSetToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }

    }
}
