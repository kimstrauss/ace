﻿namespace AhamMetaDataPL
{
    partial class CellDisclosureMemberEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label cellDisclosureKeyLabel;
            System.Windows.Forms.Label label6;
            this.outputReportIDComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.industryMemberNameComboBox = new System.Windows.Forms.ComboBox();
            this.sourceSizeSetMemberNameComboBox = new System.Windows.Forms.ComboBox();
            this.footnoteComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.destinationSizeSetMemberNameComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cellDisclosureBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.cellDisclosureKeyTextBox = new System.Windows.Forms.TextBox();
            this.outputReportKeyTextBox = new System.Windows.Forms.TextBox();
            this.industryMemberKeyTextBox = new System.Windows.Forms.TextBox();
            this.sourceSizeSetMemberKeyTextBox = new System.Windows.Forms.TextBox();
            this.destinationSizeSetMemberKeyTextBox = new System.Windows.Forms.TextBox();
            this.footnoteKeyTextBox = new System.Windows.Forms.TextBox();
            beginDateLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            cellDisclosureKeyLabel = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.cellDisclosureBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(839, 263);
            this.btnOK.TabIndex = 7;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(920, 263);
            this.btnCancel.TabIndex = 8;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(101, 263);
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(7, 25);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 30;
            beginDateLabel.Text = "Begin Date:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(233, 25);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 31;
            endDateLabel.Text = "End Date:";
            // 
            // cellDisclosureKeyLabel
            // 
            cellDisclosureKeyLabel.AutoSize = true;
            cellDisclosureKeyLabel.Location = new System.Drawing.Point(829, 217);
            cellDisclosureKeyLabel.Name = "cellDisclosureKeyLabel";
            cellDisclosureKeyLabel.Size = new System.Drawing.Size(100, 13);
            cellDisclosureKeyLabel.TabIndex = 32;
            cellDisclosureKeyLabel.Text = "Cell Disclosure Key:";
            // 
            // outputReportIDComboBox
            // 
            this.outputReportIDComboBox.FormattingEnabled = true;
            this.outputReportIDComboBox.Location = new System.Drawing.Point(97, 63);
            this.outputReportIDComboBox.Name = "outputReportIDComboBox";
            this.outputReportIDComboBox.Size = new System.Drawing.Size(832, 21);
            this.outputReportIDComboBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Report";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Industry";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Source size";
            // 
            // industryMemberNameComboBox
            // 
            this.industryMemberNameComboBox.FormattingEnabled = true;
            this.industryMemberNameComboBox.Location = new System.Drawing.Point(97, 90);
            this.industryMemberNameComboBox.Name = "industryMemberNameComboBox";
            this.industryMemberNameComboBox.Size = new System.Drawing.Size(832, 21);
            this.industryMemberNameComboBox.TabIndex = 3;
            // 
            // sourceSizeSetMemberNameComboBox
            // 
            this.sourceSizeSetMemberNameComboBox.FormattingEnabled = true;
            this.sourceSizeSetMemberNameComboBox.Location = new System.Drawing.Point(97, 117);
            this.sourceSizeSetMemberNameComboBox.Name = "sourceSizeSetMemberNameComboBox";
            this.sourceSizeSetMemberNameComboBox.Size = new System.Drawing.Size(832, 21);
            this.sourceSizeSetMemberNameComboBox.TabIndex = 4;
            // 
            // footnoteComboBox
            // 
            this.footnoteComboBox.FormattingEnabled = true;
            this.footnoteComboBox.Location = new System.Drawing.Point(97, 171);
            this.footnoteComboBox.Name = "footnoteComboBox";
            this.footnoteComboBox.Size = new System.Drawing.Size(832, 21);
            this.footnoteComboBox.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Footnote";
            // 
            // destinationSizeSetMemberNameComboBox
            // 
            this.destinationSizeSetMemberNameComboBox.FormattingEnabled = true;
            this.destinationSizeSetMemberNameComboBox.Location = new System.Drawing.Point(97, 144);
            this.destinationSizeSetMemberNameComboBox.Name = "destinationSizeSetMemberNameComboBox";
            this.destinationSizeSetMemberNameComboBox.Size = new System.Drawing.Size(832, 21);
            this.destinationSizeSetMemberNameComboBox.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Destination size";
            // 
            // cellDisclosureBindingSource
            // 
            this.cellDisclosureBindingSource.DataSource = typeof(AhamMetaDataDAL.CellDisclosure);
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cellDisclosureBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(97, 22);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.beginDateTextBox.TabIndex = 0;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cellDisclosureBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(294, 22);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.endDateTextBox.TabIndex = 1;
            // 
            // cellDisclosureKeyTextBox
            // 
            this.cellDisclosureKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cellDisclosureBindingSource, "CellDisclosureKey", true));
            this.cellDisclosureKeyTextBox.Location = new System.Drawing.Point(942, 214);
            this.cellDisclosureKeyTextBox.Name = "cellDisclosureKeyTextBox";
            this.cellDisclosureKeyTextBox.Size = new System.Drawing.Size(48, 20);
            this.cellDisclosureKeyTextBox.TabIndex = 33;
            this.cellDisclosureKeyTextBox.TabStop = false;
            // 
            // outputReportKeyTextBox
            // 
            this.outputReportKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cellDisclosureBindingSource, "OutputReportKey", true));
            this.outputReportKeyTextBox.Location = new System.Drawing.Point(942, 64);
            this.outputReportKeyTextBox.Name = "outputReportKeyTextBox";
            this.outputReportKeyTextBox.Size = new System.Drawing.Size(48, 20);
            this.outputReportKeyTextBox.TabIndex = 34;
            this.outputReportKeyTextBox.TabStop = false;
            // 
            // industryMemberKeyTextBox
            // 
            this.industryMemberKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cellDisclosureBindingSource, "IndustryMemberKey", true));
            this.industryMemberKeyTextBox.Location = new System.Drawing.Point(942, 91);
            this.industryMemberKeyTextBox.Name = "industryMemberKeyTextBox";
            this.industryMemberKeyTextBox.Size = new System.Drawing.Size(48, 20);
            this.industryMemberKeyTextBox.TabIndex = 35;
            this.industryMemberKeyTextBox.TabStop = false;
            // 
            // sourceSizeSetMemberKeyTextBox
            // 
            this.sourceSizeSetMemberKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cellDisclosureBindingSource, "SourceSizeSetMemberKey", true));
            this.sourceSizeSetMemberKeyTextBox.Location = new System.Drawing.Point(942, 118);
            this.sourceSizeSetMemberKeyTextBox.Name = "sourceSizeSetMemberKeyTextBox";
            this.sourceSizeSetMemberKeyTextBox.Size = new System.Drawing.Size(48, 20);
            this.sourceSizeSetMemberKeyTextBox.TabIndex = 36;
            this.sourceSizeSetMemberKeyTextBox.TabStop = false;
            // 
            // destinationSizeSetMemberKeyTextBox
            // 
            this.destinationSizeSetMemberKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cellDisclosureBindingSource, "DestinationSizeSetMemberKey", true));
            this.destinationSizeSetMemberKeyTextBox.Location = new System.Drawing.Point(942, 145);
            this.destinationSizeSetMemberKeyTextBox.Name = "destinationSizeSetMemberKeyTextBox";
            this.destinationSizeSetMemberKeyTextBox.Size = new System.Drawing.Size(48, 20);
            this.destinationSizeSetMemberKeyTextBox.TabIndex = 37;
            this.destinationSizeSetMemberKeyTextBox.TabStop = false;
            // 
            // footnoteKeyTextBox
            // 
            this.footnoteKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cellDisclosureBindingSource, "FootnoteKey", true));
            this.footnoteKeyTextBox.Location = new System.Drawing.Point(942, 172);
            this.footnoteKeyTextBox.Name = "footnoteKeyTextBox";
            this.footnoteKeyTextBox.Size = new System.Drawing.Size(48, 20);
            this.footnoteKeyTextBox.TabIndex = 38;
            this.footnoteKeyTextBox.TabStop = false;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(954, 38);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(25, 13);
            label6.TabIndex = 39;
            label6.Text = "Key";
            // 
            // CellDisclosureMemberEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1010, 298);
            this.Controls.Add(label6);
            this.Controls.Add(this.footnoteKeyTextBox);
            this.Controls.Add(this.destinationSizeSetMemberKeyTextBox);
            this.Controls.Add(this.sourceSizeSetMemberKeyTextBox);
            this.Controls.Add(this.industryMemberKeyTextBox);
            this.Controls.Add(this.outputReportKeyTextBox);
            this.Controls.Add(cellDisclosureKeyLabel);
            this.Controls.Add(this.cellDisclosureKeyTextBox);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.destinationSizeSetMemberNameComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.footnoteComboBox);
            this.Controls.Add(this.sourceSizeSetMemberNameComboBox);
            this.Controls.Add(this.industryMemberNameComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.outputReportIDComboBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "CellDisclosureMemberEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cell Disclosure";
            this.Controls.SetChildIndex(this.outputReportIDComboBox, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.industryMemberNameComboBox, 0);
            this.Controls.SetChildIndex(this.sourceSizeSetMemberNameComboBox, 0);
            this.Controls.SetChildIndex(this.footnoteComboBox, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.destinationSizeSetMemberNameComboBox, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            this.Controls.SetChildIndex(this.cellDisclosureKeyTextBox, 0);
            this.Controls.SetChildIndex(cellDisclosureKeyLabel, 0);
            this.Controls.SetChildIndex(this.outputReportKeyTextBox, 0);
            this.Controls.SetChildIndex(this.industryMemberKeyTextBox, 0);
            this.Controls.SetChildIndex(this.sourceSizeSetMemberKeyTextBox, 0);
            this.Controls.SetChildIndex(this.destinationSizeSetMemberKeyTextBox, 0);
            this.Controls.SetChildIndex(this.footnoteKeyTextBox, 0);
            this.Controls.SetChildIndex(label6, 0);
            ((System.ComponentModel.ISupportInitialize)(this.cellDisclosureBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox outputReportIDComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox industryMemberNameComboBox;
        private System.Windows.Forms.ComboBox sourceSizeSetMemberNameComboBox;
        private System.Windows.Forms.ComboBox footnoteComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox destinationSizeSetMemberNameComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.BindingSource cellDisclosureBindingSource;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.TextBox cellDisclosureKeyTextBox;
        private System.Windows.Forms.TextBox outputReportKeyTextBox;
        private System.Windows.Forms.TextBox industryMemberKeyTextBox;
        private System.Windows.Forms.TextBox sourceSizeSetMemberKeyTextBox;
        private System.Windows.Forms.TextBox destinationSizeSetMemberKeyTextBox;
        private System.Windows.Forms.TextBox footnoteKeyTextBox;
    }
}