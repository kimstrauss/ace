﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using System.Collections;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class CellDisclosureMemberEditForm :  HaiObjectEditForm
    {
        private CellDisclosure _cellDisclosureToEdit;

        public CellDisclosureMemberEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _cellDisclosureToEdit = (CellDisclosure)_parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            btnInsertNull.Visible = false;              // inserting nulls not meaningful

            PrepareTheFormToShow(_parameters);          // the form needs to be prepared before assigning values to controls
            cellDisclosureBindingSource.DataSource = _cellDisclosureToEdit;

            outputReportKeyTextBox.Text = "--";
            industryMemberKeyTextBox.Text = "--";
            sourceSizeSetMemberKeyTextBox.Text = "--";
            destinationSizeSetMemberKeyTextBox.Text = "--";
            footnoteKeyTextBox.Text = "--";


            SetControlsWithInitialValues();

            outputReportIDComboBox.SelectionChangeCommitted += new EventHandler(outputReportIDComboBox_SelectionChangeCommitted);
            industryMemberNameComboBox.SelectionChangeCommitted += new EventHandler(industryMemberNameComboBox_SelectionChangeCommitted);
            sourceSizeSetMemberNameComboBox.SelectionChangeCommitted += new EventHandler(sourceSizeSetMemberNameComboBox_SelectionChangeCommitted);
            destinationSizeSetMemberNameComboBox.SelectionChangeCommitted += new EventHandler(destinationSizeSetMemberNameComboBox_SelectionChangeCommitted);

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        #region Set content of controls

        private void SetControlsWithInitialValues()
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            FillOutputReport(_cellDisclosureToEdit.OutputReportID);
            FillIndustryName(_cellDisclosureToEdit.IndustryMemberName);
            FillSourceSizeSetMember(_cellDisclosureToEdit.SourceSizeSetMemberName);
            FillDestinationSizeSetMember(_cellDisclosureToEdit.DestinationSizeSetMemberName);
            FillFootnote(_cellDisclosureToEdit.Footnote);

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        private void FillOutputReport(string comboBoxText)
        {
            ClearComboBox(outputReportIDComboBox);

            ControlTag controlTag;
            controlTag = (ControlTag)outputReportKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)industryMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.NotInitialized;
            controlTag = (ControlTag)sourceSizeSetMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.NotInitialized;
            controlTag = (ControlTag)destinationSizeSetMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.NotInitialized;
            controlTag = (ControlTag)footnoteKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.NotInitialized;

            DataServiceParameters dsParameters = new DataServiceParameters();

            Cursor = Cursors.WaitCursor;
            DataAccessResult result = AhamDataServiceCellDisclosure.CharacteristicsOutputReportListGet(null, dsParameters);
            Cursor = Cursors.Default;
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<OutputReport>(outputReportIDComboBox, result, ComboBoxListType.DataListWithBlankItem, "OutputReportID", "OutputReportKey", outputReportKeyTextBox, comboBoxText);
            }
            else
            {
                MessageBox.Show("Data not retrieved: " + result.Message, "Data retrieval error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void FillIndustryName(string comboBoxText)
        {
            ClearComboBox(industryMemberNameComboBox);

            ControlTag controlTag;
            controlTag = (ControlTag)outputReportKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)industryMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)sourceSizeSetMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.NotInitialized;
            controlTag = (ControlTag)destinationSizeSetMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.NotInitialized;
            controlTag = (ControlTag)footnoteKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.NotInitialized;

            OutputReport outputReport = outputReportIDComboBox.SelectedItem as OutputReport;

            if (outputReport == null || outputReport.IsNew)     // user has not made a selection
            {
                return;
            }

            int outputReportkey = outputReport.OutputReportKey;

            Cursor = Cursors.WaitCursor;
            DataServiceParameters dsParameters = new DataServiceParameters();
            DataAccessResult result = AhamDataServiceCellDisclosure.IndustryReportMemberForOutputListGet(null, dsParameters, outputReportkey);
            Cursor = Cursors.Default;
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<IndustryReportMemberForOutput>(industryMemberNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "IndustryReportMemberForOutputName", "IndustryReportMemberForOutputKey", industryMemberKeyTextBox, comboBoxText, "IndustryMemberName");
            }
            else
            {
                MessageBox.Show("Data not retrieved: " + result.Message, "Data retrieval error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void FillSourceSizeSetMember(string comboBoxText)
        {
            ClearComboBox(sourceSizeSetMemberNameComboBox);

            ControlTag controlTag;
            controlTag = (ControlTag)outputReportKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)industryMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)sourceSizeSetMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)destinationSizeSetMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.NotInitialized;
            controlTag = (ControlTag)footnoteKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.NotInitialized;

            IndustryReportMemberForOutput industryReportMemberForOutput = industryMemberNameComboBox.SelectedItem as IndustryReportMemberForOutput;

            if (industryReportMemberForOutput == null || industryReportMemberForOutput.IsNew)    // user has not made a selection
            {
                return;
            }

            OutputReport outputReport = outputReportIDComboBox.SelectedItem as OutputReport;

            if (outputReport == null || outputReport.IsNew)
                return;

            int outputReportkey = outputReport.OutputReportKey;
            int industryReportMemberForOutputKey = industryReportMemberForOutput.IndustryReportMemberForOutputKey;

            Cursor = Cursors.WaitCursor;
            DataServiceParameters dsParameters = new DataServiceParameters();
            DataAccessResult result = AhamDataServiceCellDisclosure.SourceSizeSetMemberListGet(null, dsParameters, outputReportkey, industryReportMemberForOutputKey);
            Cursor = Cursors.Default;
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<SizeSetMember>(sourceSizeSetMemberNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "SizeSetMemberName", "SizeSetMemberKey", sourceSizeSetMemberKeyTextBox, comboBoxText, "SourceSizeSetMemberName");
            }
            else
            {
                MessageBox.Show("Data not retrieved: " + result.Message, "Data retrieval error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void FillDestinationSizeSetMember(string comboBoxText)
        {
            ClearComboBox(destinationSizeSetMemberNameComboBox);

            ControlTag controlTag;
            controlTag = (ControlTag)outputReportKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)industryMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)sourceSizeSetMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)destinationSizeSetMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)footnoteKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.NotInitialized;

            SizeSetMember sourceSizeSetMember = sourceSizeSetMemberNameComboBox.SelectedItem as SizeSetMember;

            if (sourceSizeSetMember == null || sourceSizeSetMember.IsNew)  // user has not made a selection
            {
                return;
            }

            OutputReport outputReport = outputReportIDComboBox.SelectedItem as OutputReport;
            if (outputReport == null || outputReport.IsNew)
                return;

            int outputReportKey = outputReport.OutputReportKey;

            IndustryReportMemberForOutput industryReportMemberForOutput = industryMemberNameComboBox.SelectedItem as IndustryReportMemberForOutput;
            if (industryReportMemberForOutput == null || industryReportMemberForOutput.IsNew)
                return;

            int industryMemberKey = industryReportMemberForOutput.IndustryReportMemberForOutputKey;

            int sourceSizeSetMemberKey = sourceSizeSetMember.SizeSetMemberKey;

            Cursor = Cursors.WaitCursor;
            DataServiceParameters dsParameters = new DataServiceParameters();
            DataAccessResult result = AhamDataServiceCellDisclosure.DestinationSizeSetMemberListGet(null, dsParameters, outputReportKey, industryMemberKey, sourceSizeSetMemberKey);
            Cursor = Cursors.Default;
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<SizeSetMember>(destinationSizeSetMemberNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "SizeSetMemberName", "SizeSetMemberKey", destinationSizeSetMemberKeyTextBox, comboBoxText, "DestinationSizeSetMemberName");
            }
            else
            {
                MessageBox.Show("Data not retrieved: " + result.Message, "Data retrieval error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void FillFootnote(string comboBoxText)
        {
            ClearComboBox(footnoteComboBox);

            ControlTag controlTag;
            controlTag = (ControlTag)outputReportKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)industryMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)sourceSizeSetMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)destinationSizeSetMemberKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
            controlTag = (ControlTag)footnoteKeyTextBox.Tag;
            controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;

            SizeSetMember destinationSizeSetMember = destinationSizeSetMemberNameComboBox.SelectedItem as SizeSetMember;

            if (destinationSizeSetMember == null || destinationSizeSetMember.IsNew)
                return;

            DataServiceParameters dsParameters = new DataServiceParameters();
            Cursor = Cursors.WaitCursor;
            DataAccessResult result = AhamDataServiceCellDisclosure.FootnoteListGet(null, dsParameters);
            Cursor = Cursors.Default;
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Footnote>(footnoteComboBox, result, ComboBoxListType.DataListWithBlankItem, "FootnoteText", "FootnoteKey", footnoteKeyTextBox, comboBoxText, "Footnote");
            }
            else
            {
                MessageBox.Show("Data not retrieved: " + result.Message, "Data retrieval error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion

        #region ComboBox event handlers

        void outputReportIDComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            FillIndustryName("");

            _cellDisclosureToEdit.IndustryMemberKey = 0;
            _cellDisclosureToEdit.SourceSizeSetMemberKey = 0;
            _cellDisclosureToEdit.DestinationSizeSetMemberKey = 0;
            _cellDisclosureToEdit.FootnoteKey = 0;

            ControlTag keyControlTag = (ControlTag)industryMemberKeyTextBox.Tag;
            keyControlTag.ControlAccessState = ControlAccessState.NotInitialized;

            ClearSourceSizeSettings();
            ClearDestinationSizeSettings();
            ClearFootnoteSettings();
        }

        void industryMemberNameComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            FillSourceSizeSetMember("");

            _cellDisclosureToEdit.SourceSizeSetMemberKey = 0;
            _cellDisclosureToEdit.DestinationSizeSetMemberKey = 0;
            _cellDisclosureToEdit.FootnoteKey = 0;

            ControlTag keyControlTag = (ControlTag)sourceSizeSetMemberKeyTextBox.Tag;
            keyControlTag.ControlAccessState = ControlAccessState.NotInitialized;

            ClearDestinationSizeSettings();
            ClearFootnoteSettings();
        }

        void sourceSizeSetMemberNameComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            FillDestinationSizeSetMember("");

            _cellDisclosureToEdit.DestinationSizeSetMemberKey = 0;
            _cellDisclosureToEdit.FootnoteKey = 0;

            ControlTag keyControlTag = (ControlTag)destinationSizeSetMemberKeyTextBox.Tag;
            keyControlTag.ControlAccessState = ControlAccessState.NotInitialized;

            ClearFootnoteSettings();
        }

        void destinationSizeSetMemberNameComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            FillFootnote("");

            _cellDisclosureToEdit.FootnoteKey = 0;

            ControlTag keyControlTag = (ControlTag)footnoteKeyTextBox.Tag;
            keyControlTag.ControlAccessState = ControlAccessState.NotInitialized;

        }

        void ClearComboBox(ComboBox anyComboBox)
        {
            anyComboBox.DataSource = null;
        }

        void ClearSourceSizeSettings()
        {
            ClearComboBox(sourceSizeSetMemberNameComboBox);
        }

        void ClearDestinationSizeSettings()
        {
            ClearComboBox(destinationSizeSetMemberNameComboBox);
        }

        void ClearFootnoteSettings()
        {
            ClearComboBox(footnoteComboBox);
        }

        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (outputReportIDComboBox.Text.Trim() == string.Empty)
            {
                abort = true;
                MessageBox.Show("An output report must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if (industryMemberNameComboBox.Text.Trim() == string.Empty)
            {
                abort = true;
                MessageBox.Show("An industry must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if (sourceSizeSetMemberNameComboBox.Text.Trim() == string.Empty)
            {
                abort = true;
                MessageBox.Show("A source size must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if (destinationSizeSetMemberKeyTextBox.Text.Trim() == string.Empty)
            {
                abort = true;
                MessageBox.Show("A destination size must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if (footnoteComboBox.Text.Trim() == string.Empty)
            {
                abort = true;
                MessageBox.Show("A footnote must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if (abort)
                return;

            MarkChangedProperties(_cellDisclosureToEdit);
            _cellDisclosureToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
