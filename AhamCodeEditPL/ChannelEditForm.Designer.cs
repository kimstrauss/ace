﻿namespace AhamMetaDataPL
{
    partial class ChannelEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label channelAbbreviationLabel;
            System.Windows.Forms.Label channelKeyLabel;
            System.Windows.Forms.Label channelNameLabel;
            this.channelAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.channelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.channelKeyTextBox = new System.Windows.Forms.TextBox();
            this.channelNameTextBox = new System.Windows.Forms.TextBox();
            channelAbbreviationLabel = new System.Windows.Forms.Label();
            channelKeyLabel = new System.Windows.Forms.Label();
            channelNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.channelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // channelAbbreviationLabel
            // 
            channelAbbreviationLabel.AutoSize = true;
            channelAbbreviationLabel.Location = new System.Drawing.Point(12, 15);
            channelAbbreviationLabel.Name = "channelAbbreviationLabel";
            channelAbbreviationLabel.Size = new System.Drawing.Size(111, 13);
            channelAbbreviationLabel.TabIndex = 30;
            channelAbbreviationLabel.Text = "Channel Abbreviation:";
            // 
            // channelKeyLabel
            // 
            channelKeyLabel.AutoSize = true;
            channelKeyLabel.Location = new System.Drawing.Point(12, 67);
            channelKeyLabel.Name = "channelKeyLabel";
            channelKeyLabel.Size = new System.Drawing.Size(70, 13);
            channelKeyLabel.TabIndex = 32;
            channelKeyLabel.Text = "Channel Key:";
            // 
            // channelNameLabel
            // 
            channelNameLabel.AutoSize = true;
            channelNameLabel.Location = new System.Drawing.Point(12, 41);
            channelNameLabel.Name = "channelNameLabel";
            channelNameLabel.Size = new System.Drawing.Size(80, 13);
            channelNameLabel.TabIndex = 34;
            channelNameLabel.Text = "Channel Name:";
            // 
            // channelAbbreviationTextBox
            // 
            this.channelAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.channelBindingSource, "ChannelAbbreviation", true));
            this.channelAbbreviationTextBox.Location = new System.Drawing.Point(129, 12);
            this.channelAbbreviationTextBox.Name = "channelAbbreviationTextBox";
            this.channelAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.channelAbbreviationTextBox.TabIndex = 0;
            // 
            // channelBindingSource
            // 
            this.channelBindingSource.DataSource = typeof(AhamMetaDataDAL.Channel);
            // 
            // channelKeyTextBox
            // 
            this.channelKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.channelBindingSource, "ChannelKey", true));
            this.channelKeyTextBox.Location = new System.Drawing.Point(129, 64);
            this.channelKeyTextBox.Name = "channelKeyTextBox";
            this.channelKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.channelKeyTextBox.TabIndex = 2;
            this.channelKeyTextBox.TabStop = false;
            // 
            // channelNameTextBox
            // 
            this.channelNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.channelBindingSource, "ChannelName", true));
            this.channelNameTextBox.Location = new System.Drawing.Point(129, 38);
            this.channelNameTextBox.Name = "channelNameTextBox";
            this.channelNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.channelNameTextBox.TabIndex = 1;
            // 
            // ChannelEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(channelAbbreviationLabel);
            this.Controls.Add(this.channelAbbreviationTextBox);
            this.Controls.Add(channelKeyLabel);
            this.Controls.Add(this.channelKeyTextBox);
            this.Controls.Add(channelNameLabel);
            this.Controls.Add(this.channelNameTextBox);
            this.Name = "ChannelEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Channel";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.channelNameTextBox, 0);
            this.Controls.SetChildIndex(channelNameLabel, 0);
            this.Controls.SetChildIndex(this.channelKeyTextBox, 0);
            this.Controls.SetChildIndex(channelKeyLabel, 0);
            this.Controls.SetChildIndex(this.channelAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(channelAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.channelBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource channelBindingSource;
        private System.Windows.Forms.TextBox channelAbbreviationTextBox;
        private System.Windows.Forms.TextBox channelKeyTextBox;
        private System.Windows.Forms.TextBox channelNameTextBox;
    }
}