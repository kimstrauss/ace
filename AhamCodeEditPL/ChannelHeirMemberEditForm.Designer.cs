﻿namespace AhamMetaDataPL
{
    partial class ChannelHeirMemberEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label dimensionKeyLabel;
            System.Windows.Forms.Label productDimensionKeyLabel;
            System.Windows.Forms.Label productKeyLabel;
            System.Windows.Forms.Label productNameLabel;
            System.Windows.Forms.Label unitLabelLabel;
            this.ChannelSetMemberKeyTextBox = new System.Windows.Forms.TextBox();
            this.channelHeirarchyMemberBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.channelHeirarchySetMemberKeyTextBox = new System.Windows.Forms.TextBox();
            this.childChannelSetMemberKeyTextBox = new System.Windows.Forms.TextBox();
            this.ChannelNameComboBox = new System.Windows.Forms.ComboBox();
            this.childChannelNameTextBox = new System.Windows.Forms.TextBox();
            dimensionKeyLabel = new System.Windows.Forms.Label();
            productDimensionKeyLabel = new System.Windows.Forms.Label();
            productKeyLabel = new System.Windows.Forms.Label();
            productNameLabel = new System.Windows.Forms.Label();
            unitLabelLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.channelHeirarchyMemberBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(394, 132);
            this.btnOK.TabIndex = 8;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(480, 132);
            this.btnCancel.TabIndex = 9;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 132);
            // 
            // dimensionKeyLabel
            // 
            dimensionKeyLabel.AutoSize = true;
            dimensionKeyLabel.Location = new System.Drawing.Point(481, 43);
            dimensionKeyLabel.Name = "dimensionKeyLabel";
            dimensionKeyLabel.Size = new System.Drawing.Size(28, 13);
            dimensionKeyLabel.TabIndex = 34;
            dimensionKeyLabel.Text = "Key:";
            // 
            // productDimensionKeyLabel
            // 
            productDimensionKeyLabel.AutoSize = true;
            productDimensionKeyLabel.Location = new System.Drawing.Point(313, 82);
            productDimensionKeyLabel.Name = "productDimensionKeyLabel";
            productDimensionKeyLabel.Size = new System.Drawing.Size(156, 13);
            productDimensionKeyLabel.TabIndex = 40;
            productDimensionKeyLabel.Text = "Channel Heirarchy Member Key";
            // 
            // productKeyLabel
            // 
            productKeyLabel.AutoSize = true;
            productKeyLabel.Location = new System.Drawing.Point(482, 17);
            productKeyLabel.Name = "productKeyLabel";
            productKeyLabel.Size = new System.Drawing.Size(28, 13);
            productKeyLabel.TabIndex = 42;
            productKeyLabel.Text = "Key:";
            // 
            // productNameLabel
            // 
            productNameLabel.AutoSize = true;
            productNameLabel.Location = new System.Drawing.Point(12, 42);
            productNameLabel.Name = "productNameLabel";
            productNameLabel.Size = new System.Drawing.Size(72, 13);
            productNameLabel.TabIndex = 44;
            productNameLabel.Text = "Child Channel";
            // 
            // unitLabelLabel
            // 
            unitLabelLabel.AutoSize = true;
            unitLabelLabel.Location = new System.Drawing.Point(12, 13);
            unitLabelLabel.Name = "unitLabelLabel";
            unitLabelLabel.Size = new System.Drawing.Size(80, 13);
            unitLabelLabel.TabIndex = 46;
            unitLabelLabel.Text = "Parent Channel";
            // 
            // ChannelSetMemberKeyTextBox
            // 
            this.ChannelSetMemberKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.channelHeirarchyMemberBindingSource, "ChannelSetMemberKey", true));
            this.ChannelSetMemberKeyTextBox.Location = new System.Drawing.Point(512, 14);
            this.ChannelSetMemberKeyTextBox.Name = "ChannelSetMemberKeyTextBox";
            this.ChannelSetMemberKeyTextBox.ReadOnly = true;
            this.ChannelSetMemberKeyTextBox.Size = new System.Drawing.Size(43, 20);
            this.ChannelSetMemberKeyTextBox.TabIndex = 35;
            this.ChannelSetMemberKeyTextBox.TabStop = false;
            // 
            // channelHeirarchyMemberBindingSource
            // 
            this.channelHeirarchyMemberBindingSource.DataSource = typeof(AhamMetaDataDAL.ChannelHeirarchySetMember);
            this.channelHeirarchyMemberBindingSource.CurrentChanged += new System.EventHandler(this.channelHeirarchyMemberBindingSource_CurrentChanged);
            // 
            // channelHeirarchySetMemberKeyTextBox
            // 
            this.channelHeirarchySetMemberKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.channelHeirarchyMemberBindingSource, "ChannelHeirarchySetMemberKey", true));
            this.channelHeirarchySetMemberKeyTextBox.Location = new System.Drawing.Point(510, 75);
            this.channelHeirarchySetMemberKeyTextBox.Name = "channelHeirarchySetMemberKeyTextBox";
            this.channelHeirarchySetMemberKeyTextBox.ReadOnly = true;
            this.channelHeirarchySetMemberKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.channelHeirarchySetMemberKeyTextBox.TabIndex = 41;
            this.channelHeirarchySetMemberKeyTextBox.TabStop = false;
            // 
            // childChannelSetMemberKeyTextBox
            // 
            this.childChannelSetMemberKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.channelHeirarchyMemberBindingSource, "ChildChannelSetMemberKey", true));
            this.childChannelSetMemberKeyTextBox.Location = new System.Drawing.Point(512, 43);
            this.childChannelSetMemberKeyTextBox.Name = "childChannelSetMemberKeyTextBox";
            this.childChannelSetMemberKeyTextBox.ReadOnly = true;
            this.childChannelSetMemberKeyTextBox.Size = new System.Drawing.Size(43, 20);
            this.childChannelSetMemberKeyTextBox.TabIndex = 43;
            this.childChannelSetMemberKeyTextBox.TabStop = false;
            // 
            // ChannelNameComboBox
            // 
            this.ChannelNameComboBox.FormattingEnabled = true;
            this.ChannelNameComboBox.Location = new System.Drawing.Point(135, 13);
            this.ChannelNameComboBox.Name = "ChannelNameComboBox";
            this.ChannelNameComboBox.Size = new System.Drawing.Size(332, 21);
            this.ChannelNameComboBox.TabIndex = 0;
            // 
            // childChannelNameTextBox
            // 
            this.childChannelNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.channelHeirarchyMemberBindingSource, "ChildChannelName", true));
            this.childChannelNameTextBox.Location = new System.Drawing.Point(135, 42);
            this.childChannelNameTextBox.Name = "childChannelNameTextBox";
            this.childChannelNameTextBox.ReadOnly = true;
            this.childChannelNameTextBox.Size = new System.Drawing.Size(330, 20);
            this.childChannelNameTextBox.TabIndex = 2;
            // 
            // ChannelHeirMemberEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 167);
            this.Controls.Add(dimensionKeyLabel);
            this.Controls.Add(this.ChannelSetMemberKeyTextBox);
            this.Controls.Add(productDimensionKeyLabel);
            this.Controls.Add(this.channelHeirarchySetMemberKeyTextBox);
            this.Controls.Add(productKeyLabel);
            this.Controls.Add(this.childChannelSetMemberKeyTextBox);
            this.Controls.Add(productNameLabel);
            this.Controls.Add(this.ChannelNameComboBox);
            this.Controls.Add(unitLabelLabel);
            this.Controls.Add(this.childChannelNameTextBox);
            this.Name = "ChannelHeirMemberEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Channel Heirarchy Member";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.childChannelNameTextBox, 0);
            this.Controls.SetChildIndex(unitLabelLabel, 0);
            this.Controls.SetChildIndex(this.ChannelNameComboBox, 0);
            this.Controls.SetChildIndex(productNameLabel, 0);
            this.Controls.SetChildIndex(this.childChannelSetMemberKeyTextBox, 0);
            this.Controls.SetChildIndex(productKeyLabel, 0);
            this.Controls.SetChildIndex(this.channelHeirarchySetMemberKeyTextBox, 0);
            this.Controls.SetChildIndex(productDimensionKeyLabel, 0);
            this.Controls.SetChildIndex(this.ChannelSetMemberKeyTextBox, 0);
            this.Controls.SetChildIndex(dimensionKeyLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.channelHeirarchyMemberBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource channelHeirarchyMemberBindingSource;
        private System.Windows.Forms.TextBox ChannelSetMemberKeyTextBox;
        private System.Windows.Forms.TextBox channelHeirarchySetMemberKeyTextBox;
        private System.Windows.Forms.TextBox childChannelSetMemberKeyTextBox;
        private System.Windows.Forms.ComboBox ChannelNameComboBox;
        private System.Windows.Forms.TextBox childChannelNameTextBox;
    }
}