﻿using System;
using System.Collections;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;
using System.Data.Linq;

using System.Configuration;
using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ChannelHeirMemberEditForm : HaiObjectEditForm
    {
        private ChannelHeirarchySetMember _channelMembertoEdit;

        public ChannelHeirMemberEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _channelMembertoEdit = (ChannelHeirarchySetMember)_parameters.EditObject;

            this.Load+=new EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);          // the form needs to be prepared before assigning values to controls
            channelHeirarchyMemberBindingSource.DataSource = _channelMembertoEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = ds.GetDataList(HaiBusinessObjectType.ChannelSetMember);
            if (result.Success)
            {
                HaiBindingList<ChannelSetMember> parentChannelSetList = (HaiBindingList<ChannelSetMember>)result.DataList;
                List<ChannelSetMember> delList = new List<ChannelSetMember>();
                foreach (ChannelSetMember item in parentChannelSetList)
                    if (item.ChannelSetKey != _channelMembertoEdit.ParentChannelSetKey)
                        delList.Add(item);
                foreach (ChannelSetMember delItem in delList)
                    result.DataList.Remove(delItem);

                parentChannelSetList = (HaiBindingList<ChannelSetMember>)result.DataList;

                if (parentChannelSetList.Count == 0)
                {
                    MessageBox.Show("Cannot add/edit heirarchies because no parent channels exist.", "Data integrity error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                    return;
                }

                SetupAnyComboBoxForDropDownList<ChannelSetMember>(ChannelNameComboBox, result, ComboBoxListType.DataListOnly, "ChannelName", "ChannelSetMemberKey", ChannelSetMemberKeyTextBox, _channelMembertoEdit.ChannelName.Trim());
                ChannelNameComboBox.Enabled = true;
            }
            else
            {
                MessageBox.Show("Cannot add/edit heirarchies because channel fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }


        void btnOK_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            bool abort = false;


            if (ChannelNameComboBox.Text.Trim() == string.Empty && ChannelNameComboBox.Items.Count>0)
            {
                MessageBox.Show("A parent channel must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }


            if (abort)
                return;

            // set the flags in the property controllers
            MarkChangedProperties(_channelMembertoEdit);
            _channelMembertoEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void channelHeirarchyMemberBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }
    }
}
