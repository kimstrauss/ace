﻿namespace AhamMetaDataPL
{
    partial class ChannelHeirSetEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.childSDKeyLabel = new System.Windows.Forms.Label();
            this.childChannelSetLabel = new System.Windows.Forms.Label();
            this.channelHeirarchySetKeyLabel = new System.Windows.Forms.Label();
            this.channelHeirarchySetNameLabel = new System.Windows.Forms.Label();
            this.parentSDKeyLabel = new System.Windows.Forms.Label();
            this.ParentChannelSetLabel = new System.Windows.Forms.Label();
            this.channelHeirarchySetCodeLabel = new System.Windows.Forms.Label();
            this.ChannelSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.ChannelHeirarchySetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.channelHeirarchySetKeyTextBox = new System.Windows.Forms.TextBox();
            this.channelHeirarchySetNameTextBox = new System.Windows.Forms.TextBox();
            this.ParentChannelSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.channelHeirarchySetCodeTextBox = new System.Windows.Forms.TextBox();
            this.ChannelSetNameComboBox = new System.Windows.Forms.ComboBox();
            this.ParentChannelSetNameComboBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.ChannelHeirarchySetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(452, 181);
            this.btnOK.TabIndex = 4;
             // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(533, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // childSDKeyLabel
            // 
            this.childSDKeyLabel.AutoSize = true;
            this.childSDKeyLabel.Location = new System.Drawing.Point(544, 68);
            this.childSDKeyLabel.Name = "childSDKeyLabel";
            this.childSDKeyLabel.Size = new System.Drawing.Size(28, 13);
            this.childSDKeyLabel.TabIndex = 30;
            this.childSDKeyLabel.Text = "Key:";
            // 
            // childChannelSetLabel
            // 
            this.childChannelSetLabel.AutoSize = true;
            this.childChannelSetLabel.Location = new System.Drawing.Point(6, 67);
            this.childChannelSetLabel.Name = "childChannelSetLabel";
            this.childChannelSetLabel.Size = new System.Drawing.Size(91, 13);
            this.childChannelSetLabel.TabIndex = 32;
            this.childChannelSetLabel.Text = "Child Channel Set";
            // 
            // channelHeirarchySetKeyLabel
            // 
            this.channelHeirarchySetKeyLabel.AutoSize = true;
            this.channelHeirarchySetKeyLabel.Location = new System.Drawing.Point(6, 125);
            this.channelHeirarchySetKeyLabel.Name = "channelHeirarchySetKeyLabel";
            this.channelHeirarchySetKeyLabel.Size = new System.Drawing.Size(134, 13);
            this.channelHeirarchySetKeyLabel.TabIndex = 34;
            this.channelHeirarchySetKeyLabel.Text = "Channel Heirarchy Set Key";
            // 
            // channelHeirarchySetNameLabel
            // 
            this.channelHeirarchySetNameLabel.AutoSize = true;
            this.channelHeirarchySetNameLabel.Location = new System.Drawing.Point(6, 15);
            this.channelHeirarchySetNameLabel.Name = "channelHeirarchySetNameLabel";
            this.channelHeirarchySetNameLabel.Size = new System.Drawing.Size(147, 13);
            this.channelHeirarchySetNameLabel.TabIndex = 36;
            this.channelHeirarchySetNameLabel.Text = "Channel Heirarchy Set Name:";
            // 
            // parentSDKeyLabel
            // 
            this.parentSDKeyLabel.AutoSize = true;
            this.parentSDKeyLabel.Location = new System.Drawing.Point(544, 99);
            this.parentSDKeyLabel.Name = "parentSDKeyLabel";
            this.parentSDKeyLabel.Size = new System.Drawing.Size(28, 13);
            this.parentSDKeyLabel.TabIndex = 38;
            this.parentSDKeyLabel.Text = "Key:";
            // 
            // ParentChannelSetLabel
            // 
            this.ParentChannelSetLabel.AutoSize = true;
            this.ParentChannelSetLabel.Location = new System.Drawing.Point(6, 96);
            this.ParentChannelSetLabel.Name = "ParentChannelSetLabel";
            this.ParentChannelSetLabel.Size = new System.Drawing.Size(99, 13);
            this.ParentChannelSetLabel.TabIndex = 40;
            this.ParentChannelSetLabel.Text = "Parent Channel Set";
            // 
            // channelHeirarchySetCodeLabel
            // 
            this.channelHeirarchySetCodeLabel.AutoSize = true;
            this.channelHeirarchySetCodeLabel.Location = new System.Drawing.Point(6, 41);
            this.channelHeirarchySetCodeLabel.Name = "channelHeirarchySetCodeLabel";
            this.channelHeirarchySetCodeLabel.Size = new System.Drawing.Size(144, 13);
            this.channelHeirarchySetCodeLabel.TabIndex = 41;
            this.channelHeirarchySetCodeLabel.Text = "Channel Heirarchy Set Code:";
            // 
            // ChannelSetKeyTextBox
            // 
            this.ChannelSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ChannelHeirarchySetBindingSource, "ChannelSetKey", true));
            this.ChannelSetKeyTextBox.Location = new System.Drawing.Point(578, 68);
            this.ChannelSetKeyTextBox.Name = "ChannelSetKeyTextBox";
            this.ChannelSetKeyTextBox.Size = new System.Drawing.Size(37, 20);
            this.ChannelSetKeyTextBox.TabIndex = 31;
            this.ChannelSetKeyTextBox.TabStop = false;
            // 
            // ChannelHeirarchySetBindingSource
            // 
            this.ChannelHeirarchySetBindingSource.DataSource = typeof(AhamMetaDataDAL.ChannelHeirarchySet);
            // 
            // channelHeirarchySetKeyTextBox
            // 
            this.channelHeirarchySetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ChannelHeirarchySetBindingSource, "ChannelHeirarchySetKey", true));
            this.channelHeirarchySetKeyTextBox.Location = new System.Drawing.Point(159, 122);
            this.channelHeirarchySetKeyTextBox.Name = "channelHeirarchySetKeyTextBox";
            this.channelHeirarchySetKeyTextBox.Size = new System.Drawing.Size(55, 20);
            this.channelHeirarchySetKeyTextBox.TabIndex = 35;
            this.channelHeirarchySetKeyTextBox.TabStop = false;
            // 
            // channelHeirarchySetNameTextBox
            // 
            this.channelHeirarchySetNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ChannelHeirarchySetBindingSource, "ChannelHeirarchySetName", true));
            this.channelHeirarchySetNameTextBox.Location = new System.Drawing.Point(159, 12);
            this.channelHeirarchySetNameTextBox.Name = "channelHeirarchySetNameTextBox";
            this.channelHeirarchySetNameTextBox.Size = new System.Drawing.Size(456, 20);
            this.channelHeirarchySetNameTextBox.TabIndex = 0;
            // 
            // ParentChannelSetKeyTextBox
            // 
            this.ParentChannelSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ChannelHeirarchySetBindingSource, "ParentChannelSetKey", true));
            this.ParentChannelSetKeyTextBox.Location = new System.Drawing.Point(578, 96);
            this.ParentChannelSetKeyTextBox.Name = "ParentChannelSetKeyTextBox";
            this.ParentChannelSetKeyTextBox.Size = new System.Drawing.Size(37, 20);
            this.ParentChannelSetKeyTextBox.TabIndex = 39;
            this.ParentChannelSetKeyTextBox.TabStop = false;
            // 
            // channelHeirarchySetCodeTextBox
            // 
            this.channelHeirarchySetCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ChannelHeirarchySetBindingSource, "ChannelHeirarchySetAbbreviation", true));
            this.channelHeirarchySetCodeTextBox.Location = new System.Drawing.Point(159, 38);
            this.channelHeirarchySetCodeTextBox.Name = "channelHeirarchySetCodeTextBox";
            this.channelHeirarchySetCodeTextBox.Size = new System.Drawing.Size(456, 20);
            this.channelHeirarchySetCodeTextBox.TabIndex = 1;
            // 
            // ChannelSetNameComboBox
            // 
            this.ChannelSetNameComboBox.FormattingEnabled = true;
            this.ChannelSetNameComboBox.Location = new System.Drawing.Point(159, 67);
            this.ChannelSetNameComboBox.Name = "ChannelSetNameComboBox";
            this.ChannelSetNameComboBox.Size = new System.Drawing.Size(379, 21);
            this.ChannelSetNameComboBox.TabIndex = 2;
            // 
            // ParentChannelSetNameComboBox
            // 
            this.ParentChannelSetNameComboBox.FormattingEnabled = true;
            this.ParentChannelSetNameComboBox.Location = new System.Drawing.Point(159, 95);
            this.ParentChannelSetNameComboBox.Name = "ParentChannelSetNameComboBox";
            this.ParentChannelSetNameComboBox.Size = new System.Drawing.Size(379, 21);
            this.ParentChannelSetNameComboBox.TabIndex = 3;
            // 
            // ChannelHeirSetEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 216);
            this.Controls.Add(this.ParentChannelSetNameComboBox);
            this.Controls.Add(this.ChannelSetNameComboBox);
            this.Controls.Add(this.channelHeirarchySetCodeTextBox);
            this.Controls.Add(this.channelHeirarchySetCodeLabel);
            this.Controls.Add(this.childSDKeyLabel);
            this.Controls.Add(this.ChannelSetKeyTextBox);
            this.Controls.Add(this.childChannelSetLabel);
            this.Controls.Add(this.channelHeirarchySetKeyLabel);
            this.Controls.Add(this.channelHeirarchySetKeyTextBox);
            this.Controls.Add(this.channelHeirarchySetNameLabel);
            this.Controls.Add(this.channelHeirarchySetNameTextBox);
            this.Controls.Add(this.parentSDKeyLabel);
            this.Controls.Add(this.ParentChannelSetKeyTextBox);
            this.Controls.Add(this.ParentChannelSetLabel);
            this.Name = "ChannelHeirSetEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Channel Heirarchy Set";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.ParentChannelSetLabel, 0);
            this.Controls.SetChildIndex(this.ParentChannelSetKeyTextBox, 0);
            this.Controls.SetChildIndex(this.parentSDKeyLabel, 0);
            this.Controls.SetChildIndex(this.channelHeirarchySetNameTextBox, 0);
            this.Controls.SetChildIndex(this.channelHeirarchySetNameLabel, 0);
            this.Controls.SetChildIndex(this.channelHeirarchySetKeyTextBox, 0);
            this.Controls.SetChildIndex(this.channelHeirarchySetKeyLabel, 0);
            this.Controls.SetChildIndex(this.childChannelSetLabel, 0);
            this.Controls.SetChildIndex(this.ChannelSetKeyTextBox, 0);
            this.Controls.SetChildIndex(this.childSDKeyLabel, 0);
            this.Controls.SetChildIndex(this.channelHeirarchySetCodeLabel, 0);
            this.Controls.SetChildIndex(this.channelHeirarchySetCodeTextBox, 0);
            this.Controls.SetChildIndex(this.ChannelSetNameComboBox, 0);
            this.Controls.SetChildIndex(this.ParentChannelSetNameComboBox, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ChannelHeirarchySetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource ChannelHeirarchySetBindingSource;
        private System.Windows.Forms.TextBox ChannelSetKeyTextBox;
        private System.Windows.Forms.TextBox channelHeirarchySetKeyTextBox;
        private System.Windows.Forms.TextBox channelHeirarchySetNameTextBox;
        private System.Windows.Forms.TextBox ParentChannelSetKeyTextBox;
        private System.Windows.Forms.TextBox channelHeirarchySetCodeTextBox;
        private System.Windows.Forms.Label childSDKeyLabel;
        private System.Windows.Forms.Label childChannelSetLabel;
        private System.Windows.Forms.Label channelHeirarchySetKeyLabel;
        private System.Windows.Forms.Label channelHeirarchySetNameLabel;
        private System.Windows.Forms.Label parentSDKeyLabel;
        private System.Windows.Forms.Label ParentChannelSetLabel;
        private System.Windows.Forms.Label channelHeirarchySetCodeLabel;
        private System.Windows.Forms.ComboBox ChannelSetNameComboBox;
        private System.Windows.Forms.ComboBox ParentChannelSetNameComboBox;
    }
}