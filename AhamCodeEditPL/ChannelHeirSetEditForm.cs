﻿using System;
using System.Collections;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;
using System.Data.Linq;

using System.Configuration;
using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ChannelHeirSetEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private ChannelHeirarchySet _channelHeirarchySetToEdit;

        public ChannelHeirSetEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _channelHeirarchySetToEdit = (ChannelHeirarchySet)parameters.EditObject;

            this.Load += new EventHandler(ChannelHeirarchySetEditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        void ChannelHeirarchySetEditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);
            ChannelHeirarchySetBindingSource.DataSource = _channelHeirarchySetToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = ds.GetDataList(HaiBusinessObjectType.ChannelSet);
            if (result.Success)
            {

                SetupAnyComboBoxForDropDownList<ChannelSet>(ChannelSetNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ChannelSetName", "ChannelSetKey", ChannelSetKeyTextBox, _channelHeirarchySetToEdit.ChannelSetName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit channel heirarchy sets because child channel set fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObject.HaiBusinessObjectType.ChannelSet);
            if (result.Success)
            {
                DataAccessResult memberResult = ds.GetDataList(HaiBusinessObjectType.ChannelSetMember);
                List<int> delKeys = new List<int>();
                foreach (ChannelSetMember theMember in memberResult.DataList)
                {
                    if (theMember.BeginDate.Year > 2000 || theMember.EndDate.Year < 2040)
                        delKeys.Add(theMember.ChannelSetKey);
                }
                if (delKeys.Count > 0)
                {
                    List<ChannelSet> delItems = new List<ChannelSet>();
                    foreach (ChannelSet theChannel in result.DataList)
                        foreach (int theKey in delKeys)
                            if (theChannel.ChannelSetKey == theKey)
                            {
                                delItems.Add(theChannel);
                                break;
                            }
                    foreach (ChannelSet theDelChannel in delItems)
                        result.DataList.Remove(theDelChannel);
                }

                SetupAnyComboBoxForDropDownList<ChannelSet>(ParentChannelSetNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ChannelSetName", "ChannelSetKey", ParentChannelSetKeyTextBox, _channelHeirarchySetToEdit.ParentChannelSetName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit channel heirarchy sets because parent channel set fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (channelHeirarchySetNameTextBox.Text.Trim() == string.Empty)
            {
                abort = true;
                MessageBox.Show("A channel heirarchy set name must be entered.", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if (ChannelSetNameComboBox.Text.Trim() == string.Empty && ChannelSetNameComboBox.Items.Count > 0)
            {
                abort = true;
                MessageBox.Show("A child Channel set must be selected.", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if (ParentChannelSetNameComboBox.Text.Trim() == string.Empty && ParentChannelSetNameComboBox.Items.Count > 0)
            {
                abort = true;
                MessageBox.Show("A parent channel set must be selected.", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if (abort)
                return;

            MarkChangedProperties(_channelHeirarchySetToEdit);
            _channelHeirarchySetToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }

 
      }
}
