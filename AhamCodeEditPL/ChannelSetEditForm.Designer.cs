﻿namespace AhamMetaDataPL
{
    partial class ChannelSetEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label channelSetAbbreviationLabel;
            System.Windows.Forms.Label channelSetKeyLabel;
            System.Windows.Forms.Label channelSetNameLabel;
            this.channelSetAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.channelSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.channelSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.channelSetNameTextBox = new System.Windows.Forms.TextBox();
            channelSetAbbreviationLabel = new System.Windows.Forms.Label();
            channelSetKeyLabel = new System.Windows.Forms.Label();
            channelSetNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.channelSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(387, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(468, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // channelSetAbbreviationLabel
            // 
            channelSetAbbreviationLabel.AutoSize = true;
            channelSetAbbreviationLabel.Location = new System.Drawing.Point(12, 17);
            channelSetAbbreviationLabel.Name = "channelSetAbbreviationLabel";
            channelSetAbbreviationLabel.Size = new System.Drawing.Size(130, 13);
            channelSetAbbreviationLabel.TabIndex = 30;
            channelSetAbbreviationLabel.Text = "Channel Set Abbreviation:";
            // 
            // channelSetKeyLabel
            // 
            channelSetKeyLabel.AutoSize = true;
            channelSetKeyLabel.Location = new System.Drawing.Point(12, 69);
            channelSetKeyLabel.Name = "channelSetKeyLabel";
            channelSetKeyLabel.Size = new System.Drawing.Size(89, 13);
            channelSetKeyLabel.TabIndex = 32;
            channelSetKeyLabel.Text = "Channel Set Key:";
            // 
            // channelSetNameLabel
            // 
            channelSetNameLabel.AutoSize = true;
            channelSetNameLabel.Location = new System.Drawing.Point(12, 43);
            channelSetNameLabel.Name = "channelSetNameLabel";
            channelSetNameLabel.Size = new System.Drawing.Size(99, 13);
            channelSetNameLabel.TabIndex = 34;
            channelSetNameLabel.Text = "Channel Set Name:";
            // 
            // channelSetAbbreviationTextBox
            // 
            this.channelSetAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.channelSetBindingSource, "ChannelSetAbbreviation", true));
            this.channelSetAbbreviationTextBox.Location = new System.Drawing.Point(148, 14);
            this.channelSetAbbreviationTextBox.Name = "channelSetAbbreviationTextBox";
            this.channelSetAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.channelSetAbbreviationTextBox.TabIndex = 0;
            // 
            // channelSetBindingSource
            // 
            this.channelSetBindingSource.DataSource = typeof(AhamMetaDataDAL.ChannelSet);
            // 
            // channelSetKeyTextBox
            // 
            this.channelSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.channelSetBindingSource, "ChannelSetKey", true));
            this.channelSetKeyTextBox.Location = new System.Drawing.Point(148, 66);
            this.channelSetKeyTextBox.Name = "channelSetKeyTextBox";
            this.channelSetKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.channelSetKeyTextBox.TabIndex = 2;
            this.channelSetKeyTextBox.TabStop = false;
            // 
            // channelSetNameTextBox
            // 
            this.channelSetNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.channelSetBindingSource, "ChannelSetName", true));
            this.channelSetNameTextBox.Location = new System.Drawing.Point(148, 40);
            this.channelSetNameTextBox.Name = "channelSetNameTextBox";
            this.channelSetNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.channelSetNameTextBox.TabIndex = 1;
            // 
            // ChannelSetEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 216);
            this.Controls.Add(channelSetAbbreviationLabel);
            this.Controls.Add(this.channelSetAbbreviationTextBox);
            this.Controls.Add(channelSetKeyLabel);
            this.Controls.Add(this.channelSetKeyTextBox);
            this.Controls.Add(channelSetNameLabel);
            this.Controls.Add(this.channelSetNameTextBox);
            this.Name = "ChannelSetEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ChannelSetEditForm";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.channelSetNameTextBox, 0);
            this.Controls.SetChildIndex(channelSetNameLabel, 0);
            this.Controls.SetChildIndex(this.channelSetKeyTextBox, 0);
            this.Controls.SetChildIndex(channelSetKeyLabel, 0);
            this.Controls.SetChildIndex(this.channelSetAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(channelSetAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.channelSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource channelSetBindingSource;
        private System.Windows.Forms.TextBox channelSetAbbreviationTextBox;
        private System.Windows.Forms.TextBox channelSetKeyTextBox;
        private System.Windows.Forms.TextBox channelSetNameTextBox;
    }
}