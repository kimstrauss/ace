﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class ChannelSetEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private ChannelSet _channelSetToEdit;

        public ChannelSetEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _channelSetToEdit = (ChannelSet)parameters.EditObject;

            this.Load += new System.EventHandler(SetEditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void SetEditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            channelSetBindingSource.DataSource = _channelSetToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (channelSetNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (channelSetAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_channelSetToEdit);
            _channelSetToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }

    }
}
