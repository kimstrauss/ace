﻿namespace AhamMetaDataPL
{
    partial class ContactRolesEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label associationKeyLabel;
            System.Windows.Forms.Label associationNameLabel;
            System.Windows.Forms.Label catalogKeyLabel;
            System.Windows.Forms.Label catalogNameLabel;
            System.Windows.Forms.Label contactRolesKeyLabel;
            System.Windows.Forms.Label manufacturerKeyLabel;
            System.Windows.Forms.Label manufacturerNameLabel;
            System.Windows.Forms.Label productKeyLabel;
            System.Windows.Forms.Label productNameLabel;
            System.Windows.Forms.Label securityLevelLabel;
            System.Windows.Forms.Label userCodeLabel;
            this.associationKeyTextBox = new System.Windows.Forms.TextBox();
            this.contactRolesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.associationNameComboBox = new System.Windows.Forms.ComboBox();
            this.billableCheckBox = new System.Windows.Forms.CheckBox();
            this.catalogKeyTextBox = new System.Windows.Forms.TextBox();
            this.catalogNameComboBox = new System.Windows.Forms.ComboBox();
            this.contactRolesKeyTextBox = new System.Windows.Forms.TextBox();
            this.editCodesCheckBox = new System.Windows.Forms.CheckBox();
            this.editCompanyDataCheckBox = new System.Windows.Forms.CheckBox();
            this.editModelsCheckBox = new System.Windows.Forms.CheckBox();
            this.manufacturerKeyTextBox = new System.Windows.Forms.TextBox();
            this.manufacturerNameComboBox = new System.Windows.Forms.ComboBox();
            this.productKeyTextBox = new System.Windows.Forms.TextBox();
            this.productNameComboBox = new System.Windows.Forms.ComboBox();
            this.responsibleCheckBox = new System.Windows.Forms.CheckBox();
            this.securityLevelTextBox = new System.Windows.Forms.TextBox();
            this.seeAllCheckBox = new System.Windows.Forms.CheckBox();
            this.seeCompanyDataCheckBox = new System.Windows.Forms.CheckBox();
            this.seeIndustryDataCheckBox = new System.Windows.Forms.CheckBox();
            this.seeModelsCheckBox = new System.Windows.Forms.CheckBox();
            this.seePublicCheckBox = new System.Windows.Forms.CheckBox();
            this.seeStatusDetailsCheckBox = new System.Windows.Forms.CheckBox();
            this.submitDataCheckBox = new System.Windows.Forms.CheckBox();
            this.userCodeTextBox = new System.Windows.Forms.TextBox();
            associationKeyLabel = new System.Windows.Forms.Label();
            associationNameLabel = new System.Windows.Forms.Label();
            catalogKeyLabel = new System.Windows.Forms.Label();
            catalogNameLabel = new System.Windows.Forms.Label();
            contactRolesKeyLabel = new System.Windows.Forms.Label();
            manufacturerKeyLabel = new System.Windows.Forms.Label();
            manufacturerNameLabel = new System.Windows.Forms.Label();
            productKeyLabel = new System.Windows.Forms.Label();
            productNameLabel = new System.Windows.Forms.Label();
            securityLevelLabel = new System.Windows.Forms.Label();
            userCodeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.contactRolesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(371, 480);
            this.btnOK.TabIndex = 18;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(452, 480);
            this.btnCancel.TabIndex = 19;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 480);
            // 
            // associationKeyLabel
            // 
            associationKeyLabel.AutoSize = true;
            associationKeyLabel.Location = new System.Drawing.Point(456, 27);
            associationKeyLabel.Name = "associationKeyLabel";
            associationKeyLabel.Size = new System.Drawing.Size(28, 13);
            associationKeyLabel.TabIndex = 30;
            associationKeyLabel.Text = "Key:";
            // 
            // associationNameLabel
            // 
            associationNameLabel.AutoSize = true;
            associationNameLabel.Location = new System.Drawing.Point(26, 27);
            associationNameLabel.Name = "associationNameLabel";
            associationNameLabel.Size = new System.Drawing.Size(95, 13);
            associationNameLabel.TabIndex = 32;
            associationNameLabel.Text = "Association Name:";
            // 
            // catalogKeyLabel
            // 
            catalogKeyLabel.AutoSize = true;
            catalogKeyLabel.Location = new System.Drawing.Point(456, 136);
            catalogKeyLabel.Name = "catalogKeyLabel";
            catalogKeyLabel.Size = new System.Drawing.Size(28, 13);
            catalogKeyLabel.TabIndex = 36;
            catalogKeyLabel.Text = "Key:";
            // 
            // catalogNameLabel
            // 
            catalogNameLabel.AutoSize = true;
            catalogNameLabel.Location = new System.Drawing.Point(26, 136);
            catalogNameLabel.Name = "catalogNameLabel";
            catalogNameLabel.Size = new System.Drawing.Size(77, 13);
            catalogNameLabel.TabIndex = 38;
            catalogNameLabel.Text = "Catalog Name:";
            // 
            // contactRolesKeyLabel
            // 
            contactRolesKeyLabel.AutoSize = true;
            contactRolesKeyLabel.Location = new System.Drawing.Point(377, 437);
            contactRolesKeyLabel.Name = "contactRolesKeyLabel";
            contactRolesKeyLabel.Size = new System.Drawing.Size(98, 13);
            contactRolesKeyLabel.TabIndex = 40;
            contactRolesKeyLabel.Text = "Contact Roles Key:";
            // 
            // manufacturerKeyLabel
            // 
            manufacturerKeyLabel.AutoSize = true;
            manufacturerKeyLabel.Location = new System.Drawing.Point(456, 81);
            manufacturerKeyLabel.Name = "manufacturerKeyLabel";
            manufacturerKeyLabel.Size = new System.Drawing.Size(28, 13);
            manufacturerKeyLabel.TabIndex = 48;
            manufacturerKeyLabel.Text = "Key:";
            // 
            // manufacturerNameLabel
            // 
            manufacturerNameLabel.AutoSize = true;
            manufacturerNameLabel.Location = new System.Drawing.Point(26, 81);
            manufacturerNameLabel.Name = "manufacturerNameLabel";
            manufacturerNameLabel.Size = new System.Drawing.Size(104, 13);
            manufacturerNameLabel.TabIndex = 50;
            manufacturerNameLabel.Text = "Manufacturer Name:";
            // 
            // productKeyLabel
            // 
            productKeyLabel.AutoSize = true;
            productKeyLabel.Location = new System.Drawing.Point(456, 108);
            productKeyLabel.Name = "productKeyLabel";
            productKeyLabel.Size = new System.Drawing.Size(28, 13);
            productKeyLabel.TabIndex = 52;
            productKeyLabel.Text = "Key:";
            // 
            // productNameLabel
            // 
            productNameLabel.AutoSize = true;
            productNameLabel.Location = new System.Drawing.Point(26, 108);
            productNameLabel.Name = "productNameLabel";
            productNameLabel.Size = new System.Drawing.Size(78, 13);
            productNameLabel.TabIndex = 54;
            productNameLabel.Text = "Product Name:";
            // 
            // securityLevelLabel
            // 
            securityLevelLabel.AutoSize = true;
            securityLevelLabel.Location = new System.Drawing.Point(26, 162);
            securityLevelLabel.Name = "securityLevelLabel";
            securityLevelLabel.Size = new System.Drawing.Size(77, 13);
            securityLevelLabel.TabIndex = 58;
            securityLevelLabel.Text = "Security Level:";
            // 
            // userCodeLabel
            // 
            userCodeLabel.AutoSize = true;
            userCodeLabel.Location = new System.Drawing.Point(26, 55);
            userCodeLabel.Name = "userCodeLabel";
            userCodeLabel.Size = new System.Drawing.Size(60, 13);
            userCodeLabel.TabIndex = 74;
            userCodeLabel.Text = "User Code:";
            // 
            // associationKeyTextBox
            // 
            this.associationKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.contactRolesBindingSource, "AssociationKey", true));
            this.associationKeyTextBox.Location = new System.Drawing.Point(487, 24);
            this.associationKeyTextBox.Name = "associationKeyTextBox";
            this.associationKeyTextBox.Size = new System.Drawing.Size(40, 20);
            this.associationKeyTextBox.TabIndex = 31;
            this.associationKeyTextBox.TabStop = false;
            // 
            // contactRolesBindingSource
            // 
            this.contactRolesBindingSource.DataSource = typeof(AhamMetaDataDAL.ContactRoles);
            // 
            // associationNameComboBox
            // 
            this.associationNameComboBox.FormattingEnabled = true;
            this.associationNameComboBox.Location = new System.Drawing.Point(136, 24);
            this.associationNameComboBox.Name = "associationNameComboBox";
            this.associationNameComboBox.Size = new System.Drawing.Size(310, 21);
            this.associationNameComboBox.TabIndex = 0;
            // 
            // billableCheckBox
            // 
            this.billableCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "Billable", true));
            this.billableCheckBox.Location = new System.Drawing.Point(29, 389);
            this.billableCheckBox.Name = "billableCheckBox";
            this.billableCheckBox.Size = new System.Drawing.Size(121, 24);
            this.billableCheckBox.TabIndex = 16;
            this.billableCheckBox.Text = "Billable";
            this.billableCheckBox.UseVisualStyleBackColor = true;
            // 
            // catalogKeyTextBox
            // 
            this.catalogKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.contactRolesBindingSource, "CatalogKey", true));
            this.catalogKeyTextBox.Location = new System.Drawing.Point(487, 133);
            this.catalogKeyTextBox.Name = "catalogKeyTextBox";
            this.catalogKeyTextBox.Size = new System.Drawing.Size(40, 20);
            this.catalogKeyTextBox.TabIndex = 37;
            this.catalogKeyTextBox.TabStop = false;
            // 
            // catalogNameComboBox
            // 
            this.catalogNameComboBox.FormattingEnabled = true;
            this.catalogNameComboBox.Location = new System.Drawing.Point(136, 133);
            this.catalogNameComboBox.Name = "catalogNameComboBox";
            this.catalogNameComboBox.Size = new System.Drawing.Size(310, 21);
            this.catalogNameComboBox.TabIndex = 4;
            // 
            // contactRolesKeyTextBox
            // 
            this.contactRolesKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.contactRolesBindingSource, "ContactRolesKey", true));
            this.contactRolesKeyTextBox.Location = new System.Drawing.Point(487, 434);
            this.contactRolesKeyTextBox.Name = "contactRolesKeyTextBox";
            this.contactRolesKeyTextBox.Size = new System.Drawing.Size(40, 20);
            this.contactRolesKeyTextBox.TabIndex = 41;
            this.contactRolesKeyTextBox.TabStop = false;
            // 
            // editCodesCheckBox
            // 
            this.editCodesCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "EditCodes", true));
            this.editCodesCheckBox.Location = new System.Drawing.Point(283, 231);
            this.editCodesCheckBox.Name = "editCodesCheckBox";
            this.editCodesCheckBox.Size = new System.Drawing.Size(121, 24);
            this.editCodesCheckBox.TabIndex = 13;
            this.editCodesCheckBox.Text = "Edit codes";
            this.editCodesCheckBox.UseVisualStyleBackColor = true;
            // 
            // editCompanyDataCheckBox
            // 
            this.editCompanyDataCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "EditCompanyData", true));
            this.editCompanyDataCheckBox.Location = new System.Drawing.Point(283, 201);
            this.editCompanyDataCheckBox.Name = "editCompanyDataCheckBox";
            this.editCompanyDataCheckBox.Size = new System.Drawing.Size(121, 24);
            this.editCompanyDataCheckBox.TabIndex = 12;
            this.editCompanyDataCheckBox.Text = "Edit company data";
            this.editCompanyDataCheckBox.UseVisualStyleBackColor = true;
            // 
            // editModelsCheckBox
            // 
            this.editModelsCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "EditModels", true));
            this.editModelsCheckBox.Location = new System.Drawing.Point(283, 261);
            this.editModelsCheckBox.Name = "editModelsCheckBox";
            this.editModelsCheckBox.Size = new System.Drawing.Size(121, 24);
            this.editModelsCheckBox.TabIndex = 14;
            this.editModelsCheckBox.Text = "Edit models";
            this.editModelsCheckBox.UseVisualStyleBackColor = true;
            // 
            // manufacturerKeyTextBox
            // 
            this.manufacturerKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.contactRolesBindingSource, "ManufacturerKey", true));
            this.manufacturerKeyTextBox.Location = new System.Drawing.Point(487, 78);
            this.manufacturerKeyTextBox.Name = "manufacturerKeyTextBox";
            this.manufacturerKeyTextBox.Size = new System.Drawing.Size(40, 20);
            this.manufacturerKeyTextBox.TabIndex = 49;
            this.manufacturerKeyTextBox.TabStop = false;
            // 
            // manufacturerNameComboBox
            // 
            this.manufacturerNameComboBox.FormattingEnabled = true;
            this.manufacturerNameComboBox.Location = new System.Drawing.Point(136, 78);
            this.manufacturerNameComboBox.Name = "manufacturerNameComboBox";
            this.manufacturerNameComboBox.Size = new System.Drawing.Size(310, 21);
            this.manufacturerNameComboBox.TabIndex = 2;
            // 
            // productKeyTextBox
            // 
            this.productKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.contactRolesBindingSource, "ProductKey", true));
            this.productKeyTextBox.Location = new System.Drawing.Point(487, 105);
            this.productKeyTextBox.Name = "productKeyTextBox";
            this.productKeyTextBox.Size = new System.Drawing.Size(40, 20);
            this.productKeyTextBox.TabIndex = 53;
            this.productKeyTextBox.TabStop = false;
            // 
            // productNameComboBox
            // 
            this.productNameComboBox.FormattingEnabled = true;
            this.productNameComboBox.Location = new System.Drawing.Point(136, 105);
            this.productNameComboBox.Name = "productNameComboBox";
            this.productNameComboBox.Size = new System.Drawing.Size(310, 21);
            this.productNameComboBox.TabIndex = 3;
            // 
            // responsibleCheckBox
            // 
            this.responsibleCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "Responsible", true));
            this.responsibleCheckBox.Location = new System.Drawing.Point(283, 389);
            this.responsibleCheckBox.Name = "responsibleCheckBox";
            this.responsibleCheckBox.Size = new System.Drawing.Size(95, 24);
            this.responsibleCheckBox.TabIndex = 17;
            this.responsibleCheckBox.Text = "Responsible";
            this.responsibleCheckBox.UseVisualStyleBackColor = true;
            // 
            // securityLevelTextBox
            // 
            this.securityLevelTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.contactRolesBindingSource, "SecurityLevel", true));
            this.securityLevelTextBox.Location = new System.Drawing.Point(136, 159);
            this.securityLevelTextBox.Name = "securityLevelTextBox";
            this.securityLevelTextBox.Size = new System.Drawing.Size(40, 20);
            this.securityLevelTextBox.TabIndex = 5;
            // 
            // seeAllCheckBox
            // 
            this.seeAllCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "SeeAll", true));
            this.seeAllCheckBox.Location = new System.Drawing.Point(29, 291);
            this.seeAllCheckBox.Name = "seeAllCheckBox";
            this.seeAllCheckBox.Size = new System.Drawing.Size(121, 24);
            this.seeAllCheckBox.TabIndex = 9;
            this.seeAllCheckBox.Text = "See all";
            this.seeAllCheckBox.UseVisualStyleBackColor = true;
            // 
            // seeCompanyDataCheckBox
            // 
            this.seeCompanyDataCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "SeeCompanyData", true));
            this.seeCompanyDataCheckBox.Location = new System.Drawing.Point(29, 201);
            this.seeCompanyDataCheckBox.Name = "seeCompanyDataCheckBox";
            this.seeCompanyDataCheckBox.Size = new System.Drawing.Size(121, 24);
            this.seeCompanyDataCheckBox.TabIndex = 6;
            this.seeCompanyDataCheckBox.Text = "See company data";
            this.seeCompanyDataCheckBox.UseVisualStyleBackColor = true;
            // 
            // seeIndustryDataCheckBox
            // 
            this.seeIndustryDataCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "SeeIndustryData", true));
            this.seeIndustryDataCheckBox.Location = new System.Drawing.Point(29, 231);
            this.seeIndustryDataCheckBox.Name = "seeIndustryDataCheckBox";
            this.seeIndustryDataCheckBox.Size = new System.Drawing.Size(121, 24);
            this.seeIndustryDataCheckBox.TabIndex = 7;
            this.seeIndustryDataCheckBox.Text = "See industry data";
            this.seeIndustryDataCheckBox.UseVisualStyleBackColor = true;
            // 
            // seeModelsCheckBox
            // 
            this.seeModelsCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "SeeModels", true));
            this.seeModelsCheckBox.Location = new System.Drawing.Point(29, 261);
            this.seeModelsCheckBox.Name = "seeModelsCheckBox";
            this.seeModelsCheckBox.Size = new System.Drawing.Size(121, 24);
            this.seeModelsCheckBox.TabIndex = 8;
            this.seeModelsCheckBox.Text = "See models";
            this.seeModelsCheckBox.UseVisualStyleBackColor = true;
            // 
            // seePublicCheckBox
            // 
            this.seePublicCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "SeePublic", true));
            this.seePublicCheckBox.Location = new System.Drawing.Point(29, 321);
            this.seePublicCheckBox.Name = "seePublicCheckBox";
            this.seePublicCheckBox.Size = new System.Drawing.Size(121, 24);
            this.seePublicCheckBox.TabIndex = 10;
            this.seePublicCheckBox.Text = "See public";
            this.seePublicCheckBox.UseVisualStyleBackColor = true;
            // 
            // seeStatusDetailsCheckBox
            // 
            this.seeStatusDetailsCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "SeeStatusDetails", true));
            this.seeStatusDetailsCheckBox.Location = new System.Drawing.Point(29, 351);
            this.seeStatusDetailsCheckBox.Name = "seeStatusDetailsCheckBox";
            this.seeStatusDetailsCheckBox.Size = new System.Drawing.Size(121, 24);
            this.seeStatusDetailsCheckBox.TabIndex = 11;
            this.seeStatusDetailsCheckBox.Text = "See status details";
            this.seeStatusDetailsCheckBox.UseVisualStyleBackColor = true;
            // 
            // submitDataCheckBox
            // 
            this.submitDataCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.contactRolesBindingSource, "SubmitData", true));
            this.submitDataCheckBox.Location = new System.Drawing.Point(283, 291);
            this.submitDataCheckBox.Name = "submitDataCheckBox";
            this.submitDataCheckBox.Size = new System.Drawing.Size(121, 24);
            this.submitDataCheckBox.TabIndex = 15;
            this.submitDataCheckBox.Text = "Submit data";
            this.submitDataCheckBox.UseVisualStyleBackColor = true;
            // 
            // userCodeTextBox
            // 
            this.userCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.contactRolesBindingSource, "UserCode", true));
            this.userCodeTextBox.Location = new System.Drawing.Point(136, 52);
            this.userCodeTextBox.Name = "userCodeTextBox";
            this.userCodeTextBox.Size = new System.Drawing.Size(310, 20);
            this.userCodeTextBox.TabIndex = 1;
            // 
            // ContactRolesEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 515);
            this.Controls.Add(associationKeyLabel);
            this.Controls.Add(this.associationKeyTextBox);
            this.Controls.Add(associationNameLabel);
            this.Controls.Add(this.associationNameComboBox);
            this.Controls.Add(this.billableCheckBox);
            this.Controls.Add(catalogKeyLabel);
            this.Controls.Add(this.catalogKeyTextBox);
            this.Controls.Add(catalogNameLabel);
            this.Controls.Add(this.catalogNameComboBox);
            this.Controls.Add(contactRolesKeyLabel);
            this.Controls.Add(this.contactRolesKeyTextBox);
            this.Controls.Add(this.editCodesCheckBox);
            this.Controls.Add(this.editCompanyDataCheckBox);
            this.Controls.Add(this.editModelsCheckBox);
            this.Controls.Add(manufacturerKeyLabel);
            this.Controls.Add(this.manufacturerKeyTextBox);
            this.Controls.Add(manufacturerNameLabel);
            this.Controls.Add(this.manufacturerNameComboBox);
            this.Controls.Add(productKeyLabel);
            this.Controls.Add(this.productKeyTextBox);
            this.Controls.Add(productNameLabel);
            this.Controls.Add(this.productNameComboBox);
            this.Controls.Add(this.responsibleCheckBox);
            this.Controls.Add(securityLevelLabel);
            this.Controls.Add(this.securityLevelTextBox);
            this.Controls.Add(this.seeAllCheckBox);
            this.Controls.Add(this.seeCompanyDataCheckBox);
            this.Controls.Add(this.seeIndustryDataCheckBox);
            this.Controls.Add(this.seeModelsCheckBox);
            this.Controls.Add(this.seePublicCheckBox);
            this.Controls.Add(this.seeStatusDetailsCheckBox);
            this.Controls.Add(this.submitDataCheckBox);
            this.Controls.Add(userCodeLabel);
            this.Controls.Add(this.userCodeTextBox);
            this.Name = "ContactRolesEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Contact roles";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.userCodeTextBox, 0);
            this.Controls.SetChildIndex(userCodeLabel, 0);
            this.Controls.SetChildIndex(this.submitDataCheckBox, 0);
            this.Controls.SetChildIndex(this.seeStatusDetailsCheckBox, 0);
            this.Controls.SetChildIndex(this.seePublicCheckBox, 0);
            this.Controls.SetChildIndex(this.seeModelsCheckBox, 0);
            this.Controls.SetChildIndex(this.seeIndustryDataCheckBox, 0);
            this.Controls.SetChildIndex(this.seeCompanyDataCheckBox, 0);
            this.Controls.SetChildIndex(this.seeAllCheckBox, 0);
            this.Controls.SetChildIndex(this.securityLevelTextBox, 0);
            this.Controls.SetChildIndex(securityLevelLabel, 0);
            this.Controls.SetChildIndex(this.responsibleCheckBox, 0);
            this.Controls.SetChildIndex(this.productNameComboBox, 0);
            this.Controls.SetChildIndex(productNameLabel, 0);
            this.Controls.SetChildIndex(this.productKeyTextBox, 0);
            this.Controls.SetChildIndex(productKeyLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerNameComboBox, 0);
            this.Controls.SetChildIndex(manufacturerNameLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerKeyTextBox, 0);
            this.Controls.SetChildIndex(manufacturerKeyLabel, 0);
            this.Controls.SetChildIndex(this.editModelsCheckBox, 0);
            this.Controls.SetChildIndex(this.editCompanyDataCheckBox, 0);
            this.Controls.SetChildIndex(this.editCodesCheckBox, 0);
            this.Controls.SetChildIndex(this.contactRolesKeyTextBox, 0);
            this.Controls.SetChildIndex(contactRolesKeyLabel, 0);
            this.Controls.SetChildIndex(this.catalogNameComboBox, 0);
            this.Controls.SetChildIndex(catalogNameLabel, 0);
            this.Controls.SetChildIndex(this.catalogKeyTextBox, 0);
            this.Controls.SetChildIndex(catalogKeyLabel, 0);
            this.Controls.SetChildIndex(this.billableCheckBox, 0);
            this.Controls.SetChildIndex(this.associationNameComboBox, 0);
            this.Controls.SetChildIndex(associationNameLabel, 0);
            this.Controls.SetChildIndex(this.associationKeyTextBox, 0);
            this.Controls.SetChildIndex(associationKeyLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.contactRolesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource contactRolesBindingSource;
        private System.Windows.Forms.TextBox associationKeyTextBox;
        private System.Windows.Forms.ComboBox associationNameComboBox;
        private System.Windows.Forms.CheckBox billableCheckBox;
        private System.Windows.Forms.TextBox catalogKeyTextBox;
        private System.Windows.Forms.ComboBox catalogNameComboBox;
        private System.Windows.Forms.TextBox contactRolesKeyTextBox;
        private System.Windows.Forms.CheckBox editCodesCheckBox;
        private System.Windows.Forms.CheckBox editCompanyDataCheckBox;
        private System.Windows.Forms.CheckBox editModelsCheckBox;
        private System.Windows.Forms.TextBox manufacturerKeyTextBox;
        private System.Windows.Forms.ComboBox manufacturerNameComboBox;
        private System.Windows.Forms.TextBox productKeyTextBox;
        private System.Windows.Forms.ComboBox productNameComboBox;
        private System.Windows.Forms.CheckBox responsibleCheckBox;
        private System.Windows.Forms.TextBox securityLevelTextBox;
        private System.Windows.Forms.CheckBox seeAllCheckBox;
        private System.Windows.Forms.CheckBox seeCompanyDataCheckBox;
        private System.Windows.Forms.CheckBox seeIndustryDataCheckBox;
        private System.Windows.Forms.CheckBox seeModelsCheckBox;
        private System.Windows.Forms.CheckBox seePublicCheckBox;
        private System.Windows.Forms.CheckBox seeStatusDetailsCheckBox;
        private System.Windows.Forms.CheckBox submitDataCheckBox;
        private System.Windows.Forms.TextBox userCodeTextBox;
    }
}