﻿using System;
using System.Windows.Forms;

using System.Collections;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ContactRolesEditForm : HaiObjectEditForm
    {
        private ContactRoles _contactRolesToEdit;

        public ContactRolesEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _contactRolesToEdit = (ContactRoles)_parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);
            contactRolesBindingSource.DataSource = _contactRolesToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = ds.GetDataList(HaiBusinessObjectType.Association);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Association>(associationNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "AssociationName", "AssociationKey", associationKeyTextBox, _contactRolesToEdit.AssociationName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit contact roles.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Catalog);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Catalog>(catalogNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "CatalogName", "CatalogKey", catalogKeyTextBox, _contactRolesToEdit.CatalogName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit contact roles.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Manufacturer);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Manufacturer>(manufacturerNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ManufacturerName", "ManufacturerKey", manufacturerKeyTextBox, _contactRolesToEdit.ManufacturerName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit contact roles.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Product_Aham);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Product>(productNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ProductName", "ProductKey", productKeyTextBox, _contactRolesToEdit.ProductName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit contact roles.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if ((!userCodeTextBox.ReadOnly) && (userCodeTextBox.Text.Trim() == string.Empty))
            {
                MessageBox.Show("A user code is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (associationNameComboBox.Text.Trim() == string.Empty && associationNameComboBox.Items.Count>0)
            {
                MessageBox.Show("An association must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (manufacturerNameComboBox.Text.Trim() == string.Empty && manufacturerNameComboBox.Items.Count>0)
            {
                MessageBox.Show("A manufacturer must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (productNameComboBox.Text.Trim() == string.Empty && productNameComboBox.Items.Count>0)
            {
                MessageBox.Show("A product must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (catalogNameComboBox.Text.Trim() == string.Empty && catalogNameComboBox.Items.Count>0)
            {
                MessageBox.Show("A catalog must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            // set the property values in the object that was the target of the edit
            MarkChangedProperties(_contactRolesToEdit);
            _contactRolesToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
