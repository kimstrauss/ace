﻿namespace AhamMetaDataPL
{
    partial class CouncilEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label councilAbbreviationLabel;
            System.Windows.Forms.Label councilKeyLabel;
            System.Windows.Forms.Label councilNameLabel;
            this.councilAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.councilBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.councilKeyTextBox = new System.Windows.Forms.TextBox();
            this.councilNameTextBox = new System.Windows.Forms.TextBox();
            councilAbbreviationLabel = new System.Windows.Forms.Label();
            councilKeyLabel = new System.Windows.Forms.Label();
            councilNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.councilBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // councilAbbreviationLabel
            // 
            councilAbbreviationLabel.AutoSize = true;
            councilAbbreviationLabel.Location = new System.Drawing.Point(12, 15);
            councilAbbreviationLabel.Name = "councilAbbreviationLabel";
            councilAbbreviationLabel.Size = new System.Drawing.Size(107, 13);
            councilAbbreviationLabel.TabIndex = 30;
            councilAbbreviationLabel.Text = "Council Abbreviation:";
            // 
            // councilKeyLabel
            // 
            councilKeyLabel.AutoSize = true;
            councilKeyLabel.Location = new System.Drawing.Point(12, 67);
            councilKeyLabel.Name = "councilKeyLabel";
            councilKeyLabel.Size = new System.Drawing.Size(66, 13);
            councilKeyLabel.TabIndex = 32;
            councilKeyLabel.Text = "Council Key:";
            // 
            // councilNameLabel
            // 
            councilNameLabel.AutoSize = true;
            councilNameLabel.Location = new System.Drawing.Point(12, 41);
            councilNameLabel.Name = "councilNameLabel";
            councilNameLabel.Size = new System.Drawing.Size(76, 13);
            councilNameLabel.TabIndex = 34;
            councilNameLabel.Text = "Council Name:";
            // 
            // councilAbbreviationTextBox
            // 
            this.councilAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.councilBindingSource, "CouncilAbbreviation", true));
            this.councilAbbreviationTextBox.Location = new System.Drawing.Point(125, 12);
            this.councilAbbreviationTextBox.Name = "councilAbbreviationTextBox";
            this.councilAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.councilAbbreviationTextBox.TabIndex = 0;
            // 
            // councilBindingSource
            // 
            this.councilBindingSource.DataSource = typeof(AhamMetaDataDAL.Council);
            // 
            // councilKeyTextBox
            // 
            this.councilKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.councilBindingSource, "CouncilKey", true));
            this.councilKeyTextBox.Location = new System.Drawing.Point(125, 64);
            this.councilKeyTextBox.Name = "councilKeyTextBox";
            this.councilKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.councilKeyTextBox.TabIndex = 2;
            this.councilKeyTextBox.TabStop = false;
            // 
            // councilNameTextBox
            // 
            this.councilNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.councilBindingSource, "CouncilName", true));
            this.councilNameTextBox.Location = new System.Drawing.Point(125, 38);
            this.councilNameTextBox.Name = "councilNameTextBox";
            this.councilNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.councilNameTextBox.TabIndex = 1;
            // 
            // CouncilEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(councilAbbreviationLabel);
            this.Controls.Add(this.councilAbbreviationTextBox);
            this.Controls.Add(councilKeyLabel);
            this.Controls.Add(this.councilKeyTextBox);
            this.Controls.Add(councilNameLabel);
            this.Controls.Add(this.councilNameTextBox);
            this.Name = "CouncilEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Council";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.councilNameTextBox, 0);
            this.Controls.SetChildIndex(councilNameLabel, 0);
            this.Controls.SetChildIndex(this.councilKeyTextBox, 0);
            this.Controls.SetChildIndex(councilKeyLabel, 0);
            this.Controls.SetChildIndex(this.councilAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(councilAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.councilBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource councilBindingSource;
        private System.Windows.Forms.TextBox councilAbbreviationTextBox;
        private System.Windows.Forms.TextBox councilKeyTextBox;
        private System.Windows.Forms.TextBox councilNameTextBox;
    }
}