﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class CouncilEditForm : HaiObjectEditForm
    {
        private Council _councilToEdit;

        public CouncilEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _councilToEdit = (Council)parameters.EditObject;

            this.Load+=new EventHandler(EditForm_Load);
            btnOK.Click+=new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            councilBindingSource.DataSource = _councilToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (councilNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (councilAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_councilToEdit);
            _councilToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}

