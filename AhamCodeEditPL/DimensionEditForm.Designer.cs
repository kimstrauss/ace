﻿namespace AhamMetaDataPL
{
    partial class DimensionEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label dimensionAbbreviationLabel;
            System.Windows.Forms.Label dimensionKeyLabel;
            System.Windows.Forms.Label dimensionNameLabel;
            this.dimensionAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.dimensionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dimensionKeyTextBox = new System.Windows.Forms.TextBox();
            this.dimensionNameTextBox = new System.Windows.Forms.TextBox();
            dimensionAbbreviationLabel = new System.Windows.Forms.Label();
            dimensionKeyLabel = new System.Windows.Forms.Label();
            dimensionNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dimensionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // dimensionAbbreviationLabel
            // 
            dimensionAbbreviationLabel.AutoSize = true;
            dimensionAbbreviationLabel.Location = new System.Drawing.Point(9, 15);
            dimensionAbbreviationLabel.Name = "dimensionAbbreviationLabel";
            dimensionAbbreviationLabel.Size = new System.Drawing.Size(121, 13);
            dimensionAbbreviationLabel.TabIndex = 30;
            dimensionAbbreviationLabel.Text = "Dimension Abbreviation:";
            // 
            // dimensionKeyLabel
            // 
            dimensionKeyLabel.AutoSize = true;
            dimensionKeyLabel.Location = new System.Drawing.Point(9, 67);
            dimensionKeyLabel.Name = "dimensionKeyLabel";
            dimensionKeyLabel.Size = new System.Drawing.Size(80, 13);
            dimensionKeyLabel.TabIndex = 32;
            dimensionKeyLabel.Text = "Dimension Key:";
            // 
            // dimensionNameLabel
            // 
            dimensionNameLabel.AutoSize = true;
            dimensionNameLabel.Location = new System.Drawing.Point(9, 41);
            dimensionNameLabel.Name = "dimensionNameLabel";
            dimensionNameLabel.Size = new System.Drawing.Size(90, 13);
            dimensionNameLabel.TabIndex = 34;
            dimensionNameLabel.Text = "Dimension Name:";
            // 
            // dimensionAbbreviationTextBox
            // 
            this.dimensionAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dimensionBindingSource, "DimensionAbbreviation", true));
            this.dimensionAbbreviationTextBox.Location = new System.Drawing.Point(136, 12);
            this.dimensionAbbreviationTextBox.Name = "dimensionAbbreviationTextBox";
            this.dimensionAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.dimensionAbbreviationTextBox.TabIndex = 0;
            // 
            // dimensionBindingSource
            // 
            this.dimensionBindingSource.DataSource = typeof(AhamMetaDataDAL.Dimension);
            // 
            // dimensionKeyTextBox
            // 
            this.dimensionKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dimensionBindingSource, "DimensionKey", true));
            this.dimensionKeyTextBox.Location = new System.Drawing.Point(136, 64);
            this.dimensionKeyTextBox.Name = "dimensionKeyTextBox";
            this.dimensionKeyTextBox.Size = new System.Drawing.Size(100, 20);
            this.dimensionKeyTextBox.TabIndex = 2;
            this.dimensionKeyTextBox.TabStop = false;
            // 
            // dimensionNameTextBox
            // 
            this.dimensionNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dimensionBindingSource, "DimensionName", true));
            this.dimensionNameTextBox.Location = new System.Drawing.Point(136, 38);
            this.dimensionNameTextBox.Name = "dimensionNameTextBox";
            this.dimensionNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.dimensionNameTextBox.TabIndex = 1;
            // 
            // DimensionEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(dimensionAbbreviationLabel);
            this.Controls.Add(this.dimensionAbbreviationTextBox);
            this.Controls.Add(dimensionKeyLabel);
            this.Controls.Add(this.dimensionKeyTextBox);
            this.Controls.Add(dimensionNameLabel);
            this.Controls.Add(this.dimensionNameTextBox);
            this.Name = "DimensionEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dimension";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.dimensionNameTextBox, 0);
            this.Controls.SetChildIndex(dimensionNameLabel, 0);
            this.Controls.SetChildIndex(this.dimensionKeyTextBox, 0);
            this.Controls.SetChildIndex(dimensionKeyLabel, 0);
            this.Controls.SetChildIndex(this.dimensionAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(dimensionAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dimensionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource dimensionBindingSource;
        private System.Windows.Forms.TextBox dimensionAbbreviationTextBox;
        private System.Windows.Forms.TextBox dimensionKeyTextBox;
        private System.Windows.Forms.TextBox dimensionNameTextBox;
    }
}