﻿namespace AhamMetaDataPL
{
    partial class ExpansionFactorEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label activityKeyLabel;
            System.Windows.Forms.Label activityNameLabel;
            System.Windows.Forms.Label catalogKeyLabel;
            System.Windows.Forms.Label catalogNameLabel;
            System.Windows.Forms.Label expansionFactorKeyLabel;
            System.Windows.Forms.Label expansionFactorQuantityLabel;
            System.Windows.Forms.Label expansionFactorValueLabel;
            System.Windows.Forms.Label marketkeyLabel;
            System.Windows.Forms.Label marketNameLabel;
            System.Windows.Forms.Label productKeyLabel;
            System.Windows.Forms.Label productNameLabel;
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label endDateLabel;
            this.activityKeyTextBox = new System.Windows.Forms.TextBox();
            this.expansionFactorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.activityNameComboBox = new System.Windows.Forms.ComboBox();
            this.allowReleaseCheckBox = new System.Windows.Forms.CheckBox();
            this.catalogKeyTextBox = new System.Windows.Forms.TextBox();
            this.catalogNameComboBox = new System.Windows.Forms.ComboBox();
            this.expansionFactorKeyTextBox = new System.Windows.Forms.TextBox();
            this.expansionFactorQuantityTextBox = new System.Windows.Forms.TextBox();
            this.expansionFactorValueTextBox = new System.Windows.Forms.TextBox();
            this.marketkeyTextBox = new System.Windows.Forms.TextBox();
            this.marketNameComboBox = new System.Windows.Forms.ComboBox();
            this.productKeyTextBox = new System.Windows.Forms.TextBox();
            this.productNameComboBox = new System.Windows.Forms.ComboBox();
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            activityKeyLabel = new System.Windows.Forms.Label();
            activityNameLabel = new System.Windows.Forms.Label();
            catalogKeyLabel = new System.Windows.Forms.Label();
            catalogNameLabel = new System.Windows.Forms.Label();
            expansionFactorKeyLabel = new System.Windows.Forms.Label();
            expansionFactorQuantityLabel = new System.Windows.Forms.Label();
            expansionFactorValueLabel = new System.Windows.Forms.Label();
            marketkeyLabel = new System.Windows.Forms.Label();
            marketNameLabel = new System.Windows.Forms.Label();
            productKeyLabel = new System.Windows.Forms.Label();
            productNameLabel = new System.Windows.Forms.Label();
            beginDateLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.expansionFactorBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(386, 266);
            this.btnOK.TabIndex = 8;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(467, 266);
            this.btnCancel.TabIndex = 9;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 266);
            // 
            // activityKeyLabel
            // 
            activityKeyLabel.AutoSize = true;
            activityKeyLabel.Location = new System.Drawing.Point(457, 96);
            activityKeyLabel.Name = "activityKeyLabel";
            activityKeyLabel.Size = new System.Drawing.Size(28, 13);
            activityKeyLabel.TabIndex = 30;
            activityKeyLabel.Text = "Key:";
            // 
            // activityNameLabel
            // 
            activityNameLabel.AutoSize = true;
            activityNameLabel.Location = new System.Drawing.Point(18, 96);
            activityNameLabel.Name = "activityNameLabel";
            activityNameLabel.Size = new System.Drawing.Size(75, 13);
            activityNameLabel.TabIndex = 32;
            activityNameLabel.Text = "Activity Name:";
            // 
            // catalogKeyLabel
            // 
            catalogKeyLabel.AutoSize = true;
            catalogKeyLabel.Location = new System.Drawing.Point(457, 68);
            catalogKeyLabel.Name = "catalogKeyLabel";
            catalogKeyLabel.Size = new System.Drawing.Size(28, 13);
            catalogKeyLabel.TabIndex = 38;
            catalogKeyLabel.Text = "Key:";
            // 
            // catalogNameLabel
            // 
            catalogNameLabel.AutoSize = true;
            catalogNameLabel.Location = new System.Drawing.Point(16, 69);
            catalogNameLabel.Name = "catalogNameLabel";
            catalogNameLabel.Size = new System.Drawing.Size(77, 13);
            catalogNameLabel.TabIndex = 40;
            catalogNameLabel.Text = "Catalog Name:";
            // 
            // expansionFactorKeyLabel
            // 
            expansionFactorKeyLabel.AutoSize = true;
            expansionFactorKeyLabel.Location = new System.Drawing.Point(350, 226);
            expansionFactorKeyLabel.Name = "expansionFactorKeyLabel";
            expansionFactorKeyLabel.Size = new System.Drawing.Size(113, 13);
            expansionFactorKeyLabel.TabIndex = 44;
            expansionFactorKeyLabel.Text = "Expansion Factor Key:";
            // 
            // expansionFactorQuantityLabel
            // 
            expansionFactorQuantityLabel.AutoSize = true;
            expansionFactorQuantityLabel.Location = new System.Drawing.Point(270, 150);
            expansionFactorQuantityLabel.Name = "expansionFactorQuantityLabel";
            expansionFactorQuantityLabel.Size = new System.Drawing.Size(134, 13);
            expansionFactorQuantityLabel.TabIndex = 46;
            expansionFactorQuantityLabel.Text = "Expansion Factor Quantity:";
            // 
            // expansionFactorValueLabel
            // 
            expansionFactorValueLabel.AutoSize = true;
            expansionFactorValueLabel.Location = new System.Drawing.Point(282, 177);
            expansionFactorValueLabel.Name = "expansionFactorValueLabel";
            expansionFactorValueLabel.Size = new System.Drawing.Size(122, 13);
            expansionFactorValueLabel.TabIndex = 48;
            expansionFactorValueLabel.Text = "Expansion Factor Value:";
            // 
            // marketkeyLabel
            // 
            marketkeyLabel.AutoSize = true;
            marketkeyLabel.Location = new System.Drawing.Point(457, 41);
            marketkeyLabel.Name = "marketkeyLabel";
            marketkeyLabel.Size = new System.Drawing.Size(28, 13);
            marketkeyLabel.TabIndex = 50;
            marketkeyLabel.Text = "Key:";
            // 
            // marketNameLabel
            // 
            marketNameLabel.AutoSize = true;
            marketNameLabel.Location = new System.Drawing.Point(19, 42);
            marketNameLabel.Name = "marketNameLabel";
            marketNameLabel.Size = new System.Drawing.Size(74, 13);
            marketNameLabel.TabIndex = 52;
            marketNameLabel.Text = "Market Name:";
            // 
            // productKeyLabel
            // 
            productKeyLabel.AutoSize = true;
            productKeyLabel.Location = new System.Drawing.Point(457, 15);
            productKeyLabel.Name = "productKeyLabel";
            productKeyLabel.Size = new System.Drawing.Size(28, 13);
            productKeyLabel.TabIndex = 54;
            productKeyLabel.Text = "Key:";
            // 
            // productNameLabel
            // 
            productNameLabel.AutoSize = true;
            productNameLabel.Location = new System.Drawing.Point(15, 15);
            productNameLabel.Name = "productNameLabel";
            productNameLabel.Size = new System.Drawing.Size(78, 13);
            productNameLabel.TabIndex = 56;
            productNameLabel.Text = "Product Name:";
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(30, 150);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 57;
            beginDateLabel.Text = "Begin Date:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(38, 177);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 58;
            endDateLabel.Text = "End Date:";
            // 
            // activityKeyTextBox
            // 
            this.activityKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.expansionFactorBindingSource, "ActivityKey", true));
            this.activityKeyTextBox.Location = new System.Drawing.Point(488, 93);
            this.activityKeyTextBox.Name = "activityKeyTextBox";
            this.activityKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.activityKeyTextBox.TabIndex = 31;
            this.activityKeyTextBox.TabStop = false;
            // 
            // expansionFactorBindingSource
            // 
            this.expansionFactorBindingSource.DataSource = typeof(AhamMetaDataDAL.ExpansionFactor);
            // 
            // activityNameComboBox
            // 
            this.activityNameComboBox.FormattingEnabled = true;
            this.activityNameComboBox.Location = new System.Drawing.Point(106, 93);
            this.activityNameComboBox.Name = "activityNameComboBox";
            this.activityNameComboBox.Size = new System.Drawing.Size(345, 21);
            this.activityNameComboBox.TabIndex = 3;
            // 
            // allowReleaseCheckBox
            // 
            this.allowReleaseCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.expansionFactorBindingSource, "AllowRelease", true));
            this.allowReleaseCheckBox.Location = new System.Drawing.Point(106, 199);
            this.allowReleaseCheckBox.Name = "allowReleaseCheckBox";
            this.allowReleaseCheckBox.Size = new System.Drawing.Size(200, 24);
            this.allowReleaseCheckBox.TabIndex = 35;
            this.allowReleaseCheckBox.Text = "Allow release";
            this.allowReleaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // catalogKeyTextBox
            // 
            this.catalogKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.expansionFactorBindingSource, "CatalogKey", true));
            this.catalogKeyTextBox.Location = new System.Drawing.Point(488, 65);
            this.catalogKeyTextBox.Name = "catalogKeyTextBox";
            this.catalogKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.catalogKeyTextBox.TabIndex = 39;
            this.catalogKeyTextBox.TabStop = false;
            // 
            // catalogNameComboBox
            // 
            this.catalogNameComboBox.FormattingEnabled = true;
            this.catalogNameComboBox.Location = new System.Drawing.Point(106, 66);
            this.catalogNameComboBox.Name = "catalogNameComboBox";
            this.catalogNameComboBox.Size = new System.Drawing.Size(345, 21);
            this.catalogNameComboBox.TabIndex = 2;
            // 
            // expansionFactorKeyTextBox
            // 
            this.expansionFactorKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.expansionFactorBindingSource, "ExpansionFactorKey", true));
            this.expansionFactorKeyTextBox.Location = new System.Drawing.Point(469, 223);
            this.expansionFactorKeyTextBox.Name = "expansionFactorKeyTextBox";
            this.expansionFactorKeyTextBox.Size = new System.Drawing.Size(72, 20);
            this.expansionFactorKeyTextBox.TabIndex = 45;
            this.expansionFactorKeyTextBox.TabStop = false;
            // 
            // expansionFactorQuantityTextBox
            // 
            this.expansionFactorQuantityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.expansionFactorBindingSource, "ExpansionFactorQuantity", true));
            this.expansionFactorQuantityTextBox.Location = new System.Drawing.Point(410, 147);
            this.expansionFactorQuantityTextBox.Name = "expansionFactorQuantityTextBox";
            this.expansionFactorQuantityTextBox.Size = new System.Drawing.Size(131, 20);
            this.expansionFactorQuantityTextBox.TabIndex = 6;
            // 
            // expansionFactorValueTextBox
            // 
            this.expansionFactorValueTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.expansionFactorBindingSource, "ExpansionFactorValue", true));
            this.expansionFactorValueTextBox.Location = new System.Drawing.Point(410, 173);
            this.expansionFactorValueTextBox.Name = "expansionFactorValueTextBox";
            this.expansionFactorValueTextBox.Size = new System.Drawing.Size(131, 20);
            this.expansionFactorValueTextBox.TabIndex = 7;
            // 
            // marketkeyTextBox
            // 
            this.marketkeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.expansionFactorBindingSource, "Marketkey", true));
            this.marketkeyTextBox.Location = new System.Drawing.Point(488, 38);
            this.marketkeyTextBox.Name = "marketkeyTextBox";
            this.marketkeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.marketkeyTextBox.TabIndex = 51;
            this.marketkeyTextBox.TabStop = false;
            // 
            // marketNameComboBox
            // 
            this.marketNameComboBox.FormattingEnabled = true;
            this.marketNameComboBox.Location = new System.Drawing.Point(106, 39);
            this.marketNameComboBox.Name = "marketNameComboBox";
            this.marketNameComboBox.Size = new System.Drawing.Size(345, 21);
            this.marketNameComboBox.TabIndex = 1;
            // 
            // productKeyTextBox
            // 
            this.productKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.expansionFactorBindingSource, "ProductKey", true));
            this.productKeyTextBox.Location = new System.Drawing.Point(488, 12);
            this.productKeyTextBox.Name = "productKeyTextBox";
            this.productKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.productKeyTextBox.TabIndex = 55;
            this.productKeyTextBox.TabStop = false;
            // 
            // productNameComboBox
            // 
            this.productNameComboBox.FormattingEnabled = true;
            this.productNameComboBox.Location = new System.Drawing.Point(106, 12);
            this.productNameComboBox.Name = "productNameComboBox";
            this.productNameComboBox.Size = new System.Drawing.Size(345, 21);
            this.productNameComboBox.TabIndex = 0;
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.expansionFactorBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(106, 147);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.beginDateTextBox.TabIndex = 4;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.expansionFactorBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(106, 173);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.endDateTextBox.TabIndex = 5;
            // 
            // ExpansionFactorEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 301);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(activityKeyLabel);
            this.Controls.Add(this.activityKeyTextBox);
            this.Controls.Add(activityNameLabel);
            this.Controls.Add(this.activityNameComboBox);
            this.Controls.Add(this.allowReleaseCheckBox);
            this.Controls.Add(catalogKeyLabel);
            this.Controls.Add(this.catalogKeyTextBox);
            this.Controls.Add(catalogNameLabel);
            this.Controls.Add(this.catalogNameComboBox);
            this.Controls.Add(expansionFactorKeyLabel);
            this.Controls.Add(this.expansionFactorKeyTextBox);
            this.Controls.Add(expansionFactorQuantityLabel);
            this.Controls.Add(this.expansionFactorQuantityTextBox);
            this.Controls.Add(expansionFactorValueLabel);
            this.Controls.Add(this.expansionFactorValueTextBox);
            this.Controls.Add(marketkeyLabel);
            this.Controls.Add(this.marketkeyTextBox);
            this.Controls.Add(marketNameLabel);
            this.Controls.Add(this.marketNameComboBox);
            this.Controls.Add(productKeyLabel);
            this.Controls.Add(this.productKeyTextBox);
            this.Controls.Add(productNameLabel);
            this.Controls.Add(this.productNameComboBox);
            this.Name = "ExpansionFactorEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Expansion factors";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.productNameComboBox, 0);
            this.Controls.SetChildIndex(productNameLabel, 0);
            this.Controls.SetChildIndex(this.productKeyTextBox, 0);
            this.Controls.SetChildIndex(productKeyLabel, 0);
            this.Controls.SetChildIndex(this.marketNameComboBox, 0);
            this.Controls.SetChildIndex(marketNameLabel, 0);
            this.Controls.SetChildIndex(this.marketkeyTextBox, 0);
            this.Controls.SetChildIndex(marketkeyLabel, 0);
            this.Controls.SetChildIndex(this.expansionFactorValueTextBox, 0);
            this.Controls.SetChildIndex(expansionFactorValueLabel, 0);
            this.Controls.SetChildIndex(this.expansionFactorQuantityTextBox, 0);
            this.Controls.SetChildIndex(expansionFactorQuantityLabel, 0);
            this.Controls.SetChildIndex(this.expansionFactorKeyTextBox, 0);
            this.Controls.SetChildIndex(expansionFactorKeyLabel, 0);
            this.Controls.SetChildIndex(this.catalogNameComboBox, 0);
            this.Controls.SetChildIndex(catalogNameLabel, 0);
            this.Controls.SetChildIndex(this.catalogKeyTextBox, 0);
            this.Controls.SetChildIndex(catalogKeyLabel, 0);
            this.Controls.SetChildIndex(this.allowReleaseCheckBox, 0);
            this.Controls.SetChildIndex(this.activityNameComboBox, 0);
            this.Controls.SetChildIndex(activityNameLabel, 0);
            this.Controls.SetChildIndex(this.activityKeyTextBox, 0);
            this.Controls.SetChildIndex(activityKeyLabel, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.expansionFactorBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource expansionFactorBindingSource;
        private System.Windows.Forms.TextBox activityKeyTextBox;
        private System.Windows.Forms.ComboBox activityNameComboBox;
        private System.Windows.Forms.CheckBox allowReleaseCheckBox;
        private System.Windows.Forms.TextBox catalogKeyTextBox;
        private System.Windows.Forms.ComboBox catalogNameComboBox;
        private System.Windows.Forms.TextBox expansionFactorKeyTextBox;
        private System.Windows.Forms.TextBox expansionFactorQuantityTextBox;
        private System.Windows.Forms.TextBox expansionFactorValueTextBox;
        private System.Windows.Forms.TextBox marketkeyTextBox;
        private System.Windows.Forms.ComboBox marketNameComboBox;
        private System.Windows.Forms.TextBox productKeyTextBox;
        private System.Windows.Forms.ComboBox productNameComboBox;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.TextBox endDateTextBox;
    }
}