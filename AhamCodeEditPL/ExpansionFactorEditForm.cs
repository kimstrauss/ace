﻿using System;
using System.Collections;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;
using System.Data.Linq;

using System.Configuration;
using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ExpansionFactorEditForm : HaiObjectEditForm
    {
        private ExpansionFactor _expansionFactorToEdit;

        public ExpansionFactorEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _expansionFactorToEdit = (ExpansionFactor)_parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);    // establish handler for Load Event
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);          // the form needs to be prepared before assigning values to controls
            expansionFactorBindingSource.DataSource = _expansionFactorToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;
            
            result = ds.GetDataList(HaiBusinessObjectType.Market);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Market>(marketNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "MarketName", "MarketKey", marketkeyTextBox, _expansionFactorToEdit.MarketName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit expansion factors because market fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Catalog);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Catalog>(catalogNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "CatalogName", "CatalogKey", catalogKeyTextBox, _expansionFactorToEdit.CatalogName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit expansion factors because catalog fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Product_Aham);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Product>(productNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ProductName", "ProductKey", productKeyTextBox, _expansionFactorToEdit.ProductName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit expansion factors because product fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Activity_Aham);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Activity>(activityNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ActivityName", "ActivityKey", activityKeyTextBox, _expansionFactorToEdit.ActivityName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit expansion factors because activity fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            bool abort = false;

            message = ValidateDaterange(_expansionFactorToEdit, DateRangeValidationType.Daily);
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (marketNameComboBox.Items.Count > 0 && marketNameComboBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A market must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (catalogNameComboBox.Items.Count > 0 && catalogNameComboBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A catalog must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (productNameComboBox.Items.Count > 0 && productNameComboBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A product must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (activityNameComboBox.Items.Count > 0 && activityNameComboBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An activity must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            // set the flags in the property controllers
            MarkChangedProperties(_expansionFactorToEdit);   // this is a multi-edit form
            _expansionFactorToEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
