﻿namespace AhamMetaDataPL
{
    partial class FSListEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label companyLabel;
            System.Windows.Forms.Label emailAddressLabel;
            System.Windows.Forms.Label faxNumberLabel;
            System.Windows.Forms.Label fSListKeyLabel;
            System.Windows.Forms.Label fullNameLabel;
            System.Windows.Forms.Label notesLabel;
            System.Windows.Forms.Label productCodeLabel;
            System.Windows.Forms.Label userCodeLabel;
            System.Windows.Forms.Label workPhoneNumberLabel;
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label beginChargeDateLabel;
            System.Windows.Forms.Label endChargeDateLabel;
            this.companyTextBox = new System.Windows.Forms.TextBox();
            this.fSListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.denyAccessCheckBox = new System.Windows.Forms.CheckBox();
            this.emailAddressTextBox = new System.Windows.Forms.TextBox();
            this.faxNumberTextBox = new System.Windows.Forms.TextBox();
            this.fSCheckBox = new System.Windows.Forms.CheckBox();
            this.fSListKeyTextBox = new System.Windows.Forms.TextBox();
            this.fullNameTextBox = new System.Windows.Forms.TextBox();
            this.productCodeTextBox = new System.Windows.Forms.TextBox();
            this.sendEmailCheckBox = new System.Windows.Forms.CheckBox();
            this.sendFaxCheckBox = new System.Windows.Forms.CheckBox();
            this.userCodeTextBox = new System.Windows.Forms.TextBox();
            this.webCheckBox = new System.Windows.Forms.CheckBox();
            this.workPhoneNumberTextBox = new System.Windows.Forms.TextBox();
            this.notesTextBox = new System.Windows.Forms.TextBox();
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.beginChargeDateTextBox = new System.Windows.Forms.TextBox();
            this.endChargeDateTextBox = new System.Windows.Forms.TextBox();
            companyLabel = new System.Windows.Forms.Label();
            emailAddressLabel = new System.Windows.Forms.Label();
            faxNumberLabel = new System.Windows.Forms.Label();
            fSListKeyLabel = new System.Windows.Forms.Label();
            fullNameLabel = new System.Windows.Forms.Label();
            notesLabel = new System.Windows.Forms.Label();
            productCodeLabel = new System.Windows.Forms.Label();
            userCodeLabel = new System.Windows.Forms.Label();
            workPhoneNumberLabel = new System.Windows.Forms.Label();
            beginDateLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            beginChargeDateLabel = new System.Windows.Forms.Label();
            endChargeDateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fSListBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(406, 458);
            this.btnOK.TabIndex = 19;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(487, 458);
            this.btnCancel.TabIndex = 20;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 458);
            this.btnInsertNull.TabIndex = 18;
            // 
            // companyLabel
            // 
            companyLabel.AutoSize = true;
            companyLabel.Location = new System.Drawing.Point(14, 67);
            companyLabel.Name = "companyLabel";
            companyLabel.Size = new System.Drawing.Size(54, 13);
            companyLabel.TabIndex = 34;
            companyLabel.Text = "Company:";
            // 
            // emailAddressLabel
            // 
            emailAddressLabel.AutoSize = true;
            emailAddressLabel.Location = new System.Drawing.Point(14, 119);
            emailAddressLabel.Name = "emailAddressLabel";
            emailAddressLabel.Size = new System.Drawing.Size(76, 13);
            emailAddressLabel.TabIndex = 38;
            emailAddressLabel.Text = "Email Address:";
            // 
            // faxNumberLabel
            // 
            faxNumberLabel.AutoSize = true;
            faxNumberLabel.Location = new System.Drawing.Point(14, 145);
            faxNumberLabel.Name = "faxNumberLabel";
            faxNumberLabel.Size = new System.Drawing.Size(67, 13);
            faxNumberLabel.TabIndex = 44;
            faxNumberLabel.Text = "Fax Number:";
            // 
            // fSListKeyLabel
            // 
            fSListKeyLabel.AutoSize = true;
            fSListKeyLabel.Location = new System.Drawing.Point(445, 421);
            fSListKeyLabel.Name = "fSListKeyLabel";
            fSListKeyLabel.Size = new System.Drawing.Size(60, 13);
            fSListKeyLabel.TabIndex = 48;
            fSListKeyLabel.Text = "FSList Key:";
            // 
            // fullNameLabel
            // 
            fullNameLabel.AutoSize = true;
            fullNameLabel.Location = new System.Drawing.Point(14, 15);
            fullNameLabel.Name = "fullNameLabel";
            fullNameLabel.Size = new System.Drawing.Size(57, 13);
            fullNameLabel.TabIndex = 50;
            fullNameLabel.Text = "Full Name:";
            // 
            // notesLabel
            // 
            notesLabel.AutoSize = true;
            notesLabel.Location = new System.Drawing.Point(14, 377);
            notesLabel.Name = "notesLabel";
            notesLabel.Size = new System.Drawing.Size(38, 13);
            notesLabel.TabIndex = 52;
            notesLabel.Text = "Notes:";
            // 
            // productCodeLabel
            // 
            productCodeLabel.AutoSize = true;
            productCodeLabel.Location = new System.Drawing.Point(14, 171);
            productCodeLabel.Name = "productCodeLabel";
            productCodeLabel.Size = new System.Drawing.Size(75, 13);
            productCodeLabel.TabIndex = 58;
            productCodeLabel.Text = "Product Code:";
            // 
            // userCodeLabel
            // 
            userCodeLabel.AutoSize = true;
            userCodeLabel.Location = new System.Drawing.Point(14, 41);
            userCodeLabel.Name = "userCodeLabel";
            userCodeLabel.Size = new System.Drawing.Size(60, 13);
            userCodeLabel.TabIndex = 64;
            userCodeLabel.Text = "User Code:";
            // 
            // workPhoneNumberLabel
            // 
            workPhoneNumberLabel.AutoSize = true;
            workPhoneNumberLabel.Location = new System.Drawing.Point(14, 93);
            workPhoneNumberLabel.Name = "workPhoneNumberLabel";
            workPhoneNumberLabel.Size = new System.Drawing.Size(110, 13);
            workPhoneNumberLabel.TabIndex = 68;
            workPhoneNumberLabel.Text = "Work Phone Number:";
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(14, 203);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 74;
            beginDateLabel.Text = "Begin Date:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(281, 203);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 75;
            endDateLabel.Text = "End Date:";
            // 
            // beginChargeDateLabel
            // 
            beginChargeDateLabel.AutoSize = true;
            beginChargeDateLabel.Location = new System.Drawing.Point(14, 241);
            beginChargeDateLabel.Name = "beginChargeDateLabel";
            beginChargeDateLabel.Size = new System.Drawing.Size(100, 13);
            beginChargeDateLabel.TabIndex = 76;
            beginChargeDateLabel.Text = "Begin Charge Date:";
            // 
            // endChargeDateLabel
            // 
            endChargeDateLabel.AutoSize = true;
            endChargeDateLabel.Location = new System.Drawing.Point(281, 241);
            endChargeDateLabel.Name = "endChargeDateLabel";
            endChargeDateLabel.Size = new System.Drawing.Size(92, 13);
            endChargeDateLabel.TabIndex = 77;
            endChargeDateLabel.Text = "End Charge Date:";
            // 
            // companyTextBox
            // 
            this.companyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "Company", true));
            this.companyTextBox.Location = new System.Drawing.Point(142, 64);
            this.companyTextBox.Name = "companyTextBox";
            this.companyTextBox.Size = new System.Drawing.Size(200, 20);
            this.companyTextBox.TabIndex = 2;
            // 
            // fSListBindingSource
            // 
            this.fSListBindingSource.DataSource = typeof(AhamMetaDataDAL.FSList);
            // 
            // denyAccessCheckBox
            // 
            this.denyAccessCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.fSListBindingSource, "DenyAccess", true));
            this.denyAccessCheckBox.Location = new System.Drawing.Point(142, 274);
            this.denyAccessCheckBox.Name = "denyAccessCheckBox";
            this.denyAccessCheckBox.Size = new System.Drawing.Size(95, 24);
            this.denyAccessCheckBox.TabIndex = 11;
            this.denyAccessCheckBox.Text = "Deny access";
            this.denyAccessCheckBox.UseVisualStyleBackColor = true;
            // 
            // emailAddressTextBox
            // 
            this.emailAddressTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "EmailAddress", true));
            this.emailAddressTextBox.Location = new System.Drawing.Point(142, 116);
            this.emailAddressTextBox.Name = "emailAddressTextBox";
            this.emailAddressTextBox.Size = new System.Drawing.Size(200, 20);
            this.emailAddressTextBox.TabIndex = 4;
            // 
            // faxNumberTextBox
            // 
            this.faxNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "FaxNumber", true));
            this.faxNumberTextBox.Location = new System.Drawing.Point(142, 142);
            this.faxNumberTextBox.Name = "faxNumberTextBox";
            this.faxNumberTextBox.Size = new System.Drawing.Size(200, 20);
            this.faxNumberTextBox.TabIndex = 5;
            // 
            // fSCheckBox
            // 
            this.fSCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.fSListBindingSource, "FS", true));
            this.fSCheckBox.Location = new System.Drawing.Point(142, 304);
            this.fSCheckBox.Name = "fSCheckBox";
            this.fSCheckBox.Size = new System.Drawing.Size(53, 24);
            this.fSCheckBox.TabIndex = 12;
            this.fSCheckBox.Text = "FS";
            this.fSCheckBox.UseVisualStyleBackColor = true;
            // 
            // fSListKeyTextBox
            // 
            this.fSListKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "FSListKey", true));
            this.fSListKeyTextBox.Location = new System.Drawing.Point(509, 418);
            this.fSListKeyTextBox.Name = "fSListKeyTextBox";
            this.fSListKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.fSListKeyTextBox.TabIndex = 17;
            this.fSListKeyTextBox.TabStop = false;
            // 
            // fullNameTextBox
            // 
            this.fullNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "FullName", true));
            this.fullNameTextBox.Location = new System.Drawing.Point(142, 12);
            this.fullNameTextBox.Name = "fullNameTextBox";
            this.fullNameTextBox.Size = new System.Drawing.Size(200, 20);
            this.fullNameTextBox.TabIndex = 0;
            // 
            // productCodeTextBox
            // 
            this.productCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "ProductCode", true));
            this.productCodeTextBox.Location = new System.Drawing.Point(142, 168);
            this.productCodeTextBox.Name = "productCodeTextBox";
            this.productCodeTextBox.Size = new System.Drawing.Size(200, 20);
            this.productCodeTextBox.TabIndex = 6;
            // 
            // sendEmailCheckBox
            // 
            this.sendEmailCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.fSListBindingSource, "SendEmail", true));
            this.sendEmailCheckBox.Location = new System.Drawing.Point(142, 334);
            this.sendEmailCheckBox.Name = "sendEmailCheckBox";
            this.sendEmailCheckBox.Size = new System.Drawing.Size(98, 24);
            this.sendEmailCheckBox.TabIndex = 13;
            this.sendEmailCheckBox.Text = "Send email";
            this.sendEmailCheckBox.UseVisualStyleBackColor = true;
            // 
            // sendFaxCheckBox
            // 
            this.sendFaxCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.fSListBindingSource, "SendFax", true));
            this.sendFaxCheckBox.Location = new System.Drawing.Point(271, 334);
            this.sendFaxCheckBox.Name = "sendFaxCheckBox";
            this.sendFaxCheckBox.Size = new System.Drawing.Size(83, 24);
            this.sendFaxCheckBox.TabIndex = 15;
            this.sendFaxCheckBox.Text = "Send fax";
            this.sendFaxCheckBox.UseVisualStyleBackColor = true;
            // 
            // userCodeTextBox
            // 
            this.userCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "UserCode", true));
            this.userCodeTextBox.Location = new System.Drawing.Point(142, 38);
            this.userCodeTextBox.Name = "userCodeTextBox";
            this.userCodeTextBox.Size = new System.Drawing.Size(200, 20);
            this.userCodeTextBox.TabIndex = 1;
            // 
            // webCheckBox
            // 
            this.webCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.fSListBindingSource, "Web", true));
            this.webCheckBox.Location = new System.Drawing.Point(271, 304);
            this.webCheckBox.Name = "webCheckBox";
            this.webCheckBox.Size = new System.Drawing.Size(71, 24);
            this.webCheckBox.TabIndex = 14;
            this.webCheckBox.Text = "Web";
            this.webCheckBox.UseVisualStyleBackColor = true;
            // 
            // workPhoneNumberTextBox
            // 
            this.workPhoneNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "WorkPhoneNumber", true));
            this.workPhoneNumberTextBox.Location = new System.Drawing.Point(142, 90);
            this.workPhoneNumberTextBox.Name = "workPhoneNumberTextBox";
            this.workPhoneNumberTextBox.Size = new System.Drawing.Size(200, 20);
            this.workPhoneNumberTextBox.TabIndex = 3;
            // 
            // notesTextBox
            // 
            this.notesTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "Notes", true));
            this.notesTextBox.Location = new System.Drawing.Point(142, 374);
            this.notesTextBox.Name = "notesTextBox";
            this.notesTextBox.Size = new System.Drawing.Size(287, 20);
            this.notesTextBox.TabIndex = 16;
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(142, 203);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.beginDateTextBox.TabIndex = 7;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(409, 203);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.endDateTextBox.TabIndex = 8;
            // 
            // beginChargeDateTextBox
            // 
            this.beginChargeDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "BeginChargeDate", true));
            this.beginChargeDateTextBox.Location = new System.Drawing.Point(142, 238);
            this.beginChargeDateTextBox.Name = "beginChargeDateTextBox";
            this.beginChargeDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.beginChargeDateTextBox.TabIndex = 9;
            // 
            // endChargeDateTextBox
            // 
            this.endChargeDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.fSListBindingSource, "EndChargeDate", true));
            this.endChargeDateTextBox.Location = new System.Drawing.Point(409, 238);
            this.endChargeDateTextBox.Name = "endChargeDateTextBox";
            this.endChargeDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.endChargeDateTextBox.TabIndex = 10;
            // 
            // FSListEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 493);
            this.Controls.Add(endChargeDateLabel);
            this.Controls.Add(this.endChargeDateTextBox);
            this.Controls.Add(beginChargeDateLabel);
            this.Controls.Add(this.beginChargeDateTextBox);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(companyLabel);
            this.Controls.Add(this.companyTextBox);
            this.Controls.Add(this.notesTextBox);
            this.Controls.Add(this.denyAccessCheckBox);
            this.Controls.Add(emailAddressLabel);
            this.Controls.Add(this.emailAddressTextBox);
            this.Controls.Add(faxNumberLabel);
            this.Controls.Add(this.faxNumberTextBox);
            this.Controls.Add(this.fSCheckBox);
            this.Controls.Add(fullNameLabel);
            this.Controls.Add(fSListKeyLabel);
            this.Controls.Add(this.fSListKeyTextBox);
            this.Controls.Add(this.fullNameTextBox);
            this.Controls.Add(productCodeLabel);
            this.Controls.Add(this.productCodeTextBox);
            this.Controls.Add(this.sendEmailCheckBox);
            this.Controls.Add(notesLabel);
            this.Controls.Add(this.sendFaxCheckBox);
            this.Controls.Add(userCodeLabel);
            this.Controls.Add(this.userCodeTextBox);
            this.Controls.Add(this.webCheckBox);
            this.Controls.Add(workPhoneNumberLabel);
            this.Controls.Add(this.workPhoneNumberTextBox);
            this.Name = "FSListEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FS List";
            this.Controls.SetChildIndex(this.workPhoneNumberTextBox, 0);
            this.Controls.SetChildIndex(workPhoneNumberLabel, 0);
            this.Controls.SetChildIndex(this.webCheckBox, 0);
            this.Controls.SetChildIndex(this.userCodeTextBox, 0);
            this.Controls.SetChildIndex(userCodeLabel, 0);
            this.Controls.SetChildIndex(this.sendFaxCheckBox, 0);
            this.Controls.SetChildIndex(notesLabel, 0);
            this.Controls.SetChildIndex(this.sendEmailCheckBox, 0);
            this.Controls.SetChildIndex(this.productCodeTextBox, 0);
            this.Controls.SetChildIndex(productCodeLabel, 0);
            this.Controls.SetChildIndex(this.fullNameTextBox, 0);
            this.Controls.SetChildIndex(this.fSListKeyTextBox, 0);
            this.Controls.SetChildIndex(fSListKeyLabel, 0);
            this.Controls.SetChildIndex(fullNameLabel, 0);
            this.Controls.SetChildIndex(this.fSCheckBox, 0);
            this.Controls.SetChildIndex(this.faxNumberTextBox, 0);
            this.Controls.SetChildIndex(faxNumberLabel, 0);
            this.Controls.SetChildIndex(this.emailAddressTextBox, 0);
            this.Controls.SetChildIndex(emailAddressLabel, 0);
            this.Controls.SetChildIndex(this.denyAccessCheckBox, 0);
            this.Controls.SetChildIndex(this.notesTextBox, 0);
            this.Controls.SetChildIndex(this.companyTextBox, 0);
            this.Controls.SetChildIndex(companyLabel, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            this.Controls.SetChildIndex(this.beginChargeDateTextBox, 0);
            this.Controls.SetChildIndex(beginChargeDateLabel, 0);
            this.Controls.SetChildIndex(this.endChargeDateTextBox, 0);
            this.Controls.SetChildIndex(endChargeDateLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.fSListBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource fSListBindingSource;
        private System.Windows.Forms.TextBox companyTextBox;
        private System.Windows.Forms.CheckBox denyAccessCheckBox;
        private System.Windows.Forms.TextBox emailAddressTextBox;
        private System.Windows.Forms.TextBox faxNumberTextBox;
        private System.Windows.Forms.CheckBox fSCheckBox;
        private System.Windows.Forms.TextBox fSListKeyTextBox;
        private System.Windows.Forms.TextBox fullNameTextBox;
        private System.Windows.Forms.TextBox productCodeTextBox;
        private System.Windows.Forms.CheckBox sendEmailCheckBox;
        private System.Windows.Forms.CheckBox sendFaxCheckBox;
        private System.Windows.Forms.TextBox userCodeTextBox;
        private System.Windows.Forms.CheckBox webCheckBox;
        private System.Windows.Forms.TextBox workPhoneNumberTextBox;
        private System.Windows.Forms.TextBox notesTextBox;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.TextBox beginChargeDateTextBox;
        private System.Windows.Forms.TextBox endChargeDateTextBox;
    }
}