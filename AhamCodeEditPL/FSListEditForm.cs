﻿using System;
using System.Windows.Forms;

using System.Collections;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class FSListEditForm : HaiObjectEditForm
    {
        private FSList _fsListToEdit;

        public FSListEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _fsListToEdit = (FSList)_parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            fSListBindingSource.DataSource = _fsListToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            bool abort = false;

            if (!fullNameTextBox.ReadOnly && _fsListToEdit.FullName.Trim() == string.Empty)
            {
                MessageBox.Show("A full name must be supplied.", "Validation error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (!companyTextBox.ReadOnly && _fsListToEdit.Company.Trim() == string.Empty)
            {
                MessageBox.Show("A company must be supplied.", "Validation error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            // validate the charge date range
            if (!beginChargeDateTextBox.ReadOnly && !endChargeDateTextBox.ReadOnly)
            {
                message = _fsListToEdit.ChargeDateRange.ValidateRangeForType(DateRangeValidationType.Daily);
                if (message != string.Empty)
                {
                    MessageBox.Show("Charge date range validation failed.\n\r" + message);
                    abort = true;
                }
            }
            else
            {
                if (!beginChargeDateTextBox.ReadOnly)
                {
                    message = _fsListToEdit.ChargeDateRange.ValidateBeginDateForType(DateRangeValidationType.Daily);
                    if (message != string.Empty)
                    {
                        MessageBox.Show("Charge begin date validation failed.\n\r" + message);
                        abort = true;
                    }
                }

                if (!endChargeDateTextBox.ReadOnly)
                {
                    message = _fsListToEdit.ChargeDateRange.ValidateEndDateForType(DateRangeValidationType.Daily);
                    if (message != string.Empty)
                    {
                        MessageBox.Show("Charge end date validation failed.\n\r" + message);
                        abort = true;
                    }
                }
            }

            message = ValidateDaterange(_fsListToEdit, DateRangeValidationType.Daily);
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            // set the property values in the object that was the target of the edit
            MarkChangedProperties(_fsListToEdit);        // this is a multi-edit form
            _fsListToEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
