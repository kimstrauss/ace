﻿namespace AhamMetaDataPL
{
    partial class FootnoteEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label footnoteKeyLabel;
            System.Windows.Forms.Label footnoteTextLabel;
            this.footnoteKeyTextBox = new System.Windows.Forms.TextBox();
            this.footnoteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.footnoteTextTextBox = new System.Windows.Forms.TextBox();
            footnoteKeyLabel = new System.Windows.Forms.Label();
            footnoteTextLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.footnoteBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 4;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 2;
            // 
            // footnoteKeyLabel
            // 
            footnoteKeyLabel.AutoSize = true;
            footnoteKeyLabel.Location = new System.Drawing.Point(11, 85);
            footnoteKeyLabel.Name = "footnoteKeyLabel";
            footnoteKeyLabel.Size = new System.Drawing.Size(73, 13);
            footnoteKeyLabel.TabIndex = 30;
            footnoteKeyLabel.Text = "Footnote Key:";
            // 
            // footnoteTextLabel
            // 
            footnoteTextLabel.AutoSize = true;
            footnoteTextLabel.Location = new System.Drawing.Point(11, 15);
            footnoteTextLabel.Name = "footnoteTextLabel";
            footnoteTextLabel.Size = new System.Drawing.Size(76, 13);
            footnoteTextLabel.TabIndex = 32;
            footnoteTextLabel.Text = "Footnote Text:";
            // 
            // footnoteKeyTextBox
            // 
            this.footnoteKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.footnoteBindingSource, "FootnoteKey", true));
            this.footnoteKeyTextBox.Location = new System.Drawing.Point(93, 82);
            this.footnoteKeyTextBox.Name = "footnoteKeyTextBox";
            this.footnoteKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.footnoteKeyTextBox.TabIndex = 1;
            this.footnoteKeyTextBox.TabStop = false;
            // 
            // footnoteBindingSource
            // 
            this.footnoteBindingSource.DataSource = typeof(AhamMetaDataDAL.Footnote);
            // 
            // footnoteTextTextBox
            // 
            this.footnoteTextTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.footnoteBindingSource, "FootnoteText", true));
            this.footnoteTextTextBox.Location = new System.Drawing.Point(93, 12);
            this.footnoteTextTextBox.Multiline = true;
            this.footnoteTextTextBox.Name = "footnoteTextTextBox";
            this.footnoteTextTextBox.Size = new System.Drawing.Size(406, 64);
            this.footnoteTextTextBox.TabIndex = 0;
            // 
            // FootnoteEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(footnoteKeyLabel);
            this.Controls.Add(this.footnoteKeyTextBox);
            this.Controls.Add(footnoteTextLabel);
            this.Controls.Add(this.footnoteTextTextBox);
            this.Name = "FootnoteEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FootnoteEditForm";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.footnoteTextTextBox, 0);
            this.Controls.SetChildIndex(footnoteTextLabel, 0);
            this.Controls.SetChildIndex(this.footnoteKeyTextBox, 0);
            this.Controls.SetChildIndex(footnoteKeyLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.footnoteBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource footnoteBindingSource;
        private System.Windows.Forms.TextBox footnoteKeyTextBox;
        private System.Windows.Forms.TextBox footnoteTextTextBox;
    }
}