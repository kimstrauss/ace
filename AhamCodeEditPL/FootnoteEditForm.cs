﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class FootnoteEditForm : HaiObjectEditForm
    {
        private Footnote _footnoteToEdit;

        public FootnoteEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _footnoteToEdit = (Footnote)parameters.EditObject;

            footnoteBindingSource.DataSource = _footnoteToEdit;
            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (footnoteTextTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Text for the footnote is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_footnoteToEdit);
            _footnoteToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
