﻿namespace AhamMetaDataPL
{
    partial class GeographyEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label censusRegionNumberLabel;
            System.Windows.Forms.Label countyFIPSLabel;
            System.Windows.Forms.Label countyVertexLabel;
            System.Windows.Forms.Label geographyAbbreviationLabel;
            System.Windows.Forms.Label geographyIDLabel;
            System.Windows.Forms.Label geographyKeyLabel;
            System.Windows.Forms.Label geographyNameLabel;
            System.Windows.Forms.Label geographyNameSuffixLabel;
            System.Windows.Forms.Label geographySuffixLabel;
            System.Windows.Forms.Label nationISOLabel;
            System.Windows.Forms.Label regionIDLabel;
            System.Windows.Forms.Label stateFIPSLabel;
            System.Windows.Forms.Label stateVertexLabel;
            this.geographyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.censusRegionNumberTextBox = new System.Windows.Forms.TextBox();
            this.countyFIPSTextBox = new System.Windows.Forms.TextBox();
            this.countyVertexTextBox = new System.Windows.Forms.TextBox();
            this.geographyAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.geographyIDTextBox = new System.Windows.Forms.TextBox();
            this.geographyKeyTextBox = new System.Windows.Forms.TextBox();
            this.geographyNameTextBox = new System.Windows.Forms.TextBox();
            this.geographyNameSuffixTextBox = new System.Windows.Forms.TextBox();
            this.geographySuffixTextBox = new System.Windows.Forms.TextBox();
            this.nationISOTextBox = new System.Windows.Forms.TextBox();
            this.regionIDTextBox = new System.Windows.Forms.TextBox();
            this.stateFIPSTextBox = new System.Windows.Forms.TextBox();
            this.stateVertexTextBox = new System.Windows.Forms.TextBox();
            censusRegionNumberLabel = new System.Windows.Forms.Label();
            countyFIPSLabel = new System.Windows.Forms.Label();
            countyVertexLabel = new System.Windows.Forms.Label();
            geographyAbbreviationLabel = new System.Windows.Forms.Label();
            geographyIDLabel = new System.Windows.Forms.Label();
            geographyKeyLabel = new System.Windows.Forms.Label();
            geographyNameLabel = new System.Windows.Forms.Label();
            geographyNameSuffixLabel = new System.Windows.Forms.Label();
            geographySuffixLabel = new System.Windows.Forms.Label();
            nationISOLabel = new System.Windows.Forms.Label();
            regionIDLabel = new System.Windows.Forms.Label();
            stateFIPSLabel = new System.Windows.Forms.Label();
            stateVertexLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.geographyBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(581, 353);
            this.btnOK.TabIndex = 14;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(662, 353);
            this.btnCancel.TabIndex = 15;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 353);
            this.btnInsertNull.TabIndex = 13;
            // 
            // censusRegionNumberLabel
            // 
            censusRegionNumberLabel.AutoSize = true;
            censusRegionNumberLabel.Location = new System.Drawing.Point(12, 139);
            censusRegionNumberLabel.Name = "censusRegionNumberLabel";
            censusRegionNumberLabel.Size = new System.Drawing.Size(122, 13);
            censusRegionNumberLabel.TabIndex = 30;
            censusRegionNumberLabel.Text = "Census Region Number:";
            // 
            // countyFIPSLabel
            // 
            countyFIPSLabel.AutoSize = true;
            countyFIPSLabel.Location = new System.Drawing.Point(12, 113);
            countyFIPSLabel.Name = "countyFIPSLabel";
            countyFIPSLabel.Size = new System.Drawing.Size(69, 13);
            countyFIPSLabel.TabIndex = 32;
            countyFIPSLabel.Text = "County FIPS:";
            // 
            // countyVertexLabel
            // 
            countyVertexLabel.AutoSize = true;
            countyVertexLabel.Location = new System.Drawing.Point(12, 321);
            countyVertexLabel.Name = "countyVertexLabel";
            countyVertexLabel.Size = new System.Drawing.Size(76, 13);
            countyVertexLabel.TabIndex = 34;
            countyVertexLabel.Text = "County Vertex:";
            // 
            // geographyAbbreviationLabel
            // 
            geographyAbbreviationLabel.AutoSize = true;
            geographyAbbreviationLabel.Location = new System.Drawing.Point(12, 9);
            geographyAbbreviationLabel.Name = "geographyAbbreviationLabel";
            geographyAbbreviationLabel.Size = new System.Drawing.Size(124, 13);
            geographyAbbreviationLabel.TabIndex = 36;
            geographyAbbreviationLabel.Text = "Geography Abbreviation:";
            // 
            // geographyIDLabel
            // 
            geographyIDLabel.AutoSize = true;
            geographyIDLabel.Location = new System.Drawing.Point(12, 61);
            geographyIDLabel.Name = "geographyIDLabel";
            geographyIDLabel.Size = new System.Drawing.Size(76, 13);
            geographyIDLabel.TabIndex = 38;
            geographyIDLabel.Text = "Geography ID:";
            // 
            // geographyKeyLabel
            // 
            geographyKeyLabel.AutoSize = true;
            geographyKeyLabel.Location = new System.Drawing.Point(12, 269);
            geographyKeyLabel.Name = "geographyKeyLabel";
            geographyKeyLabel.Size = new System.Drawing.Size(83, 13);
            geographyKeyLabel.TabIndex = 40;
            geographyKeyLabel.Text = "Geography Key:";
            // 
            // geographyNameLabel
            // 
            geographyNameLabel.AutoSize = true;
            geographyNameLabel.Location = new System.Drawing.Point(12, 35);
            geographyNameLabel.Name = "geographyNameLabel";
            geographyNameLabel.Size = new System.Drawing.Size(93, 13);
            geographyNameLabel.TabIndex = 42;
            geographyNameLabel.Text = "Geography Name:";
            // 
            // geographyNameSuffixLabel
            // 
            geographyNameSuffixLabel.AutoSize = true;
            geographyNameSuffixLabel.Location = new System.Drawing.Point(12, 165);
            geographyNameSuffixLabel.Name = "geographyNameSuffixLabel";
            geographyNameSuffixLabel.Size = new System.Drawing.Size(122, 13);
            geographyNameSuffixLabel.TabIndex = 44;
            geographyNameSuffixLabel.Text = "Geography Name Suffix:";
            // 
            // geographySuffixLabel
            // 
            geographySuffixLabel.AutoSize = true;
            geographySuffixLabel.Location = new System.Drawing.Point(12, 191);
            geographySuffixLabel.Name = "geographySuffixLabel";
            geographySuffixLabel.Size = new System.Drawing.Size(91, 13);
            geographySuffixLabel.TabIndex = 46;
            geographySuffixLabel.Text = "Geography Suffix:";
            // 
            // nationISOLabel
            // 
            nationISOLabel.AutoSize = true;
            nationISOLabel.Location = new System.Drawing.Point(12, 217);
            nationISOLabel.Name = "nationISOLabel";
            nationISOLabel.Size = new System.Drawing.Size(62, 13);
            nationISOLabel.TabIndex = 48;
            nationISOLabel.Text = "Nation ISO:";
            // 
            // regionIDLabel
            // 
            regionIDLabel.AutoSize = true;
            regionIDLabel.Location = new System.Drawing.Point(12, 243);
            regionIDLabel.Name = "regionIDLabel";
            regionIDLabel.Size = new System.Drawing.Size(58, 13);
            regionIDLabel.TabIndex = 50;
            regionIDLabel.Text = "Region ID:";
            // 
            // stateFIPSLabel
            // 
            stateFIPSLabel.AutoSize = true;
            stateFIPSLabel.Location = new System.Drawing.Point(12, 87);
            stateFIPSLabel.Name = "stateFIPSLabel";
            stateFIPSLabel.Size = new System.Drawing.Size(61, 13);
            stateFIPSLabel.TabIndex = 52;
            stateFIPSLabel.Text = "State FIPS:";
            // 
            // stateVertexLabel
            // 
            stateVertexLabel.AutoSize = true;
            stateVertexLabel.Location = new System.Drawing.Point(12, 295);
            stateVertexLabel.Name = "stateVertexLabel";
            stateVertexLabel.Size = new System.Drawing.Size(68, 13);
            stateVertexLabel.TabIndex = 54;
            stateVertexLabel.Text = "State Vertex:";
            // 
            // geographyBindingSource
            // 
            this.geographyBindingSource.DataSource = typeof(AhamMetaDataDAL.Geography);
            // 
            // censusRegionNumberTextBox
            // 
            this.censusRegionNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "CensusRegionNumber", true));
            this.censusRegionNumberTextBox.Location = new System.Drawing.Point(142, 136);
            this.censusRegionNumberTextBox.Name = "censusRegionNumberTextBox";
            this.censusRegionNumberTextBox.Size = new System.Drawing.Size(100, 20);
            this.censusRegionNumberTextBox.TabIndex = 5;
            // 
            // countyFIPSTextBox
            // 
            this.countyFIPSTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "CountyFIPS", true));
            this.countyFIPSTextBox.Location = new System.Drawing.Point(142, 110);
            this.countyFIPSTextBox.Name = "countyFIPSTextBox";
            this.countyFIPSTextBox.Size = new System.Drawing.Size(100, 20);
            this.countyFIPSTextBox.TabIndex = 4;
            // 
            // countyVertexTextBox
            // 
            this.countyVertexTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "CountyVertex", true));
            this.countyVertexTextBox.Location = new System.Drawing.Point(142, 318);
            this.countyVertexTextBox.Name = "countyVertexTextBox";
            this.countyVertexTextBox.Size = new System.Drawing.Size(100, 20);
            this.countyVertexTextBox.TabIndex = 12;
            // 
            // geographyAbbreviationTextBox
            // 
            this.geographyAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "GeographyAbbreviation", true));
            this.geographyAbbreviationTextBox.Location = new System.Drawing.Point(142, 6);
            this.geographyAbbreviationTextBox.Name = "geographyAbbreviationTextBox";
            this.geographyAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.geographyAbbreviationTextBox.TabIndex = 0;
            // 
            // geographyIDTextBox
            // 
            this.geographyIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "GeographyID", true));
            this.geographyIDTextBox.Location = new System.Drawing.Point(142, 58);
            this.geographyIDTextBox.Name = "geographyIDTextBox";
            this.geographyIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.geographyIDTextBox.TabIndex = 2;
            // 
            // geographyKeyTextBox
            // 
            this.geographyKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "GeographyKey", true));
            this.geographyKeyTextBox.Location = new System.Drawing.Point(142, 266);
            this.geographyKeyTextBox.Name = "geographyKeyTextBox";
            this.geographyKeyTextBox.Size = new System.Drawing.Size(100, 20);
            this.geographyKeyTextBox.TabIndex = 10;
            // 
            // geographyNameTextBox
            // 
            this.geographyNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "GeographyName", true));
            this.geographyNameTextBox.Location = new System.Drawing.Point(142, 32);
            this.geographyNameTextBox.Name = "geographyNameTextBox";
            this.geographyNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.geographyNameTextBox.TabIndex = 1;
            // 
            // geographyNameSuffixTextBox
            // 
            this.geographyNameSuffixTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "GeographyNameSuffix", true));
            this.geographyNameSuffixTextBox.Location = new System.Drawing.Point(142, 162);
            this.geographyNameSuffixTextBox.Name = "geographyNameSuffixTextBox";
            this.geographyNameSuffixTextBox.Size = new System.Drawing.Size(600, 20);
            this.geographyNameSuffixTextBox.TabIndex = 6;
            // 
            // geographySuffixTextBox
            // 
            this.geographySuffixTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "GeographySuffix", true));
            this.geographySuffixTextBox.Location = new System.Drawing.Point(142, 188);
            this.geographySuffixTextBox.Name = "geographySuffixTextBox";
            this.geographySuffixTextBox.Size = new System.Drawing.Size(100, 20);
            this.geographySuffixTextBox.TabIndex = 7;
            // 
            // nationISOTextBox
            // 
            this.nationISOTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "NationISO", true));
            this.nationISOTextBox.Location = new System.Drawing.Point(142, 214);
            this.nationISOTextBox.Name = "nationISOTextBox";
            this.nationISOTextBox.Size = new System.Drawing.Size(100, 20);
            this.nationISOTextBox.TabIndex = 8;
            // 
            // regionIDTextBox
            // 
            this.regionIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "RegionID", true));
            this.regionIDTextBox.Location = new System.Drawing.Point(142, 240);
            this.regionIDTextBox.Name = "regionIDTextBox";
            this.regionIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.regionIDTextBox.TabIndex = 9;
            // 
            // stateFIPSTextBox
            // 
            this.stateFIPSTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "StateFIPS", true));
            this.stateFIPSTextBox.Location = new System.Drawing.Point(142, 84);
            this.stateFIPSTextBox.Name = "stateFIPSTextBox";
            this.stateFIPSTextBox.Size = new System.Drawing.Size(100, 20);
            this.stateFIPSTextBox.TabIndex = 3;
            // 
            // stateVertexTextBox
            // 
            this.stateVertexTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.geographyBindingSource, "StateVertex", true));
            this.stateVertexTextBox.Location = new System.Drawing.Point(142, 292);
            this.stateVertexTextBox.Name = "stateVertexTextBox";
            this.stateVertexTextBox.Size = new System.Drawing.Size(100, 20);
            this.stateVertexTextBox.TabIndex = 11;
            // 
            // GeographyEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 388);
            this.Controls.Add(censusRegionNumberLabel);
            this.Controls.Add(this.censusRegionNumberTextBox);
            this.Controls.Add(countyFIPSLabel);
            this.Controls.Add(this.countyFIPSTextBox);
            this.Controls.Add(countyVertexLabel);
            this.Controls.Add(this.countyVertexTextBox);
            this.Controls.Add(geographyAbbreviationLabel);
            this.Controls.Add(this.geographyAbbreviationTextBox);
            this.Controls.Add(geographyIDLabel);
            this.Controls.Add(this.geographyIDTextBox);
            this.Controls.Add(geographyKeyLabel);
            this.Controls.Add(this.geographyKeyTextBox);
            this.Controls.Add(geographyNameLabel);
            this.Controls.Add(this.geographyNameTextBox);
            this.Controls.Add(geographyNameSuffixLabel);
            this.Controls.Add(this.geographyNameSuffixTextBox);
            this.Controls.Add(geographySuffixLabel);
            this.Controls.Add(this.geographySuffixTextBox);
            this.Controls.Add(nationISOLabel);
            this.Controls.Add(this.nationISOTextBox);
            this.Controls.Add(regionIDLabel);
            this.Controls.Add(this.regionIDTextBox);
            this.Controls.Add(stateFIPSLabel);
            this.Controls.Add(this.stateFIPSTextBox);
            this.Controls.Add(stateVertexLabel);
            this.Controls.Add(this.stateVertexTextBox);
            this.Name = "GeographyEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Geography";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.stateVertexTextBox, 0);
            this.Controls.SetChildIndex(stateVertexLabel, 0);
            this.Controls.SetChildIndex(this.stateFIPSTextBox, 0);
            this.Controls.SetChildIndex(stateFIPSLabel, 0);
            this.Controls.SetChildIndex(this.regionIDTextBox, 0);
            this.Controls.SetChildIndex(regionIDLabel, 0);
            this.Controls.SetChildIndex(this.nationISOTextBox, 0);
            this.Controls.SetChildIndex(nationISOLabel, 0);
            this.Controls.SetChildIndex(this.geographySuffixTextBox, 0);
            this.Controls.SetChildIndex(geographySuffixLabel, 0);
            this.Controls.SetChildIndex(this.geographyNameSuffixTextBox, 0);
            this.Controls.SetChildIndex(geographyNameSuffixLabel, 0);
            this.Controls.SetChildIndex(this.geographyNameTextBox, 0);
            this.Controls.SetChildIndex(geographyNameLabel, 0);
            this.Controls.SetChildIndex(this.geographyKeyTextBox, 0);
            this.Controls.SetChildIndex(geographyKeyLabel, 0);
            this.Controls.SetChildIndex(this.geographyIDTextBox, 0);
            this.Controls.SetChildIndex(geographyIDLabel, 0);
            this.Controls.SetChildIndex(this.geographyAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(geographyAbbreviationLabel, 0);
            this.Controls.SetChildIndex(this.countyVertexTextBox, 0);
            this.Controls.SetChildIndex(countyVertexLabel, 0);
            this.Controls.SetChildIndex(this.countyFIPSTextBox, 0);
            this.Controls.SetChildIndex(countyFIPSLabel, 0);
            this.Controls.SetChildIndex(this.censusRegionNumberTextBox, 0);
            this.Controls.SetChildIndex(censusRegionNumberLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.geographyBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource geographyBindingSource;
        private System.Windows.Forms.TextBox censusRegionNumberTextBox;
        private System.Windows.Forms.TextBox countyFIPSTextBox;
        private System.Windows.Forms.TextBox countyVertexTextBox;
        private System.Windows.Forms.TextBox geographyAbbreviationTextBox;
        private System.Windows.Forms.TextBox geographyIDTextBox;
        private System.Windows.Forms.TextBox geographyKeyTextBox;
        private System.Windows.Forms.TextBox geographyNameTextBox;
        private System.Windows.Forms.TextBox geographyNameSuffixTextBox;
        private System.Windows.Forms.TextBox geographySuffixTextBox;
        private System.Windows.Forms.TextBox nationISOTextBox;
        private System.Windows.Forms.TextBox regionIDTextBox;
        private System.Windows.Forms.TextBox stateFIPSTextBox;
        private System.Windows.Forms.TextBox stateVertexTextBox;

    }
}