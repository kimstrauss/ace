﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class GeographyEditForm : HaiObjectEditForm
    {
        private Geography _geographyToEdit;
        ////private Geography _workingGeography;

        public GeographyEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _geographyToEdit = (Geography)parameters.EditObject;

            this.Load += new EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            geographyBindingSource.DataSource = _geographyToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (!geographyNameTextBox.ReadOnly && (geographyNameTextBox.Text.Trim() == string.Empty))
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (!geographyAbbreviationTextBox.ReadOnly && (geographyAbbreviationTextBox.Text.Trim() == string.Empty))
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (!geographyIDTextBox.ReadOnly && (geographyIDTextBox.Text.Trim() == string.Empty))
            {
                MessageBox.Show("An ID is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (!geographyNameSuffixTextBox.ReadOnly && (geographyNameSuffixTextBox.Text.Trim() == string.Empty))
            {
                MessageBox.Show("A geography name suffix is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_geographyToEdit);
            _geographyToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
