﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class GeographySetEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private GeographySet _geographySetToEdit;

        public GeographySetEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _geographySetToEdit = (GeographySet)parameters.EditObject;

            this.Load += new System.EventHandler(SetEditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void SetEditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);

            switch (_geographySetToEdit.GeographyLevel.ToUpper())
            {
                case "A":
                    rbAll.Checked = true;
                    break;
                case "N":
                    rbNation.Checked = true;
                    break;
                case "S":
                    rbState.Checked = true;
                    break;
                case "C":
                    rbCounty.Checked = true;
                    break;
                case "R":
                    rbRegion.Checked = true;
                    break;
                case "E":
                    rbExport.Checked = true;
                    break;
                default:
                    throw new Exception("Unknown GeographyLevel code: " + _geographySetToEdit.GeographyLevel);
            }

            geographySetBindingSource.DataSource = _geographySetToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (geographySetNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (geographySetAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            if (rbAll.Checked) _geographySetToEdit.GeographyLevel = "A";
            if (rbNation.Checked) _geographySetToEdit.GeographyLevel = "N";
            if (rbState.Checked) _geographySetToEdit.GeographyLevel = "S";
            if (rbCounty.Checked) _geographySetToEdit.GeographyLevel = "C";
            if (rbRegion.Checked) _geographySetToEdit.GeographyLevel = "R";
            if (rbExport.Checked) _geographySetToEdit.GeographyLevel = "E";

            MarkChangedProperties(_geographySetToEdit);
            _geographySetToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
