﻿namespace AhamMetaDataPL
{
    partial class GroupEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label councilKeyLabel;
            System.Windows.Forms.Label councilNameLabel;
            System.Windows.Forms.Label groupAbbreviationLabel;
            System.Windows.Forms.Label groupKeyLabel;
            System.Windows.Forms.Label groupNameLabel;
            System.Windows.Forms.Label previewRevisionReleaseDateLabel;
            System.Windows.Forms.Label revisionReleaseDateLabel;
            this.groupBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.councilKeyTextBox = new System.Windows.Forms.TextBox();
            this.councilNameComboBox = new System.Windows.Forms.ComboBox();
            this.groupAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.groupKeyTextBox = new System.Windows.Forms.TextBox();
            this.groupNameTextBox = new System.Windows.Forms.TextBox();
            this.previewRevisionReleaseDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.revisionReleaseDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            councilKeyLabel = new System.Windows.Forms.Label();
            councilNameLabel = new System.Windows.Forms.Label();
            groupAbbreviationLabel = new System.Windows.Forms.Label();
            groupKeyLabel = new System.Windows.Forms.Label();
            groupNameLabel = new System.Windows.Forms.Label();
            previewRevisionReleaseDateLabel = new System.Windows.Forms.Label();
            revisionReleaseDateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.groupBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(419, 181);
            this.btnOK.TabIndex = 7;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(500, 181);
            this.btnCancel.TabIndex = 8;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 6;
            // 
            // councilKeyLabel
            // 
            councilKeyLabel.AutoSize = true;
            councilKeyLabel.Location = new System.Drawing.Point(473, 61);
            councilKeyLabel.Name = "councilKeyLabel";
            councilKeyLabel.Size = new System.Drawing.Size(28, 13);
            councilKeyLabel.TabIndex = 30;
            councilKeyLabel.Text = "Key:";
            // 
            // councilNameLabel
            // 
            councilNameLabel.AutoSize = true;
            councilNameLabel.Location = new System.Drawing.Point(12, 61);
            councilNameLabel.Name = "councilNameLabel";
            councilNameLabel.Size = new System.Drawing.Size(76, 13);
            councilNameLabel.TabIndex = 32;
            councilNameLabel.Text = "Council Name:";
            // 
            // groupAbbreviationLabel
            // 
            groupAbbreviationLabel.AutoSize = true;
            groupAbbreviationLabel.Location = new System.Drawing.Point(12, 9);
            groupAbbreviationLabel.Name = "groupAbbreviationLabel";
            groupAbbreviationLabel.Size = new System.Drawing.Size(101, 13);
            groupAbbreviationLabel.TabIndex = 34;
            groupAbbreviationLabel.Text = "Group Abbreviation:";
            // 
            // groupKeyLabel
            // 
            groupKeyLabel.AutoSize = true;
            groupKeyLabel.Location = new System.Drawing.Point(12, 140);
            groupKeyLabel.Name = "groupKeyLabel";
            groupKeyLabel.Size = new System.Drawing.Size(60, 13);
            groupKeyLabel.TabIndex = 36;
            groupKeyLabel.Text = "Group Key:";
            // 
            // groupNameLabel
            // 
            groupNameLabel.AutoSize = true;
            groupNameLabel.Location = new System.Drawing.Point(12, 35);
            groupNameLabel.Name = "groupNameLabel";
            groupNameLabel.Size = new System.Drawing.Size(70, 13);
            groupNameLabel.TabIndex = 38;
            groupNameLabel.Text = "Group Name:";
            // 
            // previewRevisionReleaseDateLabel
            // 
            previewRevisionReleaseDateLabel.AutoSize = true;
            previewRevisionReleaseDateLabel.Location = new System.Drawing.Point(12, 115);
            previewRevisionReleaseDateLabel.Name = "previewRevisionReleaseDateLabel";
            previewRevisionReleaseDateLabel.Size = new System.Drawing.Size(160, 13);
            previewRevisionReleaseDateLabel.TabIndex = 40;
            previewRevisionReleaseDateLabel.Text = "Preview Revision Release Date:";
            // 
            // revisionReleaseDateLabel
            // 
            revisionReleaseDateLabel.AutoSize = true;
            revisionReleaseDateLabel.Location = new System.Drawing.Point(12, 89);
            revisionReleaseDateLabel.Name = "revisionReleaseDateLabel";
            revisionReleaseDateLabel.Size = new System.Drawing.Size(119, 13);
            revisionReleaseDateLabel.TabIndex = 42;
            revisionReleaseDateLabel.Text = "Revision Release Date:";
            // 
            // groupBindingSource
            // 
            this.groupBindingSource.DataSource = typeof(AhamMetaDataDAL.Group);
            // 
            // councilKeyTextBox
            // 
            this.councilKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.groupBindingSource, "CouncilKey", true));
            this.councilKeyTextBox.Location = new System.Drawing.Point(507, 58);
            this.councilKeyTextBox.Name = "councilKeyTextBox";
            this.councilKeyTextBox.Size = new System.Drawing.Size(31, 20);
            this.councilKeyTextBox.TabIndex = 31;
            this.councilKeyTextBox.TabStop = false;
            // 
            // councilNameComboBox
            // 
            this.councilNameComboBox.FormattingEnabled = true;
            this.councilNameComboBox.Location = new System.Drawing.Point(178, 58);
            this.councilNameComboBox.Name = "councilNameComboBox";
            this.councilNameComboBox.Size = new System.Drawing.Size(289, 21);
            this.councilNameComboBox.TabIndex = 2;
            // 
            // groupAbbreviationTextBox
            // 
            this.groupAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.groupBindingSource, "GroupAbbreviation", true));
            this.groupAbbreviationTextBox.Location = new System.Drawing.Point(178, 6);
            this.groupAbbreviationTextBox.Name = "groupAbbreviationTextBox";
            this.groupAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.groupAbbreviationTextBox.TabIndex = 0;
            // 
            // groupKeyTextBox
            // 
            this.groupKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.groupBindingSource, "GroupKey", true));
            this.groupKeyTextBox.Location = new System.Drawing.Point(178, 137);
            this.groupKeyTextBox.Name = "groupKeyTextBox";
            this.groupKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.groupKeyTextBox.TabIndex = 5;
            this.groupKeyTextBox.TabStop = false;
            // 
            // groupNameTextBox
            // 
            this.groupNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.groupBindingSource, "GroupName", true));
            this.groupNameTextBox.Location = new System.Drawing.Point(178, 32);
            this.groupNameTextBox.Name = "groupNameTextBox";
            this.groupNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.groupNameTextBox.TabIndex = 1;
            // 
            // previewRevisionReleaseDateDateTimePicker
            // 
            this.previewRevisionReleaseDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.groupBindingSource, "PreviewRevisionReleaseDate", true));
            this.previewRevisionReleaseDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.previewRevisionReleaseDateDateTimePicker.Location = new System.Drawing.Point(178, 111);
            this.previewRevisionReleaseDateDateTimePicker.Name = "previewRevisionReleaseDateDateTimePicker";
            this.previewRevisionReleaseDateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.previewRevisionReleaseDateDateTimePicker.TabIndex = 4;
            // 
            // revisionReleaseDateDateTimePicker
            // 
            this.revisionReleaseDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.groupBindingSource, "RevisionReleaseDate", true));
            this.revisionReleaseDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.revisionReleaseDateDateTimePicker.Location = new System.Drawing.Point(178, 85);
            this.revisionReleaseDateDateTimePicker.Name = "revisionReleaseDateDateTimePicker";
            this.revisionReleaseDateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.revisionReleaseDateDateTimePicker.TabIndex = 3;
            // 
            // GroupEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 216);
            this.Controls.Add(councilKeyLabel);
            this.Controls.Add(this.councilKeyTextBox);
            this.Controls.Add(councilNameLabel);
            this.Controls.Add(this.councilNameComboBox);
            this.Controls.Add(groupAbbreviationLabel);
            this.Controls.Add(this.groupAbbreviationTextBox);
            this.Controls.Add(groupKeyLabel);
            this.Controls.Add(this.groupKeyTextBox);
            this.Controls.Add(groupNameLabel);
            this.Controls.Add(this.groupNameTextBox);
            this.Controls.Add(previewRevisionReleaseDateLabel);
            this.Controls.Add(this.previewRevisionReleaseDateDateTimePicker);
            this.Controls.Add(revisionReleaseDateLabel);
            this.Controls.Add(this.revisionReleaseDateDateTimePicker);
            this.Name = "GroupEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Group";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.revisionReleaseDateDateTimePicker, 0);
            this.Controls.SetChildIndex(revisionReleaseDateLabel, 0);
            this.Controls.SetChildIndex(this.previewRevisionReleaseDateDateTimePicker, 0);
            this.Controls.SetChildIndex(previewRevisionReleaseDateLabel, 0);
            this.Controls.SetChildIndex(this.groupNameTextBox, 0);
            this.Controls.SetChildIndex(groupNameLabel, 0);
            this.Controls.SetChildIndex(this.groupKeyTextBox, 0);
            this.Controls.SetChildIndex(groupKeyLabel, 0);
            this.Controls.SetChildIndex(this.groupAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(groupAbbreviationLabel, 0);
            this.Controls.SetChildIndex(this.councilNameComboBox, 0);
            this.Controls.SetChildIndex(councilNameLabel, 0);
            this.Controls.SetChildIndex(this.councilKeyTextBox, 0);
            this.Controls.SetChildIndex(councilKeyLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.groupBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource groupBindingSource;
        private System.Windows.Forms.TextBox councilKeyTextBox;
        private System.Windows.Forms.ComboBox councilNameComboBox;
        private System.Windows.Forms.TextBox groupAbbreviationTextBox;
        private System.Windows.Forms.TextBox groupKeyTextBox;
        private System.Windows.Forms.TextBox groupNameTextBox;
        private System.Windows.Forms.DateTimePicker previewRevisionReleaseDateDateTimePicker;
        private System.Windows.Forms.DateTimePicker revisionReleaseDateDateTimePicker;
    }
}