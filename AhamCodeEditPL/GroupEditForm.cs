﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using System.Configuration;
using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class GroupEditForm : HaiObjectEditForm
    {
        private Group _groupToEdit;

        public GroupEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _groupToEdit = (Group)parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);    // establish handler for Load Event
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);
            groupBindingSource.DataSource = _groupToEdit;

            previewRevisionReleaseDateDateTimePicker.MinDate = DateTime.Parse(ConfigurationManager.AppSettings["EarliestBeginDate"]);
            previewRevisionReleaseDateDateTimePicker.MaxDate = DateTime.Parse(ConfigurationManager.AppSettings["LatestEndDate"]);
            revisionReleaseDateDateTimePicker.MinDate = DateTime.Parse(ConfigurationManager.AppSettings["EarliestBeginDate"]);
            revisionReleaseDateDateTimePicker.MaxDate = DateTime.Parse(ConfigurationManager.AppSettings["LatestEndDate"]);
            
            // assign ComboBox items source to the ComboBox.Tag
            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result = ds.GetDataList(HaiBusinessObjectType.Council);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Council>(councilNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "CouncilName", "CouncilKey", councilKeyTextBox, _groupToEdit.CouncilName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit group because council fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (groupNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (groupAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (councilNameComboBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A council must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_groupToEdit);
            _groupToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
