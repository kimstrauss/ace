﻿namespace AhamMetaDataPL
{
    partial class HolidayCADateEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label dateOfHolidayLabel;
            System.Windows.Forms.Label holidayDateKeyLabel;
            System.Windows.Forms.Label nameLabel;
            this.dateOfHolidayDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.holidayDateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.holidayDateKeyTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            dateOfHolidayLabel = new System.Windows.Forms.Label();
            holidayDateKeyLabel = new System.Windows.Forms.Label();
            nameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.holidayDateBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // dateOfHolidayLabel
            // 
            dateOfHolidayLabel.AutoSize = true;
            dateOfHolidayLabel.Location = new System.Drawing.Point(12, 18);
            dateOfHolidayLabel.Name = "dateOfHolidayLabel";
            dateOfHolidayLabel.Size = new System.Drawing.Size(85, 13);
            dateOfHolidayLabel.TabIndex = 30;
            dateOfHolidayLabel.Text = "Date Of Holiday:";
            // 
            // holidayDateKeyLabel
            // 
            holidayDateKeyLabel.AutoSize = true;
            holidayDateKeyLabel.Location = new System.Drawing.Point(5, 69);
            holidayDateKeyLabel.Name = "holidayDateKeyLabel";
            holidayDateKeyLabel.Size = new System.Drawing.Size(92, 13);
            holidayDateKeyLabel.TabIndex = 31;
            holidayDateKeyLabel.Text = "Holiday Date Key:";
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(59, 43);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(38, 13);
            nameLabel.TabIndex = 32;
            nameLabel.Text = "Name:";
            // 
            // dateOfHolidayDateTimePicker
            // 
            this.dateOfHolidayDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.holidayDateBindingSource, "DateOfHoliday", true));
            this.dateOfHolidayDateTimePicker.Location = new System.Drawing.Point(103, 14);
            this.dateOfHolidayDateTimePicker.Name = "dateOfHolidayDateTimePicker";
            this.dateOfHolidayDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateOfHolidayDateTimePicker.TabIndex = 0;
            // 
            // holidayDateBindingSource
            // 
            this.holidayDateBindingSource.DataSource = typeof(AhamMetaDataDAL.HolidayDate);
            // 
            // holidayDateKeyTextBox
            // 
            this.holidayDateKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.holidayDateBindingSource, "HolidayDateKey", true));
            this.holidayDateKeyTextBox.Location = new System.Drawing.Point(103, 66);
            this.holidayDateKeyTextBox.Name = "holidayDateKeyTextBox";
            this.holidayDateKeyTextBox.Size = new System.Drawing.Size(100, 20);
            this.holidayDateKeyTextBox.TabIndex = 2;
            this.holidayDateKeyTextBox.TabStop = false;
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.holidayDateBindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(103, 40);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(400, 20);
            this.nameTextBox.TabIndex = 1;
            // 
            // HolidayDateEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(nameLabel);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(holidayDateKeyLabel);
            this.Controls.Add(this.holidayDateKeyTextBox);
            this.Controls.Add(dateOfHolidayLabel);
            this.Controls.Add(this.dateOfHolidayDateTimePicker);
            this.Name = "HolidayCADateEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "HolidayCADateEditForm";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.dateOfHolidayDateTimePicker, 0);
            this.Controls.SetChildIndex(dateOfHolidayLabel, 0);
            this.Controls.SetChildIndex(this.holidayDateKeyTextBox, 0);
            this.Controls.SetChildIndex(holidayDateKeyLabel, 0);
            this.Controls.SetChildIndex(this.nameTextBox, 0);
            this.Controls.SetChildIndex(nameLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.holidayDateBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource holidayDateBindingSource;
        private System.Windows.Forms.DateTimePicker dateOfHolidayDateTimePicker;
        private System.Windows.Forms.TextBox holidayDateKeyTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
    }
}