﻿using System;
using System.Windows.Forms;

using System.Configuration;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class HolidayCADateEditForm : HaiObjectEditForm
    {
        private HolidayCADate _holidayCAToEdit;

        public HolidayCADateEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            dateOfHolidayDateTimePicker.MinDate = DateTime.Parse(ConfigurationManager.AppSettings["EarliestBeginDate"]);
            dateOfHolidayDateTimePicker.MaxDate = DateTime.Parse(ConfigurationManager.AppSettings["LatestEndDate"]);

            _holidayCAToEdit = (HolidayCADate)parameters.EditObject;

            holidayDateBindingSource.DataSource = _holidayCAToEdit;
            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            dateOfHolidayDateTimePicker.DataBindings["Value"].WriteValue();
            _holidayCAToEdit.DateOfHoliday = _holidayCAToEdit.DateOfHoliday.Date;       // force the time to be dropped.

            MarkChangedProperties(_holidayCAToEdit);
            _holidayCAToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
