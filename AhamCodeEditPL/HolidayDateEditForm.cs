﻿using System;
using System.Windows.Forms;

using System.Configuration;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class HolidayDateEditForm : HaiObjectEditForm
    {
        private HolidayDate _holidayToEdit;

        public HolidayDateEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            dateOfHolidayDateTimePicker.MinDate = DateTime.Parse(ConfigurationManager.AppSettings["EarliestBeginDate"]);
            dateOfHolidayDateTimePicker.MaxDate = DateTime.Parse(ConfigurationManager.AppSettings["LatestEndDate"]);

            _holidayToEdit = (HolidayDate)parameters.EditObject;

            holidayDateBindingSource.DataSource = _holidayToEdit;
            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            dateOfHolidayDateTimePicker.DataBindings["Value"].WriteValue();
            _holidayToEdit.DateOfHoliday = _holidayToEdit.DateOfHoliday.Date;       // force the time to be dropped.

            MarkChangedProperties(_holidayToEdit);
            _holidayToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
