﻿namespace AhamMetaDataPL
{
    partial class InputColumnEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label labelKey;
            System.Windows.Forms.Label activityNameLabel;
            System.Windows.Forms.Label catalogNameLabel;
            System.Windows.Forms.Label channelNameLabel;
            System.Windows.Forms.Label columnHeaderLabel;
            System.Windows.Forms.Label geographyNameLabel;
            System.Windows.Forms.Label inputColumnKeyLabel;
            System.Windows.Forms.Label inputFormNameLabel;
            System.Windows.Forms.Label marketNameLabel;
            System.Windows.Forms.Label measureNameLabel;
            System.Windows.Forms.Label size1NameLabel;
            System.Windows.Forms.Label size2NameLabel;
            System.Windows.Forms.Label size3NameLabel;
            System.Windows.Forms.Label size4NameLabel;
            System.Windows.Forms.Label size5NameLabel;
            System.Windows.Forms.Label sortKeyLabel;
            System.Windows.Forms.Label targetYearLabel;
            System.Windows.Forms.Label useNameLabel;
            System.Windows.Forms.Label columnHeader2Label;
            this.activityKeyTextBox = new System.Windows.Forms.TextBox();
            this.activityNameComboBox = new System.Windows.Forms.ComboBox();
            this.catalogKeyTextBox = new System.Windows.Forms.TextBox();
            this.catalogNameComboBox = new System.Windows.Forms.ComboBox();
            this.channelKeyTextBox = new System.Windows.Forms.TextBox();
            this.channelNameComboBox = new System.Windows.Forms.ComboBox();
            this.columnHeaderTextBox = new System.Windows.Forms.TextBox();
            this.geographyKeyTextBox = new System.Windows.Forms.TextBox();
            this.geographyNameComboBox = new System.Windows.Forms.ComboBox();
            this.inputColumnKeyTextBox = new System.Windows.Forms.TextBox();
            this.inputFormKeyTextBox = new System.Windows.Forms.TextBox();
            this.inputFormNameComboBox = new System.Windows.Forms.ComboBox();
            this.marketKeyTextBox = new System.Windows.Forms.TextBox();
            this.marketNameComboBox = new System.Windows.Forms.ComboBox();
            this.measureKeyTextBox = new System.Windows.Forms.TextBox();
            this.measureNameComboBox = new System.Windows.Forms.ComboBox();
            this.size1KeyTextBox = new System.Windows.Forms.TextBox();
            this.size1NameComboBox = new System.Windows.Forms.ComboBox();
            this.size2KeyTextBox = new System.Windows.Forms.TextBox();
            this.size2NameComboBox = new System.Windows.Forms.ComboBox();
            this.size3KeyTextBox = new System.Windows.Forms.TextBox();
            this.size3NameComboBox = new System.Windows.Forms.ComboBox();
            this.size4KeyTextBox = new System.Windows.Forms.TextBox();
            this.size4NameComboBox = new System.Windows.Forms.ComboBox();
            this.size5KeyTextBox = new System.Windows.Forms.TextBox();
            this.size5NameComboBox = new System.Windows.Forms.ComboBox();
            this.sortKeyTextBox = new System.Windows.Forms.TextBox();
            this.targetYearTextBox = new System.Windows.Forms.TextBox();
            this.useKeyTextBox = new System.Windows.Forms.TextBox();
            this.useNameComboBox = new System.Windows.Forms.ComboBox();
            this.columnHeader2TextBox = new System.Windows.Forms.TextBox();
            this.inputColumnBindingSource = new System.Windows.Forms.BindingSource(this.components);
            labelKey = new System.Windows.Forms.Label();
            activityNameLabel = new System.Windows.Forms.Label();
            catalogNameLabel = new System.Windows.Forms.Label();
            channelNameLabel = new System.Windows.Forms.Label();
            columnHeaderLabel = new System.Windows.Forms.Label();
            geographyNameLabel = new System.Windows.Forms.Label();
            inputColumnKeyLabel = new System.Windows.Forms.Label();
            inputFormNameLabel = new System.Windows.Forms.Label();
            marketNameLabel = new System.Windows.Forms.Label();
            measureNameLabel = new System.Windows.Forms.Label();
            size1NameLabel = new System.Windows.Forms.Label();
            size2NameLabel = new System.Windows.Forms.Label();
            size3NameLabel = new System.Windows.Forms.Label();
            size4NameLabel = new System.Windows.Forms.Label();
            size5NameLabel = new System.Windows.Forms.Label();
            sortKeyLabel = new System.Windows.Forms.Label();
            targetYearLabel = new System.Windows.Forms.Label();
            useNameLabel = new System.Windows.Forms.Label();
            columnHeader2Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.inputColumnBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(349, 553);
            this.btnOK.TabIndex = 17;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(430, 553);
            this.btnCancel.TabIndex = 18;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 553);
            this.btnInsertNull.TabIndex = 16;
            // 
            // labelKey
            // 
            labelKey.AutoSize = true;
            labelKey.Location = new System.Drawing.Point(462, 8);
            labelKey.Name = "labelKey";
            labelKey.Size = new System.Drawing.Size(25, 13);
            labelKey.TabIndex = 29;
            labelKey.Text = "Key";
            // 
            // activityNameLabel
            // 
            activityNameLabel.AutoSize = true;
            activityNameLabel.Location = new System.Drawing.Point(12, 61);
            activityNameLabel.Name = "activityNameLabel";
            activityNameLabel.Size = new System.Drawing.Size(44, 13);
            activityNameLabel.TabIndex = 31;
            activityNameLabel.Text = "Activity:";
            // 
            // catalogNameLabel
            // 
            catalogNameLabel.AutoSize = true;
            catalogNameLabel.Location = new System.Drawing.Point(12, 88);
            catalogNameLabel.Name = "catalogNameLabel";
            catalogNameLabel.Size = new System.Drawing.Size(46, 13);
            catalogNameLabel.TabIndex = 35;
            catalogNameLabel.Text = "Catalog:";
            // 
            // channelNameLabel
            // 
            channelNameLabel.AutoSize = true;
            channelNameLabel.Location = new System.Drawing.Point(12, 115);
            channelNameLabel.Name = "channelNameLabel";
            channelNameLabel.Size = new System.Drawing.Size(49, 13);
            channelNameLabel.TabIndex = 39;
            channelNameLabel.Text = "Channel:";
            // 
            // columnHeaderLabel
            // 
            columnHeaderLabel.AutoSize = true;
            columnHeaderLabel.Location = new System.Drawing.Point(12, 385);
            columnHeaderLabel.Name = "columnHeaderLabel";
            columnHeaderLabel.Size = new System.Drawing.Size(83, 13);
            columnHeaderLabel.TabIndex = 41;
            columnHeaderLabel.Text = "Column Header:";
            // 
            // geographyNameLabel
            // 
            geographyNameLabel.AutoSize = true;
            geographyNameLabel.Location = new System.Drawing.Point(12, 142);
            geographyNameLabel.Name = "geographyNameLabel";
            geographyNameLabel.Size = new System.Drawing.Size(62, 13);
            geographyNameLabel.TabIndex = 45;
            geographyNameLabel.Text = "Geography:";
            // 
            // inputColumnKeyLabel
            // 
            inputColumnKeyLabel.AutoSize = true;
            inputColumnKeyLabel.Location = new System.Drawing.Point(361, 515);
            inputColumnKeyLabel.Name = "inputColumnKeyLabel";
            inputColumnKeyLabel.Size = new System.Drawing.Size(93, 13);
            inputColumnKeyLabel.TabIndex = 47;
            inputColumnKeyLabel.Text = "Input Column Key:";
            // 
            // inputFormNameLabel
            // 
            inputFormNameLabel.AutoSize = true;
            inputFormNameLabel.Location = new System.Drawing.Point(12, 34);
            inputFormNameLabel.Name = "inputFormNameLabel";
            inputFormNameLabel.Size = new System.Drawing.Size(60, 13);
            inputFormNameLabel.TabIndex = 51;
            inputFormNameLabel.Text = "Input Form:";
            // 
            // marketNameLabel
            // 
            marketNameLabel.AutoSize = true;
            marketNameLabel.Location = new System.Drawing.Point(12, 169);
            marketNameLabel.Name = "marketNameLabel";
            marketNameLabel.Size = new System.Drawing.Size(43, 13);
            marketNameLabel.TabIndex = 55;
            marketNameLabel.Text = "Market:";
            // 
            // measureNameLabel
            // 
            measureNameLabel.AutoSize = true;
            measureNameLabel.Location = new System.Drawing.Point(12, 196);
            measureNameLabel.Name = "measureNameLabel";
            measureNameLabel.Size = new System.Drawing.Size(51, 13);
            measureNameLabel.TabIndex = 59;
            measureNameLabel.Text = "Measure:";
            // 
            // size1NameLabel
            // 
            size1NameLabel.AutoSize = true;
            size1NameLabel.Location = new System.Drawing.Point(12, 250);
            size1NameLabel.Name = "size1NameLabel";
            size1NameLabel.Size = new System.Drawing.Size(36, 13);
            size1NameLabel.TabIndex = 63;
            size1NameLabel.Text = "Size1:";
            // 
            // size2NameLabel
            // 
            size2NameLabel.AutoSize = true;
            size2NameLabel.Location = new System.Drawing.Point(12, 277);
            size2NameLabel.Name = "size2NameLabel";
            size2NameLabel.Size = new System.Drawing.Size(36, 13);
            size2NameLabel.TabIndex = 67;
            size2NameLabel.Text = "Size2:";
            // 
            // size3NameLabel
            // 
            size3NameLabel.AutoSize = true;
            size3NameLabel.Location = new System.Drawing.Point(12, 304);
            size3NameLabel.Name = "size3NameLabel";
            size3NameLabel.Size = new System.Drawing.Size(36, 13);
            size3NameLabel.TabIndex = 71;
            size3NameLabel.Text = "Size3:";
            // 
            // size4NameLabel
            // 
            size4NameLabel.AutoSize = true;
            size4NameLabel.Location = new System.Drawing.Point(12, 331);
            size4NameLabel.Name = "size4NameLabel";
            size4NameLabel.Size = new System.Drawing.Size(36, 13);
            size4NameLabel.TabIndex = 75;
            size4NameLabel.Text = "Size4:";
            // 
            // size5NameLabel
            // 
            size5NameLabel.AutoSize = true;
            size5NameLabel.Location = new System.Drawing.Point(12, 358);
            size5NameLabel.Name = "size5NameLabel";
            size5NameLabel.Size = new System.Drawing.Size(36, 13);
            size5NameLabel.TabIndex = 79;
            size5NameLabel.Text = "Size5:";
            // 
            // sortKeyLabel
            // 
            sortKeyLabel.AutoSize = true;
            sortKeyLabel.Location = new System.Drawing.Point(246, 449);
            sortKeyLabel.Name = "sortKeyLabel";
            sortKeyLabel.Size = new System.Drawing.Size(50, 13);
            sortKeyLabel.TabIndex = 81;
            sortKeyLabel.Text = "Sort Key:";
            // 
            // targetYearLabel
            // 
            targetYearLabel.AutoSize = true;
            targetYearLabel.Location = new System.Drawing.Point(96, 449);
            targetYearLabel.Name = "targetYearLabel";
            targetYearLabel.Size = new System.Drawing.Size(66, 13);
            targetYearLabel.TabIndex = 83;
            targetYearLabel.Text = "Target Year:";
            // 
            // useNameLabel
            // 
            useNameLabel.AutoSize = true;
            useNameLabel.Location = new System.Drawing.Point(12, 223);
            useNameLabel.Name = "useNameLabel";
            useNameLabel.Size = new System.Drawing.Size(29, 13);
            useNameLabel.TabIndex = 87;
            useNameLabel.Text = "Use:";
            // 
            // activityKeyTextBox
            // 
            this.activityKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "ActivityKey", true));
            this.activityKeyTextBox.Location = new System.Drawing.Point(460, 59);
            this.activityKeyTextBox.Name = "activityKeyTextBox";
            this.activityKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.activityKeyTextBox.TabIndex = 30;
            this.activityKeyTextBox.TabStop = false;
            // 
            // activityNameComboBox
            // 
            this.activityNameComboBox.FormattingEnabled = true;
            this.activityNameComboBox.Location = new System.Drawing.Point(99, 58);
            this.activityNameComboBox.Name = "activityNameComboBox";
            this.activityNameComboBox.Size = new System.Drawing.Size(338, 21);
            this.activityNameComboBox.TabIndex = 1;
            // 
            // catalogKeyTextBox
            // 
            this.catalogKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "CatalogKey", true));
            this.catalogKeyTextBox.Location = new System.Drawing.Point(460, 86);
            this.catalogKeyTextBox.Name = "catalogKeyTextBox";
            this.catalogKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.catalogKeyTextBox.TabIndex = 34;
            this.catalogKeyTextBox.TabStop = false;
            // 
            // catalogNameComboBox
            // 
            this.catalogNameComboBox.FormattingEnabled = true;
            this.catalogNameComboBox.Location = new System.Drawing.Point(99, 85);
            this.catalogNameComboBox.Name = "catalogNameComboBox";
            this.catalogNameComboBox.Size = new System.Drawing.Size(338, 21);
            this.catalogNameComboBox.TabIndex = 2;
            // 
            // channelKeyTextBox
            // 
            this.channelKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "ChannelKey", true));
            this.channelKeyTextBox.Location = new System.Drawing.Point(460, 113);
            this.channelKeyTextBox.Name = "channelKeyTextBox";
            this.channelKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.channelKeyTextBox.TabIndex = 38;
            this.channelKeyTextBox.TabStop = false;
            // 
            // channelNameComboBox
            // 
            this.channelNameComboBox.FormattingEnabled = true;
            this.channelNameComboBox.Location = new System.Drawing.Point(99, 112);
            this.channelNameComboBox.Name = "channelNameComboBox";
            this.channelNameComboBox.Size = new System.Drawing.Size(338, 21);
            this.channelNameComboBox.TabIndex = 3;
            // 
            // columnHeaderTextBox
            // 
            this.columnHeaderTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "ColumnHeader", true));
            this.columnHeaderTextBox.Location = new System.Drawing.Point(99, 382);
            this.columnHeaderTextBox.Name = "columnHeaderTextBox";
            this.columnHeaderTextBox.Size = new System.Drawing.Size(338, 20);
            this.columnHeaderTextBox.TabIndex = 13;
            // 
            // geographyKeyTextBox
            // 
            this.geographyKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "GeographyKey", true));
            this.geographyKeyTextBox.Location = new System.Drawing.Point(460, 140);
            this.geographyKeyTextBox.Name = "geographyKeyTextBox";
            this.geographyKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.geographyKeyTextBox.TabIndex = 44;
            this.geographyKeyTextBox.TabStop = false;
            // 
            // geographyNameComboBox
            // 
            this.geographyNameComboBox.FormattingEnabled = true;
            this.geographyNameComboBox.Location = new System.Drawing.Point(99, 139);
            this.geographyNameComboBox.Name = "geographyNameComboBox";
            this.geographyNameComboBox.Size = new System.Drawing.Size(338, 21);
            this.geographyNameComboBox.TabIndex = 4;
            // 
            // inputColumnKeyTextBox
            // 
            this.inputColumnKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "InputColumnKey", true));
            this.inputColumnKeyTextBox.Location = new System.Drawing.Point(460, 512);
            this.inputColumnKeyTextBox.Name = "inputColumnKeyTextBox";
            this.inputColumnKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.inputColumnKeyTextBox.TabIndex = 48;
            this.inputColumnKeyTextBox.TabStop = false;
            // 
            // inputFormKeyTextBox
            // 
            this.inputFormKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "InputFormKey", true));
            this.inputFormKeyTextBox.Location = new System.Drawing.Point(460, 32);
            this.inputFormKeyTextBox.Name = "inputFormKeyTextBox";
            this.inputFormKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.inputFormKeyTextBox.TabIndex = 50;
            this.inputFormKeyTextBox.TabStop = false;
            // 
            // inputFormNameComboBox
            // 
            this.inputFormNameComboBox.FormattingEnabled = true;
            this.inputFormNameComboBox.Location = new System.Drawing.Point(99, 31);
            this.inputFormNameComboBox.Name = "inputFormNameComboBox";
            this.inputFormNameComboBox.Size = new System.Drawing.Size(338, 21);
            this.inputFormNameComboBox.TabIndex = 0;
            // 
            // marketKeyTextBox
            // 
            this.marketKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "MarketKey", true));
            this.marketKeyTextBox.Location = new System.Drawing.Point(460, 167);
            this.marketKeyTextBox.Name = "marketKeyTextBox";
            this.marketKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.marketKeyTextBox.TabIndex = 54;
            this.marketKeyTextBox.TabStop = false;
            // 
            // marketNameComboBox
            // 
            this.marketNameComboBox.FormattingEnabled = true;
            this.marketNameComboBox.Location = new System.Drawing.Point(99, 166);
            this.marketNameComboBox.Name = "marketNameComboBox";
            this.marketNameComboBox.Size = new System.Drawing.Size(338, 21);
            this.marketNameComboBox.TabIndex = 5;
            // 
            // measureKeyTextBox
            // 
            this.measureKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "MeasureKey", true));
            this.measureKeyTextBox.Location = new System.Drawing.Point(460, 194);
            this.measureKeyTextBox.Name = "measureKeyTextBox";
            this.measureKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.measureKeyTextBox.TabIndex = 58;
            this.measureKeyTextBox.TabStop = false;
            // 
            // measureNameComboBox
            // 
            this.measureNameComboBox.FormattingEnabled = true;
            this.measureNameComboBox.Location = new System.Drawing.Point(99, 193);
            this.measureNameComboBox.Name = "measureNameComboBox";
            this.measureNameComboBox.Size = new System.Drawing.Size(338, 21);
            this.measureNameComboBox.TabIndex = 6;
            // 
            // size1KeyTextBox
            // 
            this.size1KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "Size1Key", true));
            this.size1KeyTextBox.Location = new System.Drawing.Point(460, 248);
            this.size1KeyTextBox.Name = "size1KeyTextBox";
            this.size1KeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.size1KeyTextBox.TabIndex = 62;
            this.size1KeyTextBox.TabStop = false;
            // 
            // size1NameComboBox
            // 
            this.size1NameComboBox.FormattingEnabled = true;
            this.size1NameComboBox.Location = new System.Drawing.Point(99, 247);
            this.size1NameComboBox.Name = "size1NameComboBox";
            this.size1NameComboBox.Size = new System.Drawing.Size(338, 21);
            this.size1NameComboBox.TabIndex = 8;
            // 
            // size2KeyTextBox
            // 
            this.size2KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "Size2Key", true));
            this.size2KeyTextBox.Location = new System.Drawing.Point(460, 275);
            this.size2KeyTextBox.Name = "size2KeyTextBox";
            this.size2KeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.size2KeyTextBox.TabIndex = 66;
            this.size2KeyTextBox.TabStop = false;
            // 
            // size2NameComboBox
            // 
            this.size2NameComboBox.FormattingEnabled = true;
            this.size2NameComboBox.Location = new System.Drawing.Point(99, 274);
            this.size2NameComboBox.Name = "size2NameComboBox";
            this.size2NameComboBox.Size = new System.Drawing.Size(338, 21);
            this.size2NameComboBox.TabIndex = 9;
            // 
            // size3KeyTextBox
            // 
            this.size3KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "Size3Key", true));
            this.size3KeyTextBox.Location = new System.Drawing.Point(460, 302);
            this.size3KeyTextBox.Name = "size3KeyTextBox";
            this.size3KeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.size3KeyTextBox.TabIndex = 70;
            this.size3KeyTextBox.TabStop = false;
            // 
            // size3NameComboBox
            // 
            this.size3NameComboBox.FormattingEnabled = true;
            this.size3NameComboBox.Location = new System.Drawing.Point(99, 301);
            this.size3NameComboBox.Name = "size3NameComboBox";
            this.size3NameComboBox.Size = new System.Drawing.Size(338, 21);
            this.size3NameComboBox.TabIndex = 10;
            // 
            // size4KeyTextBox
            // 
            this.size4KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "Size4Key", true));
            this.size4KeyTextBox.Location = new System.Drawing.Point(460, 329);
            this.size4KeyTextBox.Name = "size4KeyTextBox";
            this.size4KeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.size4KeyTextBox.TabIndex = 74;
            this.size4KeyTextBox.TabStop = false;
            // 
            // size4NameComboBox
            // 
            this.size4NameComboBox.FormattingEnabled = true;
            this.size4NameComboBox.Location = new System.Drawing.Point(99, 328);
            this.size4NameComboBox.Name = "size4NameComboBox";
            this.size4NameComboBox.Size = new System.Drawing.Size(338, 21);
            this.size4NameComboBox.TabIndex = 11;
            // 
            // size5KeyTextBox
            // 
            this.size5KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "Size5Key", true));
            this.size5KeyTextBox.Location = new System.Drawing.Point(460, 356);
            this.size5KeyTextBox.Name = "size5KeyTextBox";
            this.size5KeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.size5KeyTextBox.TabIndex = 78;
            this.size5KeyTextBox.TabStop = false;
            // 
            // size5NameComboBox
            // 
            this.size5NameComboBox.FormattingEnabled = true;
            this.size5NameComboBox.Location = new System.Drawing.Point(99, 355);
            this.size5NameComboBox.Name = "size5NameComboBox";
            this.size5NameComboBox.Size = new System.Drawing.Size(338, 21);
            this.size5NameComboBox.TabIndex = 12;
            // 
            // sortKeyTextBox
            // 
            this.sortKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "SortKey", true));
            this.sortKeyTextBox.Location = new System.Drawing.Point(302, 446);
            this.sortKeyTextBox.Name = "sortKeyTextBox";
            this.sortKeyTextBox.Size = new System.Drawing.Size(31, 20);
            this.sortKeyTextBox.TabIndex = 15;
            // 
            // targetYearTextBox
            // 
            this.targetYearTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "TargetYear", true));
            this.targetYearTextBox.Location = new System.Drawing.Point(168, 446);
            this.targetYearTextBox.Name = "targetYearTextBox";
            this.targetYearTextBox.Size = new System.Drawing.Size(48, 20);
            this.targetYearTextBox.TabIndex = 14;
            // 
            // useKeyTextBox
            // 
            this.useKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "UseKey", true));
            this.useKeyTextBox.Location = new System.Drawing.Point(460, 221);
            this.useKeyTextBox.Name = "useKeyTextBox";
            this.useKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.useKeyTextBox.TabIndex = 86;
            this.useKeyTextBox.TabStop = false;
            // 
            // useNameComboBox
            // 
            this.useNameComboBox.FormattingEnabled = true;
            this.useNameComboBox.Location = new System.Drawing.Point(99, 220);
            this.useNameComboBox.Name = "useNameComboBox";
            this.useNameComboBox.Size = new System.Drawing.Size(338, 21);
            this.useNameComboBox.TabIndex = 7;
            // 
            // columnHeader2Label
            // 
            columnHeader2Label.AutoSize = true;
            columnHeader2Label.Location = new System.Drawing.Point(12, 411);
            columnHeader2Label.Name = "columnHeader2Label";
            columnHeader2Label.Size = new System.Drawing.Size(89, 13);
            columnHeader2Label.TabIndex = 88;
            columnHeader2Label.Text = "Column Header2:";
            // 
            // columnHeader2TextBox
            // 
            this.columnHeader2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputColumnBindingSource, "ColumnHeader2", true));
            this.columnHeader2TextBox.Location = new System.Drawing.Point(99, 408);
            this.columnHeader2TextBox.Name = "columnHeader2TextBox";
            this.columnHeader2TextBox.Size = new System.Drawing.Size(338, 20);
            this.columnHeader2TextBox.TabIndex = 89;
            // 
            // inputColumnBindingSource
            // 
            this.inputColumnBindingSource.DataSource = typeof(AhamMetaDataDAL.InputColumn);
            // 
            // InputColumnEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 588);
            this.Controls.Add(this.columnHeader2TextBox);
            this.Controls.Add(columnHeader2Label);
            this.Controls.Add(labelKey);
            this.Controls.Add(this.activityKeyTextBox);
            this.Controls.Add(activityNameLabel);
            this.Controls.Add(this.activityNameComboBox);
            this.Controls.Add(this.catalogKeyTextBox);
            this.Controls.Add(catalogNameLabel);
            this.Controls.Add(this.catalogNameComboBox);
            this.Controls.Add(this.channelKeyTextBox);
            this.Controls.Add(channelNameLabel);
            this.Controls.Add(this.channelNameComboBox);
            this.Controls.Add(columnHeaderLabel);
            this.Controls.Add(this.columnHeaderTextBox);
            this.Controls.Add(this.geographyKeyTextBox);
            this.Controls.Add(geographyNameLabel);
            this.Controls.Add(this.geographyNameComboBox);
            this.Controls.Add(inputColumnKeyLabel);
            this.Controls.Add(this.inputColumnKeyTextBox);
            this.Controls.Add(this.inputFormKeyTextBox);
            this.Controls.Add(inputFormNameLabel);
            this.Controls.Add(this.inputFormNameComboBox);
            this.Controls.Add(this.marketKeyTextBox);
            this.Controls.Add(marketNameLabel);
            this.Controls.Add(this.marketNameComboBox);
            this.Controls.Add(this.measureKeyTextBox);
            this.Controls.Add(measureNameLabel);
            this.Controls.Add(this.measureNameComboBox);
            this.Controls.Add(this.size1KeyTextBox);
            this.Controls.Add(size1NameLabel);
            this.Controls.Add(this.size1NameComboBox);
            this.Controls.Add(this.size2KeyTextBox);
            this.Controls.Add(size2NameLabel);
            this.Controls.Add(this.size2NameComboBox);
            this.Controls.Add(this.size3KeyTextBox);
            this.Controls.Add(size3NameLabel);
            this.Controls.Add(this.size3NameComboBox);
            this.Controls.Add(this.size4KeyTextBox);
            this.Controls.Add(size4NameLabel);
            this.Controls.Add(this.size4NameComboBox);
            this.Controls.Add(this.size5KeyTextBox);
            this.Controls.Add(size5NameLabel);
            this.Controls.Add(this.size5NameComboBox);
            this.Controls.Add(sortKeyLabel);
            this.Controls.Add(this.sortKeyTextBox);
            this.Controls.Add(targetYearLabel);
            this.Controls.Add(this.targetYearTextBox);
            this.Controls.Add(this.useKeyTextBox);
            this.Controls.Add(useNameLabel);
            this.Controls.Add(this.useNameComboBox);
            this.Name = "InputColumnEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Input column";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.useNameComboBox, 0);
            this.Controls.SetChildIndex(useNameLabel, 0);
            this.Controls.SetChildIndex(this.useKeyTextBox, 0);
            this.Controls.SetChildIndex(this.targetYearTextBox, 0);
            this.Controls.SetChildIndex(targetYearLabel, 0);
            this.Controls.SetChildIndex(this.sortKeyTextBox, 0);
            this.Controls.SetChildIndex(sortKeyLabel, 0);
            this.Controls.SetChildIndex(this.size5NameComboBox, 0);
            this.Controls.SetChildIndex(size5NameLabel, 0);
            this.Controls.SetChildIndex(this.size5KeyTextBox, 0);
            this.Controls.SetChildIndex(this.size4NameComboBox, 0);
            this.Controls.SetChildIndex(size4NameLabel, 0);
            this.Controls.SetChildIndex(this.size4KeyTextBox, 0);
            this.Controls.SetChildIndex(this.size3NameComboBox, 0);
            this.Controls.SetChildIndex(size3NameLabel, 0);
            this.Controls.SetChildIndex(this.size3KeyTextBox, 0);
            this.Controls.SetChildIndex(this.size2NameComboBox, 0);
            this.Controls.SetChildIndex(size2NameLabel, 0);
            this.Controls.SetChildIndex(this.size2KeyTextBox, 0);
            this.Controls.SetChildIndex(this.size1NameComboBox, 0);
            this.Controls.SetChildIndex(size1NameLabel, 0);
            this.Controls.SetChildIndex(this.size1KeyTextBox, 0);
            this.Controls.SetChildIndex(this.measureNameComboBox, 0);
            this.Controls.SetChildIndex(measureNameLabel, 0);
            this.Controls.SetChildIndex(this.measureKeyTextBox, 0);
            this.Controls.SetChildIndex(this.marketNameComboBox, 0);
            this.Controls.SetChildIndex(marketNameLabel, 0);
            this.Controls.SetChildIndex(this.marketKeyTextBox, 0);
            this.Controls.SetChildIndex(this.inputFormNameComboBox, 0);
            this.Controls.SetChildIndex(inputFormNameLabel, 0);
            this.Controls.SetChildIndex(this.inputFormKeyTextBox, 0);
            this.Controls.SetChildIndex(this.inputColumnKeyTextBox, 0);
            this.Controls.SetChildIndex(inputColumnKeyLabel, 0);
            this.Controls.SetChildIndex(this.geographyNameComboBox, 0);
            this.Controls.SetChildIndex(geographyNameLabel, 0);
            this.Controls.SetChildIndex(this.geographyKeyTextBox, 0);
            this.Controls.SetChildIndex(this.columnHeaderTextBox, 0);
            this.Controls.SetChildIndex(columnHeaderLabel, 0);
            this.Controls.SetChildIndex(this.channelNameComboBox, 0);
            this.Controls.SetChildIndex(channelNameLabel, 0);
            this.Controls.SetChildIndex(this.channelKeyTextBox, 0);
            this.Controls.SetChildIndex(this.catalogNameComboBox, 0);
            this.Controls.SetChildIndex(catalogNameLabel, 0);
            this.Controls.SetChildIndex(this.catalogKeyTextBox, 0);
            this.Controls.SetChildIndex(this.activityNameComboBox, 0);
            this.Controls.SetChildIndex(activityNameLabel, 0);
            this.Controls.SetChildIndex(this.activityKeyTextBox, 0);
            this.Controls.SetChildIndex(labelKey, 0);
            this.Controls.SetChildIndex(columnHeader2Label, 0);
            this.Controls.SetChildIndex(this.columnHeader2TextBox, 0);
            ((System.ComponentModel.ISupportInitialize)(this.inputColumnBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource inputColumnBindingSource;
        private System.Windows.Forms.TextBox activityKeyTextBox;
        private System.Windows.Forms.ComboBox activityNameComboBox;
        private System.Windows.Forms.TextBox catalogKeyTextBox;
        private System.Windows.Forms.ComboBox catalogNameComboBox;
        private System.Windows.Forms.TextBox channelKeyTextBox;
        private System.Windows.Forms.ComboBox channelNameComboBox;
        private System.Windows.Forms.TextBox columnHeaderTextBox;
        private System.Windows.Forms.TextBox geographyKeyTextBox;
        private System.Windows.Forms.ComboBox geographyNameComboBox;
        private System.Windows.Forms.TextBox inputColumnKeyTextBox;
        private System.Windows.Forms.TextBox inputFormKeyTextBox;
        private System.Windows.Forms.ComboBox inputFormNameComboBox;
        private System.Windows.Forms.TextBox marketKeyTextBox;
        private System.Windows.Forms.ComboBox marketNameComboBox;
        private System.Windows.Forms.TextBox measureKeyTextBox;
        private System.Windows.Forms.ComboBox measureNameComboBox;
        private System.Windows.Forms.TextBox size1KeyTextBox;
        private System.Windows.Forms.ComboBox size1NameComboBox;
        private System.Windows.Forms.TextBox size2KeyTextBox;
        private System.Windows.Forms.ComboBox size2NameComboBox;
        private System.Windows.Forms.TextBox size3KeyTextBox;
        private System.Windows.Forms.ComboBox size3NameComboBox;
        private System.Windows.Forms.TextBox size4KeyTextBox;
        private System.Windows.Forms.ComboBox size4NameComboBox;
        private System.Windows.Forms.TextBox size5KeyTextBox;
        private System.Windows.Forms.ComboBox size5NameComboBox;
        private System.Windows.Forms.TextBox sortKeyTextBox;
        private System.Windows.Forms.TextBox targetYearTextBox;
        private System.Windows.Forms.TextBox useKeyTextBox;
        private System.Windows.Forms.ComboBox useNameComboBox;
        private System.Windows.Forms.TextBox columnHeader2TextBox;
    }
}