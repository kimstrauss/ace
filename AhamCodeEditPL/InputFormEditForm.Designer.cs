﻿namespace AhamMetaDataPL
{
    partial class InputFormEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label inputFormKeyLabel;
            System.Windows.Forms.Label showTotalsLabel;
            System.Windows.Forms.Label titleLabel;
            System.Windows.Forms.Label title2Label;
            this.inputFormKeyTextBox = new System.Windows.Forms.TextBox();
            this.inputFormBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.showTotalsCheckBox = new System.Windows.Forms.CheckBox();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.title2TextBox = new System.Windows.Forms.TextBox();
            inputFormKeyLabel = new System.Windows.Forms.Label();
            showTotalsLabel = new System.Windows.Forms.Label();
            titleLabel = new System.Windows.Forms.Label();
            title2Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.inputFormBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(343, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(424, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // inputFormKeyLabel
            // 
            inputFormKeyLabel.AutoSize = true;
            inputFormKeyLabel.Location = new System.Drawing.Point(362, 138);
            inputFormKeyLabel.Name = "inputFormKeyLabel";
            inputFormKeyLabel.Size = new System.Drawing.Size(81, 13);
            inputFormKeyLabel.TabIndex = 30;
            inputFormKeyLabel.Text = "Input Form Key:";
            // 
            // showTotalsLabel
            // 
            showTotalsLabel.AutoSize = true;
            showTotalsLabel.Location = new System.Drawing.Point(12, 63);
            showTotalsLabel.Name = "showTotalsLabel";
            showTotalsLabel.Size = new System.Drawing.Size(69, 13);
            showTotalsLabel.TabIndex = 32;
            showTotalsLabel.Text = "Show Totals:";
            // 
            // titleLabel
            // 
            titleLabel.AutoSize = true;
            titleLabel.Location = new System.Drawing.Point(12, 9);
            titleLabel.Name = "titleLabel";
            titleLabel.Size = new System.Drawing.Size(30, 13);
            titleLabel.TabIndex = 34;
            titleLabel.Text = "Title:";
            // 
            // title2Label
            // 
            title2Label.AutoSize = true;
            title2Label.Location = new System.Drawing.Point(12, 35);
            title2Label.Name = "title2Label";
            title2Label.Size = new System.Drawing.Size(36, 13);
            title2Label.TabIndex = 36;
            title2Label.Text = "Title2:";
            // 
            // inputFormKeyTextBox
            // 
            this.inputFormKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputFormBindingSource, "InputFormKey", true));
            this.inputFormKeyTextBox.Location = new System.Drawing.Point(449, 135);
            this.inputFormKeyTextBox.Name = "inputFormKeyTextBox";
            this.inputFormKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.inputFormKeyTextBox.TabIndex = 31;
            this.inputFormKeyTextBox.TabStop = false;
            // 
            // inputFormBindingSource
            // 
            this.inputFormBindingSource.DataSource = typeof(AhamMetaDataDAL.InputForm);
            // 
            // showTotalsCheckBox
            // 
            this.showTotalsCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.inputFormBindingSource, "ShowTotals", true));
            this.showTotalsCheckBox.Location = new System.Drawing.Point(99, 58);
            this.showTotalsCheckBox.Name = "showTotalsCheckBox";
            this.showTotalsCheckBox.Size = new System.Drawing.Size(104, 24);
            this.showTotalsCheckBox.TabIndex = 2;
            this.showTotalsCheckBox.UseVisualStyleBackColor = true;
            // 
            // titleTextBox
            // 
            this.titleTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputFormBindingSource, "Title", true));
            this.titleTextBox.Location = new System.Drawing.Point(99, 6);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(400, 20);
            this.titleTextBox.TabIndex = 0;
            // 
            // title2TextBox
            // 
            this.title2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.inputFormBindingSource, "Title2", true));
            this.title2TextBox.Location = new System.Drawing.Point(99, 32);
            this.title2TextBox.Name = "title2TextBox";
            this.title2TextBox.Size = new System.Drawing.Size(400, 20);
            this.title2TextBox.TabIndex = 1;
            // 
            // InputFormEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 216);
            this.Controls.Add(inputFormKeyLabel);
            this.Controls.Add(this.inputFormKeyTextBox);
            this.Controls.Add(showTotalsLabel);
            this.Controls.Add(this.showTotalsCheckBox);
            this.Controls.Add(titleLabel);
            this.Controls.Add(this.titleTextBox);
            this.Controls.Add(title2Label);
            this.Controls.Add(this.title2TextBox);
            this.Name = "InputFormEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Input form";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.title2TextBox, 0);
            this.Controls.SetChildIndex(title2Label, 0);
            this.Controls.SetChildIndex(this.titleTextBox, 0);
            this.Controls.SetChildIndex(titleLabel, 0);
            this.Controls.SetChildIndex(this.showTotalsCheckBox, 0);
            this.Controls.SetChildIndex(showTotalsLabel, 0);
            this.Controls.SetChildIndex(this.inputFormKeyTextBox, 0);
            this.Controls.SetChildIndex(inputFormKeyLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.inputFormBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource inputFormBindingSource;
        private System.Windows.Forms.TextBox inputFormKeyTextBox;
        private System.Windows.Forms.CheckBox showTotalsCheckBox;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.TextBox title2TextBox;
    }
}