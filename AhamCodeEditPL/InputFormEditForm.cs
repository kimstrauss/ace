﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class InputFormEditForm : HaiObjectEditForm
    {
        private InputForm _inputFormToEdit;

        public InputFormEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters=parameters;
            _inputFormToEdit = (InputForm)parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            inputFormBindingSource.DataSource = _inputFormToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if ((titleTextBox.Text.Trim() == string.Empty) && (title2TextBox.Text.Trim() == string.Empty))
            {
                MessageBox.Show("At least one title must be entered", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_inputFormToEdit);
            _inputFormToEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
