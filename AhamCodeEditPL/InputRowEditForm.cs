﻿using System;
using System.Windows.Forms;

using System.Collections;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class InputRowEditForm : HaiObjectEditForm
    {
        private InputRow _inputRowToEdit;

        public InputRowEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _inputRowToEdit = (InputRow)_parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);          // the form needs to be prepared before assigning values to controls
            inputRowBindingSource.DataSource = _inputRowToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = ds.GetDataList(HaiBusinessObjectType.InputForm);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<InputForm>(inputFormNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "InputFormName", "InputFormKey", inputFormKeyTextBox, _inputRowToEdit.InputFormName.Trim(), "InputFormName");
            }
            else
            {
                MessageBox.Show("Cannot add/edit input row.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Activity_Aham);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Activity>(activityNameComboBox, result, ComboBoxListType.DataListWithNullItem, "ActivityName", "ActivityKey", activityKeyTextBox, _inputRowToEdit.ActivityName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit input row.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Catalog);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Catalog>(catalogNameComboBox, result, ComboBoxListType.DataListWithNullItem, "CatalogName", "CatalogKey", catalogKeyTextBox, _inputRowToEdit.CatalogName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit input row.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Channel);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Channel>(channelNameComboBox, result, ComboBoxListType.DataListWithNullItem, "ChannelName", "ChannelKey", channelKeyTextBox, _inputRowToEdit.ChannelName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit input row.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Geography);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Geography>(geographyNameComboBox, result, ComboBoxListType.DataListWithNullItem, "GeographyAbbreviation", "GeographyKey", geographyKeyTextBox, _inputRowToEdit.GeographyName, "GeographyName");
            }
            else
            {
                MessageBox.Show("Cannot add/edit input row.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Market);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Market>(marketNameComboBox, result, ComboBoxListType.DataListWithNullItem, "MarketName", "MarketKey", marketKeyTextBox, _inputRowToEdit.MarketName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit input row.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Measure);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Measure>(measureNameComboBox, result, ComboBoxListType.DataListWithNullItem, "MeasureName", "MeasureKey", measureKeyTextBox, _inputRowToEdit.MeasureName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit input row.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Use);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Use>(useNameComboBox, result, ComboBoxListType.DataListWithNullItem, "UseName", "UseKey", useKeyTextBox, _inputRowToEdit.UseName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit input row.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            // N.B. a separate instance of the "Size" binding list is required for each "SizeN" combobox
            result = ds.GetDataList(HaiBusinessObjectType.Size);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<AhamMetaDataDAL.Size>(size1NameComboBox, result, ComboBoxListType.DataListWithNullItem, "SizeName", "SizeKey", size1KeyTextBox, _inputRowToEdit.Size1Name.Trim(), "Size1Name");
            }
            else
            {
                MessageBox.Show("Cannot add/edit input column.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Size);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<AhamMetaDataDAL.Size>(size2NameComboBox, result, ComboBoxListType.DataListWithNullItem, "SizeName", "SizeKey", size2KeyTextBox, _inputRowToEdit.Size2Name.Trim(), "Size2Name");
            }
            else
            {
                MessageBox.Show("Cannot add/edit input column.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Size);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<AhamMetaDataDAL.Size>(size3NameComboBox, result, ComboBoxListType.DataListWithNullItem, "SizeName", "SizeKey", size3KeyTextBox, _inputRowToEdit.Size3Name.Trim(), "Size3Name");
            }
            else
            {
                MessageBox.Show("Cannot add/edit input column.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Size);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<AhamMetaDataDAL.Size>(size4NameComboBox, result, ComboBoxListType.DataListWithNullItem, "SizeName", "SizeKey", size4KeyTextBox, _inputRowToEdit.Size4Name.Trim(), "Size4Name");
            }
            else
            {
                MessageBox.Show("Cannot add/edit input column.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Size);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<AhamMetaDataDAL.Size>(size5NameComboBox, result, ComboBoxListType.DataListWithNullItem, "SizeName", "SizeKey", size5KeyTextBox, _inputRowToEdit.Size5Name.Trim(), "Size5Name");
            }
            else
            {
                MessageBox.Show("Cannot add/edit input column.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;
            string message = string.Empty;

            if (rowHeaderTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A header is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (sortKeyTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A sort key is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (inputFormNameComboBox.Text.Trim() == string.Empty && inputFormNameComboBox.Items.Count>0)
            {
                MessageBox.Show("An input form must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            // set the property values in the object that was the target of the edit
            MarkChangedProperties(_inputRowToEdit);    // this is a multi-edit form
            _inputRowToEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
