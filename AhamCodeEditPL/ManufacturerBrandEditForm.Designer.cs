﻿namespace AhamMetaDataPL
{
    partial class ManufacturerBrandEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label brandNameLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label fromManfacturerKeyLabel;
            System.Windows.Forms.Label manufacturerBrandKeyLabel;
            System.Windows.Forms.Label marketKeyLabel;
            System.Windows.Forms.Label productKeyLabel;
            System.Windows.Forms.Label reportingManufacturerKeyLabel;
            System.Windows.Forms.Label toManufacturerKeyLabel;
            System.Windows.Forms.Label fromManufacturerNameLabel;
            System.Windows.Forms.Label marketNameLabel;
            System.Windows.Forms.Label productNameLabel;
            System.Windows.Forms.Label reportingManufacturerNameLabel;
            System.Windows.Forms.Label toManufacturerNameLabel;
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.manufacturerBrandBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.brandNameTextBox = new System.Windows.Forms.TextBox();
            this.displayBrandCheckBox = new System.Windows.Forms.CheckBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.includedCheckBox = new System.Windows.Forms.CheckBox();
            this.manufacturerBrandKeyTextBox = new System.Windows.Forms.TextBox();
            this.marketKeyTextBox = new System.Windows.Forms.TextBox();
            this.productKeyTextBox = new System.Windows.Forms.TextBox();
            this.reportingManufacturerKeyTextBox = new System.Windows.Forms.TextBox();
            this.toManufacturerKeyTextBox = new System.Windows.Forms.TextBox();
            this.fromManufacturerNameComboBox = new System.Windows.Forms.ComboBox();
            this.marketNameComboBox = new System.Windows.Forms.ComboBox();
            this.productNameComboBox = new System.Windows.Forms.ComboBox();
            this.reportingManufacturerNameComboBox = new System.Windows.Forms.ComboBox();
            this.toManufacturerNameComboBox = new System.Windows.Forms.ComboBox();
            this.fromManufacturerKeyTextBox = new System.Windows.Forms.TextBox();
            beginDateLabel = new System.Windows.Forms.Label();
            brandNameLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            fromManfacturerKeyLabel = new System.Windows.Forms.Label();
            manufacturerBrandKeyLabel = new System.Windows.Forms.Label();
            marketKeyLabel = new System.Windows.Forms.Label();
            productKeyLabel = new System.Windows.Forms.Label();
            reportingManufacturerKeyLabel = new System.Windows.Forms.Label();
            toManufacturerKeyLabel = new System.Windows.Forms.Label();
            fromManufacturerNameLabel = new System.Windows.Forms.Label();
            marketNameLabel = new System.Windows.Forms.Label();
            productNameLabel = new System.Windows.Forms.Label();
            reportingManufacturerNameLabel = new System.Windows.Forms.Label();
            toManufacturerNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerBrandBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(482, 309);
            this.btnOK.TabIndex = 10;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(563, 309);
            this.btnCancel.TabIndex = 11;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 309);
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(110, 196);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 30;
            beginDateLabel.Text = "Begin Date:";
            // 
            // brandNameLabel
            // 
            brandNameLabel.AutoSize = true;
            brandNameLabel.Location = new System.Drawing.Point(20, 74);
            brandNameLabel.Name = "brandNameLabel";
            brandNameLabel.Size = new System.Drawing.Size(69, 13);
            brandNameLabel.TabIndex = 32;
            brandNameLabel.Text = "Brand Name:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(118, 220);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 36;
            endDateLabel.Text = "End Date:";
            // 
            // fromManfacturerKeyLabel
            // 
            fromManfacturerKeyLabel.AutoSize = true;
            fromManfacturerKeyLabel.Location = new System.Drawing.Point(553, 127);
            fromManfacturerKeyLabel.Name = "fromManfacturerKeyLabel";
            fromManfacturerKeyLabel.Size = new System.Drawing.Size(28, 13);
            fromManfacturerKeyLabel.TabIndex = 38;
            fromManfacturerKeyLabel.Text = "Key:";
            // 
            // manufacturerBrandKeyLabel
            // 
            manufacturerBrandKeyLabel.AutoSize = true;
            manufacturerBrandKeyLabel.Location = new System.Drawing.Point(456, 272);
            manufacturerBrandKeyLabel.Name = "manufacturerBrandKeyLabel";
            manufacturerBrandKeyLabel.Size = new System.Drawing.Size(125, 13);
            manufacturerBrandKeyLabel.TabIndex = 44;
            manufacturerBrandKeyLabel.Text = "Manufacturer Brand Key:";
            // 
            // marketKeyLabel
            // 
            marketKeyLabel.AutoSize = true;
            marketKeyLabel.Location = new System.Drawing.Point(553, 100);
            marketKeyLabel.Name = "marketKeyLabel";
            marketKeyLabel.Size = new System.Drawing.Size(28, 13);
            marketKeyLabel.TabIndex = 46;
            marketKeyLabel.Text = "Key:";
            // 
            // productKeyLabel
            // 
            productKeyLabel.AutoSize = true;
            productKeyLabel.Location = new System.Drawing.Point(553, 47);
            productKeyLabel.Name = "productKeyLabel";
            productKeyLabel.Size = new System.Drawing.Size(28, 13);
            productKeyLabel.TabIndex = 50;
            productKeyLabel.Text = "Key:";
            // 
            // reportingManufacturerKeyLabel
            // 
            reportingManufacturerKeyLabel.AutoSize = true;
            reportingManufacturerKeyLabel.Location = new System.Drawing.Point(553, 20);
            reportingManufacturerKeyLabel.Name = "reportingManufacturerKeyLabel";
            reportingManufacturerKeyLabel.Size = new System.Drawing.Size(28, 13);
            reportingManufacturerKeyLabel.TabIndex = 54;
            reportingManufacturerKeyLabel.Text = "Key:";
            // 
            // toManufacturerKeyLabel
            // 
            toManufacturerKeyLabel.AutoSize = true;
            toManufacturerKeyLabel.Location = new System.Drawing.Point(553, 154);
            toManufacturerKeyLabel.Name = "toManufacturerKeyLabel";
            toManufacturerKeyLabel.Size = new System.Drawing.Size(28, 13);
            toManufacturerKeyLabel.TabIndex = 58;
            toManufacturerKeyLabel.Text = "Key:";
            // 
            // fromManufacturerNameLabel
            // 
            fromManufacturerNameLabel.AutoSize = true;
            fromManufacturerNameLabel.Location = new System.Drawing.Point(20, 124);
            fromManufacturerNameLabel.Name = "fromManufacturerNameLabel";
            fromManufacturerNameLabel.Size = new System.Drawing.Size(130, 13);
            fromManufacturerNameLabel.TabIndex = 61;
            fromManufacturerNameLabel.Text = "From Manufacturer Name:";
            // 
            // marketNameLabel
            // 
            marketNameLabel.AutoSize = true;
            marketNameLabel.Location = new System.Drawing.Point(20, 97);
            marketNameLabel.Name = "marketNameLabel";
            marketNameLabel.Size = new System.Drawing.Size(74, 13);
            marketNameLabel.TabIndex = 62;
            marketNameLabel.Text = "Market Name:";
            // 
            // productNameLabel
            // 
            productNameLabel.AutoSize = true;
            productNameLabel.Location = new System.Drawing.Point(20, 47);
            productNameLabel.Name = "productNameLabel";
            productNameLabel.Size = new System.Drawing.Size(78, 13);
            productNameLabel.TabIndex = 63;
            productNameLabel.Text = "Product Name:";
            // 
            // reportingManufacturerNameLabel
            // 
            reportingManufacturerNameLabel.AutoSize = true;
            reportingManufacturerNameLabel.Location = new System.Drawing.Point(20, 20);
            reportingManufacturerNameLabel.Name = "reportingManufacturerNameLabel";
            reportingManufacturerNameLabel.Size = new System.Drawing.Size(153, 13);
            reportingManufacturerNameLabel.TabIndex = 64;
            reportingManufacturerNameLabel.Text = "Reporting Manufacturer Name:";
            // 
            // toManufacturerNameLabel
            // 
            toManufacturerNameLabel.AutoSize = true;
            toManufacturerNameLabel.Location = new System.Drawing.Point(20, 154);
            toManufacturerNameLabel.Name = "toManufacturerNameLabel";
            toManufacturerNameLabel.Size = new System.Drawing.Size(120, 13);
            toManufacturerNameLabel.TabIndex = 65;
            toManufacturerNameLabel.Text = "To Manufacturer Name:";
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBrandBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(179, 193);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(104, 20);
            this.beginDateTextBox.TabIndex = 6;
            // 
            // manufacturerBrandBindingSource
            // 
            this.manufacturerBrandBindingSource.DataSource = typeof(AhamMetaDataDAL.ManufacturerBrand);
            // 
            // brandNameTextBox
            // 
            this.brandNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBrandBindingSource, "BrandName", true));
            this.brandNameTextBox.Location = new System.Drawing.Point(179, 71);
            this.brandNameTextBox.Name = "brandNameTextBox";
            this.brandNameTextBox.Size = new System.Drawing.Size(368, 20);
            this.brandNameTextBox.TabIndex = 2;
            // 
            // displayBrandCheckBox
            // 
            this.displayBrandCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerBrandBindingSource, "DisplayBrand", true));
            this.displayBrandCheckBox.Location = new System.Drawing.Point(346, 193);
            this.displayBrandCheckBox.Name = "displayBrandCheckBox";
            this.displayBrandCheckBox.Size = new System.Drawing.Size(104, 24);
            this.displayBrandCheckBox.TabIndex = 8;
            this.displayBrandCheckBox.Text = "Display brand";
            this.displayBrandCheckBox.UseVisualStyleBackColor = true;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBrandBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(179, 217);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(104, 20);
            this.endDateTextBox.TabIndex = 7;
            // 
            // includedCheckBox
            // 
            this.includedCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerBrandBindingSource, "Included", true));
            this.includedCheckBox.Location = new System.Drawing.Point(346, 217);
            this.includedCheckBox.Name = "includedCheckBox";
            this.includedCheckBox.Size = new System.Drawing.Size(104, 24);
            this.includedCheckBox.TabIndex = 9;
            this.includedCheckBox.Text = "Included";
            this.includedCheckBox.UseVisualStyleBackColor = true;
            // 
            // manufacturerBrandKeyTextBox
            // 
            this.manufacturerBrandKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBrandBindingSource, "ManufacturerBrandKey", true));
            this.manufacturerBrandKeyTextBox.Location = new System.Drawing.Point(587, 269);
            this.manufacturerBrandKeyTextBox.Name = "manufacturerBrandKeyTextBox";
            this.manufacturerBrandKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.manufacturerBrandKeyTextBox.TabIndex = 45;
            this.manufacturerBrandKeyTextBox.TabStop = false;
            // 
            // marketKeyTextBox
            // 
            this.marketKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBrandBindingSource, "MarketKey", true));
            this.marketKeyTextBox.Location = new System.Drawing.Point(587, 98);
            this.marketKeyTextBox.Name = "marketKeyTextBox";
            this.marketKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.marketKeyTextBox.TabIndex = 47;
            this.marketKeyTextBox.TabStop = false;
            // 
            // productKeyTextBox
            // 
            this.productKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBrandBindingSource, "ProductKey", true));
            this.productKeyTextBox.Location = new System.Drawing.Point(587, 47);
            this.productKeyTextBox.Name = "productKeyTextBox";
            this.productKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.productKeyTextBox.TabIndex = 51;
            this.productKeyTextBox.TabStop = false;
            // 
            // reportingManufacturerKeyTextBox
            // 
            this.reportingManufacturerKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBrandBindingSource, "ReportingManufacturerKey", true));
            this.reportingManufacturerKeyTextBox.Location = new System.Drawing.Point(587, 18);
            this.reportingManufacturerKeyTextBox.Name = "reportingManufacturerKeyTextBox";
            this.reportingManufacturerKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.reportingManufacturerKeyTextBox.TabIndex = 55;
            this.reportingManufacturerKeyTextBox.TabStop = false;
            // 
            // toManufacturerKeyTextBox
            // 
            this.toManufacturerKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBrandBindingSource, "ToManufacturerKey", true));
            this.toManufacturerKeyTextBox.Location = new System.Drawing.Point(587, 152);
            this.toManufacturerKeyTextBox.Name = "toManufacturerKeyTextBox";
            this.toManufacturerKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.toManufacturerKeyTextBox.TabIndex = 59;
            this.toManufacturerKeyTextBox.TabStop = false;
            // 
            // fromManufacturerNameComboBox
            // 
            this.fromManufacturerNameComboBox.FormattingEnabled = true;
            this.fromManufacturerNameComboBox.Location = new System.Drawing.Point(179, 124);
            this.fromManufacturerNameComboBox.Name = "fromManufacturerNameComboBox";
            this.fromManufacturerNameComboBox.Size = new System.Drawing.Size(368, 21);
            this.fromManufacturerNameComboBox.TabIndex = 4;
            // 
            // marketNameComboBox
            // 
            this.marketNameComboBox.FormattingEnabled = true;
            this.marketNameComboBox.Location = new System.Drawing.Point(179, 97);
            this.marketNameComboBox.Name = "marketNameComboBox";
            this.marketNameComboBox.Size = new System.Drawing.Size(368, 21);
            this.marketNameComboBox.TabIndex = 3;
            // 
            // productNameComboBox
            // 
            this.productNameComboBox.FormattingEnabled = true;
            this.productNameComboBox.Location = new System.Drawing.Point(179, 44);
            this.productNameComboBox.Name = "productNameComboBox";
            this.productNameComboBox.Size = new System.Drawing.Size(368, 21);
            this.productNameComboBox.TabIndex = 1;
            // 
            // reportingManufacturerNameComboBox
            // 
            this.reportingManufacturerNameComboBox.FormattingEnabled = true;
            this.reportingManufacturerNameComboBox.Location = new System.Drawing.Point(179, 17);
            this.reportingManufacturerNameComboBox.Name = "reportingManufacturerNameComboBox";
            this.reportingManufacturerNameComboBox.Size = new System.Drawing.Size(368, 21);
            this.reportingManufacturerNameComboBox.TabIndex = 0;
            // 
            // toManufacturerNameComboBox
            // 
            this.toManufacturerNameComboBox.FormattingEnabled = true;
            this.toManufacturerNameComboBox.Location = new System.Drawing.Point(179, 151);
            this.toManufacturerNameComboBox.Name = "toManufacturerNameComboBox";
            this.toManufacturerNameComboBox.Size = new System.Drawing.Size(368, 21);
            this.toManufacturerNameComboBox.TabIndex = 5;
            // 
            // fromManufacturerKeyTextBox
            // 
            this.fromManufacturerKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBrandBindingSource, "FromManufacturerKey", true));
            this.fromManufacturerKeyTextBox.Location = new System.Drawing.Point(587, 124);
            this.fromManufacturerKeyTextBox.Name = "fromManufacturerKeyTextBox";
            this.fromManufacturerKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.fromManufacturerKeyTextBox.TabIndex = 67;
            this.fromManufacturerKeyTextBox.TabStop = false;
            // 
            // ManufacturerBrandEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 344);
            this.Controls.Add(this.fromManufacturerKeyTextBox);
            this.Controls.Add(toManufacturerNameLabel);
            this.Controls.Add(this.toManufacturerNameComboBox);
            this.Controls.Add(reportingManufacturerNameLabel);
            this.Controls.Add(this.reportingManufacturerNameComboBox);
            this.Controls.Add(productNameLabel);
            this.Controls.Add(this.productNameComboBox);
            this.Controls.Add(marketNameLabel);
            this.Controls.Add(this.marketNameComboBox);
            this.Controls.Add(fromManufacturerNameLabel);
            this.Controls.Add(this.fromManufacturerNameComboBox);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(brandNameLabel);
            this.Controls.Add(this.brandNameTextBox);
            this.Controls.Add(this.displayBrandCheckBox);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(fromManfacturerKeyLabel);
            this.Controls.Add(this.includedCheckBox);
            this.Controls.Add(manufacturerBrandKeyLabel);
            this.Controls.Add(this.manufacturerBrandKeyTextBox);
            this.Controls.Add(marketKeyLabel);
            this.Controls.Add(this.marketKeyTextBox);
            this.Controls.Add(productKeyLabel);
            this.Controls.Add(this.productKeyTextBox);
            this.Controls.Add(reportingManufacturerKeyLabel);
            this.Controls.Add(this.reportingManufacturerKeyTextBox);
            this.Controls.Add(toManufacturerKeyLabel);
            this.Controls.Add(this.toManufacturerKeyTextBox);
            this.Name = "ManufacturerBrandEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manufacturer Brands";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.toManufacturerKeyTextBox, 0);
            this.Controls.SetChildIndex(toManufacturerKeyLabel, 0);
            this.Controls.SetChildIndex(this.reportingManufacturerKeyTextBox, 0);
            this.Controls.SetChildIndex(reportingManufacturerKeyLabel, 0);
            this.Controls.SetChildIndex(this.productKeyTextBox, 0);
            this.Controls.SetChildIndex(productKeyLabel, 0);
            this.Controls.SetChildIndex(this.marketKeyTextBox, 0);
            this.Controls.SetChildIndex(marketKeyLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerBrandKeyTextBox, 0);
            this.Controls.SetChildIndex(manufacturerBrandKeyLabel, 0);
            this.Controls.SetChildIndex(this.includedCheckBox, 0);
            this.Controls.SetChildIndex(fromManfacturerKeyLabel, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            this.Controls.SetChildIndex(this.displayBrandCheckBox, 0);
            this.Controls.SetChildIndex(this.brandNameTextBox, 0);
            this.Controls.SetChildIndex(brandNameLabel, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            this.Controls.SetChildIndex(this.fromManufacturerNameComboBox, 0);
            this.Controls.SetChildIndex(fromManufacturerNameLabel, 0);
            this.Controls.SetChildIndex(this.marketNameComboBox, 0);
            this.Controls.SetChildIndex(marketNameLabel, 0);
            this.Controls.SetChildIndex(this.productNameComboBox, 0);
            this.Controls.SetChildIndex(productNameLabel, 0);
            this.Controls.SetChildIndex(this.reportingManufacturerNameComboBox, 0);
            this.Controls.SetChildIndex(reportingManufacturerNameLabel, 0);
            this.Controls.SetChildIndex(this.toManufacturerNameComboBox, 0);
            this.Controls.SetChildIndex(toManufacturerNameLabel, 0);
            this.Controls.SetChildIndex(this.fromManufacturerKeyTextBox, 0);
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerBrandBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource manufacturerBrandBindingSource;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.TextBox brandNameTextBox;
        private System.Windows.Forms.CheckBox displayBrandCheckBox;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.CheckBox includedCheckBox;
        private System.Windows.Forms.TextBox manufacturerBrandKeyTextBox;
        private System.Windows.Forms.TextBox marketKeyTextBox;
        private System.Windows.Forms.TextBox productKeyTextBox;
        private System.Windows.Forms.TextBox reportingManufacturerKeyTextBox;
        private System.Windows.Forms.TextBox toManufacturerKeyTextBox;
        private System.Windows.Forms.ComboBox fromManufacturerNameComboBox;
        private System.Windows.Forms.ComboBox marketNameComboBox;
        private System.Windows.Forms.ComboBox productNameComboBox;
        private System.Windows.Forms.ComboBox reportingManufacturerNameComboBox;
        private System.Windows.Forms.ComboBox toManufacturerNameComboBox;
        private System.Windows.Forms.TextBox fromManufacturerKeyTextBox;
    }
}