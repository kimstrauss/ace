﻿namespace AhamMetaDataPL
{
    partial class ManufacturerEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label billableLabel;
            System.Windows.Forms.Label manufacturerAbbreviationLabel;
            System.Windows.Forms.Label manufacturerKeyLabel;
            System.Windows.Forms.Label manufacturerNameLabel;
            System.Windows.Forms.Label uRLLabel;
            this.billableCheckBox = new System.Windows.Forms.CheckBox();
            this.manufacturerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.manufacturerAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.manufacturerKeyTextBox = new System.Windows.Forms.TextBox();
            this.manufacturerNameTextBox = new System.Windows.Forms.TextBox();
            this.uRLTextBox = new System.Windows.Forms.TextBox();
            billableLabel = new System.Windows.Forms.Label();
            manufacturerAbbreviationLabel = new System.Windows.Forms.Label();
            manufacturerKeyLabel = new System.Windows.Forms.Label();
            manufacturerNameLabel = new System.Windows.Forms.Label();
            uRLLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(395, 181);
            this.btnOK.TabIndex = 5;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(476, 181);
            this.btnCancel.TabIndex = 6;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 4;
            // 
            // billableLabel
            // 
            billableLabel.AutoSize = true;
            billableLabel.Location = new System.Drawing.Point(12, 71);
            billableLabel.Name = "billableLabel";
            billableLabel.Size = new System.Drawing.Size(43, 13);
            billableLabel.TabIndex = 30;
            billableLabel.Text = "Billable:";
            // 
            // manufacturerAbbreviationLabel
            // 
            manufacturerAbbreviationLabel.AutoSize = true;
            manufacturerAbbreviationLabel.Location = new System.Drawing.Point(12, 15);
            manufacturerAbbreviationLabel.Name = "manufacturerAbbreviationLabel";
            manufacturerAbbreviationLabel.Size = new System.Drawing.Size(135, 13);
            manufacturerAbbreviationLabel.TabIndex = 32;
            manufacturerAbbreviationLabel.Text = "Manufacturer Abbreviation:";
            // 
            // manufacturerKeyLabel
            // 
            manufacturerKeyLabel.AutoSize = true;
            manufacturerKeyLabel.Location = new System.Drawing.Point(12, 124);
            manufacturerKeyLabel.Name = "manufacturerKeyLabel";
            manufacturerKeyLabel.Size = new System.Drawing.Size(94, 13);
            manufacturerKeyLabel.TabIndex = 34;
            manufacturerKeyLabel.Text = "Manufacturer Key:";
            // 
            // manufacturerNameLabel
            // 
            manufacturerNameLabel.AutoSize = true;
            manufacturerNameLabel.Location = new System.Drawing.Point(12, 41);
            manufacturerNameLabel.Name = "manufacturerNameLabel";
            manufacturerNameLabel.Size = new System.Drawing.Size(104, 13);
            manufacturerNameLabel.TabIndex = 36;
            manufacturerNameLabel.Text = "Manufacturer Name:";
            // 
            // uRLLabel
            // 
            uRLLabel.AutoSize = true;
            uRLLabel.Location = new System.Drawing.Point(12, 98);
            uRLLabel.Name = "uRLLabel";
            uRLLabel.Size = new System.Drawing.Size(32, 13);
            uRLLabel.TabIndex = 38;
            uRLLabel.Text = "URL:";
            // 
            // billableCheckBox
            // 
            this.billableCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerBindingSource, "Billable", true));
            this.billableCheckBox.Location = new System.Drawing.Point(153, 66);
            this.billableCheckBox.Name = "billableCheckBox";
            this.billableCheckBox.Size = new System.Drawing.Size(104, 24);
            this.billableCheckBox.TabIndex = 2;
            this.billableCheckBox.UseVisualStyleBackColor = true;
            // 
            // manufacturerBindingSource
            // 
            this.manufacturerBindingSource.DataSource = typeof(AhamMetaDataDAL.Manufacturer);
            // 
            // manufacturerAbbreviationTextBox
            // 
            this.manufacturerAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBindingSource, "ManufacturerAbbreviation", true));
            this.manufacturerAbbreviationTextBox.Location = new System.Drawing.Point(153, 12);
            this.manufacturerAbbreviationTextBox.Name = "manufacturerAbbreviationTextBox";
            this.manufacturerAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.manufacturerAbbreviationTextBox.TabIndex = 0;
            // 
            // manufacturerKeyTextBox
            // 
            this.manufacturerKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBindingSource, "ManufacturerKey", true));
            this.manufacturerKeyTextBox.Location = new System.Drawing.Point(153, 121);
            this.manufacturerKeyTextBox.Name = "manufacturerKeyTextBox";
            this.manufacturerKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.manufacturerKeyTextBox.TabIndex = 35;
            this.manufacturerKeyTextBox.TabStop = false;
            // 
            // manufacturerNameTextBox
            // 
            this.manufacturerNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBindingSource, "ManufacturerName", true));
            this.manufacturerNameTextBox.Location = new System.Drawing.Point(153, 38);
            this.manufacturerNameTextBox.Name = "manufacturerNameTextBox";
            this.manufacturerNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.manufacturerNameTextBox.TabIndex = 1;
            // 
            // uRLTextBox
            // 
            this.uRLTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerBindingSource, "URL", true));
            this.uRLTextBox.Location = new System.Drawing.Point(153, 95);
            this.uRLTextBox.Name = "uRLTextBox";
            this.uRLTextBox.Size = new System.Drawing.Size(398, 20);
            this.uRLTextBox.TabIndex = 3;
            // 
            // ManufacturerEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 216);
            this.Controls.Add(billableLabel);
            this.Controls.Add(this.billableCheckBox);
            this.Controls.Add(manufacturerAbbreviationLabel);
            this.Controls.Add(this.manufacturerAbbreviationTextBox);
            this.Controls.Add(manufacturerKeyLabel);
            this.Controls.Add(this.manufacturerKeyTextBox);
            this.Controls.Add(manufacturerNameLabel);
            this.Controls.Add(this.manufacturerNameTextBox);
            this.Controls.Add(uRLLabel);
            this.Controls.Add(this.uRLTextBox);
            this.Name = "ManufacturerEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manufacturer";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.uRLTextBox, 0);
            this.Controls.SetChildIndex(uRLLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerNameTextBox, 0);
            this.Controls.SetChildIndex(manufacturerNameLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerKeyTextBox, 0);
            this.Controls.SetChildIndex(manufacturerKeyLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(manufacturerAbbreviationLabel, 0);
            this.Controls.SetChildIndex(this.billableCheckBox, 0);
            this.Controls.SetChildIndex(billableLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource manufacturerBindingSource;
        private System.Windows.Forms.CheckBox billableCheckBox;
        private System.Windows.Forms.TextBox manufacturerAbbreviationTextBox;
        private System.Windows.Forms.TextBox manufacturerKeyTextBox;
        private System.Windows.Forms.TextBox manufacturerNameTextBox;
        private System.Windows.Forms.TextBox uRLTextBox;
    }
}