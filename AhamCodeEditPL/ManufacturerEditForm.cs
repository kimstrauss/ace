﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ManufacturerEditForm : HaiObjectEditForm
    {
        private Manufacturer _manufacturerToEdit;

        public ManufacturerEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _manufacturerToEdit = (Manufacturer)parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            manufacturerBindingSource.DataSource = _manufacturerToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (manufacturerNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (manufacturerAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_manufacturerToEdit);
            _manufacturerToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
