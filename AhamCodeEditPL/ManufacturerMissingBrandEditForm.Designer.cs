﻿namespace AhamMetaDataPL
{
    partial class ManufacturerMissingBrandEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label brandNameLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label manufacturerMissingBrandKeyLabel;
            System.Windows.Forms.Label marketKeyLabel;
            System.Windows.Forms.Label marketNameLabel;
            System.Windows.Forms.Label productKeyLabel;
            System.Windows.Forms.Label productNameLabel;
            this.manufacturerMissingBrandBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.brandNameTextBox = new System.Windows.Forms.TextBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.isBrandCheckBox = new System.Windows.Forms.CheckBox();
            this.manufacturerMissingBrandKeyTextBox = new System.Windows.Forms.TextBox();
            this.marketKeyTextBox = new System.Windows.Forms.TextBox();
            this.marketNameComboBox = new System.Windows.Forms.ComboBox();
            this.productKeyTextBox = new System.Windows.Forms.TextBox();
            this.productNameComboBox = new System.Windows.Forms.ComboBox();
            beginDateLabel = new System.Windows.Forms.Label();
            brandNameLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            manufacturerMissingBrandKeyLabel = new System.Windows.Forms.Label();
            marketKeyLabel = new System.Windows.Forms.Label();
            marketNameLabel = new System.Windows.Forms.Label();
            productKeyLabel = new System.Windows.Forms.Label();
            productNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerMissingBrandBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(371, 225);
            this.btnOK.TabIndex = 6;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(452, 225);
            this.btnCancel.TabIndex = 7;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 225);
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(30, 141);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 30;
            beginDateLabel.Text = "Begin Date:";
            // 
            // brandNameLabel
            // 
            brandNameLabel.AutoSize = true;
            brandNameLabel.Location = new System.Drawing.Point(13, 62);
            brandNameLabel.Name = "brandNameLabel";
            brandNameLabel.Size = new System.Drawing.Size(69, 13);
            brandNameLabel.TabIndex = 32;
            brandNameLabel.Text = "Brand Name:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(202, 141);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 34;
            endDateLabel.Text = "End Date:";
            // 
            // manufacturerMissingBrandKeyLabel
            // 
            manufacturerMissingBrandKeyLabel.AutoSize = true;
            manufacturerMissingBrandKeyLabel.Location = new System.Drawing.Point(317, 181);
            manufacturerMissingBrandKeyLabel.Name = "manufacturerMissingBrandKeyLabel";
            manufacturerMissingBrandKeyLabel.Size = new System.Drawing.Size(163, 13);
            manufacturerMissingBrandKeyLabel.TabIndex = 38;
            manufacturerMissingBrandKeyLabel.Text = "Manufacturer Missing Brand Key:";
            // 
            // marketKeyLabel
            // 
            marketKeyLabel.AutoSize = true;
            marketKeyLabel.Location = new System.Drawing.Point(454, 95);
            marketKeyLabel.Name = "marketKeyLabel";
            marketKeyLabel.Size = new System.Drawing.Size(28, 13);
            marketKeyLabel.TabIndex = 40;
            marketKeyLabel.Text = "Key:";
            // 
            // marketNameLabel
            // 
            marketNameLabel.AutoSize = true;
            marketNameLabel.Location = new System.Drawing.Point(13, 95);
            marketNameLabel.Name = "marketNameLabel";
            marketNameLabel.Size = new System.Drawing.Size(74, 13);
            marketNameLabel.TabIndex = 42;
            marketNameLabel.Text = "Market Name:";
            // 
            // productKeyLabel
            // 
            productKeyLabel.AutoSize = true;
            productKeyLabel.Location = new System.Drawing.Point(454, 28);
            productKeyLabel.Name = "productKeyLabel";
            productKeyLabel.Size = new System.Drawing.Size(28, 13);
            productKeyLabel.TabIndex = 44;
            productKeyLabel.Text = "Key:";
            // 
            // productNameLabel
            // 
            productNameLabel.AutoSize = true;
            productNameLabel.Location = new System.Drawing.Point(13, 26);
            productNameLabel.Name = "productNameLabel";
            productNameLabel.Size = new System.Drawing.Size(78, 13);
            productNameLabel.TabIndex = 46;
            productNameLabel.Text = "Product Name:";
            // 
            // manufacturerMissingBrandBindingSource
            // 
            this.manufacturerMissingBrandBindingSource.DataSource = typeof(AhamMetaDataDAL.ManufacturerMissingBrand);
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerMissingBrandBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(98, 138);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(94, 20);
            this.beginDateTextBox.TabIndex = 3;
            // 
            // brandNameTextBox
            // 
            this.brandNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerMissingBrandBindingSource, "BrandName", true));
            this.brandNameTextBox.Location = new System.Drawing.Point(98, 59);
            this.brandNameTextBox.Name = "brandNameTextBox";
            this.brandNameTextBox.Size = new System.Drawing.Size(429, 20);
            this.brandNameTextBox.TabIndex = 1;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerMissingBrandBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(260, 138);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(94, 20);
            this.endDateTextBox.TabIndex = 4;
            // 
            // isBrandCheckBox
            // 
            this.isBrandCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerMissingBrandBindingSource, "IsBrand", true));
            this.isBrandCheckBox.Location = new System.Drawing.Point(371, 136);
            this.isBrandCheckBox.Name = "isBrandCheckBox";
            this.isBrandCheckBox.Size = new System.Drawing.Size(121, 24);
            this.isBrandCheckBox.TabIndex = 5;
            this.isBrandCheckBox.Text = "Is brand";
            this.isBrandCheckBox.UseVisualStyleBackColor = true;
            // 
            // manufacturerMissingBrandKeyTextBox
            // 
            this.manufacturerMissingBrandKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerMissingBrandBindingSource, "ManufacturerMissingBrandKey", true));
            this.manufacturerMissingBrandKeyTextBox.Location = new System.Drawing.Point(484, 178);
            this.manufacturerMissingBrandKeyTextBox.Name = "manufacturerMissingBrandKeyTextBox";
            this.manufacturerMissingBrandKeyTextBox.Size = new System.Drawing.Size(43, 20);
            this.manufacturerMissingBrandKeyTextBox.TabIndex = 39;
            this.manufacturerMissingBrandKeyTextBox.TabStop = false;
            // 
            // marketKeyTextBox
            // 
            this.marketKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerMissingBrandBindingSource, "MarketKey", true));
            this.marketKeyTextBox.Location = new System.Drawing.Point(484, 92);
            this.marketKeyTextBox.Name = "marketKeyTextBox";
            this.marketKeyTextBox.Size = new System.Drawing.Size(43, 20);
            this.marketKeyTextBox.TabIndex = 41;
            this.marketKeyTextBox.TabStop = false;
            // 
            // marketNameComboBox
            // 
            this.marketNameComboBox.FormattingEnabled = true;
            this.marketNameComboBox.Location = new System.Drawing.Point(98, 92);
            this.marketNameComboBox.Name = "marketNameComboBox";
            this.marketNameComboBox.Size = new System.Drawing.Size(345, 21);
            this.marketNameComboBox.TabIndex = 2;
            // 
            // productKeyTextBox
            // 
            this.productKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerMissingBrandBindingSource, "ProductKey", true));
            this.productKeyTextBox.Location = new System.Drawing.Point(484, 26);
            this.productKeyTextBox.Name = "productKeyTextBox";
            this.productKeyTextBox.Size = new System.Drawing.Size(43, 20);
            this.productKeyTextBox.TabIndex = 45;
            this.productKeyTextBox.TabStop = false;
            // 
            // productNameComboBox
            // 
            this.productNameComboBox.FormattingEnabled = true;
            this.productNameComboBox.Location = new System.Drawing.Point(98, 23);
            this.productNameComboBox.Name = "productNameComboBox";
            this.productNameComboBox.Size = new System.Drawing.Size(345, 21);
            this.productNameComboBox.TabIndex = 0;
            // 
            // ManufacturerMissingBrandEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 260);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(brandNameLabel);
            this.Controls.Add(this.brandNameTextBox);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(this.isBrandCheckBox);
            this.Controls.Add(manufacturerMissingBrandKeyLabel);
            this.Controls.Add(this.manufacturerMissingBrandKeyTextBox);
            this.Controls.Add(marketKeyLabel);
            this.Controls.Add(this.marketKeyTextBox);
            this.Controls.Add(marketNameLabel);
            this.Controls.Add(this.marketNameComboBox);
            this.Controls.Add(productKeyLabel);
            this.Controls.Add(this.productKeyTextBox);
            this.Controls.Add(productNameLabel);
            this.Controls.Add(this.productNameComboBox);
            this.Name = "ManufacturerMissingBrandEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manufacturer Missing Brand";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.productNameComboBox, 0);
            this.Controls.SetChildIndex(productNameLabel, 0);
            this.Controls.SetChildIndex(this.productKeyTextBox, 0);
            this.Controls.SetChildIndex(productKeyLabel, 0);
            this.Controls.SetChildIndex(this.marketNameComboBox, 0);
            this.Controls.SetChildIndex(marketNameLabel, 0);
            this.Controls.SetChildIndex(this.marketKeyTextBox, 0);
            this.Controls.SetChildIndex(marketKeyLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerMissingBrandKeyTextBox, 0);
            this.Controls.SetChildIndex(manufacturerMissingBrandKeyLabel, 0);
            this.Controls.SetChildIndex(this.isBrandCheckBox, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            this.Controls.SetChildIndex(this.brandNameTextBox, 0);
            this.Controls.SetChildIndex(brandNameLabel, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerMissingBrandBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource manufacturerMissingBrandBindingSource;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.TextBox brandNameTextBox;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.CheckBox isBrandCheckBox;
        private System.Windows.Forms.TextBox manufacturerMissingBrandKeyTextBox;
        private System.Windows.Forms.TextBox marketKeyTextBox;
        private System.Windows.Forms.ComboBox marketNameComboBox;
        private System.Windows.Forms.TextBox productKeyTextBox;
        private System.Windows.Forms.ComboBox productNameComboBox;
    }
}