﻿using System;
using System.Windows.Forms;

using System.Collections;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ManufacturerMissingBrandEditForm : HaiObjectEditForm
    {
        private ManufacturerMissingBrand _manufacturerMissingBrandToEdit;

        public ManufacturerMissingBrandEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _manufacturerMissingBrandToEdit = (ManufacturerMissingBrand)_parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);          // the form needs to be prepared before assigning values to controls
            manufacturerMissingBrandBindingSource.DataSource = _manufacturerMissingBrandToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = ds.GetDataList(HaiBusinessObjectType.Product_Aham);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Product>(productNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ProductName", "ProductKey", productKeyTextBox, _manufacturerMissingBrandToEdit.ProductName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer brand.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Market);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Market>(marketNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "MarketName", "MarketKey", marketKeyTextBox, _manufacturerMissingBrandToEdit.MarketName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer brand.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            bool abort = false;

            // validate the BrandName
            if (!brandNameTextBox.ReadOnly)
            {
                if (!brandNameTextBox.ReadOnly && (brandNameTextBox.Text.Trim() == string.Empty))
                {
                    MessageBox.Show("A brand name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    abort = true;
                }
            }

            message = ValidateDaterange(_manufacturerMissingBrandToEdit, DateRangeValidationType.Daily);
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (productNameComboBox.Text.Trim() == string.Empty && productNameComboBox.Items.Count > 0)
            {
                MessageBox.Show("A product must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (marketNameComboBox.Text.Trim() == string.Empty && marketNameComboBox.Items.Count > 0)
            {
                MessageBox.Show("A market must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            // set the property values in the object that was the target of the edit
            MarkChangedProperties(_manufacturerMissingBrandToEdit);      // this is a multi-edit form
            _manufacturerMissingBrandToEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
