﻿namespace AhamMetaDataPL
{
    partial class ManufacturerReportEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label activitySetKeyLabel;
            System.Windows.Forms.Label activitySetNameLabel;
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label catalogSetKeyLabel;
            System.Windows.Forms.Label catalogSetNameLabel;
            System.Windows.Forms.Label channelSetKeyLabel;
            System.Windows.Forms.Label channelSetNameLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label frequencyKeyLabel;
            System.Windows.Forms.Label frequencyNameLabel;
            System.Windows.Forms.Label geographySetKeyLabel;
            System.Windows.Forms.Label geographySetNameLabel;
            System.Windows.Forms.Label manufacturerKeyLabel;
            System.Windows.Forms.Label manufacturerNameLabel;
            System.Windows.Forms.Label manufacturerReportKeyLabel;
            System.Windows.Forms.Label measureSetKeyLabel;
            System.Windows.Forms.Label measureSetNameLabel;
            System.Windows.Forms.Label productKeyLabel;
            System.Windows.Forms.Label productNameLabel;
            System.Windows.Forms.Label reportNameLabel;
            System.Windows.Forms.Label statusLabel;
            System.Windows.Forms.Label useSetKeyLabel;
            System.Windows.Forms.Label useSetNameLabel;
            System.Windows.Forms.Label marketSetNameLabel;
            System.Windows.Forms.Label marketSetKeyLabel;
            System.Windows.Forms.Label sizeSetNameLabel;
            System.Windows.Forms.Label sizeSetKeyLabel;
            System.Windows.Forms.Label inputFormNameLabel;
            System.Windows.Forms.Label inputFormKeyLabel;
            this.activitySetKeyTextBox = new System.Windows.Forms.TextBox();
            this.manufacturerReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.catalogSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.channelSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.frequencyKeyTextBox = new System.Windows.Forms.TextBox();
            this.geographySetKeyTextBox = new System.Windows.Forms.TextBox();
            this.manufacturerKeyTextBox = new System.Windows.Forms.TextBox();
            this.manufacturerReportKeyTextBox = new System.Windows.Forms.TextBox();
            this.measureSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.productKeyTextBox = new System.Windows.Forms.TextBox();
            this.reportNameTextBox = new System.Windows.Forms.TextBox();
            this.serialNumberCheckBox = new System.Windows.Forms.CheckBox();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.useSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.activitySetNameComboBox = new System.Windows.Forms.ComboBox();
            this.catalogSetNameComboBox = new System.Windows.Forms.ComboBox();
            this.channelSetNameComboBox = new System.Windows.Forms.ComboBox();
            this.frequencyNameComboBox = new System.Windows.Forms.ComboBox();
            this.geographySetNameComboBox = new System.Windows.Forms.ComboBox();
            this.manufacturerNameComboBox = new System.Windows.Forms.ComboBox();
            this.measureSetNameComboBox = new System.Windows.Forms.ComboBox();
            this.productNameComboBox = new System.Windows.Forms.ComboBox();
            this.useSetNameComboBox = new System.Windows.Forms.ComboBox();
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.marketSetNameComboBox = new System.Windows.Forms.ComboBox();
            this.marketSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.sizeSetNameComboBox = new System.Windows.Forms.ComboBox();
            this.sizeSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.inputFormNameComboBox = new System.Windows.Forms.ComboBox();
            this.inputFormKeyTextBox = new System.Windows.Forms.TextBox();
            this.masterCheckBox = new System.Windows.Forms.CheckBox();
            this.createWeeklyCheckBox = new System.Windows.Forms.CheckBox();
            this.yTDCheckBox = new System.Windows.Forms.CheckBox();
            this.reconcileCheckBox = new System.Windows.Forms.CheckBox();
            this.calendarCheckBox = new System.Windows.Forms.CheckBox();
            this.forecastedCheckBox = new System.Windows.Forms.CheckBox();
            this.participationCheckedCheckBox = new System.Windows.Forms.CheckBox();
            this.btnCreateName = new System.Windows.Forms.Button();
            this.SkipReportCardCheckBox = new System.Windows.Forms.CheckBox();
            activitySetKeyLabel = new System.Windows.Forms.Label();
            activitySetNameLabel = new System.Windows.Forms.Label();
            beginDateLabel = new System.Windows.Forms.Label();
            catalogSetKeyLabel = new System.Windows.Forms.Label();
            catalogSetNameLabel = new System.Windows.Forms.Label();
            channelSetKeyLabel = new System.Windows.Forms.Label();
            channelSetNameLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            frequencyKeyLabel = new System.Windows.Forms.Label();
            frequencyNameLabel = new System.Windows.Forms.Label();
            geographySetKeyLabel = new System.Windows.Forms.Label();
            geographySetNameLabel = new System.Windows.Forms.Label();
            manufacturerKeyLabel = new System.Windows.Forms.Label();
            manufacturerNameLabel = new System.Windows.Forms.Label();
            manufacturerReportKeyLabel = new System.Windows.Forms.Label();
            measureSetKeyLabel = new System.Windows.Forms.Label();
            measureSetNameLabel = new System.Windows.Forms.Label();
            productKeyLabel = new System.Windows.Forms.Label();
            productNameLabel = new System.Windows.Forms.Label();
            reportNameLabel = new System.Windows.Forms.Label();
            statusLabel = new System.Windows.Forms.Label();
            useSetKeyLabel = new System.Windows.Forms.Label();
            useSetNameLabel = new System.Windows.Forms.Label();
            marketSetNameLabel = new System.Windows.Forms.Label();
            marketSetKeyLabel = new System.Windows.Forms.Label();
            sizeSetNameLabel = new System.Windows.Forms.Label();
            sizeSetKeyLabel = new System.Windows.Forms.Label();
            inputFormNameLabel = new System.Windows.Forms.Label();
            inputFormKeyLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerReportBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(450, 525);
            this.btnOK.TabIndex = 26;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(531, 525);
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 525);
            this.btnInsertNull.TabIndex = 28;
            // 
            // activitySetKeyLabel
            // 
            activitySetKeyLabel.AutoSize = true;
            activitySetKeyLabel.Location = new System.Drawing.Point(515, 69);
            activitySetKeyLabel.Name = "activitySetKeyLabel";
            activitySetKeyLabel.Size = new System.Drawing.Size(28, 13);
            activitySetKeyLabel.TabIndex = 16;
            activitySetKeyLabel.Text = "Key:";
            // 
            // activitySetNameLabel
            // 
            activitySetNameLabel.AutoSize = true;
            activitySetNameLabel.Location = new System.Drawing.Point(11, 69);
            activitySetNameLabel.Name = "activitySetNameLabel";
            activitySetNameLabel.Size = new System.Drawing.Size(94, 13);
            activitySetNameLabel.TabIndex = 32;
            activitySetNameLabel.Text = "Activity Set Name:";
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(143, 443);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 34;
            beginDateLabel.Text = "Begin Date:";
            // 
            // catalogSetKeyLabel
            // 
            catalogSetKeyLabel.AutoSize = true;
            catalogSetKeyLabel.Location = new System.Drawing.Point(515, 96);
            catalogSetKeyLabel.Name = "catalogSetKeyLabel";
            catalogSetKeyLabel.Size = new System.Drawing.Size(28, 13);
            catalogSetKeyLabel.TabIndex = 17;
            catalogSetKeyLabel.Text = "Key:";
            // 
            // catalogSetNameLabel
            // 
            catalogSetNameLabel.AutoSize = true;
            catalogSetNameLabel.Location = new System.Drawing.Point(11, 96);
            catalogSetNameLabel.Name = "catalogSetNameLabel";
            catalogSetNameLabel.Size = new System.Drawing.Size(96, 13);
            catalogSetNameLabel.TabIndex = 38;
            catalogSetNameLabel.Text = "Catalog Set Name:";
            // 
            // channelSetKeyLabel
            // 
            channelSetKeyLabel.AutoSize = true;
            channelSetKeyLabel.Location = new System.Drawing.Point(515, 124);
            channelSetKeyLabel.Name = "channelSetKeyLabel";
            channelSetKeyLabel.Size = new System.Drawing.Size(28, 13);
            channelSetKeyLabel.TabIndex = 18;
            channelSetKeyLabel.Text = "Key:";
            // 
            // channelSetNameLabel
            // 
            channelSetNameLabel.AutoSize = true;
            channelSetNameLabel.Location = new System.Drawing.Point(11, 124);
            channelSetNameLabel.Name = "channelSetNameLabel";
            channelSetNameLabel.Size = new System.Drawing.Size(99, 13);
            channelSetNameLabel.TabIndex = 42;
            channelSetNameLabel.Text = "Channel Set Name:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(326, 443);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 44;
            endDateLabel.Text = "End Date:";
            // 
            // frequencyKeyLabel
            // 
            frequencyKeyLabel.AutoSize = true;
            frequencyKeyLabel.Location = new System.Drawing.Point(515, 151);
            frequencyKeyLabel.Name = "frequencyKeyLabel";
            frequencyKeyLabel.Size = new System.Drawing.Size(28, 13);
            frequencyKeyLabel.TabIndex = 19;
            frequencyKeyLabel.Text = "Key:";
            // 
            // frequencyNameLabel
            // 
            frequencyNameLabel.AutoSize = true;
            frequencyNameLabel.Location = new System.Drawing.Point(11, 151);
            frequencyNameLabel.Name = "frequencyNameLabel";
            frequencyNameLabel.Size = new System.Drawing.Size(91, 13);
            frequencyNameLabel.TabIndex = 48;
            frequencyNameLabel.Text = "Frequency Name:";
            // 
            // geographySetKeyLabel
            // 
            geographySetKeyLabel.AutoSize = true;
            geographySetKeyLabel.Location = new System.Drawing.Point(515, 205);
            geographySetKeyLabel.Name = "geographySetKeyLabel";
            geographySetKeyLabel.Size = new System.Drawing.Size(28, 13);
            geographySetKeyLabel.TabIndex = 50;
            geographySetKeyLabel.Text = "Key:";
            // 
            // geographySetNameLabel
            // 
            geographySetNameLabel.AutoSize = true;
            geographySetNameLabel.Location = new System.Drawing.Point(11, 205);
            geographySetNameLabel.Name = "geographySetNameLabel";
            geographySetNameLabel.Size = new System.Drawing.Size(112, 13);
            geographySetNameLabel.TabIndex = 52;
            geographySetNameLabel.Text = "Geography Set Name:";
            // 
            // manufacturerKeyLabel
            // 
            manufacturerKeyLabel.AutoSize = true;
            manufacturerKeyLabel.Location = new System.Drawing.Point(515, 16);
            manufacturerKeyLabel.Name = "manufacturerKeyLabel";
            manufacturerKeyLabel.Size = new System.Drawing.Size(28, 13);
            manufacturerKeyLabel.TabIndex = 14;
            manufacturerKeyLabel.Text = "Key:";
            // 
            // manufacturerNameLabel
            // 
            manufacturerNameLabel.AutoSize = true;
            manufacturerNameLabel.Location = new System.Drawing.Point(11, 16);
            manufacturerNameLabel.Name = "manufacturerNameLabel";
            manufacturerNameLabel.Size = new System.Drawing.Size(104, 13);
            manufacturerNameLabel.TabIndex = 56;
            manufacturerNameLabel.Text = "Manufacturer Name:";
            // 
            // manufacturerReportKeyLabel
            // 
            manufacturerReportKeyLabel.AutoSize = true;
            manufacturerReportKeyLabel.Location = new System.Drawing.Point(414, 498);
            manufacturerReportKeyLabel.Name = "manufacturerReportKeyLabel";
            manufacturerReportKeyLabel.Size = new System.Drawing.Size(129, 13);
            manufacturerReportKeyLabel.TabIndex = 58;
            manufacturerReportKeyLabel.Text = "Manufacturer Report Key:";
            // 
            // measureSetKeyLabel
            // 
            measureSetKeyLabel.AutoSize = true;
            measureSetKeyLabel.Location = new System.Drawing.Point(515, 177);
            measureSetKeyLabel.Name = "measureSetKeyLabel";
            measureSetKeyLabel.Size = new System.Drawing.Size(28, 13);
            measureSetKeyLabel.TabIndex = 20;
            measureSetKeyLabel.Text = "Key:";
            // 
            // measureSetNameLabel
            // 
            measureSetNameLabel.AutoSize = true;
            measureSetNameLabel.Location = new System.Drawing.Point(11, 177);
            measureSetNameLabel.Name = "measureSetNameLabel";
            measureSetNameLabel.Size = new System.Drawing.Size(101, 13);
            measureSetNameLabel.TabIndex = 62;
            measureSetNameLabel.Text = "Measure Set Name:";
            // 
            // productKeyLabel
            // 
            productKeyLabel.AutoSize = true;
            productKeyLabel.Location = new System.Drawing.Point(515, 42);
            productKeyLabel.Name = "productKeyLabel";
            productKeyLabel.Size = new System.Drawing.Size(28, 13);
            productKeyLabel.TabIndex = 15;
            productKeyLabel.Text = "Key:";
            // 
            // productNameLabel
            // 
            productNameLabel.AutoSize = true;
            productNameLabel.Location = new System.Drawing.Point(11, 42);
            productNameLabel.Name = "productNameLabel";
            productNameLabel.Size = new System.Drawing.Size(78, 13);
            productNameLabel.TabIndex = 66;
            productNameLabel.Text = "Product Name:";
            // 
            // reportNameLabel
            // 
            reportNameLabel.AutoSize = true;
            reportNameLabel.Location = new System.Drawing.Point(9, 466);
            reportNameLabel.Name = "reportNameLabel";
            reportNameLabel.Size = new System.Drawing.Size(73, 13);
            reportNameLabel.TabIndex = 68;
            reportNameLabel.Text = "Report Name:";
            // 
            // statusLabel
            // 
            statusLabel.AutoSize = true;
            statusLabel.Location = new System.Drawing.Point(503, 443);
            statusLabel.Name = "statusLabel";
            statusLabel.Size = new System.Drawing.Size(40, 13);
            statusLabel.TabIndex = 23;
            statusLabel.Text = "Status:";
            // 
            // useSetKeyLabel
            // 
            useSetKeyLabel.AutoSize = true;
            useSetKeyLabel.Location = new System.Drawing.Point(515, 258);
            useSetKeyLabel.Name = "useSetKeyLabel";
            useSetKeyLabel.Size = new System.Drawing.Size(28, 13);
            useSetKeyLabel.TabIndex = 74;
            useSetKeyLabel.Text = "Key:";
            // 
            // useSetNameLabel
            // 
            useSetNameLabel.AutoSize = true;
            useSetNameLabel.Location = new System.Drawing.Point(11, 258);
            useSetNameLabel.Name = "useSetNameLabel";
            useSetNameLabel.Size = new System.Drawing.Size(79, 13);
            useSetNameLabel.TabIndex = 76;
            useSetNameLabel.Text = "Use Set Name:";
            // 
            // marketSetNameLabel
            // 
            marketSetNameLabel.AutoSize = true;
            marketSetNameLabel.Location = new System.Drawing.Point(11, 231);
            marketSetNameLabel.Name = "marketSetNameLabel";
            marketSetNameLabel.Size = new System.Drawing.Size(93, 13);
            marketSetNameLabel.TabIndex = 88;
            marketSetNameLabel.Text = "Market Set Name:";
            // 
            // marketSetKeyLabel
            // 
            marketSetKeyLabel.AutoSize = true;
            marketSetKeyLabel.Location = new System.Drawing.Point(515, 231);
            marketSetKeyLabel.Name = "marketSetKeyLabel";
            marketSetKeyLabel.Size = new System.Drawing.Size(28, 13);
            marketSetKeyLabel.TabIndex = 89;
            marketSetKeyLabel.Text = "Key:";
            // 
            // sizeSetNameLabel
            // 
            sizeSetNameLabel.AutoSize = true;
            sizeSetNameLabel.Location = new System.Drawing.Point(12, 312);
            sizeSetNameLabel.Name = "sizeSetNameLabel";
            sizeSetNameLabel.Size = new System.Drawing.Size(80, 13);
            sizeSetNameLabel.TabIndex = 90;
            sizeSetNameLabel.Text = "Size Set Name:";
            // 
            // sizeSetKeyLabel
            // 
            sizeSetKeyLabel.AutoSize = true;
            sizeSetKeyLabel.Location = new System.Drawing.Point(515, 312);
            sizeSetKeyLabel.Name = "sizeSetKeyLabel";
            sizeSetKeyLabel.Size = new System.Drawing.Size(28, 13);
            sizeSetKeyLabel.TabIndex = 91;
            sizeSetKeyLabel.Text = "Key:";
            // 
            // inputFormNameLabel
            // 
            inputFormNameLabel.AutoSize = true;
            inputFormNameLabel.Location = new System.Drawing.Point(12, 285);
            inputFormNameLabel.Name = "inputFormNameLabel";
            inputFormNameLabel.Size = new System.Drawing.Size(91, 13);
            inputFormNameLabel.TabIndex = 92;
            inputFormNameLabel.Text = "Input Form Name:";
            // 
            // inputFormKeyLabel
            // 
            inputFormKeyLabel.AutoSize = true;
            inputFormKeyLabel.Location = new System.Drawing.Point(515, 285);
            inputFormKeyLabel.Name = "inputFormKeyLabel";
            inputFormKeyLabel.Size = new System.Drawing.Size(28, 13);
            inputFormKeyLabel.TabIndex = 93;
            inputFormKeyLabel.Text = "Key:";
            // 
            // activitySetKeyTextBox
            // 
            this.activitySetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "ActivitySetKey", true));
            this.activitySetKeyTextBox.Location = new System.Drawing.Point(549, 66);
            this.activitySetKeyTextBox.Name = "activitySetKeyTextBox";
            this.activitySetKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.activitySetKeyTextBox.TabIndex = 31;
            this.activitySetKeyTextBox.TabStop = false;
            // 
            // manufacturerReportBindingSource
            // 
            this.manufacturerReportBindingSource.DataSource = typeof(AhamMetaDataDAL.ManufacturerReport);
            // 
            // catalogSetKeyTextBox
            // 
            this.catalogSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "CatalogSetKey", true));
            this.catalogSetKeyTextBox.Location = new System.Drawing.Point(549, 93);
            this.catalogSetKeyTextBox.Name = "catalogSetKeyTextBox";
            this.catalogSetKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.catalogSetKeyTextBox.TabIndex = 37;
            this.catalogSetKeyTextBox.TabStop = false;
            // 
            // channelSetKeyTextBox
            // 
            this.channelSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "ChannelSetKey", true));
            this.channelSetKeyTextBox.Location = new System.Drawing.Point(549, 121);
            this.channelSetKeyTextBox.Name = "channelSetKeyTextBox";
            this.channelSetKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.channelSetKeyTextBox.TabIndex = 41;
            this.channelSetKeyTextBox.TabStop = false;
            // 
            // frequencyKeyTextBox
            // 
            this.frequencyKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "FrequencyKey", true));
            this.frequencyKeyTextBox.Location = new System.Drawing.Point(549, 148);
            this.frequencyKeyTextBox.Name = "frequencyKeyTextBox";
            this.frequencyKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.frequencyKeyTextBox.TabIndex = 47;
            this.frequencyKeyTextBox.TabStop = false;
            // 
            // geographySetKeyTextBox
            // 
            this.geographySetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "GeographySetKey", true));
            this.geographySetKeyTextBox.Location = new System.Drawing.Point(549, 202);
            this.geographySetKeyTextBox.Name = "geographySetKeyTextBox";
            this.geographySetKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.geographySetKeyTextBox.TabIndex = 51;
            this.geographySetKeyTextBox.TabStop = false;
            // 
            // manufacturerKeyTextBox
            // 
            this.manufacturerKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "ManufacturerKey", true));
            this.manufacturerKeyTextBox.Location = new System.Drawing.Point(549, 13);
            this.manufacturerKeyTextBox.Name = "manufacturerKeyTextBox";
            this.manufacturerKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.manufacturerKeyTextBox.TabIndex = 55;
            this.manufacturerKeyTextBox.TabStop = false;
            // 
            // manufacturerReportKeyTextBox
            // 
            this.manufacturerReportKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "ManufacturerReportKey", true));
            this.manufacturerReportKeyTextBox.Location = new System.Drawing.Point(549, 495);
            this.manufacturerReportKeyTextBox.Name = "manufacturerReportKeyTextBox";
            this.manufacturerReportKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.manufacturerReportKeyTextBox.TabIndex = 59;
            this.manufacturerReportKeyTextBox.TabStop = false;
            // 
            // measureSetKeyTextBox
            // 
            this.measureSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "MeasureSetKey", true));
            this.measureSetKeyTextBox.Location = new System.Drawing.Point(549, 174);
            this.measureSetKeyTextBox.Name = "measureSetKeyTextBox";
            this.measureSetKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.measureSetKeyTextBox.TabIndex = 61;
            this.measureSetKeyTextBox.TabStop = false;
            // 
            // productKeyTextBox
            // 
            this.productKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "ProductKey", true));
            this.productKeyTextBox.Location = new System.Drawing.Point(549, 39);
            this.productKeyTextBox.Name = "productKeyTextBox";
            this.productKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.productKeyTextBox.TabIndex = 65;
            this.productKeyTextBox.TabStop = false;
            // 
            // reportNameTextBox
            // 
            this.reportNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "ReportName", true));
            this.reportNameTextBox.Location = new System.Drawing.Point(146, 466);
            this.reportNameTextBox.Name = "reportNameTextBox";
            this.reportNameTextBox.Size = new System.Drawing.Size(456, 20);
            this.reportNameTextBox.TabIndex = 24;
            // 
            // serialNumberCheckBox
            // 
            this.serialNumberCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerReportBindingSource, "SerialNumber", true));
            this.serialNumberCheckBox.Location = new System.Drawing.Point(221, 380);
            this.serialNumberCheckBox.Name = "serialNumberCheckBox";
            this.serialNumberCheckBox.Size = new System.Drawing.Size(96, 24);
            this.serialNumberCheckBox.TabIndex = 17;
            this.serialNumberCheckBox.Text = "Serial number";
            this.serialNumberCheckBox.UseVisualStyleBackColor = true;
            // 
            // statusTextBox
            // 
            this.statusTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.statusTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "Status", true));
            this.statusTextBox.Location = new System.Drawing.Point(549, 440);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.Size = new System.Drawing.Size(53, 20);
            this.statusTextBox.TabIndex = 23;
            this.statusTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.statusTextBox_Validating);
            // 
            // useSetKeyTextBox
            // 
            this.useSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "UseSetKey", true));
            this.useSetKeyTextBox.Location = new System.Drawing.Point(549, 255);
            this.useSetKeyTextBox.Name = "useSetKeyTextBox";
            this.useSetKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.useSetKeyTextBox.TabIndex = 75;
            this.useSetKeyTextBox.TabStop = false;
            // 
            // activitySetNameComboBox
            // 
            this.activitySetNameComboBox.FormattingEnabled = true;
            this.activitySetNameComboBox.Location = new System.Drawing.Point(146, 66);
            this.activitySetNameComboBox.Name = "activitySetNameComboBox";
            this.activitySetNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.activitySetNameComboBox.TabIndex = 2;
            // 
            // catalogSetNameComboBox
            // 
            this.catalogSetNameComboBox.FormattingEnabled = true;
            this.catalogSetNameComboBox.Location = new System.Drawing.Point(146, 93);
            this.catalogSetNameComboBox.Name = "catalogSetNameComboBox";
            this.catalogSetNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.catalogSetNameComboBox.TabIndex = 3;
            // 
            // channelSetNameComboBox
            // 
            this.channelSetNameComboBox.FormattingEnabled = true;
            this.channelSetNameComboBox.Location = new System.Drawing.Point(146, 120);
            this.channelSetNameComboBox.Name = "channelSetNameComboBox";
            this.channelSetNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.channelSetNameComboBox.TabIndex = 4;
            // 
            // frequencyNameComboBox
            // 
            this.frequencyNameComboBox.FormattingEnabled = true;
            this.frequencyNameComboBox.Location = new System.Drawing.Point(146, 147);
            this.frequencyNameComboBox.Name = "frequencyNameComboBox";
            this.frequencyNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.frequencyNameComboBox.TabIndex = 5;
            // 
            // geographySetNameComboBox
            // 
            this.geographySetNameComboBox.FormattingEnabled = true;
            this.geographySetNameComboBox.Location = new System.Drawing.Point(146, 201);
            this.geographySetNameComboBox.Name = "geographySetNameComboBox";
            this.geographySetNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.geographySetNameComboBox.TabIndex = 7;
            // 
            // manufacturerNameComboBox
            // 
            this.manufacturerNameComboBox.FormattingEnabled = true;
            this.manufacturerNameComboBox.Location = new System.Drawing.Point(146, 12);
            this.manufacturerNameComboBox.Name = "manufacturerNameComboBox";
            this.manufacturerNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.manufacturerNameComboBox.TabIndex = 0;
            // 
            // measureSetNameComboBox
            // 
            this.measureSetNameComboBox.FormattingEnabled = true;
            this.measureSetNameComboBox.Location = new System.Drawing.Point(146, 174);
            this.measureSetNameComboBox.Name = "measureSetNameComboBox";
            this.measureSetNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.measureSetNameComboBox.TabIndex = 6;
            // 
            // productNameComboBox
            // 
            this.productNameComboBox.FormattingEnabled = true;
            this.productNameComboBox.Location = new System.Drawing.Point(146, 39);
            this.productNameComboBox.Name = "productNameComboBox";
            this.productNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.productNameComboBox.TabIndex = 1;
            // 
            // useSetNameComboBox
            // 
            this.useSetNameComboBox.FormattingEnabled = true;
            this.useSetNameComboBox.Location = new System.Drawing.Point(146, 255);
            this.useSetNameComboBox.Name = "useSetNameComboBox";
            this.useSetNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.useSetNameComboBox.TabIndex = 9;
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(212, 440);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.beginDateTextBox.TabIndex = 21;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(387, 440);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.endDateTextBox.TabIndex = 22;
            // 
            // marketSetNameComboBox
            // 
            this.marketSetNameComboBox.FormattingEnabled = true;
            this.marketSetNameComboBox.Location = new System.Drawing.Point(146, 228);
            this.marketSetNameComboBox.Name = "marketSetNameComboBox";
            this.marketSetNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.marketSetNameComboBox.TabIndex = 8;
            // 
            // marketSetKeyTextBox
            // 
            this.marketSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "MarketSetKey", true));
            this.marketSetKeyTextBox.Location = new System.Drawing.Point(549, 228);
            this.marketSetKeyTextBox.Name = "marketSetKeyTextBox";
            this.marketSetKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.marketSetKeyTextBox.TabIndex = 90;
            this.marketSetKeyTextBox.TabStop = false;
            // 
            // sizeSetNameComboBox
            // 
            this.sizeSetNameComboBox.FormattingEnabled = true;
            this.sizeSetNameComboBox.Location = new System.Drawing.Point(146, 309);
            this.sizeSetNameComboBox.Name = "sizeSetNameComboBox";
            this.sizeSetNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.sizeSetNameComboBox.TabIndex = 11;
            // 
            // sizeSetKeyTextBox
            // 
            this.sizeSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "SizeSetKey", true));
            this.sizeSetKeyTextBox.Location = new System.Drawing.Point(549, 309);
            this.sizeSetKeyTextBox.Name = "sizeSetKeyTextBox";
            this.sizeSetKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.sizeSetKeyTextBox.TabIndex = 92;
            this.sizeSetKeyTextBox.TabStop = false;
            // 
            // inputFormNameComboBox
            // 
            this.inputFormNameComboBox.FormattingEnabled = true;
            this.inputFormNameComboBox.Location = new System.Drawing.Point(146, 282);
            this.inputFormNameComboBox.Name = "inputFormNameComboBox";
            this.inputFormNameComboBox.Size = new System.Drawing.Size(350, 21);
            this.inputFormNameComboBox.TabIndex = 10;
            // 
            // inputFormKeyTextBox
            // 
            this.inputFormKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.manufacturerReportBindingSource, "InputFormKey", true));
            this.inputFormKeyTextBox.Location = new System.Drawing.Point(549, 282);
            this.inputFormKeyTextBox.Name = "inputFormKeyTextBox";
            this.inputFormKeyTextBox.Size = new System.Drawing.Size(53, 20);
            this.inputFormKeyTextBox.TabIndex = 94;
            this.inputFormKeyTextBox.TabStop = false;
            // 
            // masterCheckBox
            // 
            this.masterCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerReportBindingSource, "Master", true));
            this.masterCheckBox.Location = new System.Drawing.Point(146, 350);
            this.masterCheckBox.Name = "masterCheckBox";
            this.masterCheckBox.Size = new System.Drawing.Size(68, 24);
            this.masterCheckBox.TabIndex = 12;
            this.masterCheckBox.Text = "Master";
            this.masterCheckBox.UseVisualStyleBackColor = true;
            // 
            // createWeeklyCheckBox
            // 
            this.createWeeklyCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerReportBindingSource, "CreateWeekly", true));
            this.createWeeklyCheckBox.Location = new System.Drawing.Point(221, 350);
            this.createWeeklyCheckBox.Name = "createWeeklyCheckBox";
            this.createWeeklyCheckBox.Size = new System.Drawing.Size(104, 24);
            this.createWeeklyCheckBox.TabIndex = 13;
            this.createWeeklyCheckBox.Text = "Create weekly";
            this.createWeeklyCheckBox.UseVisualStyleBackColor = true;
            // 
            // yTDCheckBox
            // 
            this.yTDCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerReportBindingSource, "YTD", true));
            this.yTDCheckBox.Location = new System.Drawing.Point(146, 380);
            this.yTDCheckBox.Name = "yTDCheckBox";
            this.yTDCheckBox.Size = new System.Drawing.Size(53, 24);
            this.yTDCheckBox.TabIndex = 16;
            this.yTDCheckBox.Text = "YTD";
            this.yTDCheckBox.UseVisualStyleBackColor = true;
            // 
            // reconcileCheckBox
            // 
            this.reconcileCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerReportBindingSource, "Reconcile", true));
            this.reconcileCheckBox.Location = new System.Drawing.Point(341, 380);
            this.reconcileCheckBox.Name = "reconcileCheckBox";
            this.reconcileCheckBox.Size = new System.Drawing.Size(77, 24);
            this.reconcileCheckBox.TabIndex = 18;
            this.reconcileCheckBox.Text = "Reconcile";
            this.reconcileCheckBox.UseVisualStyleBackColor = true;
            // 
            // calendarCheckBox
            // 
            this.calendarCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerReportBindingSource, "Calendar", true));
            this.calendarCheckBox.Location = new System.Drawing.Point(341, 350);
            this.calendarCheckBox.Name = "calendarCheckBox";
            this.calendarCheckBox.Size = new System.Drawing.Size(77, 24);
            this.calendarCheckBox.TabIndex = 14;
            this.calendarCheckBox.Text = "Calendar";
            this.calendarCheckBox.UseVisualStyleBackColor = true;
            // 
            // forecastedCheckBox
            // 
            this.forecastedCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerReportBindingSource, "Forecasted", true));
            this.forecastedCheckBox.Location = new System.Drawing.Point(446, 380);
            this.forecastedCheckBox.Name = "forecastedCheckBox";
            this.forecastedCheckBox.Size = new System.Drawing.Size(81, 24);
            this.forecastedCheckBox.TabIndex = 19;
            this.forecastedCheckBox.Text = "Forecasted";
            this.forecastedCheckBox.UseVisualStyleBackColor = true;
            // 
            // participationCheckedCheckBox
            // 
            this.participationCheckedCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerReportBindingSource, "ParticipationChecked", true));
            this.participationCheckedCheckBox.Location = new System.Drawing.Point(446, 350);
            this.participationCheckedCheckBox.Name = "participationCheckedCheckBox";
            this.participationCheckedCheckBox.Size = new System.Drawing.Size(134, 24);
            this.participationCheckedCheckBox.TabIndex = 15;
            this.participationCheckedCheckBox.Text = "Participation checked";
            this.participationCheckedCheckBox.UseVisualStyleBackColor = true;
            // 
            // btnCreateName
            // 
            this.btnCreateName.Location = new System.Drawing.Point(146, 492);
            this.btnCreateName.Name = "btnCreateName";
            this.btnCreateName.Size = new System.Drawing.Size(75, 23);
            this.btnCreateName.TabIndex = 25;
            this.btnCreateName.Text = "Create name";
            this.btnCreateName.UseVisualStyleBackColor = true;
            this.btnCreateName.Click += new System.EventHandler(this.btnCreateName_Click);
            // 
            // SkipReportCardCheckBox
            // 
            this.SkipReportCardCheckBox.AutoSize = true;
            this.SkipReportCardCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.manufacturerReportBindingSource, "SkipReportCard", true));
            this.SkipReportCardCheckBox.Location = new System.Drawing.Point(146, 411);
            this.SkipReportCardCheckBox.Name = "SkipReportCardCheckBox";
            this.SkipReportCardCheckBox.Size = new System.Drawing.Size(107, 17);
            this.SkipReportCardCheckBox.TabIndex = 20;
            this.SkipReportCardCheckBox.Text = "Skip Report Card";
            this.SkipReportCardCheckBox.UseVisualStyleBackColor = true;
            // 
            // ManufacturerReportEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 560);
            this.Controls.Add(this.SkipReportCardCheckBox);
            this.Controls.Add(this.btnCreateName);
            this.Controls.Add(this.participationCheckedCheckBox);
            this.Controls.Add(this.forecastedCheckBox);
            this.Controls.Add(this.calendarCheckBox);
            this.Controls.Add(this.reconcileCheckBox);
            this.Controls.Add(this.yTDCheckBox);
            this.Controls.Add(this.createWeeklyCheckBox);
            this.Controls.Add(this.masterCheckBox);
            this.Controls.Add(inputFormKeyLabel);
            this.Controls.Add(this.inputFormKeyTextBox);
            this.Controls.Add(inputFormNameLabel);
            this.Controls.Add(this.inputFormNameComboBox);
            this.Controls.Add(sizeSetKeyLabel);
            this.Controls.Add(this.sizeSetKeyTextBox);
            this.Controls.Add(sizeSetNameLabel);
            this.Controls.Add(this.sizeSetNameComboBox);
            this.Controls.Add(marketSetKeyLabel);
            this.Controls.Add(this.marketSetKeyTextBox);
            this.Controls.Add(marketSetNameLabel);
            this.Controls.Add(this.marketSetNameComboBox);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(this.useSetNameComboBox);
            this.Controls.Add(this.productNameComboBox);
            this.Controls.Add(this.measureSetNameComboBox);
            this.Controls.Add(this.manufacturerNameComboBox);
            this.Controls.Add(this.geographySetNameComboBox);
            this.Controls.Add(this.frequencyNameComboBox);
            this.Controls.Add(this.channelSetNameComboBox);
            this.Controls.Add(this.catalogSetNameComboBox);
            this.Controls.Add(this.activitySetNameComboBox);
            this.Controls.Add(activitySetKeyLabel);
            this.Controls.Add(this.activitySetKeyTextBox);
            this.Controls.Add(activitySetNameLabel);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(catalogSetKeyLabel);
            this.Controls.Add(this.catalogSetKeyTextBox);
            this.Controls.Add(catalogSetNameLabel);
            this.Controls.Add(channelSetKeyLabel);
            this.Controls.Add(this.channelSetKeyTextBox);
            this.Controls.Add(channelSetNameLabel);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(frequencyKeyLabel);
            this.Controls.Add(this.frequencyKeyTextBox);
            this.Controls.Add(frequencyNameLabel);
            this.Controls.Add(geographySetKeyLabel);
            this.Controls.Add(this.geographySetKeyTextBox);
            this.Controls.Add(geographySetNameLabel);
            this.Controls.Add(manufacturerKeyLabel);
            this.Controls.Add(this.manufacturerKeyTextBox);
            this.Controls.Add(manufacturerNameLabel);
            this.Controls.Add(manufacturerReportKeyLabel);
            this.Controls.Add(this.manufacturerReportKeyTextBox);
            this.Controls.Add(measureSetKeyLabel);
            this.Controls.Add(this.measureSetKeyTextBox);
            this.Controls.Add(measureSetNameLabel);
            this.Controls.Add(productKeyLabel);
            this.Controls.Add(this.productKeyTextBox);
            this.Controls.Add(productNameLabel);
            this.Controls.Add(reportNameLabel);
            this.Controls.Add(this.reportNameTextBox);
            this.Controls.Add(this.serialNumberCheckBox);
            this.Controls.Add(statusLabel);
            this.Controls.Add(this.statusTextBox);
            this.Controls.Add(useSetKeyLabel);
            this.Controls.Add(this.useSetKeyTextBox);
            this.Controls.Add(useSetNameLabel);
            this.Name = "ManufacturerReportEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manufacturer report";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(useSetNameLabel, 0);
            this.Controls.SetChildIndex(this.useSetKeyTextBox, 0);
            this.Controls.SetChildIndex(useSetKeyLabel, 0);
            this.Controls.SetChildIndex(this.statusTextBox, 0);
            this.Controls.SetChildIndex(statusLabel, 0);
            this.Controls.SetChildIndex(this.serialNumberCheckBox, 0);
            this.Controls.SetChildIndex(this.reportNameTextBox, 0);
            this.Controls.SetChildIndex(reportNameLabel, 0);
            this.Controls.SetChildIndex(productNameLabel, 0);
            this.Controls.SetChildIndex(this.productKeyTextBox, 0);
            this.Controls.SetChildIndex(productKeyLabel, 0);
            this.Controls.SetChildIndex(measureSetNameLabel, 0);
            this.Controls.SetChildIndex(this.measureSetKeyTextBox, 0);
            this.Controls.SetChildIndex(measureSetKeyLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerReportKeyTextBox, 0);
            this.Controls.SetChildIndex(manufacturerReportKeyLabel, 0);
            this.Controls.SetChildIndex(manufacturerNameLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerKeyTextBox, 0);
            this.Controls.SetChildIndex(manufacturerKeyLabel, 0);
            this.Controls.SetChildIndex(geographySetNameLabel, 0);
            this.Controls.SetChildIndex(this.geographySetKeyTextBox, 0);
            this.Controls.SetChildIndex(geographySetKeyLabel, 0);
            this.Controls.SetChildIndex(frequencyNameLabel, 0);
            this.Controls.SetChildIndex(this.frequencyKeyTextBox, 0);
            this.Controls.SetChildIndex(frequencyKeyLabel, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            this.Controls.SetChildIndex(channelSetNameLabel, 0);
            this.Controls.SetChildIndex(this.channelSetKeyTextBox, 0);
            this.Controls.SetChildIndex(channelSetKeyLabel, 0);
            this.Controls.SetChildIndex(catalogSetNameLabel, 0);
            this.Controls.SetChildIndex(this.catalogSetKeyTextBox, 0);
            this.Controls.SetChildIndex(catalogSetKeyLabel, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            this.Controls.SetChildIndex(activitySetNameLabel, 0);
            this.Controls.SetChildIndex(this.activitySetKeyTextBox, 0);
            this.Controls.SetChildIndex(activitySetKeyLabel, 0);
            this.Controls.SetChildIndex(this.activitySetNameComboBox, 0);
            this.Controls.SetChildIndex(this.catalogSetNameComboBox, 0);
            this.Controls.SetChildIndex(this.channelSetNameComboBox, 0);
            this.Controls.SetChildIndex(this.frequencyNameComboBox, 0);
            this.Controls.SetChildIndex(this.geographySetNameComboBox, 0);
            this.Controls.SetChildIndex(this.manufacturerNameComboBox, 0);
            this.Controls.SetChildIndex(this.measureSetNameComboBox, 0);
            this.Controls.SetChildIndex(this.productNameComboBox, 0);
            this.Controls.SetChildIndex(this.useSetNameComboBox, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(this.marketSetNameComboBox, 0);
            this.Controls.SetChildIndex(marketSetNameLabel, 0);
            this.Controls.SetChildIndex(this.marketSetKeyTextBox, 0);
            this.Controls.SetChildIndex(marketSetKeyLabel, 0);
            this.Controls.SetChildIndex(this.sizeSetNameComboBox, 0);
            this.Controls.SetChildIndex(sizeSetNameLabel, 0);
            this.Controls.SetChildIndex(this.sizeSetKeyTextBox, 0);
            this.Controls.SetChildIndex(sizeSetKeyLabel, 0);
            this.Controls.SetChildIndex(this.inputFormNameComboBox, 0);
            this.Controls.SetChildIndex(inputFormNameLabel, 0);
            this.Controls.SetChildIndex(this.inputFormKeyTextBox, 0);
            this.Controls.SetChildIndex(inputFormKeyLabel, 0);
            this.Controls.SetChildIndex(this.masterCheckBox, 0);
            this.Controls.SetChildIndex(this.createWeeklyCheckBox, 0);
            this.Controls.SetChildIndex(this.yTDCheckBox, 0);
            this.Controls.SetChildIndex(this.reconcileCheckBox, 0);
            this.Controls.SetChildIndex(this.calendarCheckBox, 0);
            this.Controls.SetChildIndex(this.forecastedCheckBox, 0);
            this.Controls.SetChildIndex(this.participationCheckedCheckBox, 0);
            this.Controls.SetChildIndex(this.btnCreateName, 0);
            this.Controls.SetChildIndex(this.SkipReportCardCheckBox, 0);
            ((System.ComponentModel.ISupportInitialize)(this.manufacturerReportBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource manufacturerReportBindingSource;
        private System.Windows.Forms.TextBox activitySetKeyTextBox;
        private System.Windows.Forms.TextBox catalogSetKeyTextBox;
        private System.Windows.Forms.TextBox channelSetKeyTextBox;
        private System.Windows.Forms.TextBox frequencyKeyTextBox;
        private System.Windows.Forms.TextBox geographySetKeyTextBox;
        private System.Windows.Forms.TextBox manufacturerKeyTextBox;
        private System.Windows.Forms.TextBox manufacturerReportKeyTextBox;
        private System.Windows.Forms.TextBox measureSetKeyTextBox;
        private System.Windows.Forms.TextBox productKeyTextBox;
        private System.Windows.Forms.TextBox reportNameTextBox;
        private System.Windows.Forms.CheckBox serialNumberCheckBox;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.TextBox useSetKeyTextBox;
        private System.Windows.Forms.ComboBox activitySetNameComboBox;
        private System.Windows.Forms.ComboBox catalogSetNameComboBox;
        private System.Windows.Forms.ComboBox channelSetNameComboBox;
        private System.Windows.Forms.ComboBox frequencyNameComboBox;
        private System.Windows.Forms.ComboBox geographySetNameComboBox;
        private System.Windows.Forms.ComboBox manufacturerNameComboBox;
        private System.Windows.Forms.ComboBox measureSetNameComboBox;
        private System.Windows.Forms.ComboBox productNameComboBox;
        private System.Windows.Forms.ComboBox useSetNameComboBox;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.ComboBox marketSetNameComboBox;
        private System.Windows.Forms.TextBox marketSetKeyTextBox;
        private System.Windows.Forms.ComboBox sizeSetNameComboBox;
        private System.Windows.Forms.TextBox sizeSetKeyTextBox;
        private System.Windows.Forms.ComboBox inputFormNameComboBox;
        private System.Windows.Forms.TextBox inputFormKeyTextBox;
        private System.Windows.Forms.CheckBox masterCheckBox;
        private System.Windows.Forms.CheckBox createWeeklyCheckBox;
        private System.Windows.Forms.CheckBox yTDCheckBox;
        private System.Windows.Forms.CheckBox reconcileCheckBox;
        private System.Windows.Forms.CheckBox calendarCheckBox;
        private System.Windows.Forms.CheckBox forecastedCheckBox;
        private System.Windows.Forms.CheckBox participationCheckedCheckBox;
        private System.Windows.Forms.Button btnCreateName;
        private System.Windows.Forms.CheckBox SkipReportCardCheckBox;
    }
}