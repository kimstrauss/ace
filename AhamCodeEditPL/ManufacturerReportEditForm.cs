﻿using System;
using System.Windows.Forms;

using System.Collections;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ManufacturerReportEditForm : HaiObjectEditForm
    {
        private ManufacturerReport _manufacturerReportToEdit;
        private bool _productSelectionChanged;
        private bool _marketSetSelectionChanged;

        public ManufacturerReportEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _manufacturerReportToEdit = (ManufacturerReport)parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);          // the form needs to be prepared before assigning values to controls
            manufacturerReportBindingSource.DataSource = _manufacturerReportToEdit;

            switch (_parameters.GloballyUsefulStuff.DatabaseContext)
            {
                case 1: // AHAM
                    masterCheckBox.Enabled = true;
                    createWeeklyCheckBox.Enabled = true;
                    calendarCheckBox.Enabled = true;
                    participationCheckedCheckBox.Enabled = true;
                    yTDCheckBox.Enabled = true;
                    forecastedCheckBox.Enabled = true;
                    statusTextBox.Enabled = true;
                    reconcileCheckBox.Enabled = true;
                    SkipReportCardCheckBox.Enabled = true;

                    serialNumberCheckBox.Enabled = true;
                    break;

                case 2: // OPEI
                    masterCheckBox.Enabled = false;
                    createWeeklyCheckBox.Enabled = false;
                    calendarCheckBox.Enabled = false;
                    participationCheckedCheckBox.Enabled = false;
                    yTDCheckBox.Enabled = false;
                    forecastedCheckBox.Enabled = false;
                    statusTextBox.Enabled = false;
                    reconcileCheckBox.Enabled = false;
                    SkipReportCardCheckBox.Enabled = false;

                    serialNumberCheckBox.Enabled = true;
                    break;

                default:
                    throw new Exception("Unknown database in ManufacturerReportEditForm");
            }


            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = ds.GetDataList(HaiBusinessObjectType.Manufacturer);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Manufacturer>(manufacturerNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ManufacturerName", "ManufacturerKey", manufacturerKeyTextBox, _manufacturerReportToEdit.ManufacturerName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.CatalogSet);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<CatalogSet>(catalogSetNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "CatalogSetName", "CatalogSetKey", catalogSetKeyTextBox, _manufacturerReportToEdit.CatalogSetName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.ActivitySet_Aham);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<ActivitySet>(activitySetNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ActivitySetName", "ActivitySetKey", activitySetKeyTextBox, _manufacturerReportToEdit.ActivitySetName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.ChannelSet);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<ChannelSet>(channelSetNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ChannelSetName", "ChannelSetKey", channelSetKeyTextBox, _manufacturerReportToEdit.ChannelSetName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.GeographySet);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<GeographySet>(geographySetNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "GeographySetName", "GeographySetKey", geographySetKeyTextBox, _manufacturerReportToEdit.GeographySetName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.MarketSet);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<MarketSet>(marketSetNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "MarketSetName", "MarketSetKey", marketSetKeyTextBox, _manufacturerReportToEdit.MarketSetName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Product_Aham);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Product>(productNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ProductName", "ProductKey", productKeyTextBox, _manufacturerReportToEdit.ProductName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.MeasureSet);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<MeasureSet>(measureSetNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "MeasureSetName", "MeasureSetKey", measureSetKeyTextBox, _manufacturerReportToEdit.MeasureSetName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.UseSet);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<UseSet>(useSetNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "UseSetName", "UseSetKey", useSetKeyTextBox, _manufacturerReportToEdit.UseSetName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Frequency);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Frequency>(frequencyNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "FrequencyName", "FrequencyKey", frequencyKeyTextBox, _manufacturerReportToEdit.FrequencyName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            FillSizeSetNameComboBox();

            result = ds.GetDataList(HaiBusinessObjectType.InputForm);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<InputForm>(inputFormNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "InputFormName", "InputFormKey", inputFormKeyTextBox, _manufacturerReportToEdit.InputFormName.Trim(), "InputFormName");
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            marketSetNameComboBox.Enter+=new EventHandler(marketSetNameComboBox_Enter);
            marketSetNameComboBox.Leave+=new EventHandler(marketSetNameComboBox_Leave);
            marketSetNameComboBox.SelectedValueChanged += new EventHandler(marketSetNameComboBox_SelectedValueChanged);
            productNameComboBox.Enter+=new EventHandler(productNameComboBox_Enter);
            productNameComboBox.Leave+=new EventHandler(productNameComboBox_Leave);
            productNameComboBox.SelectedValueChanged += new EventHandler(productNameComboBox_SelectedValueChanged);

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        void productNameComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            _productSelectionChanged = true;
        }

        void marketSetNameComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            _marketSetSelectionChanged = true;
        }

        void marketSetNameComboBox_Leave(object sender, EventArgs e)
        {
            if (_marketSetSelectionChanged)
                InvalidateSizeSetSelection();
        }

        void marketSetNameComboBox_Enter(object sender, EventArgs e)
        {
            _marketSetSelectionChanged = false;
        }

        void productNameComboBox_Leave(object sender, EventArgs e)
        {
            if (_productSelectionChanged)
                InvalidateSizeSetSelection();
        }

        void productNameComboBox_Enter(object sender, EventArgs e)
        {
            _productSelectionChanged = false;
        }

        private void FillSizeSetNameComboBox()
        {
            int productKey;
            int marketSetKey;

            // only fill the SizeSet combo box if both product and marketset are not mutlivalued.
            if (productNameComboBox.Items.Count == 0 || marketSetNameComboBox.Items.Count == 0)
                return;

            productKey = _manufacturerReportToEdit.ProductKey;
            marketSetKey = _manufacturerReportToEdit.MarketSetKey;

            DataAccessResult result = AhamDataServiceSize.SizeSetFilteredListGet(productKey, marketSetKey, new DataServiceParameters());
            if (result.Success)
            {
                HaiBindingList<SizeSet> sizeSetList = (HaiBindingList<SizeSet>)result.DataList;
                if (sizeSetList.Count > 0)
                {
                    SetupAnyComboBoxForDropDownList<SizeSet>(sizeSetNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "SizeSetName", "SizeSetKey", sizeSetKeyTextBox, _manufacturerReportToEdit.SizeSetName.Trim());
                }
                else
                {
                    sizeSetNameComboBox.DataSource = null;
                    sizeSetKeyTextBox.Text = NoValueToShow;
                }
            }
            else
            {
                MessageBox.Show("Cannot add/edit manufacturer report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }
        }

        private void InvalidateSizeSetSelection()
        {
            FillSizeSetNameComboBox();
            sizeSetNameComboBox.Text = string.Empty;

            // if either product selection or market set selection is multi-valued
            // ... then a size set cannot be selected
            if (productNameComboBox.Items.Count == 0 || marketSetNameComboBox.Items.Count == 0)
                {
                sizeSetNameComboBox.Enabled = false;
                _controllerList["SizeSetKey"].DoNotApplyFormattingToBoundValue = true;  // combobox is disabled; show no value
            }
            else
            {
                sizeSetNameComboBox.Enabled = true;
                _controllerList["SizeSetKey"].DoNotApplyFormattingToBoundValue = false; // combobox is enabled; allow showing formatted value
            }
        }

        private void SizeSetEditEnabler()
        {
            if (productKeyTextBox.Text.Trim() == NoValueToShow || marketSetKeyTextBox.Text.Trim() == NoValueToShow)
            {
                sizeSetNameComboBox.Enabled = false;
                sizeSetKeyTextBox.Text = NoValueToShow;
            }
            else
            {
                FillSizeSetNameComboBox();
                sizeSetKeyTextBox.DataBindings["Text"].ReadValue();
                sizeSetNameComboBox.Enabled = true;
            }
        }

        private void statusTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (btnCancel.Focused)
                return;

            string status = statusTextBox.Text.Trim();
            if (status != "A" && status != "E" && status != "R")
            {
                MessageBox.Show("Status code must be:\n\rA(ctual),\n\rE(stimated), or\n\rR(econile).", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Cancel = true;
            }
        }

        private void btnCreateName_Click(object sender, EventArgs e)
        {
            if (reportNameTextBox.ReadOnly || !reportNameTextBox.Enabled)
            {
                MessageBox.Show("Name field is read only!", "Action not available", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string reportName = string.Empty;

            reportName = frequencyNameComboBox.Text + " " + catalogSetNameComboBox.Text + " " + productNameComboBox.Text 
                + " " + activitySetNameComboBox.Text + " " + " to " + geographySetNameComboBox.Text;
            int maxLength =_browsablePropertyList["ReportName"].MaxStringLength;
            if (reportName.Length > maxLength)
            {
                MessageBox.Show("Generated report name exceeds limit of " + maxLength.ToString() + " characters\n\rthe name has been truncated.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                reportName = reportName.Substring(0, maxLength);
            }
                
            reportNameTextBox.Text = reportName;
            reportNameTextBox.DataBindings["Text"].WriteValue();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;
            string message = string.Empty;

            if (!reportNameTextBox.ReadOnly)
            {
                if (!reportNameTextBox.ReadOnly && reportNameTextBox.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("A report name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    abort = true;
                }
            }

            if (manufacturerNameComboBox.Items.Count > 0 && manufacturerNameComboBox.Text == string.Empty)
            {
                MessageBox.Show("A manufacturer must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (activitySetNameComboBox.Items.Count > 0 && activitySetNameComboBox.Text == string.Empty)
            {
                MessageBox.Show("An activity set must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            /* DMM 1/28/2010 */
            //if (catalogSetNameComboBox.Items.Count > 0 && activitySetNameComboBox.Text == string.Empty)
            //{
            //    MessageBox.Show("An activity set must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    abort = true;
            //}
            if (catalogSetNameComboBox.Items.Count > 0 && catalogSetNameComboBox.Text == string.Empty)
            {
                MessageBox.Show("An catalog set must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (channelSetNameComboBox.Items.Count > 0 && channelSetNameComboBox.Text == string.Empty)
            {
                MessageBox.Show("A channel set must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (frequencyNameComboBox.Items.Count > 0 && frequencyNameComboBox.Text == string.Empty)
            {
                MessageBox.Show("A frequency must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (geographySetNameComboBox.Items.Count > 0 && geographySetNameComboBox.Text == string.Empty)
            {
                MessageBox.Show("A geography set must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (marketSetNameComboBox.Items.Count > 0 && marketSetNameComboBox.Text == string.Empty)
            {
                MessageBox.Show("A market set must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (measureSetNameComboBox.Items.Count > 0 && measureSetNameComboBox.Text == string.Empty)
            {
                MessageBox.Show("A measure set must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (productNameComboBox.Items.Count > 0 && productNameComboBox.Text == string.Empty)
            {
                MessageBox.Show("A product must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (useSetNameComboBox.Items.Count > 0 && useSetNameComboBox.Text == string.Empty)
            {
                MessageBox.Show("A use set must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (inputFormNameComboBox.Items.Count > 0 && inputFormNameComboBox.Text == string.Empty)
            {
                MessageBox.Show("An input form must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if ((sizeSetNameComboBox.Items.Count > 0) && (sizeSetNameComboBox.Text == string.Empty))
            {
                MessageBox.Show("A size set must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (statusTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Status must be set.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            DateRangeValidationType validationType;
            switch (_parameters.GloballyUsefulStuff.DatabaseContext)
            {
                case 1: // AHAM
                    validationType = DateRangeValidationType.Daily;
                    break;

                case 2: // OPEI
                    validationType = DateRangeValidationType.Monthly;
                    break;

                default:
                    throw new Exception("Unknown database context in ManufacturerReportEditForm");
            }

            message = ValidateDaterange(_manufacturerReportToEdit, validationType);
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            if (sizeSetNameComboBox.Items.Count > 0 && sizeSetNameComboBox.Enabled == true)
                sizeSetKeyTextBox.ReadOnly = false;

            // set the property values in the object that was the target of the edit
            MarkChangedProperties(_manufacturerReportToEdit);    // this is a multi-edit form
            _manufacturerReportToEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
