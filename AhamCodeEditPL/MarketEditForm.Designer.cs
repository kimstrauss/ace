﻿namespace AhamMetaDataPL
{
    partial class MarketEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label marketAbbreviationLabel;
            System.Windows.Forms.Label marketKeyLabel;
            System.Windows.Forms.Label marketNameLabel;
            this.marketAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.marketBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.marketKeyTextBox = new System.Windows.Forms.TextBox();
            this.marketNameTextBox = new System.Windows.Forms.TextBox();
            marketAbbreviationLabel = new System.Windows.Forms.Label();
            marketKeyLabel = new System.Windows.Forms.Label();
            marketNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.marketBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 4;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 2;
            // 
            // marketAbbreviationLabel
            // 
            marketAbbreviationLabel.AutoSize = true;
            marketAbbreviationLabel.Location = new System.Drawing.Point(10, 15);
            marketAbbreviationLabel.Name = "marketAbbreviationLabel";
            marketAbbreviationLabel.Size = new System.Drawing.Size(105, 13);
            marketAbbreviationLabel.TabIndex = 30;
            marketAbbreviationLabel.Text = "Market Abbreviation:";
            // 
            // marketKeyLabel
            // 
            marketKeyLabel.AutoSize = true;
            marketKeyLabel.Location = new System.Drawing.Point(10, 67);
            marketKeyLabel.Name = "marketKeyLabel";
            marketKeyLabel.Size = new System.Drawing.Size(64, 13);
            marketKeyLabel.TabIndex = 32;
            marketKeyLabel.Text = "Market Key:";
            // 
            // marketNameLabel
            // 
            marketNameLabel.AutoSize = true;
            marketNameLabel.Location = new System.Drawing.Point(10, 41);
            marketNameLabel.Name = "marketNameLabel";
            marketNameLabel.Size = new System.Drawing.Size(74, 13);
            marketNameLabel.TabIndex = 34;
            marketNameLabel.Text = "Market Name:";
            // 
            // marketAbbreviationTextBox
            // 
            this.marketAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.marketBindingSource, "MarketAbbreviation", true));
            this.marketAbbreviationTextBox.Location = new System.Drawing.Point(121, 12);
            this.marketAbbreviationTextBox.Name = "marketAbbreviationTextBox";
            this.marketAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.marketAbbreviationTextBox.TabIndex = 0;
            // 
            // marketBindingSource
            // 
            this.marketBindingSource.DataSource = typeof(AhamMetaDataDAL.Market);
            // 
            // marketKeyTextBox
            // 
            this.marketKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.marketBindingSource, "MarketKey", true));
            this.marketKeyTextBox.Location = new System.Drawing.Point(121, 64);
            this.marketKeyTextBox.Name = "marketKeyTextBox";
            this.marketKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.marketKeyTextBox.TabIndex = 33;
            this.marketKeyTextBox.TabStop = false;
            // 
            // marketNameTextBox
            // 
            this.marketNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.marketBindingSource, "MarketName", true));
            this.marketNameTextBox.Location = new System.Drawing.Point(121, 38);
            this.marketNameTextBox.Name = "marketNameTextBox";
            this.marketNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.marketNameTextBox.TabIndex = 1;
            // 
            // MarketEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(marketAbbreviationLabel);
            this.Controls.Add(this.marketAbbreviationTextBox);
            this.Controls.Add(marketKeyLabel);
            this.Controls.Add(this.marketKeyTextBox);
            this.Controls.Add(marketNameLabel);
            this.Controls.Add(this.marketNameTextBox);
            this.Name = "MarketEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Define a market";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.marketNameTextBox, 0);
            this.Controls.SetChildIndex(marketNameLabel, 0);
            this.Controls.SetChildIndex(this.marketKeyTextBox, 0);
            this.Controls.SetChildIndex(marketKeyLabel, 0);
            this.Controls.SetChildIndex(this.marketAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(marketAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.marketBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource marketBindingSource;
        private System.Windows.Forms.TextBox marketAbbreviationTextBox;
        private System.Windows.Forms.TextBox marketKeyTextBox;
        private System.Windows.Forms.TextBox marketNameTextBox;
    }
}