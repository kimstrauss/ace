﻿namespace AhamMetaDataPL
{
    partial class MarketSetEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label marketSetAbbreviationLabel;
            System.Windows.Forms.Label marketSetKeyLabel;
            System.Windows.Forms.Label marketSetNameLabel;
            this.marketSetAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.marketSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.marketSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.marketSetNameTextBox = new System.Windows.Forms.TextBox();
            marketSetAbbreviationLabel = new System.Windows.Forms.Label();
            marketSetKeyLabel = new System.Windows.Forms.Label();
            marketSetNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.marketSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 4;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 2;
            // 
            // marketSetAbbreviationLabel
            // 
            marketSetAbbreviationLabel.AutoSize = true;
            marketSetAbbreviationLabel.Location = new System.Drawing.Point(4, 15);
            marketSetAbbreviationLabel.Name = "marketSetAbbreviationLabel";
            marketSetAbbreviationLabel.Size = new System.Drawing.Size(124, 13);
            marketSetAbbreviationLabel.TabIndex = 30;
            marketSetAbbreviationLabel.Text = "Market Set Abbreviation:";
            // 
            // marketSetKeyLabel
            // 
            marketSetKeyLabel.AutoSize = true;
            marketSetKeyLabel.Location = new System.Drawing.Point(4, 67);
            marketSetKeyLabel.Name = "marketSetKeyLabel";
            marketSetKeyLabel.Size = new System.Drawing.Size(83, 13);
            marketSetKeyLabel.TabIndex = 32;
            marketSetKeyLabel.Text = "Market Set Key:";
            // 
            // marketSetNameLabel
            // 
            marketSetNameLabel.AutoSize = true;
            marketSetNameLabel.Location = new System.Drawing.Point(4, 41);
            marketSetNameLabel.Name = "marketSetNameLabel";
            marketSetNameLabel.Size = new System.Drawing.Size(93, 13);
            marketSetNameLabel.TabIndex = 34;
            marketSetNameLabel.Text = "Market Set Name:";
            // 
            // marketSetAbbreviationTextBox
            // 
            this.marketSetAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.marketSetBindingSource, "MarketSetAbbreviation", true));
            this.marketSetAbbreviationTextBox.Location = new System.Drawing.Point(134, 12);
            this.marketSetAbbreviationTextBox.Name = "marketSetAbbreviationTextBox";
            this.marketSetAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.marketSetAbbreviationTextBox.TabIndex = 0;
            // 
            // marketSetBindingSource
            // 
            this.marketSetBindingSource.DataSource = typeof(AhamMetaDataDAL.MarketSet);
            // 
            // marketSetKeyTextBox
            // 
            this.marketSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.marketSetBindingSource, "MarketSetKey", true));
            this.marketSetKeyTextBox.Location = new System.Drawing.Point(134, 64);
            this.marketSetKeyTextBox.Name = "marketSetKeyTextBox";
            this.marketSetKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.marketSetKeyTextBox.TabIndex = 33;
            this.marketSetKeyTextBox.TabStop = false;
            // 
            // marketSetNameTextBox
            // 
            this.marketSetNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.marketSetBindingSource, "MarketSetName", true));
            this.marketSetNameTextBox.Location = new System.Drawing.Point(134, 38);
            this.marketSetNameTextBox.Name = "marketSetNameTextBox";
            this.marketSetNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.marketSetNameTextBox.TabIndex = 1;
            // 
            // MarketSetEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(marketSetAbbreviationLabel);
            this.Controls.Add(this.marketSetAbbreviationTextBox);
            this.Controls.Add(marketSetKeyLabel);
            this.Controls.Add(this.marketSetKeyTextBox);
            this.Controls.Add(marketSetNameLabel);
            this.Controls.Add(this.marketSetNameTextBox);
            this.Name = "MarketSetEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Define a market set";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.marketSetNameTextBox, 0);
            this.Controls.SetChildIndex(marketSetNameLabel, 0);
            this.Controls.SetChildIndex(this.marketSetKeyTextBox, 0);
            this.Controls.SetChildIndex(marketSetKeyLabel, 0);
            this.Controls.SetChildIndex(this.marketSetAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(marketSetAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.marketSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource marketSetBindingSource;
        private System.Windows.Forms.TextBox marketSetAbbreviationTextBox;
        private System.Windows.Forms.TextBox marketSetKeyTextBox;
        private System.Windows.Forms.TextBox marketSetNameTextBox;
    }
}