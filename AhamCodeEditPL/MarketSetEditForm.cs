﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class MarketSetEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private MarketSet _marketSetToEdit;

        public MarketSetEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _marketSetToEdit = (MarketSet)parameters.EditObject;

            this.Load += new System.EventHandler(SetEditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void SetEditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            marketSetBindingSource.DataSource = _marketSetToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (marketSetNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (marketSetAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_marketSetToEdit);
            _marketSetToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }

    }
}
