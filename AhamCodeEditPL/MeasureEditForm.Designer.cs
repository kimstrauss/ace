﻿namespace AhamMetaDataPL
{
    partial class MeasureEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label measureAbbreviationLabel;
            System.Windows.Forms.Label measureKeyLabel;
            System.Windows.Forms.Label measureNameLabel;
            this.measureAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.measureBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.measureKeyTextBox = new System.Windows.Forms.TextBox();
            this.measureNameTextBox = new System.Windows.Forms.TextBox();
            measureAbbreviationLabel = new System.Windows.Forms.Label();
            measureKeyLabel = new System.Windows.Forms.Label();
            measureNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.measureBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 4;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 2;
            // 
            // measureAbbreviationLabel
            // 
            measureAbbreviationLabel.AutoSize = true;
            measureAbbreviationLabel.Location = new System.Drawing.Point(3, 15);
            measureAbbreviationLabel.Name = "measureAbbreviationLabel";
            measureAbbreviationLabel.Size = new System.Drawing.Size(113, 13);
            measureAbbreviationLabel.TabIndex = 30;
            measureAbbreviationLabel.Text = "Measure Abbreviation:";
            // 
            // measureKeyLabel
            // 
            measureKeyLabel.AutoSize = true;
            measureKeyLabel.Location = new System.Drawing.Point(3, 67);
            measureKeyLabel.Name = "measureKeyLabel";
            measureKeyLabel.Size = new System.Drawing.Size(72, 13);
            measureKeyLabel.TabIndex = 32;
            measureKeyLabel.Text = "Measure Key:";
            // 
            // measureNameLabel
            // 
            measureNameLabel.AutoSize = true;
            measureNameLabel.Location = new System.Drawing.Point(3, 41);
            measureNameLabel.Name = "measureNameLabel";
            measureNameLabel.Size = new System.Drawing.Size(82, 13);
            measureNameLabel.TabIndex = 34;
            measureNameLabel.Text = "Measure Name:";
            // 
            // measureAbbreviationTextBox
            // 
            this.measureAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.measureBindingSource, "MeasureAbbreviation", true));
            this.measureAbbreviationTextBox.Location = new System.Drawing.Point(122, 12);
            this.measureAbbreviationTextBox.Name = "measureAbbreviationTextBox";
            this.measureAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.measureAbbreviationTextBox.TabIndex = 0;
            // 
            // measureBindingSource
            // 
            this.measureBindingSource.DataSource = typeof(AhamMetaDataDAL.Measure);
            // 
            // measureKeyTextBox
            // 
            this.measureKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.measureBindingSource, "MeasureKey", true));
            this.measureKeyTextBox.Location = new System.Drawing.Point(122, 64);
            this.measureKeyTextBox.Name = "measureKeyTextBox";
            this.measureKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.measureKeyTextBox.TabIndex = 33;
            this.measureKeyTextBox.TabStop = false;
            // 
            // measureNameTextBox
            // 
            this.measureNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.measureBindingSource, "MeasureName", true));
            this.measureNameTextBox.Location = new System.Drawing.Point(122, 38);
            this.measureNameTextBox.Name = "measureNameTextBox";
            this.measureNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.measureNameTextBox.TabIndex = 1;
            // 
            // MeasureEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(measureAbbreviationLabel);
            this.Controls.Add(this.measureAbbreviationTextBox);
            this.Controls.Add(measureKeyLabel);
            this.Controls.Add(this.measureKeyTextBox);
            this.Controls.Add(measureNameLabel);
            this.Controls.Add(this.measureNameTextBox);
            this.Name = "MeasureEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Measure";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.measureNameTextBox, 0);
            this.Controls.SetChildIndex(measureNameLabel, 0);
            this.Controls.SetChildIndex(this.measureKeyTextBox, 0);
            this.Controls.SetChildIndex(measureKeyLabel, 0);
            this.Controls.SetChildIndex(this.measureAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(measureAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.measureBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource measureBindingSource;
        private System.Windows.Forms.TextBox measureAbbreviationTextBox;
        private System.Windows.Forms.TextBox measureKeyTextBox;
        private System.Windows.Forms.TextBox measureNameTextBox;
    }
}