﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class MeasureEditForm : HaiObjectEditForm
    {
        private Measure _measureToEdit;

        public MeasureEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _measureToEdit = (Measure)parameters.EditObject;

            this.Load += new EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            measureBindingSource.DataSource = _measureToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (measureNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (measureAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_measureToEdit);
            _measureToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
