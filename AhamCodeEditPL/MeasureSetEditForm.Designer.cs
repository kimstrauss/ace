﻿namespace AhamMetaDataPL
{
    partial class MeasureSetEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label measureSetAbbreviationLabel;
            System.Windows.Forms.Label measureSetKeyLabel;
            System.Windows.Forms.Label measureSetNameLabel;
            this.measureSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.measureSetAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.measureSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.measureSetNameTextBox = new System.Windows.Forms.TextBox();
            measureSetAbbreviationLabel = new System.Windows.Forms.Label();
            measureSetKeyLabel = new System.Windows.Forms.Label();
            measureSetNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.measureSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(384, 181);
            this.btnOK.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(465, 181);
            this.btnCancel.TabIndex = 4;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 2;
            // 
            // measureSetAbbreviationLabel
            // 
            measureSetAbbreviationLabel.AutoSize = true;
            measureSetAbbreviationLabel.Location = new System.Drawing.Point(4, 15);
            measureSetAbbreviationLabel.Name = "measureSetAbbreviationLabel";
            measureSetAbbreviationLabel.Size = new System.Drawing.Size(132, 13);
            measureSetAbbreviationLabel.TabIndex = 30;
            measureSetAbbreviationLabel.Text = "Measure Set Abbreviation:";
            // 
            // measureSetKeyLabel
            // 
            measureSetKeyLabel.AutoSize = true;
            measureSetKeyLabel.Location = new System.Drawing.Point(4, 67);
            measureSetKeyLabel.Name = "measureSetKeyLabel";
            measureSetKeyLabel.Size = new System.Drawing.Size(91, 13);
            measureSetKeyLabel.TabIndex = 32;
            measureSetKeyLabel.Text = "Measure Set Key:";
            // 
            // measureSetNameLabel
            // 
            measureSetNameLabel.AutoSize = true;
            measureSetNameLabel.Location = new System.Drawing.Point(4, 41);
            measureSetNameLabel.Name = "measureSetNameLabel";
            measureSetNameLabel.Size = new System.Drawing.Size(101, 13);
            measureSetNameLabel.TabIndex = 34;
            measureSetNameLabel.Text = "Measure Set Name:";
            // 
            // measureSetBindingSource
            // 
            this.measureSetBindingSource.DataSource = typeof(AhamMetaDataDAL.MeasureSet);
            // 
            // measureSetAbbreviationTextBox
            // 
            this.measureSetAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.measureSetBindingSource, "MeasureSetAbbreviation", true));
            this.measureSetAbbreviationTextBox.Location = new System.Drawing.Point(142, 12);
            this.measureSetAbbreviationTextBox.Name = "measureSetAbbreviationTextBox";
            this.measureSetAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.measureSetAbbreviationTextBox.TabIndex = 0;
            // 
            // measureSetKeyTextBox
            // 
            this.measureSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.measureSetBindingSource, "MeasureSetKey", true));
            this.measureSetKeyTextBox.Location = new System.Drawing.Point(142, 64);
            this.measureSetKeyTextBox.Name = "measureSetKeyTextBox";
            this.measureSetKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.measureSetKeyTextBox.TabIndex = 33;
            this.measureSetKeyTextBox.TabStop = false;
            // 
            // measureSetNameTextBox
            // 
            this.measureSetNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.measureSetBindingSource, "MeasureSetName", true));
            this.measureSetNameTextBox.Location = new System.Drawing.Point(142, 38);
            this.measureSetNameTextBox.Name = "measureSetNameTextBox";
            this.measureSetNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.measureSetNameTextBox.TabIndex = 1;
            // 
            // MeasureSetEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 216);
            this.Controls.Add(measureSetAbbreviationLabel);
            this.Controls.Add(this.measureSetAbbreviationTextBox);
            this.Controls.Add(measureSetKeyLabel);
            this.Controls.Add(this.measureSetKeyTextBox);
            this.Controls.Add(measureSetNameLabel);
            this.Controls.Add(this.measureSetNameTextBox);
            this.Name = "MeasureSetEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Measure Set";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.measureSetNameTextBox, 0);
            this.Controls.SetChildIndex(measureSetNameLabel, 0);
            this.Controls.SetChildIndex(this.measureSetKeyTextBox, 0);
            this.Controls.SetChildIndex(measureSetKeyLabel, 0);
            this.Controls.SetChildIndex(this.measureSetAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(measureSetAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.measureSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource measureSetBindingSource;
        private System.Windows.Forms.TextBox measureSetAbbreviationTextBox;
        private System.Windows.Forms.TextBox measureSetKeyTextBox;
        private System.Windows.Forms.TextBox measureSetNameTextBox;

    }
}