﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class MeasureSetMemberEditForm : Form
    {
        private HaiBindingList<MeasureSetMember> _members;

        public MeasureSetMemberEditForm(HaiBindingList<MeasureSetMember> members)
        {
            InitializeComponent();

            _members = members;
        }

        private void MeasureSetMemberEditForm_Load(object sender, EventArgs e)
        {
            bool hasCommonValue = true;
            int valueToTest = _members[0].GroupNumber;

            foreach (MeasureSetMember member in _members)
            {
                if (member.GroupNumber != valueToTest)
                {
                    hasCommonValue = false;
                    break;
                }
            }

            if (hasCommonValue)
                txtGroupNumber.Text = valueToTest.ToString();
            else
                txtGroupNumber.Text = string.Empty;

            lblNumberOfItemsToEdit.Text = "Number of items included: " + _members.Count.ToString();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            int groupNumber;

            if (!int.TryParse(txtGroupNumber.Text, out groupNumber))
            {
                MessageBox.Show("An integer must be entered.", "Data validation error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            foreach (MeasureSetMember member in _members)
            {
                member.GroupNumber = groupNumber;
            }
            
            DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
