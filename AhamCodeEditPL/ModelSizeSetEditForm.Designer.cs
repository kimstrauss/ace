﻿namespace AhamMetaDataPL
{
    partial class ModelSizeSetEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label marketKeyLabel;
            System.Windows.Forms.Label marketNameLabel;
            System.Windows.Forms.Label modelSizeSetKeyLabel;
            System.Windows.Forms.Label modelSizeSetNameLabel;
            System.Windows.Forms.Label productDimensionKeyLabel;
            System.Windows.Forms.Label productDimensionNameLabel;
            this.marketKeyTextBox = new System.Windows.Forms.TextBox();
            this.modelSizeSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.marketNameComboBox = new System.Windows.Forms.ComboBox();
            this.modelSizeSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.modelSizeSetNameTextBox = new System.Windows.Forms.TextBox();
            this.productDimensionKeyTextBox = new System.Windows.Forms.TextBox();
            this.productDimensionNameComboBox = new System.Windows.Forms.ComboBox();
            marketKeyLabel = new System.Windows.Forms.Label();
            marketNameLabel = new System.Windows.Forms.Label();
            modelSizeSetKeyLabel = new System.Windows.Forms.Label();
            modelSizeSetNameLabel = new System.Windows.Forms.Label();
            productDimensionKeyLabel = new System.Windows.Forms.Label();
            productDimensionNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.modelSizeSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(452, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(533, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 3;
            // 
            // marketKeyLabel
            // 
            marketKeyLabel.AutoSize = true;
            marketKeyLabel.Location = new System.Drawing.Point(527, 41);
            marketKeyLabel.Name = "marketKeyLabel";
            marketKeyLabel.Size = new System.Drawing.Size(28, 13);
            marketKeyLabel.TabIndex = 30;
            marketKeyLabel.Text = "Key:";
            // 
            // marketNameLabel
            // 
            marketNameLabel.AutoSize = true;
            marketNameLabel.Location = new System.Drawing.Point(6, 41);
            marketNameLabel.Name = "marketNameLabel";
            marketNameLabel.Size = new System.Drawing.Size(74, 13);
            marketNameLabel.TabIndex = 32;
            marketNameLabel.Text = "Market Name:";
            // 
            // modelSizeSetKeyLabel
            // 
            modelSizeSetKeyLabel.AutoSize = true;
            modelSizeSetKeyLabel.Location = new System.Drawing.Point(6, 95);
            modelSizeSetKeyLabel.Name = "modelSizeSetKeyLabel";
            modelSizeSetKeyLabel.Size = new System.Drawing.Size(102, 13);
            modelSizeSetKeyLabel.TabIndex = 34;
            modelSizeSetKeyLabel.Text = "Model Size Set Key:";
            // 
            // modelSizeSetNameLabel
            // 
            modelSizeSetNameLabel.AutoSize = true;
            modelSizeSetNameLabel.Location = new System.Drawing.Point(6, 15);
            modelSizeSetNameLabel.Name = "modelSizeSetNameLabel";
            modelSizeSetNameLabel.Size = new System.Drawing.Size(112, 13);
            modelSizeSetNameLabel.TabIndex = 36;
            modelSizeSetNameLabel.Text = "Model Size Set Name:";
            // 
            // productDimensionKeyLabel
            // 
            productDimensionKeyLabel.AutoSize = true;
            productDimensionKeyLabel.Location = new System.Drawing.Point(527, 68);
            productDimensionKeyLabel.Name = "productDimensionKeyLabel";
            productDimensionKeyLabel.Size = new System.Drawing.Size(28, 13);
            productDimensionKeyLabel.TabIndex = 38;
            productDimensionKeyLabel.Text = "Key:";
            // 
            // productDimensionNameLabel
            // 
            productDimensionNameLabel.AutoSize = true;
            productDimensionNameLabel.Location = new System.Drawing.Point(6, 68);
            productDimensionNameLabel.Name = "productDimensionNameLabel";
            productDimensionNameLabel.Size = new System.Drawing.Size(130, 13);
            productDimensionNameLabel.TabIndex = 40;
            productDimensionNameLabel.Text = "Product Dimension Name:";
            // 
            // marketKeyTextBox
            // 
            this.marketKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.modelSizeSetBindingSource, "MarketKey", true));
            this.marketKeyTextBox.Location = new System.Drawing.Point(561, 39);
            this.marketKeyTextBox.Name = "marketKeyTextBox";
            this.marketKeyTextBox.Size = new System.Drawing.Size(37, 20);
            this.marketKeyTextBox.TabIndex = 31;
            this.marketKeyTextBox.TabStop = false;
            // 
            // modelSizeSetBindingSource
            // 
            this.modelSizeSetBindingSource.DataSource = typeof(AhamMetaDataDAL.ModelSizeSet);
            // 
            // marketNameComboBox
            // 
            this.marketNameComboBox.FormattingEnabled = true;
            this.marketNameComboBox.Location = new System.Drawing.Point(142, 38);
            this.marketNameComboBox.Name = "marketNameComboBox";
            this.marketNameComboBox.Size = new System.Drawing.Size(379, 21);
            this.marketNameComboBox.TabIndex = 1;
            // 
            // modelSizeSetKeyTextBox
            // 
            this.modelSizeSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.modelSizeSetBindingSource, "ModelSizeSetKey", true));
            this.modelSizeSetKeyTextBox.Location = new System.Drawing.Point(142, 92);
            this.modelSizeSetKeyTextBox.Name = "modelSizeSetKeyTextBox";
            this.modelSizeSetKeyTextBox.Size = new System.Drawing.Size(55, 20);
            this.modelSizeSetKeyTextBox.TabIndex = 35;
            this.modelSizeSetKeyTextBox.TabStop = false;
            // 
            // modelSizeSetNameTextBox
            // 
            this.modelSizeSetNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.modelSizeSetBindingSource, "ModelSizeSetName", true));
            this.modelSizeSetNameTextBox.Location = new System.Drawing.Point(142, 12);
            this.modelSizeSetNameTextBox.Name = "modelSizeSetNameTextBox";
            this.modelSizeSetNameTextBox.Size = new System.Drawing.Size(456, 20);
            this.modelSizeSetNameTextBox.TabIndex = 0;
            // 
            // productDimensionKeyTextBox
            // 
            this.productDimensionKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.modelSizeSetBindingSource, "ProductDimensionKey", true));
            this.productDimensionKeyTextBox.Location = new System.Drawing.Point(561, 66);
            this.productDimensionKeyTextBox.Name = "productDimensionKeyTextBox";
            this.productDimensionKeyTextBox.Size = new System.Drawing.Size(37, 20);
            this.productDimensionKeyTextBox.TabIndex = 39;
            this.productDimensionKeyTextBox.TabStop = false;
            // 
            // productDimensionNameComboBox
            // 
            this.productDimensionNameComboBox.FormattingEnabled = true;
            this.productDimensionNameComboBox.Location = new System.Drawing.Point(142, 65);
            this.productDimensionNameComboBox.Name = "productDimensionNameComboBox";
            this.productDimensionNameComboBox.Size = new System.Drawing.Size(379, 21);
            this.productDimensionNameComboBox.TabIndex = 2;
            // 
            // ModelSizeSetEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 216);
            this.Controls.Add(marketKeyLabel);
            this.Controls.Add(this.marketKeyTextBox);
            this.Controls.Add(marketNameLabel);
            this.Controls.Add(this.marketNameComboBox);
            this.Controls.Add(modelSizeSetKeyLabel);
            this.Controls.Add(this.modelSizeSetKeyTextBox);
            this.Controls.Add(modelSizeSetNameLabel);
            this.Controls.Add(this.modelSizeSetNameTextBox);
            this.Controls.Add(productDimensionKeyLabel);
            this.Controls.Add(this.productDimensionKeyTextBox);
            this.Controls.Add(productDimensionNameLabel);
            this.Controls.Add(this.productDimensionNameComboBox);
            this.Name = "ModelSizeSetEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Model Size Set";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.productDimensionNameComboBox, 0);
            this.Controls.SetChildIndex(productDimensionNameLabel, 0);
            this.Controls.SetChildIndex(this.productDimensionKeyTextBox, 0);
            this.Controls.SetChildIndex(productDimensionKeyLabel, 0);
            this.Controls.SetChildIndex(this.modelSizeSetNameTextBox, 0);
            this.Controls.SetChildIndex(modelSizeSetNameLabel, 0);
            this.Controls.SetChildIndex(this.modelSizeSetKeyTextBox, 0);
            this.Controls.SetChildIndex(modelSizeSetKeyLabel, 0);
            this.Controls.SetChildIndex(this.marketNameComboBox, 0);
            this.Controls.SetChildIndex(marketNameLabel, 0);
            this.Controls.SetChildIndex(this.marketKeyTextBox, 0);
            this.Controls.SetChildIndex(marketKeyLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.modelSizeSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource modelSizeSetBindingSource;
        private System.Windows.Forms.TextBox marketKeyTextBox;
        private System.Windows.Forms.ComboBox marketNameComboBox;
        private System.Windows.Forms.TextBox modelSizeSetKeyTextBox;
        private System.Windows.Forms.TextBox modelSizeSetNameTextBox;
        private System.Windows.Forms.TextBox productDimensionKeyTextBox;
        private System.Windows.Forms.ComboBox productDimensionNameComboBox;
    }
}