﻿namespace AhamMetaDataPL
{
    partial class ModelSizeSetMemberEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblNumberOfItemsToEdit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.sizeLevelTextBox = new System.Windows.Forms.TextBox();
            this.modelSizeSetMemberComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.parentModelSizeSetMemberKeyTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(436, 180);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 24);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(347, 180);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(83, 24);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblNumberOfItemsToEdit
            // 
            this.lblNumberOfItemsToEdit.AutoSize = true;
            this.lblNumberOfItemsToEdit.Location = new System.Drawing.Point(12, 18);
            this.lblNumberOfItemsToEdit.Name = "lblNumberOfItemsToEdit";
            this.lblNumberOfItemsToEdit.Size = new System.Drawing.Size(129, 13);
            this.lblNumberOfItemsToEdit.TabIndex = 6;
            this.lblNumberOfItemsToEdit.Text = "Number of items included:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Size level: ";
            // 
            // sizeLevelTextBox
            // 
            this.sizeLevelTextBox.Location = new System.Drawing.Point(72, 47);
            this.sizeLevelTextBox.Name = "sizeLevelTextBox";
            this.sizeLevelTextBox.Size = new System.Drawing.Size(39, 20);
            this.sizeLevelTextBox.TabIndex = 0;
            // 
            // modelSizeSetMemberComboBox
            // 
            this.modelSizeSetMemberComboBox.FormattingEnabled = true;
            this.modelSizeSetMemberComboBox.Location = new System.Drawing.Point(15, 105);
            this.modelSizeSetMemberComboBox.Name = "modelSizeSetMemberComboBox";
            this.modelSizeSetMemberComboBox.Size = new System.Drawing.Size(451, 21);
            this.modelSizeSetMemberComboBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Parent size member name";
            // 
            // parentModelSizeSetMemberKeyTextBox
            // 
            this.parentModelSizeSetMemberKeyTextBox.Location = new System.Drawing.Point(480, 106);
            this.parentModelSizeSetMemberKeyTextBox.Name = "parentModelSizeSetMemberKeyTextBox";
            this.parentModelSizeSetMemberKeyTextBox.Size = new System.Drawing.Size(39, 20);
            this.parentModelSizeSetMemberKeyTextBox.TabIndex = 11;
            this.parentModelSizeSetMemberKeyTextBox.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(483, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Key";
            // 
            // ModelSizeSetMemberEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 216);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.parentModelSizeSetMemberKeyTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.modelSizeSetMemberComboBox);
            this.Controls.Add(this.sizeLevelTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNumberOfItemsToEdit);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Name = "ModelSizeSetMemberEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Model size set member";
            this.Load += new System.EventHandler(this.ModelSizeSetMemberEditForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblNumberOfItemsToEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox sizeLevelTextBox;
        private System.Windows.Forms.ComboBox modelSizeSetMemberComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox parentModelSizeSetMemberKeyTextBox;
        private System.Windows.Forms.Label label3;
    }
}