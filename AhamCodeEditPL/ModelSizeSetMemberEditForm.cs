﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Collections;


using AhamMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ModelSizeSetMemberEditForm : Form
    {
        private HaiBindingList<ModelSizeSetMember> _members;

        public ModelSizeSetMemberEditForm(HaiBindingList<ModelSizeSetMember> members)
        {
            InitializeComponent();

            _members = members;
        }

        private void ModelSizeSetMemberEditForm_Load(object sender, EventArgs e)
        {
            bool hasCommonSizeLevel = true;
            bool hasCommonParent = true;

            int sizeLevelToTest = _members[0].SizeLevel;
            int? parentSizeKeyToTest = _members[0].ParentSizeKey;

            foreach (ModelSizeSetMember mssm in _members)
            {
                if (mssm.SizeLevel != sizeLevelToTest)
                    hasCommonSizeLevel = false;
                if (mssm.ParentSizeKey != parentSizeKeyToTest)
                    hasCommonParent = false;
            }

            if (!hasCommonParent)
            {
                MessageBox.Show("Parent members must all be the same", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            if (!hasCommonSizeLevel)
            {
                MessageBox.Show("Size levels must all be the same", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            lblNumberOfItemsToEdit.Text = "Number of items included: " + _members.Count.ToString();

            parentModelSizeSetMemberKeyTextBox.ReadOnly = true;
            sizeLevelTextBox.Text = _members[0].SizeLevel.ToString();

            AhamDataService ds = new AhamDataService(new HaiMetaDataDAL.DataServiceParameters());
            DataAccessResult result = ds.GetDataList(HaiBusinessObjectType.ModelSizeSetMember);
            if (result.Success)
            {
                HaiBindingList<ModelSizeSetMember> modelSizeSetMemberList = (HaiBindingList<ModelSizeSetMember>)result.DataList;
                Hashtable modelSizeSetMemberHashTbl = new Hashtable(modelSizeSetMemberList.ToDictionary(x => x.ModelSizeSetMemberName));
                modelSizeSetMemberHashTbl.Add(string.Empty, null);

                modelSizeSetMemberComboBox.Tag = modelSizeSetMemberHashTbl;

                modelSizeSetMemberComboBox.Items.Clear();
                modelSizeSetMemberComboBox.BeginUpdate();

                foreach (string key in modelSizeSetMemberHashTbl.Keys)
                {
                    modelSizeSetMemberComboBox.Items.Add(key);
                }
                modelSizeSetMemberComboBox.EndUpdate();

                modelSizeSetMemberComboBox.Sorted = true;
                modelSizeSetMemberComboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
                modelSizeSetMemberComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

                modelSizeSetMemberComboBox.Text = _members[0].ParentSizeName;
                modelSizeSetMemberComboBox_TextChanged(null, null);
            }
            else
            {
                MessageBox.Show("Unable to retrieve Model Size Set Members", "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            modelSizeSetMemberComboBox.TextChanged += new EventHandler(modelSizeSetMemberComboBox_TextChanged);
        }

        void modelSizeSetMemberComboBox_TextChanged(object sender, EventArgs e)
        {
            Hashtable modelSizeSetMemberHashTbl = (Hashtable)modelSizeSetMemberComboBox.Tag;
            ModelSizeSetMember modelSizeSetMember = (ModelSizeSetMember)modelSizeSetMemberHashTbl[modelSizeSetMemberComboBox.Text];

            if (modelSizeSetMember == null)
            {
                parentModelSizeSetMemberKeyTextBox.Text = "--";
            }
            else
            {
                parentModelSizeSetMemberKeyTextBox.Text = modelSizeSetMember.ModelSizeSetMemberKey.ToString();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!Utilities.IsNumeric(sizeLevelTextBox.Text.Trim()))
            {
                MessageBox.Show("Size level must be numeric.", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            int sizeLevel = int.Parse(sizeLevelTextBox.Text);

            int parentSizeMemberKeyValue;
            int? parentSizeMemberKey;
            bool ok = int.TryParse(parentModelSizeSetMemberKeyTextBox.Text.Trim(),out parentSizeMemberKeyValue);
            if (ok)
                parentSizeMemberKey = parentSizeMemberKeyValue;
            else
                parentSizeMemberKey = null;

            foreach (ModelSizeSetMember mssm in _members)
            {
                mssm.ParentSizeName = modelSizeSetMemberComboBox.Text.Trim();
                mssm.ParentSizeKey = parentSizeMemberKey;
                mssm.SizeLevel = sizeLevel;
                mssm.MarkDirty();
            }

            DialogResult = DialogResult.OK;
            this.Close();
        }

    }
}
