﻿namespace AhamMetaDataPL
{
    partial class OutputDistributionManufacturerEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label manufacturerKeyLabel;
            System.Windows.Forms.Label manufacturerNameLabel;
            System.Windows.Forms.Label outputDistributionManufacturerKeyLabel;
            System.Windows.Forms.Label outputReportIDLabel;
            System.Windows.Forms.Label outputReportKeyLabel;
            this.outputDistributionManufacturerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.disabledCheckBox = new System.Windows.Forms.CheckBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.manufacturerKeyTextBox = new System.Windows.Forms.TextBox();
            this.manufacturerNameComboBox = new System.Windows.Forms.ComboBox();
            this.outputDistributionManufacturerKeyTextBox = new System.Windows.Forms.TextBox();
            this.outputReportIDComboBox = new System.Windows.Forms.ComboBox();
            this.outputReportKeyTextBox = new System.Windows.Forms.TextBox();
            beginDateLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            manufacturerKeyLabel = new System.Windows.Forms.Label();
            manufacturerNameLabel = new System.Windows.Forms.Label();
            outputDistributionManufacturerKeyLabel = new System.Windows.Forms.Label();
            outputReportIDLabel = new System.Windows.Forms.Label();
            outputReportKeyLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.outputDistributionManufacturerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(437, 181);
            this.btnOK.TabIndex = 5;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(518, 181);
            this.btnCancel.TabIndex = 6;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(108, 68);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 30;
            beginDateLabel.Text = "Begin Date:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(294, 68);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 34;
            endDateLabel.Text = "End Date:";
            // 
            // manufacturerKeyLabel
            // 
            manufacturerKeyLabel.AutoSize = true;
            manufacturerKeyLabel.Location = new System.Drawing.Point(523, 14);
            manufacturerKeyLabel.Name = "manufacturerKeyLabel";
            manufacturerKeyLabel.Size = new System.Drawing.Size(28, 13);
            manufacturerKeyLabel.TabIndex = 36;
            manufacturerKeyLabel.Text = "Key:";
            // 
            // manufacturerNameLabel
            // 
            manufacturerNameLabel.AutoSize = true;
            manufacturerNameLabel.Location = new System.Drawing.Point(4, 14);
            manufacturerNameLabel.Name = "manufacturerNameLabel";
            manufacturerNameLabel.Size = new System.Drawing.Size(104, 13);
            manufacturerNameLabel.TabIndex = 38;
            manufacturerNameLabel.Text = "Manufacturer Name:";
            // 
            // outputDistributionManufacturerKeyLabel
            // 
            outputDistributionManufacturerKeyLabel.AutoSize = true;
            outputDistributionManufacturerKeyLabel.Location = new System.Drawing.Point(365, 139);
            outputDistributionManufacturerKeyLabel.Name = "outputDistributionManufacturerKeyLabel";
            outputDistributionManufacturerKeyLabel.Size = new System.Drawing.Size(184, 13);
            outputDistributionManufacturerKeyLabel.TabIndex = 40;
            outputDistributionManufacturerKeyLabel.Text = "Output Distribution Manufacturer Key:";
            // 
            // outputReportIDLabel
            // 
            outputReportIDLabel.AutoSize = true;
            outputReportIDLabel.Location = new System.Drawing.Point(4, 41);
            outputReportIDLabel.Name = "outputReportIDLabel";
            outputReportIDLabel.Size = new System.Drawing.Size(91, 13);
            outputReportIDLabel.TabIndex = 42;
            outputReportIDLabel.Text = "Output Report ID:";
            // 
            // outputReportKeyLabel
            // 
            outputReportKeyLabel.AutoSize = true;
            outputReportKeyLabel.Location = new System.Drawing.Point(523, 40);
            outputReportKeyLabel.Name = "outputReportKeyLabel";
            outputReportKeyLabel.Size = new System.Drawing.Size(28, 13);
            outputReportKeyLabel.TabIndex = 44;
            outputReportKeyLabel.Text = "Key:";
            // 
            // outputDistributionManufacturerBindingSource
            // 
            this.outputDistributionManufacturerBindingSource.DataSource = typeof(AhamMetaDataDAL.OutputDistributionManufacturer);
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputDistributionManufacturerBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(177, 65);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(95, 20);
            this.beginDateTextBox.TabIndex = 2;
            // 
            // disabledCheckBox
            // 
            this.disabledCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.outputDistributionManufacturerBindingSource, "Disabled", true));
            this.disabledCheckBox.Location = new System.Drawing.Point(111, 91);
            this.disabledCheckBox.Name = "disabledCheckBox";
            this.disabledCheckBox.Size = new System.Drawing.Size(73, 24);
            this.disabledCheckBox.TabIndex = 4;
            this.disabledCheckBox.Text = "Disabled";
            this.disabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputDistributionManufacturerBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(363, 65);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(95, 20);
            this.endDateTextBox.TabIndex = 3;
            // 
            // manufacturerKeyTextBox
            // 
            this.manufacturerKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputDistributionManufacturerBindingSource, "ManufacturerKey", true));
            this.manufacturerKeyTextBox.Location = new System.Drawing.Point(555, 11);
            this.manufacturerKeyTextBox.Name = "manufacturerKeyTextBox";
            this.manufacturerKeyTextBox.Size = new System.Drawing.Size(38, 20);
            this.manufacturerKeyTextBox.TabIndex = 37;
            this.manufacturerKeyTextBox.TabStop = false;
            // 
            // manufacturerNameComboBox
            // 
            this.manufacturerNameComboBox.FormattingEnabled = true;
            this.manufacturerNameComboBox.Location = new System.Drawing.Point(111, 11);
            this.manufacturerNameComboBox.Name = "manufacturerNameComboBox";
            this.manufacturerNameComboBox.Size = new System.Drawing.Size(400, 21);
            this.manufacturerNameComboBox.TabIndex = 0;
            // 
            // outputDistributionManufacturerKeyTextBox
            // 
            this.outputDistributionManufacturerKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputDistributionManufacturerBindingSource, "OutputDistributionManufacturerKey", true));
            this.outputDistributionManufacturerKeyTextBox.Location = new System.Drawing.Point(555, 136);
            this.outputDistributionManufacturerKeyTextBox.Name = "outputDistributionManufacturerKeyTextBox";
            this.outputDistributionManufacturerKeyTextBox.Size = new System.Drawing.Size(38, 20);
            this.outputDistributionManufacturerKeyTextBox.TabIndex = 41;
            this.outputDistributionManufacturerKeyTextBox.TabStop = false;
            // 
            // outputReportIDComboBox
            // 
            this.outputReportIDComboBox.FormattingEnabled = true;
            this.outputReportIDComboBox.Location = new System.Drawing.Point(111, 38);
            this.outputReportIDComboBox.Name = "outputReportIDComboBox";
            this.outputReportIDComboBox.Size = new System.Drawing.Size(400, 21);
            this.outputReportIDComboBox.TabIndex = 1;
            // 
            // outputReportKeyTextBox
            // 
            this.outputReportKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputDistributionManufacturerBindingSource, "OutputReportKey", true));
            this.outputReportKeyTextBox.Location = new System.Drawing.Point(555, 37);
            this.outputReportKeyTextBox.Name = "outputReportKeyTextBox";
            this.outputReportKeyTextBox.Size = new System.Drawing.Size(38, 20);
            this.outputReportKeyTextBox.TabIndex = 45;
            this.outputReportKeyTextBox.TabStop = false;
            // 
            // OutputDistributionManufacturerEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 216);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(this.disabledCheckBox);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(manufacturerKeyLabel);
            this.Controls.Add(this.manufacturerKeyTextBox);
            this.Controls.Add(manufacturerNameLabel);
            this.Controls.Add(this.manufacturerNameComboBox);
            this.Controls.Add(outputDistributionManufacturerKeyLabel);
            this.Controls.Add(this.outputDistributionManufacturerKeyTextBox);
            this.Controls.Add(outputReportIDLabel);
            this.Controls.Add(this.outputReportIDComboBox);
            this.Controls.Add(outputReportKeyLabel);
            this.Controls.Add(this.outputReportKeyTextBox);
            this.Name = "OutputDistributionManufacturerEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Output Distribution Manufacturer";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.outputReportKeyTextBox, 0);
            this.Controls.SetChildIndex(outputReportKeyLabel, 0);
            this.Controls.SetChildIndex(this.outputReportIDComboBox, 0);
            this.Controls.SetChildIndex(outputReportIDLabel, 0);
            this.Controls.SetChildIndex(this.outputDistributionManufacturerKeyTextBox, 0);
            this.Controls.SetChildIndex(outputDistributionManufacturerKeyLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerNameComboBox, 0);
            this.Controls.SetChildIndex(manufacturerNameLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerKeyTextBox, 0);
            this.Controls.SetChildIndex(manufacturerKeyLabel, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            this.Controls.SetChildIndex(this.disabledCheckBox, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.outputDistributionManufacturerBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource outputDistributionManufacturerBindingSource;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.CheckBox disabledCheckBox;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.TextBox manufacturerKeyTextBox;
        private System.Windows.Forms.ComboBox manufacturerNameComboBox;
        private System.Windows.Forms.TextBox outputDistributionManufacturerKeyTextBox;
        private System.Windows.Forms.ComboBox outputReportIDComboBox;
        private System.Windows.Forms.TextBox outputReportKeyTextBox;

    }
}