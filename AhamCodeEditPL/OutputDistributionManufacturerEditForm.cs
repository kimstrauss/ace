﻿using System;
using System.Windows.Forms;

using System.Configuration;
using System.Collections;

using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;
using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class OutputDistributionManufacturerEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private OutputDistributionManufacturer _outputDistributionManufacturerToEdit;

        public OutputDistributionManufacturerEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _outputDistributionManufacturerToEdit = (OutputDistributionManufacturer)_parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);
            outputDistributionManufacturerBindingSource.DataSource = _outputDistributionManufacturerToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = ds.GetDataList(HaiBusinessObjectType.OutputReport);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<OutputReport>(outputReportIDComboBox, result, ComboBoxListType.DataListWithBlankItem, "OutputReportID", "OutputReportKey", outputReportKeyTextBox, _outputDistributionManufacturerToEdit.OutputReportID.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit OutputDistributionManufacturer.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Manufacturer);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Manufacturer>(manufacturerNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ManufacturerName", "ManufacturerKey", manufacturerKeyTextBox, _outputDistributionManufacturerToEdit.ManufacturerName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit OutputDistributionManufacturer.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;
            string message = string.Empty;

            if (outputReportIDComboBox.Items.Count > 0 && outputReportIDComboBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An output report ID is required.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (manufacturerNameComboBox.Items.Count > 0 && manufacturerNameComboBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An manufacturer name is required.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            message = ValidateDaterange(_outputDistributionManufacturerToEdit, DateRangeValidationType.Daily);
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_outputDistributionManufacturerToEdit);
            _outputDistributionManufacturerToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
