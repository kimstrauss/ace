﻿namespace AhamMetaDataPL
{
    partial class OutputDistributionUserEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label outputDistributionUserKeyLabel;
            System.Windows.Forms.Label outputReportIDLabel;
            System.Windows.Forms.Label outputReportKeyLabel;
            System.Windows.Forms.Label userCodeLabel;
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.outputDistributionUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.disabledCheckBox = new System.Windows.Forms.CheckBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.outputDistributionUserKeyTextBox = new System.Windows.Forms.TextBox();
            this.outputReportIDComboBox = new System.Windows.Forms.ComboBox();
            this.outputReportKeyTextBox = new System.Windows.Forms.TextBox();
            this.userCodeTextBox = new System.Windows.Forms.TextBox();
            beginDateLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            outputDistributionUserKeyLabel = new System.Windows.Forms.Label();
            outputReportIDLabel = new System.Windows.Forms.Label();
            outputReportKeyLabel = new System.Windows.Forms.Label();
            userCodeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.outputDistributionUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(445, 181);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(526, 181);
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(100, 68);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 30;
            beginDateLabel.Text = "Begin Date:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(312, 68);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 34;
            endDateLabel.Text = "End Date:";
            // 
            // outputDistributionUserKeyLabel
            // 
            outputDistributionUserKeyLabel.AutoSize = true;
            outputDistributionUserKeyLabel.Location = new System.Drawing.Point(408, 107);
            outputDistributionUserKeyLabel.Name = "outputDistributionUserKeyLabel";
            outputDistributionUserKeyLabel.Size = new System.Drawing.Size(143, 13);
            outputDistributionUserKeyLabel.TabIndex = 36;
            outputDistributionUserKeyLabel.Text = "Output Distribution User Key:";
            // 
            // outputReportIDLabel
            // 
            outputReportIDLabel.AutoSize = true;
            outputReportIDLabel.Location = new System.Drawing.Point(6, 15);
            outputReportIDLabel.Name = "outputReportIDLabel";
            outputReportIDLabel.Size = new System.Drawing.Size(91, 13);
            outputReportIDLabel.TabIndex = 38;
            outputReportIDLabel.Text = "Output Report ID:";
            // 
            // outputReportKeyLabel
            // 
            outputReportKeyLabel.AutoSize = true;
            outputReportKeyLabel.Location = new System.Drawing.Point(523, 15);
            outputReportKeyLabel.Name = "outputReportKeyLabel";
            outputReportKeyLabel.Size = new System.Drawing.Size(28, 13);
            outputReportKeyLabel.TabIndex = 40;
            outputReportKeyLabel.Text = "Key:";
            // 
            // userCodeLabel
            // 
            userCodeLabel.AutoSize = true;
            userCodeLabel.Location = new System.Drawing.Point(6, 39);
            userCodeLabel.Name = "userCodeLabel";
            userCodeLabel.Size = new System.Drawing.Size(60, 13);
            userCodeLabel.TabIndex = 42;
            userCodeLabel.Text = "User Code:";
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputDistributionUserBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(169, 65);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(87, 20);
            this.beginDateTextBox.TabIndex = 4;
            // 
            // outputDistributionUserBindingSource
            // 
            this.outputDistributionUserBindingSource.DataSource = typeof(AhamMetaDataDAL.OutputDistributionUser);
            // 
            // disabledCheckBox
            // 
            this.disabledCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.outputDistributionUserBindingSource, "Disabled", true));
            this.disabledCheckBox.Location = new System.Drawing.Point(373, 39);
            this.disabledCheckBox.Name = "disabledCheckBox";
            this.disabledCheckBox.Size = new System.Drawing.Size(73, 24);
            this.disabledCheckBox.TabIndex = 3;
            this.disabledCheckBox.Text = "Disabled";
            this.disabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputDistributionUserBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(373, 65);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(87, 20);
            this.endDateTextBox.TabIndex = 5;
            // 
            // outputDistributionUserKeyTextBox
            // 
            this.outputDistributionUserKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputDistributionUserBindingSource, "OutputDistributionUserKey", true));
            this.outputDistributionUserKeyTextBox.Location = new System.Drawing.Point(554, 103);
            this.outputDistributionUserKeyTextBox.Name = "outputDistributionUserKeyTextBox";
            this.outputDistributionUserKeyTextBox.Size = new System.Drawing.Size(48, 20);
            this.outputDistributionUserKeyTextBox.TabIndex = 37;
            this.outputDistributionUserKeyTextBox.TabStop = false;
            // 
            // outputReportIDComboBox
            // 
            this.outputReportIDComboBox.FormattingEnabled = true;
            this.outputReportIDComboBox.Location = new System.Drawing.Point(103, 12);
            this.outputReportIDComboBox.Name = "outputReportIDComboBox";
            this.outputReportIDComboBox.Size = new System.Drawing.Size(400, 21);
            this.outputReportIDComboBox.TabIndex = 0;
            // 
            // outputReportKeyTextBox
            // 
            this.outputReportKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputDistributionUserBindingSource, "OutputReportKey", true));
            this.outputReportKeyTextBox.Location = new System.Drawing.Point(554, 12);
            this.outputReportKeyTextBox.Name = "outputReportKeyTextBox";
            this.outputReportKeyTextBox.Size = new System.Drawing.Size(48, 20);
            this.outputReportKeyTextBox.TabIndex = 41;
            this.outputReportKeyTextBox.TabStop = false;
            // 
            // userCodeTextBox
            // 
            this.userCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputDistributionUserBindingSource, "UserCode", true));
            this.userCodeTextBox.Location = new System.Drawing.Point(103, 39);
            this.userCodeTextBox.Name = "userCodeTextBox";
            this.userCodeTextBox.Size = new System.Drawing.Size(225, 20);
            this.userCodeTextBox.TabIndex = 1;
            // 
            // OutputDistributionUserEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 216);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(this.disabledCheckBox);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(outputDistributionUserKeyLabel);
            this.Controls.Add(this.outputDistributionUserKeyTextBox);
            this.Controls.Add(outputReportIDLabel);
            this.Controls.Add(this.outputReportIDComboBox);
            this.Controls.Add(outputReportKeyLabel);
            this.Controls.Add(this.outputReportKeyTextBox);
            this.Controls.Add(userCodeLabel);
            this.Controls.Add(this.userCodeTextBox);
            this.Name = "OutputDistributionUserEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Output Distribution User";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.userCodeTextBox, 0);
            this.Controls.SetChildIndex(userCodeLabel, 0);
            this.Controls.SetChildIndex(this.outputReportKeyTextBox, 0);
            this.Controls.SetChildIndex(outputReportKeyLabel, 0);
            this.Controls.SetChildIndex(this.outputReportIDComboBox, 0);
            this.Controls.SetChildIndex(outputReportIDLabel, 0);
            this.Controls.SetChildIndex(this.outputDistributionUserKeyTextBox, 0);
            this.Controls.SetChildIndex(outputDistributionUserKeyLabel, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            this.Controls.SetChildIndex(this.disabledCheckBox, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.outputDistributionUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource outputDistributionUserBindingSource;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.CheckBox disabledCheckBox;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.TextBox outputDistributionUserKeyTextBox;
        private System.Windows.Forms.ComboBox outputReportIDComboBox;
        private System.Windows.Forms.TextBox outputReportKeyTextBox;
        private System.Windows.Forms.TextBox userCodeTextBox;
    }
}