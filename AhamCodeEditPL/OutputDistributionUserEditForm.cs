﻿using System;
using System.Windows.Forms;

using System.Configuration;
using System.Collections;

using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;
using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class OutputDistributionUserEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private OutputDistributionUser _outputDistributionUserToEdit;

        public OutputDistributionUserEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _outputDistributionUserToEdit = (OutputDistributionUser)_parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            outputDistributionUserBindingSource.DataSource = _outputDistributionUserToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result = ds.GetDataList(HaiBusinessObjectType.OutputReport);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<OutputReport>(outputReportIDComboBox, result, ComboBoxListType.DataListWithBlankItem, "OutputReportID", "OutputReportKey", outputReportKeyTextBox, _outputDistributionUserToEdit.OutputReportID.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit OutputDistributionUser.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;
            string message = string.Empty;

            if (userCodeTextBox.Text.Trim() == string.Empty && !userCodeTextBox.ReadOnly)
            {
                MessageBox.Show("A user code is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (outputReportIDComboBox.Text.Trim() == string.Empty && outputReportIDComboBox.Items.Count>0)
            {
                MessageBox.Show("An output report ID is required.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            message = ValidateDaterange(_outputDistributionUserToEdit, DateRangeValidationType.Daily);
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            //outputReportKeyTextBox.ReadOnly = false;
            MarkChangedProperties(_outputDistributionUserToEdit);
            _outputDistributionUserToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
