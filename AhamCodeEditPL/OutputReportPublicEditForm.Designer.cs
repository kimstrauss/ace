﻿namespace AhamMetaDataPL
{
    partial class OutputReportPublicEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label outputReportIDLabel;
            System.Windows.Forms.Label outputReportKeyLabel;
            System.Windows.Forms.Label outputReportPublicKeyLabel;
            this.outputReportPublicBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.outputReportIDComboBox = new System.Windows.Forms.ComboBox();
            this.outputReportKeyTextBox = new System.Windows.Forms.TextBox();
            this.outputReportPublicKeyTextBox = new System.Windows.Forms.TextBox();
            beginDateLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            outputReportIDLabel = new System.Windows.Forms.Label();
            outputReportKeyLabel = new System.Windows.Forms.Label();
            outputReportPublicKeyLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.outputReportPublicBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(462, 181);
            this.btnOK.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(543, 181);
            this.btnCancel.TabIndex = 4;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(104, 42);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 30;
            beginDateLabel.Text = "Begin Date:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(299, 42);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 32;
            endDateLabel.Text = "End Date:";
            // 
            // outputReportIDLabel
            // 
            outputReportIDLabel.AutoSize = true;
            outputReportIDLabel.Location = new System.Drawing.Point(7, 15);
            outputReportIDLabel.Name = "outputReportIDLabel";
            outputReportIDLabel.Size = new System.Drawing.Size(91, 13);
            outputReportIDLabel.TabIndex = 34;
            outputReportIDLabel.Text = "Output Report ID:";
            // 
            // outputReportKeyLabel
            // 
            outputReportKeyLabel.AutoSize = true;
            outputReportKeyLabel.Location = new System.Drawing.Point(519, 15);
            outputReportKeyLabel.Name = "outputReportKeyLabel";
            outputReportKeyLabel.Size = new System.Drawing.Size(28, 13);
            outputReportKeyLabel.TabIndex = 36;
            outputReportKeyLabel.Text = "Key:";
            // 
            // outputReportPublicKeyLabel
            // 
            outputReportPublicKeyLabel.AutoSize = true;
            outputReportPublicKeyLabel.Location = new System.Drawing.Point(417, 141);
            outputReportPublicKeyLabel.Name = "outputReportPublicKeyLabel";
            outputReportPublicKeyLabel.Size = new System.Drawing.Size(130, 13);
            outputReportPublicKeyLabel.TabIndex = 38;
            outputReportPublicKeyLabel.Text = "Output Report Public Key:";
            // 
            // outputReportPublicBindingSource
            // 
            this.outputReportPublicBindingSource.DataSource = typeof(AhamMetaDataDAL.OutputReportPublic);
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputReportPublicBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(173, 39);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(102, 20);
            this.beginDateTextBox.TabIndex = 1;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputReportPublicBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(360, 39);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(102, 20);
            this.endDateTextBox.TabIndex = 2;
            // 
            // outputReportIDComboBox
            // 
            this.outputReportIDComboBox.FormattingEnabled = true;
            this.outputReportIDComboBox.Location = new System.Drawing.Point(104, 12);
            this.outputReportIDComboBox.Name = "outputReportIDComboBox";
            this.outputReportIDComboBox.Size = new System.Drawing.Size(400, 21);
            this.outputReportIDComboBox.TabIndex = 0;
            // 
            // outputReportKeyTextBox
            // 
            this.outputReportKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputReportPublicBindingSource, "OutputReportKey", true));
            this.outputReportKeyTextBox.Location = new System.Drawing.Point(553, 12);
            this.outputReportKeyTextBox.Name = "outputReportKeyTextBox";
            this.outputReportKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.outputReportKeyTextBox.TabIndex = 37;
            this.outputReportKeyTextBox.TabStop = false;
            // 
            // outputReportPublicKeyTextBox
            // 
            this.outputReportPublicKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.outputReportPublicBindingSource, "OutputReportPublicKey", true));
            this.outputReportPublicKeyTextBox.Location = new System.Drawing.Point(553, 138);
            this.outputReportPublicKeyTextBox.Name = "outputReportPublicKeyTextBox";
            this.outputReportPublicKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.outputReportPublicKeyTextBox.TabIndex = 39;
            this.outputReportPublicKeyTextBox.TabStop = false;
            // 
            // OutputReportPublicEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 216);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(outputReportIDLabel);
            this.Controls.Add(this.outputReportIDComboBox);
            this.Controls.Add(outputReportKeyLabel);
            this.Controls.Add(this.outputReportKeyTextBox);
            this.Controls.Add(outputReportPublicKeyLabel);
            this.Controls.Add(this.outputReportPublicKeyTextBox);
            this.Name = "OutputReportPublicEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Output Report Public";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.outputReportPublicKeyTextBox, 0);
            this.Controls.SetChildIndex(outputReportPublicKeyLabel, 0);
            this.Controls.SetChildIndex(this.outputReportKeyTextBox, 0);
            this.Controls.SetChildIndex(outputReportKeyLabel, 0);
            this.Controls.SetChildIndex(this.outputReportIDComboBox, 0);
            this.Controls.SetChildIndex(outputReportIDLabel, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.outputReportPublicBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource outputReportPublicBindingSource;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.ComboBox outputReportIDComboBox;
        private System.Windows.Forms.TextBox outputReportKeyTextBox;
        private System.Windows.Forms.TextBox outputReportPublicKeyTextBox;
    }
}