﻿using System;
using System.Windows.Forms;

using System.Configuration;
using System.Collections;

using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;
using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class OutputReportPublicEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private OutputReportPublic _outputReportPublicToEdit;

        public OutputReportPublicEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _outputReportPublicToEdit = (OutputReportPublic)_parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            outputReportPublicBindingSource.DataSource = _outputReportPublicToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result = ds.GetDataList(HaiBusinessObjectType.OutputReport);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<OutputReport>(outputReportIDComboBox, result, ComboBoxListType.DataListWithBlankItem, "OutputReportID", "OutputReportKey", outputReportKeyTextBox, _outputReportPublicToEdit.OutputReportID.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit OutputReportPublic.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;
            string message = string.Empty;

            if (outputReportIDComboBox.Text.Trim() == string.Empty && outputReportIDComboBox.Items.Count>0)
            {
                MessageBox.Show("An output report must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            message = ValidateDaterange(_outputReportPublicToEdit, DateRangeValidationType.Monthly);
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_outputReportPublicToEdit);
            _outputReportPublicToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
