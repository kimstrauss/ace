﻿namespace AhamMetaDataPL
{
    partial class PLogListEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblManufacturerName = new System.Windows.Forms.Label();
            this.lblReportName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gridPLog = new HaiBusinessUI.ucReadonlyDatagridForm();
            this.btnPrintGrid = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(620, 541);
            this.btnOK.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(701, 541);
            this.btnCancel.TabIndex = 2;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 541);
            // 
            // lblManufacturerName
            // 
            this.lblManufacturerName.Location = new System.Drawing.Point(9, 9);
            this.lblManufacturerName.Name = "lblManufacturerName";
            this.lblManufacturerName.Size = new System.Drawing.Size(510, 24);
            this.lblManufacturerName.TabIndex = 30;
            this.lblManufacturerName.Text = "Manufacturer:";
            this.lblManufacturerName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReportName
            // 
            this.lblReportName.Location = new System.Drawing.Point(9, 39);
            this.lblReportName.Name = "lblReportName";
            this.lblReportName.Size = new System.Drawing.Size(510, 24);
            this.lblReportName.TabIndex = 31;
            this.lblReportName.Text = "Report:";
            this.lblReportName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Select PLogs to delete";
            // 
            // gridPLog
            // 
            this.gridPLog.ActionRedirectionObjectType = HaiBusinessObject.HaiBusinessObjectType.UnknownHaiBusinessObjectType;
            this.gridPLog.BackColor = System.Drawing.SystemColors.Control;
            this.gridPLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gridPLog.Location = new System.Drawing.Point(12, 93);
            this.gridPLog.Name = "gridPLog";
            this.gridPLog.Size = new System.Drawing.Size(764, 395);
            this.gridPLog.TabIndex = 0;
            // 
            // btnPrintGrid
            // 
            this.btnPrintGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintGrid.Location = new System.Drawing.Point(701, 494);
            this.btnPrintGrid.Name = "btnPrintGrid";
            this.btnPrintGrid.Size = new System.Drawing.Size(75, 23);
            this.btnPrintGrid.TabIndex = 34;
            this.btnPrintGrid.Text = "Print";
            this.btnPrintGrid.UseVisualStyleBackColor = true;
            this.btnPrintGrid.Click += new System.EventHandler(this.btnPrintGrid_Click);
            // 
            // PLogListEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 576);
            this.Controls.Add(this.btnPrintGrid);
            this.Controls.Add(this.lblReportName);
            this.Controls.Add(this.lblManufacturerName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gridPLog);
            this.Name = "PLogListEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit list of PLogs";
            this.Load += new System.EventHandler(this.PLogListEditForm_Load);
            this.Controls.SetChildIndex(this.gridPLog, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblManufacturerName, 0);
            this.Controls.SetChildIndex(this.lblReportName, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.btnPrintGrid, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblManufacturerName;
        private System.Windows.Forms.Label lblReportName;
        private System.Windows.Forms.Label label1;
        private HaiBusinessUI.ucReadonlyDatagridForm gridPLog;
        private System.Windows.Forms.Button btnPrintGrid;
    }
}