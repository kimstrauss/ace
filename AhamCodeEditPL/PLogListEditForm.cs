﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;

using AhamMetaDataDAL;
using HaiBusinessObject;
using HaiBusinessUI;
using HaiMetaDataDAL;
using HaiInterfaces;

namespace AhamMetaDataPL
{
    // N.B. this form is a "one off" that does not follow the pattern for edit forms.
    public partial class PLogListEditForm : HaiObjectEditForm
    {
        HaiBusinessObject.DataAccessResult _pLogResult;

        public PLogListEditForm(string manufacturerName, string reportName, HaiBusinessObject.DataAccessResult pLogResult, EditFormParameters parameters)
        {
            InitializeComponent();

            lblManufacturerName.Text = "Manufacturer: " + manufacturerName;
            lblReportName.Text = "Report: " + reportName;
            _pLogResult = pLogResult;

            gridPLog.GUS = parameters.GloballyUsefulStuff;

            // create an event handler clicks that select a row
            gridPLog.DataGridView.RowHeaderMouseClick += new DataGridViewCellMouseEventHandler(DataGridView_RowHeaderMouseClick);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        void DataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIndex = e.RowIndex;
            DataGridViewRow row = gridPLog.DataGridView.Rows[rowIndex];
            ManufacturerPLog pLog = (ManufacturerPLog)row.DataBoundItem;

            // prevent rows with related data from being selected
            if (pLog.HasData)
                row.Selected = false;
        }

        private void PLogListEditForm_Load(object sender, EventArgs e)
        {
            // sort the binding list by the EndDate
            HaiBindingList<ManufacturerPLog> theBindingList = (HaiBindingList<ManufacturerPLog>)_pLogResult.DataList;
            theBindingList.SortOnProperty("EndDate", System.ComponentModel.ListSortDirection.Descending);
            HaiReports.ReportParameters reportParameters = new HaiReports.ReportParameters();
            reportParameters.SetBasicProperties(HaiReports.ListType.unknown);
            theBindingList.ReportParameters = reportParameters;

            // show the list of PLogs in the grid
            string[] cols = { "EndDate", "HasData", "StatusName", "PostPeriod", "ManufacturerPLogKey" };
            gridPLog.PrepareGridForDisplay(_pLogResult, new List<string>(cols));
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            HaiBindingList<ManufacturerPLog> selectedPLogs = gridPLog.GetReadonlyBindingListOfSelectedItems<ManufacturerPLog>(false);

            // ensure user has made an explicit selection
            if (selectedPLogs.Count == 0)
            {
                MessageBox.Show("Nothing selected!", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // select by "draggnig" can include rows with data; verify pLog rows with related data are not selected
            string keys = string.Empty;
            foreach (ManufacturerPLog pLog in selectedPLogs)
            {
                if (pLog.HasData)
                    keys = keys + " " + pLog.ManufacturerPLogKey.ToString();
            }

            // ... issue message and abort if pLogs with data are found
            if (keys != string.Empty)
            {
                MessageBox.Show("Data exists for PLog(s) with key(s):\r\n " + keys + "\n\rNo deletions performed.", 
                    "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // return to the calller the list of items to be deleted
            _pLogResult.PassBackDataList = selectedPLogs;

            // inform the caller that the edit is good
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnPrintGrid_Click(object sender, EventArgs e)
        {
            gridPLog.PrintGridReport();
        }
    }
}
