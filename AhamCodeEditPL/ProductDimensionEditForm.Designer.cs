﻿namespace AhamMetaDataPL
{
    partial class ProductDimensionEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label dimensionKeyLabel;
            System.Windows.Forms.Label dimensionNameLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label productDimensionKeyLabel;
            System.Windows.Forms.Label productKeyLabel;
            System.Windows.Forms.Label productNameLabel;
            System.Windows.Forms.Label unitLabelLabel;
            System.Windows.Forms.Label productDimensionNameLabel;
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.productDimensionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.continuousCheckBox = new System.Windows.Forms.CheckBox();
            this.dimensionKeyTextBox = new System.Windows.Forms.TextBox();
            this.dimensionNameComboBox = new System.Windows.Forms.ComboBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.productDimensionKeyTextBox = new System.Windows.Forms.TextBox();
            this.productKeyTextBox = new System.Windows.Forms.TextBox();
            this.productNameComboBox = new System.Windows.Forms.ComboBox();
            this.unitLabelTextBox = new System.Windows.Forms.TextBox();
            this.productDimensionNameTextBox = new System.Windows.Forms.TextBox();
            this.btnCreateName = new System.Windows.Forms.Button();
            beginDateLabel = new System.Windows.Forms.Label();
            dimensionKeyLabel = new System.Windows.Forms.Label();
            dimensionNameLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            productDimensionKeyLabel = new System.Windows.Forms.Label();
            productKeyLabel = new System.Windows.Forms.Label();
            productNameLabel = new System.Windows.Forms.Label();
            unitLabelLabel = new System.Windows.Forms.Label();
            productDimensionNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.productDimensionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(412, 228);
            this.btnOK.TabIndex = 8;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(493, 228);
            this.btnCancel.TabIndex = 9;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 228);
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(138, 95);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 30;
            beginDateLabel.Text = "Begin Date:";
            // 
            // dimensionKeyLabel
            // 
            dimensionKeyLabel.AutoSize = true;
            dimensionKeyLabel.Location = new System.Drawing.Point(481, 43);
            dimensionKeyLabel.Name = "dimensionKeyLabel";
            dimensionKeyLabel.Size = new System.Drawing.Size(28, 13);
            dimensionKeyLabel.TabIndex = 34;
            dimensionKeyLabel.Text = "Key:";
            // 
            // dimensionNameLabel
            // 
            dimensionNameLabel.AutoSize = true;
            dimensionNameLabel.Location = new System.Drawing.Point(13, 42);
            dimensionNameLabel.Name = "dimensionNameLabel";
            dimensionNameLabel.Size = new System.Drawing.Size(90, 13);
            dimensionNameLabel.TabIndex = 36;
            dimensionNameLabel.Text = "Dimension Name:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(318, 95);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 38;
            endDateLabel.Text = "End Date:";
            // 
            // productDimensionKeyLabel
            // 
            productDimensionKeyLabel.AutoSize = true;
            productDimensionKeyLabel.Location = new System.Drawing.Point(384, 198);
            productDimensionKeyLabel.Name = "productDimensionKeyLabel";
            productDimensionKeyLabel.Size = new System.Drawing.Size(120, 13);
            productDimensionKeyLabel.TabIndex = 40;
            productDimensionKeyLabel.Text = "Product Dimension Key:";
            // 
            // productKeyLabel
            // 
            productKeyLabel.AutoSize = true;
            productKeyLabel.Location = new System.Drawing.Point(482, 17);
            productKeyLabel.Name = "productKeyLabel";
            productKeyLabel.Size = new System.Drawing.Size(28, 13);
            productKeyLabel.TabIndex = 42;
            productKeyLabel.Text = "Key:";
            // 
            // productNameLabel
            // 
            productNameLabel.AutoSize = true;
            productNameLabel.Location = new System.Drawing.Point(13, 15);
            productNameLabel.Name = "productNameLabel";
            productNameLabel.Size = new System.Drawing.Size(78, 13);
            productNameLabel.TabIndex = 44;
            productNameLabel.Text = "Product Name:";
            // 
            // unitLabelLabel
            // 
            unitLabelLabel.AutoSize = true;
            unitLabelLabel.Location = new System.Drawing.Point(13, 69);
            unitLabelLabel.Name = "unitLabelLabel";
            unitLabelLabel.Size = new System.Drawing.Size(58, 13);
            unitLabelLabel.TabIndex = 46;
            unitLabelLabel.Text = "Unit Label:";
            // 
            // productDimensionNameLabel
            // 
            productDimensionNameLabel.AutoSize = true;
            productDimensionNameLabel.Location = new System.Drawing.Point(3, 121);
            productDimensionNameLabel.Name = "productDimensionNameLabel";
            productDimensionNameLabel.Size = new System.Drawing.Size(130, 13);
            productDimensionNameLabel.TabIndex = 47;
            productDimensionNameLabel.Text = "Product Dimension Name:";
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productDimensionBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(204, 92);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(93, 20);
            this.beginDateTextBox.TabIndex = 3;
            // 
            // productDimensionBindingSource
            // 
            this.productDimensionBindingSource.DataSource = typeof(AhamMetaDataDAL.ProductDimension);
            // 
            // continuousCheckBox
            // 
            this.continuousCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.productDimensionBindingSource, "Continuous", true));
            this.continuousCheckBox.Location = new System.Drawing.Point(139, 187);
            this.continuousCheckBox.Name = "continuousCheckBox";
            this.continuousCheckBox.Size = new System.Drawing.Size(79, 24);
            this.continuousCheckBox.TabIndex = 7;
            this.continuousCheckBox.Text = "Continuous";
            this.continuousCheckBox.UseVisualStyleBackColor = true;
            // 
            // dimensionKeyTextBox
            // 
            this.dimensionKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productDimensionBindingSource, "DimensionKey", true));
            this.dimensionKeyTextBox.Location = new System.Drawing.Point(512, 40);
            this.dimensionKeyTextBox.Name = "dimensionKeyTextBox";
            this.dimensionKeyTextBox.Size = new System.Drawing.Size(43, 20);
            this.dimensionKeyTextBox.TabIndex = 35;
            this.dimensionKeyTextBox.TabStop = false;
            // 
            // dimensionNameComboBox
            // 
            this.dimensionNameComboBox.FormattingEnabled = true;
            this.dimensionNameComboBox.Location = new System.Drawing.Point(139, 39);
            this.dimensionNameComboBox.Name = "dimensionNameComboBox";
            this.dimensionNameComboBox.Size = new System.Drawing.Size(332, 21);
            this.dimensionNameComboBox.TabIndex = 1;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productDimensionBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(376, 92);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(93, 20);
            this.endDateTextBox.TabIndex = 4;
            // 
            // productDimensionKeyTextBox
            // 
            this.productDimensionKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productDimensionBindingSource, "ProductDimensionKey", true));
            this.productDimensionKeyTextBox.Location = new System.Drawing.Point(510, 195);
            this.productDimensionKeyTextBox.Name = "productDimensionKeyTextBox";
            this.productDimensionKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.productDimensionKeyTextBox.TabIndex = 41;
            this.productDimensionKeyTextBox.TabStop = false;
            // 
            // productKeyTextBox
            // 
            this.productKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productDimensionBindingSource, "ProductKey", true));
            this.productKeyTextBox.Location = new System.Drawing.Point(512, 13);
            this.productKeyTextBox.Name = "productKeyTextBox";
            this.productKeyTextBox.Size = new System.Drawing.Size(43, 20);
            this.productKeyTextBox.TabIndex = 43;
            this.productKeyTextBox.TabStop = false;
            // 
            // productNameComboBox
            // 
            this.productNameComboBox.FormattingEnabled = true;
            this.productNameComboBox.Location = new System.Drawing.Point(139, 12);
            this.productNameComboBox.Name = "productNameComboBox";
            this.productNameComboBox.Size = new System.Drawing.Size(332, 21);
            this.productNameComboBox.TabIndex = 0;
            // 
            // unitLabelTextBox
            // 
            this.unitLabelTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productDimensionBindingSource, "UnitLabel", true));
            this.unitLabelTextBox.Location = new System.Drawing.Point(139, 66);
            this.unitLabelTextBox.Name = "unitLabelTextBox";
            this.unitLabelTextBox.Size = new System.Drawing.Size(330, 20);
            this.unitLabelTextBox.TabIndex = 2;
            // 
            // productDimensionNameTextBox
            // 
            this.productDimensionNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productDimensionBindingSource, "ProductDimensionName", true));
            this.productDimensionNameTextBox.Location = new System.Drawing.Point(139, 118);
            this.productDimensionNameTextBox.Name = "productDimensionNameTextBox";
            this.productDimensionNameTextBox.Size = new System.Drawing.Size(330, 20);
            this.productDimensionNameTextBox.TabIndex = 5;
            // 
            // btnCreateName
            // 
            this.btnCreateName.Location = new System.Drawing.Point(139, 144);
            this.btnCreateName.Name = "btnCreateName";
            this.btnCreateName.Size = new System.Drawing.Size(75, 23);
            this.btnCreateName.TabIndex = 6;
            this.btnCreateName.Text = "Create name";
            this.btnCreateName.UseVisualStyleBackColor = true;
            this.btnCreateName.Click += new System.EventHandler(this.btnCreateName_Click);
            // 
            // ProductDimensionEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 263);
            this.Controls.Add(this.btnCreateName);
            this.Controls.Add(productDimensionNameLabel);
            this.Controls.Add(this.productDimensionNameTextBox);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(this.continuousCheckBox);
            this.Controls.Add(dimensionKeyLabel);
            this.Controls.Add(this.dimensionKeyTextBox);
            this.Controls.Add(dimensionNameLabel);
            this.Controls.Add(this.dimensionNameComboBox);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(productDimensionKeyLabel);
            this.Controls.Add(this.productDimensionKeyTextBox);
            this.Controls.Add(productKeyLabel);
            this.Controls.Add(this.productKeyTextBox);
            this.Controls.Add(productNameLabel);
            this.Controls.Add(this.productNameComboBox);
            this.Controls.Add(unitLabelLabel);
            this.Controls.Add(this.unitLabelTextBox);
            this.Name = "ProductDimensionEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Product Dimension";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.unitLabelTextBox, 0);
            this.Controls.SetChildIndex(unitLabelLabel, 0);
            this.Controls.SetChildIndex(this.productNameComboBox, 0);
            this.Controls.SetChildIndex(productNameLabel, 0);
            this.Controls.SetChildIndex(this.productKeyTextBox, 0);
            this.Controls.SetChildIndex(productKeyLabel, 0);
            this.Controls.SetChildIndex(this.productDimensionKeyTextBox, 0);
            this.Controls.SetChildIndex(productDimensionKeyLabel, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            this.Controls.SetChildIndex(this.dimensionNameComboBox, 0);
            this.Controls.SetChildIndex(dimensionNameLabel, 0);
            this.Controls.SetChildIndex(this.dimensionKeyTextBox, 0);
            this.Controls.SetChildIndex(dimensionKeyLabel, 0);
            this.Controls.SetChildIndex(this.continuousCheckBox, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            this.Controls.SetChildIndex(this.productDimensionNameTextBox, 0);
            this.Controls.SetChildIndex(productDimensionNameLabel, 0);
            this.Controls.SetChildIndex(this.btnCreateName, 0);
            ((System.ComponentModel.ISupportInitialize)(this.productDimensionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource productDimensionBindingSource;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.CheckBox continuousCheckBox;
        private System.Windows.Forms.TextBox dimensionKeyTextBox;
        private System.Windows.Forms.ComboBox dimensionNameComboBox;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.TextBox productDimensionKeyTextBox;
        private System.Windows.Forms.TextBox productKeyTextBox;
        private System.Windows.Forms.ComboBox productNameComboBox;
        private System.Windows.Forms.TextBox unitLabelTextBox;
        private System.Windows.Forms.TextBox productDimensionNameTextBox;
        private System.Windows.Forms.Button btnCreateName;
    }
}