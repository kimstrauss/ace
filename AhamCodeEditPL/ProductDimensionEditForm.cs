﻿using System;
using System.Collections;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;
using System.Data.Linq;

using System.Configuration;
using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ProductDimensionEditForm : HaiObjectEditForm
    {
        private ProductDimension _productDimensionToEdit;

        public ProductDimensionEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _productDimensionToEdit = (ProductDimension)_parameters.EditObject;

            this.Load+=new EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);          // the form needs to be prepared before assigning values to controls
            productDimensionBindingSource.DataSource = _productDimensionToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = ds.GetDataList(HaiBusinessObjectType.Product_Aham);
            if (result.Success)
            {
                HaiBindingList<Product> productList = (HaiBindingList<Product>)result.DataList;

                if (productList.Count == 0)
                {
                    MessageBox.Show("Cannot add/edit product dimension because no products are defined.", "Data integrity error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                    return;
                }

                SetupAnyComboBoxForDropDownList<Product>(productNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ProductName", "ProductKey", productKeyTextBox, _productDimensionToEdit.ProductName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit expansion factors because product fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Dimension);
            if (result.Success)
            {
                HaiBindingList<Dimension> dimensionList = (HaiBindingList<Dimension>)result.DataList;

                if (dimensionList.Count == 0)
                {
                    MessageBox.Show("Cannot add/edit product dimension because no dimensions are defined.", "Data integrity error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                    return;
                }

                SetupAnyComboBoxForDropDownList<Dimension>(dimensionNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "DimensionName", "DimensionKey", dimensionKeyTextBox, _productDimensionToEdit.DimensionName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit expansion factors because product fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        private void btnCreateName_Click(object sender, EventArgs e)
        {
            if (productDimensionNameTextBox.ReadOnly || !productDimensionNameTextBox.Enabled)
            {
                MessageBox.Show("Name field is read only!", "Action not available", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string productDimensionName = string.Empty;

            productDimensionName = productNameComboBox.Text.Trim() + " - " + dimensionNameComboBox.Text.Trim();
            int maxLength = _browsablePropertyList["ProductDimensionName"].MaxStringLength;
            if (productDimensionName.Length > maxLength)
            {
                MessageBox.Show("Generated report name exceeds limit of " + maxLength.ToString() + " characters\n\rthe name has been truncated.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                productDimensionName = productDimensionName.Substring(0, maxLength);
            }

            productDimensionNameTextBox.Text = productDimensionName;
            productDimensionNameTextBox.DataBindings["Text"].WriteValue();
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            bool abort = false;

            if (unitLabelTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Unit label must be entered.", "Validation errors", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (productNameComboBox.Text.Trim() == string.Empty && productNameComboBox.Items.Count>0)
            {
                MessageBox.Show("A product name must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (dimensionNameComboBox.Text.Trim() == string.Empty && dimensionNameComboBox.Items.Count>0)
            {
                MessageBox.Show("A dimension name must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (productDimensionNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A product/dimension name must be provided.", "Validation errors", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            message = ValidateDaterange(_productDimensionToEdit, DateRangeValidationType.Monthly);
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            // set the flags in the property controllers
            MarkChangedProperties(_productDimensionToEdit);
            _productDimensionToEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
