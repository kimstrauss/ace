﻿namespace AhamMetaDataPL
{
    partial class ProductEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label productAbbreviationLabel;
            System.Windows.Forms.Label productKeyLabel;
            System.Windows.Forms.Label productNameLabel;
            System.Windows.Forms.Label productOwnerKeyLabel;
            System.Windows.Forms.Label productOwnerNameLabel;
            System.Windows.Forms.Label groupNameLabel;
            System.Windows.Forms.Label groupKeyLabel;
            this.productBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.productKeyTextBox = new System.Windows.Forms.TextBox();
            this.productNameTextBox = new System.Windows.Forms.TextBox();
            this.productOwnerKeyTextBox = new System.Windows.Forms.TextBox();
            this.productOwnerNameComboBox = new System.Windows.Forms.ComboBox();
            this.groupNameComboBox = new System.Windows.Forms.ComboBox();
            this.groupKeyTextBox = new System.Windows.Forms.TextBox();
            productAbbreviationLabel = new System.Windows.Forms.Label();
            productKeyLabel = new System.Windows.Forms.Label();
            productNameLabel = new System.Windows.Forms.Label();
            productOwnerKeyLabel = new System.Windows.Forms.Label();
            productOwnerNameLabel = new System.Windows.Forms.Label();
            groupNameLabel = new System.Windows.Forms.Label();
            groupKeyLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 5;
            // 
            // productAbbreviationLabel
            // 
            productAbbreviationLabel.AutoSize = true;
            productAbbreviationLabel.Location = new System.Drawing.Point(6, 15);
            productAbbreviationLabel.Name = "productAbbreviationLabel";
            productAbbreviationLabel.Size = new System.Drawing.Size(109, 13);
            productAbbreviationLabel.TabIndex = 29;
            productAbbreviationLabel.Text = "Product Abbreviation:";
            // 
            // productKeyLabel
            // 
            productKeyLabel.AutoSize = true;
            productKeyLabel.Location = new System.Drawing.Point(405, 134);
            productKeyLabel.Name = "productKeyLabel";
            productKeyLabel.Size = new System.Drawing.Size(68, 13);
            productKeyLabel.TabIndex = 31;
            productKeyLabel.Text = "Product Key:";
            // 
            // productNameLabel
            // 
            productNameLabel.AutoSize = true;
            productNameLabel.Location = new System.Drawing.Point(6, 41);
            productNameLabel.Name = "productNameLabel";
            productNameLabel.Size = new System.Drawing.Size(78, 13);
            productNameLabel.TabIndex = 33;
            productNameLabel.Text = "Product Name:";
            // 
            // productOwnerKeyLabel
            // 
            productOwnerKeyLabel.AutoSize = true;
            productOwnerKeyLabel.Location = new System.Drawing.Point(442, 64);
            productOwnerKeyLabel.Name = "productOwnerKeyLabel";
            productOwnerKeyLabel.Size = new System.Drawing.Size(28, 13);
            productOwnerKeyLabel.TabIndex = 37;
            productOwnerKeyLabel.Text = "Key:";
            // 
            // productOwnerNameLabel
            // 
            productOwnerNameLabel.AutoSize = true;
            productOwnerNameLabel.Location = new System.Drawing.Point(6, 67);
            productOwnerNameLabel.Name = "productOwnerNameLabel";
            productOwnerNameLabel.Size = new System.Drawing.Size(112, 13);
            productOwnerNameLabel.TabIndex = 39;
            productOwnerNameLabel.Text = "Product Owner Name:";
            // 
            // groupNameLabel
            // 
            groupNameLabel.AutoSize = true;
            groupNameLabel.Location = new System.Drawing.Point(6, 94);
            groupNameLabel.Name = "groupNameLabel";
            groupNameLabel.Size = new System.Drawing.Size(70, 13);
            groupNameLabel.TabIndex = 40;
            groupNameLabel.Text = "Group Name:";
            // 
            // groupKeyLabel
            // 
            groupKeyLabel.AutoSize = true;
            groupKeyLabel.Location = new System.Drawing.Point(442, 91);
            groupKeyLabel.Name = "groupKeyLabel";
            groupKeyLabel.Size = new System.Drawing.Size(28, 13);
            groupKeyLabel.TabIndex = 41;
            groupKeyLabel.Text = "Key:";
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataSource = typeof(AhamMetaDataDAL.Product);
            // 
            // productAbbreviationTextBox
            // 
            this.productAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ProductAbbreviation", true));
            this.productAbbreviationTextBox.Location = new System.Drawing.Point(126, 12);
            this.productAbbreviationTextBox.Name = "productAbbreviationTextBox";
            this.productAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.productAbbreviationTextBox.TabIndex = 0;
            // 
            // productKeyTextBox
            // 
            this.productKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ProductKey", true));
            this.productKeyTextBox.Location = new System.Drawing.Point(476, 131);
            this.productKeyTextBox.Name = "productKeyTextBox";
            this.productKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.productKeyTextBox.TabIndex = 1;
            this.productKeyTextBox.TabStop = false;
            // 
            // productNameTextBox
            // 
            this.productNameTextBox.Location = new System.Drawing.Point(126, 38);
            this.productNameTextBox.Name = "productNameTextBox";
            this.productNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.productNameTextBox.TabIndex = 1;
            // 
            // productOwnerKeyTextBox
            // 
            this.productOwnerKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ProductOwnerKey", true));
            this.productOwnerKeyTextBox.Location = new System.Drawing.Point(476, 64);
            this.productOwnerKeyTextBox.Name = "productOwnerKeyTextBox";
            this.productOwnerKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.productOwnerKeyTextBox.TabIndex = 38;
            this.productOwnerKeyTextBox.TabStop = false;
            // 
            // productOwnerNameComboBox
            // 
            this.productOwnerNameComboBox.FormattingEnabled = true;
            this.productOwnerNameComboBox.Location = new System.Drawing.Point(126, 64);
            this.productOwnerNameComboBox.Name = "productOwnerNameComboBox";
            this.productOwnerNameComboBox.Size = new System.Drawing.Size(306, 21);
            this.productOwnerNameComboBox.TabIndex = 2;
            // 
            // groupNameComboBox
            // 
            this.groupNameComboBox.FormattingEnabled = true;
            this.groupNameComboBox.Location = new System.Drawing.Point(126, 91);
            this.groupNameComboBox.Name = "groupNameComboBox";
            this.groupNameComboBox.Size = new System.Drawing.Size(306, 21);
            this.groupNameComboBox.TabIndex = 3;
            // 
            // groupKeyTextBox
            // 
            this.groupKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "GroupKey", true));
            this.groupKeyTextBox.Location = new System.Drawing.Point(476, 89);
            this.groupKeyTextBox.Name = "groupKeyTextBox";
            this.groupKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.groupKeyTextBox.TabIndex = 42;
            this.groupKeyTextBox.TabStop = false;
            // 
            // ProductEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(groupKeyLabel);
            this.Controls.Add(this.groupKeyTextBox);
            this.Controls.Add(groupNameLabel);
            this.Controls.Add(this.groupNameComboBox);
            this.Controls.Add(productAbbreviationLabel);
            this.Controls.Add(this.productAbbreviationTextBox);
            this.Controls.Add(productKeyLabel);
            this.Controls.Add(this.productKeyTextBox);
            this.Controls.Add(productNameLabel);
            this.Controls.Add(this.productNameTextBox);
            this.Controls.Add(productOwnerKeyLabel);
            this.Controls.Add(this.productOwnerKeyTextBox);
            this.Controls.Add(productOwnerNameLabel);
            this.Controls.Add(this.productOwnerNameComboBox);
            this.Name = "ProductEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Product";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.productOwnerNameComboBox, 0);
            this.Controls.SetChildIndex(productOwnerNameLabel, 0);
            this.Controls.SetChildIndex(this.productOwnerKeyTextBox, 0);
            this.Controls.SetChildIndex(productOwnerKeyLabel, 0);
            this.Controls.SetChildIndex(this.productNameTextBox, 0);
            this.Controls.SetChildIndex(productNameLabel, 0);
            this.Controls.SetChildIndex(this.productKeyTextBox, 0);
            this.Controls.SetChildIndex(productKeyLabel, 0);
            this.Controls.SetChildIndex(this.productAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(productAbbreviationLabel, 0);
            this.Controls.SetChildIndex(this.groupNameComboBox, 0);
            this.Controls.SetChildIndex(groupNameLabel, 0);
            this.Controls.SetChildIndex(this.groupKeyTextBox, 0);
            this.Controls.SetChildIndex(groupKeyLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource productBindingSource;
        private System.Windows.Forms.TextBox productAbbreviationTextBox;
        private System.Windows.Forms.TextBox productKeyTextBox;
        private System.Windows.Forms.TextBox productNameTextBox;
        private System.Windows.Forms.TextBox productOwnerKeyTextBox;
        private System.Windows.Forms.ComboBox productOwnerNameComboBox;
        private System.Windows.Forms.ComboBox groupNameComboBox;
        private System.Windows.Forms.TextBox groupKeyTextBox;
    }
}