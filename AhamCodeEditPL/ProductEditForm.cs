﻿using System;
using System.Windows.Forms;

using System.Collections;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ProductEditForm : HaiObjectEditForm
    {
        private Product _productToEdit;
        public ProductEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _productToEdit = (Product)parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);    // establish handler for Load Event
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);          // the form needs to be prepared before assigning values to controls
            productBindingSource.DataSource = _productToEdit;

            // Note "A"
            // N.B.  Databinding creates a problem with the self-referencing between product and productOwner (which is also "product").
            // It appears that the databinding logic is associating the fields in _productToEdit with the data that is retrieved to
            // provide the source for the productOwnerNameComboBox list.  As a result, when the selection is changed in the productOwnerNameComboBox
            // the text for "ProductName" in _productToEdit is also (erroneously) changed, and this can be reflected in the bound
            // productNameTextBox control and the grid.  Therefore, the productNameTextBox must not be bound to 
            // _productToEdit.ProductName.  Additionally, to prevent the changes from being dynamically displayed in the source grid
            // the form is shown using the Multiple Edit technique rather than the Single Edit.

            productNameTextBox.Text = _productToEdit.ProductName.Trim();

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = ds.GetDataList(HaiBusinessObjectType.Product_Aham);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Product>(productOwnerNameComboBox, result, ComboBoxListType.DataListWithNullItem, "ProductName", "ProductKey", productOwnerKeyTextBox, _productToEdit.ProductOwnerName.Trim(),"ProductName");
            }
            else
            {
                MessageBox.Show("Cannot add/edit product.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Group);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<Group>(groupNameComboBox, result, ComboBoxListType.DataListWithNullItem, "GroupName", "GroupKey", groupKeyTextBox, _productToEdit.GroupName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit product.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
       }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (productNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (productAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            // set the property values in the object that was the target of the edit
            MarkChangedProperties(_productToEdit);


            // See long comment Note "A" in EditForm_Load() for an explanation
            // ... of the following special treatment that is required
            _productToEdit.ProductName = productNameTextBox.Text.Trim();
            _productToEdit.ProductOwnerName = productOwnerNameComboBox.Text.Trim();

            _productToEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
