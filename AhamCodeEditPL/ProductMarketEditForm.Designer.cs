﻿namespace AhamMetaDataPL
{
    partial class ProductMarketEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label channelKeyLabel;
            System.Windows.Forms.Label channelNameLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label marketKeyLabel;
            System.Windows.Forms.Label marketNameLabel;
            System.Windows.Forms.Label productKeyLabel;
            System.Windows.Forms.Label productMarketKeyLabel;
            System.Windows.Forms.Label productNameLabel;
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.productMarketBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.channelKeyTextBox = new System.Windows.Forms.TextBox();
            this.channelNameComboBox = new System.Windows.Forms.ComboBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.marketKeyTextBox = new System.Windows.Forms.TextBox();
            this.marketNameComboBox = new System.Windows.Forms.ComboBox();
            this.productKeyTextBox = new System.Windows.Forms.TextBox();
            this.productMarketKeyTextBox = new System.Windows.Forms.TextBox();
            this.productNameComboBox = new System.Windows.Forms.ComboBox();
            beginDateLabel = new System.Windows.Forms.Label();
            channelKeyLabel = new System.Windows.Forms.Label();
            channelNameLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            marketKeyLabel = new System.Windows.Forms.Label();
            marketNameLabel = new System.Windows.Forms.Label();
            productKeyLabel = new System.Windows.Forms.Label();
            productMarketKeyLabel = new System.Windows.Forms.Label();
            productNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.productMarketBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(405, 181);
            this.btnOK.TabIndex = 5;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(486, 181);
            this.btnCancel.TabIndex = 6;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(87, 96);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 30;
            beginDateLabel.Text = "Begin Date:";
            // 
            // channelKeyLabel
            // 
            channelKeyLabel.AutoSize = true;
            channelKeyLabel.Location = new System.Drawing.Point(484, 73);
            channelKeyLabel.Name = "channelKeyLabel";
            channelKeyLabel.Size = new System.Drawing.Size(28, 13);
            channelKeyLabel.TabIndex = 32;
            channelKeyLabel.Text = "Key:";
            // 
            // channelNameLabel
            // 
            channelNameLabel.AutoSize = true;
            channelNameLabel.Location = new System.Drawing.Point(6, 70);
            channelNameLabel.Name = "channelNameLabel";
            channelNameLabel.Size = new System.Drawing.Size(80, 13);
            channelNameLabel.TabIndex = 34;
            channelNameLabel.Text = "Channel Name:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(300, 99);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 36;
            endDateLabel.Text = "End Date:";
            // 
            // marketKeyLabel
            // 
            marketKeyLabel.AutoSize = true;
            marketKeyLabel.Location = new System.Drawing.Point(483, 45);
            marketKeyLabel.Name = "marketKeyLabel";
            marketKeyLabel.Size = new System.Drawing.Size(28, 13);
            marketKeyLabel.TabIndex = 38;
            marketKeyLabel.Text = "Key:";
            // 
            // marketNameLabel
            // 
            marketNameLabel.AutoSize = true;
            marketNameLabel.Location = new System.Drawing.Point(6, 45);
            marketNameLabel.Name = "marketNameLabel";
            marketNameLabel.Size = new System.Drawing.Size(74, 13);
            marketNameLabel.TabIndex = 40;
            marketNameLabel.Text = "Market Name:";
            // 
            // productKeyLabel
            // 
            productKeyLabel.AutoSize = true;
            productKeyLabel.Location = new System.Drawing.Point(483, 17);
            productKeyLabel.Name = "productKeyLabel";
            productKeyLabel.Size = new System.Drawing.Size(28, 13);
            productKeyLabel.TabIndex = 42;
            productKeyLabel.Text = "Key:";
            // 
            // productMarketKeyLabel
            // 
            productMarketKeyLabel.AutoSize = true;
            productMarketKeyLabel.Location = new System.Drawing.Point(405, 146);
            productMarketKeyLabel.Name = "productMarketKeyLabel";
            productMarketKeyLabel.Size = new System.Drawing.Size(104, 13);
            productMarketKeyLabel.TabIndex = 44;
            productMarketKeyLabel.Text = "Product Market Key:";
            // 
            // productNameLabel
            // 
            productNameLabel.AutoSize = true;
            productNameLabel.Location = new System.Drawing.Point(6, 15);
            productNameLabel.Name = "productNameLabel";
            productNameLabel.Size = new System.Drawing.Size(78, 13);
            productNameLabel.TabIndex = 46;
            productNameLabel.Text = "Product Name:";
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productMarketBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(157, 93);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(95, 20);
            this.beginDateTextBox.TabIndex = 3;
            // 
            // productMarketBindingSource
            // 
            this.productMarketBindingSource.DataSource = typeof(AhamMetaDataDAL.ProductMarket);
            // 
            // channelKeyTextBox
            // 
            this.channelKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productMarketBindingSource, "ChannelKey", true));
            this.channelKeyTextBox.Location = new System.Drawing.Point(515, 69);
            this.channelKeyTextBox.Name = "channelKeyTextBox";
            this.channelKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.channelKeyTextBox.TabIndex = 33;
            this.channelKeyTextBox.TabStop = false;
            // 
            // channelNameComboBox
            // 
            this.channelNameComboBox.FormattingEnabled = true;
            this.channelNameComboBox.Location = new System.Drawing.Point(89, 66);
            this.channelNameComboBox.Name = "channelNameComboBox";
            this.channelNameComboBox.Size = new System.Drawing.Size(375, 21);
            this.channelNameComboBox.TabIndex = 2;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productMarketBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(370, 92);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(95, 20);
            this.endDateTextBox.TabIndex = 4;
            // 
            // marketKeyTextBox
            // 
            this.marketKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productMarketBindingSource, "MarketKey", true));
            this.marketKeyTextBox.Location = new System.Drawing.Point(514, 41);
            this.marketKeyTextBox.Name = "marketKeyTextBox";
            this.marketKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.marketKeyTextBox.TabIndex = 39;
            this.marketKeyTextBox.TabStop = false;
            // 
            // marketNameComboBox
            // 
            this.marketNameComboBox.FormattingEnabled = true;
            this.marketNameComboBox.Location = new System.Drawing.Point(89, 39);
            this.marketNameComboBox.Name = "marketNameComboBox";
            this.marketNameComboBox.Size = new System.Drawing.Size(375, 21);
            this.marketNameComboBox.TabIndex = 1;
            // 
            // productKeyTextBox
            // 
            this.productKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productMarketBindingSource, "ProductKey", true));
            this.productKeyTextBox.Location = new System.Drawing.Point(514, 13);
            this.productKeyTextBox.Name = "productKeyTextBox";
            this.productKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.productKeyTextBox.TabIndex = 43;
            this.productKeyTextBox.TabStop = false;
            // 
            // productMarketKeyTextBox
            // 
            this.productMarketKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productMarketBindingSource, "ProductMarketKey", true));
            this.productMarketKeyTextBox.Location = new System.Drawing.Point(515, 143);
            this.productMarketKeyTextBox.Name = "productMarketKeyTextBox";
            this.productMarketKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.productMarketKeyTextBox.TabIndex = 45;
            this.productMarketKeyTextBox.TabStop = false;
            // 
            // productNameComboBox
            // 
            this.productNameComboBox.FormattingEnabled = true;
            this.productNameComboBox.Location = new System.Drawing.Point(89, 12);
            this.productNameComboBox.Name = "productNameComboBox";
            this.productNameComboBox.Size = new System.Drawing.Size(375, 21);
            this.productNameComboBox.TabIndex = 0;
            // 
            // ProductMarketEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 216);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(channelKeyLabel);
            this.Controls.Add(this.channelKeyTextBox);
            this.Controls.Add(channelNameLabel);
            this.Controls.Add(this.channelNameComboBox);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(marketKeyLabel);
            this.Controls.Add(this.marketKeyTextBox);
            this.Controls.Add(marketNameLabel);
            this.Controls.Add(this.marketNameComboBox);
            this.Controls.Add(productKeyLabel);
            this.Controls.Add(this.productKeyTextBox);
            this.Controls.Add(productMarketKeyLabel);
            this.Controls.Add(this.productMarketKeyTextBox);
            this.Controls.Add(productNameLabel);
            this.Controls.Add(this.productNameComboBox);
            this.Name = "ProductMarketEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Product/Market";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.productNameComboBox, 0);
            this.Controls.SetChildIndex(productNameLabel, 0);
            this.Controls.SetChildIndex(this.productMarketKeyTextBox, 0);
            this.Controls.SetChildIndex(productMarketKeyLabel, 0);
            this.Controls.SetChildIndex(this.productKeyTextBox, 0);
            this.Controls.SetChildIndex(productKeyLabel, 0);
            this.Controls.SetChildIndex(this.marketNameComboBox, 0);
            this.Controls.SetChildIndex(marketNameLabel, 0);
            this.Controls.SetChildIndex(this.marketKeyTextBox, 0);
            this.Controls.SetChildIndex(marketKeyLabel, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            this.Controls.SetChildIndex(this.channelNameComboBox, 0);
            this.Controls.SetChildIndex(channelNameLabel, 0);
            this.Controls.SetChildIndex(this.channelKeyTextBox, 0);
            this.Controls.SetChildIndex(channelKeyLabel, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.productMarketBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource productMarketBindingSource;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.TextBox channelKeyTextBox;
        private System.Windows.Forms.ComboBox channelNameComboBox;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.TextBox marketKeyTextBox;
        private System.Windows.Forms.ComboBox marketNameComboBox;
        private System.Windows.Forms.TextBox productKeyTextBox;
        private System.Windows.Forms.TextBox productMarketKeyTextBox;
        private System.Windows.Forms.ComboBox productNameComboBox;
    }
}