﻿using System;
using System.Collections;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;
using System.Data.Linq;

using System.Configuration;
using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ProductMarketEditForm : HaiObjectEditForm
    {
        private ProductMarket _productMarketToEdit;

        public ProductMarketEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _productMarketToEdit = (ProductMarket)_parameters.EditObject;

            this.Load += new System.EventHandler(EditForm_Load);    // establish handler for Load Event
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);          // the form needs to be prepared before assigning values to controls
            productMarketBindingSource.DataSource = _productMarketToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = ds.GetDataList(HaiBusinessObjectType.Market);
            if (result.Success)
            {
                HaiBindingList<Market> marketList = (HaiBindingList<Market>)result.DataList;

                if (marketList.Count == 0)
                {
                    MessageBox.Show("Cannot add/edit product/market because no markets are defined.", "Data integrity error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                    return;
                }

                SetupAnyComboBoxForDropDownList<Market>(marketNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "MarketName", "MarketKey", marketKeyTextBox, _productMarketToEdit.MarketName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit product/market because market fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Channel);
            if (result.Success)
            {
                HaiBindingList<Channel> channelList = (HaiBindingList<Channel>)result.DataList;

                if (channelList.Count == 0)
                {
                    MessageBox.Show("Cannot add/edit product/market because no channels are defined.", "Data integrity error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                    return;
                }

                SetupAnyComboBoxForDropDownList<Channel>(channelNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ChannelName", "ChannelKey", channelKeyTextBox, _productMarketToEdit.ChannelName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit product/market because channel fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            result = ds.GetDataList(HaiBusinessObjectType.Product_Aham);
            if (result.Success)
            {
                HaiBindingList<Product> productList = (HaiBindingList<Product>)result.DataList;

                if (productList.Count == 0)
                {
                    MessageBox.Show("Cannot add/edit product/market because no products are defined.", "Data integrity error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                    return;
                }

                SetupAnyComboBoxForDropDownList<Product>(productNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ProductName", "ProductKey", productKeyTextBox, _productMarketToEdit.ProductName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit product/market because product fetch failed.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            bool abort = false;

            message = ValidateDaterange(_productMarketToEdit, DateRangeValidationType.Monthly);
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (marketNameComboBox.Items.Count > 0 && marketNameComboBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A market must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (channelNameComboBox.Items.Count > 0 && channelNameComboBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A channel must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (productNameComboBox.Items.Count > 0 && productNameComboBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A product must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            // set the flags in the property controllers
            MarkChangedProperties(_productMarketToEdit);   // this is a multi-edit form
            _productMarketToEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
