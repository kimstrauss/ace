﻿namespace AhamMetaDataPL
{
    partial class ProductSetEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label productSetAbbreviationLabel;
            System.Windows.Forms.Label productSetKeyLabel;
            System.Windows.Forms.Label productSetNameLabel;
            this.productSetAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.productSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.productSetNameTextBox = new System.Windows.Forms.TextBox();
            productSetAbbreviationLabel = new System.Windows.Forms.Label();
            productSetKeyLabel = new System.Windows.Forms.Label();
            productSetNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.productSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 4;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 2;
            // 
            // productSetAbbreviationLabel
            // 
            productSetAbbreviationLabel.AutoSize = true;
            productSetAbbreviationLabel.Location = new System.Drawing.Point(3, 15);
            productSetAbbreviationLabel.Name = "productSetAbbreviationLabel";
            productSetAbbreviationLabel.Size = new System.Drawing.Size(128, 13);
            productSetAbbreviationLabel.TabIndex = 30;
            productSetAbbreviationLabel.Text = "Product Set Abbreviation:";
            // 
            // productSetKeyLabel
            // 
            productSetKeyLabel.AutoSize = true;
            productSetKeyLabel.Location = new System.Drawing.Point(3, 67);
            productSetKeyLabel.Name = "productSetKeyLabel";
            productSetKeyLabel.Size = new System.Drawing.Size(87, 13);
            productSetKeyLabel.TabIndex = 32;
            productSetKeyLabel.Text = "Product Set Key:";
            // 
            // productSetNameLabel
            // 
            productSetNameLabel.AutoSize = true;
            productSetNameLabel.Location = new System.Drawing.Point(3, 41);
            productSetNameLabel.Name = "productSetNameLabel";
            productSetNameLabel.Size = new System.Drawing.Size(97, 13);
            productSetNameLabel.TabIndex = 34;
            productSetNameLabel.Text = "Product Set Name:";
            // 
            // productSetAbbreviationTextBox
            // 
            this.productSetAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productSetBindingSource, "ProductSetAbbreviation", true));
            this.productSetAbbreviationTextBox.Location = new System.Drawing.Point(137, 12);
            this.productSetAbbreviationTextBox.Name = "productSetAbbreviationTextBox";
            this.productSetAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.productSetAbbreviationTextBox.TabIndex = 0;
            // 
            // productSetBindingSource
            // 
            this.productSetBindingSource.DataSource = typeof(AhamMetaDataDAL.ProductSet);
            // 
            // productSetKeyTextBox
            // 
            this.productSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productSetBindingSource, "ProductSetKey", true));
            this.productSetKeyTextBox.Location = new System.Drawing.Point(137, 64);
            this.productSetKeyTextBox.Name = "productSetKeyTextBox";
            this.productSetKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.productSetKeyTextBox.TabIndex = 33;
            this.productSetKeyTextBox.TabStop = false;
            // 
            // productSetNameTextBox
            // 
            this.productSetNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productSetBindingSource, "ProductSetName", true));
            this.productSetNameTextBox.Location = new System.Drawing.Point(137, 38);
            this.productSetNameTextBox.Name = "productSetNameTextBox";
            this.productSetNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.productSetNameTextBox.TabIndex = 1;
            // 
            // ProductSetEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(productSetAbbreviationLabel);
            this.Controls.Add(this.productSetAbbreviationTextBox);
            this.Controls.Add(productSetKeyLabel);
            this.Controls.Add(this.productSetKeyTextBox);
            this.Controls.Add(productSetNameLabel);
            this.Controls.Add(this.productSetNameTextBox);
            this.Name = "ProductSetEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ProductSetEditForm";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.productSetNameTextBox, 0);
            this.Controls.SetChildIndex(productSetNameLabel, 0);
            this.Controls.SetChildIndex(this.productSetKeyTextBox, 0);
            this.Controls.SetChildIndex(productSetKeyLabel, 0);
            this.Controls.SetChildIndex(this.productSetAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(productSetAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.productSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource productSetBindingSource;
        private System.Windows.Forms.TextBox productSetAbbreviationTextBox;
        private System.Windows.Forms.TextBox productSetKeyTextBox;
        private System.Windows.Forms.TextBox productSetNameTextBox;
    }
}