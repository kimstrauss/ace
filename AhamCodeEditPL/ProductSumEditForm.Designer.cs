﻿namespace AhamMetaDataPL
{
    partial class ProductSumEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblTargetList = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.sourceGrid = new HaiBusinessUI.ucReadonlyDatagridForm();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.targetGrid = new HaiBusinessUI.ucReadonlyDatagridForm();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAggregatedProduct = new System.Windows.Forms.TextBox();
            this.btnPrintTargetGrid = new System.Windows.Forms.Button();
            this.btnPrintSourceGrid = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(779, 475);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(860, 475);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblTargetList
            // 
            this.lblTargetList.AutoSize = true;
            this.lblTargetList.Location = new System.Drawing.Point(532, 60);
            this.lblTargetList.Name = "lblTargetList";
            this.lblTargetList.Size = new System.Drawing.Size(117, 13);
            this.lblTargetList.TabIndex = 18;
            this.lblTargetList.Text = "Aggregate components";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Available products";
            // 
            // sourceGrid
            // 
            this.sourceGrid.ActionRedirectionObjectType = HaiBusinessObject.HaiBusinessObjectType.UnknownHaiBusinessObjectType;
            this.sourceGrid.BackColor = System.Drawing.SystemColors.Control;
            this.sourceGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sourceGrid.Location = new System.Drawing.Point(12, 77);
            this.sourceGrid.Name = "sourceGrid";
            this.sourceGrid.Size = new System.Drawing.Size(403, 357);
            this.sourceGrid.TabIndex = 1;
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(432, 118);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(79, 25);
            this.btnRemove.TabIndex = 3;
            this.btnRemove.Text = "<< Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(432, 89);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(79, 25);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add >>";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // targetGrid
            // 
            this.targetGrid.ActionRedirectionObjectType = HaiBusinessObject.HaiBusinessObjectType.UnknownHaiBusinessObjectType;
            this.targetGrid.BackColor = System.Drawing.SystemColors.Control;
            this.targetGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.targetGrid.Location = new System.Drawing.Point(529, 77);
            this.targetGrid.Name = "targetGrid";
            this.targetGrid.Size = new System.Drawing.Size(403, 357);
            this.targetGrid.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Aggregated product:";
            // 
            // txtAggregatedProduct
            // 
            this.txtAggregatedProduct.Location = new System.Drawing.Point(125, 19);
            this.txtAggregatedProduct.Name = "txtAggregatedProduct";
            this.txtAggregatedProduct.Size = new System.Drawing.Size(807, 20);
            this.txtAggregatedProduct.TabIndex = 0;
            // 
            // btnPrintTargetGrid
            // 
            this.btnPrintTargetGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintTargetGrid.Location = new System.Drawing.Point(857, 440);
            this.btnPrintTargetGrid.Name = "btnPrintTargetGrid";
            this.btnPrintTargetGrid.Size = new System.Drawing.Size(75, 23);
            this.btnPrintTargetGrid.TabIndex = 35;
            this.btnPrintTargetGrid.Text = "Print";
            this.btnPrintTargetGrid.UseVisualStyleBackColor = true;
            this.btnPrintTargetGrid.Click += new System.EventHandler(this.btnPrintTargetGrid_Click);
            // 
            // btnPrintSourceGrid
            // 
            this.btnPrintSourceGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintSourceGrid.Location = new System.Drawing.Point(340, 440);
            this.btnPrintSourceGrid.Name = "btnPrintSourceGrid";
            this.btnPrintSourceGrid.Size = new System.Drawing.Size(75, 23);
            this.btnPrintSourceGrid.TabIndex = 36;
            this.btnPrintSourceGrid.Text = "Print";
            this.btnPrintSourceGrid.UseVisualStyleBackColor = true;
            this.btnPrintSourceGrid.Click += new System.EventHandler(this.btnPrintSourceGrid_Click);
            // 
            // ProductSumEditForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(947, 510);
            this.Controls.Add(this.btnPrintSourceGrid);
            this.Controls.Add(this.btnPrintTargetGrid);
            this.Controls.Add(this.txtAggregatedProduct);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblTargetList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sourceGrid);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.targetGrid);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Name = "ProductSumEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit the components of a product aggregate";
            this.Load += new System.EventHandler(this.ProductSumEditForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblTargetList;
        private System.Windows.Forms.Label label1;
        private HaiBusinessUI.ucReadonlyDatagridForm sourceGrid;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private HaiBusinessUI.ucReadonlyDatagridForm targetGrid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAggregatedProduct;
        private System.Windows.Forms.Button btnPrintTargetGrid;
        private System.Windows.Forms.Button btnPrintSourceGrid;
    }
}