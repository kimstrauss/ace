﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;

using AhamMetaDataDAL;
using HaiBusinessObject;
using HaiBusinessUI;
using HaiMetaDataDAL;
using HaiInterfaces;

namespace AhamMetaDataPL
{
    public partial class ProductSumEditForm : Form
    {
        private Product _productToAggregate;
        private EditFormParameters _parameters;

        private HaiBindingList<Product> _productList;
        private HaiBindingList<Product> _availableProducts = new HaiBindingList<Product>();
        private HaiBindingList<Product> _originalComponentProducts = new HaiBindingList<Product>();
        private HaiBindingList<Product> _finalComponentProducts = new HaiBindingList<Product>();
        private HaiBindingList<ProductSum> _originalProductSumList = new HaiBindingList<ProductSum>();

        private HaiBindingList<ProductSum> _beginningComponentList=new HaiBindingList<ProductSum>();
        private AhamDataService _ds;

        public ProductSumEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _productToAggregate = (Product)parameters.EditObject;

            sourceGrid.GUS = _parameters.GloballyUsefulStuff;
            targetGrid.GUS = _parameters.GloballyUsefulStuff;

            sourceGrid.DataGridView.CellDoubleClick += new DataGridViewCellEventHandler(SourceDataGridView_CellDoubleClick);
            targetGrid.DataGridView.CellDoubleClick+=new DataGridViewCellEventHandler(TargetDataGridView_CellDoubleClick);
        }

        void SourceDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnAdd_Click(null, null);
        }

        void TargetDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnRemove_Click(null, null);
        }

        private void ProductSumEditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            txtAggregatedProduct.Text = _productToAggregate.ProductName + " [" + _productToAggregate.ProductAbbreviation + "]";
            txtAggregatedProduct.ReadOnly = true;

            _ds = new AhamDataService(new DataServiceParameters());

            DataAccessResult result;

            // get the list of all products
            result = _ds.GetDataList(HaiBusinessObjectType.Product_Aham);
            if (result.Success)
            {
                _productList = (HaiBindingList<Product>)result.DataList;
                HaiReports.ReportParameters reportParametersProductList = new HaiReports.ReportParameters();
                reportParametersProductList.SetBasicProperties(HaiReports.ListType.Dictionary);
                _productList.ReportParameters = reportParametersProductList;

                // initialize the available products to all products
                foreach (Product product in _productList)
                    _availableProducts.AddToList(product);
            }
            else
            {
                MessageBox.Show("Unable to load Products\r\n" + result.Message, "Data load error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
            }

            // create the list of available products (not in the aggregate list) and the aggregate list
            result = _ds.GetDataList(HaiBusinessObjectType.ProductSum);
            if (result.Success)
            {
                // get the list of all ProductSum objects and reduce to ProductSum objects related to the _productToAggregate
                HaiBindingList<ProductSum> fullProductSumList = (HaiBindingList<ProductSum>)result.DataList;
                var thisProductSumList = fullProductSumList.Where<ProductSum>(x => (x.ProductKey == _productToAggregate.ProductKey)).AsEnumerable<ProductSum>();

                // configure the available and component lists by "adding" all component products 
                foreach (ProductSum productSum in thisProductSumList)
                {
                    _originalProductSumList.AddToList(productSum);
                    Product aComponentProduct = _productList.GetItemWithPrimaryKey(productSum.ComponentProductKey);
                    _originalComponentProducts.AddToList(aComponentProduct);
                    AddToComponentList(aComponentProduct);
                }
            }
            else
            {
                MessageBox.Show("Unable to load ProductSums\r\n" + result.Message, "Data load error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
            }

            // fill the sourceGrid with the available products
            _availableProducts.SortOnProperty("ProductAbbreviation", System.ComponentModel.ListSortDirection.Ascending);

            // inject ReportParameters to support report writing
            HaiReports.ReportParameters reportParametersAvailableProducts = new HaiReports.ReportParameters();
            reportParametersAvailableProducts.SetBasicProperties(HaiReports.ListType.Dictionary);
            _availableProducts.ReportParameters = reportParametersAvailableProducts;

            DataAccessResult resultForAvailable = new DataAccessResult();   // create a dummy DataAccessResult
            resultForAvailable.Success = true;
            resultForAvailable.DataList = _availableProducts;
            resultForAvailable.BrowsablePropertyList = Product.GetBrowsablePropertyList();

            string[] availCols = { "ProductAbbreviation", "ProductName", "ProductKey" };
            sourceGrid.PrepareGridForDisplay(resultForAvailable, availCols.ToList<string>());


            // fill the targetGrid with the components
            _finalComponentProducts.SortOnProperty("ProductAbbreviation", System.ComponentModel.ListSortDirection.Ascending);

            DataAccessResult resultForComponents = new DataAccessResult();   // create a dummy DataAccessResult
            resultForComponents.Success = true;
            resultForComponents.DataList = _finalComponentProducts;
            resultForComponents.BrowsablePropertyList = Product.GetBrowsablePropertyList();

            // inject ReportParameters to support report writing
            HaiReports.ReportParameters reportParametersComponentProduct = new HaiReports.ReportParameters();
            reportParametersComponentProduct.SetBasicProperties(HaiReports.ListType.unknown);
            _finalComponentProducts.ReportParameters = reportParametersComponentProduct;

            string[] componentCols = { "ProductAbbreviation", "ProductName", "ProductKey" };
            targetGrid.PrepareGridForDisplay(resultForComponents, componentCols.ToList<string>());

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        private void AddToComponentList(Product productToAdd)
        {
            bool ok = _availableProducts.Remove(productToAdd);      // remove the selected product from the available list
            if (!ok)
            {
                throw new Exception("Internal error.  Missing product in available list.");
            }

            _finalComponentProducts.AddToList(productToAdd);        // ... and add it to the final list.
            _availableProducts.SortOnProperty("ProductAbbreviation", System.ComponentModel.ListSortDirection.Ascending);
        }

        private void RemoveFromCompnentList(Product productToRemove)
        {
            if (productToRemove.ProductKey == _productToAggregate.ProductKey)             // the "self" item must always be in the list
            {
                MessageBox.Show("The component list must contain the main product", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            bool ok = _finalComponentProducts.Remove(productToRemove);  // remove the selected product from the component list
            if (!ok)
            {
                throw new Exception("Internal error.  Missing product in components list.");
            }

            _availableProducts.AddToList(productToRemove);              // ... and return it to the pool in the available list.
            _availableProducts.SortOnProperty("ProductAbbreviation", System.ComponentModel.ListSortDirection.Ascending);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            sourceGrid.SelectRowForCurrentCell();
            HaiBindingList<Product> itemsToAdd = sourceGrid.GetReadonlyBindingListOfSelectedItems<Product>();
            if (itemsToAdd.Count == 0)
            {
                MessageBox.Show("At least product must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            foreach (Product product in itemsToAdd)
            {
                AddToComponentList(product);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            targetGrid.SelectRowForCurrentCell();
            HaiBindingList<Product> itemsToRemove = targetGrid.GetReadonlyBindingListOfSelectedItems<Product>();
            if (itemsToRemove.Count == 0)
            {
                MessageBox.Show("At least product must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            foreach (Product product in itemsToRemove)
            {
                RemoveFromCompnentList(product);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // create a list of ProductSum items to added and deleted
            List<HaiBusinessObjectBase> actionList = new List<HaiBusinessObjectBase>();

            // find the new products for the aggregate and create a new ProductSum objects
            foreach (Product product in _finalComponentProducts)
            {
                if (!_originalComponentProducts.Contains<Product>(product))
                {
                    ProductSum productSum = new ProductSum();
                    productSum.ComponentProductKey = product.ProductKey;
                    productSum.ProductKey = _productToAggregate.ProductKey;
                    actionList.Add(productSum);
                }
            }

            // find original products that are not in final; find those ProductSum objects and mark for delete.
            foreach (Product product in _originalComponentProducts)
            {
                if (!_finalComponentProducts.Contains<Product>(product))
                {
                    foreach (ProductSum productSum in _originalProductSumList)
                    {
                        if (productSum.ComponentProductKey == product.ProductKey)
                        {
                            productSum.Delete();
                            actionList.Add(productSum);
                            continue;
                        }
                    }
                }
            }

            // set the actionList as the list of selected items.
            _parameters.ItemsSelectedForEdit = actionList;

            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnPrintSourceGrid_Click(object sender, EventArgs e)
        {
            sourceGrid.PrintGridReport();
        }

        private void btnPrintTargetGrid_Click(object sender, EventArgs e)
        {
            targetGrid.PrintGridReport();
        }
    }
}
