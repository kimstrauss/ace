﻿namespace AhamMetaDataPL
{
    partial class ReleaseExceptionEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label outputReportIDLabel;
            System.Windows.Forms.Label outputReportKeyLabel;
            System.Windows.Forms.Label releaseExceptionKeyLabel;
            System.Windows.Forms.Label reportEndDateLabel;
            this.hideFinalCheckBox = new System.Windows.Forms.CheckBox();
            this.releaseExceptionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.hidePublicCheckBox = new System.Windows.Forms.CheckBox();
            this.outputReportIDComboBox = new System.Windows.Forms.ComboBox();
            this.outputReportKeyTextBox = new System.Windows.Forms.TextBox();
            this.releaseExceptionKeyTextBox = new System.Windows.Forms.TextBox();
            this.reportEndDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            outputReportIDLabel = new System.Windows.Forms.Label();
            outputReportKeyLabel = new System.Windows.Forms.Label();
            releaseExceptionKeyLabel = new System.Windows.Forms.Label();
            reportEndDateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.releaseExceptionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(379, 181);
            this.btnOK.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(460, 181);
            this.btnCancel.TabIndex = 5;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            // 
            // outputReportIDLabel
            // 
            outputReportIDLabel.AutoSize = true;
            outputReportIDLabel.Location = new System.Drawing.Point(9, 15);
            outputReportIDLabel.Name = "outputReportIDLabel";
            outputReportIDLabel.Size = new System.Drawing.Size(91, 13);
            outputReportIDLabel.TabIndex = 34;
            outputReportIDLabel.Text = "Output Report ID:";
            // 
            // outputReportKeyLabel
            // 
            outputReportKeyLabel.AutoSize = true;
            outputReportKeyLabel.Location = new System.Drawing.Point(458, 15);
            outputReportKeyLabel.Name = "outputReportKeyLabel";
            outputReportKeyLabel.Size = new System.Drawing.Size(28, 13);
            outputReportKeyLabel.TabIndex = 36;
            outputReportKeyLabel.Text = "Key:";
            // 
            // releaseExceptionKeyLabel
            // 
            releaseExceptionKeyLabel.AutoSize = true;
            releaseExceptionKeyLabel.Location = new System.Drawing.Point(363, 134);
            releaseExceptionKeyLabel.Name = "releaseExceptionKeyLabel";
            releaseExceptionKeyLabel.Size = new System.Drawing.Size(120, 13);
            releaseExceptionKeyLabel.TabIndex = 38;
            releaseExceptionKeyLabel.Text = "Release Exception Key:";
            // 
            // reportEndDateLabel
            // 
            reportEndDateLabel.AutoSize = true;
            reportEndDateLabel.Location = new System.Drawing.Point(10, 43);
            reportEndDateLabel.Name = "reportEndDateLabel";
            reportEndDateLabel.Size = new System.Drawing.Size(90, 13);
            reportEndDateLabel.TabIndex = 40;
            reportEndDateLabel.Text = "Report End Date:";
            // 
            // hideFinalCheckBox
            // 
            this.hideFinalCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.releaseExceptionBindingSource, "HideFinal", true));
            this.hideFinalCheckBox.Location = new System.Drawing.Point(105, 65);
            this.hideFinalCheckBox.Name = "hideFinalCheckBox";
            this.hideFinalCheckBox.Size = new System.Drawing.Size(85, 24);
            this.hideFinalCheckBox.TabIndex = 2;
            this.hideFinalCheckBox.Text = "Hide final";
            this.hideFinalCheckBox.UseVisualStyleBackColor = true;
            // 
            // releaseExceptionBindingSource
            // 
            this.releaseExceptionBindingSource.DataSource = typeof(AhamMetaDataDAL.ReleaseException);
            // 
            // hidePublicCheckBox
            // 
            this.hidePublicCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.releaseExceptionBindingSource, "HidePublic", true));
            this.hidePublicCheckBox.Location = new System.Drawing.Point(217, 65);
            this.hidePublicCheckBox.Name = "hidePublicCheckBox";
            this.hidePublicCheckBox.Size = new System.Drawing.Size(88, 24);
            this.hidePublicCheckBox.TabIndex = 3;
            this.hidePublicCheckBox.Text = "Hide public";
            this.hidePublicCheckBox.UseVisualStyleBackColor = true;
            // 
            // outputReportIDComboBox
            // 
            this.outputReportIDComboBox.FormattingEnabled = true;
            this.outputReportIDComboBox.Location = new System.Drawing.Point(105, 12);
            this.outputReportIDComboBox.Name = "outputReportIDComboBox";
            this.outputReportIDComboBox.Size = new System.Drawing.Size(344, 21);
            this.outputReportIDComboBox.TabIndex = 0;
            // 
            // outputReportKeyTextBox
            // 
            this.outputReportKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.releaseExceptionBindingSource, "OutputReportKey", true));
            this.outputReportKeyTextBox.Location = new System.Drawing.Point(489, 12);
            this.outputReportKeyTextBox.Name = "outputReportKeyTextBox";
            this.outputReportKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.outputReportKeyTextBox.TabIndex = 37;
            this.outputReportKeyTextBox.TabStop = false;
            // 
            // releaseExceptionKeyTextBox
            // 
            this.releaseExceptionKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.releaseExceptionBindingSource, "ReleaseExceptionKey", true));
            this.releaseExceptionKeyTextBox.Location = new System.Drawing.Point(489, 131);
            this.releaseExceptionKeyTextBox.Name = "releaseExceptionKeyTextBox";
            this.releaseExceptionKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.releaseExceptionKeyTextBox.TabIndex = 39;
            this.releaseExceptionKeyTextBox.TabStop = false;
            // 
            // reportEndDateDateTimePicker
            // 
            this.reportEndDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.releaseExceptionBindingSource, "ReportEndDate", true));
            this.reportEndDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.reportEndDateDateTimePicker.Location = new System.Drawing.Point(105, 39);
            this.reportEndDateDateTimePicker.Name = "reportEndDateDateTimePicker";
            this.reportEndDateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.reportEndDateDateTimePicker.TabIndex = 1;
            // 
            // ReleaseExceptionEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 216);
            this.Controls.Add(this.hideFinalCheckBox);
            this.Controls.Add(this.hidePublicCheckBox);
            this.Controls.Add(outputReportIDLabel);
            this.Controls.Add(this.outputReportIDComboBox);
            this.Controls.Add(outputReportKeyLabel);
            this.Controls.Add(this.outputReportKeyTextBox);
            this.Controls.Add(releaseExceptionKeyLabel);
            this.Controls.Add(this.releaseExceptionKeyTextBox);
            this.Controls.Add(reportEndDateLabel);
            this.Controls.Add(this.reportEndDateDateTimePicker);
            this.Name = "ReleaseExceptionEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Release exception";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.reportEndDateDateTimePicker, 0);
            this.Controls.SetChildIndex(reportEndDateLabel, 0);
            this.Controls.SetChildIndex(this.releaseExceptionKeyTextBox, 0);
            this.Controls.SetChildIndex(releaseExceptionKeyLabel, 0);
            this.Controls.SetChildIndex(this.outputReportKeyTextBox, 0);
            this.Controls.SetChildIndex(outputReportKeyLabel, 0);
            this.Controls.SetChildIndex(this.outputReportIDComboBox, 0);
            this.Controls.SetChildIndex(outputReportIDLabel, 0);
            this.Controls.SetChildIndex(this.hidePublicCheckBox, 0);
            this.Controls.SetChildIndex(this.hideFinalCheckBox, 0);
            ((System.ComponentModel.ISupportInitialize)(this.releaseExceptionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource releaseExceptionBindingSource;
        private System.Windows.Forms.CheckBox hideFinalCheckBox;
        private System.Windows.Forms.CheckBox hidePublicCheckBox;
        private System.Windows.Forms.ComboBox outputReportIDComboBox;
        private System.Windows.Forms.TextBox outputReportKeyTextBox;
        private System.Windows.Forms.TextBox releaseExceptionKeyTextBox;
        private System.Windows.Forms.DateTimePicker reportEndDateDateTimePicker;
    }
}