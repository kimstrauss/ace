﻿using System;
using System.Windows.Forms;

using System.Configuration;
using System.Collections;

using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;
using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class ReleaseExceptionEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private ReleaseException _releaseExceptionToEdit;

        public ReleaseExceptionEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _releaseExceptionToEdit = (ReleaseException)parameters.EditObject;

            // constrain range of allowed dates
            reportEndDateDateTimePicker.MinDate = DateTime.Parse(ConfigurationManager.AppSettings["EarliestBeginDate"]);
            reportEndDateDateTimePicker.MaxDate = DateTime.Parse(ConfigurationManager.AppSettings["LatestEndDate"]);

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            releaseExceptionBindingSource.DataSource = _releaseExceptionToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result = ds.GetDataList(HaiBusinessObjectType.OutputReport);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<OutputReport>(outputReportIDComboBox, result, ComboBoxListType.DataListWithBlankItem, "OutputReportID", "OutputReportKey", outputReportKeyTextBox, _releaseExceptionToEdit.OutputReportID);
            }
            else
            {
                MessageBox.Show("Cannot add/edit Release Exception.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;
            string message = string.Empty;

            if (outputReportIDComboBox.Text.Trim() == string.Empty && outputReportIDComboBox.Items.Count>0)
            {
                MessageBox.Show("An output report must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            reportEndDateDateTimePicker.DataBindings["Value"].WriteValue();                         
            MarkChangedProperties(_releaseExceptionToEdit);

            _releaseExceptionToEdit.ReportEndDate = _releaseExceptionToEdit.ReportEndDate.Date;     // drop the time portion of DateTime
            _releaseExceptionToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
