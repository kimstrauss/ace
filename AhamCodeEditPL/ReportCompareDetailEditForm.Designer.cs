﻿namespace AhamMetaDataPL
{
    partial class ReportCompareDetailEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label activityKeyLabel;
            System.Windows.Forms.Label activityNameLabel;
            System.Windows.Forms.Label catalogNameLabel;
            System.Windows.Forms.Label channelNameLabel;
            System.Windows.Forms.Label manufacturerReportNameLabel;
            System.Windows.Forms.Label measureNameLabel;
            System.Windows.Forms.Label reportCompareDetailKeyLabel;
            System.Windows.Forms.Label reportCompareMasterKeyLabel;
            System.Windows.Forms.Label size1NameLabel;
            System.Windows.Forms.Label size2NameLabel;
            System.Windows.Forms.Label size3NameLabel;
            System.Windows.Forms.Label size4NameLabel;
            System.Windows.Forms.Label size5NameLabel;
            System.Windows.Forms.Label useNameLabel;
            this.reportCompareDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.activityKeyTextBox = new System.Windows.Forms.TextBox();
            this.activityNameComboBox = new System.Windows.Forms.ComboBox();
            this.catalogKeyTextBox = new System.Windows.Forms.TextBox();
            this.catalogNameComboBox = new System.Windows.Forms.ComboBox();
            this.channelKeyTextBox = new System.Windows.Forms.TextBox();
            this.channelNameComboBox = new System.Windows.Forms.ComboBox();
            this.isCalendarCheckBox = new System.Windows.Forms.CheckBox();
            this.manufacturerReportKeyTextBox = new System.Windows.Forms.TextBox();
            this.manufacturerReportNameComboBox = new System.Windows.Forms.ComboBox();
            this.measureKeyTextBox = new System.Windows.Forms.TextBox();
            this.measureNameComboBox = new System.Windows.Forms.ComboBox();
            this.reportCompareDetailKeyTextBox = new System.Windows.Forms.TextBox();
            this.reportCompareMasterKeyTextBox = new System.Windows.Forms.TextBox();
            this.size1KeyTextBox = new System.Windows.Forms.TextBox();
            this.size1NameComboBox = new System.Windows.Forms.ComboBox();
            this.size2KeyTextBox = new System.Windows.Forms.TextBox();
            this.size2NameComboBox = new System.Windows.Forms.ComboBox();
            this.size3KeyTextBox = new System.Windows.Forms.TextBox();
            this.size3NameComboBox = new System.Windows.Forms.ComboBox();
            this.size4KeyTextBox = new System.Windows.Forms.TextBox();
            this.size4NameComboBox = new System.Windows.Forms.ComboBox();
            this.size5KeyTextBox = new System.Windows.Forms.TextBox();
            this.size5NameComboBox = new System.Windows.Forms.ComboBox();
            this.useKeyTextBox = new System.Windows.Forms.TextBox();
            this.useNameComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbRightSide = new System.Windows.Forms.RadioButton();
            this.rbLeftSide = new System.Windows.Forms.RadioButton();
            activityKeyLabel = new System.Windows.Forms.Label();
            activityNameLabel = new System.Windows.Forms.Label();
            catalogNameLabel = new System.Windows.Forms.Label();
            channelNameLabel = new System.Windows.Forms.Label();
            manufacturerReportNameLabel = new System.Windows.Forms.Label();
            measureNameLabel = new System.Windows.Forms.Label();
            reportCompareDetailKeyLabel = new System.Windows.Forms.Label();
            reportCompareMasterKeyLabel = new System.Windows.Forms.Label();
            size1NameLabel = new System.Windows.Forms.Label();
            size2NameLabel = new System.Windows.Forms.Label();
            size3NameLabel = new System.Windows.Forms.Label();
            size4NameLabel = new System.Windows.Forms.Label();
            size5NameLabel = new System.Windows.Forms.Label();
            useNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.reportCompareDetailBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(598, 484);
            this.btnOK.TabIndex = 13;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(679, 484);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 484);
            // 
            // activityKeyLabel
            // 
            activityKeyLabel.AutoSize = true;
            activityKeyLabel.Location = new System.Drawing.Point(729, 33);
            activityKeyLabel.Name = "activityKeyLabel";
            activityKeyLabel.Size = new System.Drawing.Size(25, 13);
            activityKeyLabel.TabIndex = 30;
            activityKeyLabel.Text = "Key";
            // 
            // activityNameLabel
            // 
            activityNameLabel.AutoSize = true;
            activityNameLabel.Location = new System.Drawing.Point(10, 85);
            activityNameLabel.Name = "activityNameLabel";
            activityNameLabel.Size = new System.Drawing.Size(75, 13);
            activityNameLabel.TabIndex = 32;
            activityNameLabel.Text = "Activity Name:";
            // 
            // catalogNameLabel
            // 
            catalogNameLabel.AutoSize = true;
            catalogNameLabel.Location = new System.Drawing.Point(10, 112);
            catalogNameLabel.Name = "catalogNameLabel";
            catalogNameLabel.Size = new System.Drawing.Size(77, 13);
            catalogNameLabel.TabIndex = 36;
            catalogNameLabel.Text = "Catalog Name:";
            // 
            // channelNameLabel
            // 
            channelNameLabel.AutoSize = true;
            channelNameLabel.Location = new System.Drawing.Point(10, 139);
            channelNameLabel.Name = "channelNameLabel";
            channelNameLabel.Size = new System.Drawing.Size(80, 13);
            channelNameLabel.TabIndex = 40;
            channelNameLabel.Text = "Channel Name:";
            // 
            // manufacturerReportNameLabel
            // 
            manufacturerReportNameLabel.AutoSize = true;
            manufacturerReportNameLabel.Location = new System.Drawing.Point(10, 58);
            manufacturerReportNameLabel.Name = "manufacturerReportNameLabel";
            manufacturerReportNameLabel.Size = new System.Drawing.Size(139, 13);
            manufacturerReportNameLabel.TabIndex = 46;
            manufacturerReportNameLabel.Text = "Manufacturer Report Name:";
            // 
            // measureNameLabel
            // 
            measureNameLabel.AutoSize = true;
            measureNameLabel.Location = new System.Drawing.Point(10, 166);
            measureNameLabel.Name = "measureNameLabel";
            measureNameLabel.Size = new System.Drawing.Size(82, 13);
            measureNameLabel.TabIndex = 50;
            measureNameLabel.Text = "Measure Name:";
            // 
            // reportCompareDetailKeyLabel
            // 
            reportCompareDetailKeyLabel.AutoSize = true;
            reportCompareDetailKeyLabel.Location = new System.Drawing.Point(544, 443);
            reportCompareDetailKeyLabel.Name = "reportCompareDetailKeyLabel";
            reportCompareDetailKeyLabel.Size = new System.Drawing.Size(138, 13);
            reportCompareDetailKeyLabel.TabIndex = 52;
            reportCompareDetailKeyLabel.Text = "Report Compare Detail Key:";
            // 
            // reportCompareMasterKeyLabel
            // 
            reportCompareMasterKeyLabel.AutoSize = true;
            reportCompareMasterKeyLabel.Location = new System.Drawing.Point(10, 447);
            reportCompareMasterKeyLabel.Name = "reportCompareMasterKeyLabel";
            reportCompareMasterKeyLabel.Size = new System.Drawing.Size(143, 13);
            reportCompareMasterKeyLabel.TabIndex = 54;
            reportCompareMasterKeyLabel.Text = "Report Compare Master Key:";
            // 
            // size1NameLabel
            // 
            size1NameLabel.AutoSize = true;
            size1NameLabel.Location = new System.Drawing.Point(10, 223);
            size1NameLabel.Name = "size1NameLabel";
            size1NameLabel.Size = new System.Drawing.Size(64, 13);
            size1NameLabel.TabIndex = 60;
            size1NameLabel.Text = "Size1Name:";
            // 
            // size2NameLabel
            // 
            size2NameLabel.AutoSize = true;
            size2NameLabel.Location = new System.Drawing.Point(10, 250);
            size2NameLabel.Name = "size2NameLabel";
            size2NameLabel.Size = new System.Drawing.Size(64, 13);
            size2NameLabel.TabIndex = 64;
            size2NameLabel.Text = "Size2Name:";
            // 
            // size3NameLabel
            // 
            size3NameLabel.AutoSize = true;
            size3NameLabel.Location = new System.Drawing.Point(10, 277);
            size3NameLabel.Name = "size3NameLabel";
            size3NameLabel.Size = new System.Drawing.Size(64, 13);
            size3NameLabel.TabIndex = 68;
            size3NameLabel.Text = "Size3Name:";
            // 
            // size4NameLabel
            // 
            size4NameLabel.AutoSize = true;
            size4NameLabel.Location = new System.Drawing.Point(10, 304);
            size4NameLabel.Name = "size4NameLabel";
            size4NameLabel.Size = new System.Drawing.Size(64, 13);
            size4NameLabel.TabIndex = 72;
            size4NameLabel.Text = "Size4Name:";
            // 
            // size5NameLabel
            // 
            size5NameLabel.AutoSize = true;
            size5NameLabel.Location = new System.Drawing.Point(10, 331);
            size5NameLabel.Name = "size5NameLabel";
            size5NameLabel.Size = new System.Drawing.Size(64, 13);
            size5NameLabel.TabIndex = 76;
            size5NameLabel.Text = "Size5Name:";
            // 
            // useNameLabel
            // 
            useNameLabel.AutoSize = true;
            useNameLabel.Location = new System.Drawing.Point(10, 195);
            useNameLabel.Name = "useNameLabel";
            useNameLabel.Size = new System.Drawing.Size(60, 13);
            useNameLabel.TabIndex = 80;
            useNameLabel.Text = "Use Name:";
            // 
            // reportCompareDetailBindingSource
            // 
            this.reportCompareDetailBindingSource.DataSource = typeof(AhamMetaDataDAL.ReportCompareDetail);
            // 
            // activityKeyTextBox
            // 
            this.activityKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "ActivityKey", true));
            this.activityKeyTextBox.Location = new System.Drawing.Point(693, 82);
            this.activityKeyTextBox.Name = "activityKeyTextBox";
            this.activityKeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.activityKeyTextBox.TabIndex = 31;
            this.activityKeyTextBox.TabStop = false;
            // 
            // activityNameComboBox
            // 
            this.activityNameComboBox.FormattingEnabled = true;
            this.activityNameComboBox.Location = new System.Drawing.Point(159, 82);
            this.activityNameComboBox.Name = "activityNameComboBox";
            this.activityNameComboBox.Size = new System.Drawing.Size(523, 21);
            this.activityNameComboBox.TabIndex = 1;
            // 
            // catalogKeyTextBox
            // 
            this.catalogKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "CatalogKey", true));
            this.catalogKeyTextBox.Location = new System.Drawing.Point(693, 109);
            this.catalogKeyTextBox.Name = "catalogKeyTextBox";
            this.catalogKeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.catalogKeyTextBox.TabIndex = 35;
            this.catalogKeyTextBox.TabStop = false;
            // 
            // catalogNameComboBox
            // 
            this.catalogNameComboBox.FormattingEnabled = true;
            this.catalogNameComboBox.Location = new System.Drawing.Point(159, 109);
            this.catalogNameComboBox.Name = "catalogNameComboBox";
            this.catalogNameComboBox.Size = new System.Drawing.Size(523, 21);
            this.catalogNameComboBox.TabIndex = 2;
            // 
            // channelKeyTextBox
            // 
            this.channelKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "ChannelKey", true));
            this.channelKeyTextBox.Location = new System.Drawing.Point(693, 136);
            this.channelKeyTextBox.Name = "channelKeyTextBox";
            this.channelKeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.channelKeyTextBox.TabIndex = 39;
            this.channelKeyTextBox.TabStop = false;
            // 
            // channelNameComboBox
            // 
            this.channelNameComboBox.FormattingEnabled = true;
            this.channelNameComboBox.Location = new System.Drawing.Point(159, 136);
            this.channelNameComboBox.Name = "channelNameComboBox";
            this.channelNameComboBox.Size = new System.Drawing.Size(523, 21);
            this.channelNameComboBox.TabIndex = 3;
            // 
            // isCalendarCheckBox
            // 
            this.isCalendarCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.reportCompareDetailBindingSource, "IsCalendar", true));
            this.isCalendarCheckBox.Location = new System.Drawing.Point(159, 371);
            this.isCalendarCheckBox.Name = "isCalendarCheckBox";
            this.isCalendarCheckBox.Size = new System.Drawing.Size(121, 24);
            this.isCalendarCheckBox.TabIndex = 11;
            this.isCalendarCheckBox.Text = "Is calendar";
            this.isCalendarCheckBox.UseVisualStyleBackColor = true;
            // 
            // manufacturerReportKeyTextBox
            // 
            this.manufacturerReportKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "ManufacturerReportKey", true));
            this.manufacturerReportKeyTextBox.Location = new System.Drawing.Point(693, 55);
            this.manufacturerReportKeyTextBox.Name = "manufacturerReportKeyTextBox";
            this.manufacturerReportKeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.manufacturerReportKeyTextBox.TabIndex = 45;
            this.manufacturerReportKeyTextBox.TabStop = false;
            // 
            // manufacturerReportNameComboBox
            // 
            this.manufacturerReportNameComboBox.FormattingEnabled = true;
            this.manufacturerReportNameComboBox.Location = new System.Drawing.Point(159, 55);
            this.manufacturerReportNameComboBox.Name = "manufacturerReportNameComboBox";
            this.manufacturerReportNameComboBox.Size = new System.Drawing.Size(523, 21);
            this.manufacturerReportNameComboBox.TabIndex = 0;
            // 
            // measureKeyTextBox
            // 
            this.measureKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "MeasureKey", true));
            this.measureKeyTextBox.Location = new System.Drawing.Point(693, 163);
            this.measureKeyTextBox.Name = "measureKeyTextBox";
            this.measureKeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.measureKeyTextBox.TabIndex = 49;
            this.measureKeyTextBox.TabStop = false;
            // 
            // measureNameComboBox
            // 
            this.measureNameComboBox.FormattingEnabled = true;
            this.measureNameComboBox.Location = new System.Drawing.Point(159, 163);
            this.measureNameComboBox.Name = "measureNameComboBox";
            this.measureNameComboBox.Size = new System.Drawing.Size(523, 21);
            this.measureNameComboBox.TabIndex = 4;
            // 
            // reportCompareDetailKeyTextBox
            // 
            this.reportCompareDetailKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "ReportCompareDetailKey", true));
            this.reportCompareDetailKeyTextBox.Location = new System.Drawing.Point(693, 440);
            this.reportCompareDetailKeyTextBox.Name = "reportCompareDetailKeyTextBox";
            this.reportCompareDetailKeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.reportCompareDetailKeyTextBox.TabIndex = 53;
            this.reportCompareDetailKeyTextBox.TabStop = false;
            // 
            // reportCompareMasterKeyTextBox
            // 
            this.reportCompareMasterKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "ReportCompareMasterKey", true));
            this.reportCompareMasterKeyTextBox.Location = new System.Drawing.Point(159, 444);
            this.reportCompareMasterKeyTextBox.Name = "reportCompareMasterKeyTextBox";
            this.reportCompareMasterKeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.reportCompareMasterKeyTextBox.TabIndex = 55;
            this.reportCompareMasterKeyTextBox.TabStop = false;
            // 
            // size1KeyTextBox
            // 
            this.size1KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "Size1Key", true));
            this.size1KeyTextBox.Location = new System.Drawing.Point(693, 220);
            this.size1KeyTextBox.Name = "size1KeyTextBox";
            this.size1KeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.size1KeyTextBox.TabIndex = 59;
            this.size1KeyTextBox.TabStop = false;
            // 
            // size1NameComboBox
            // 
            this.size1NameComboBox.FormattingEnabled = true;
            this.size1NameComboBox.Location = new System.Drawing.Point(159, 220);
            this.size1NameComboBox.Name = "size1NameComboBox";
            this.size1NameComboBox.Size = new System.Drawing.Size(523, 21);
            this.size1NameComboBox.TabIndex = 6;
            // 
            // size2KeyTextBox
            // 
            this.size2KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "Size2Key", true));
            this.size2KeyTextBox.Location = new System.Drawing.Point(693, 247);
            this.size2KeyTextBox.Name = "size2KeyTextBox";
            this.size2KeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.size2KeyTextBox.TabIndex = 63;
            this.size2KeyTextBox.TabStop = false;
            // 
            // size2NameComboBox
            // 
            this.size2NameComboBox.FormattingEnabled = true;
            this.size2NameComboBox.Location = new System.Drawing.Point(159, 247);
            this.size2NameComboBox.Name = "size2NameComboBox";
            this.size2NameComboBox.Size = new System.Drawing.Size(523, 21);
            this.size2NameComboBox.TabIndex = 7;
            // 
            // size3KeyTextBox
            // 
            this.size3KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "Size3Key", true));
            this.size3KeyTextBox.Location = new System.Drawing.Point(693, 274);
            this.size3KeyTextBox.Name = "size3KeyTextBox";
            this.size3KeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.size3KeyTextBox.TabIndex = 67;
            this.size3KeyTextBox.TabStop = false;
            // 
            // size3NameComboBox
            // 
            this.size3NameComboBox.FormattingEnabled = true;
            this.size3NameComboBox.Location = new System.Drawing.Point(159, 274);
            this.size3NameComboBox.Name = "size3NameComboBox";
            this.size3NameComboBox.Size = new System.Drawing.Size(523, 21);
            this.size3NameComboBox.TabIndex = 8;
            // 
            // size4KeyTextBox
            // 
            this.size4KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "Size4Key", true));
            this.size4KeyTextBox.Location = new System.Drawing.Point(693, 301);
            this.size4KeyTextBox.Name = "size4KeyTextBox";
            this.size4KeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.size4KeyTextBox.TabIndex = 71;
            this.size4KeyTextBox.TabStop = false;
            // 
            // size4NameComboBox
            // 
            this.size4NameComboBox.FormattingEnabled = true;
            this.size4NameComboBox.Location = new System.Drawing.Point(159, 301);
            this.size4NameComboBox.Name = "size4NameComboBox";
            this.size4NameComboBox.Size = new System.Drawing.Size(523, 21);
            this.size4NameComboBox.TabIndex = 9;
            // 
            // size5KeyTextBox
            // 
            this.size5KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "Size5Key", true));
            this.size5KeyTextBox.Location = new System.Drawing.Point(693, 328);
            this.size5KeyTextBox.Name = "size5KeyTextBox";
            this.size5KeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.size5KeyTextBox.TabIndex = 75;
            this.size5KeyTextBox.TabStop = false;
            // 
            // size5NameComboBox
            // 
            this.size5NameComboBox.FormattingEnabled = true;
            this.size5NameComboBox.Location = new System.Drawing.Point(159, 328);
            this.size5NameComboBox.Name = "size5NameComboBox";
            this.size5NameComboBox.Size = new System.Drawing.Size(523, 21);
            this.size5NameComboBox.TabIndex = 10;
            // 
            // useKeyTextBox
            // 
            this.useKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareDetailBindingSource, "UseKey", true));
            this.useKeyTextBox.Location = new System.Drawing.Point(693, 192);
            this.useKeyTextBox.Name = "useKeyTextBox";
            this.useKeyTextBox.Size = new System.Drawing.Size(61, 20);
            this.useKeyTextBox.TabIndex = 79;
            this.useKeyTextBox.TabStop = false;
            // 
            // useNameComboBox
            // 
            this.useNameComboBox.FormattingEnabled = true;
            this.useNameComboBox.Location = new System.Drawing.Point(159, 192);
            this.useNameComboBox.Name = "useNameComboBox";
            this.useNameComboBox.Size = new System.Drawing.Size(523, 21);
            this.useNameComboBox.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbRightSide);
            this.groupBox1.Controls.Add(this.rbLeftSide);
            this.groupBox1.Location = new System.Drawing.Point(266, 360);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(190, 40);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Side";
            // 
            // rbRightSide
            // 
            this.rbRightSide.AutoSize = true;
            this.rbRightSide.Location = new System.Drawing.Point(113, 15);
            this.rbRightSide.Name = "rbRightSide";
            this.rbRightSide.Size = new System.Drawing.Size(50, 17);
            this.rbRightSide.TabIndex = 1;
            this.rbRightSide.TabStop = true;
            this.rbRightSide.Text = "Right";
            this.rbRightSide.UseVisualStyleBackColor = true;
            // 
            // rbLeftSide
            // 
            this.rbLeftSide.AutoSize = true;
            this.rbLeftSide.Location = new System.Drawing.Point(16, 14);
            this.rbLeftSide.Name = "rbLeftSide";
            this.rbLeftSide.Size = new System.Drawing.Size(43, 17);
            this.rbLeftSide.TabIndex = 0;
            this.rbLeftSide.TabStop = true;
            this.rbLeftSide.Text = "Left";
            this.rbLeftSide.UseVisualStyleBackColor = true;
            this.rbLeftSide.CheckedChanged += new System.EventHandler(this.rbLeftSide_CheckedChanged);
            // 
            // ReportCompareDetailEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 519);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(activityKeyLabel);
            this.Controls.Add(this.activityKeyTextBox);
            this.Controls.Add(activityNameLabel);
            this.Controls.Add(this.activityNameComboBox);
            this.Controls.Add(this.catalogKeyTextBox);
            this.Controls.Add(catalogNameLabel);
            this.Controls.Add(this.catalogNameComboBox);
            this.Controls.Add(this.channelKeyTextBox);
            this.Controls.Add(channelNameLabel);
            this.Controls.Add(this.channelNameComboBox);
            this.Controls.Add(this.isCalendarCheckBox);
            this.Controls.Add(this.manufacturerReportKeyTextBox);
            this.Controls.Add(manufacturerReportNameLabel);
            this.Controls.Add(this.manufacturerReportNameComboBox);
            this.Controls.Add(this.measureKeyTextBox);
            this.Controls.Add(measureNameLabel);
            this.Controls.Add(this.measureNameComboBox);
            this.Controls.Add(reportCompareDetailKeyLabel);
            this.Controls.Add(this.reportCompareDetailKeyTextBox);
            this.Controls.Add(reportCompareMasterKeyLabel);
            this.Controls.Add(this.reportCompareMasterKeyTextBox);
            this.Controls.Add(this.size1KeyTextBox);
            this.Controls.Add(size1NameLabel);
            this.Controls.Add(this.size1NameComboBox);
            this.Controls.Add(this.size2KeyTextBox);
            this.Controls.Add(size2NameLabel);
            this.Controls.Add(this.size2NameComboBox);
            this.Controls.Add(this.size3KeyTextBox);
            this.Controls.Add(size3NameLabel);
            this.Controls.Add(this.size3NameComboBox);
            this.Controls.Add(this.size4KeyTextBox);
            this.Controls.Add(size4NameLabel);
            this.Controls.Add(this.size4NameComboBox);
            this.Controls.Add(this.size5KeyTextBox);
            this.Controls.Add(size5NameLabel);
            this.Controls.Add(this.size5NameComboBox);
            this.Controls.Add(this.useKeyTextBox);
            this.Controls.Add(useNameLabel);
            this.Controls.Add(this.useNameComboBox);
            this.Name = "ReportCompareDetailEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit report compare detail";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.useNameComboBox, 0);
            this.Controls.SetChildIndex(useNameLabel, 0);
            this.Controls.SetChildIndex(this.useKeyTextBox, 0);
            this.Controls.SetChildIndex(this.size5NameComboBox, 0);
            this.Controls.SetChildIndex(size5NameLabel, 0);
            this.Controls.SetChildIndex(this.size5KeyTextBox, 0);
            this.Controls.SetChildIndex(this.size4NameComboBox, 0);
            this.Controls.SetChildIndex(size4NameLabel, 0);
            this.Controls.SetChildIndex(this.size4KeyTextBox, 0);
            this.Controls.SetChildIndex(this.size3NameComboBox, 0);
            this.Controls.SetChildIndex(size3NameLabel, 0);
            this.Controls.SetChildIndex(this.size3KeyTextBox, 0);
            this.Controls.SetChildIndex(this.size2NameComboBox, 0);
            this.Controls.SetChildIndex(size2NameLabel, 0);
            this.Controls.SetChildIndex(this.size2KeyTextBox, 0);
            this.Controls.SetChildIndex(this.size1NameComboBox, 0);
            this.Controls.SetChildIndex(size1NameLabel, 0);
            this.Controls.SetChildIndex(this.size1KeyTextBox, 0);
            this.Controls.SetChildIndex(this.reportCompareMasterKeyTextBox, 0);
            this.Controls.SetChildIndex(reportCompareMasterKeyLabel, 0);
            this.Controls.SetChildIndex(this.reportCompareDetailKeyTextBox, 0);
            this.Controls.SetChildIndex(reportCompareDetailKeyLabel, 0);
            this.Controls.SetChildIndex(this.measureNameComboBox, 0);
            this.Controls.SetChildIndex(measureNameLabel, 0);
            this.Controls.SetChildIndex(this.measureKeyTextBox, 0);
            this.Controls.SetChildIndex(this.manufacturerReportNameComboBox, 0);
            this.Controls.SetChildIndex(manufacturerReportNameLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerReportKeyTextBox, 0);
            this.Controls.SetChildIndex(this.isCalendarCheckBox, 0);
            this.Controls.SetChildIndex(this.channelNameComboBox, 0);
            this.Controls.SetChildIndex(channelNameLabel, 0);
            this.Controls.SetChildIndex(this.channelKeyTextBox, 0);
            this.Controls.SetChildIndex(this.catalogNameComboBox, 0);
            this.Controls.SetChildIndex(catalogNameLabel, 0);
            this.Controls.SetChildIndex(this.catalogKeyTextBox, 0);
            this.Controls.SetChildIndex(this.activityNameComboBox, 0);
            this.Controls.SetChildIndex(activityNameLabel, 0);
            this.Controls.SetChildIndex(this.activityKeyTextBox, 0);
            this.Controls.SetChildIndex(activityKeyLabel, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.reportCompareDetailBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource reportCompareDetailBindingSource;
        private System.Windows.Forms.TextBox activityKeyTextBox;
        private System.Windows.Forms.ComboBox activityNameComboBox;
        private System.Windows.Forms.TextBox catalogKeyTextBox;
        private System.Windows.Forms.ComboBox catalogNameComboBox;
        private System.Windows.Forms.TextBox channelKeyTextBox;
        private System.Windows.Forms.ComboBox channelNameComboBox;
        private System.Windows.Forms.CheckBox isCalendarCheckBox;
        private System.Windows.Forms.TextBox manufacturerReportKeyTextBox;
        private System.Windows.Forms.ComboBox manufacturerReportNameComboBox;
        private System.Windows.Forms.TextBox measureKeyTextBox;
        private System.Windows.Forms.ComboBox measureNameComboBox;
        private System.Windows.Forms.TextBox reportCompareDetailKeyTextBox;
        private System.Windows.Forms.TextBox reportCompareMasterKeyTextBox;
        private System.Windows.Forms.TextBox size1KeyTextBox;
        private System.Windows.Forms.ComboBox size1NameComboBox;
        private System.Windows.Forms.TextBox size2KeyTextBox;
        private System.Windows.Forms.ComboBox size2NameComboBox;
        private System.Windows.Forms.TextBox size3KeyTextBox;
        private System.Windows.Forms.ComboBox size3NameComboBox;
        private System.Windows.Forms.TextBox size4KeyTextBox;
        private System.Windows.Forms.ComboBox size4NameComboBox;
        private System.Windows.Forms.TextBox size5KeyTextBox;
        private System.Windows.Forms.ComboBox size5NameComboBox;
        private System.Windows.Forms.TextBox useKeyTextBox;
        private System.Windows.Forms.ComboBox useNameComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbRightSide;
        private System.Windows.Forms.RadioButton rbLeftSide;
    }
}