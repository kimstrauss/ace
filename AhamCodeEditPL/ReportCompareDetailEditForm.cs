﻿using System;
using System.Windows.Forms;

using System.Collections;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ReportCompareDetailEditForm : HaiObjectEditForm
    {
        private HaiBindingList<ManufacturerReport> _reportList;
        private ReportCompareDetail _reportCompareDetailToEdit;
        private ReportCompareMaster _reportCompareMaster;
        private AhamDataService _ds;
        private string _nullString = Utilities.GetStringForNull();

        public ReportCompareDetailEditForm(EditFormParameters parameters, ReportCompareMaster reportCompareMaster)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _reportCompareDetailToEdit = (ReportCompareDetail)_parameters.EditObject;
            _reportCompareMaster = reportCompareMaster;
            if (_reportCompareDetailToEdit.IsNew)   // new ReportCompareDetail must have the key for its ReportCompareMasterKey set
                _reportCompareDetailToEdit.ReportCompareMasterKey = _reportCompareMaster.ReportCompareMasterKey;

            _ds = new AhamDataService(new DataServiceParameters());

            this.Load += new System.EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;

            btnInsertNull.Visible = false;
            PrepareTheFormToShow(_parameters);
            reportCompareDetailBindingSource.DataSource = _reportCompareDetailToEdit;

            if (_reportCompareDetailToEdit.Side == 0)
                rbLeftSide.Checked = true;
            else
                rbRightSide.Checked = true;

            if (!GetReportList())
                return;

            DataAccessResult result = new DataAccessResult();
            result.Success = true;
            result.DataList = _reportList;

            SetupAnyComboBoxForDropDownList<ManufacturerReport>(manufacturerReportNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ReportName", "ManufacturerReportKey", 
                manufacturerReportKeyTextBox, _reportCompareDetailToEdit.ManufacturerReportName.Trim(),"ManufacturerReportName");

            // detail that is being added has no report, so arbitrarily set it to the first one in the list
            if (_reportCompareDetailToEdit.ManufacturerReportName == string.Empty)
            {
                _reportCompareDetailToEdit.ManufacturerReportName = ((ManufacturerReport)(manufacturerReportNameComboBox.Items[0])).ReportName;
                _reportCompareDetailToEdit.ManufacturerReportKey = ((ManufacturerReport)(manufacturerReportNameComboBox.Items[0])).ManufacturerReportKey;
            }

            manufacturerReportNameComboBox_TextChanged(null, null); // force the dependent lists to be filled
            manufacturerReportNameComboBox.TextChanged += new EventHandler(manufacturerReportNameComboBox_TextChanged);

            this.Owner.Cursor = Cursors.Default;
        }

        // set content of dependent lists when the manufacturer changes
        void manufacturerReportNameComboBox_TextChanged(object sender, EventArgs e)
        {
            SetActivityList();
            SetCatalogList();
            SetChannelList();
            SetMeasureList();
            SetUseList();
            SetSizeListAll();
        }

        private bool GetReportList()
        {
            bool dataRetrievalFailed = false;
            _reportList = new HaiBindingList<ManufacturerReport>();
            List<int> keyList = new List<int>();
            DataAccessResult result;
            ManufacturerReport report;


            // get related report 1
            report = new ManufacturerReport();
            report.ManufacturerReportKey = _reportCompareMaster.Report1Key;
            result = _ds.DataItemReGet(report);
            if (result.Success)
            {
                _reportList.AddToList((ManufacturerReport)result.SingleItem);
                keyList.Add(((ManufacturerReport)result.SingleItem).ManufacturerReportKey);
            }
            else
                dataRetrievalFailed = true;

            // get related report 2
            report = new ManufacturerReport();
            report.ManufacturerReportKey = _reportCompareMaster.Report2Key;
            result = _ds.DataItemReGet(report);
            if (result.Success)
            {
                if (!keyList.Contains(((ManufacturerReport)result.SingleItem).ManufacturerReportKey))
                {
                    _reportList.AddToList((ManufacturerReport)result.SingleItem);
                    keyList.Add(((ManufacturerReport)result.SingleItem).ManufacturerReportKey);
                }
            }
            else
                dataRetrievalFailed = true;

            // get related report 3, if it exists
            if (_reportCompareMaster.Report3Key.HasValue)
            {
                report = new ManufacturerReport();
                report.ManufacturerReportKey = _reportCompareMaster.Report3Key.Value;
                result = _ds.DataItemReGet(report);
                if (result.Success)
                {
                    if (!keyList.Contains(((ManufacturerReport)result.SingleItem).ManufacturerReportKey))
                    {
                        _reportList.AddToList((ManufacturerReport)result.SingleItem);
                        keyList.Add(((ManufacturerReport)result.SingleItem).ManufacturerReportKey);
                    }
                }
                else
                    dataRetrievalFailed = true;
            }

            // get related report 4, if it exists
            if (_reportCompareMaster.Report4Key.HasValue)
            {
                report = new ManufacturerReport();
                report.ManufacturerReportKey = _reportCompareMaster.Report4Key.Value;
                result = _ds.DataItemReGet(report);
                if (result.Success)
                {
                    if (!keyList.Contains(((ManufacturerReport)result.SingleItem).ManufacturerReportKey))
                    {
                        _reportList.AddToList((ManufacturerReport)result.SingleItem);
                        keyList.Add(((ManufacturerReport)result.SingleItem).ManufacturerReportKey);
                    }
                }
                else
                    dataRetrievalFailed = true;
            }

            // get related report 5, if it exists
            if (_reportCompareMaster.Report5Key.HasValue)
            {
                report = new ManufacturerReport();
                report.ManufacturerReportKey = _reportCompareMaster.Report5Key.Value;
                result = _ds.DataItemReGet(report);
                if (result.Success)
                {
                    if (!keyList.Contains(((ManufacturerReport)result.SingleItem).ManufacturerReportKey))
                    {
                        _reportList.AddToList((ManufacturerReport)result.SingleItem);
                        keyList.Add(((ManufacturerReport)result.SingleItem).ManufacturerReportKey);
                    }
                }
                else
                    dataRetrievalFailed = true;
            }

            if (dataRetrievalFailed)
            {
                MessageBox.Show("Data retrieval failed.", "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return !dataRetrievalFailed;
        }

        private bool SetActivityList()
        {
            bool success = true;
            ManufacturerReport report = manufacturerReportNameComboBox.SelectedItem as ManufacturerReport;

            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ActivitySetMember_Aham);
            if (!result.Success)
            {
                success = false;
                return success;
            }

            HaiBindingList<ActivitySetMember> activitySetMembers = (HaiBindingList<ActivitySetMember>)result.DataList;
            List<ActivitySetMember> activitySetMembersRelated = activitySetMembers.Where<ActivitySetMember>(x => (x.ActivitySetKey == report.ActivitySetKey)).ToList<ActivitySetMember>();
            activitySetMembers.ClearList();
            foreach (ActivitySetMember m in activitySetMembersRelated)
            {
                activitySetMembers.AddToList(m);
            }

            result = new DataAccessResult();
            result.Success = true;
            result.DataList = activitySetMembers;

            SetupAnyComboBoxForDropDownList<ActivitySetMember>(activityNameComboBox, result, ComboBoxListType.DataListWithNullItem, "ActivityName", "ActivityKey", activityKeyTextBox, _reportCompareDetailToEdit.ActivityName.Trim());

            return success;
        }

        private bool SetCatalogList()
        {
            bool success = true;
            ManufacturerReport report = manufacturerReportNameComboBox.SelectedItem as ManufacturerReport;

            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.CatalogSetMember);
            if (!result.Success)
            {
                success = false;
                return success;
            }

            HaiBindingList<CatalogSetMember> catalogSetMembers = (HaiBindingList<CatalogSetMember>)result.DataList;
            List<CatalogSetMember> catalogSetMembersRelated = catalogSetMembers.Where<CatalogSetMember>(x => (x.CatalogSetKey == report.CatalogSetKey)).ToList<CatalogSetMember>();
            catalogSetMembers.ClearList();
            foreach (CatalogSetMember c in catalogSetMembersRelated)
            {
                catalogSetMembers.AddToList(c);
            }

            result = new DataAccessResult();
            result.Success = true;
            result.DataList = catalogSetMembers;

            SetupAnyComboBoxForDropDownList<CatalogSetMember>(catalogNameComboBox, result, ComboBoxListType.DataListWithNullItem, "CatalogName", "CatalogKey", catalogKeyTextBox, _reportCompareDetailToEdit.CatalogName.Trim());

            return success;
        }

        private bool SetChannelList()
        {
            bool success = true;
            ManufacturerReport report = manufacturerReportNameComboBox.SelectedItem as ManufacturerReport;

            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.ChannelSetMember);
            if (!result.Success)
            {
                success = false;
                return success;
            }

            HaiBindingList<ChannelSetMember> channelSetMembers = (HaiBindingList<ChannelSetMember>)result.DataList;
            List<ChannelSetMember> channelSetMembersRelated = channelSetMembers.Where<ChannelSetMember>(x => (x.ChannelSetKey == report.ChannelSetKey)).ToList<ChannelSetMember>();
            channelSetMembers.ClearList();
            foreach (ChannelSetMember c in channelSetMembersRelated)
            {
                channelSetMembers.AddToList(c);
            }

            result = new DataAccessResult();
            result.Success = true;
            result.DataList = channelSetMembers;

            SetupAnyComboBoxForDropDownList<ChannelSetMember>(channelNameComboBox, result, ComboBoxListType.DataListWithNullItem, "ChannelName", "ChannelKey", channelKeyTextBox, _reportCompareDetailToEdit.ChannelName.Trim());

            return success;
        }

        private bool SetMeasureList()
        {
            bool success = true;
            ManufacturerReport report = manufacturerReportNameComboBox.SelectedItem as ManufacturerReport;

            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.MeasureSetMember);
            if (!result.Success)
            {
                success = false;
                return success;
            }

            HaiBindingList<MeasureSetMember> measureSetMembers = (HaiBindingList<MeasureSetMember>)result.DataList;
            List<MeasureSetMember> measureSetMembersRelated = measureSetMembers.Where<MeasureSetMember>(x => (x.MeasureSetKey == report.MeasureSetKey)).ToList<MeasureSetMember>();
            measureSetMembers.ClearList();
            foreach (MeasureSetMember m in measureSetMembersRelated)
            {
                measureSetMembers.AddToList(m);
            }

            result = new DataAccessResult();
            result.Success = true;
            result.DataList = measureSetMembers;

            SetupAnyComboBoxForDropDownList<MeasureSetMember>(measureNameComboBox, result, ComboBoxListType.DataListWithNullItem, "MeasureName", "MeasureKey", measureKeyTextBox, _reportCompareDetailToEdit.MeasureName.Trim());

            return success;
        }

        private bool SetUseList()
        {
            bool success = true;
            ManufacturerReport report = manufacturerReportNameComboBox.SelectedItem as ManufacturerReport;

            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.UseSetMember);
            if (!result.Success)
            {
                success = false;
                return success;
            }

            HaiBindingList<UseSetMember> useSetMembers = (HaiBindingList<UseSetMember>)result.DataList;
            List<UseSetMember> useSetMembersRelated = useSetMembers.Where<UseSetMember>(x => (x.UseSetKey == report.UseSetKey)).ToList<UseSetMember>();
            useSetMembers.ClearList();
            foreach (UseSetMember u in useSetMembersRelated)
            {
                useSetMembers.AddToList(u);
            }

            result = new DataAccessResult();
            result.Success = true;
            result.DataList = useSetMembers;

            SetupAnyComboBoxForDropDownList<UseSetMember>(useNameComboBox, result, ComboBoxListType.DataListWithNullItem, "UseName", "UseKey", useKeyTextBox, _reportCompareDetailToEdit.UseName.Trim());

            return success;
        }

        private bool SetSizeListAll()
        {
            bool success = true;
            ManufacturerReport report = manufacturerReportNameComboBox.SelectedItem as ManufacturerReport;
            DataAccessResult result;

            result = AhamDataServiceSize.SizeLookupManufacturerReport(report.ManufacturerReportKey, 1, new DataServiceParameters());
            if (!result.Success)
            {
                success = false;
                return success;
            }
            else
                SetupAnyComboBoxForDropDownList<AhamMetaDataDAL.Size>(size1NameComboBox, result, ComboBoxListType.DataListWithNullItem, "SizeName", "SizeKey", size1KeyTextBox, _reportCompareDetailToEdit.Size1Name.Trim(), "Size1Name");


            result = AhamDataServiceSize.SizeLookupManufacturerReport(report.ManufacturerReportKey, 2, new DataServiceParameters());
            if (!result.Success)
            {
                success = false;
                return success;
            }
            else
                SetupAnyComboBoxForDropDownList<AhamMetaDataDAL.Size>(size2NameComboBox, result, ComboBoxListType.DataListWithNullItem, "SizeName", "SizeKey", size2KeyTextBox, _reportCompareDetailToEdit.Size2Name.Trim(), "Size2Name");

            result = AhamDataServiceSize.SizeLookupManufacturerReport(report.ManufacturerReportKey, 3, new DataServiceParameters());
            if (!result.Success)
            {
                success = false;
                return success;
            }
            else
                SetupAnyComboBoxForDropDownList<AhamMetaDataDAL.Size>(size3NameComboBox, result, ComboBoxListType.DataListWithNullItem, "SizeName", "SizeKey", size3KeyTextBox, _reportCompareDetailToEdit.Size3Name.Trim(), "Size3Name");

            result = AhamDataServiceSize.SizeLookupManufacturerReport(report.ManufacturerReportKey, 4, new DataServiceParameters());
            if (!result.Success)
            {
                success = false;
                return success;
            }
            else
                SetupAnyComboBoxForDropDownList<AhamMetaDataDAL.Size>(size4NameComboBox, result, ComboBoxListType.DataListWithNullItem, "SizeName", "SizeKey", size4KeyTextBox, _reportCompareDetailToEdit.Size4Name.Trim(), "Size4Name");

            result = AhamDataServiceSize.SizeLookupManufacturerReport(report.ManufacturerReportKey, 5, new DataServiceParameters());
            if (!result.Success)
            {
                success = false;
                return success;
            }
            else
                SetupAnyComboBoxForDropDownList<AhamMetaDataDAL.Size>(size5NameComboBox, result, ComboBoxListType.DataListWithNullItem, "SizeName", "SizeKey", size5KeyTextBox, _reportCompareDetailToEdit.Size5Name.Trim(), "Size5Name");

            return success;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort=false;

            if (manufacturerReportNameComboBox.Text.Trim() == string.Empty && manufacturerReportNameComboBox.Items.Count > 0)
            {
                MessageBox.Show("A manufacturer report must be selected.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_reportCompareDetailToEdit);
            _reportCompareDetailToEdit.MarkDirty();

            // inform the caller that the edit is good.
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void rbLeftSide_CheckedChanged(object sender, EventArgs e)
        {
            if (rbLeftSide.Checked)
                _reportCompareDetailToEdit.Side = 0;
            else
                _reportCompareDetailToEdit.Side = 1;
        }
    }
}
