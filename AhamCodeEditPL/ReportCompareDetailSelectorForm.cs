﻿using System.Windows.Forms;

using System.Collections;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ReportCompareDetailSelectorForm : HaiMetaDataEditor
    {
        EditFormParameters _parameters;
        ReportCompareMaster _reportCompareMaster;
        private List<HaiBusinessObjectBase> _unfilteredHBOList;

        private int _rightMargin = 30;
        private int _bottomMargin = 110;

        public ReportCompareDetailSelectorForm(EditFormParameters parameters)
        {
            InitializeComponent();
            _mainMenu.Visible = false;

            _parameters = parameters;
        }

        private void ReportCompareDetailSelectorForm_Load(object sender, System.EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;

            _reportCompareMaster = (ReportCompareMaster)_parameters.EditObject;
            int reportCompareMasterKey = _reportCompareMaster.ReportCompareMasterKey;
            DataAccessResult result = AhamDataServiceReportManagement.ReportCompareDetailForMasterListGet(reportCompareMasterKey, new DataServiceParameters());

            reportCompareDetailGrid.DataGridView.CellDoubleClick += new DataGridViewCellEventHandler(DataGridView_CellDoubleClick);
            reportCompareDetailGrid.GUS = _parameters.GloballyUsefulStuff;

            reportCompareDetailGrid.PrepareGridForDisplay(result, null);

            HaiBindingList<ReportCompareDetail> theBindingList = (HaiBindingList<ReportCompareDetail>)(((BindingSource)reportCompareDetailGrid.DataGridView.DataSource).List);        // get the data collection
            HaiReports.ReportParameters reportParameters = new HaiReports.ReportParameters();
            reportParameters.SetBasicProperties(HaiReports.ListType.unknown);
            theBindingList.ReportParameters = reportParameters;
            List<ReportCompareDetail> unfilteredRCDList = theBindingList.GetUnfilteredList();
            _unfilteredHBOList = new List<HaiBusinessObjectBase>();
            foreach (ReportCompareDetail r in unfilteredRCDList)
            {
                _unfilteredHBOList.Add(r);
            }

            this.Owner.Cursor = Cursors.Default;
        }

        void DataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!(e.RowIndex < 0))
            {
                reportCompareDetailGrid.DataGridView.Rows[e.RowIndex].Selected = true;
                EditTheSelectedItem();
            }
        }

        private void btnEdit_Click(object sender, System.EventArgs e)
        {
            EditTheSelectedItem();
        }

        // EditTheSelectedItem() -- performs function of superclass EditItemsInTheGrid() w/o using delegates
        void EditTheSelectedItem()
        {
            HaiBindingList<ReportCompareDetail> reportCompareDetailList = reportCompareDetailGrid.GetReadonlyBindingListOfSelectedItems<ReportCompareDetail>(true);
            if (reportCompareDetailList.Count != 1)
            {
                MessageBox.Show("A single item must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            EditFormParameters editFormParameters = CreateEditFormParametersForDirectEdit(reportCompareDetailList[0], _unfilteredHBOList, ReportCompareDetail.GetBrowsablePropertyList());

            ReportCompareDetailEditForm theForm = new ReportCompareDetailEditForm(editFormParameters, _reportCompareMaster);
            theForm.ShowDialog(this);

            if (theForm.DialogResult == DialogResult.Cancel)
            {
                editFormParameters.EditObject.CancelEdit();
            }
            else
            {
                Cursor = Cursors.WaitCursor;

                AhamDataService ds = new AhamDataService(new DataServiceParameters());
                DataAccessResult result = ds.Save(editFormParameters.EditObject);
                Cursor = Cursors.Default;

                if (result.Success)
                {
                    editFormParameters.EditObject.ApplyEdit();
                }
                else
                {
                    editFormParameters.EditObject.CancelEdit();
                    MessageBox.Show("Could not save changes: " + result.Message, "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            reportCompareDetailGrid.DataGridView.Refresh();

            theForm.Dispose();
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
            EditFormParameters editFormParameters = CreateEditFormParametersForNew<ReportCompareDetail>(_unfilteredHBOList);

            ReportCompareDetailEditForm theForm = new ReportCompareDetailEditForm(editFormParameters, _reportCompareMaster);
            theForm.ShowDialog(this);

            if (theForm.DialogResult == DialogResult.Cancel)
            {
                editFormParameters.EditObject.CancelEdit();
            }
            else
            {
                Cursor = Cursors.WaitCursor;

                AhamDataService ds = new AhamDataService(new DataServiceParameters());
                DataAccessResult result = ds.Save(editFormParameters.EditObject);
                Cursor = Cursors.Default;

                if (result.Success)
                {
                    editFormParameters.EditObject.ApplyEdit();
                    IUntypedBindingList bindingList = (IUntypedBindingList)((BindingSource)(reportCompareDetailGrid.DataGridView.DataSource)).List;
                    bindingList.AddToList(editFormParameters.EditObject);
                }
                else
                {
                    editFormParameters.EditObject.CancelEdit();
                    MessageBox.Show("Could not save changes: " + result.Message, "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            reportCompareDetailGrid.DataGridView.Refresh();

            theForm.Dispose();
        }

        private void btnExit_Click(object sender, System.EventArgs e)
        {
            // ReportCompareMaster should have at least 2 detail records
            if (_unfilteredHBOList.Count < 2)
            {
                DialogResult yesNo = MessageBox.Show("Less than two detail records.\n\r\nExit anyway?", "Data entry error", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (yesNo == DialogResult.Yes)
                {
                    this.DialogResult = DialogResult.Cancel;
                    this.Close();
                }
                else
                    return;
            }

             // ReportCompareMaster should have both a "left side" and a "right side" ReportCompareDetail record
            bool hasLeft = false;
            bool hasRight = false;

            foreach (HaiBusinessObjectBase hbo in _unfilteredHBOList)
            {
                ReportCompareDetail RCD = (ReportCompareDetail)hbo;
                if (RCD.Side == 0)
                    hasLeft = true;
                if (RCD.Side == 1)
                    hasRight = true;
            }

            if (!hasLeft)
            {
                DialogResult yesNo = MessageBox.Show("Missing \"left\" side.\n\r\nExit anyway?", "Data entry error", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (yesNo == DialogResult.No)
                    return;
            }

            if (!hasRight)
            {
                DialogResult yesNo = MessageBox.Show("Missing \"right\" side.\n\r\nExit anyway?", "Data entry error", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (yesNo == DialogResult.No)
                    return;
            }

            this.Close();
        }

        private void ReportCompareDetailSelectorForm_Resize(object sender, System.EventArgs e)
        {
            if (reportCompareDetailGrid!=null)
                reportCompareDetailGrid.Size = new System.Drawing.Size(this.Width - _rightMargin, this.Height - _bottomMargin);
        }

        private void btnDelete_Click(object sender, System.EventArgs e)
        {
            HaiBindingList<ReportCompareDetail> selectedObjects = reportCompareDetailGrid.GetReadonlyBindingListOfSelectedItems<ReportCompareDetail>(true);
            if (selectedObjects.Count == 0)           // nothing selected
            {
                MessageBox.Show("Nothing selected.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                DialogResult yesNo = MessageBox.Show(selectedObjects.Count.ToString() + " item(s) will be deleted. \n\r Are you sure?", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (yesNo == DialogResult.No)
                    return;
            }

            AhamDataService ds = new AhamDataService(new DataServiceParameters());
            IUntypedBindingList datalist = (IUntypedBindingList)((BindingSource)(reportCompareDetailGrid.DataGridView.DataSource)).DataSource;
            foreach (HaiBusinessObjectBase haiObject in selectedObjects)
            {
                haiObject.MarkDeleted();
                DataAccessResult result = ds.Save(haiObject);
                if (result.Success)
                    datalist.RemoveFromList(haiObject);
                else
                {
                    MessageBox.Show("Delete failed. -- " + result.Message);
                    haiObject.MarkClean();
                }
            }

            reportCompareDetailGrid.DataGridView.Refresh();
        }

        private void btnPrint_Click(object sender, System.EventArgs e)
        {
            reportCompareDetailGrid.PrintGridReport();
        }
    }
}
