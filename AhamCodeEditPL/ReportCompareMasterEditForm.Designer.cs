﻿namespace AhamMetaDataPL
{
    partial class ReportCompareMasterEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label errorMessageLabel;
            System.Windows.Forms.Label manufacturerKeyLabel;
            System.Windows.Forms.Label manufacturerNameLabel;
            System.Windows.Forms.Label relationshipLabel;
            System.Windows.Forms.Label report1NameLabel;
            System.Windows.Forms.Label report2NameLabel;
            System.Windows.Forms.Label report3NameLabel;
            System.Windows.Forms.Label report4NameLabel;
            System.Windows.Forms.Label report5NameLabel;
            System.Windows.Forms.Label reportCompareMasterKeyLabel;
            System.Windows.Forms.Label label1;
            this.errorMessageTextBox = new System.Windows.Forms.TextBox();
            this.reportCompareMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.isErrorCheckBox = new System.Windows.Forms.CheckBox();
            this.manufacturerKeyTextBox = new System.Windows.Forms.TextBox();
            this.manufacturerNameComboBox = new System.Windows.Forms.ComboBox();
            this.reconcileCheckBox = new System.Windows.Forms.CheckBox();
            this.relationshipTextBox = new System.Windows.Forms.TextBox();
            this.report1KeyTextBox = new System.Windows.Forms.TextBox();
            this.report1NameComboBox = new System.Windows.Forms.ComboBox();
            this.report2KeyTextBox = new System.Windows.Forms.TextBox();
            this.report2NameComboBox = new System.Windows.Forms.ComboBox();
            this.report3KeyTextBox = new System.Windows.Forms.TextBox();
            this.report3NameComboBox = new System.Windows.Forms.ComboBox();
            this.report4KeyTextBox = new System.Windows.Forms.TextBox();
            this.report4NameComboBox = new System.Windows.Forms.ComboBox();
            this.report5KeyTextBox = new System.Windows.Forms.TextBox();
            this.report5NameComboBox = new System.Windows.Forms.ComboBox();
            this.reportCompareMasterKeyTextBox = new System.Windows.Forms.TextBox();
            this.yTDCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbGreaterThanEqual = new System.Windows.Forms.RadioButton();
            this.rbLessThanEqual = new System.Windows.Forms.RadioButton();
            this.rbGreaterThan = new System.Windows.Forms.RadioButton();
            this.rbLessThan = new System.Windows.Forms.RadioButton();
            this.rbEqual = new System.Windows.Forms.RadioButton();
            this.btnCreateMessage = new System.Windows.Forms.Button();
            this.dateTime = new System.Windows.Forms.DateTimePicker();
            errorMessageLabel = new System.Windows.Forms.Label();
            manufacturerKeyLabel = new System.Windows.Forms.Label();
            manufacturerNameLabel = new System.Windows.Forms.Label();
            relationshipLabel = new System.Windows.Forms.Label();
            report1NameLabel = new System.Windows.Forms.Label();
            report2NameLabel = new System.Windows.Forms.Label();
            report3NameLabel = new System.Windows.Forms.Label();
            report4NameLabel = new System.Windows.Forms.Label();
            report5NameLabel = new System.Windows.Forms.Label();
            reportCompareMasterKeyLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.reportCompareMasterBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(538, 487);
            this.btnOK.TabIndex = 15;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(619, 487);
            this.btnCancel.TabIndex = 16;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 487);
            this.btnInsertNull.TabIndex = 14;
            // 
            // errorMessageLabel
            // 
            errorMessageLabel.AutoSize = true;
            errorMessageLabel.Location = new System.Drawing.Point(8, 356);
            errorMessageLabel.Name = "errorMessageLabel";
            errorMessageLabel.Size = new System.Drawing.Size(78, 13);
            errorMessageLabel.TabIndex = 30;
            errorMessageLabel.Text = "Error Message:";
            // 
            // manufacturerKeyLabel
            // 
            manufacturerKeyLabel.AutoSize = true;
            manufacturerKeyLabel.Location = new System.Drawing.Point(654, 36);
            manufacturerKeyLabel.Name = "manufacturerKeyLabel";
            manufacturerKeyLabel.Size = new System.Drawing.Size(25, 13);
            manufacturerKeyLabel.TabIndex = 34;
            manufacturerKeyLabel.Text = "Key";
            // 
            // manufacturerNameLabel
            // 
            manufacturerNameLabel.AutoSize = true;
            manufacturerNameLabel.Location = new System.Drawing.Point(8, 55);
            manufacturerNameLabel.Name = "manufacturerNameLabel";
            manufacturerNameLabel.Size = new System.Drawing.Size(73, 13);
            manufacturerNameLabel.TabIndex = 36;
            manufacturerNameLabel.Text = "Manufacturer:";
            // 
            // relationshipLabel
            // 
            relationshipLabel.AutoSize = true;
            relationshipLabel.Location = new System.Drawing.Point(8, 294);
            relationshipLabel.Name = "relationshipLabel";
            relationshipLabel.Size = new System.Drawing.Size(68, 13);
            relationshipLabel.TabIndex = 40;
            relationshipLabel.Text = "Relationship:";
            // 
            // report1NameLabel
            // 
            report1NameLabel.AutoSize = true;
            report1NameLabel.Location = new System.Drawing.Point(8, 91);
            report1NameLabel.Name = "report1NameLabel";
            report1NameLabel.Size = new System.Drawing.Size(76, 13);
            report1NameLabel.TabIndex = 44;
            report1NameLabel.Text = "Report1Name:";
            // 
            // report2NameLabel
            // 
            report2NameLabel.AutoSize = true;
            report2NameLabel.Location = new System.Drawing.Point(8, 128);
            report2NameLabel.Name = "report2NameLabel";
            report2NameLabel.Size = new System.Drawing.Size(76, 13);
            report2NameLabel.TabIndex = 48;
            report2NameLabel.Text = "Report2Name:";
            // 
            // report3NameLabel
            // 
            report3NameLabel.AutoSize = true;
            report3NameLabel.Location = new System.Drawing.Point(8, 167);
            report3NameLabel.Name = "report3NameLabel";
            report3NameLabel.Size = new System.Drawing.Size(76, 13);
            report3NameLabel.TabIndex = 52;
            report3NameLabel.Text = "Report3Name:";
            // 
            // report4NameLabel
            // 
            report4NameLabel.AutoSize = true;
            report4NameLabel.Location = new System.Drawing.Point(8, 204);
            report4NameLabel.Name = "report4NameLabel";
            report4NameLabel.Size = new System.Drawing.Size(76, 13);
            report4NameLabel.TabIndex = 56;
            report4NameLabel.Text = "Report4Name:";
            // 
            // report5NameLabel
            // 
            report5NameLabel.AutoSize = true;
            report5NameLabel.Location = new System.Drawing.Point(8, 242);
            report5NameLabel.Name = "report5NameLabel";
            report5NameLabel.Size = new System.Drawing.Size(76, 13);
            report5NameLabel.TabIndex = 60;
            report5NameLabel.Text = "Report5Name:";
            // 
            // reportCompareMasterKeyLabel
            // 
            reportCompareMasterKeyLabel.AutoSize = true;
            reportCompareMasterKeyLabel.Location = new System.Drawing.Point(502, 449);
            reportCompareMasterKeyLabel.Name = "reportCompareMasterKeyLabel";
            reportCompareMasterKeyLabel.Size = new System.Drawing.Size(143, 13);
            reportCompareMasterKeyLabel.TabIndex = 62;
            reportCompareMasterKeyLabel.Text = "Report Compare Master Key:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(8, 16);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(55, 13);
            label1.TabIndex = 69;
            label1.Text = "Date filter:";
            // 
            // errorMessageTextBox
            // 
            this.errorMessageTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareMasterBindingSource, "ErrorMessage", true));
            this.errorMessageTextBox.Location = new System.Drawing.Point(87, 353);
            this.errorMessageTextBox.Multiline = true;
            this.errorMessageTextBox.Name = "errorMessageTextBox";
            this.errorMessageTextBox.Size = new System.Drawing.Size(505, 57);
            this.errorMessageTextBox.TabIndex = 10;
            // 
            // reportCompareMasterBindingSource
            // 
            this.reportCompareMasterBindingSource.DataSource = typeof(AhamMetaDataDAL.ReportCompareMaster);
            // 
            // isErrorCheckBox
            // 
            this.isErrorCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.reportCompareMasterBindingSource, "IsError", true));
            this.isErrorCheckBox.Location = new System.Drawing.Point(87, 323);
            this.isErrorCheckBox.Name = "isErrorCheckBox";
            this.isErrorCheckBox.Size = new System.Drawing.Size(121, 24);
            this.isErrorCheckBox.TabIndex = 9;
            this.isErrorCheckBox.Text = "Is error";
            this.isErrorCheckBox.UseVisualStyleBackColor = true;
            // 
            // manufacturerKeyTextBox
            // 
            this.manufacturerKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareMasterBindingSource, "ManufacturerKey", true));
            this.manufacturerKeyTextBox.Location = new System.Drawing.Point(651, 53);
            this.manufacturerKeyTextBox.Name = "manufacturerKeyTextBox";
            this.manufacturerKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.manufacturerKeyTextBox.TabIndex = 35;
            this.manufacturerKeyTextBox.TabStop = false;
            // 
            // manufacturerNameComboBox
            // 
            this.manufacturerNameComboBox.FormattingEnabled = true;
            this.manufacturerNameComboBox.Location = new System.Drawing.Point(87, 52);
            this.manufacturerNameComboBox.Name = "manufacturerNameComboBox";
            this.manufacturerNameComboBox.Size = new System.Drawing.Size(558, 21);
            this.manufacturerNameComboBox.TabIndex = 1;
            // 
            // reconcileCheckBox
            // 
            this.reconcileCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.reportCompareMasterBindingSource, "Reconcile", true));
            this.reconcileCheckBox.Location = new System.Drawing.Point(87, 416);
            this.reconcileCheckBox.Name = "reconcileCheckBox";
            this.reconcileCheckBox.Size = new System.Drawing.Size(81, 24);
            this.reconcileCheckBox.TabIndex = 12;
            this.reconcileCheckBox.Text = "Reconcile";
            this.reconcileCheckBox.UseVisualStyleBackColor = true;
            // 
            // relationshipTextBox
            // 
            this.relationshipTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareMasterBindingSource, "Relationship", true));
            this.relationshipTextBox.Location = new System.Drawing.Point(87, 291);
            this.relationshipTextBox.Name = "relationshipTextBox";
            this.relationshipTextBox.Size = new System.Drawing.Size(44, 20);
            this.relationshipTextBox.TabIndex = 7;
            this.relationshipTextBox.TabStop = false;
            // 
            // report1KeyTextBox
            // 
            this.report1KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareMasterBindingSource, "Report1Key", true));
            this.report1KeyTextBox.Location = new System.Drawing.Point(651, 89);
            this.report1KeyTextBox.Name = "report1KeyTextBox";
            this.report1KeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.report1KeyTextBox.TabIndex = 43;
            this.report1KeyTextBox.TabStop = false;
            // 
            // report1NameComboBox
            // 
            this.report1NameComboBox.FormattingEnabled = true;
            this.report1NameComboBox.Location = new System.Drawing.Point(87, 88);
            this.report1NameComboBox.Name = "report1NameComboBox";
            this.report1NameComboBox.Size = new System.Drawing.Size(558, 21);
            this.report1NameComboBox.TabIndex = 2;
            // 
            // report2KeyTextBox
            // 
            this.report2KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareMasterBindingSource, "Report2Key", true));
            this.report2KeyTextBox.Location = new System.Drawing.Point(651, 126);
            this.report2KeyTextBox.Name = "report2KeyTextBox";
            this.report2KeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.report2KeyTextBox.TabIndex = 47;
            this.report2KeyTextBox.TabStop = false;
            // 
            // report2NameComboBox
            // 
            this.report2NameComboBox.FormattingEnabled = true;
            this.report2NameComboBox.Location = new System.Drawing.Point(87, 125);
            this.report2NameComboBox.Name = "report2NameComboBox";
            this.report2NameComboBox.Size = new System.Drawing.Size(558, 21);
            this.report2NameComboBox.TabIndex = 3;
            // 
            // report3KeyTextBox
            // 
            this.report3KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareMasterBindingSource, "Report3Key", true));
            this.report3KeyTextBox.Location = new System.Drawing.Point(651, 165);
            this.report3KeyTextBox.Name = "report3KeyTextBox";
            this.report3KeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.report3KeyTextBox.TabIndex = 51;
            this.report3KeyTextBox.TabStop = false;
            // 
            // report3NameComboBox
            // 
            this.report3NameComboBox.FormattingEnabled = true;
            this.report3NameComboBox.Location = new System.Drawing.Point(87, 164);
            this.report3NameComboBox.Name = "report3NameComboBox";
            this.report3NameComboBox.Size = new System.Drawing.Size(558, 21);
            this.report3NameComboBox.TabIndex = 4;
            // 
            // report4KeyTextBox
            // 
            this.report4KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareMasterBindingSource, "Report4Key", true));
            this.report4KeyTextBox.Location = new System.Drawing.Point(651, 202);
            this.report4KeyTextBox.Name = "report4KeyTextBox";
            this.report4KeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.report4KeyTextBox.TabIndex = 55;
            this.report4KeyTextBox.TabStop = false;
            // 
            // report4NameComboBox
            // 
            this.report4NameComboBox.FormattingEnabled = true;
            this.report4NameComboBox.Location = new System.Drawing.Point(87, 201);
            this.report4NameComboBox.Name = "report4NameComboBox";
            this.report4NameComboBox.Size = new System.Drawing.Size(558, 21);
            this.report4NameComboBox.TabIndex = 5;
            // 
            // report5KeyTextBox
            // 
            this.report5KeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareMasterBindingSource, "Report5Key", true));
            this.report5KeyTextBox.Location = new System.Drawing.Point(651, 240);
            this.report5KeyTextBox.Name = "report5KeyTextBox";
            this.report5KeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.report5KeyTextBox.TabIndex = 59;
            this.report5KeyTextBox.TabStop = false;
            // 
            // report5NameComboBox
            // 
            this.report5NameComboBox.FormattingEnabled = true;
            this.report5NameComboBox.Location = new System.Drawing.Point(87, 239);
            this.report5NameComboBox.Name = "report5NameComboBox";
            this.report5NameComboBox.Size = new System.Drawing.Size(558, 21);
            this.report5NameComboBox.TabIndex = 6;
            // 
            // reportCompareMasterKeyTextBox
            // 
            this.reportCompareMasterKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportCompareMasterBindingSource, "ReportCompareMasterKey", true));
            this.reportCompareMasterKeyTextBox.Location = new System.Drawing.Point(651, 446);
            this.reportCompareMasterKeyTextBox.Name = "reportCompareMasterKeyTextBox";
            this.reportCompareMasterKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.reportCompareMasterKeyTextBox.TabIndex = 63;
            this.reportCompareMasterKeyTextBox.TabStop = false;
            // 
            // yTDCheckBox
            // 
            this.yTDCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.reportCompareMasterBindingSource, "YTD", true));
            this.yTDCheckBox.Location = new System.Drawing.Point(174, 416);
            this.yTDCheckBox.Name = "yTDCheckBox";
            this.yTDCheckBox.Size = new System.Drawing.Size(92, 24);
            this.yTDCheckBox.TabIndex = 13;
            this.yTDCheckBox.Text = "Year to date";
            this.yTDCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbGreaterThanEqual);
            this.groupBox1.Controls.Add(this.rbLessThanEqual);
            this.groupBox1.Controls.Add(this.rbGreaterThan);
            this.groupBox1.Controls.Add(this.rbLessThan);
            this.groupBox1.Controls.Add(this.rbEqual);
            this.groupBox1.Location = new System.Drawing.Point(147, 281);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(497, 36);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // rbGreaterThanEqual
            // 
            this.rbGreaterThanEqual.AutoSize = true;
            this.rbGreaterThanEqual.Location = new System.Drawing.Point(365, 12);
            this.rbGreaterThanEqual.Name = "rbGreaterThanEqual";
            this.rbGreaterThanEqual.Size = new System.Drawing.Size(125, 17);
            this.rbGreaterThanEqual.TabIndex = 4;
            this.rbGreaterThanEqual.TabStop = true;
            this.rbGreaterThanEqual.Text = "Greater than or equal";
            this.rbGreaterThanEqual.UseVisualStyleBackColor = true;
            this.rbGreaterThanEqual.CheckedChanged += new System.EventHandler(this.rbGreaterThanEqual_CheckedChanged);
            // 
            // rbLessThanEqual
            // 
            this.rbLessThanEqual.AutoSize = true;
            this.rbLessThanEqual.Location = new System.Drawing.Point(243, 12);
            this.rbLessThanEqual.Name = "rbLessThanEqual";
            this.rbLessThanEqual.Size = new System.Drawing.Size(112, 17);
            this.rbLessThanEqual.TabIndex = 3;
            this.rbLessThanEqual.TabStop = true;
            this.rbLessThanEqual.Text = "Less than or equal";
            this.rbLessThanEqual.UseVisualStyleBackColor = true;
            this.rbLessThanEqual.CheckedChanged += new System.EventHandler(this.rbLessThanEqual_CheckedChanged);
            // 
            // rbGreaterThan
            // 
            this.rbGreaterThan.AutoSize = true;
            this.rbGreaterThan.Location = new System.Drawing.Point(151, 12);
            this.rbGreaterThan.Name = "rbGreaterThan";
            this.rbGreaterThan.Size = new System.Drawing.Size(84, 17);
            this.rbGreaterThan.TabIndex = 2;
            this.rbGreaterThan.TabStop = true;
            this.rbGreaterThan.Text = "Greater than";
            this.rbGreaterThan.UseVisualStyleBackColor = true;
            this.rbGreaterThan.CheckedChanged += new System.EventHandler(this.rbGreaterThan_CheckedChanged);
            // 
            // rbLessThan
            // 
            this.rbLessThan.AutoSize = true;
            this.rbLessThan.Location = new System.Drawing.Point(70, 12);
            this.rbLessThan.Name = "rbLessThan";
            this.rbLessThan.Size = new System.Drawing.Size(71, 17);
            this.rbLessThan.TabIndex = 1;
            this.rbLessThan.TabStop = true;
            this.rbLessThan.Text = "Less than";
            this.rbLessThan.UseVisualStyleBackColor = true;
            this.rbLessThan.CheckedChanged += new System.EventHandler(this.rbLessThan_CheckedChanged);
            // 
            // rbEqual
            // 
            this.rbEqual.AutoSize = true;
            this.rbEqual.Location = new System.Drawing.Point(9, 12);
            this.rbEqual.Name = "rbEqual";
            this.rbEqual.Size = new System.Drawing.Size(52, 17);
            this.rbEqual.TabIndex = 0;
            this.rbEqual.TabStop = true;
            this.rbEqual.Text = "Equal";
            this.rbEqual.UseVisualStyleBackColor = true;
            this.rbEqual.CheckedChanged += new System.EventHandler(this.rbEqual_CheckedChanged);
            // 
            // btnCreateMessage
            // 
            this.btnCreateMessage.Location = new System.Drawing.Point(598, 356);
            this.btnCreateMessage.Name = "btnCreateMessage";
            this.btnCreateMessage.Size = new System.Drawing.Size(98, 23);
            this.btnCreateMessage.TabIndex = 11;
            this.btnCreateMessage.Text = "Create message";
            this.btnCreateMessage.UseVisualStyleBackColor = true;
            this.btnCreateMessage.Click += new System.EventHandler(this.btnCreateMessage_Click);
            // 
            // dateTime
            // 
            this.dateTime.CustomFormat = "";
            this.dateTime.Location = new System.Drawing.Point(87, 12);
            this.dateTime.Name = "dateTime";
            this.dateTime.Size = new System.Drawing.Size(200, 20);
            this.dateTime.TabIndex = 0;
            // 
            // ReportCompareMasterEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 522);
            this.Controls.Add(label1);
            this.Controls.Add(this.dateTime);
            this.Controls.Add(this.btnCreateMessage);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(errorMessageLabel);
            this.Controls.Add(this.errorMessageTextBox);
            this.Controls.Add(this.isErrorCheckBox);
            this.Controls.Add(manufacturerKeyLabel);
            this.Controls.Add(this.manufacturerKeyTextBox);
            this.Controls.Add(manufacturerNameLabel);
            this.Controls.Add(this.manufacturerNameComboBox);
            this.Controls.Add(this.reconcileCheckBox);
            this.Controls.Add(relationshipLabel);
            this.Controls.Add(this.relationshipTextBox);
            this.Controls.Add(this.report1KeyTextBox);
            this.Controls.Add(report1NameLabel);
            this.Controls.Add(this.report1NameComboBox);
            this.Controls.Add(this.report2KeyTextBox);
            this.Controls.Add(report2NameLabel);
            this.Controls.Add(this.report2NameComboBox);
            this.Controls.Add(this.report3KeyTextBox);
            this.Controls.Add(report3NameLabel);
            this.Controls.Add(this.report3NameComboBox);
            this.Controls.Add(this.report4KeyTextBox);
            this.Controls.Add(report4NameLabel);
            this.Controls.Add(this.report4NameComboBox);
            this.Controls.Add(this.report5KeyTextBox);
            this.Controls.Add(report5NameLabel);
            this.Controls.Add(this.report5NameComboBox);
            this.Controls.Add(reportCompareMasterKeyLabel);
            this.Controls.Add(this.reportCompareMasterKeyTextBox);
            this.Controls.Add(this.yTDCheckBox);
            this.Name = "ReportCompareMasterEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Report Compare Master";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.yTDCheckBox, 0);
            this.Controls.SetChildIndex(this.reportCompareMasterKeyTextBox, 0);
            this.Controls.SetChildIndex(reportCompareMasterKeyLabel, 0);
            this.Controls.SetChildIndex(this.report5NameComboBox, 0);
            this.Controls.SetChildIndex(report5NameLabel, 0);
            this.Controls.SetChildIndex(this.report5KeyTextBox, 0);
            this.Controls.SetChildIndex(this.report4NameComboBox, 0);
            this.Controls.SetChildIndex(report4NameLabel, 0);
            this.Controls.SetChildIndex(this.report4KeyTextBox, 0);
            this.Controls.SetChildIndex(this.report3NameComboBox, 0);
            this.Controls.SetChildIndex(report3NameLabel, 0);
            this.Controls.SetChildIndex(this.report3KeyTextBox, 0);
            this.Controls.SetChildIndex(this.report2NameComboBox, 0);
            this.Controls.SetChildIndex(report2NameLabel, 0);
            this.Controls.SetChildIndex(this.report2KeyTextBox, 0);
            this.Controls.SetChildIndex(this.report1NameComboBox, 0);
            this.Controls.SetChildIndex(report1NameLabel, 0);
            this.Controls.SetChildIndex(this.report1KeyTextBox, 0);
            this.Controls.SetChildIndex(this.relationshipTextBox, 0);
            this.Controls.SetChildIndex(relationshipLabel, 0);
            this.Controls.SetChildIndex(this.reconcileCheckBox, 0);
            this.Controls.SetChildIndex(this.manufacturerNameComboBox, 0);
            this.Controls.SetChildIndex(manufacturerNameLabel, 0);
            this.Controls.SetChildIndex(this.manufacturerKeyTextBox, 0);
            this.Controls.SetChildIndex(manufacturerKeyLabel, 0);
            this.Controls.SetChildIndex(this.isErrorCheckBox, 0);
            this.Controls.SetChildIndex(this.errorMessageTextBox, 0);
            this.Controls.SetChildIndex(errorMessageLabel, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.btnCreateMessage, 0);
            this.Controls.SetChildIndex(this.dateTime, 0);
            this.Controls.SetChildIndex(label1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.reportCompareMasterBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource reportCompareMasterBindingSource;
        private System.Windows.Forms.TextBox errorMessageTextBox;
        private System.Windows.Forms.CheckBox isErrorCheckBox;
        private System.Windows.Forms.TextBox manufacturerKeyTextBox;
        private System.Windows.Forms.ComboBox manufacturerNameComboBox;
        private System.Windows.Forms.CheckBox reconcileCheckBox;
        private System.Windows.Forms.TextBox relationshipTextBox;
        private System.Windows.Forms.TextBox report1KeyTextBox;
        private System.Windows.Forms.ComboBox report1NameComboBox;
        private System.Windows.Forms.TextBox report2KeyTextBox;
        private System.Windows.Forms.ComboBox report2NameComboBox;
        private System.Windows.Forms.TextBox report3KeyTextBox;
        private System.Windows.Forms.ComboBox report3NameComboBox;
        private System.Windows.Forms.TextBox report4KeyTextBox;
        private System.Windows.Forms.ComboBox report4NameComboBox;
        private System.Windows.Forms.TextBox report5KeyTextBox;
        private System.Windows.Forms.ComboBox report5NameComboBox;
        private System.Windows.Forms.TextBox reportCompareMasterKeyTextBox;
        private System.Windows.Forms.CheckBox yTDCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbEqual;
        private System.Windows.Forms.RadioButton rbGreaterThanEqual;
        private System.Windows.Forms.RadioButton rbLessThanEqual;
        private System.Windows.Forms.RadioButton rbGreaterThan;
        private System.Windows.Forms.RadioButton rbLessThan;
        private System.Windows.Forms.Button btnCreateMessage;
        private System.Windows.Forms.DateTimePicker dateTime;
    }
}