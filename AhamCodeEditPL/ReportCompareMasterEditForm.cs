﻿using System;
using System.Windows.Forms;

using System.Collections;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class ReportCompareMasterEditForm : HaiObjectEditForm
    {
        private ReportCompareMaster _reportCompareMasterToEdit;
        private HaiBindingList<ManufacturerReport> _manufacturerReportList;
        private bool _formIsLoading = false;

        public ReportCompareMasterEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _reportCompareMasterToEdit = (ReportCompareMaster)_parameters.EditObject;

            this.Load += new EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        void EditForm_Load(object sender, EventArgs e)
        {
            _formIsLoading = true;
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);          // the form needs to be prepared before assigning values to controls
            reportCompareMasterBindingSource.DataSource = _reportCompareMasterToEdit;

            dateTime.MinDate = Utilities.GetEarliestBeginDate();
            dateTime.MaxDate = Utilities.GetLatestEndDate();
            dateTime.Format = DateTimePickerFormat.Custom;
            dateTime.CustomFormat = "MMM/d/yyyy";

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            if (_parameters.NumberOfItemsToEdit < 1)        // this is an ADD (no duplicate)
            {
                result = ds.GetDataList(HaiBusinessObjectType.Manufacturer);
                if (result.Success)
                {
                    SetupAnyComboBoxForDropDownList<Manufacturer>(manufacturerNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ManufacturerName", "ManufacturerKey", manufacturerKeyTextBox, _reportCompareMasterToEdit.ManufacturerName.Trim());
                }
                else
                {
                    MessageBox.Show("Cannot add/edit report compare master.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                    return;
                }

                result = ds.GetDataList(HaiBusinessObjectType.ManufacturerReport);
                if (result.Success)
                {
                    _manufacturerReportList = (HaiBindingList<ManufacturerReport>)result.DataList;

                    SetReportNameComboBoxes();
                }
                else
                {
                    MessageBox.Show("Cannot add/edit report compare master.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                    return;
                }

                _reportCompareMasterToEdit.Relationship = "=";
            }
            else                                            // this is an EDIT
            {
                dateTime.Enabled = false;
                manufacturerNameComboBox.Enabled = false;
                report1NameComboBox.Enabled = false;
                report2NameComboBox.Enabled = false;
                report3NameComboBox.Enabled = false;
                report4NameComboBox.Enabled = false;
                report5NameComboBox.Enabled = false;
            }

            switch (_reportCompareMasterToEdit.Relationship)
            {
                case "=":
                    rbEqual.Checked = true;
                    break;
                case "<":
                    rbLessThan.Checked = true;
                    break;
                case ">":
                    rbGreaterThan.Checked = true;
                    break;
                case "<=":
                    rbLessThanEqual.Checked = true;
                    break;
                case ">=":
                    rbGreaterThanEqual.Checked = true;
                    break;
                default:
                    break;
            }

            manufacturerNameComboBox.Leave += new EventHandler(manufacturerNameComboBox_TextChanged);

            //reportCompareMasterBindingSource.DataSource = _reportCompareMasterToEdit;
            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
            _formIsLoading = false;
        }

        void dateTime_ValueChanged(object sender, EventArgs e)
        {
            if (!_formIsLoading)
            {
                SetReportNameComboBoxes();
            }
        }

        void manufacturerNameComboBox_TextChanged(object sender, EventArgs e)
        {
            if (!_formIsLoading)
            {
                reinitializeTheEditObject();
            }

            SetReportNameComboBoxes();
        }

        private void SetReportNameComboBoxes()
        {
            // get reports for the selected manufacturer
            var mfrRptList = _manufacturerReportList.Where<ManufacturerReport>(x => (x.ManufacturerKey == _reportCompareMasterToEdit.ManufacturerKey));

            // create a list of reports that is limited to those with the filter date in the daterange
            DateTime filterDate = dateTime.Value.Date;

            HaiBindingList<ManufacturerReport> reportsForManufacturer;
            DataAccessResult result;
            reportsForManufacturer = new HaiBindingList<ManufacturerReport>();
            foreach (ManufacturerReport manufacturerReport in mfrRptList)
            {
                if (manufacturerReport.DateRange.RangeIsCurrent(filterDate))
                    reportsForManufacturer.AddToList(manufacturerReport);
            }

            result = new DataAccessResult();
            result.DataList = reportsForManufacturer;
            result.Success = true;
            result.BrowsablePropertyList = ManufacturerReport.GetBrowsablePropertyList();
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<ManufacturerReport>(report1NameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ReportName", "ManufacturerReportKey", report1KeyTextBox, _reportCompareMasterToEdit.Report1Name.Trim(), "Report1Name");
            }
            else
            {
                MessageBox.Show("Cannot add/edit report compare master.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }


            reportsForManufacturer = new HaiBindingList<ManufacturerReport>();
            foreach (ManufacturerReport manufacturerReport in mfrRptList)
            {
                if (manufacturerReport.DateRange.RangeIsCurrent(filterDate))
                    reportsForManufacturer.AddToList(manufacturerReport);
            }

            result = new DataAccessResult();
            result.DataList = reportsForManufacturer;
            result.Success = true;
            result.BrowsablePropertyList = ManufacturerReport.GetBrowsablePropertyList();
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<ManufacturerReport>(report2NameComboBox, result, ComboBoxListType.DataListWithBlankItem, "ReportName", "ManufacturerReportKey", report2KeyTextBox, _reportCompareMasterToEdit.Report2Name.Trim(), "Report2Name");
            }
            else
            {
                MessageBox.Show("Cannot add/edit report compare master.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }


            reportsForManufacturer = new HaiBindingList<ManufacturerReport>();
            foreach (ManufacturerReport manufacturerReport in mfrRptList)
            {
                if (manufacturerReport.DateRange.RangeIsCurrent(filterDate))
                    reportsForManufacturer.AddToList(manufacturerReport);
            }

            result = new DataAccessResult();
            result.DataList = reportsForManufacturer;
            result.Success = true;
            result.BrowsablePropertyList = ManufacturerReport.GetBrowsablePropertyList();
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<ManufacturerReport>(report3NameComboBox, result, ComboBoxListType.DataListWithNullItem, "ReportName", "ManufacturerReportKey", report3KeyTextBox, _reportCompareMasterToEdit.Report3Name.Trim(), "Report3Name");
            }
            else
            {
                MessageBox.Show("Cannot add/edit report compare master.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }


            reportsForManufacturer = new HaiBindingList<ManufacturerReport>();
            foreach (ManufacturerReport manufacturerReport in mfrRptList)
            {
                if (manufacturerReport.DateRange.RangeIsCurrent(filterDate))
                    reportsForManufacturer.AddToList(manufacturerReport);
            }

            result = new DataAccessResult();
            result.DataList = reportsForManufacturer;
            result.Success = true;
            result.BrowsablePropertyList = ManufacturerReport.GetBrowsablePropertyList();
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<ManufacturerReport>(report4NameComboBox, result, ComboBoxListType.DataListWithNullItem, "ReportName", "ManufacturerReportKey", report4KeyTextBox, _reportCompareMasterToEdit.Report4Name.Trim(), "Report4Name");
            }
            else
            {
                MessageBox.Show("Cannot add/edit report compare master.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }


            reportsForManufacturer = new HaiBindingList<ManufacturerReport>();
            foreach (ManufacturerReport manufacturerReport in mfrRptList)
            {
                if (manufacturerReport.DateRange.RangeIsCurrent(filterDate))
                    reportsForManufacturer.AddToList(manufacturerReport);
            }

            result = new DataAccessResult();
            result.DataList = reportsForManufacturer;
            result.Success = true;
            result.BrowsablePropertyList = ManufacturerReport.GetBrowsablePropertyList();
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<ManufacturerReport>(report5NameComboBox, result, ComboBoxListType.DataListWithNullItem, "ReportName", "ManufacturerReportKey", report5KeyTextBox, _reportCompareMasterToEdit.Report5Name.Trim(), "Report5Name");
            }
            else
            {
                MessageBox.Show("Cannot add/edit report compare master.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }
        }

        private void rbEqual_CheckedChanged(object sender, EventArgs e)
        {
            _reportCompareMasterToEdit.Relationship = "=";
            relationshipTextBox.DataBindings["Text"].ReadValue();
        }

        private void rbLessThan_CheckedChanged(object sender, EventArgs e)
        {
            _reportCompareMasterToEdit.Relationship = "<";
            relationshipTextBox.DataBindings["Text"].ReadValue();
        }

        private void rbGreaterThan_CheckedChanged(object sender, EventArgs e)
        {
            _reportCompareMasterToEdit.Relationship = ">";
            relationshipTextBox.DataBindings["Text"].ReadValue();
        }

        private void rbLessThanEqual_CheckedChanged(object sender, EventArgs e)
        {
            _reportCompareMasterToEdit.Relationship = "<=";
            relationshipTextBox.DataBindings["Text"].ReadValue();

        }

        private void rbGreaterThanEqual_CheckedChanged(object sender, EventArgs e)
        {
            _reportCompareMasterToEdit.Relationship = ">=";
            relationshipTextBox.DataBindings["Text"].ReadValue();
        }

        private void btnCreateMessage_Click(object sender, EventArgs e)
        {
            if ((report1NameComboBox.Text.Trim() == string.Empty) || (report2NameComboBox.Text.Trim() == string.Empty))
            {
                MessageBox.Show("Report 1 and Report 2 must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string text = "The totals on " + report1NameComboBox.Text.Trim() + " are not "
                + relationshipTextBox.Text.Trim() + " the totals on the report "
                + report2NameComboBox.Text.Trim() + ".";

            errorMessageTextBox.Text = text;
            errorMessageTextBox.DataBindings["Text"].WriteValue();
        }

        private void reinitializeTheEditObject()
        {
            int manufacturerKey = _reportCompareMasterToEdit.ManufacturerKey;
            ReportCompareMaster newRCM = new ReportCompareMaster();

            foreach (string propertyName in _parameters.BrowsablePropertyList.Keys)
            {
                object value = Utilities.CallByName(newRCM, propertyName, CallType.Get);
                Utilities.CallByName(_reportCompareMasterToEdit, propertyName, CallType.Set, value);
            }

            _reportCompareMasterToEdit.ManufacturerKey = manufacturerKey;

            rbEqual.Checked = false;        // force an event if already "checked"
            rbEqual.Checked = true;
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;
            bool hasDuplicateReport = false;
            List<int> reportKeys = new List<int>();

            if (_parameters.NumberOfItemsToEdit < 1)
            {
                if (manufacturerNameComboBox.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("A manufacturer must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    abort = true;
                }

                if (report1NameComboBox.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Report 1 must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    abort = true;
                }
                else
                {
                    reportKeys.Add(_reportCompareMasterToEdit.Report1Key);
                }

                if (report2NameComboBox.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Report 2 must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    abort = true;
                }
                else
                {
                    if (reportKeys.Contains(_reportCompareMasterToEdit.Report2Key))
                        hasDuplicateReport=true;
                    else
                        reportKeys.Add(_reportCompareMasterToEdit.Report2Key);
                }

                if ((report4NameComboBox.Text.Trim() != string.Empty) && (report3NameComboBox.Text.Trim() == string.Empty))
                {
                    MessageBox.Show("Cannot select Report 4 when Report 3 is not selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    abort = true;
                }

                if ((report5NameComboBox.Text.Trim() != string.Empty) && ((report3NameComboBox.Text.Trim() == string.Empty) || (report4NameComboBox.Text.Trim() == string.Empty)))
                {
                    MessageBox.Show("Cannot select Report 5 when Report 3 or Report 4 is not selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    abort = true;
                }

                if (_reportCompareMasterToEdit.Report3Key.HasValue)
                {
                    if (reportKeys.Contains(_reportCompareMasterToEdit.Report3Key.Value))
                        hasDuplicateReport = true;
                    else
                        reportKeys.Add(_reportCompareMasterToEdit.Report3Key.Value);
                }

                if (_reportCompareMasterToEdit.Report4Key.HasValue)
                {
                    if (reportKeys.Contains(_reportCompareMasterToEdit.Report4Key.Value))
                        hasDuplicateReport = true;
                    else
                        reportKeys.Add(_reportCompareMasterToEdit.Report4Key.Value);
                }

                if (_reportCompareMasterToEdit.Report5Key.HasValue)
                {
                    if (reportKeys.Contains(_reportCompareMasterToEdit.Report5Key.Value))
                        hasDuplicateReport = true;
                    else
                        reportKeys.Add(_reportCompareMasterToEdit.Report5Key.Value);
                }

                if (hasDuplicateReport)
                {
                    MessageBox.Show("A report can only be used once.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    abort = true;
                }
            }

            if (errorMessageTextBox.Text.Trim() == string.Empty)
            {
                    MessageBox.Show("An error message is required.", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    abort = true;
            }

            if (abort)
                return;

            // set the property values in the object that was the target of the edit
            MarkChangedProperties(_reportCompareMasterToEdit);
            _reportCompareMasterToEdit.MarkDirty();

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
