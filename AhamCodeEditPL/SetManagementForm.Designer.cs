﻿namespace AhamMetaDataPL
{
    partial class SetManagementForm<TSet, TItem, TMember>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.groupBoxFilter = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClearFilter = new System.Windows.Forms.Button();
            this.btnApplyDateFilter = new System.Windows.Forms.Button();
            this.dateTimePickerFilter = new System.Windows.Forms.DateTimePicker();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTargetList = new System.Windows.Forms.Label();
            this.chkHideConflictingItems = new System.Windows.Forms.CheckBox();
            this.btnEditValues = new System.Windows.Forms.Button();
            this.btnPrintTargetGrid = new System.Windows.Forms.Button();
            this.sourceGrid = new HaiBusinessUI.ucReadonlyDatagridForm();
            this.ucDateRangeDefault = new HaiBusinessUI.ucDateRange();
            this.targetGrid = new HaiBusinessUI.ucReadonlyDatagridForm();
            this.btnPrintSourceGrid = new System.Windows.Forms.Button();
            this.groupBoxFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(443, 128);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(79, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Add >>";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(443, 186);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(79, 23);
            this.btnEdit.TabIndex = 6;
            this.btnEdit.Text = "Edit dates...";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(443, 157);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(79, 23);
            this.btnRemove.TabIndex = 5;
            this.btnRemove.Text = "<< Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // groupBoxFilter
            // 
            this.groupBoxFilter.Controls.Add(this.label3);
            this.groupBoxFilter.Controls.Add(this.btnClearFilter);
            this.groupBoxFilter.Controls.Add(this.btnApplyDateFilter);
            this.groupBoxFilter.Controls.Add(this.dateTimePickerFilter);
            this.groupBoxFilter.Location = new System.Drawing.Point(540, 7);
            this.groupBoxFilter.Name = "groupBoxFilter";
            this.groupBoxFilter.Size = new System.Drawing.Size(191, 80);
            this.groupBoxFilter.TabIndex = 2;
            this.groupBoxFilter.TabStop = false;
            this.groupBoxFilter.Text = "Date filtering";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Filtering date";
            // 
            // btnClearFilter
            // 
            this.btnClearFilter.Location = new System.Drawing.Point(98, 47);
            this.btnClearFilter.Name = "btnClearFilter";
            this.btnClearFilter.Size = new System.Drawing.Size(78, 23);
            this.btnClearFilter.TabIndex = 2;
            this.btnClearFilter.Text = "Remove filter";
            this.btnClearFilter.UseVisualStyleBackColor = true;
            this.btnClearFilter.Click += new System.EventHandler(this.btnClearFilter_Click);
            // 
            // btnApplyDateFilter
            // 
            this.btnApplyDateFilter.Location = new System.Drawing.Point(10, 47);
            this.btnApplyDateFilter.Name = "btnApplyDateFilter";
            this.btnApplyDateFilter.Size = new System.Drawing.Size(78, 23);
            this.btnApplyDateFilter.TabIndex = 1;
            this.btnApplyDateFilter.Text = "Apply filter";
            this.btnApplyDateFilter.UseVisualStyleBackColor = true;
            this.btnApplyDateFilter.Click += new System.EventHandler(this.btnApplyDateFilter_Click);
            // 
            // dateTimePickerFilter
            // 
            this.dateTimePickerFilter.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerFilter.Location = new System.Drawing.Point(79, 21);
            this.dateTimePickerFilter.Name = "dateTimePickerFilter";
            this.dateTimePickerFilter.Size = new System.Drawing.Size(96, 20);
            this.dateTimePickerFilter.TabIndex = 0;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(947, 677);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(1028, 677);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Available items";
            // 
            // lblTargetList
            // 
            this.lblTargetList.AutoSize = true;
            this.lblTargetList.Location = new System.Drawing.Point(543, 99);
            this.lblTargetList.Name = "lblTargetList";
            this.lblTargetList.Size = new System.Drawing.Size(68, 13);
            this.lblTargetList.TabIndex = 12;
            this.lblTargetList.Text = "List members";
            // 
            // chkHideConflictingItems
            // 
            this.chkHideConflictingItems.AutoSize = true;
            this.chkHideConflictingItems.Location = new System.Drawing.Point(167, 98);
            this.chkHideConflictingItems.Name = "chkHideConflictingItems";
            this.chkHideConflictingItems.Size = new System.Drawing.Size(133, 17);
            this.chkHideConflictingItems.TabIndex = 11;
            this.chkHideConflictingItems.Text = "Hide overlapping items";
            this.chkHideConflictingItems.UseVisualStyleBackColor = true;
            // 
            // btnEditValues
            // 
            this.btnEditValues.Location = new System.Drawing.Point(443, 215);
            this.btnEditValues.Name = "btnEditValues";
            this.btnEditValues.Size = new System.Drawing.Size(79, 23);
            this.btnEditValues.TabIndex = 7;
            this.btnEditValues.Text = "Edit values...";
            this.btnEditValues.UseVisualStyleBackColor = true;
            this.btnEditValues.Click += new System.EventHandler(this.btnEditValues_Click);
            // 
            // btnPrintTargetGrid
            // 
            this.btnPrintTargetGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintTargetGrid.Location = new System.Drawing.Point(540, 635);
            this.btnPrintTargetGrid.Name = "btnPrintTargetGrid";
            this.btnPrintTargetGrid.Size = new System.Drawing.Size(75, 23);
            this.btnPrintTargetGrid.TabIndex = 13;
            this.btnPrintTargetGrid.Text = "Print";
            this.btnPrintTargetGrid.UseVisualStyleBackColor = true;
            this.btnPrintTargetGrid.Click += new System.EventHandler(this.btnPrintTargetGrid_Click);
            // 
            // sourceGrid
            // 
            this.sourceGrid.ActionRedirectionObjectType = HaiBusinessObject.HaiBusinessObjectType.UnknownHaiBusinessObjectType;
            this.sourceGrid.BackColor = System.Drawing.SystemColors.Control;
            this.sourceGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sourceGrid.Location = new System.Drawing.Point(23, 116);
            this.sourceGrid.Name = "sourceGrid";
            this.sourceGrid.Size = new System.Drawing.Size(403, 513);
            this.sourceGrid.TabIndex = 3;
            // 
            // ucDateRangeDefault
            // 
            this.ucDateRangeDefault.Location = new System.Drawing.Point(23, 7);
            this.ucDateRangeDefault.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ucDateRangeDefault.Name = "ucDateRangeDefault";
            this.ucDateRangeDefault.Size = new System.Drawing.Size(287, 80);
            this.ucDateRangeDefault.TabIndex = 1;
            // 
            // targetGrid
            // 
            this.targetGrid.ActionRedirectionObjectType = HaiBusinessObject.HaiBusinessObjectType.UnknownHaiBusinessObjectType;
            this.targetGrid.BackColor = System.Drawing.SystemColors.Control;
            this.targetGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.targetGrid.Location = new System.Drawing.Point(540, 116);
            this.targetGrid.Name = "targetGrid";
            this.targetGrid.Size = new System.Drawing.Size(563, 513);
            this.targetGrid.TabIndex = 8;
            // 
            // btnPrintSourceGrid
            // 
            this.btnPrintSourceGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintSourceGrid.Location = new System.Drawing.Point(23, 635);
            this.btnPrintSourceGrid.Name = "btnPrintSourceGrid";
            this.btnPrintSourceGrid.Size = new System.Drawing.Size(75, 23);
            this.btnPrintSourceGrid.TabIndex = 14;
            this.btnPrintSourceGrid.Text = "Print";
            this.btnPrintSourceGrid.UseVisualStyleBackColor = true;
            this.btnPrintSourceGrid.Click += new System.EventHandler(this.btnPrintSourceGrid_Click);
            // 
            // SetManagementForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1119, 713);
            this.Controls.Add(this.btnPrintSourceGrid);
            this.Controls.Add(this.btnPrintTargetGrid);
            this.Controls.Add(this.btnEditValues);
            this.Controls.Add(this.chkHideConflictingItems);
            this.Controls.Add(this.lblTargetList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sourceGrid);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBoxFilter);
            this.Controls.Add(this.ucDateRangeDefault);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.targetGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetManagementForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Set Management";
            this.Load += new System.EventHandler(this.SetManagement_Load);
            this.groupBoxFilter.ResumeLayout(false);
            this.groupBoxFilter.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private HaiBusinessUI.ucReadonlyDatagridForm targetGrid;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnRemove;
        private HaiBusinessUI.ucDateRange ucDateRangeDefault;
        private System.Windows.Forms.GroupBox groupBoxFilter;
        private System.Windows.Forms.DateTimePicker dateTimePickerFilter;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private HaiBusinessUI.ucReadonlyDatagridForm sourceGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTargetList;
        private System.Windows.Forms.Button btnClearFilter;
        private System.Windows.Forms.Button btnApplyDateFilter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkHideConflictingItems;
        private System.Windows.Forms.Button btnEditValues;
        private System.Windows.Forms.Button btnPrintTargetGrid;
        private System.Windows.Forms.Button btnPrintSourceGrid;

    }
}