﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;

using AhamMetaDataDAL;
using HaiBusinessObject;
using HaiBusinessUI;
using HaiMetaDataDAL;
using HaiInterfaces;

namespace AhamMetaDataPL
{
    public partial class SetManagementForm<TSet, TItem, TMember> : Form
    {
        /* Generic Type parameters
             TSet - type of the set that will be managed
             TItem - type of the items that make up the set
             TMember - type of the members of the set
        */

        private Type _memberEditFormType = null;

        private ISet _setToManage;                          // the set that will be managed
        private List<string> _itemPropertiesToShow;         // properties to show for items that can be in the set
        private List<string> _memberPropertiesToShow;       // properties to show for members of the set

        private bool _isFilterApplied = false;
        private DataServiceParameters _dataserviceParameters = new DataServiceParameters();

        private HaiBindingList<TMember> _allSetMembers;                                     // all members related to this set
        private HaiBindingList<TItem> _allSourceItems;                                      // unfiltered list of source items
        private HaiBindingList<TMember> _targetBindingList;                                 // binding list of members in target grid
        private HaiBindingList<TItem> _sourceBindingList;                                   // binding list of items in the source grid
        private HaiBindingList<TMember> _deletedSetMembers = new HaiBindingList<TMember>(); // members that have been marked for deletion

        private GloballyUsefulStuff _gus;

        private bool _isModelSizeSet = false;

        // form constructors
        public SetManagementForm(ISet setToManage, List<string> itemPropertiesToShow, List<string> memberPropertiesToShow, GloballyUsefulStuff gus)
        {
            InitializeComponent();

            // transfer calling parameters to instance variables
            _setToManage = setToManage;
            _itemPropertiesToShow = itemPropertiesToShow;
            _memberPropertiesToShow = memberPropertiesToShow;
            _gus = gus;

            InitializeMe();
        }

        public SetManagementForm(ISet setToManage, List<string> itemPropertiesToShow, List<string> memberPropertiesToShow, Type memberEditFormType, GloballyUsefulStuff gus)
        {
            InitializeComponent();

            // transfer calling parameters to instance variables
            _memberEditFormType = memberEditFormType;

            _setToManage = setToManage;
            _itemPropertiesToShow = itemPropertiesToShow;
            _memberPropertiesToShow = memberPropertiesToShow;
            _gus = gus;

            InitializeMe();
        }

        private void InitializeMe()
        {
            btnClearFilter.Enabled = false;

            // inject GUS into the grid control (b/c it was created by the designer/InitializeComponent() code)
            sourceGrid.GUS = _gus;
            targetGrid.GUS = _gus;

            // set parameters needed for instantiating the data service
            System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();

            // set up event handlers
            targetGrid.DataGridView.CellDoubleClick += new DataGridViewCellEventHandler(TargetDataGridView_CellDoubleClick);
            sourceGrid.DataGridView.CellDoubleClick += new DataGridViewCellEventHandler(SourceDataGridView_CellDoubleClick);
            ucDateRangeDefault.DateRangeModified += new EventHandler(ucDateRangeDefault_DateRangeModified);

            // if a Type for a SetMember edit form is passed make the "Edit values..." button visible; otherwise hide.
            if (_memberEditFormType == null)
                btnEditValues.Visible = false;
            else
                btnEditValues.Visible = true;
        }

        // get data when form is loaded
        private void SetManagement_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            _isModelSizeSet = _setToManage is AhamMetaDataDAL.ModelSizeSet;

            // set the text of the title bar for this form
            this.Text = "Set management: " + _setToManage.SetName + " [" + _setToManage.SetAbbreviation + "]";

            // create the data service
            AhamDataService ds = new AhamDataService(_dataserviceParameters);

            // get the table of items 
            HaiBusinessObjectBase itemDummy = Activator.CreateInstance<TItem>() as HaiBusinessObjectBase;   // create instance to get ObjectType
            DataAccessResult resultForItem = ds.GetDataList(itemDummy.ObjectType);
            if (resultForItem.Success)
            {
                _allSourceItems = (HaiBindingList<TItem>)resultForItem.DataList;

                // create and fill a "dummy" DataAccessResult (and its DataList) to initialize the source grid
                DataAccessResult resultToInitializeSourceGrid = new DataAccessResult();
                resultToInitializeSourceGrid.Success = true;
                resultToInitializeSourceGrid.BrowsablePropertyList = resultForItem.BrowsablePropertyList;
                HaiBindingList<TItem> sourceItems = new HaiBindingList<TItem>();

                // N.B. Special Case Code to handle limiting items available when managing ModelSizeSet
                // ... specifically, sizes that will be available must have DimensionKey == DimKey
                if (_isModelSizeSet)
                {
                    int modelSizeSetDimKey = ((ModelSizeSet)_setToManage).DimKey;                       // get the dim key for the size set
                    HaiBindingList<AhamMetaDataDAL.Size> itemsToRemove = new HaiBindingList<Size>();    // create a list to record items to be removed

                    foreach (TItem item in _allSourceItems)
                    {
                        AhamMetaDataDAL.Size size = item as AhamMetaDataDAL.Size;
                        if (size.DimensionKey != modelSizeSetDimKey)       // if the dimension key does not match the item should be removed
                        {
                            itemsToRemove.AddToList(size);
                        }
                    }

                    // remove the items from the list
                    foreach (AhamMetaDataDAL.Size size in itemsToRemove)
                    {
                        _allSourceItems.RemoveFromList(size);
                    }
                }

                // create the list of source items that will be shown as "available items"
                foreach (TItem item in _allSourceItems)
                {
                    sourceItems.AddToList(item);
                }
                resultToInitializeSourceGrid.DataList = sourceItems;

                // initialize the source grid
                sourceGrid.PrepareGridForDisplay(resultToInitializeSourceGrid, _itemPropertiesToShow);

                // recover the bindingList from the source grid so that it can be manipulated by FilterSourceGridMembers()
                _sourceBindingList = (HaiBindingList<TItem>)((BindingSource)sourceGrid.DataGridView.DataSource).List;
                
                HaiReports.ReportParameters reportParameters = new HaiReports.ReportParameters();
                reportParameters.SetBasicProperties(HaiReports.ListType.Dictionary);
                _sourceBindingList.ReportParameters = reportParameters;
            }
            else
            {
                MessageBox.Show(resultForItem.Message, "Data retrieval error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            // get the SetMember list
            HaiBusinessObjectBase memberDummy = Activator.CreateInstance<TMember>() as HaiBusinessObjectBase;
            DataAccessResult resultForMember = ds.GetDataList(memberDummy.ObjectType);

            if (resultForMember.Success)
            {
                // filter the universe of members to get only those that belong to this Set
                HaiBindingList<TMember> universeOfSetMembers = (HaiBindingList<TMember>)resultForMember.DataList;
                _allSetMembers = new HaiBindingList<TMember>();

                foreach (TMember setMember in universeOfSetMembers)
                {
                    if (((IMember)setMember).SetKey == _setToManage.SetKey)
                        _allSetMembers.AddToList(setMember);
                }

                HaiBindingList<TMember> targetSetMembers = new HaiBindingList<TMember>();

                foreach (TMember setMember in _allSetMembers)
                {
                    targetSetMembers.AddToList(setMember);
                }
                resultForMember.DataList = targetSetMembers;
                targetGrid.PrepareGridForDisplay(resultForMember, _memberPropertiesToShow);
                if (targetGrid.DataGridView.DataSource == null)
                    MessageBox.Show(resultForMember.Message, "Data retrieval error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    _targetBindingList = (HaiBindingList<TMember>)((BindingSource)targetGrid.DataGridView.DataSource).List;

                    HaiReports.ReportParameters reportParameters = new HaiReports.ReportParameters();
                    reportParameters.SetBasicProperties(HaiReports.ListType.SetMembers);
                    reportParameters.TitleQualifier = _setToManage.SetName;
                    _targetBindingList.ReportParameters = reportParameters;
                }
            }
            else
            {
                MessageBox.Show(resultForMember.Message, "Data retrieval error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            // initialize the control to set default daterange for "Add >" 
            ucDateRangeDefault.InitializeWithOpenDates();   // N.B. forces DateRangeModified event

            // set up handler for checkbox that selects whether to hide items
            // ... that would cause daterange conflict with set members
            chkHideConflictingItems.CheckStateChanged += new EventHandler(chkHideConflictingItems_CheckStateChanged);

            // if there are any rows in the source grid, then make the top-left cell the current cell.
            if (sourceGrid.DataGridView.Rows.Count>0)
                if (sourceGrid.DataGridView.Rows[0].Cells.Count>0)
                sourceGrid.DataGridView.CurrentCell = sourceGrid.DataGridView.Rows[0].Cells[0];

            Cursor = Cursors.Default;
        }

        #region Grid Event Handlers

        // double-click on source grid  => Add item to target
        void SourceDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (sourceGrid.DataGridView.CurrentRow != null)
            {
                if (!(e.RowIndex < 0))
                {
                    sourceGrid.DataGridView.Rows[e.RowIndex].Selected = true;
                    btnAdd_Click(sender, e);
                }
            }
        }

        // double-click on target grid  => Edit item in target
        void TargetDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (targetGrid.DataGridView.CurrentRow != null)
            {
                if (!(e.RowIndex < 0))
                {
                    targetGrid.DataGridView.Rows[e.RowIndex].Selected = true;
                    btnEdit_Click(sender, e);
                }
            }
        }
        #endregion

        #region Button Event Handlers

        // "Add" button  => Add selected item(s) from source to the target grid; verify no date range overlaps
        private void btnAdd_Click(object sender, EventArgs e)
        {
            // get the source (dictionary) items selected for adding
            HaiBindingList<HaiBusinessObjectBase> itemsToAdd = sourceGrid.GetReadonlyBindingListOfSelectedItems(true);
            if (itemsToAdd.Count == 0)
            {
                MessageBox.Show("At least one object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // verify that the current daterange is valid
            string message = string.Empty;
            DateRange desiredDateRange = new DateRange();
            if (!ucDateRangeDefault.UpdateDateRange(desiredDateRange, ref message))
            {
                MessageBox.Show(message, "Date Range Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // validate the desired date range
            message = desiredDateRange.ValidateRangeForType(DateRangeValidationType.Monthly);
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Date Range Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            // create set members for selected dictionaries and specified daterange
            HaiBindingList<TMember> membersToAdd = new HaiBindingList<TMember>();

            foreach (IItem itemToAdd in itemsToAdd)
            {
                TMember memberToAdd = Activator.CreateInstance<TMember>();

                ((IMember)memberToAdd).DateRange.BeginDate = desiredDateRange.BeginDate;
                ((IMember)memberToAdd).DateRange.EndDate = desiredDateRange.EndDate;

                ((IMember)memberToAdd).ItemKey = itemToAdd.ItemKey;
                ((IMember)memberToAdd).ItemName = itemToAdd.ItemName;
                ((IMember)memberToAdd).ItemAbbreviation = itemToAdd.ItemAbbreviation;

                ((IMember)memberToAdd).SetKey = ((ISet)_setToManage).SetKey;
                ((IMember)memberToAdd).SetAbbreviation = ((ISet)_setToManage).SetAbbreviation;
                ((IMember)memberToAdd).SetName = ((ISet)_setToManage).SetName;

                membersToAdd.AddToList(memberToAdd);
            }

            // check for daterange overlap
            bool dateRangeOverlapExits = false;
            foreach (IMember memberToAdd in membersToAdd)
            {
                if (MemberHasDateRangeConflict(memberToAdd, desiredDateRange))
                    dateRangeOverlapExits = true;
            }

            if (dateRangeOverlapExits)
            {
                MessageBox.Show("Date range overlap.", "Edit error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // add the new members to the list of all members
            foreach (TMember memberToAdd in membersToAdd)
            {
                _allSetMembers.AddToList(memberToAdd);
            }

            // set the members displayed in the target grid based on any current filtering
            if (_isFilterApplied)
                FilterTargetGridMembers();
            else
                UnfilterTargetGridMembers();

            bool currentItemWasRemoved = false;
            FilterSourceGridMembers(ref currentItemWasRemoved);
            // if the filter removed the row with current cell in the source grid, then make the current cell null
            if (currentItemWasRemoved)
                sourceGrid.DataGridView.CurrentCell = null;
        }

        // "Edit date ranges" selected item(s) in target list; verify changes do cause daterange overlaps
        private void btnEdit_Click(object sender, EventArgs e)
        {
            // get the selected rows
            targetGrid.SelectRowForCurrentCell();
            HaiBindingList<TMember> membersToEdit = targetGrid.GetReadonlyBindingListOfSelectedItems<TMember>();
            if (membersToEdit.Count == 0)
            {
                MessageBox.Show("At least object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // determine the default daterange to use for updating the objects selected for editing
            DateRange desiredDateRange;
            if (membersToEdit.Count == 1)       // single edit; default to object's daterange
            {
                IMember memberToEdit = (IMember)membersToEdit[0];
                desiredDateRange = new DateRange(memberToEdit.DateRange.BeginDate, memberToEdit.DateRange.EndDate);
            }
            else                                // multiple edit; default to current "add" daterange
            {
                desiredDateRange = new DateRange();
                string message = string.Empty;
                if (!ucDateRangeDefault.UpdateDateRange(desiredDateRange, ref message))
                {
                    MessageBox.Show(message, "Date Range Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }

            // create and open daterange edit form to allow changing the daterange
            EditDateRange editDateRangeForm = new EditDateRange(desiredDateRange);
            if (editDateRangeForm.ShowDialog() != DialogResult.OK)
            {
                editDateRangeForm.Dispose();
                return;
            }

            editDateRangeForm.Dispose();

            // check for daterange overlap
            bool dateRangeOverlapExits = false;
            foreach (TMember member in membersToEdit)
            {
                if (MemberHasDateRangeConflict((IMember)member, desiredDateRange))
                    dateRangeOverlapExits = true;
            }

            if (dateRangeOverlapExits)
            {
                MessageBox.Show("Date range overlap.", "Edit error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                // apply the desired daterange to all members selected for editing
                foreach (IMember member in membersToEdit)  // iterate the list of selected objects
                {
                    // update the date range
                    member.DateRange.BeginDate = desiredDateRange.BeginDate;
                    member.DateRange.EndDate = desiredDateRange.EndDate;
                    ((HaiBusinessObjectBase)member).MarkDirty();

                    // indicate that list item has been modified to force grid to update display with changed value.
                    BindingSource bindingSource = (BindingSource)targetGrid.DataGridView.DataSource;
                    IUntypedBindingList gridMemberList = (IUntypedBindingList)bindingSource.DataSource;
                    gridMemberList.ResetItem(gridMemberList.IndexOf(member));
                }
            }

            // new daterange may hide some members, so call the filtering routine
            if (_isFilterApplied)
                FilterTargetGridMembers();
            else
                UnfilterTargetGridMembers();

            bool currentItemWasRemoved = false;
            FilterSourceGridMembers(ref currentItemWasRemoved);
            // if the filter removed the row with current cell in the source grid, then make the current cell null
            if (currentItemWasRemoved)
                sourceGrid.DataGridView.CurrentCell = null;
        }

        // "Remove" selected item(s) from target list
        private void btnRemove_Click(object sender, EventArgs e)
        {
            // get the selected rows
            targetGrid.SelectRowForCurrentCell();
            HaiBindingList<TMember> membersToDelete = targetGrid.GetReadonlyBindingListOfSelectedItems<TMember>();

            // get confirmation
            if (membersToDelete.Count > 0)
            {
                DialogResult yesno = MessageBox.Show("Are you sure that you want \n\r to remove " 
                    + membersToDelete.Count.ToString() + " object(s)?", "Confirm action", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (yesno == DialogResult.No)
                    return;
            }
            else
            {
                MessageBox.Show("At least one object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // remove the members: mark as deleted, add to the list of deleted members, and remove from lists of available members
            foreach (TMember memberToDelete in membersToDelete)
            {
                HaiBusinessObjectBase memberToDeleteAsBase = memberToDelete as HaiBusinessObjectBase;
                memberToDeleteAsBase.MarkDeleted();
                if (!memberToDeleteAsBase.IsNew)
                    _deletedSetMembers.AddToList(memberToDelete);
                _targetBindingList.RemoveFromList(memberToDelete);
                _allSetMembers.RemoveFromList(memberToDelete);
                targetGrid.Refresh();
            }

            bool currentItemWasRemoved = false;
            FilterSourceGridMembers(ref currentItemWasRemoved);
            // if the filter removed the row with current cell in the source grid, then make the current cell null
            if (currentItemWasRemoved)
                sourceGrid.DataGridView.CurrentCell = null;
        }

        // "Edit values" of selected Set Member(s)
        private void btnEditValues_Click(object sender, EventArgs e)
        {
            // get the selected rows
            targetGrid.SelectRowForCurrentCell();
            HaiBindingList<TMember> membersToEdit = targetGrid.GetReadonlyBindingListOfSelectedItems<TMember>();
            if (membersToEdit.Count == 0)
            {
                MessageBox.Show("At least object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // create an instance of the edit form based on the Type, pass target list of members as a parameter to the constructor
            Form memberEditForm = (Form)Activator.CreateInstance(_memberEditFormType, membersToEdit);
            memberEditForm.ShowDialog();

            if (memberEditForm.DialogResult == DialogResult.OK)
            {
                foreach (TMember member in membersToEdit)
                {
                    HaiBusinessObjectBase memberAsBusObj = member as HaiBusinessObjectBase;
                    memberAsBusObj.MarkDirty();
                }
            }

            targetGrid.Refresh();
            memberEditForm.Dispose();
        }

        // handle possible change in default date range
        void ucDateRangeDefault_DateRangeModified(object sender, EventArgs e)
        {
            bool currentItemWasRemoved = false;
            FilterSourceGridMembers(ref currentItemWasRemoved);
            // if the filter removed the row with current cell in the source grid, then make the current cell null
            if (currentItemWasRemoved)
                sourceGrid.DataGridView.CurrentCell = null;
        }

        // handle request to filter target list items based on date filter
        private void btnApplyDateFilter_Click(object sender, EventArgs e)
        {
            FilterTargetGridMembers();
            btnClearFilter.Enabled = true;
            lblTargetList.Text = "List members - FILTERED";
        }

        // handle request to remove date filtering
        private void btnClearFilter_Click(object sender, EventArgs e)
        {
            UnfilterTargetGridMembers();
            btnClearFilter.Enabled = false;
            lblTargetList.Text = "List members";
        }

        // print the target grid
        private void btnPrintTargetGrid_Click(object sender, EventArgs e)
        {
            targetGrid.PrintGridReport();
        }

        // print the source grid
        private void btnPrintSourceGrid_Click(object sender, EventArgs e)
        {
            sourceGrid.PrintGridReport();
        }

        // "OK" button
        private void btnOK_Click(object sender, EventArgs e)
        {
            // process list of set members and update those that are dirty  _allSetMembers
            AhamDataService ds = new AhamDataService(_dataserviceParameters);
            DataAccessResult result;

            // Special Case Code to
            // ... validate SizeLevel if this edit is for ModelSizeSet
            if (_isModelSizeSet)
            {
                string message = string.Empty;
                foreach(IMember member in _allSetMembers)
                {
                    ModelSizeSetMember modelSizeSetMember = (ModelSizeSetMember)member;
                    if (modelSizeSetMember.SizeLevel < 1)
                    {
                        message += "\n\r" + modelSizeSetMember.UniqueIdentifier;
                    }
                }

                if (message != string.Empty)
                {
                    message = "Size Level must be greater than 0.\r\n\r\nIllegal values found in members:" + message;
                    MessageBox.Show(message, "Data validation error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }


            Cursor = Cursors.WaitCursor;

            foreach (TMember member in _allSetMembers)
            {
                HaiBusinessObjectBase anyHaiBusinessObject = member as HaiBusinessObjectBase;
                if (anyHaiBusinessObject.IsDirty)
                {
                    result = ds.Save(anyHaiBusinessObject);
                    if (!result.Success)
                        MessageBox.Show("Unable to update: " + anyHaiBusinessObject.UniqueIdentifier + "\r\n\nError: " + result.Message, 
                            "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            foreach (TMember member in _deletedSetMembers)
            {
                HaiBusinessObjectBase anyHaiBusinessObject = member as HaiBusinessObjectBase;
                if (anyHaiBusinessObject.IsDeleted)
                {
                    result = ds.Save(anyHaiBusinessObject);
                    if (!result.Success)
                        MessageBox.Show("Unable to update: " + anyHaiBusinessObject.UniqueIdentifier + "\r\n\nError: " + result.Message,
                            "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            Cursor = Cursors.Default;

            this.DialogResult = DialogResult.OK;
        }

        // "Cancel" button
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        #endregion

        #region Other Event Handlers

        void chkHideConflictingItems_CheckStateChanged(object sender, EventArgs e)
        {
            bool currentItemWasRemoved = false;
            FilterSourceGridMembers(ref currentItemWasRemoved);
            // if the filter removed the row with current cell in the source grid, then make the current cell null
            if (currentItemWasRemoved)
                sourceGrid.DataGridView.CurrentCell = null;
        }

        #endregion

        #region Utilities

        private void FilterSourceGridMembers(ref bool currentItemWasRemoved)
        {
            DateRange sourceDateRange = new DateRange();
            string message = string.Empty;
            if (!ucDateRangeDefault.UpdateDateRange(sourceDateRange, ref message))
            {
                MessageBox.Show(message, "Date range error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Cursor = Cursors.WaitCursor;        // the filtering of the source grid could take time.

            // creat a list of set members that have a daterange overlap with the current default daterange
            List<IMember> overlapMembers = new List<IMember>();
            foreach (TMember member in _allSetMembers)
            {
                IMember iMember = (IMember)member;
                if (iMember.DateRange.OverlapsRange(sourceDateRange))   // test for overlap
                    overlapMembers.Add(iMember);                        // ... and add to list potential confilicts
            }

            DataGridViewCell currentCell = sourceGrid.DataGridView.CurrentCell;
            object currentRowItem = null;
            if (currentCell != null)
                    currentRowItem = _sourceBindingList[currentCell.RowIndex];


            // adjust the items available in the source list so that no date range overlaps will be created by "Add >"
            _sourceBindingList.BeginUpdate();                           // suspend firing of OnListChanged events to grid
            foreach (TItem sourceItem in _allSourceItems)               // iterate all possible source items
            {
                bool createsOverlap = false;                            // assume no overlap in the target list

                if (chkHideConflictingItems.Checked)                    // test for overlap only if conflicts should be hidden
                {
                    foreach (IMember iMember in overlapMembers)             // search the list of potential conflicts
                    {                                                       
                        if (((IItem)sourceItem).ItemKey == iMember.ItemKey) // if the item (key) is found
                        {
                            createsOverlap = true;                          // ... then indicate it creates an conflict
                            break;                                          // ... and that no more searching needed; break out.
                        }
                    }
                }

                if (createsOverlap)                                     // if there is now an conflict for this source item
                {
                    if (_sourceBindingList.ContainedInList(sourceItem)) // ... and the offender is in the source binding list
                    {
                        _sourceBindingList.RemoveFromList(sourceItem);  // ... then it must be removed from the available source items
                        if (sourceItem.Equals(currentRowItem))
                        {
                            currentItemWasRemoved = true;
                        }
                    }
                }
                else                                                    // if there is not now a conflict
                {
                    if (!_sourceBindingList.ContainedInList(sourceItem))// ... and this item is not in the source binding list
                        _sourceBindingList.AddToList(sourceItem);       // ... then it must be added to the list of available source items
                }
            }
            _sourceBindingList.EndUpdate();                             // resume event OnListChanged firings, and fire event

            Cursor = Cursors.Default;
        }

        // limit the members shown in the target grid to those with daterange that includes the filtering date
        private void FilterTargetGridMembers()
        {
            DateRange filterDate = new DateRange(dateTimePickerFilter.Value, dateTimePickerFilter.Value);

            _targetBindingList.BeginUpdate();
            foreach (TMember member in _allSetMembers)
            {
                if (((IMember)member).DateRange.OverlapsRange(filterDate))
                {
                    if (!_targetBindingList.ContainedInList(member))
                        _targetBindingList.AddToList(member);
                }
                else
                {
                    if (_targetBindingList.ContainedInList(member))
                        _targetBindingList.RemoveFromList(member);
                }
            }
            _targetBindingList.EndUpdate();

            _isFilterApplied = true;
        }

        // do not limit members shown in the target grid
        private void UnfilterTargetGridMembers()
        {
            _targetBindingList.BeginUpdate();
            foreach (TMember member in _allSetMembers)
            {
                if (!_targetBindingList.ContainedInList(member))
                    _targetBindingList.AddToList(member);
            }
            _targetBindingList.EndUpdate();

            _isFilterApplied = false;
        }

        // test whether a member has a daterange overlap with members currently in the set
        private bool MemberHasDateRangeConflict(IMember memberToTest, DateRange desiredDateRange)
        {
            bool hasDateRangeConflict = false;

            // ensure that no member for this kind of item has a daterange overlap
            foreach (IMember member in _allSetMembers)
            {
                if ((memberToTest.ItemKey == member.ItemKey) && !(memberToTest.Equals(member)))  // the member is in the set; check for daterange conflict
                {
                    if (desiredDateRange.OverlapsRange(member.DateRange))
                        hasDateRangeConflict = true;
                }
            }

            return hasDateRangeConflict;
        }
        #endregion
    }
}
