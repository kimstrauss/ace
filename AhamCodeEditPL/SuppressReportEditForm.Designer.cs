﻿namespace AhamMetaDataPL
{
    partial class SuppressReportEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label beginDateLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label industryReportMemberKeyLabel;
            System.Windows.Forms.Label industryReportMemberNameLabel;
            System.Windows.Forms.Label suppressReportKeyLabel;
            this.suppressReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.beginDateTextBox = new System.Windows.Forms.TextBox();
            this.endDateTextBox = new System.Windows.Forms.TextBox();
            this.industryReportMemberKeyTextBox = new System.Windows.Forms.TextBox();
            this.industryReportMemberNameComboBox = new System.Windows.Forms.ComboBox();
            this.suppressReportKeyTextBox = new System.Windows.Forms.TextBox();
            beginDateLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            industryReportMemberKeyLabel = new System.Windows.Forms.Label();
            industryReportMemberNameLabel = new System.Windows.Forms.Label();
            suppressReportKeyLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.suppressReportBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(592, 181);
            this.btnOK.TabIndex = 5;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(673, 181);
            this.btnCancel.TabIndex = 6;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 4;
            // 
            // beginDateLabel
            // 
            beginDateLabel.AutoSize = true;
            beginDateLabel.Location = new System.Drawing.Point(100, 45);
            beginDateLabel.Name = "beginDateLabel";
            beginDateLabel.Size = new System.Drawing.Size(63, 13);
            beginDateLabel.TabIndex = 30;
            beginDateLabel.Text = "Begin Date:";
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Location = new System.Drawing.Point(108, 71);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.Size = new System.Drawing.Size(55, 13);
            endDateLabel.TabIndex = 32;
            endDateLabel.Text = "End Date:";
            // 
            // industryReportMemberKeyLabel
            // 
            industryReportMemberKeyLabel.AutoSize = true;
            industryReportMemberKeyLabel.Location = new System.Drawing.Point(666, 42);
            industryReportMemberKeyLabel.Name = "industryReportMemberKeyLabel";
            industryReportMemberKeyLabel.Size = new System.Drawing.Size(28, 13);
            industryReportMemberKeyLabel.TabIndex = 34;
            industryReportMemberKeyLabel.Text = "Key:";
            // 
            // industryReportMemberNameLabel
            // 
            industryReportMemberNameLabel.AutoSize = true;
            industryReportMemberNameLabel.Location = new System.Drawing.Point(9, 15);
            industryReportMemberNameLabel.Name = "industryReportMemberNameLabel";
            industryReportMemberNameLabel.Size = new System.Drawing.Size(154, 13);
            industryReportMemberNameLabel.TabIndex = 36;
            industryReportMemberNameLabel.Text = "Industry Report Member Name:";
            // 
            // suppressReportKeyLabel
            // 
            suppressReportKeyLabel.AutoSize = true;
            suppressReportKeyLabel.Location = new System.Drawing.Point(584, 139);
            suppressReportKeyLabel.Name = "suppressReportKeyLabel";
            suppressReportKeyLabel.Size = new System.Drawing.Size(110, 13);
            suppressReportKeyLabel.TabIndex = 38;
            suppressReportKeyLabel.Text = "Suppress Report Key:";
            // 
            // suppressReportBindingSource
            // 
            this.suppressReportBindingSource.DataSource = typeof(AhamMetaDataDAL.SuppressReport);
            // 
            // beginDateTextBox
            // 
            this.beginDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.suppressReportBindingSource, "BeginDate", true));
            this.beginDateTextBox.Location = new System.Drawing.Point(169, 42);
            this.beginDateTextBox.Name = "beginDateTextBox";
            this.beginDateTextBox.Size = new System.Drawing.Size(96, 20);
            this.beginDateTextBox.TabIndex = 1;
            // 
            // endDateTextBox
            // 
            this.endDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.suppressReportBindingSource, "EndDate", true));
            this.endDateTextBox.Location = new System.Drawing.Point(169, 68);
            this.endDateTextBox.Name = "endDateTextBox";
            this.endDateTextBox.Size = new System.Drawing.Size(96, 20);
            this.endDateTextBox.TabIndex = 2;
            // 
            // industryReportMemberKeyTextBox
            // 
            this.industryReportMemberKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.suppressReportBindingSource, "IndustryReportMemberKey", true));
            this.industryReportMemberKeyTextBox.Location = new System.Drawing.Point(696, 39);
            this.industryReportMemberKeyTextBox.Name = "industryReportMemberKeyTextBox";
            this.industryReportMemberKeyTextBox.Size = new System.Drawing.Size(48, 20);
            this.industryReportMemberKeyTextBox.TabIndex = 3;
            this.industryReportMemberKeyTextBox.TabStop = false;
            // 
            // industryReportMemberNameComboBox
            // 
            this.industryReportMemberNameComboBox.FormattingEnabled = true;
            this.industryReportMemberNameComboBox.Location = new System.Drawing.Point(169, 12);
            this.industryReportMemberNameComboBox.Name = "industryReportMemberNameComboBox";
            this.industryReportMemberNameComboBox.Size = new System.Drawing.Size(575, 21);
            this.industryReportMemberNameComboBox.TabIndex = 0;
            // 
            // suppressReportKeyTextBox
            // 
            this.suppressReportKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.suppressReportBindingSource, "SuppressReportKey", true));
            this.suppressReportKeyTextBox.Location = new System.Drawing.Point(696, 136);
            this.suppressReportKeyTextBox.Name = "suppressReportKeyTextBox";
            this.suppressReportKeyTextBox.Size = new System.Drawing.Size(48, 20);
            this.suppressReportKeyTextBox.TabIndex = 4;
            this.suppressReportKeyTextBox.TabStop = false;
            // 
            // SuppressReportEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 216);
            this.Controls.Add(beginDateLabel);
            this.Controls.Add(this.beginDateTextBox);
            this.Controls.Add(endDateLabel);
            this.Controls.Add(this.endDateTextBox);
            this.Controls.Add(industryReportMemberKeyLabel);
            this.Controls.Add(this.industryReportMemberKeyTextBox);
            this.Controls.Add(industryReportMemberNameLabel);
            this.Controls.Add(this.industryReportMemberNameComboBox);
            this.Controls.Add(suppressReportKeyLabel);
            this.Controls.Add(this.suppressReportKeyTextBox);
            this.Name = "SuppressReportEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Suppress Report";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.suppressReportKeyTextBox, 0);
            this.Controls.SetChildIndex(suppressReportKeyLabel, 0);
            this.Controls.SetChildIndex(this.industryReportMemberNameComboBox, 0);
            this.Controls.SetChildIndex(industryReportMemberNameLabel, 0);
            this.Controls.SetChildIndex(this.industryReportMemberKeyTextBox, 0);
            this.Controls.SetChildIndex(industryReportMemberKeyLabel, 0);
            this.Controls.SetChildIndex(this.endDateTextBox, 0);
            this.Controls.SetChildIndex(endDateLabel, 0);
            this.Controls.SetChildIndex(this.beginDateTextBox, 0);
            this.Controls.SetChildIndex(beginDateLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.suppressReportBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource suppressReportBindingSource;
        private System.Windows.Forms.TextBox beginDateTextBox;
        private System.Windows.Forms.TextBox endDateTextBox;
        private System.Windows.Forms.TextBox industryReportMemberKeyTextBox;
        private System.Windows.Forms.ComboBox industryReportMemberNameComboBox;
        private System.Windows.Forms.TextBox suppressReportKeyTextBox;
    }
}