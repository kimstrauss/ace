﻿using System;
using System.Windows.Forms;

using System.Collections;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class SuppressReportEditForm : HaiObjectEditForm
    {
        private SuppressReport _suppressReportToEdit;

        public SuppressReportEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _suppressReportToEdit = (SuppressReport)parameters.EditObject;

            this.Load+=new EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            this.Owner.Cursor = Cursors.WaitCursor;     // since these may be long running the hourglass must be shown on the parent form

            PrepareTheFormToShow(_parameters);
            suppressReportBindingSource.DataSource = _suppressReportToEdit;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;

            result = AhamMetaDataDAL.AhamDataServiceReportManagement.IndustryReportMemberListGet(null, new DataServiceParameters());
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<IndustryReportMember>(industryReportMemberNameComboBox, result, ComboBoxListType.DataListWithBlankItem, "IndustryReportMemberName", "IndustryReportMemberKey", industryReportMemberKeyTextBox, _suppressReportToEdit.IndustryReportMemberName.Trim());
            }
            else
            {
                MessageBox.Show("Cannot add/edit suppress report.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            this.Owner.Cursor = Cursors.Default;        // restore the cursor on the parent
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;
            string message = string.Empty;

            if (industryReportMemberNameComboBox.Text == string.Empty && industryReportMemberNameComboBox.Items.Count>0)
            {
                MessageBox.Show("An Industry Report Member must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            message = ValidateDaterange(_suppressReportToEdit, DateRangeValidationType.Monthly);
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            // inform the caller that the edit is good.
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
