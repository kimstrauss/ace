﻿namespace AhamMetaDataPL
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputFormNameComboBox = new System.Windows.Forms.ComboBox();
            this.inputFormKeyTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // inputFormNameComboBox
            // 
            this.inputFormNameComboBox.FormattingEnabled = true;
            this.inputFormNameComboBox.Location = new System.Drawing.Point(23, 35);
            this.inputFormNameComboBox.Name = "inputFormNameComboBox";
            this.inputFormNameComboBox.Size = new System.Drawing.Size(427, 21);
            this.inputFormNameComboBox.TabIndex = 30;
            // 
            // inputFormKeyTextBox
            // 
            this.inputFormKeyTextBox.Location = new System.Drawing.Point(474, 35);
            this.inputFormKeyTextBox.Name = "inputFormKeyTextBox";
            this.inputFormKeyTextBox.Size = new System.Drawing.Size(45, 20);
            this.inputFormKeyTextBox.TabIndex = 63;
            this.inputFormKeyTextBox.TabStop = false;
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 483);
            this.Controls.Add(this.inputFormKeyTextBox);
            this.Controls.Add(this.inputFormNameComboBox);
            this.Name = "TestForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TestForm";
            this.Load += new System.EventHandler(this.TestForm_Load);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.inputFormNameComboBox, 0);
            this.Controls.SetChildIndex(this.inputFormKeyTextBox, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox inputFormNameComboBox;
        private System.Windows.Forms.TextBox inputFormKeyTextBox;
    }
}