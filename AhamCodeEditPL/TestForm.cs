﻿using System;
using System.Windows.Forms;

using System.Collections;

using AhamMetaDataDAL;
using HaiBusinessUI;
using System.Linq;
using System.Drawing;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace AhamMetaDataPL
{
    public partial class TestForm : HaiObjectEditForm
    {
        public TestForm(EditFormParameters parameters)
        {
            InitializeComponent();
        }

        private void TestForm_Load(object sender, EventArgs e)
        {
            //PrepareTheFormToShow(_parameters);          // the form needs to be prepared before assigning values to controls
            ControlTag controlTag;
            controlTag = new ControlTag();
            inputFormNameComboBox.Tag = controlTag;

            controlTag = new ControlTag();
            inputFormKeyTextBox.Tag = controlTag;

            DataServiceParameters dsParameters = new DataServiceParameters();
            AhamDataService ds = new AhamDataService(dsParameters);

            DataAccessResult result;
            result = ds.GetDataList(HaiBusinessObjectType.InputForm);
            if (result.Success)
            {
                SetupAnyComboBoxForDropDownList<InputForm>(inputFormNameComboBox, result, ComboBoxListType.DataListWithNullItem, "InputFormName", "InputFormKey", inputFormKeyTextBox, "");
            }
            else
            {
                MessageBox.Show("Cannot add/edit input column.\r\n" + result.Message, "Data fetch error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }
        }
    }
}
