﻿namespace AhamMetaDataPL
{
    partial class UseEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label useAbbreviationLabel;
            System.Windows.Forms.Label useKeyLabel;
            System.Windows.Forms.Label useNameLabel;
            this.useAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.useBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.useKeyTextBox = new System.Windows.Forms.TextBox();
            this.useNameTextBox = new System.Windows.Forms.TextBox();
            useAbbreviationLabel = new System.Windows.Forms.Label();
            useKeyLabel = new System.Windows.Forms.Label();
            useNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.useBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 4;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 2;
            // 
            // useAbbreviationLabel
            // 
            useAbbreviationLabel.AutoSize = true;
            useAbbreviationLabel.Location = new System.Drawing.Point(4, 15);
            useAbbreviationLabel.Name = "useAbbreviationLabel";
            useAbbreviationLabel.Size = new System.Drawing.Size(91, 13);
            useAbbreviationLabel.TabIndex = 30;
            useAbbreviationLabel.Text = "Use Abbreviation:";
            // 
            // useKeyLabel
            // 
            useKeyLabel.AutoSize = true;
            useKeyLabel.Location = new System.Drawing.Point(4, 67);
            useKeyLabel.Name = "useKeyLabel";
            useKeyLabel.Size = new System.Drawing.Size(50, 13);
            useKeyLabel.TabIndex = 32;
            useKeyLabel.Text = "Use Key:";
            // 
            // useNameLabel
            // 
            useNameLabel.AutoSize = true;
            useNameLabel.Location = new System.Drawing.Point(4, 41);
            useNameLabel.Name = "useNameLabel";
            useNameLabel.Size = new System.Drawing.Size(60, 13);
            useNameLabel.TabIndex = 34;
            useNameLabel.Text = "Use Name:";
            // 
            // useAbbreviationTextBox
            // 
            this.useAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.useBindingSource, "UseAbbreviation", true));
            this.useAbbreviationTextBox.Location = new System.Drawing.Point(101, 12);
            this.useAbbreviationTextBox.Name = "useAbbreviationTextBox";
            this.useAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.useAbbreviationTextBox.TabIndex = 0;
            // 
            // useBindingSource
            // 
            this.useBindingSource.DataSource = typeof(AhamMetaDataDAL.Use);
            // 
            // useKeyTextBox
            // 
            this.useKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.useBindingSource, "UseKey", true));
            this.useKeyTextBox.Location = new System.Drawing.Point(101, 64);
            this.useKeyTextBox.Name = "useKeyTextBox";
            this.useKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.useKeyTextBox.TabIndex = 33;
            this.useKeyTextBox.TabStop = false;
            // 
            // useNameTextBox
            // 
            this.useNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.useBindingSource, "UseName", true));
            this.useNameTextBox.Location = new System.Drawing.Point(101, 38);
            this.useNameTextBox.Name = "useNameTextBox";
            this.useNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.useNameTextBox.TabIndex = 1;
            // 
            // UseEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(useAbbreviationLabel);
            this.Controls.Add(this.useAbbreviationTextBox);
            this.Controls.Add(useKeyLabel);
            this.Controls.Add(this.useKeyTextBox);
            this.Controls.Add(useNameLabel);
            this.Controls.Add(this.useNameTextBox);
            this.Name = "UseEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Use";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.useNameTextBox, 0);
            this.Controls.SetChildIndex(useNameLabel, 0);
            this.Controls.SetChildIndex(this.useKeyTextBox, 0);
            this.Controls.SetChildIndex(useKeyLabel, 0);
            this.Controls.SetChildIndex(this.useAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(useAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.useBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource useBindingSource;
        private System.Windows.Forms.TextBox useAbbreviationTextBox;
        private System.Windows.Forms.TextBox useKeyTextBox;
        private System.Windows.Forms.TextBox useNameTextBox;
    }
}