﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class UseEditForm : HaiObjectEditForm
    {
        private Use _useToEdit;

        public UseEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _useToEdit = (Use)parameters.EditObject;
            this.Load += new EventHandler(EditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            useBindingSource.DataSource = _useToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (useNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (useAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_useToEdit);
            _useToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
