﻿namespace AhamMetaDataPL
{
    partial class UseSetEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label useSetAbbreviationLabel;
            System.Windows.Forms.Label useSetKeyLabel;
            System.Windows.Forms.Label useSetNameLabel;
            this.useSetAbbreviationTextBox = new System.Windows.Forms.TextBox();
            this.useSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.useSetKeyTextBox = new System.Windows.Forms.TextBox();
            this.useSetNameTextBox = new System.Windows.Forms.TextBox();
            useSetAbbreviationLabel = new System.Windows.Forms.Label();
            useSetKeyLabel = new System.Windows.Forms.Label();
            useSetNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.useSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(374, 181);
            this.btnOK.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(455, 181);
            this.btnCancel.TabIndex = 4;
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 181);
            this.btnInsertNull.TabIndex = 2;
            // 
            // useSetAbbreviationLabel
            // 
            useSetAbbreviationLabel.AutoSize = true;
            useSetAbbreviationLabel.Location = new System.Drawing.Point(6, 15);
            useSetAbbreviationLabel.Name = "useSetAbbreviationLabel";
            useSetAbbreviationLabel.Size = new System.Drawing.Size(110, 13);
            useSetAbbreviationLabel.TabIndex = 30;
            useSetAbbreviationLabel.Text = "Use Set Abbreviation:";
            // 
            // useSetKeyLabel
            // 
            useSetKeyLabel.AutoSize = true;
            useSetKeyLabel.Location = new System.Drawing.Point(6, 67);
            useSetKeyLabel.Name = "useSetKeyLabel";
            useSetKeyLabel.Size = new System.Drawing.Size(69, 13);
            useSetKeyLabel.TabIndex = 32;
            useSetKeyLabel.Text = "Use Set Key:";
            // 
            // useSetNameLabel
            // 
            useSetNameLabel.AutoSize = true;
            useSetNameLabel.Location = new System.Drawing.Point(6, 41);
            useSetNameLabel.Name = "useSetNameLabel";
            useSetNameLabel.Size = new System.Drawing.Size(79, 13);
            useSetNameLabel.TabIndex = 34;
            useSetNameLabel.Text = "Use Set Name:";
            // 
            // useSetAbbreviationTextBox
            // 
            this.useSetAbbreviationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.useSetBindingSource, "UseSetAbbreviation", true));
            this.useSetAbbreviationTextBox.Location = new System.Drawing.Point(122, 12);
            this.useSetAbbreviationTextBox.Name = "useSetAbbreviationTextBox";
            this.useSetAbbreviationTextBox.Size = new System.Drawing.Size(100, 20);
            this.useSetAbbreviationTextBox.TabIndex = 0;
            // 
            // useSetBindingSource
            // 
            this.useSetBindingSource.DataSource = typeof(AhamMetaDataDAL.UseSet);
            // 
            // useSetKeyTextBox
            // 
            this.useSetKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.useSetBindingSource, "UseSetKey", true));
            this.useSetKeyTextBox.Location = new System.Drawing.Point(122, 64);
            this.useSetKeyTextBox.Name = "useSetKeyTextBox";
            this.useSetKeyTextBox.Size = new System.Drawing.Size(50, 20);
            this.useSetKeyTextBox.TabIndex = 33;
            this.useSetKeyTextBox.TabStop = false;
            // 
            // useSetNameTextBox
            // 
            this.useSetNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.useSetBindingSource, "UseSetName", true));
            this.useSetNameTextBox.Location = new System.Drawing.Point(122, 38);
            this.useSetNameTextBox.Name = "useSetNameTextBox";
            this.useSetNameTextBox.Size = new System.Drawing.Size(400, 20);
            this.useSetNameTextBox.TabIndex = 1;
            // 
            // UseSetEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 216);
            this.Controls.Add(useSetAbbreviationLabel);
            this.Controls.Add(this.useSetAbbreviationTextBox);
            this.Controls.Add(useSetKeyLabel);
            this.Controls.Add(this.useSetKeyTextBox);
            this.Controls.Add(useSetNameLabel);
            this.Controls.Add(this.useSetNameTextBox);
            this.Name = "UseSetEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Use Set";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.useSetNameTextBox, 0);
            this.Controls.SetChildIndex(useSetNameLabel, 0);
            this.Controls.SetChildIndex(this.useSetKeyTextBox, 0);
            this.Controls.SetChildIndex(useSetKeyLabel, 0);
            this.Controls.SetChildIndex(this.useSetAbbreviationTextBox, 0);
            this.Controls.SetChildIndex(useSetAbbreviationLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.useSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource useSetBindingSource;
        private System.Windows.Forms.TextBox useSetAbbreviationTextBox;
        private System.Windows.Forms.TextBox useSetKeyTextBox;
        private System.Windows.Forms.TextBox useSetNameTextBox;
    }
}