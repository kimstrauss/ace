﻿using System;
using System.Windows.Forms;

using AhamMetaDataDAL;
using HaiBusinessUI;

namespace AhamMetaDataPL
{
    public partial class UseSetEditForm : HaiBusinessUI.HaiObjectEditForm
    {
        private UseSet _useSetToEdit;

        public UseSetEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            _parameters = parameters;
            _useSetToEdit = (UseSet)parameters.EditObject;

            this.Load += new System.EventHandler(SetEditForm_Load);
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void SetEditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
            useSetBindingSource.DataSource = _useSetToEdit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool abort = false;

            if (useSetNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A name is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (useSetAbbreviationTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("An abbreviation is required.", "Data input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                abort = true;
            }

            if (abort)
                return;

            MarkChangedProperties(_useSetToEdit);
            _useSetToEdit.MarkDirty();

            this.DialogResult = DialogResult.OK;
            Close();
        }

    }
}
