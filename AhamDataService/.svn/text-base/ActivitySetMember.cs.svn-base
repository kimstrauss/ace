﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ActivitySetMember : HaiBusinessObjectBase, IMember        // UNDERLYING TABLE: SMActivity
    {
        public ActivitySetMember() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ActivitySetMember_Aham;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());
        }

        #region Properties

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        private int _activitySetMemberKey;
        public int ActivitySetMemberKey
        {
            get
            {
                return _activitySetMemberKey;
            }
            set
            {
                _activitySetMemberKey = value;
            }
        }

        private int _activitySetKey;
        public int ActivitySetKey
        {
            get
            {
                return _activitySetKey;
            }
            set
            {
                _activitySetKey = value;
            }
        }

        private string _activitySetName;
        public string ActivitySetName
        {
            get
            {
                return _activitySetName;
            }
            set
            {
                _activitySetName = value;
            }
        }

        private string _activitySetAbbreviation;
        public string ActivitySetAbbreviation
        {
            get
            {
                return _activitySetAbbreviation;
            }
            set
            {
                _activitySetAbbreviation = value;
            }
        }
        private int _activityKey;
        public int ActivityKey
        {
            get
            {
                return _activityKey;
            }
            set
            {
                _activityKey = value;
            }
        }

        private string _activityName;
        public string ActivityName
        {
            get
            {
                return _activityName;
            }
            set
            {
                _activityName = value;
            }
        }

        private string _activityAbbreviation;
        public string ActivityAbbreviation
        {
            get
            {
                return _activityAbbreviation;
            }
            set
            {
                _activityAbbreviation = value;
            }
        }
        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return ActivityName + "<->" + ActivitySetName + " [" + DateRange.ToString() + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ActivitySetMemberKey;
            }
            set
            {
                ActivitySetMemberKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ActivitySetMemberKey", "ActivitySetMemberKey", "Key for this activity set member", "Activity set member key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityKey", "ActivityKey", "Key for the activity", "Activity key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivitySetKey", "ActivitySetKey", "Key for the activity set", "Activity set key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityName", "", "Name of the activity", "Activity name");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityAbbreviation", "ActivityAbbrev", "Abbreviation for the activity", "Activity abbreviation");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivitySetName", "ActivitySetName", "Name of the activity set", "Activity set name");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivitySetAbbreviation", "ActivitySetAbbrev", "Abbreviation for the activity set", "Activity set abbreviation");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

        }

        #region IMember Members

        int IMember.ItemKey
        {
            get
            {
                return ActivityKey;
            }
            set
            {
                ActivityKey = value;
            }
        }

        int IMember.SetKey
        {
            get
            {
                return ActivitySetKey;
            }
            set
            {
                ActivitySetKey = value;
            }
        }

        string IMember.ItemAbbreviation
        {
            get
            {
                return ActivityAbbreviation;
            }
            set
            {
                ActivityAbbreviation = value;
            }
        }

        string IMember.ItemName
        {
            get
            {
                return ActivityName;
            }
            set
            {
                ActivityName = value;
            }
        }

        string IMember.SetAbbreviation
        {
            get
            {
                return ActivitySetAbbreviation;
            }
            set
            {
                ActivitySetAbbreviation = value;
            }
        }

        string IMember.SetName
        {
            get
            {
                return ActivitySetName;
            }
            set
            {
                ActivitySetName = value;
            }
        }

        DateRange IMember.DateRange
        {
            get
            {
                return DateRange;
            }
        }

        #endregion


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
