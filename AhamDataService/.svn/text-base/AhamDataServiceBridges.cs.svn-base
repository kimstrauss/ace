﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal static class AhamDataServiceBridges
    {

        #region ProductMarket

        internal static DataAccessResult ProductMarketListGet(ProductMarket productMarketToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ProductMarket> productMarkets = new HaiBindingList<ProductMarket>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<AhamDataLinq.ProductMarket>(x => x.DProduct);
                    dlo.LoadWith<AhamDataLinq.ProductMarket>(x => x.DMarket);
                    dlo.LoadWith<AhamDataLinq.ProductMarket>(x => x.DChannel);

                    List<AhamDataLinq.ProductMarket> dProductMarkets;
                    if (productMarketToGet == null)
                    {
                        dProductMarkets = ahamOper.ProductMarkets.ToList<AhamDataLinq.ProductMarket>();
                    }
                    else
                    {
                        dProductMarkets = new List<AhamDataLinq.ProductMarket>();
                        dProductMarkets.Add(ahamOper.ProductMarkets.Single<AhamDataLinq.ProductMarket>(x => (x.ProductMarketKey == productMarketToGet.ProductMarketKey)));
                    }

                    foreach (AhamDataLinq.ProductMarket dProductMarket in dProductMarkets)
                    {
                        ProductMarket productMarket = new ProductMarket();

                        productMarket.ProductMarketKey = dProductMarket.ProductMarketKey;

                        productMarket.ProductKey = dProductMarket.ProductKey;
                        productMarket.ProductName = dProductMarket.DProduct.ProductName.Trim();
                        productMarket.MarketKey = dProductMarket.MarketKey;
                        productMarket.MarketName = dProductMarket.DMarket.MarketName.Trim();
                        productMarket.ChannelKey = dProductMarket.ChannelKey;
                        productMarket.ChannelName = dProductMarket.DChannel.ChannelName.Trim();

                        productMarket.BeginDate = dProductMarket.BeginDate.Date;
                        productMarket.EndDate = dProductMarket.EndDate.Date;

                        productMarket.ApplicationContext = dProductMarket.AppContext.Trim();
                        productMarket.RowVersion = dProductMarket.RowVersion;
                        productMarket.UserName = dProductMarket.EntryUid.Trim();

                        productMarket.MarkOld();
                        productMarkets.AddToList(productMarket);
                    }

                    result.DataList = productMarkets;
                    result.Success = true;
                    result.BrowsablePropertyList = ProductMarket.GetBrowsablePropertyList();

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ProductMarketGet(ProductMarket productMarketToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ProductMarketListGet(productMarketToGet, parameters);
            if (result.Success)
            {
                ProductMarket productMarket = (ProductMarket)result.SingleItem;
                result.ItemIsChanged = (productMarketToGet.RowVersion != productMarket.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ProductMarketSave(ProductMarket productMarket, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (productMarket.IsSavable)
            {
                if (productMarket.IsDeleted)
                    result = ProductMarketDelete(productMarket, parameters);
                else if (productMarket.IsNew)
                    result = ProductMarketInsert(productMarket, parameters);
                else if (productMarket.IsDirty)
                    result = ProductMarketUpdate(productMarket, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ProductMarketInsert(ProductMarket productMarket, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.ProductMarket dProductMarket = new AhamDataLinq.ProductMarket();

                    dProductMarket.ProductKey = productMarket.ProductKey;
                    dProductMarket.MarketKey = productMarket.MarketKey;
                    dProductMarket.ChannelKey = productMarket.ChannelKey;

                    dProductMarket.BeginDate = productMarket.BeginDate.Date;
                    dProductMarket.EndDate = productMarket.EndDate.Date;

                    dProductMarket.AppContext = parameters.ApplicationContext.Trim();
                    dProductMarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.ProductMarkets.InsertOnSubmit(dProductMarket);
                    ahamOper.SubmitChanges();

                    productMarket.ProductMarketKey = dProductMarket.ProductMarketKey;
                    productMarket.RowVersion = dProductMarket.RowVersion;

                    productMarket.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ProductMarketUpdate(ProductMarket productMarket, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.ProductMarket dProductMarket = ahamOper.ProductMarkets.Single<AhamDataLinq.ProductMarket>(x => (x.ProductMarketKey == productMarket.ProductMarketKey));

                    if (dProductMarket.RowVersion != productMarket.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + productMarket.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dProductMarket.MarketKey = productMarket.MarketKey;
                    dProductMarket.ChannelKey = productMarket.ChannelKey;

                    dProductMarket.BeginDate = productMarket.BeginDate.Date;
                    dProductMarket.EndDate = productMarket.EndDate.Date;

                    dProductMarket.AppContext = parameters.ApplicationContext.Trim();
                    dProductMarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    productMarket.MarkOld();
                    productMarket.RowVersion = dProductMarket.RowVersion;
                    result.Success = true;   
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ProductMarketDelete(ProductMarket productMarket, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.ProductMarket dProductMarket = ahamOper.ProductMarkets.Single<AhamDataLinq.ProductMarket>(x => (x.ProductMarketKey == productMarket.ProductMarketKey));

                    if (dProductMarket.RowVersion != productMarket.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + productMarket.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dProductMarket.AppContext=parameters.ApplicationContext.Trim();
                    dProductMarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.ProductMarkets.DeleteOnSubmit(dProductMarket);
                    ahamOper.SubmitChanges();

                    productMarket.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion
    }
}
