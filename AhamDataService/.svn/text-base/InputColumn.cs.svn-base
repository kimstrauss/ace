﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class InputColumn : HaiBusinessObjectBase        // UNDERLYING TABLE: DInputColumn
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public InputColumn() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.InputColumn;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ColumnHeader = string.Empty;
            ColumnHeader2 = string.Empty;
            InputFormName = string.Empty;
            ActivityName = string.Empty;
            GeographyName = string.Empty;
            UseName = string.Empty;
            ChannelName = string.Empty;
            MeasureName = string.Empty;
            CatalogName = string.Empty;
            MarketName = string.Empty;
            Size1Name = string.Empty;
            Size2Name = string.Empty;
            Size3Name = string.Empty;
            Size4Name = string.Empty;
            Size5Name = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "InputColumnKey = " + InputColumnKey.ToString();
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return InputColumnKey;
            }
            set
            {
                InputColumnKey = value;
            }
        }

        public int InputColumnKey
        {
            get;
            set;
        }

        public string ColumnHeader
        {
            get;
            set;
        }

        public string ColumnHeader2
        {
            get;
            set;
        }

        public int SortKey
        {
            get;
            set;
        }

        public int? TargetYear
        {
            get;
            set;
        }

        public int InputFormKey
        {
            get;
            set;
        }

        public string InputFormName
        {
            get;
            set;
        }

        public int? ActivityKey
        {
            get;
            set;
        }

        public string ActivityName
        {
            get;
            set;
        }

        public int? GeographyKey
        {
            get;
            set;
        }

        public string GeographyName
        {
            get;
            set;
        }

        public int? UseKey
        {
            get;
            set;
        }

        public string UseName
        {
            get;
            set;
        }

        public int? ChannelKey
        {
            get;
            set;
        }
        public string ChannelName
        {
            get;
            set;
        }

        public int? MeasureKey
        {
            get;
            set;
        }

        public string MeasureName
        {
            get;
            set;
        }

        public int? CatalogKey
        {
            get;
            set;
        }

        public string CatalogName
        {
            get;
            set;
        }

        public int? MarketKey
        {
            get;
            set;
        }

        public string MarketName
        {
            get;
            set;
        }

        public int? Size1Key
        {
            get;
            set;
        }

        public string Size1Name
        {
            get;
            set;
        }

        public int? Size2Key
        {
            get;
            set;
        }

        public string Size2Name
        {
            get;
            set;
        }

        public int? Size3Key
        {
            get;
            set;
        }

        public string Size3Name
        {
            get;
            set;
        }

        public int? Size4Key
        {
            get;
            set;
        }

        public string Size4Name
        {
            get;
            set;
        }

        public int? Size5Key
        {
            get;
            set;
        }

        public string Size5Name
        {
            get;
            set;
        }

        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("InputColumnKey", "DInputColumnKey", "Key for the input column", "Input column key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("InputFormKey", "DInputFormKey", "Key for the input form", "Input form key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("InputFormName", "", "Name of the input form", "Input form name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ColumnHeader", "ColumnHeader1", "Column header", "Column header");
            bp.MaxStringLength = 50;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ColumnHeader2", "ColumnHeader2", "Column header2", "Column header2");
            bp.MaxStringLength = 50;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SortKey", "SortKey", "Sort key", "Sort key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("TargetYear", "targetyear", "Target year", "Target year");
            bp.DataType = TypeCode.Int32;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityKey", "activitykey", "Key for the activity", "Activity key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityName", "", "Name of the activity", "Activity name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyKey", "geokey", "Key for the geography", "Geography key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyName", "", "Name of the geography", "Geography name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseKey", "usekey", "Key for the use", "Use key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseName", "", "Name of the use", "Use name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelKey", "channelkey", "Key for the channel", "Channel key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelName", "", "Name of the channel", "Channel name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureKey", "measurekey", "Key for the measure", "Measure key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureName", "", "Name of the measure", "Measure name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogKey", "catalogkey", "Key for the catalog", "Catalog key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogName", "", "Name of the catalog", "Catalog name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketKey", "marketkey", "Key for the market", "Market key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketName", "", "Name of the market", "Market name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size1Key", "size1key", "Key for Size1", "Size1 key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size1Name", "", "Name of Size1", "Size1 name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size2Key", "size2key", "Key for Size2", "Size2 key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size2Name", "", "Name of Size2", "Size2 name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size3Key", "size3key", "Key for Size3", "Size3 key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size3Name", "", "Name of Size3", "Size3 name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size4Key", "size4key", "Key for Size4", "Size4 key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size4Name", "", "Name of Size4", "Size4 name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size5Key", "size5key", "Key for Size5", "Size5 key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size5Name", "", "Name of Size5", "Size5 name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
