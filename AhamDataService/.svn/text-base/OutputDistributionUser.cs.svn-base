﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class OutputDistributionUser : HaiBusinessObjectBase, IDateRange // UNDERLYING TABLE: OutputDistributionUser
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public OutputDistributionUser()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.OutputDistributionUser;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            OutputReportID = string.Empty;
            UserCode = string.Empty;

            // Initializations for IDateRange implementation
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());
            if (_kindredPropertyNames == null)
            {
                _kindredPropertyNames = new string[0] { };
            }
        }

        #region Properties

        public int OutputDistributionUserKey
        {
            get;
            set;
        }

        public string UserCode
        {
            get;
            set;
        }

        public int OutputReportKey
        {
            get;
            set;
        }

        public string OutputReportID
        {
            get;
            set;
        }

        public bool Disabled
        {
            get;
            set;
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "OutputDistributionUserKey = " + OutputDistributionUserKey.ToString();
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        private static string[] _kindredPropertyNames;
        [Browsable(false)]
        public string[] KindredPropertyNames
        {
            get
            {
                return _kindredPropertyNames;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return OutputDistributionUserKey;
            }
            set
            {
                OutputDistributionUserKey = value;
            }
        }

        #endregion

        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("OutputDistributionUserKey", "OutputDistributionUserKey", "Key for the output distribution", "Key for the output distribution");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UserCode", "Ntuser", "User code", "User code");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 50;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("OutputReportKey", "OutputReportKey", "Key for the output report", "Key for the output report");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("OutputReportID", "", "ID for the output report", "ID for the output report");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 0;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Disabled", "disabled", "Disabled?", "Disabled?");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "begindate", "Begin date", "Begin date");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "Enddate", "End date", "End date");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion

    }
}
