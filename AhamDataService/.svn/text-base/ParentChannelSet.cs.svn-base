﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ParentChannelSet : HaiBusinessObjectBase, ISet        // UNDERLYING TABLE: SDChannel
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ParentChannelSet()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ParentChannelSet;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ParentChannelSetAbbreviation = string.Empty;
            ParentChannelSetAbbreviation = string.Empty;
        }

        #region Properties
        private int _parentChannelSetKey;
        public int ParentChannelSetKey
        {
            get
            {
                return _parentChannelSetKey;
            }
            set
            {
                _parentChannelSetKey = value;
            }
        }

        private string _parentChannelSetName;
        public string ParentChannelSetName
        {
            get
            {
                return ParentChannelSetName;
            }
            set
            {
                ParentChannelSetName = value;
            }
        }

        private string _parentChannelSetAbbreviation;
        public string ParentChannelSetAbbreviation
        {
            get
            {
                return _parentChannelSetAbbreviation;
            }
            set
            {
                _parentChannelSetAbbreviation = value;
            }
        }
        #endregion

        public override string UniqueIdentifier
        {
            get
            {
                return ParentChannelSetName + " [" + ParentChannelSetAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ParentChannelSetKey;
            }
            set
            {
                ParentChannelSetKey = value;
            }
        }

        #region ISet Members

        [Browsable(false)]
        int ISet.SetKey
        {
            get
            {
                return ParentChannelSetKey;
            }
        }

        [Browsable(false)]
        string ISet.SetName
        {
            get
            {
                return ParentChannelSetName;
            }
        }

        [Browsable(false)]
        string ISet.SetAbbreviation
        {
            get
            {
                return ParentChannelSetAbbreviation;
            }
        }

        #endregion


        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ParentChannelSetKey", "ParentChannelSetKey", "Key for the channel set", "Parent Channel set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ParentChannelSetName", "ParentChannelSetName", "Name of the channel set", "Parent Channel set name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ParentChannelSetAbbreviation", "ParentChannelSetAbbrev", "Abbreviation for the channel set", "Parent Channel set abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
