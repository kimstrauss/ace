﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class Activity : HaiBusinessObjectBase, IItem        // UNDERLYING TABLE: DActivity
    {
        public Activity() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Activity_Aham;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ActivityName = string.Empty;
            ActivityAbbreviation = string.Empty;
            ActivityStockFlow = string.Empty;
        }

        #region Properties
        private int _activityKey;
        public int ActivityKey
        {
            get
            {
                return _activityKey;
            }
            set
            {
                _activityKey = value;
            }
        }

        private string _activityName;
        public string ActivityName
        {
            get
            {
                return _activityName;
            }
            set
            {
                _activityName = value;
            }
        }

        private string _activityAbbreviation;
        public string ActivityAbbreviation
        {
            get
            {
                return _activityAbbreviation;
            }
            set
            {
                _activityAbbreviation = value;
            }
        }

        private string _activityStockFlow;
        public string ActivityStockFlow
        {
            get
            {
                return _activityStockFlow;
            }
            set
            {
                _activityStockFlow = value;
            }
        }
        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return ActivityName + " [" + ActivityAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ActivityKey;
            }
            set
            {
               ActivityKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ActivityKey", "ActivityKey", "Key for the activity", "Activity key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityName", "ActivityName", "Name of the activity", "Activity name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityAbbreviation", "ActivityAbbrev", "Abbreviation for the activity", "Activity abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityStockFlow", "ActivityStockFlow", "Stock or flow", "Stock or flow");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 1;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region IItem Members

        [Browsable(false)]
        int IItem.ItemKey
        {
            get
            {
                return ActivityKey;
            }
        }

        [Browsable(false)]
        string IItem.ItemName
        {
            get
            {
                return ActivityName;
            }
        }

        [Browsable(false)]
        string IItem.ItemAbbreviation
        {
            get
            {
                return ActivityAbbreviation;
            }
        }

        #endregion


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
