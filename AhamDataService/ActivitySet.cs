﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ActivitySet : HaiBusinessObjectBase, ISet        // UNDERLYING TABLE: SDActivity
    {
        public ActivitySet() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ActivitySet_Aham;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();
        }

        #region Properties

        private int _activitySetKey;
        public int ActivitySetKey
        {
            get
            {
                return _activitySetKey;
            }
            set
            {
                _activitySetKey = value;
            }
        }

        private string _activitySetName;
        public string ActivitySetName
        {
            get
            {
                return _activitySetName;
            }
            set
            {
                _activitySetName = value;
            }
        }

        private string _activitySetAbbreviation;
        public string ActivitySetAbbreviation
        {
            get
            {
                return _activitySetAbbreviation;
            }
            set
            {
                _activitySetAbbreviation = value;
            }
        }

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return ActivitySetName + " [" + ActivitySetAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ActivitySetKey;
            }
            set
            {
                ActivitySetKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ActivitySetKey", "ActivitySetKey", "Key for the activity set ", "Activity set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivitySetName", "ActivitySetName", "Name of the activity set", "Activity set name");
            bp.MaxStringLength = 64;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivitySetAbbreviation", "ActivitySetAbbrev", "Abbreviation for the activity set", "Activity set abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region ISet Members

        int ISet.SetKey
        {
            get
            {
                return ActivitySetKey;
            }
        }

        string ISet.SetName
        {
            get
            {
                return ActivitySetName;
            }
        }

        string ISet.SetAbbreviation
        {
            get
            {
                return ActivitySetAbbreviation;
            }
        }

        #endregion


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
