﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Linq;
using System.Linq;

using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    public class AhamDataService : IMetaDataDS
    {
        private DataServiceParameters _parameters;

        public AhamDataService(DataServiceParameters parameters)
        {
            _parameters = parameters;
        }

        /*   === FUTURE MODIFICATIONS FOR POSSIBLE WCF SERVICE ARCHITECTURE ===
         * 
         * "Constructor" for possible future singleton instantiation of AhamDataService, as
         * would be needed for using a WCF service.
         * 
         * For this to work, the existing public constructor will need to be made private,
         * and the DataServiceParameters object will have to be removed from the constructor.
         * Instead, a DataServiceParameters will have to be passed with each call to the
         * methods of the singleton.  For example, the call for saving an object would
         * need to look like:
         * 
         *      DataAccessResult result = ahamDataService.Save(parameters, anyHaiBusinessObject);
         *      
         * Save() would then pass the parameters object on to the specific "Save", such as
         * 
         *      AhamDataServiceCatalog.CatalogSave((Catalog)anyHaiBusinessObject, parameters)
         *      
         
            static private AhamDataService _ahamDataService = new AhamDataService(new DataServiceParameters());
            public static AhamDataService GetAhamDataService()
            {
                return _ahamDataService;
            }
         
         * Implementing this change would require modifying all calls to methods on AhamDataService
         * to include a "DataServiceParameters" object that will be supplied by the caller.  Because
         * various callers will be using the same singleton in the service, the DataServiceParameters
         * object need to be "customized" for the caller to indicate the name of the user, etc.
         * 
         */

        public DataAccessResult GetDataList(HaiBusinessObjectType objectType)
        {
            switch (objectType)
            {
                case HaiBusinessObjectType.Catalog:
                    return AhamDataServiceCatalog.CatalogListGet(null, _parameters);
                case HaiBusinessObjectType.CatalogSet:
                    return AhamDataServiceCatalog.CatalogSetListGet(null, _parameters);
                case HaiBusinessObjectType.CatalogSetMember:
                    return AhamDataServiceCatalog.CatalogSetMemberListGet(null, _parameters);

                case HaiBusinessObjectType.Channel:
                    return AhamDataServiceChannel.ChannelListGet(null, _parameters);
                case HaiBusinessObjectType.ChannelSet:
                    return AhamDataServiceChannel.ChannelSetListGet(null, _parameters);
                case HaiBusinessObjectType.ChannelSetMember:
                    return AhamDataServiceChannel.ChannelSetMemberListGet(null, _parameters);
                case HaiBusinessObjectType.ChannelHeirarchySet:
                    return AhamDataServiceChannel.ChannelHeirarchySetListGet(null, _parameters);
                case HaiBusinessObjectType.ChannelHeirarchySetMember:
                    return AhamDataServiceChannel.ChannelHeirarchySetMemberListGet(null, _parameters);

                case HaiBusinessObjectType.Activity_Aham:
                    return AhamDataServiceActivity.ActivityListGet(null, _parameters);
                case HaiBusinessObjectType.ActivitySet_Aham:
                    return AhamDataServiceActivity.ActivitySetListGet(null, _parameters);
                case HaiBusinessObjectType.ActivitySetMember_Aham:
                    return AhamDataServiceActivity.ActivitySetMemberListGet(null, _parameters);

                case HaiBusinessObjectType.Geography :
                    return AhamDataServiceGeography.GeographyListGet(null, _parameters);
                case HaiBusinessObjectType.GeographySet:
                    return AhamDataServiceGeography.GeographySetListGet(null, _parameters);
                case HaiBusinessObjectType.GeographySetMember:
                    return AhamDataServiceGeography.GeographySetMemberListGet(null,_parameters);

                case HaiBusinessObjectType.Market:
                    return AhamDataServiceMarket.MarketListGet(null, _parameters);
                case HaiBusinessObjectType.MarketSet:
                    return AhamDataServiceMarket.MarketSetListGet(null, _parameters);
                case HaiBusinessObjectType.MarketSetMember:
                    return AhamDataServiceMarket.MarketSetMemberListGet(null, _parameters);

                case HaiBusinessObjectType.Use:
                    return AhamDataServiceUse.UseListGet(null, _parameters);
                case HaiBusinessObjectType.UseSet:
                    return AhamDataServiceUse.UseSetListGet(null, _parameters);
                case HaiBusinessObjectType.UseSetMember:
                    return AhamDataServiceUse.UseSetMemberListGet(null, _parameters);

                case HaiBusinessObjectType.Product_Aham:
                    return AhamDataServiceProduct.ProductListGet(null, _parameters);
                case HaiBusinessObjectType.ProductSet_Aham:
                    return AhamDataServiceProduct.ProductSetListGet(null, _parameters);
                case HaiBusinessObjectType.ProductSetMember_Aham:
                    return AhamDataServiceProduct.ProductSetMemberListGet(null, _parameters);

                case HaiBusinessObjectType.Measure:
                    return AhamDataServiceMeasure.MeasureListGet(null, _parameters);
                case HaiBusinessObjectType.MeasureSet:
                    return AhamDataServiceMeasure.MeasureSetListGet(null, _parameters);
                case HaiBusinessObjectType.MeasureSetMember:
                    return AhamDataServiceMeasure.MeasureSetMemberListGet(null, _parameters);

                case HaiBusinessObjectType.Council:
                    return AhamDataServiceCouncil.CouncilListGet(null, _parameters);

                case HaiBusinessObjectType.Group:
                    return AhamDataServiceGroup.GroupListGet(null, _parameters);

                case HaiBusinessObjectType.GroupMember:
                    return AhamDataServiceGroupMember.GroupMemberListGet(null, _parameters);

                case HaiBusinessObjectType.ExpansionFactor:
                    return AhamDataServiceExpansionFactor.ExpansionFactorListGet(null, _parameters);

                case HaiBusinessObjectType.CellDisclosure:
                    return AhamDataServiceCellDisclosure.CellDisclosureListGet(null, _parameters);

                case HaiBusinessObjectType.Footnote:
                    return AhamDataServiceCellDisclosure.FootnoteListGet(null, _parameters);

                case HaiBusinessObjectType.OutputReport:
                    return AhamDataServiceCellDisclosure.OutputReportListGet(null, _parameters);

                case HaiBusinessObjectType.HolidayDate:
                    return AhamDataServiceReportManagement.HolidayDateListGet(null, _parameters);

                case HaiBusinessObjectType.HolidayCADate:
                    return AhamDataServiceReportManagement.HolidayCADateListGet(null, _parameters);

                case HaiBusinessObjectType.ReleaseException:
                    return AhamDataServiceReportManagement.ReleaseExceptionListGet(null, _parameters);

                case HaiBusinessObjectType.ContactRoles:
                    return AhamDataServiceContactManagement.ContactRolesListGet(null, _parameters);

                case HaiBusinessObjectType.Manufacturer:
                    return AhamDataServiceBusinessEntities.ManufacturerListGet(null, _parameters);

                case HaiBusinessObjectType.Association:
                    return AhamDataServiceBusinessEntities.AssociationListGet(null, _parameters);

                case HaiBusinessObjectType.FSList:
                    return AhamDataServiceContactManagement.FSListListGet(null, _parameters);

                case HaiBusinessObjectType.ManufacturerBrand:
                    return AhamDataServiceReportManagement.ManufacturerBrandListGet(null, _parameters);

                case HaiBusinessObjectType.Frequency:
                    return AhamDataServiceReportManagement.FrequencyListGet(null, _parameters);

                case HaiBusinessObjectType.InputForm:
                    return AhamDataServiceReportManagement.InputFormListGet(null, _parameters);

                case HaiBusinessObjectType.ManufacturerReport:
                    return AhamDataServiceReportManagement.ManufacturerReportListGet(null, _parameters);

                case HaiBusinessObjectType.Dimension:
                    return AhamDataServiceDimension.DimensionListGet(null, _parameters);

                case HaiBusinessObjectType.ProductDimension:
                    return AhamDataServiceDimension.ProductDimensionListGet(null, _parameters);

                case HaiBusinessObjectType.ModelSizeSet:
                    return AhamDataServiceSize.ModelSizeSetListGet(null, _parameters);

                case HaiBusinessObjectType.ManufacturerMissingBrand:
                    return AhamDataServiceReportManagement.ManufacturerMissingBrandListGet(null, _parameters);

                case HaiBusinessObjectType.SuppressReport:
                    return AhamDataServiceReportManagement.SuppressReportListGet(null, _parameters);

                case HaiBusinessObjectType.OutputDistributionManufacturer:
                    return AhamDataServiceReportManagement.OutputDistributionManufacturerListGet(null, _parameters);

                case HaiBusinessObjectType.OutputDistributionUser:
                    return AhamDataServiceReportManagement.OutputDistributionUserListGet(null, _parameters);

                case HaiBusinessObjectType.OutputReportPublic:
                    return AhamDataServiceReportManagement.OutputReportPublicListGet(null, _parameters);

                case HaiBusinessObjectType.ProductMarket:
                    return AhamDataServiceBridges.ProductMarketListGet(null, _parameters);

                case HaiBusinessObjectType.ProductSum:
                    return AhamDataServiceProduct.ProductSumListGet(null, _parameters);

                case HaiBusinessObjectType.InputColumn:
                    return AhamDataServiceReportManagement.InputColumnListGet(null, _parameters);

                case HaiBusinessObjectType.InputRow:
                    return AhamDataServiceReportManagement.InputRowListGet(null, _parameters);

                case HaiBusinessObjectType.Size:
                    return AhamDataServiceSize.SizeListGet(null, _parameters);

                case HaiBusinessObjectType.ModelSizeSetMember:
                    return AhamDataServiceSize.ModelSizeSetMemberListGet(null, _parameters);

                case HaiBusinessObjectType.ReportCompareMaster:
                    return AhamDataServiceReportManagement.ReportCompareMasterListGet(null, _parameters);

                case HaiBusinessObjectType.ReportCompareDetail:
                    return AhamDataServiceReportManagement.ReportCompareDetailListGet(null, _parameters);


                default:
                    DataAccessResult errorResult = new DataAccessResult();
                    errorResult.Message = "Unsupported data type requested [GetDataList()]: " + objectType.ToString();
                    errorResult.Success = false;
                    return errorResult;
            }
        }

        public DataAccessResult DataItemReGet(HaiBusinessObjectBase anyHaiBusinessObject)
        {
            switch (anyHaiBusinessObject.ObjectType)
            {
                case HaiBusinessObjectType.Catalog:
                    return AhamDataServiceCatalog.CatalogGet((Catalog)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.CatalogSet:
                    return AhamDataServiceCatalog.CatalogSetGet((CatalogSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.CatalogSetMember:
                    return AhamDataServiceCatalog.CatalogSetMemberGet((CatalogSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Channel:
                    return AhamDataServiceChannel.ChannelGet((Channel)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ChannelSet:
                    return AhamDataServiceChannel.ChannelSetGet((ChannelSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ChannelSetMember:
                    return AhamDataServiceChannel.ChannelSetMemberGet((ChannelSetMember)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ChannelHeirarchySet:
                    return AhamDataServiceChannel.ChannelHeirarchySetGet((ChannelHeirarchySet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ChannelHeirarchySetMember:
                    return AhamDataServiceChannel.ChannelHeirarchySetMemberGet((ChannelHeirarchySetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Activity_Aham:
                    return AhamDataServiceActivity.ActivityGet((Activity)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ActivitySet_Aham:
                    return AhamDataServiceActivity.ActivitySetGet((ActivitySet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ActivitySetMember_Aham:
                    return AhamDataServiceActivity.ActivitySetMemberGet((ActivitySetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Geography:
                    return AhamDataServiceGeography.GeographyGet((Geography)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.GeographySet:
                    return AhamDataServiceGeography.GeographySetGet((GeographySet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.GeographySetMember:
                    return AhamDataServiceGeography.GeographySetMemberGet((GeographySetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Market:
                    return AhamDataServiceMarket.MarketGet((Market)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.MarketSet:
                    return AhamDataServiceMarket.MarketSetGet((MarketSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.MarketSetMember:
                    return AhamDataServiceMarket.MarketSetMemberGet((MarketSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Use:
                    return AhamDataServiceUse.UseGet((Use)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.UseSet:
                    return AhamDataServiceUse.UseSetGet((UseSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.UseSetMember:
                    return AhamDataServiceUse.UseSetMemberGet((UseSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Product_Aham:
                    return AhamDataServiceProduct.ProductGet((Product)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ProductSet_Aham:
                    return AhamDataServiceProduct.ProductSetGet((ProductSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ProductSetMember_Aham:
                    return AhamDataServiceProduct.ProductSetMemberGet((ProductSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Measure:
                    return AhamDataServiceMeasure.MeasureGet((Measure)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.MeasureSet:
                    return AhamDataServiceMeasure.MeasureSetGet((MeasureSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.MeasureSetMember:
                    return AhamDataServiceMeasure.MeasureSetMemberGet((MeasureSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Council:
                    return AhamDataServiceCouncil.CouncilGet((Council)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Group:
                    return AhamDataServiceGroup.GroupGet((Group)anyHaiBusinessObject,_parameters);

                case HaiBusinessObjectType.GroupMember:
                    return AhamDataServiceGroupMember.GroupMemberGet((GroupMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ExpansionFactor:
                    return AhamDataServiceExpansionFactor.ExpansionFactorGet((ExpansionFactor)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.CellDisclosure:
                    return AhamDataServiceCellDisclosure.CellDisclosureGet((CellDisclosure)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Footnote:
                    return AhamDataServiceCellDisclosure.FootnoteGet((Footnote)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.HolidayDate:
                    return AhamDataServiceReportManagement.HolidayDateGet((HolidayDate)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.HolidayCADate:
                    return AhamDataServiceReportManagement.HolidayCADateGet((HolidayCADate)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ReleaseException:
                    return AhamDataServiceReportManagement.ReleaseExceptionGet((ReleaseException)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ContactRoles:
                    return AhamDataServiceContactManagement.ContactRolesGet((ContactRoles)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Manufacturer:
                    return AhamDataServiceBusinessEntities.ManufacturerGet((Manufacturer)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Association:
                    return AhamDataServiceBusinessEntities.AssociationGet((Association)anyHaiBusinessObject, _parameters);
                    
                case HaiBusinessObjectType.FSList:
                    return AhamDataServiceContactManagement.FSListGet((FSList)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ManufacturerBrand:
                    return AhamDataServiceReportManagement.ManufacturerBrandGet((ManufacturerBrand)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.InputForm:
                    return AhamDataServiceReportManagement.InputFormGet((InputForm)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ManufacturerReport:
                    return AhamDataServiceReportManagement.ManufacturerReportGet((ManufacturerReport)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Dimension:
                    return AhamDataServiceDimension.DimensionGet((Dimension)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ProductDimension:
                    return AhamDataServiceDimension.ProductDimensionGet((ProductDimension)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ModelSizeSet:
                    return AhamDataServiceSize.ModelSizeSetGet((ModelSizeSet)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ManufacturerMissingBrand:
                    return AhamDataServiceReportManagement.ManufacturerMissingBrandGet((ManufacturerMissingBrand)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.SuppressReport:
                    return AhamDataServiceReportManagement.SuppressReportGet((SuppressReport)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.OutputDistributionManufacturer:
                    return AhamDataServiceReportManagement.OutputDistributionManufacturerGet((OutputDistributionManufacturer)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.OutputDistributionUser:
                    return AhamDataServiceReportManagement.OutputDistributionUserGet((OutputDistributionUser)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.OutputReportPublic:
                    return AhamDataServiceReportManagement.OutputReportPublicGet((OutputReportPublic)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ProductMarket:
                    return AhamDataServiceBridges.ProductMarketGet((ProductMarket)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ProductSum:
                    return AhamDataServiceProduct.ProductSumGet((ProductSum)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.InputColumn:
                    return AhamDataServiceReportManagement.InputColumnGet((InputColumn)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.InputRow:
                    return AhamDataServiceReportManagement.InputRowGet((InputRow)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Size:
                    return AhamDataServiceSize.SizeGet((Size)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ModelSizeSetMember:
                    return AhamDataServiceSize.ModelSizeSetMemberGet((ModelSizeSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ReportCompareMaster:
                    return AhamDataServiceReportManagement.ReportCompareMasterGet((ReportCompareMaster)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ReportCompareDetail:
                    return AhamDataServiceReportManagement.ReportCompareDetailGet((ReportCompareDetail)anyHaiBusinessObject, _parameters);


                default:
                    DataAccessResult errorResult = new DataAccessResult();
                    errorResult.Message = "Unsupported data type requested [DataItemReGet()]: " + anyHaiBusinessObject.ObjectType.ToString();
                    errorResult.Success = false;
                    return errorResult;
            }
        }

        public DataAccessResult Save(HaiBusinessObjectBase anyHaiBusinessObject)
        {
            // no database action need if "deleting" a new object
            if (anyHaiBusinessObject.IsDeleted  && anyHaiBusinessObject.IsNew) 
            {
                DataAccessResult result = new DataAccessResult();
                result.Success = true;
                return result;
            }

            // Log user activity while in development
            //LogSaveActivity(anyHaiBusinessObject);

            switch (anyHaiBusinessObject.ObjectType)
            {
                case HaiBusinessObjectType.Catalog:
                    return AhamDataServiceCatalog.CatalogSave((Catalog)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.CatalogSet:
                    return AhamDataServiceCatalog.CatalogSetSave((CatalogSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.CatalogSetMember:
                    return AhamDataServiceCatalog.CatalogSetMemberSave((CatalogSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Channel:
                    return AhamDataServiceChannel.ChannelSave((Channel)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ChannelSet:
                    return AhamDataServiceChannel.ChannelSetSave((ChannelSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ChannelSetMember:
                    return AhamDataServiceChannel.ChannelSetMemberSave((ChannelSetMember)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ChannelHeirarchySet:
                    return AhamDataServiceChannel.ChannelHeirarchySetSave((ChannelHeirarchySet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ChannelHeirarchySetMember:
                    return AhamDataServiceChannel.ChannelHeirarchySetMemberSave((ChannelHeirarchySetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Activity_Aham:
                    return AhamDataServiceActivity.ActivitySave((Activity)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ActivitySet_Aham:
                    return AhamDataServiceActivity.ActivitySetSave((ActivitySet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ActivitySetMember_Aham:
                    return AhamDataServiceActivity.ActivitySetMemberSave((ActivitySetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Geography:
                    return AhamDataServiceGeography.GeographySave((Geography)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.GeographySet:
                    return AhamDataServiceGeography.GeographySetSave((GeographySet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.GeographySetMember:
                    return AhamDataServiceGeography.GeographySetMemberSave((GeographySetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Market:
                    return AhamDataServiceMarket.MarketSave((Market)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.MarketSet:
                    return AhamDataServiceMarket.MarketSetSave((MarketSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.MarketSetMember:
                    return AhamDataServiceMarket.MarketSetMemberSave((MarketSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Use:
                    return AhamDataServiceUse.UseSave((Use)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.UseSet:
                    return AhamDataServiceUse.UseSetSave((UseSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.UseSetMember:
                    return AhamDataServiceUse.UseSetMemberSave((UseSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Product_Aham:
                    return AhamDataServiceProduct.ProductSave((Product)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ProductSet_Aham:
                    return AhamDataServiceProduct.ProductSetSave((ProductSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.ProductSetMember_Aham:
                    return AhamDataServiceProduct.ProductSetMemberSave((ProductSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Measure:
                    return AhamDataServiceMeasure.MeasureSave((Measure)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.MeasureSet:
                    return AhamDataServiceMeasure.MeasureSetSave((MeasureSet)anyHaiBusinessObject, _parameters);
                case HaiBusinessObjectType.MeasureSetMember:
                    return AhamDataServiceMeasure.MeasureSetMemberSave((MeasureSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Council:
                    return AhamDataServiceCouncil.CouncilSave((Council)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Group:
                    return AhamDataServiceGroup.GroupSave((Group)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.GroupMember:
                    return AhamDataServiceGroupMember.GroupMemberSave((GroupMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ExpansionFactor:
                    return AhamDataServiceExpansionFactor.ExpansionFactorSave((ExpansionFactor)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.CellDisclosure:
                    return AhamDataServiceCellDisclosure.CellDisclosureSave((CellDisclosure)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Footnote:
                    return AhamDataServiceCellDisclosure.FootnoteSave((Footnote)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.HolidayDate:
                    return AhamDataServiceReportManagement.HolidayDateSave((HolidayDate)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.HolidayCADate:
                    return AhamDataServiceReportManagement.HolidayCADateSave((HolidayCADate)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ReleaseException:
                    return AhamDataServiceReportManagement.ReleaseExceptionSave((ReleaseException)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ContactRoles:
                    return AhamDataServiceContactManagement.ContactRolesSave((ContactRoles)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Manufacturer:
                    return AhamDataServiceBusinessEntities.ManufacturerSave((Manufacturer)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Association:
                    return AhamDataServiceBusinessEntities.AssociationSave((Association)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.FSList:
                    return AhamDataServiceContactManagement.FSListSave((FSList)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ManufacturerBrand:
                    return AhamDataServiceReportManagement.ManufacturerBrandSave((ManufacturerBrand)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.InputForm:
                    return AhamDataServiceReportManagement.InputFormSave((InputForm)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ManufacturerReport:
                    return AhamDataServiceReportManagement.ManufacturerReportSave((ManufacturerReport)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.Dimension:
                    return AhamDataServiceDimension.DimensionSave((Dimension)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ProductDimension:
                    return AhamDataServiceDimension.ProductDimensionSave((ProductDimension)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ModelSizeSet:
                    return AhamDataServiceSize.ModelSizeSetSave((ModelSizeSet)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ManufacturerMissingBrand:
                    return AhamDataServiceReportManagement.ManufacturerMissingBrandSave((ManufacturerMissingBrand)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.SuppressReport:
                    return AhamDataServiceReportManagement.SuppressReportSave((SuppressReport)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.OutputDistributionManufacturer:
                    return AhamDataServiceReportManagement.OutputDistributionManufacturerSave((OutputDistributionManufacturer)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.OutputDistributionUser:
                    return AhamDataServiceReportManagement.OutputDistributionUserSave((OutputDistributionUser)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.OutputReportPublic:
                    return AhamDataServiceReportManagement.OutputReportPublicSave((OutputReportPublic)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ProductMarket:
                    return AhamDataServiceBridges.ProductMarketSave((ProductMarket)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ProductSum:
                    return AhamDataServiceProduct.ProductSumSave((ProductSum)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.InputColumn:
                    return AhamDataServiceReportManagement.InputColumnSave((InputColumn)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.InputRow:
                    return AhamDataServiceReportManagement.InputRowSave((InputRow)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ModelSizeSetMember:
                    return AhamDataServiceSize.ModelSizeSetMemberSave((ModelSizeSetMember)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ReportCompareMaster:
                    return AhamDataServiceReportManagement.ReportCompareMasterSave((ReportCompareMaster)anyHaiBusinessObject, _parameters);

                case HaiBusinessObjectType.ReportCompareDetail:
                    return AhamDataServiceReportManagement.ReportCompareDetailSave((ReportCompareDetail)anyHaiBusinessObject, _parameters);


                default:
                    DataAccessResult errorResult = new DataAccessResult();
                    errorResult.Message = "Unsupported data type requested [Save()]: " + anyHaiBusinessObject.ObjectType.ToString();
                    errorResult.Success = false;
                    return errorResult;
            }
        }

        public DataAccessResult DuplicateSetMembers(HaiBusinessObjectBase sourceSet, HaiBusinessObjectBase targetSet)
        {

            switch (sourceSet.ObjectType)
            {
                case HaiBusinessObjectType.ActivitySet_Aham:
                    return AhamDataServiceActivity.ActivitySetMembersDuplicate((ActivitySet)sourceSet, (ActivitySet)targetSet, _parameters);

                case HaiBusinessObjectType.CatalogSet:
                    return AhamDataServiceCatalog.CatalogSetMembersDuplicate((CatalogSet)sourceSet, (CatalogSet)targetSet, _parameters);

                case HaiBusinessObjectType.ChannelSet:
                    return AhamDataServiceChannel.ChannelSetMembersDuplicate((ChannelSet)sourceSet, (ChannelSet)targetSet, _parameters);

                //case HaiBusinessObjectType.ChannelHeirarchySet:
                //    return AhamDataServiceChannel.ChannelHeirarchySetMembersDuplicate((ChannelHeirarchySet)sourceSet, (ChannelHeirarchySet)targetSet, _parameters);

                case HaiBusinessObjectType.GeographySet:
                    return AhamDataServiceGeography.GeographySetMembersDuplicate((GeographySet)sourceSet, (GeographySet)targetSet, _parameters);

                case HaiBusinessObjectType.MarketSet:
                    return AhamDataServiceMarket.MarketSetMembersDuplicate((MarketSet)sourceSet, (MarketSet)targetSet, _parameters);

                case HaiBusinessObjectType.UseSet:
                    return AhamDataServiceUse.UseSetMembersDuplicate((UseSet)sourceSet, (UseSet)targetSet, _parameters);

                case HaiBusinessObjectType.ProductSet_Aham:
                    return AhamDataServiceProduct.ProductSetMembersDuplicate((ProductSet)sourceSet, (ProductSet)targetSet, _parameters);

                case HaiBusinessObjectType.MeasureSet:
                    return AhamDataServiceMeasure.MeasureSetMembersDuplicate((MeasureSet)sourceSet, (MeasureSet)targetSet, _parameters);

                case HaiBusinessObjectType.ModelSizeSetMember:
                    return AhamDataServiceSize.ModelSizeSetMembersDuplicate((ModelSizeSet)sourceSet, (ModelSizeSet)targetSet, _parameters);


                default:
                    DataAccessResult errorResult = new DataAccessResult();
                    errorResult.Message = "Unsupported data type requested [DuplicateSetMembers()]: " + sourceSet.ObjectType.ToString();
                    errorResult.Success = false;
                    return errorResult;
            }
        }

        public DataAccessTestResult TestObjectForSaveConflicts(HaiBusinessObjectBase anyHaiBusinessObject)
        {
            DataAccessTestResult testResult = new DataAccessTestResult();

            if (anyHaiBusinessObject is ManufacturerReport)
            {
                ManufacturerReport mfrReport = (ManufacturerReport)anyHaiBusinessObject;
                testResult = AhamDataServiceReportManagement.TestReportForSaveConflicts(mfrReport, _parameters);
                testResult.ObjectName = mfrReport.ReportName;   // get name of report with conflict; use in return message
            }
            else
            {
                testResult.Success = true;
                testResult.Passed = true;
                testResult.Message = string.Empty;
            }

            return testResult;
        }



// LOGGING TO FACILITATE UNDERSTANDING OF TESTING ACTIVITY.  REMOVE FOR PRODUCTION
        private void LogSaveActivity(HaiBusinessObjectBase anyHaiBusinessObject)
        {
            string objectName = anyHaiBusinessObject.ObjectType.ToString();
            string userName = _parameters.UserName;

            string action = string.Empty;
            if (anyHaiBusinessObject.IsNew) action = "Add";
            else if (anyHaiBusinessObject.IsDeleted) action = "Delete";
            else if (anyHaiBusinessObject.IsDirty) action = "Update";
            else action = "Unknown";

            string serverName = Utilities.GetServerName();
            string databaseName = Utilities.GetDatabaseName();

            string connectionString = "Data Source=sqlDev16/Tr;Initial Catalog=HaiAdmin;Integrated Security=True";
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            string workstation = connection.WorkstationId;
            int objectKey = anyHaiBusinessObject.PrimaryKey;

            string sqlText = "INSERT INTO AceLog (ObjectName, UserName, ServerName, DatabaseName, Action, [When], Workstation, Objectkey) "
                + " VALUES('" + objectName + "', '" + userName + "', '" + serverName + "', '" + databaseName + "', '" + action + "', '" + DateTime.Now.ToString() + "', '" + workstation + "', '" + objectKey + "')";

            SqlCommand sqlCommand = new SqlCommand(sqlText, connection);
            sqlCommand.ExecuteNonQuery();

            connection.Close();
            connection.Dispose();
            sqlCommand.Dispose();
        }
    }
}
