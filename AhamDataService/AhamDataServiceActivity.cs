﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal static class AhamDataServiceActivity
    {

        #region Activity

        internal static DataAccessResult ActivityListGet(Activity activityToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Activity> activities = new HaiBindingList<Activity>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DActivity> DActivities;
                    if (activityToGet == null)
                    {
                        DActivities = ahamOper.DActivities.ToList<DActivity>();
                    }
                    else
                    {
                        DActivities = new List<DActivity>();
                        DActivities.Add(ahamOper.DActivities.Single<DActivity>(x => (x.ActivityKey == activityToGet.ActivityKey)));
                    }

                    foreach (DActivity dactivity in DActivities)
                    {
                        Activity activity = new Activity();

                        activity.ActivityAbbreviation = dactivity.ActivityAbbrev.Trim();
                        activity.ActivityKey = dactivity.ActivityKey;
                        activity.ActivityName = dactivity.ActivityName.Trim();

                        activity.ActivityStockFlow = dactivity.ActivityStockFlow.ToString();

                        activity.ApplicationContext = dactivity.AppContext.Trim();
                        activity.RowVersion = dactivity.RowVersion;
                        activity.UserName = dactivity.EntryUid.Trim();

                        activity.MarkOld();
                        activities.AddToList(activity);
                    }

                    result.DataList = activities;
                    result.Success = true;
                    result.BrowsablePropertyList = Activity.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ActivityGet(Activity activityToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ActivityListGet(activityToGet, parameters);
            if (result.Success)
            {
                Activity activity = (Activity)result.SingleItem;
                result.ItemIsChanged = (activityToGet.RowVersion != activity.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ActivitySave(Activity activity, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (activity.IsSavable)
            {
                if (activity.IsDeleted)
                    result = ActivityDelete(activity, parameters);
                else if (activity.IsNew)
                    result = ActivityInsert(activity, parameters);
                else if (activity.IsDirty)
                    result = ActivityUpdate(activity, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ActivityInsert(Activity activity, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DActivity dactivity = new DActivity();
                    dactivity.ActivityAbbrev = activity.ActivityAbbreviation.Trim();
                    dactivity.ActivityName = activity.ActivityName.Trim();

                    dactivity.ActivityStockFlow = (char)activity.ActivityStockFlow[0];

                    dactivity.AppContext = parameters.ApplicationContext.Trim();
                    dactivity.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DActivity>().InsertOnSubmit(dactivity);
                    ahamOper.SubmitChanges();

                    activity.ActivityKey = dactivity.ActivityKey;
                    activity.RowVersion = dactivity.RowVersion;

                    activity.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ActivityUpdate(Activity activity, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DActivity dactivity = ahamOper.DActivities.Single<DActivity>(x => (x.ActivityKey == activity.ActivityKey));

                    if (dactivity.RowVersion != activity.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + activity.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dactivity.ActivityAbbrev = activity.ActivityAbbreviation.Trim();
                    dactivity.ActivityName = activity.ActivityName.Trim();

                    dactivity.ActivityStockFlow = (char)activity.ActivityStockFlow[0];

                    dactivity.AppContext = parameters.ApplicationContext.Trim();
                    dactivity.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    activity.MarkOld();
                    activity.RowVersion = dactivity.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ActivityDelete(Activity activity, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DActivity dactivity = ahamOper.DActivities.Single<DActivity>(x => (x.ActivityKey == activity.ActivityKey));

                    if (dactivity.RowVersion != activity.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + activity.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dactivity.AppContext = parameters.ApplicationContext.Trim();
                    dactivity.EntryUid = parameters.UserName.Trim();

                    ahamOper.DActivities.DeleteOnSubmit(dactivity);
                    ahamOper.SubmitChanges();

                    activity.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region ActivitySet

        internal static DataAccessResult ActivitySetListGet(ActivitySet activitySetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ActivitySet> activitySetList = new HaiBindingList<ActivitySet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SDActivity> SDActivities;
                    if (activitySetToGet == null)
                        SDActivities = ahamOper.SDActivities.ToList<SDActivity>();
                    else
                    {
                        SDActivities = new List<SDActivity>();
                        SDActivities.Add(ahamOper.SDActivities.Single<SDActivity>(x => (x.ActivitySetKey == activitySetToGet.ActivitySetKey)));
                    }

                    foreach (SDActivity sdactivity in SDActivities)
                    {
                        ActivitySet activitySet = new ActivitySet();

                        activitySet.ActivitySetAbbreviation = sdactivity.ActivitySetAbbrev.Trim();
                        activitySet.ActivitySetKey = sdactivity.ActivitySetKey;
                        activitySet.ActivitySetName = sdactivity.ActivitySetName.Trim();

                        activitySet.ApplicationContext = sdactivity.AppContext.Trim();
                        activitySet.RowVersion = sdactivity.RowVersion;
                        activitySet.UserName = sdactivity.EntryUid;

                        activitySet.MarkOld();
                        activitySetList.AddToList(activitySet);
                    }

                    result.DataList = activitySetList;
                    result.Success = true;
                    result.BrowsablePropertyList = ActivitySet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ActivitySetGet(ActivitySet activitySetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ActivitySetListGet(activitySetToGet, parameters);
            if (result.Success)
            {
                ActivitySet activitySet = (ActivitySet)result.SingleItem;
                result.ItemIsChanged = (activitySetToGet.RowVersion != activitySet.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ActivitySetSave(ActivitySet activitySet, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (activitySet.IsSavable)
            {
                if (activitySet.IsDeleted)
                    result = ActivitySetDelete(activitySet, parameters);
                else if (activitySet.IsNew)
                    result = ActivitySetInsert(activitySet, parameters);
                else if (activitySet.IsDirty)
                    result = ActivitySetUpdate(activitySet, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ActivitySetInsert(ActivitySet activitySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDActivity sdactivity = new SDActivity();
                    sdactivity.ActivitySetAbbrev = activitySet.ActivitySetAbbreviation.Trim();
                    sdactivity.ActivitySetName = activitySet.ActivitySetName.Trim();

                    sdactivity.AppContext = parameters.ApplicationContext.Trim();
                    sdactivity.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SDActivity>().InsertOnSubmit(sdactivity);
                    ahamOper.SubmitChanges();

                    activitySet.ActivitySetKey = sdactivity.ActivitySetKey;
                    activitySet.RowVersion = sdactivity.RowVersion;

                    activitySet.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ActivitySetUpdate(ActivitySet activitySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDActivity sdactivity = ahamOper.SDActivities.Single<SDActivity>(x => (x.ActivitySetKey == activitySet.ActivitySetKey));

                    if (sdactivity.RowVersion != activitySet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + activitySet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdactivity.ActivitySetAbbrev = activitySet.ActivitySetAbbreviation.Trim();
                    sdactivity.ActivitySetName = activitySet.ActivitySetName.Trim();

                    sdactivity.AppContext = parameters.ApplicationContext.Trim();
                    sdactivity.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    activitySet.MarkOld();
                    activitySet.RowVersion = sdactivity.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ActivitySetDelete(ActivitySet activitySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDActivity sdactivity = ahamOper.SDActivities.Single<SDActivity>(x => (x.ActivitySetKey == activitySet.ActivitySetKey));

                    if (sdactivity.RowVersion != activitySet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + activitySet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdactivity.AppContext = parameters.ApplicationContext.Trim();
                    sdactivity.EntryUid = parameters.UserName.Trim();

                    ahamOper.SDActivities.DeleteOnSubmit(sdactivity);
                    ahamOper.SubmitChanges();

                    activitySet.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region ActivitySetMember

        internal static DataAccessResult ActivitySetMemberListGet(ActivitySetMember activitySetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ActivitySetMember> activitySetMemberList = new HaiBindingList<ActivitySetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMActivity> SMActivities;
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<SMActivity>(x => x.DActivity);
                    dlo.LoadWith<SMActivity>(x => x.SDActivity);
                    ahamOper.LoadOptions = dlo;

                    if (activitySetMemberToGet == null)
                    {
                        SMActivities = ahamOper.SMActivities.ToList<SMActivity>();
                    }
                    else
                    {
                        SMActivities = new List<SMActivity>();
                        SMActivities.Add(ahamOper.SMActivities.Single<SMActivity>(x => (x.ActivitySetMemberKey == activitySetMemberToGet.ActivitySetMemberKey)));
                    }

                    foreach (SMActivity smactivity in SMActivities)
                    {
                        ActivitySetMember activitySetMember = new ActivitySetMember();

                        activitySetMember.ActivityKey = smactivity.ActivityKey;
                        activitySetMember.ActivitySetKey = smactivity.ActivitySetKey;
                        activitySetMember.ActivitySetMemberKey = smactivity.ActivitySetMemberKey;
                        activitySetMember.DateRange.BeginDate = smactivity.BeginDate.Date;
                        activitySetMember.DateRange.EndDate = smactivity.EndDate.Date;

                        activitySetMember.ActivityName = smactivity.DActivity.ActivityName.Trim();
                        activitySetMember.ActivityAbbreviation = smactivity.DActivity.ActivityAbbrev.Trim();
                        activitySetMember.ActivitySetName = smactivity.SDActivity.ActivitySetName.Trim();
                        activitySetMember.ActivitySetAbbreviation = smactivity.SDActivity.ActivitySetAbbrev.Trim();

                        activitySetMember.ApplicationContext = smactivity.AppContext.Trim();
                        activitySetMember.RowVersion = smactivity.RowVersion;
                        activitySetMember.UserName = smactivity.EntryUid.Trim();

                        activitySetMember.MarkOld();
                        activitySetMemberList.AddToList(activitySetMember);
                    }

                    result.DataList = activitySetMemberList;
                    result.Success = true;
                    result.BrowsablePropertyList = ActivitySetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ActivitySetMemberGet(ActivitySetMember activitySetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ActivitySetMemberListGet(activitySetMemberToGet, parameters);
            if (result.Success)
            {
                ActivitySetMember activitySetMember = (ActivitySetMember)result.SingleItem;
                result.ItemIsChanged = (activitySetMemberToGet.RowVersion != activitySetMember.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ActivitySetMemberSave(ActivitySetMember activitySetMember, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (activitySetMember.IsSavable)
            {
                if (activitySetMember.IsDeleted)
                    result = ActivitySetMemberDelete(activitySetMember, parameters);
                else if (activitySetMember.IsNew)
                    result = ActivitySetMemberInsert(activitySetMember, parameters);
                else if (activitySetMember.IsDirty)
                    result = ActivitySetMemberUpdate(activitySetMember, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ActivitySetMemberInsert(ActivitySetMember activitySetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMActivity smactivity = new SMActivity();
                    smactivity.ActivityKey = activitySetMember.ActivityKey;
                    smactivity.ActivitySetKey = activitySetMember.ActivitySetKey;
                    smactivity.BeginDate = activitySetMember.DateRange.BeginDate.Date;
                    smactivity.EndDate = activitySetMember.DateRange.EndDate.Date;

                    smactivity.AppContext = parameters.ApplicationContext.Trim();
                    smactivity.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SMActivity>().InsertOnSubmit(smactivity);
                    ahamOper.SubmitChanges();

                    activitySetMember.ActivitySetMemberKey = smactivity.ActivitySetMemberKey;
                    activitySetMember.RowVersion = smactivity.RowVersion;

                    activitySetMember.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ActivitySetMemberUpdate(ActivitySetMember activitySetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMActivity smactivity = ahamOper.SMActivities.Single<SMActivity>(x => (x.ActivitySetMemberKey == activitySetMember.ActivitySetMemberKey));

                    if (smactivity.RowVersion != activitySetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + activitySetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smactivity.ActivityKey = activitySetMember.ActivityKey;
                    smactivity.ActivitySetMemberKey = activitySetMember.ActivitySetMemberKey;
                    smactivity.BeginDate = activitySetMember.DateRange.BeginDate.Date;
                    smactivity.EndDate = activitySetMember.DateRange.EndDate.Date;

                    smactivity.AppContext = parameters.ApplicationContext.Trim();
                    smactivity.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    activitySetMember.MarkOld();
                    activitySetMember.RowVersion = smactivity.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ActivitySetMemberDelete(ActivitySetMember activitySetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMActivity smactivity = ahamOper.SMActivities.Single<SMActivity>(x => (x.ActivitySetMemberKey == activitySetMember.ActivitySetMemberKey));

                    if (smactivity.RowVersion != activitySetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + activitySetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smactivity.AppContext = parameters.ApplicationContext.Trim();
                    smactivity.EntryUid = parameters.UserName.Trim();

                    ahamOper.SMActivities.DeleteOnSubmit(smactivity);
                    ahamOper.SubmitChanges();

                    activitySetMember.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ActivitySetMembersDuplicate(ActivitySet sourceActivitySet, ActivitySet targetActivitySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMActivity> smActivities;
                    List<SMActivity> duplicatedItems = new List<SMActivity>();

                    smActivities = ahamOper.SMActivities.Where(x => (x.ActivitySetKey == sourceActivitySet.ActivitySetKey)).ToList<SMActivity>();

                    foreach (SMActivity smActivity in smActivities)
                    {
                        SMActivity duplicateSMActivity = new SMActivity();

                        duplicateSMActivity.ActivitySetKey = targetActivitySet.ActivitySetKey;
                        duplicateSMActivity.ActivityKey = smActivity.ActivityKey;
                        duplicateSMActivity.BeginDate = smActivity.BeginDate.Date;
                        duplicateSMActivity.EndDate = smActivity.EndDate.Date;
                        duplicateSMActivity.EntryUid = parameters.UserName.Trim();
                        duplicateSMActivity.AppContext = parameters.ApplicationContext.Trim();

                        duplicatedItems.Add(duplicateSMActivity);
                    }

                    ahamOper.GetTable<SMActivity>().InsertAllOnSubmit<SMActivity>(duplicatedItems);
                    ahamOper.SubmitChanges();

                    result.Success = true;
                    result.BrowsablePropertyList = ActivitySetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion
    }
}
