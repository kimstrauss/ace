﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    public class AhamDataServiceBusinessEntities
    {
        #region Manufacturers

        internal static DataAccessResult ManufacturerListGet(Manufacturer manufacturerToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Manufacturer> manufacturers = new HaiBindingList<Manufacturer>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DMfg> mfgs;
                    if (manufacturerToGet == null)
                    {
                        mfgs = ahamOper.DMfgs.ToList<DMfg>();
                    }
                    else
                    {
                        mfgs = new List<DMfg>();
                        mfgs.Add(ahamOper.DMfgs.Single<DMfg>(x => (x.MfgKey==manufacturerToGet.ManufacturerKey)));
                    }

                    foreach (DMfg mfg in mfgs)
                    {
                        Manufacturer manufacturer = new Manufacturer();

                        manufacturer.ManufacturerKey = mfg.MfgKey;
                        manufacturer.ManufacturerAbbreviation = mfg.MfgAbbrev.Trim();
                        manufacturer.ManufacturerName = mfg.MfgName.Trim();

                        if (mfg.URL == null)
                            manufacturer.URL = string.Empty;
                        else
                            manufacturer.URL = mfg.URL.Trim();

                        manufacturer.Billable = Utilities.ConvertByteToBool(mfg.Billable);

                        manufacturer.ApplicationContext = mfg.AppContext.Trim();
                        manufacturer.RowVersion = mfg.RowVersion;
                        manufacturer.UserName = mfg.EntryUid.Trim();

                        manufacturer.MarkOld();
                        manufacturers.AddToList(manufacturer);
                    }

                    result.DataList = manufacturers;
                    result.Success = true;
                    result.BrowsablePropertyList = Manufacturer.GetBrowsablePropertyList();
                }

                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ManufacturerGet(Manufacturer manufacturerToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ManufacturerListGet(manufacturerToGet, parameters);
            if (result.Success)
            {
                Manufacturer manufacturer = (Manufacturer)result.SingleItem;
                result.ItemIsChanged = (manufacturerToGet.RowVersion != manufacturer.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ManufacturerSave(Manufacturer manufacturer, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (manufacturer.IsSavable)
            {
                if (manufacturer.IsDeleted)
                    result = ManufacturerDelete(manufacturer, parameters);
                else if (manufacturer.IsNew)
                    result = ManufacturerInsert(manufacturer, parameters);
                else if (manufacturer.IsDirty)
                    result = ManufacturerUpdate(manufacturer, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        internal static DataAccessResult ManufacturerInsert(Manufacturer manufacturer, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMfg mfg = new DMfg();

                    mfg.MfgAbbrev = manufacturer.ManufacturerAbbreviation.Trim();
                    mfg.MfgName = manufacturer.ManufacturerName.Trim();

                    if ((manufacturer.URL == null) || (manufacturer.URL.Trim() == string.Empty))
                        mfg.URL = null;
                    else
                        mfg.URL = manufacturer.URL.Trim();

                    mfg.Billable = Utilities.ConvertBoolToByte(manufacturer.Billable);

                    mfg.AppContext = parameters.ApplicationContext.Trim();
                    mfg.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DMfg>().InsertOnSubmit(mfg);
                    ahamOper.SubmitChanges();

                    manufacturer.ManufacturerKey = mfg.MfgKey;
                    manufacturer.RowVersion = mfg.RowVersion;

                    manufacturer.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ManufacturerUpdate(Manufacturer manufacturer, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMfg mfg = ahamOper.DMfgs.Single<DMfg>(x => (x.MfgKey == manufacturer.ManufacturerKey));
                    if (mfg.RowVersion != manufacturer.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + manufacturer.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    mfg.MfgAbbrev = manufacturer.ManufacturerAbbreviation.Trim();
                    mfg.MfgName = manufacturer.ManufacturerName.Trim();

                    if (manufacturer.URL.Trim() == string.Empty)
                        mfg.URL = null;
                    else
                        mfg.URL = manufacturer.URL.Trim();

                    mfg.Billable = Utilities.ConvertBoolToByte(manufacturer.Billable);

                    mfg.AppContext = parameters.ApplicationContext.Trim();
                    mfg.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    manufacturer.MarkOld();
                    manufacturer.RowVersion = mfg.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ManufacturerDelete(Manufacturer manufacturer, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMfg mfg = ahamOper.DMfgs.Single<DMfg>(x => (x.MfgKey == manufacturer.ManufacturerKey));
                    if (mfg.RowVersion != manufacturer.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + manufacturer.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    mfg.AppContext = parameters.ApplicationContext.Trim();
                    mfg.EntryUid = parameters.UserName.Trim();

                    ahamOper.DMfgs.DeleteOnSubmit(mfg);
                    ahamOper.SubmitChanges();

                    manufacturer.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region Associations

        internal static DataAccessResult AssociationListGet(Association associationToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Association> associations = new HaiBindingList<Association>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DAssn> assns;

                    if (associationToGet == null)
                    {
                        assns = ahamOper.DAssns.ToList<DAssn>();
                    }
                    else
                    {
                        assns = new List<DAssn>();
                        assns.Add(ahamOper.DAssns.Single<DAssn>(x => (x.AssnKey == associationToGet.AssociationKey)));
                    }

                    foreach (DAssn assn in assns)
                    {
                        Association association = new Association();

                        association.AssociationKey = assn.AssnKey;
                        association.AssociationName = assn.AssnName.Trim();
                        association.AssociationAbbreviation = assn.AssnAbbrev.Trim();

                        association.ApplicationContext = assn.AppContext.Trim();
                        association.UserName = assn.EntryUid.Trim();
                        association.RowVersion = assn.RowVersion;

                        association.MarkOld();
                        associations.AddToList(association);
                    }

                    result.DataList = associations;
                    result.Success = true;
                    result.BrowsablePropertyList = Association.GetBrowsablePropertyList();
                }

                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult AssociationGet(Association associationToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = AssociationListGet(associationToGet, parameters);
            if (result.Success)
            {
                Association association = (Association)result.SingleItem;
                result.ItemIsChanged = (associationToGet.RowVersion != association.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult AssociationSave(Association association, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (association.IsSavable)
            {
                if (association.IsDeleted)
                    result = AssociationDelete(association, parameters);
                else if (association.IsNew)
                    result = AssociationInsert(association, parameters);
                else if (association.IsDirty)
                    result = AssociationUpdate(association, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        internal static DataAccessResult AssociationInsert(Association association, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DAssn assn = new DAssn();

                    assn.AssnAbbrev = association.AssociationAbbreviation.Trim();
                    assn.AssnName = association.AssociationName.Trim();

                    assn.AppContext = parameters.ApplicationContext.Trim();
                    assn.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DAssn>().InsertOnSubmit(assn);
                    ahamOper.SubmitChanges();

                    association.AssociationKey = assn.AssnKey;
                    association.RowVersion = assn.RowVersion;

                    association.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult AssociationUpdate(Association association, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DAssn assn = ahamOper.DAssns.Single<DAssn>(x => (x.AssnKey == association.AssociationKey));
                    if (assn.RowVersion != association.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + association.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    assn.AssnAbbrev = association.AssociationAbbreviation;
                    assn.AssnName = association.AssociationName;

                    assn.AppContext = parameters.ApplicationContext.Trim();
                    assn.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    association.MarkOld();
                    association.RowVersion = assn.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult AssociationDelete(Association association, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DAssn assn = ahamOper.DAssns.Single<DAssn>(x => (x.AssnKey == association.AssociationKey));
                    if (assn.RowVersion != association.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + association.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    assn.AppContext = parameters.ApplicationContext.Trim();
                    assn.EntryUid = parameters.UserName.Trim();

                    ahamOper.DAssns.DeleteOnSubmit(assn);
                    ahamOper.SubmitChanges();

                    association.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion
    }
}
