﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal static class AhamDataServiceCatalog
    {

        #region Catalog

        internal static DataAccessResult CatalogListGet(Catalog catalogToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Catalog> catalogs = new HaiBindingList<Catalog>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DCatalog> DCatalogs;
                    if (catalogToGet == null)
                        DCatalogs = ahamOper.DCatalogs.ToList<DCatalog>();
                    else
                    {
                        DCatalogs = new List<DCatalog>();
                        DCatalogs.Add(ahamOper.DCatalogs.Single<DCatalog>(x => (x.CatalogKey == catalogToGet.CatalogKey)));
                    }

                    foreach (DCatalog dcatalog in DCatalogs)
                    {
                        Catalog catalog = new Catalog();

                        catalog.CatalogAbbreviation = dcatalog.CatalogAbbrev.Trim();
                        catalog.CatalogKey = dcatalog.CatalogKey;
                        catalog.CatalogName = dcatalog.CatalogName.Trim();

                        catalog.ApplicationContext = dcatalog.AppContext.Trim();
                        catalog.RowVersion = dcatalog.RowVersion;
                        catalog.UserName = dcatalog.EntryUid.Trim();

                        catalog.MarkOld();
                        catalogs.AddToList(catalog);
                    }

                    result.DataList = catalogs;
                    result.Success = true;
                    result.BrowsablePropertyList = Catalog.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult CatalogGet(Catalog catalogToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = CatalogListGet(catalogToGet, parameters);
            if (result.Success)
            {
                Catalog catalog = (Catalog)result.SingleItem;
                result.ItemIsChanged = (catalogToGet.RowVersion != catalog.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult CatalogSave(Catalog catalog, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (catalog.IsSavable)
            {
                if (catalog.IsDeleted)
                    result = CatalogDelete(catalog, parameters);
                else if (catalog.IsNew)
                    result = CatalogInsert(catalog, parameters);
                else if (catalog.IsDirty)
                    result = CatalogUpdate(catalog, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult CatalogInsert(Catalog catalog, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DCatalog dcatalog = new DCatalog();
                    dcatalog.CatalogAbbrev = catalog.CatalogAbbreviation.Trim();
                    dcatalog.CatalogName = catalog.CatalogName.Trim();

                    dcatalog.AppContext = parameters.ApplicationContext.Trim();
                    dcatalog.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DCatalog>().InsertOnSubmit(dcatalog);
                    ahamOper.SubmitChanges();

                    catalog.CatalogKey = dcatalog.CatalogKey;
                    catalog.RowVersion = dcatalog.RowVersion;

                    catalog.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult CatalogUpdate(Catalog catalog, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DCatalog dcatalog = ahamOper.DCatalogs.Single<DCatalog>(x => (x.CatalogKey == catalog.CatalogKey));

                    if (dcatalog.RowVersion != catalog.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + catalog.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dcatalog.CatalogAbbrev = catalog.CatalogAbbreviation.Trim();
                    dcatalog.CatalogName = catalog.CatalogName.Trim();

                    dcatalog.AppContext = parameters.ApplicationContext.Trim();
                    dcatalog.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    catalog.MarkOld();
                    catalog.RowVersion = dcatalog.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult CatalogDelete(Catalog catalog, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DCatalog dcatalog = ahamOper.DCatalogs.Single<DCatalog>(x => (x.CatalogKey == catalog.CatalogKey));

                    if (dcatalog.RowVersion != catalog.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + catalog.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dcatalog.AppContext = parameters.ApplicationContext.Trim();
                    dcatalog.EntryUid = parameters.UserName.Trim();

                    ahamOper.DCatalogs.DeleteOnSubmit(dcatalog);
                    ahamOper.SubmitChanges();

                    catalog.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region CatalogSet

        internal static DataAccessResult CatalogSetListGet(CatalogSet catalogSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<CatalogSet> catalogSetList = new HaiBindingList<CatalogSet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SDCatalog> SDCatalogs;
                    if (catalogSetToGet == null)
                        SDCatalogs = ahamOper.SDCatalogs.ToList<SDCatalog>();
                    else
                    {
                        SDCatalogs = new List<SDCatalog>();
                        SDCatalogs.Add(ahamOper.SDCatalogs.Single<SDCatalog>(x => (x.CatalogSetKey == catalogSetToGet.CatalogSetKey)));
                    }

                    foreach (SDCatalog sdcatalog in SDCatalogs)
                    {
                        CatalogSet catalogSet = new CatalogSet();

                        catalogSet.CatalogSetAbbreviation = sdcatalog.CatalogSetAbbrev.Trim();
                        catalogSet.CatalogSetKey = sdcatalog.CatalogSetKey;
                        catalogSet.CatalogSetName = sdcatalog.CatalogSetName.Trim();

                        catalogSet.ApplicationContext = sdcatalog.AppContext.Trim();
                        catalogSet.RowVersion = sdcatalog.RowVersion;
                        catalogSet.UserName = sdcatalog.EntryUid.Trim();

                        catalogSet.MarkOld();
                        catalogSetList.AddToList(catalogSet);
                    }

                    result.DataList = catalogSetList;
                    result.Success = true;
                    result.BrowsablePropertyList = CatalogSet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult CatalogSetGet(CatalogSet catalogSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = CatalogSetListGet(catalogSetToGet, parameters);
            if (result.Success)
            {
                CatalogSet catalogSet = (CatalogSet)result.SingleItem;
                result.ItemIsChanged = (catalogSetToGet.RowVersion != catalogSet.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult CatalogSetSave(CatalogSet catalogSet, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (catalogSet.IsSavable)
            {
                if (catalogSet.IsDeleted)
                    result = CatalogSetDelete(catalogSet, parameters);
                else if (catalogSet.IsNew)
                    result = CatalogSetInsert(catalogSet, parameters);
                else if (catalogSet.IsDirty)
                    result = CatalogSetUpdate(catalogSet, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult CatalogSetInsert(CatalogSet catalogSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDCatalog sdcatalog = new SDCatalog();

                    sdcatalog.CatalogSetAbbrev = catalogSet.CatalogSetAbbreviation.Trim();
                    sdcatalog.CatalogSetName = catalogSet.CatalogSetName.Trim();

                    sdcatalog.AppContext = parameters.ApplicationContext.Trim();
                    sdcatalog.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SDCatalog>().InsertOnSubmit(sdcatalog);
                    ahamOper.SubmitChanges();

                    catalogSet.CatalogSetKey = sdcatalog.CatalogSetKey;
                    catalogSet.RowVersion = sdcatalog.RowVersion;

                    catalogSet.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult CatalogSetUpdate(CatalogSet catalogSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDCatalog sdcatalog = ahamOper.SDCatalogs.Single<SDCatalog>(x => (x.CatalogSetKey == catalogSet.CatalogSetKey));

                    if (sdcatalog.RowVersion != catalogSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + catalogSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdcatalog.CatalogSetAbbrev = catalogSet.CatalogSetAbbreviation.Trim();
                    sdcatalog.CatalogSetName = catalogSet.CatalogSetName.Trim();

                    sdcatalog.AppContext = parameters.ApplicationContext.Trim();
                    sdcatalog.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    catalogSet.MarkOld();
                    catalogSet.RowVersion = sdcatalog.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult CatalogSetDelete(CatalogSet catalogSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDCatalog sdcatalog = ahamOper.SDCatalogs.Single<SDCatalog>(x => (x.CatalogSetKey == catalogSet.CatalogSetKey));

                    if (sdcatalog.RowVersion != catalogSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + catalogSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdcatalog.AppContext = parameters.ApplicationContext.Trim();
                    sdcatalog.EntryUid = parameters.UserName.Trim();

                    ahamOper.SDCatalogs.DeleteOnSubmit(sdcatalog);
                    ahamOper.SubmitChanges();

                    catalogSet.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region CatalogSetMember

        internal static DataAccessResult CatalogSetMemberListGet(CatalogSetMember catalogSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<CatalogSetMember> catalogSetMemberList = new HaiBindingList<CatalogSetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMCatalog> SMCatalogs;
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<SMCatalog>(x => x.DCatalog);
                    dlo.LoadWith<SMCatalog>(x => x.SDCatalog);
                    ahamOper.LoadOptions = dlo;

                    if (catalogSetMemberToGet == null)
                    {
                        SMCatalogs = ahamOper.SMCatalogs.ToList<SMCatalog>();
                    }
                    else
                    {
                        SMCatalogs = new List<SMCatalog>();
                        SMCatalogs.Add(ahamOper.SMCatalogs.Single<SMCatalog>(x => (x.CatalogSetMemberKey == catalogSetMemberToGet.CatalogSetMemberKey)));
                    }

                    foreach (SMCatalog smcatalog in SMCatalogs)
                    {
                        CatalogSetMember catalogSetMember = new CatalogSetMember();

                        catalogSetMember.CatalogKey = smcatalog.CatalogKey;
                        catalogSetMember.CatalogSetKey = smcatalog.CatalogSetKey;
                        catalogSetMember.CatalogSetMemberKey = smcatalog.CatalogSetMemberKey;
                        catalogSetMember.DateRange.BeginDate = smcatalog.BeginDate.Date;
                        catalogSetMember.DateRange.EndDate = smcatalog.EndDate.Date;

                        catalogSetMember.CatalogName = smcatalog.DCatalog.CatalogName.Trim();
                        catalogSetMember.CatalogAbbreviation = smcatalog.DCatalog.CatalogAbbrev.Trim();
                        catalogSetMember.CatalogSetAbbreviation = smcatalog.SDCatalog.CatalogSetAbbrev.Trim();
                        catalogSetMember.CatalogSetName = smcatalog.SDCatalog.CatalogSetName.Trim();

                        catalogSetMember.ApplicationContext = smcatalog.AppContext.Trim();
                        catalogSetMember.RowVersion = smcatalog.RowVersion;
                        catalogSetMember.UserName = smcatalog.EntryUid.Trim();

                        catalogSetMember.MarkOld();
                        catalogSetMemberList.AddToList(catalogSetMember);
                    }

                    result.DataList = catalogSetMemberList;
                    result.Success = true;
                    result.BrowsablePropertyList = CatalogSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult CatalogSetMemberGet(CatalogSetMember catalogSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = CatalogSetMemberListGet(catalogSetMemberToGet, parameters);
            if (result.Success)
            {
                CatalogSetMember catalogSetMember = (CatalogSetMember)result.SingleItem;
                result.ItemIsChanged = (catalogSetMemberToGet.RowVersion != catalogSetMember.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult CatalogSetMemberSave(CatalogSetMember catalogSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (catalogSetMember.IsSavable)
            {
                if (catalogSetMember.IsDeleted)
                    result = CatalogSetMemberDelete(catalogSetMember, parameters);
                else if (catalogSetMember.IsNew)
                    result = CatalogSetMemberInsert(catalogSetMember, parameters);
                else if (catalogSetMember.IsDirty)
                    result = CatalogSetMemberUpdate(catalogSetMember, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult CatalogSetMemberInsert(CatalogSetMember catalogSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMCatalog smcatalog = new SMCatalog();
                    smcatalog.CatalogKey = catalogSetMember.CatalogKey;
                    smcatalog.CatalogSetKey = catalogSetMember.CatalogSetKey;
                    smcatalog.BeginDate = catalogSetMember.DateRange.BeginDate.Date;
                    smcatalog.EndDate = catalogSetMember.DateRange.EndDate.Date;

                    smcatalog.AppContext = parameters.ApplicationContext.Trim();
                    smcatalog.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SMCatalog>().InsertOnSubmit(smcatalog);
                    ahamOper.SubmitChanges();

                    catalogSetMember.CatalogSetMemberKey = smcatalog.CatalogSetMemberKey;
                    catalogSetMember.RowVersion = smcatalog.RowVersion;

                    catalogSetMember.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult CatalogSetMemberUpdate(CatalogSetMember catalogSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMCatalog smcatalog = ahamOper.SMCatalogs.Single<SMCatalog>(x => (x.CatalogSetMemberKey == catalogSetMember.CatalogSetMemberKey));

                    if (smcatalog.RowVersion != catalogSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + catalogSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smcatalog.CatalogKey = catalogSetMember.CatalogKey;
                    smcatalog.CatalogSetKey = catalogSetMember.CatalogSetKey;
                    smcatalog.BeginDate = catalogSetMember.DateRange.BeginDate.Date;
                    smcatalog.EndDate = catalogSetMember.DateRange.EndDate.Date;

                    smcatalog.AppContext = parameters.ApplicationContext.Trim();
                    smcatalog.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    catalogSetMember.MarkOld();
                    catalogSetMember.RowVersion = smcatalog.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult CatalogSetMemberDelete(CatalogSetMember catalogSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMCatalog smcatalog = ahamOper.SMCatalogs.Single<SMCatalog>(x => (x.CatalogSetMemberKey == catalogSetMember.CatalogSetMemberKey));

                    if (smcatalog.RowVersion != catalogSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + catalogSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smcatalog.AppContext = parameters.ApplicationContext.Trim();
                    smcatalog.EntryUid = parameters.UserName.Trim();

                    ahamOper.SMCatalogs.DeleteOnSubmit(smcatalog);
                    ahamOper.SubmitChanges();

                    catalogSetMember.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult CatalogSetMembersDuplicate(CatalogSet sourceCatalogSet, CatalogSet targetCatalogSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMCatalog> SMCatalogs;
                    List<SMCatalog> duplicatedItems = new List<SMCatalog>();

                    SMCatalogs = ahamOper.SMCatalogs.Where(x => (x.CatalogSetKey == sourceCatalogSet.CatalogSetKey)).ToList<SMCatalog>();

                    foreach (SMCatalog smCatalog in SMCatalogs)
                    {
                        SMCatalog duplicateSMCatalog = new SMCatalog();

                        duplicateSMCatalog.CatalogSetKey = targetCatalogSet.CatalogSetKey;
                        duplicateSMCatalog.CatalogKey = smCatalog.CatalogKey;
                        duplicateSMCatalog.BeginDate = smCatalog.BeginDate.Date;
                        duplicateSMCatalog.EndDate = smCatalog.EndDate.Date;
                        duplicateSMCatalog.EntryUid = parameters.UserName.Trim();
                        duplicateSMCatalog.AppContext = parameters.ApplicationContext.Trim();

                        duplicatedItems.Add(duplicateSMCatalog);
                    }

                    ahamOper.GetTable<SMCatalog>().InsertAllOnSubmit<SMCatalog>(duplicatedItems);
                    ahamOper.SubmitChanges();

                    result.Success = true;
                    result.BrowsablePropertyList = CatalogSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

    }
}
