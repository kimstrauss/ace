﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    public class AhamDataServiceCellDisclosure
    {
        #region Methods that are to be called from AhamDataService

        internal static DataAccessResult CellDisclosureListGet(CellDisclosure cellDisclosureToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<CellDisclosure> cellDisclosures = new HaiBindingList<CellDisclosure>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    int key;
                    if (cellDisclosureToGet == null)
                        key = 0;
                    else
                        key = cellDisclosureToGet.CellDisclosureKey;

                    var cellDisclosureResult = ahamOper.ceDCellDisclosureLookup(0, 0, 0, key);
                    foreach (ceDCellDisclosureLookupResult cellDisclosureRs in cellDisclosureResult)
                    {
                        CellDisclosure cellDisclosure = new CellDisclosure();

                        cellDisclosure.CellDisclosureKey = cellDisclosureRs.celldiscKey;
                        cellDisclosure.DateRange.BeginDate = cellDisclosureRs.begindate.Date;
                        cellDisclosure.DateRange.EndDate = cellDisclosureRs.enddate.Date;
                        cellDisclosure.OutputReportID = cellDisclosureRs.outputreportid.Trim();
                        cellDisclosure.OutputReportKey = cellDisclosureRs.outputreportkey;
                        cellDisclosure.IndustryMemberName = cellDisclosureRs.RIndustryMemberName.Trim();
                        cellDisclosure.IndustryMemberKey = cellDisclosureRs.rindustrymemberkey;
                        cellDisclosure.SourceSizeSetMemberName = cellDisclosureRs.SourceSizeSetName.Trim();
                        cellDisclosure.SourceSizeSetMemberKey = cellDisclosureRs.sourceSizeSetMemberKey;
                        cellDisclosure.DestinationSizeSetMemberName = cellDisclosureRs.DestSizeSetName.Trim();
                        cellDisclosure.DestinationSizeSetMemberKey = cellDisclosureRs.DestSizesetmemberkey;
                        cellDisclosure.Footnote = cellDisclosureRs.footnote.Trim();
                        cellDisclosure.FootnoteKey = cellDisclosureRs.footnotekey;

                        cellDisclosure.RowVersion = cellDisclosureRs.RowVersion;
                        cellDisclosure.ApplicationContext = cellDisclosureRs.AppContext.Trim();
                        cellDisclosure.UserName = cellDisclosureRs.EntryUid.Trim();

                        cellDisclosure.MarkOld();
                        cellDisclosures.AddToList(cellDisclosure);
                    }

                    cellDisclosureResult.Dispose();

                    result.DataList = cellDisclosures;
                    result.Success = true;
                    result.BrowsablePropertyList = CellDisclosure.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult CellDisclosureGet(CellDisclosure cellDisclosureToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = CellDisclosureListGet(cellDisclosureToGet, parameters);
            if (result.Success)
            {
                CellDisclosure cellDisclosure = (CellDisclosure)result.SingleItem;
                result.ItemIsChanged = (cellDisclosure.RowVersion != cellDisclosureToGet.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult CellDisclosureSave(CellDisclosure cellDisclosure, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (cellDisclosure.IsSavable)
            {
                if (cellDisclosure.IsDeleted)
                    result = CellDisclosureDelete(cellDisclosure, parameters);
                else if (cellDisclosure.IsNew)
                    result = CellDisclosureInsert(cellDisclosure, parameters);
                else if (cellDisclosure.IsDirty)
                    result = CellDisclosureUpdate(cellDisclosure, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        internal static DataAccessResult CellDisclosureUpdate(CellDisclosure cellDisclosure, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DCellDisclosure dcellDisclosure = ahamOper.DCellDisclosures.Single<DCellDisclosure>(x => (x.celldiscKey == cellDisclosure.CellDisclosureKey));

                    if (dcellDisclosure.RowVersion != cellDisclosure.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + cellDisclosure.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dcellDisclosure.begindate = cellDisclosure.DateRange.BeginDate.Date;
                    dcellDisclosure.enddate = cellDisclosure.DateRange.EndDate.Date;
                    dcellDisclosure.DestSizesetmemberkey = cellDisclosure.DestinationSizeSetMemberKey;
                    dcellDisclosure.FootnoteKey = cellDisclosure.FootnoteKey;
                    dcellDisclosure.outputreportkey = cellDisclosure.OutputReportKey;
                    dcellDisclosure.rindustrymemberkey = cellDisclosure.IndustryMemberKey;
                    dcellDisclosure.sourceSizeSetMemberKey = cellDisclosure.SourceSizeSetMemberKey;

                    dcellDisclosure.EntryUid = parameters.UserName.Trim();
                    dcellDisclosure.AppContext = parameters.ApplicationContext.Trim();

                    ahamOper.SubmitChanges();

                    cellDisclosure.MarkOld();
                    cellDisclosure.RowVersion = dcellDisclosure.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult CellDisclosureInsert(CellDisclosure cellDisclosure, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DCellDisclosure dcellDisclosure = new DCellDisclosure();

                    dcellDisclosure.begindate = cellDisclosure.DateRange.BeginDate.Date;
                    dcellDisclosure.enddate = cellDisclosure.DateRange.EndDate.Date;
                    dcellDisclosure.DestSizesetmemberkey = cellDisclosure.DestinationSizeSetMemberKey;
                    dcellDisclosure.FootnoteKey = cellDisclosure.FootnoteKey;
                    dcellDisclosure.outputreportkey = cellDisclosure.OutputReportKey;
                    dcellDisclosure.rindustrymemberkey = cellDisclosure.IndustryMemberKey;
                    dcellDisclosure.sourceSizeSetMemberKey = cellDisclosure.SourceSizeSetMemberKey;

                    dcellDisclosure.EntryUid = parameters.UserName.Trim();
                    dcellDisclosure.AppContext = parameters.ApplicationContext.Trim();

                    ahamOper.GetTable<DCellDisclosure>().InsertOnSubmit(dcellDisclosure);
                    ahamOper.SubmitChanges();

                    cellDisclosure.CellDisclosureKey = dcellDisclosure.celldiscKey;
                    cellDisclosure.RowVersion = dcellDisclosure.RowVersion;

                    cellDisclosure.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult CellDisclosureDelete(CellDisclosure cellDisclosure, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DCellDisclosure dcellDisclosure = ahamOper.DCellDisclosures.Single<DCellDisclosure>(x => (x.celldiscKey==cellDisclosure.CellDisclosureKey));

                    if (dcellDisclosure.RowVersion != cellDisclosure.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + cellDisclosure.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dcellDisclosure.AppContext = parameters.ApplicationContext.Trim();
                    dcellDisclosure.EntryUid = parameters.UserName.Trim();

                    ahamOper.DCellDisclosures.DeleteOnSubmit(dcellDisclosure);
                    ahamOper.SubmitChanges();

                    cellDisclosure.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }



        internal static DataAccessResult FootnoteGet(Footnote footnoteToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = FootnoteListGet(footnoteToGet, parameters);
            if (result.Success)
            {
                Footnote footnote = (Footnote)result.SingleItem;
                result.ItemIsChanged = (footnoteToGet.RowVersion != footnote.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult FootnoteSave(Footnote footnote, DataServiceParameters parameters)
        {
            DataAccessResult result;

            if (footnote.IsSavable)
            {
                if (footnote.IsDeleted)
                    result = FootnoteDelete(footnote, parameters);
                else if (footnote.IsNew)
                    result = FootnoteInsert(footnote, parameters);
                else if (footnote.IsDirty)
                    result = FootnoteUpdate(footnote, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        internal static DataAccessResult FootnoteUpdate(Footnote footnote, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    dFootnote dfootnote = ahamOper.dFootnotes.Single<dFootnote>(x => (x.FootnoteKey == footnote.FootnoteKey));

                    if (dfootnote.RowVersion != footnote.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + footnote.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dfootnote.Footnote = footnote.FootnoteText.Trim();

                    dfootnote.AppContext = parameters.ApplicationContext.Trim();
                    dfootnote.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    footnote.MarkOld();
                    footnote.RowVersion = dfootnote.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult FootnoteInsert(Footnote footnote, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    dFootnote dfootnote = new dFootnote();
                    dfootnote.Footnote = footnote.FootnoteText;

                    dfootnote.AppContext = parameters.ApplicationContext.Trim();
                    dfootnote.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<dFootnote>().InsertOnSubmit(dfootnote);
                    ahamOper.SubmitChanges();

                    footnote.FootnoteKey = dfootnote.FootnoteKey;
                    footnote.RowVersion = dfootnote.RowVersion;

                    footnote.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult FootnoteDelete(Footnote footnote, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    dFootnote dfootnote = ahamOper.dFootnotes.Single<dFootnote>(x => (x.FootnoteKey == footnote.FootnoteKey));

                    if (dfootnote.RowVersion != footnote.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + footnote.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dfootnote.AppContext = parameters.ApplicationContext.Trim();
                    dfootnote.EntryUid = parameters.UserName.Trim();

                    ahamOper.dFootnotes.DeleteOnSubmit(dfootnote);
                    ahamOper.SubmitChanges();

                    footnote.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region Support methods that can be called directly from AhamDataServiceCellDisclosure

        public static DataAccessResult CellDisclosureLikeListGet(CellDisclosure likeThisCellDisclosure, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<CellDisclosure> cellDisclosures = new HaiBindingList<CellDisclosure>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    var cellDisclosureResult = ahamOper.ceDCellDisclosureLookup(likeThisCellDisclosure.OutputReportKey , likeThisCellDisclosure.IndustryMemberKey, likeThisCellDisclosure.SourceSizeSetMemberKey, 0);
                    foreach (ceDCellDisclosureLookupResult cellDisclosureRs in cellDisclosureResult)
                    {
                        CellDisclosure cellDisclosure = new CellDisclosure();

                        cellDisclosure.CellDisclosureKey = cellDisclosureRs.celldiscKey;
                        cellDisclosure.DateRange.BeginDate = cellDisclosureRs.begindate.Date;
                        cellDisclosure.DateRange.EndDate = cellDisclosureRs.enddate.Date;
                        cellDisclosure.OutputReportID = cellDisclosureRs.outputreportid.Trim();
                        cellDisclosure.OutputReportKey = cellDisclosureRs.outputreportkey;
                        cellDisclosure.IndustryMemberName = cellDisclosureRs.RIndustryMemberName.Trim();
                        cellDisclosure.IndustryMemberKey = cellDisclosureRs.rindustrymemberkey;
                        cellDisclosure.SourceSizeSetMemberName = cellDisclosureRs.SourceSizeSetName.Trim();
                        cellDisclosure.SourceSizeSetMemberKey = cellDisclosureRs.sourceSizeSetMemberKey;
                        cellDisclosure.DestinationSizeSetMemberName = cellDisclosureRs.DestSizeSetName.Trim();
                        cellDisclosure.DestinationSizeSetMemberKey = cellDisclosureRs.DestSizesetmemberkey;
                        cellDisclosure.Footnote = cellDisclosureRs.footnote.Trim();
                        cellDisclosure.FootnoteKey = cellDisclosureRs.footnotekey;

                        cellDisclosure.RowVersion = cellDisclosureRs.RowVersion;
                        cellDisclosure.ApplicationContext = cellDisclosureRs.AppContext;
                        cellDisclosure.UserName = cellDisclosureRs.EntryUid;

                        cellDisclosure.MarkOld();
                        cellDisclosures.AddToList(cellDisclosure);
                    }

                    cellDisclosureResult.Dispose();

                    result.DataList = cellDisclosures;
                    result.Success = true;
                    result.BrowsablePropertyList = CellDisclosure.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        public static DataAccessResult CharacteristicsOutputReportListGet(OutputReport outputReportToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<OutputReport> outputReports = new HaiBindingList<OutputReport>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    int key;
                    if (outputReportToGet == null)
                        key = 0;
                    else
                        key = outputReportToGet.OutputReportKey;
                    int characteristicsReports = 3;
                    var outputReportResult = ahamOper.ceOutputReportLookup(key, characteristicsReports);
                    foreach (ceOutputReportLookupResult outputReportRs in outputReportResult)
                    {
                        OutputReport outputReport = new OutputReport();

                        outputReport.OutputReportKey = outputReportRs.outputreportkey;
                        outputReport.OutputReportID = outputReportRs.outputreportid.Trim();

                        outputReport.RowVersion = outputReportRs.RowVersion;
                        outputReport.ApplicationContext = outputReportRs.AppContext;
                        outputReport.UserName = outputReportRs.EntryUid;

                        outputReport.MarkOld();
                        outputReports.AddToList(outputReport);
                    }

                    outputReportResult.Dispose();

                    result.DataList = outputReports;
                    result.Success = true;
                    result.BrowsablePropertyList = OutputReport.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        public static DataAccessResult OutputReportListGet(OutputReport outputReportToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<OutputReport> outputReports = new HaiBindingList<OutputReport>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    int key;
                    if (outputReportToGet == null)
                        key = 0;
                    else
                        key = outputReportToGet.OutputReportKey;
                    int allReports = 0;
                    var outputReportResult = ahamOper.ceOutputReportLookup(key, allReports);
                    foreach (ceOutputReportLookupResult outputReportRs in outputReportResult)
                    {
                        OutputReport outputReport = new OutputReport();

                        outputReport.OutputReportKey = outputReportRs.outputreportkey;
                        outputReport.OutputReportID = outputReportRs.outputreportid.Trim();

                        outputReport.RowVersion = outputReportRs.RowVersion;
                        outputReport.ApplicationContext = outputReportRs.AppContext;
                        outputReport.UserName = outputReportRs.EntryUid;

                        outputReport.MarkOld();
                        outputReports.AddToList(outputReport);
                    }

                    outputReportResult.Dispose();

                    result.DataList = outputReports;
                    result.Success = true;
                    result.BrowsablePropertyList = OutputReport.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        public static DataAccessResult IndustryReportMemberForOutputListGet(IndustryReportMemberForOutput industryReportMemberForOutputToGet, DataServiceParameters parameters, int outputReportKey)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<IndustryReportMemberForOutput> industryReportMembers = new HaiBindingList<IndustryReportMemberForOutput>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    int key;
                    if (industryReportMemberForOutputToGet == null)
                        key = 0;
                    else
                        key = industryReportMemberForOutputToGet.IndustryReportMemberForOutputKey;

                    var industryNameResult = ahamOper.ceRindustryMembersLookup(outputReportKey, 0);
                    foreach (ceRindustryMembersLookupResult industryReportMemberRs in industryNameResult)
                    {
                        IndustryReportMemberForOutput industryReportMemberForOutput = new IndustryReportMemberForOutput();

                        industryReportMemberForOutput.IndustryReportMemberForOutputKey = industryReportMemberRs.RIndustryMemberKey;
                        industryReportMemberForOutput.IndustryReportMemberForOutputName = industryReportMemberRs.RindustryMemberName.Trim();

                        industryReportMemberForOutput.RowVersion = industryReportMemberRs.RowVersion;
                        industryReportMemberForOutput.ApplicationContext = industryReportMemberRs.AppContext;
                        industryReportMemberForOutput.UserName = industryReportMemberRs.EntryUid;

                        industryReportMemberForOutput.MarkOld();
                        industryReportMembers.AddToList(industryReportMemberForOutput);
                    }

                    industryNameResult.Dispose();

                    result.DataList = industryReportMembers;
                    result.Success = true;
                    result.BrowsablePropertyList = IndustryReportMemberForOutput.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        public static DataAccessResult SourceSizeSetMemberListGet(SizeSetMember sourceSizeSetMemberToGet, DataServiceParameters parameters, int outputReportKey, int industryMemberKey)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<SizeSetMember> sizeSetMembers = new HaiBindingList<SizeSetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    int key;
                    if (sourceSizeSetMemberToGet == null)
                        key = 0;
                    else
                        key = sourceSizeSetMemberToGet.SizeSetMemberKey;

                    var sizeSetMemberResult = ahamOper.ceSourceSizeSetMemberLookup(outputReportKey, industryMemberKey, key);
                    foreach (ceSourceSizeSetMemberLookupResult sizeSetMembersRs in sizeSetMemberResult)
                    {
                        SizeSetMember sizeSetMember = new SizeSetMember();

                        sizeSetMember.SizeSetMemberName = sizeSetMembersRs.SourceSizeSetName.Trim();
                        sizeSetMember.SizeSetMemberKey = sizeSetMembersRs.sourceSizeSetMemberKey;

                        sizeSetMember.RowVersion = sizeSetMembersRs.RowVersion;
                        sizeSetMember.ApplicationContext = sizeSetMembersRs.AppContext;
                        sizeSetMember.UserName = sizeSetMembersRs.EntryUid;

                        sizeSetMember.MarkOld();
                        sizeSetMembers.AddToList(sizeSetMember);
                    }

                    sizeSetMemberResult.Dispose();

                    result.DataList = sizeSetMembers;
                    result.Success = true;
                    result.BrowsablePropertyList = SizeSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        public static DataAccessResult DestinationSizeSetMemberListGet(SizeSetMember destinationSizeSetMemberToGet, DataServiceParameters parameters, int outputReportKey, int industryMemberKey, int sourceSizeSetMemberKey)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<SizeSetMember> sizeSetMembers = new HaiBindingList<SizeSetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    int key;
                    if (destinationSizeSetMemberToGet == null)
                        key = 0;
                    else
                        key = destinationSizeSetMemberToGet.SizeSetMemberKey;

                    var sizeSetMemberResult = ahamOper.ceDestSizeSetMemberLookup(outputReportKey, industryMemberKey, sourceSizeSetMemberKey, key);
                    foreach (ceDestSizeSetMemberLookupResult sizeSetMembersRs in sizeSetMemberResult)
                    {
                        SizeSetMember sizeSetMember = new SizeSetMember();

                        sizeSetMember.SizeSetMemberName = sizeSetMembersRs.DestSizeSetName.Trim();
                        sizeSetMember.SizeSetMemberKey = sizeSetMembersRs.destSizeSetMemberKey;

                        sizeSetMember.RowVersion = sizeSetMembersRs.RowVersion;
                        sizeSetMember.ApplicationContext = sizeSetMembersRs.AppContext;
                        sizeSetMember.UserName = sizeSetMembersRs.EntryUid;

                        sizeSetMember.MarkOld();
                        sizeSetMembers.AddToList(sizeSetMember);
                    }

                    sizeSetMemberResult.Dispose();

                    result.DataList = sizeSetMembers;
                    result.Success = true;
                    result.BrowsablePropertyList = SizeSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        public static DataAccessResult FootnoteListGet(Footnote footnoteToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Footnote> footnotes = new HaiBindingList<Footnote>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<dFootnote> DFootnotes;
                    if (footnoteToGet == null)
                    {
                        DFootnotes = ahamOper.dFootnotes.ToList<dFootnote>();
                    }
                    else
                    {
                        DFootnotes = new List<dFootnote>();
                        DFootnotes.Add(ahamOper.dFootnotes.Single<dFootnote>(x => (x.FootnoteKey==footnoteToGet.FootnoteKey)));
                    }

                    foreach (dFootnote dfootnote in DFootnotes)
                    {
                        Footnote footnote = new Footnote();

                        footnote.FootnoteKey = dfootnote.FootnoteKey;
                        footnote.FootnoteText = dfootnote.Footnote.Trim();

                        footnote.ApplicationContext = dfootnote.AppContext;
                        footnote.RowVersion = dfootnote.RowVersion;
                        footnote.UserName = dfootnote.EntryUid;

                        footnote.MarkOld();
                        footnotes.AddToList(footnote);
                    }

                    result.DataList = footnotes;
                    result.Success = true;
                    result.BrowsablePropertyList = Footnote.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion
    }
}
