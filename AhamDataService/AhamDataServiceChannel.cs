﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal static class AhamDataServiceChannel
    {

        #region Channel

        internal static DataAccessResult ChannelListGet(Channel channelToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Channel> channels = new HaiBindingList<Channel>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DChannel> DChannels;
                    if (channelToGet == null)
                        DChannels = ahamOper.DChannels.ToList<DChannel>();
                    else
                    {
                        DChannels = new List<DChannel>();
                        DChannels.Add(ahamOper.DChannels.Single<DChannel>(x => (x.ChannelKey == channelToGet.ChannelKey)));
                    }

                    foreach (DChannel dchannel in DChannels)
                    {
                        Channel channel = new Channel();

                        channel.ChannelAbbreviation = dchannel.ChannelAbbrev.Trim();
                        channel.ChannelKey = dchannel.ChannelKey;
                        channel.ChannelName = dchannel.ChannelName.Trim();

                        channel.ApplicationContext = dchannel.AppContext.Trim();
                        channel.RowVersion = dchannel.RowVersion;
                        channel.UserName = dchannel.EntryUid.Trim();

                        channel.MarkOld();
                        channels.AddToList(channel);
                    }

                    result.DataList = channels;
                    result.Success = true;
                    result.BrowsablePropertyList = Channel.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ChannelGet(Channel channelToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ChannelListGet(channelToGet, parameters);
            if (result.Success)
            {
                Channel channel = (Channel)result.SingleItem;
                result.ItemIsChanged = (channelToGet.RowVersion != channel.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ChannelSave(Channel channel, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (channel.IsSavable)
            {
                if (channel.IsDeleted)
                    result = ChannelDelete(channel, parameters);
                else if (channel.IsNew)
                    result = ChannelInsert(channel, parameters);
                else if (channel.IsDirty)
                    result = ChannelUpdate(channel, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ChannelInsert(Channel channel, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DChannel dchannel = new DChannel();
                    dchannel.ChannelAbbrev = channel.ChannelAbbreviation.Trim();
                    dchannel.ChannelName = channel.ChannelName.Trim();

                    dchannel.AppContext = parameters.ApplicationContext.Trim();
                    dchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DChannel>().InsertOnSubmit(dchannel);
                    ahamOper.SubmitChanges();

                    channel.ChannelKey = dchannel.ChannelKey;
                    channel.RowVersion = dchannel.RowVersion;

                    channel.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ChannelUpdate(Channel channel, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DChannel dchannel = ahamOper.DChannels.Single<DChannel>(x => (x.ChannelKey == channel.ChannelKey));

                    if (dchannel.RowVersion != channel.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + channel.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dchannel.ChannelAbbrev = channel.ChannelAbbreviation.Trim();
                    dchannel.ChannelName = channel.ChannelName.Trim();

                    dchannel.AppContext = parameters.ApplicationContext.Trim();
                    dchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    channel.MarkOld();
                    channel.RowVersion = dchannel.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ChannelDelete(Channel channel, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DChannel dchannel = ahamOper.DChannels.Single<DChannel>(x => (x.ChannelKey == channel.ChannelKey));

                    if (dchannel.RowVersion != channel.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + channel.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dchannel.AppContext = parameters.ApplicationContext.Trim();
                    dchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.DChannels.DeleteOnSubmit(dchannel);
                    ahamOper.SubmitChanges();

                    channel.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region ChannelSet

        internal static DataAccessResult ChannelSetListGet(ChannelSet channelSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ChannelSet> channelSetList = new HaiBindingList<ChannelSet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SDChannel> SDChannels;
                    if (channelSetToGet == null)
                        SDChannels = ahamOper.SDChannels.ToList<SDChannel>();
                    else
                    {
                        SDChannels = new List<SDChannel>();
                        SDChannels.Add(ahamOper.SDChannels.Single<SDChannel>(x => (x.ChannelSetKey == channelSetToGet.ChannelSetKey)));
                    }

                    foreach (SDChannel sdchannel in SDChannels)
                    {
                        ChannelSet channelSet = new ChannelSet();

                        channelSet.ChannelSetAbbreviation = sdchannel.ChannelSetAbbrev.Trim();
                        channelSet.ChannelSetKey = sdchannel.ChannelSetKey;
                        channelSet.ChannelSetName = sdchannel.ChannelSetName.Trim();

                        channelSet.ApplicationContext = sdchannel.AppContext.Trim();
                        channelSet.RowVersion = sdchannel.RowVersion;
                        channelSet.UserName = sdchannel.EntryUid.Trim();

                        channelSet.MarkOld();
                        channelSetList.AddToList(channelSet);
                    }

                    result.DataList = channelSetList;
                    result.Success = true;
                    result.BrowsablePropertyList = ChannelSet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ChannelSetGet(ChannelSet channelSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ChannelSetListGet(channelSetToGet, parameters);
            if (result.Success)
            {
                ChannelSet channelSet = (ChannelSet)result.SingleItem;
                result.ItemIsChanged = (channelSetToGet.RowVersion != channelSet.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ChannelSetSave(ChannelSet channelSet, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (channelSet.IsSavable)
            {
                if (channelSet.IsDeleted)
                    result = ChannelSetDelete(channelSet, parameters);
                else if (channelSet.IsNew)
                    result = ChannelSetInsert(channelSet, parameters);
                else if (channelSet.IsDirty)
                    result = ChannelSetUpdate(channelSet, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ChannelSetInsert(ChannelSet channelSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDChannel sdchannel = new SDChannel();
                    sdchannel.ChannelSetAbbrev = channelSet.ChannelSetAbbreviation.Trim();
                    sdchannel.ChannelSetName = channelSet.ChannelSetName.Trim();

                    sdchannel.AppContext = parameters.ApplicationContext.Trim();
                    sdchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SDChannel>().InsertOnSubmit(sdchannel);
                    ahamOper.SubmitChanges();

                    channelSet.ChannelSetKey = sdchannel.ChannelSetKey;
                    channelSet.RowVersion = sdchannel.RowVersion;

                    channelSet.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ChannelSetUpdate(ChannelSet channelSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDChannel sdchannel = ahamOper.SDChannels.Single<SDChannel>(x => (x.ChannelSetKey == channelSet.ChannelSetKey));

                    if (sdchannel.RowVersion != channelSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + channelSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdchannel.ChannelSetAbbrev = channelSet.ChannelSetAbbreviation.Trim();
                    sdchannel.ChannelSetName = channelSet.ChannelSetName.Trim();

                    sdchannel.AppContext = parameters.ApplicationContext.Trim();
                    sdchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    channelSet.MarkOld();
                    channelSet.RowVersion = sdchannel.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ChannelSetDelete(ChannelSet channelSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDChannel sdchannel = ahamOper.SDChannels.Single<SDChannel>(x => (x.ChannelSetKey == channelSet.ChannelSetKey));

                    if (sdchannel.RowVersion != channelSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + channelSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdchannel.AppContext = parameters.ApplicationContext.Trim();
                    sdchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.SDChannels.DeleteOnSubmit(sdchannel);
                    ahamOper.SubmitChanges();

                    channelSet.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion


        #region ParentChannelSet

        internal static DataAccessResult ParentChannelSetListGet(ParentChannelSet channelSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ParentChannelSet> channelSetList = new HaiBindingList<ParentChannelSet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SDChannel> SDChannels;
                    if (channelSetToGet == null)
                        SDChannels = ahamOper.SDChannels.ToList<SDChannel>();
                    else
                    {
                        SDChannels = new List<SDChannel>();
                        SDChannels.Add(ahamOper.SDChannels.Single<SDChannel>(x => (x.ChannelSetKey == channelSetToGet.ParentChannelSetKey)));
                    }

                    foreach (SDChannel sdchannel in SDChannels)
                    {
                        ParentChannelSet channelSet = new ParentChannelSet();

                        channelSet.ParentChannelSetAbbreviation = sdchannel.ChannelSetAbbrev.Trim();
                        channelSet.ParentChannelSetKey = sdchannel.ChannelSetKey;
                        channelSet.ParentChannelSetName = sdchannel.ChannelSetName.Trim();

                        channelSet.ApplicationContext = sdchannel.AppContext.Trim();
                        channelSet.RowVersion = sdchannel.RowVersion;
                        channelSet.UserName = sdchannel.EntryUid.Trim();

                        channelSet.MarkOld();
                        channelSetList.AddToList(channelSet);
                    }

                    result.DataList = channelSetList;
                    result.Success = true;
                    result.BrowsablePropertyList = ChannelSet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ParentChannelSetGet(ParentChannelSet channelSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ParentChannelSetListGet(channelSetToGet, parameters);
            if (result.Success)
            {
                ParentChannelSet channelSet = (ParentChannelSet)result.SingleItem;
                result.ItemIsChanged = (channelSetToGet.RowVersion != channelSet.RowVersion);
            }

            return result;
        }

        #endregion


        #region ChannelSetMember

        internal static DataAccessResult ChannelSetMemberListGet(ChannelSetMember channelSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ChannelSetMember> channelSetMemberList = new HaiBindingList<ChannelSetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMChannel> SMChannels;
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<SMChannel>(x => x.DChannel);
                    dlo.LoadWith<SMChannel>(x => x.SDChannel);
                    dlo.LoadWith<SMChannel>(x => x.SupressReport);
                    ahamOper.LoadOptions = dlo;

                    if (channelSetMemberToGet == null)
                    {
                        SMChannels = ahamOper.SMChannels.ToList<SMChannel>();
                    }
                    else
                    {
                        SMChannels = new List<SMChannel>();
                        SMChannels.Add(ahamOper.SMChannels.Single<SMChannel>(x => (x.ChannelSetMemberKey == channelSetMemberToGet.ChannelSetMemberKey)));
                    }

                    foreach (SMChannel smchannel in SMChannels)
                    {
                        ChannelSetMember channelSetMember = new ChannelSetMember();

                        channelSetMember.ChannelKey = smchannel.ChannelKey;
                        channelSetMember.ChannelSetKey = smchannel.ChannelSetKey;
                        channelSetMember.ChannelSetMemberKey = smchannel.ChannelSetMemberKey;
                        channelSetMember.DateRange.BeginDate = smchannel.BeginDate.Date;
                        channelSetMember.DateRange.EndDate = smchannel.EndDate.Date;

                        channelSetMember.ChannelName = smchannel.DChannel.ChannelName.Trim();
                        channelSetMember.ChannelAbbreviation = smchannel.DChannel.ChannelAbbrev.Trim();
                        channelSetMember.ChannelSetAbbreviation = smchannel.SDChannel.ChannelSetAbbrev.Trim();
                        channelSetMember.ChannelSetName = smchannel.SDChannel.ChannelSetName.Trim();

                        channelSetMember.ApplicationContext = smchannel.AppContext.Trim();
                        channelSetMember.RowVersion = smchannel.RowVersion;
                        channelSetMember.UserName = smchannel.EntryUid.Trim();

                        channelSetMember.MarkOld();
                        channelSetMemberList.AddToList(channelSetMember);
                    }

                    result.DataList = channelSetMemberList;
                    result.Success = true;
                    result.BrowsablePropertyList = ChannelSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ChannelSetMemberGet(ChannelSetMember channelSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ChannelSetMemberListGet(channelSetMemberToGet, parameters);
            if (result.Success)
            {
                ChannelSetMember channelSetMember = (ChannelSetMember)result.SingleItem;
                result.ItemIsChanged = (channelSetMemberToGet.RowVersion != channelSetMember.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ChannelSetMemberSave(ChannelSetMember channelSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (channelSetMember.IsSavable)
            {
                if (channelSetMember.IsDeleted)
                    result = ChannelSetMemberDelete(channelSetMember, parameters);
                else if (channelSetMember.IsNew)
                    result = ChannelSetMemberInsert(channelSetMember, parameters);
                else if (channelSetMember.IsDirty)
                    result = ChannelSetMemberUpdate(channelSetMember, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ChannelSetMemberInsert(ChannelSetMember channelSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMChannel smchannel = new SMChannel();
                    smchannel.ChannelKey = channelSetMember.ChannelKey;
                    smchannel.ChannelSetKey = channelSetMember.ChannelSetKey;
                    smchannel.BeginDate = channelSetMember.DateRange.BeginDate.Date;
                    smchannel.EndDate = channelSetMember.DateRange.EndDate.Date;

                    smchannel.AppContext = parameters.ApplicationContext.Trim();
                    smchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SMChannel>().InsertOnSubmit(smchannel);
                    ahamOper.SubmitChanges();

                    channelSetMember.ChannelSetMemberKey = smchannel.ChannelSetMemberKey;
                    channelSetMember.RowVersion = smchannel.RowVersion;

                    channelSetMember.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ChannelSetMemberUpdate(ChannelSetMember channelSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMChannel smchannel = ahamOper.SMChannels.Single<SMChannel>(x => (x.ChannelSetMemberKey == channelSetMember.ChannelSetMemberKey));

                    if (smchannel.RowVersion != channelSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + channelSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smchannel.ChannelKey = channelSetMember.ChannelKey;
                    smchannel.ChannelSetKey = channelSetMember.ChannelSetKey;
                    smchannel.BeginDate = channelSetMember.DateRange.BeginDate.Date;
                    smchannel.EndDate = channelSetMember.DateRange.EndDate.Date;

                    smchannel.AppContext = parameters.ApplicationContext.Trim();
                    smchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    channelSetMember.MarkOld();
                    channelSetMember.RowVersion = smchannel.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ChannelSetMemberDelete(ChannelSetMember channelSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMChannel smchannel = ahamOper.SMChannels.Single<SMChannel>(x => (x.ChannelSetMemberKey == channelSetMember.ChannelSetMemberKey));
                    // add check for the existence of this channel in hmchannel

                    List<HmChannel> hmList;
                        hmList = ahamOper.HmChannels.ToList<HmChannel>();

                    foreach (HmChannel theChannel in hmList)
                    {
                        if (theChannel.SmChannelKeyParent == channelSetMember.ChannelSetMemberKey)
                        {
                            result.Success = false;
                            result.Message = "Channel Set Member exists as a parent in a Channel Heirarchy"
                                + "\" You must update the heirarchies before removing this member";
                            return result;
                        }
                        if (theChannel.SmChannelKeyChild == channelSetMember.ChannelSetMemberKey)
                        {
                            theChannel.AppContext = parameters.ApplicationContext.Trim();
                            theChannel.EntryUid = parameters.UserName.Trim();
                            ahamOper.HmChannels.DeleteOnSubmit(theChannel);
                        }
                    }

                    if (smchannel.RowVersion != channelSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + channelSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }
                    ahamOper.SubmitChanges();

                    smchannel.AppContext = parameters.ApplicationContext.Trim();
                    smchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.SMChannels.DeleteOnSubmit(smchannel);
                    ahamOper.SubmitChanges();

                    channelSetMember.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ChannelSetMembersDuplicate(ChannelSet sourceChannelSet, ChannelSet targetChannelSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            
            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMChannel> smChannels;
                    List<SMChannel> duplicatedItems = new List<SMChannel>();

                    smChannels = ahamOper.SMChannels.Where(x => (x.ChannelSetKey == sourceChannelSet.ChannelSetKey)).ToList<SMChannel>();

                    foreach (SMChannel smChannel in smChannels)
                    {
                        SMChannel duplicateSMChannel = new SMChannel();

                        duplicateSMChannel.ChannelSetKey = targetChannelSet.ChannelSetKey;
                        duplicateSMChannel.ChannelKey = smChannel.ChannelKey;

                        duplicateSMChannel.SuppressTarget = smChannel.SuppressTarget;

                        duplicateSMChannel.BeginDate = smChannel.BeginDate.Date;
                        duplicateSMChannel.EndDate = smChannel.EndDate.Date;
                        duplicateSMChannel.EntryUid = parameters.UserName.Trim();
                        duplicateSMChannel.AppContext = parameters.ApplicationContext.Trim();

                        duplicatedItems.Add(duplicateSMChannel);
                    }

                    ahamOper.GetTable<SMChannel>().InsertAllOnSubmit<SMChannel>(duplicatedItems);
                    ahamOper.SubmitChanges();

                    result.Success = true;
                    result.BrowsablePropertyList = ChannelSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region ChannelHeirarchySet

        internal static DataAccessResult ChannelHeirarchySetListGet(ChannelHeirarchySet channelHeirarchySetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ChannelHeirarchySet> channelSetList = new HaiBindingList<ChannelHeirarchySet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<HdChannel>(x => x.SDChildChannel);
                    //dlo.LoadWith<HdChannel>(x => x.SDParentChannel);
                    ahamOper.LoadOptions = dlo;
                    
                    List<HdChannel> HDChannels;
                    if (channelHeirarchySetToGet == null)
                        HDChannels = ahamOper.HdChannels.ToList<HdChannel>();
                    else
                    {
                        HDChannels = new List<HdChannel>();
                        HDChannels.Add(ahamOper.HdChannels.Single<HdChannel>(x => (x.HdChannelKey == channelHeirarchySetToGet.ChannelHeirarchySetKey)));
                    }

                    foreach (HdChannel hdchannel in HDChannels)
                    {
                        ChannelHeirarchySet channelHeirarchySet = new ChannelHeirarchySet();

                        channelHeirarchySet.ChannelHeirarchySetAbbreviation = hdchannel.Code.Trim();
                        channelHeirarchySet.ChannelHeirarchySetKey = hdchannel.HdChannelKey;
                        channelHeirarchySet.ChannelHeirarchySetName = hdchannel.Name.Trim();
                        channelHeirarchySet.ChannelSetKey = hdchannel.SdChildKey;
                        channelHeirarchySet.ChannelSetName = hdchannel.SDChildChannel.ChannelSetName.Trim();
                        channelHeirarchySet.ParentChannelSetKey = hdchannel.SdParentKey;
                        channelHeirarchySet.ParentChannelSetName = hdchannel.SDParentChannel.ChannelSetName.Trim();

                        channelHeirarchySet.ApplicationContext = hdchannel.AppContext.Trim();
                        channelHeirarchySet.RowVersion = hdchannel.RowVersion;
                        channelHeirarchySet.UserName = hdchannel.EntryUid.Trim();

                        channelHeirarchySet.MarkOld();
                        channelSetList.AddToList(channelHeirarchySet);
                    }

                    result.DataList = channelSetList;
                    result.Success = true;
                    result.BrowsablePropertyList = ChannelHeirarchySet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ChannelHeirarchySetGet(ChannelHeirarchySet channelHeirarchySetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ChannelHeirarchySetListGet(channelHeirarchySetToGet, parameters);
            if (result.Success)
            {
                ChannelHeirarchySet channelHeirarchySet = (ChannelHeirarchySet)result.SingleItem;
                result.ItemIsChanged = (channelHeirarchySetToGet.RowVersion != channelHeirarchySet.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ChannelHeirarchySetSave(ChannelHeirarchySet channelHeirarchySet, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (channelHeirarchySet.IsSavable)
            {
                if (channelHeirarchySet.IsDeleted)
                    result = ChannelHeirarchySetDelete(channelHeirarchySet, parameters);
                else if (channelHeirarchySet.IsNew)
                    result = ChannelHeirarchySetInsert(channelHeirarchySet, parameters);
                else if (channelHeirarchySet.IsDirty)
                    result = ChannelHeirarchySetUpdate(channelHeirarchySet, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ChannelHeirarchySetInsert(ChannelHeirarchySet channelHeirarchySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    HdChannel hdchannel = new HdChannel();
                    hdchannel.Code = channelHeirarchySet.ChannelHeirarchySetAbbreviation.Trim();
                    hdchannel.Name = channelHeirarchySet.ChannelHeirarchySetName.Trim();
                    hdchannel.SdChildKey = channelHeirarchySet.ChannelSetKey;
                    hdchannel.SdParentKey = channelHeirarchySet.ParentChannelSetKey;

                    hdchannel.AppContext = parameters.ApplicationContext.Trim();
                    hdchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<HdChannel>().InsertOnSubmit(hdchannel);
                    ahamOper.SubmitChanges();

                    channelHeirarchySet.ChannelHeirarchySetKey = hdchannel.HdChannelKey;
                    channelHeirarchySet.RowVersion = hdchannel.RowVersion;

                    channelHeirarchySet.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ChannelHeirarchySetUpdate(ChannelHeirarchySet channelHeirarchySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    HdChannel hdchannel = ahamOper.HdChannels.Single<HdChannel>(x => (x.HdChannelKey == channelHeirarchySet.ChannelHeirarchySetKey));

                    if (hdchannel.RowVersion != channelHeirarchySet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + channelHeirarchySet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    hdchannel.Code = channelHeirarchySet.ChannelHeirarchySetAbbreviation.Trim();
                    hdchannel.Name = channelHeirarchySet.ChannelHeirarchySetName.Trim();

                    hdchannel.AppContext = parameters.ApplicationContext.Trim();
                    hdchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    channelHeirarchySet.MarkOld();
                    channelHeirarchySet.RowVersion = hdchannel.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ChannelHeirarchySetDelete(ChannelHeirarchySet channelHeirarchySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    HdChannel hdchannel = ahamOper.HdChannels.Single<HdChannel>(x => (x.HdChannelKey == channelHeirarchySet.ChannelHeirarchySetKey));

                    if (hdchannel.RowVersion != channelHeirarchySet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + channelHeirarchySet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    hdchannel.AppContext = parameters.ApplicationContext.Trim();
                    hdchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.HdChannels.DeleteOnSubmit(hdchannel);
                    ahamOper.SubmitChanges();

                    channelHeirarchySet.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region ChannelHeirarchySetMember

        internal static DataAccessResult ChannelHeirarchySetMemberListGet(ChannelHeirarchySetMember channelHeirarchySetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ChannelHeirarchySetMember> channelHeirarchySetMemberList = new HaiBindingList<ChannelHeirarchySetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<HmChannel> HmChannels;
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<HmChannel>(x => x.SMChannel);
                    dlo.LoadWith<HmChannel>(x => x.HdChannel);
                    dlo.LoadWith<HmChannel>(x => x.ParentSMChannel);
                    //dlo.LoadWith<HmChannel>(x => x.SMChannel.DChannel);
                    //dlo.LoadWith<HmChannel>(x => x.ParentSMChannel.DChannel);
                    ahamOper.LoadOptions = dlo;

                    if (channelHeirarchySetMemberToGet == null)
                    {
                        HmChannels = ahamOper.HmChannels.ToList<HmChannel>();
                    }
                    else
                    {
                        HmChannels = new List<HmChannel>();
                        HmChannels.Add(ahamOper.HmChannels.Single<HmChannel>(x => (x.HmChannelKey == channelHeirarchySetMemberToGet.ChannelHeirarchySetMemberKey)));
                    }

                    foreach (HmChannel hmChannel in HmChannels)
                    {
                        ChannelHeirarchySetMember channelSetMember = new ChannelHeirarchySetMember();

                        channelSetMember.ChildChannelSetMemberKey = hmChannel.SmChannelKeyChild;
                        channelSetMember.ChannelSetMemberKey = hmChannel.SmChannelKeyParent;
                        channelSetMember.ParentChannelSetKey = hmChannel.HdChannel.SdParentKey;
                        channelSetMember.ChannelHeirarchySetKey = hmChannel.HdChannelKey;
                        channelSetMember.ChannelHeirarchySetMemberKey = hmChannel.HmChannelKey;
                 
                        channelSetMember.ChildChannelName = hmChannel.SMChannel.DChannel.ChannelName.Trim();
                        channelSetMember.ChildChannelAbbreviation = hmChannel.SMChannel.DChannel.ChannelAbbrev.Trim();
                        channelSetMember.ChannelName = hmChannel.ParentSMChannel.DChannel.ChannelName.Trim();
                        channelSetMember.ChannelAbbreviation = hmChannel.ParentSMChannel.DChannel.ChannelAbbrev.Trim();

                        channelSetMember.IsArbitraryParent = Utilities.ConvertByteToBool(hmChannel.isArbitraryParent);

                        channelSetMember.ChannelHeirarchySetAbbreviation = hmChannel.HdChannel.Code.Trim();
                        channelSetMember.ChannelHeirarchySetName = hmChannel.HdChannel.Name.Trim();
                        //channelSetMember.DateRange.BeginDate = DateTime.MinValue.Date;
                        //channelSetMember.DateRange.EndDate = DateTime.MaxValue.Date;

                        channelSetMember.ApplicationContext = hmChannel.AppContext.Trim();
                        channelSetMember.RowVersion = hmChannel.RowVersion;
                        channelSetMember.UserName = hmChannel.EntryUid.Trim();

                        channelSetMember.MarkOld();
                        channelHeirarchySetMemberList.AddToList(channelSetMember);
                    }

                    result.DataList = channelHeirarchySetMemberList;
                    result.Success = true;
                    result.BrowsablePropertyList = ChannelHeirarchySetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ChannelHeirarchySetMemberGet(ChannelHeirarchySetMember channelHeirarchySetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ChannelHeirarchySetMemberListGet(channelHeirarchySetMemberToGet, parameters);
            if (result.Success)
            {
                ChannelHeirarchySetMember channelHeirarchySetMember = (ChannelHeirarchySetMember)result.SingleItem;
                result.ItemIsChanged = (channelHeirarchySetMemberToGet.RowVersion != channelHeirarchySetMember.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ChannelHeirarchySetMemberSave(ChannelHeirarchySetMember channelHeirarchySetMember, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (channelHeirarchySetMember.IsSavable)
            {
                //if (channelHeirarchySetMember.IsDeleted)
                //    result = ChannelHeirarchySetMemberDelete(channelHeirarchySetMember, parameters);
                //else if (channelHeirarchySetMember.IsNew)
                //    result = ChannelHeirarchySetMemberInsert(channelHeirarchySetMember, parameters);
                //else 
                if (channelHeirarchySetMember.IsDirty)
                    result = ChannelHeirarchySetMemberUpdate(channelHeirarchySetMember, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        //private static DataAccessResult ChannelHeirarchySetMemberInsert(ChannelHeirarchySetMember channelHeirarchySetMember, DataServiceParameters parameters)
        //{
        //    DataAccessResult result = new DataAccessResult();

        //    using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
        //    {
        //        try
        //        {
        //            HmChannel hmchannel = new HmChannel();
        //            hmchannel.SmChannelKeyChild = channelHeirarchySetMember.ChildChannelSetMemberKey;
        //            hmchannel.HdChannelKey = channelHeirarchySetMember.ChannelHeirarchySetKey;
        //            hmchannel.SmChannelKeyParent = channelHeirarchySetMember.ChannelSetMemberKey;

        //            hmchannel.AppContext = parameters.ApplicationContext.Trim();
        //            hmchannel.EntryUid = parameters.UserName.Trim();

        //            ahamOper.GetTable<HmChannel>().InsertOnSubmit(hmchannel);
        //            ahamOper.SubmitChanges();

        //            channelHeirarchySetMember.ChannelSetMemberKey = hmchannel.HmChannelKey;
        //            channelHeirarchySetMember.RowVersion = hmchannel.RowVersion;

        //            channelHeirarchySetMember.MarkOld();
        //            result.Success = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            result.Success = false;
        //            result.Message = ex.Message;
        //        }
        //    }

        //    return result;
        //}

        private static DataAccessResult ChannelHeirarchySetMemberUpdate(ChannelHeirarchySetMember channelHeirarchySetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    HmChannel hmchannel = ahamOper.HmChannels.Single<HmChannel>(x => (x.HmChannelKey == channelHeirarchySetMember.ChannelHeirarchySetMemberKey));

                    if (hmchannel.RowVersion != channelHeirarchySetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + channelHeirarchySetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    hmchannel.SmChannelKeyChild = channelHeirarchySetMember.ChildChannelSetMemberKey;
                    hmchannel.SmChannelKeyParent = channelHeirarchySetMember.ChannelSetMemberKey;
                    hmchannel.isArbitraryParent = 0;
                    //hmchannel.HdChannelKey = channelHeirarchySetMember.ChannelHeirarchySetKey;

                    hmchannel.AppContext = parameters.ApplicationContext.Trim();
                    hmchannel.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    channelHeirarchySetMember.MarkOld();
                    channelHeirarchySetMember.RowVersion = hmchannel.RowVersion;
                    result = ChannelHeirarchySetMemberGet(channelHeirarchySetMember, parameters);
                    channelHeirarchySetMember.IsArbitraryParent = ((ChannelHeirarchySetMember)result.SingleItem).IsArbitraryParent;
                    channelHeirarchySetMember.ChannelAbbreviation = ((ChannelHeirarchySetMember)result.SingleItem).ChannelAbbreviation;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        //private static DataAccessResult ChannelHeirarchySetMemberDelete(ChannelHeirarchySetMember channelHeirarchySetMember, DataServiceParameters parameters)
        //{
        //    DataAccessResult result = new DataAccessResult();

        //    using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
        //    {
        //        try
        //        {
        //            HmChannel hmchannel = ahamOper.HmChannels.Single<HmChannel>(x => (x.HmChannelKey == channelHeirarchySetMember.ChannelHeirarchySetMemberKey));

        //            if (hmchannel.RowVersion != channelHeirarchySetMember.RowVersion)
        //            {
        //                result.Success = false;
        //                result.Message = "Concurrency error: \"" + channelHeirarchySetMember.UniqueIdentifier
        //                    + "\" has been altered since being retrieved for this operation."
        //                    + "\n\n\rYour changes have NOT been saved.";
        //                return result;
        //            }

        //            hmchannel.AppContext = parameters.ApplicationContext.Trim();
        //            hmchannel.EntryUid = parameters.UserName.Trim();

        //            ahamOper.HmChannels.DeleteOnSubmit(hmchannel);
        //            ahamOper.SubmitChanges();

        //            channelHeirarchySetMember.MarkClean();
        //            result.Success = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            result.Success = false;
        //            result.Message = ex.Message;
        //        }
        //    }

        //    return result;
        //}

        internal static DataAccessResult ChannelHeirarchySetMembersDuplicate(ChannelHeirarchySet sourceChannelHeirarchySet, ChannelHeirarchySet targetChannelHeirarchySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<HmChannel> hmChannels;
                    List<HmChannel> duplicatedItems = new List<HmChannel>();

                    hmChannels = ahamOper.HmChannels.Where(x => (x.HmChannelKey == sourceChannelHeirarchySet.ChannelHeirarchySetKey)).ToList<HmChannel>();

                    foreach (HmChannel hmChannel in hmChannels)
                    {
                        HmChannel duplicateHmChannel = new HmChannel();

                        duplicateHmChannel.HdChannelKey = targetChannelHeirarchySet.ChannelSetKey;
                        duplicateHmChannel.SmChannelKeyChild = hmChannel.SmChannelKeyChild;
                        duplicateHmChannel.SmChannelKeyParent = hmChannel.SmChannelKeyParent;
                        duplicateHmChannel.isArbitraryParent = hmChannel.isArbitraryParent;

                        //duplicateHmChannel.SuppressTarget = hmChannel.SuppressTarget;

                        duplicateHmChannel.EntryUid = parameters.UserName.Trim();
                        duplicateHmChannel.AppContext = parameters.ApplicationContext.Trim();

                        duplicatedItems.Add(duplicateHmChannel);
                    }

                    ahamOper.GetTable<HmChannel>().InsertAllOnSubmit<HmChannel>(duplicatedItems);
                    ahamOper.SubmitChanges();

                    result.Success = true;
                    result.BrowsablePropertyList = ChannelSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

    }
}
