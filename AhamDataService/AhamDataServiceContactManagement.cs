﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal class AhamDataServiceContactManagement
    {
        #region ContactRoles

        internal static DataAccessResult ContactRolesListGet(ContactRoles contactRolesToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ContactRoles> contactRolesList = new HaiBindingList<ContactRoles>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<AhamDataLinq.ContactRole>(x => (x.DAssn));
                    dlo.LoadWith<AhamDataLinq.ContactRole>(x => (x.DMfg));
                    dlo.LoadWith<AhamDataLinq.ContactRole>(x => (x.DProduct));
                    dlo.LoadWith<AhamDataLinq.ContactRole>(x => (x.DCatalog));
                    ahamOper.LoadOptions = dlo;

                    List<AhamDataLinq.ContactRole> dcontactRoles;
                    if (contactRolesToGet == null)
                    {
                        dcontactRoles = ahamOper.ContactRoles.ToList<AhamDataLinq.ContactRole>();
                    }
                    else
                    {
                        dcontactRoles=new List<AhamDataLinq.ContactRole>();
                        dcontactRoles.Add(ahamOper.ContactRoles.Single<AhamDataLinq.ContactRole>(x => (x.ContactKey==contactRolesToGet.ContactRolesKey)));
                    }

                    foreach (AhamDataLinq.ContactRole dcontactRole in dcontactRoles)
                    {
                        ContactRoles contactRoles = new ContactRoles();

                        contactRoles.ContactRolesKey = dcontactRole.ContactKey;
                        contactRoles.UserCode = dcontactRole.NTUser.Trim();
                        contactRoles.AssociationKey = dcontactRole.AssnKey;
                        contactRoles.AssociationName = dcontactRole.DAssn.AssnName.Trim();
                        contactRoles.ManufacturerKey = dcontactRole.MfgKey;
                        contactRoles.ManufacturerName = dcontactRole.DMfg.MfgName.Trim();
                        contactRoles.ProductKey = dcontactRole.ProductKey;
                        contactRoles.ProductName = dcontactRole.DProduct.ProductName.Trim();
                        contactRoles.CatalogKey = dcontactRole.CatalogKey;
                        contactRoles.CatalogName = dcontactRole.DCatalog.CatalogName.Trim();
                        contactRoles.SeeCompanyData = Utilities.ConvertByteToBool(dcontactRole.SeeCoData);
                        contactRoles.EditCompanyData = Utilities.ConvertByteToBool(dcontactRole.EditCoData);
                        contactRoles.SeeIndustryData = Utilities.ConvertByteToBool(dcontactRole.SeeIndData);
                        contactRoles.SeeModels = Utilities.ConvertByteToBool(dcontactRole.SeeModels);
                        contactRoles.EditModels = Utilities.ConvertByteToBool(dcontactRole.EditModels);
                        contactRoles.SubmitData = Utilities.ConvertByteToBool(dcontactRole.SubmitData);
                        contactRoles.Responsible = Utilities.ConvertByteToBool(dcontactRole.Responsible);
                        contactRoles.EditCodes = Utilities.ConvertByteToBool(dcontactRole.EditCodes);
                        contactRoles.SeePublic = Utilities.ConvertByteToBool(dcontactRole.SeePublic);
                        contactRoles.SeeAll = Utilities.ConvertByteToBool(dcontactRole.SeeAll);
                        contactRoles.SeeStatusDetails = Utilities.ConvertByteToBool(dcontactRole.SeeStatusDetails);
                        contactRoles.Billable = Utilities.ConvertByteToBool(dcontactRole.Billable);
                        contactRoles.SecurityLevel = dcontactRole.SecLevel;

                        contactRoles.ApplicationContext = dcontactRole.AppContext.Trim();
                        contactRoles.RowVersion = dcontactRole.RowVersion;
                        contactRoles.UserName = dcontactRole.EntryUid.Trim();

                        contactRoles.MarkOld();
                        contactRolesList.AddToList(contactRoles);
                    }

                    result.DataList = contactRolesList;
                    result.Success = true;
                    result.BrowsablePropertyList = ContactRoles.GetBrowsablePropertyList();
                }

                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ContactRolesGet(ContactRoles contactRolesToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ContactRolesListGet(contactRolesToGet, parameters);
            if (result.Success)
            {
                ContactRoles contactRoles = (ContactRoles)result.SingleItem;
                result.ItemIsChanged = (contactRolesToGet.RowVersion != contactRoles.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ContactRolesSave(ContactRoles contactRoles, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (contactRoles.IsSavable)
            {
                if (contactRoles.IsDeleted)
                    result = ContactRolesDelete(contactRoles, parameters);
                else if (contactRoles.IsNew)
                    result = ContactRolesInsert(contactRoles, parameters);
                else if (contactRoles.IsDirty)
                    result = ContactRolesUpdate(contactRoles, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        internal static DataAccessResult ContactRolesInsert(ContactRoles contactRoles, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.ContactRole dcontactRoles = new ContactRole();

                    dcontactRoles.NTUser = contactRoles.UserCode.Trim();
                    dcontactRoles.AssnKey = contactRoles.AssociationKey;
                    dcontactRoles.MfgKey = contactRoles.ManufacturerKey;
                    dcontactRoles.ProductKey = contactRoles.ProductKey;
                    dcontactRoles.CatalogKey = contactRoles.CatalogKey;
                    dcontactRoles.SeeCoData = Utilities.ConvertBoolToByte(contactRoles.SeeCompanyData);
                    dcontactRoles.EditCoData = Utilities.ConvertBoolToByte(contactRoles.EditCompanyData);
                    dcontactRoles.SeeIndData= Utilities.ConvertBoolToByte(contactRoles.SeeIndustryData);
                    dcontactRoles.SeeModels = Utilities.ConvertBoolToByte(contactRoles.SeeModels);
                    dcontactRoles.EditModels = Utilities.ConvertBoolToByte(contactRoles.EditModels);
                    dcontactRoles.SubmitData = Utilities.ConvertBoolToByte(contactRoles.SubmitData);
                    dcontactRoles.Responsible = Utilities.ConvertBoolToByte(contactRoles.Responsible);
                    dcontactRoles.EditCodes = Utilities.ConvertBoolToByte(contactRoles.EditCodes);
                    dcontactRoles.SeePublic = Utilities.ConvertBoolToByte(contactRoles.SeePublic);
                    dcontactRoles.SeeAll = Utilities.ConvertBoolToByte(contactRoles.SeeAll);
                    dcontactRoles.SeeStatusDetails = Utilities.ConvertBoolToByte(contactRoles.SeeStatusDetails);
                    dcontactRoles.Billable = Utilities.ConvertBoolToByte(contactRoles.Billable);
                    dcontactRoles.SecLevel = (byte)contactRoles.SecurityLevel;

                    dcontactRoles.AppContext = parameters.ApplicationContext.Trim();
                    dcontactRoles.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<ContactRole>().InsertOnSubmit(dcontactRoles);
                    ahamOper.SubmitChanges();

                    contactRoles.ContactRolesKey = dcontactRoles.ContactKey;
                    contactRoles.RowVersion = dcontactRoles.RowVersion;

                    contactRoles.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ContactRolesUpdate(ContactRoles contactRoles, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.ContactRole dcontactRoles = ahamOper.ContactRoles.Single<ContactRole>(x => (x.ContactKey == contactRoles.ContactRolesKey));
                    if (dcontactRoles.RowVersion != contactRoles.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + contactRoles.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dcontactRoles.NTUser = contactRoles.UserCode.Trim();
                    dcontactRoles.AssnKey = contactRoles.AssociationKey;
                    dcontactRoles.MfgKey = contactRoles.ManufacturerKey;
                    dcontactRoles.ProductKey = contactRoles.ProductKey;
                    dcontactRoles.CatalogKey = contactRoles.CatalogKey;
                    dcontactRoles.SeeCoData = Utilities.ConvertBoolToByte(contactRoles.SeeCompanyData);
                    dcontactRoles.EditCoData = Utilities.ConvertBoolToByte(contactRoles.EditCompanyData);
                    dcontactRoles.SeeIndData = Utilities.ConvertBoolToByte(contactRoles.SeeIndustryData);
                    dcontactRoles.SeeModels = Utilities.ConvertBoolToByte(contactRoles.SeeModels);
                    dcontactRoles.EditModels = Utilities.ConvertBoolToByte(contactRoles.EditModels);
                    dcontactRoles.SubmitData = Utilities.ConvertBoolToByte(contactRoles.SubmitData);
                    dcontactRoles.Responsible = Utilities.ConvertBoolToByte(contactRoles.Responsible);
                    dcontactRoles.EditCodes = Utilities.ConvertBoolToByte(contactRoles.EditCodes);
                    dcontactRoles.SeePublic = Utilities.ConvertBoolToByte(contactRoles.SeePublic);
                    dcontactRoles.SeeAll = Utilities.ConvertBoolToByte(contactRoles.SeeAll);
                    dcontactRoles.SeeStatusDetails = Utilities.ConvertBoolToByte(contactRoles.SeeStatusDetails);
                    dcontactRoles.Billable = Utilities.ConvertBoolToByte(contactRoles.Billable);
                    dcontactRoles.SecLevel = (byte)contactRoles.SecurityLevel;

                    dcontactRoles.AppContext = parameters.ApplicationContext.Trim();
                    dcontactRoles.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    contactRoles.MarkOld();
                    contactRoles.RowVersion = dcontactRoles.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ContactRolesDelete(ContactRoles contactRoles, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.ContactRole dcontactRoles = ahamOper.ContactRoles.Single<ContactRole>(x => (x.ContactKey == contactRoles.ContactRolesKey));
                    if (dcontactRoles.RowVersion != contactRoles.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + contactRoles.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dcontactRoles.AppContext = parameters.ApplicationContext.Trim();
                    dcontactRoles.EntryUid = parameters.UserName.Trim();

                    ahamOper.ContactRoles.DeleteOnSubmit(dcontactRoles);
                    ahamOper.SubmitChanges();

                    contactRoles.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region FSList

        internal static DataAccessResult FSListListGet(FSList fsListToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<FSList> fsLists = new HaiBindingList<FSList>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<AhamDataLinq.FSList> dfsLists;

                    if (fsListToGet == null)
                    {
                        dfsLists = ahamOper.FSLists.ToList<AhamDataLinq.FSList>();
                    }
                    else
                    {
                        dfsLists = new List<AhamDataLinq.FSList>();
                        dfsLists.Add(ahamOper.FSLists.Single<AhamDataLinq.FSList>(x => (x.fslistid==fsListToGet.FSListKey)));
                    }

                    foreach (AhamDataLinq.FSList dfslist in dfsLists)
                    {
                        FSList fslist = new FSList();

                        fslist.FSListKey = dfslist.fslistid;
                        fslist.FullName = dfslist.FULL_NAME.Trim();
                        fslist.UserCode = dfslist.NTUser.Trim();
                        fslist.Company = dfslist.COMPANY.Trim();
                        fslist.WorkPhoneNumber = dfslist.WORK_PHONE.Trim();
                        fslist.FaxNumber = dfslist.FAX.Trim();
                        fslist.EmailAddress = dfslist.EMAIL.Trim();
                        fslist.ProductCode = dfslist.PRODUCT_CODE.Trim();
                        fslist.FS = Utilities.ConvertByteToBool(dfslist.FS);
                        fslist.Web = Utilities.ConvertByteToBool(dfslist.WEB);
                        fslist.SendEmail = Utilities.ConvertByteToBool(dfslist.SendEmail);
                        fslist.SendFax = Utilities.ConvertByteToBool(dfslist.SendFax);
                        fslist.Notes = dfslist.notes.Trim();
                        fslist.DateRange.BeginDate = dfslist.BeginDate.Date;
                        fslist.DateRange.EndDate = dfslist.EndDate.Date;
                        fslist.ChargeDateRange.BeginDate = dfslist.BeginChargeDate.Date;
                        fslist.ChargeDateRange.EndDate = dfslist.EndChargeDate.Date;
                        fslist.DenyAccess = Utilities.ConvertByteToBool(dfslist.DenyAccess);
                        fslist.ApplicationContext = parameters.ApplicationContext.Trim();
                        fslist.RowVersion = dfslist.RowVersion;
                        fslist.UserName = parameters.UserName.Trim();

                        fslist.ApplicationContext = dfslist.AppContext.Trim();
                        fslist.RowVersion = dfslist.RowVersion;
                        fslist.UserName = dfslist.EntryUid;

                        fslist.MarkOld();
                        fsLists.AddToList(fslist);
                    }

                    result.DataList = fsLists;
                    result.Success = true;
                    result.BrowsablePropertyList = FSList.GetBrowsablePropertyList();
                }

                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult FSListGet(FSList fsListToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = FSListListGet(fsListToGet, parameters);
            if (result.Success)
            {
                FSList fsList = (FSList)result.SingleItem;
                result.ItemIsChanged = (fsListToGet.RowVersion != fsList.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult FSListSave(FSList fsList, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (fsList.IsSavable)
            {
                if (fsList.IsDeleted)
                    result = FSListDelete(fsList, parameters);
                else if (fsList.IsNew)
                    result = FSListInsert(fsList, parameters);
                else if (fsList.IsDirty)
                    result = FSListUpdate(fsList, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        internal static DataAccessResult FSListInsert(FSList fsList, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.FSList dfsList = new AhamDataLinq.FSList();

                    dfsList.FULL_NAME = fsList.FullName.Trim();
                    dfsList.NTUser = fsList.UserCode.Trim();
                    dfsList.COMPANY = fsList.Company.Trim();
                    dfsList.WORK_PHONE = fsList.WorkPhoneNumber.Trim();
                    dfsList.FAX = fsList.FaxNumber.Trim();
                    dfsList.EMAIL = fsList.EmailAddress.Trim();
                    dfsList.PRODUCT_CODE = fsList.ProductCode.Trim();
                    dfsList.FS = Utilities.ConvertBoolToByte(fsList.FS);
                    dfsList.WEB = Utilities.ConvertBoolToByte(fsList.Web);
                    dfsList.SendEmail = Utilities.ConvertBoolToByte(fsList.SendEmail);
                    dfsList.SendFax = Utilities.ConvertBoolToByte(fsList.SendFax);
                    dfsList.notes = fsList.Notes.Trim();
                    dfsList.BeginDate = fsList.DateRange.BeginDate.Date;
                    dfsList.EndDate = fsList.DateRange.EndDate.Date;
                    dfsList.BeginChargeDate = fsList.ChargeDateRange.BeginDate.Date;
                    dfsList.EndChargeDate = fsList.ChargeDateRange.EndDate.Date;
                    dfsList.DenyAccess = Utilities.ConvertBoolToByte(fsList.DenyAccess);
                    dfsList.FULL_SITE = 0;  // no longer used
                    dfsList.PRIME = 0;      // no longer used

                    dfsList.AppContext = parameters.ApplicationContext.Trim();
                    dfsList.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<AhamDataLinq.FSList>().InsertOnSubmit(dfsList);
                    ahamOper.SubmitChanges();

                    fsList.FSListKey = dfsList.fslistid;
                    fsList.RowVersion = dfsList.RowVersion;

                    fsList.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult FSListUpdate(FSList fsList, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.FSList dfsList = ahamOper.FSLists.Single<AhamDataLinq.FSList>(x => (x.fslistid == fsList.FSListKey));
                    if (dfsList.RowVersion!=fsList.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + fsList.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dfsList.FULL_NAME = fsList.FullName.Trim();
                    dfsList.NTUser = fsList.UserCode.Trim();
                    dfsList.COMPANY = fsList.Company.Trim();
                    dfsList.WORK_PHONE = fsList.WorkPhoneNumber.Trim();
                    dfsList.FAX = fsList.FaxNumber.Trim();
                    dfsList.EMAIL = fsList.EmailAddress.Trim();
                    dfsList.PRODUCT_CODE = fsList.ProductCode.Trim();
                    dfsList.FS = Utilities.ConvertBoolToByte(fsList.FS);
                    dfsList.WEB = Utilities.ConvertBoolToByte(fsList.Web);
                    dfsList.SendEmail = Utilities.ConvertBoolToByte(fsList.SendEmail);
                    dfsList.SendFax = Utilities.ConvertBoolToByte(fsList.SendFax);
                    dfsList.notes = fsList.Notes.Trim();
                    dfsList.BeginDate = fsList.DateRange.BeginDate.Date;
                    dfsList.EndDate = fsList.DateRange.EndDate.Date;
                    dfsList.BeginChargeDate = fsList.ChargeDateRange.BeginDate.Date;
                    dfsList.EndChargeDate = fsList.ChargeDateRange.EndDate.Date;
                    dfsList.DenyAccess = Utilities.ConvertBoolToByte(fsList.DenyAccess);

                    dfsList.AppContext = parameters.ApplicationContext.Trim();
                    dfsList.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    fsList.MarkOld();
                    fsList.RowVersion = dfsList.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult FSListDelete(FSList fsList, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.FSList dfsList = ahamOper.FSLists.Single<AhamDataLinq.FSList>(x => (x.fslistid == fsList.FSListKey));
                    if (dfsList.RowVersion!=fsList.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + fsList.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dfsList.AppContext = parameters.ApplicationContext.Trim();
                    dfsList.EntryUid = parameters.UserName.Trim();

                    ahamOper.FSLists.DeleteOnSubmit(dfsList);
                    ahamOper.SubmitChanges();

                    fsList.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }
 
        #endregion
    }
}
