﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal static class AhamDataServiceCouncil
    {
        #region Council

        internal static DataAccessResult CouncilListGet(Council councilToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Council> councils = new HaiBindingList<Council>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DCouncil> DCouncils;
                    if (councilToGet == null)
                        DCouncils = ahamOper.DCouncils.ToList<DCouncil>();
                    else
                    {
                        DCouncils = new List<DCouncil>();
                        DCouncils.Add(ahamOper.DCouncils.Single<DCouncil>(x => (x.CouncilKey == councilToGet.CouncilKey)));
                    }

                    foreach (DCouncil dcouncil in DCouncils)
                    {
                        Council council = new Council();

                        council.CouncilAbbreviation = dcouncil.CouncilAbbrev.Trim();
                        council.CouncilKey = dcouncil.CouncilKey;
                        council.CouncilName = dcouncil.CouncilName.Trim();

                        council.ApplicationContext = dcouncil.AppContext.Trim();
                        council.RowVersion = dcouncil.RowVersion;
                        council.UserName = dcouncil.EntryUid.Trim();

                        council.MarkOld();
                        councils.AddToList(council);
                    }

                    result.DataList = councils;
                    result.Success = true;
                    result.BrowsablePropertyList = Council.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult CouncilGet(Council councilToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = CouncilListGet(councilToGet, parameters);
            if (result.Success)
            {
                Council council = (Council)result.SingleItem;
                result.ItemIsChanged = (councilToGet.RowVersion != council.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult CouncilSave(Council council, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (council.IsSavable)
            {
                if (council.IsDeleted)
                    result = CouncilDelete(council, parameters);
                else if (council.IsNew)
                    result = CouncilInsert(council, parameters);
                else if (council.IsDirty)
                    result = CouncilUpdate(council, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult CouncilInsert(Council council, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DCouncil dcouncil = new DCouncil();
                    dcouncil.CouncilAbbrev = council.CouncilAbbreviation.Trim();
                    dcouncil.CouncilName = council.CouncilName.Trim();

                    dcouncil.AppContext = parameters.ApplicationContext.Trim();
                    dcouncil.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DCouncil>().InsertOnSubmit(dcouncil);
                    ahamOper.SubmitChanges();

                    council.CouncilKey = dcouncil.CouncilKey;
                    council.RowVersion = dcouncil.RowVersion;

                    council.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult CouncilUpdate(Council council, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DCouncil dcouncil = ahamOper.DCouncils.Single<DCouncil>(x => (x.CouncilKey == council.CouncilKey));

                    if (dcouncil.RowVersion != council.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + council.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dcouncil.CouncilAbbrev = council.CouncilAbbreviation.Trim();
                    dcouncil.CouncilName = council.CouncilName.Trim();

                    dcouncil.AppContext = parameters.ApplicationContext.Trim();
                    dcouncil.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    council.MarkOld();
                    council.RowVersion = dcouncil.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult CouncilDelete(Council council, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DCouncil dcouncil = ahamOper.DCouncils.Single<DCouncil>(x => (x.CouncilKey == council.CouncilKey));

                    if (dcouncil.RowVersion != council.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + council.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dcouncil.AppContext = parameters.ApplicationContext.Trim();
                    dcouncil.EntryUid = parameters.UserName.Trim();

                    ahamOper.DCouncils.DeleteOnSubmit(dcouncil);
                    ahamOper.SubmitChanges();

                    council.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion
    }
}
