﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal class AhamDataServiceDimension
    {
        #region Dimension

        internal static DataAccessResult DimensionListGet(Dimension dimensionToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Dimension> dimensions = new HaiBindingList<Dimension>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DDim> DDims;
                    if (dimensionToGet == null)
                        DDims = ahamOper.DDims.ToList<DDim>();
                    else
                    {
                        DDims = new List<DDim>();
                        DDims.Add(ahamOper.DDims.Single<DDim>(x => (x.DimKey == dimensionToGet.DimensionKey)));
                    }

                    foreach (DDim ddimension in DDims)
                    {
                        Dimension dimension = new Dimension();

                        dimension.DimensionAbbreviation = ddimension.DimAbbrev.Trim();
                        dimension.DimensionKey = ddimension.DimKey;
                        dimension.DimensionName = ddimension.DimName.Trim();

                        dimension.ApplicationContext = ddimension.AppContext.Trim();
                        dimension.RowVersion = ddimension.RowVersion;
                        dimension.UserName = ddimension.EntryUid.Trim();

                        dimension.MarkOld();
                        dimensions.AddToList(dimension);
                    }

                    result.DataList = dimensions;
                    result.Success = true;
                    result.BrowsablePropertyList = Dimension.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult DimensionGet(Dimension dimensionToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = DimensionListGet(dimensionToGet, parameters);
            if (result.Success)
            {
                Dimension dimension = (Dimension)result.SingleItem;
                result.ItemIsChanged = (dimensionToGet.RowVersion != dimension.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult DimensionSave(Dimension dimension, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (dimension.IsSavable)
            {
                if (dimension.IsDeleted)
                    result = DimensionDelete(dimension, parameters);
                else if (dimension.IsNew)
                    result = DimensionInsert(dimension, parameters);
                else if (dimension.IsDirty)
                    result = DimensionUpdate(dimension, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult DimensionInsert(Dimension dimension, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DDim ddimension = new DDim();
                    ddimension.DimAbbrev = dimension.DimensionAbbreviation.Trim();
                    ddimension.DimName = dimension.DimensionName.Trim();

                    ddimension.AppContext = parameters.ApplicationContext.Trim();
                    ddimension.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DDim>().InsertOnSubmit(ddimension);
                    ahamOper.SubmitChanges();

                    dimension.DimensionKey = ddimension.DimKey;
                    dimension.RowVersion = ddimension.RowVersion;

                    dimension.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult DimensionUpdate(Dimension dimension, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DDim ddimension = ahamOper.DDims.Single<DDim>(x => (x.DimKey == dimension.DimensionKey));

                    if (ddimension.RowVersion != dimension.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + dimension.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    ddimension.DimAbbrev = dimension.DimensionAbbreviation.Trim();
                    ddimension.DimName = dimension.DimensionName.Trim();

                    ddimension.AppContext = parameters.ApplicationContext.Trim();
                    ddimension.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    dimension.MarkOld();
                    dimension.RowVersion = ddimension.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult DimensionDelete(Dimension dimension, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DDim ddimension = ahamOper.DDims.Single<DDim>(x => (x.DimKey == dimension.DimensionKey));

                    if (ddimension.RowVersion != dimension.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + dimension.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    ddimension.AppContext = parameters.ApplicationContext.Trim();
                    ddimension.EntryUid = parameters.UserName.Trim();

                    ahamOper.DDims.DeleteOnSubmit(ddimension);
                    ahamOper.SubmitChanges();

                    dimension.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion


        #region ProductDimension

        internal static DataAccessResult ProductDimensionListGet(ProductDimension productDimensionToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ProductDimension> productDimensions = new HaiBindingList<ProductDimension>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<DProductDim>(x => x.DProduct);
                    dlo.LoadWith<DProductDim>(x => x.DDim);
                    ahamOper.LoadOptions = dlo;

                    List<DProductDim> DProductDims;
                    if (productDimensionToGet == null)
                        DProductDims = ahamOper.DProductDims.ToList<DProductDim>();
                    else
                    {
                        DProductDims = new List<DProductDim>();
                        DProductDims.Add(ahamOper.DProductDims.Single<DProductDim>(x => (x.ProductDimKey == productDimensionToGet.ProductDimensionKey)));
                    }

                    foreach (DProductDim dproductDim in DProductDims)
                    {
                        ProductDimension productDimension = new ProductDimension();

                        productDimension.ProductDimensionKey = dproductDim.ProductDimKey;
                        productDimension.ProductDimensionName = dproductDim.ProductDimName.Trim();
                        productDimension.ProductKey = dproductDim.ProductKey;
                        productDimension.ProductName = dproductDim.DProduct.ProductName.Trim();
                        productDimension.DimensionKey = dproductDim.DimKey;
                        productDimension.DimensionName = dproductDim.DDim.DimName.Trim();
                        productDimension.Continuous = Utilities.ConvertByteToBool(dproductDim.Continuous);
                        productDimension.UnitLabel = dproductDim.UnitLabel.Trim();
                        productDimension.BeginDate = dproductDim.BeginDate.Date;
                        productDimension.EndDate = dproductDim.EndDate.Date;

                        productDimension.RowVersion = dproductDim.RowVersion;
                        productDimension.UserName = dproductDim.EntryUid.Trim();
                        productDimension.ApplicationContext = dproductDim.AppContext.Trim();

                        productDimension.MarkOld();
                        productDimensions.AddToList(productDimension);
                    }

                    result.DataList = productDimensions;
                    result.Success = true;
                    result.BrowsablePropertyList = ProductDimension.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ProductDimensionGet(ProductDimension productDimensionToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ProductDimensionListGet(productDimensionToGet, parameters);
            if (result.Success)
            {
                ProductDimension productDimension = (ProductDimension)result.SingleItem;
                result.ItemIsChanged = (productDimensionToGet.RowVersion != productDimension.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ProductDimensionSave(ProductDimension productDimension, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (productDimension.IsSavable)
            {
                if (productDimension.IsDeleted)
                    result = ProductDimensionDelete(productDimension, parameters);
                else if (productDimension.IsNew)
                    result = ProductDimensionInsert(productDimension, parameters);
                else if (productDimension.IsDirty)
                    result = ProductDimensionUpdate(productDimension, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ProductDimensionInsert(ProductDimension productDimension, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DProductDim dproductDim = new DProductDim();

                    dproductDim.ProductKey = productDimension.ProductKey;
                    dproductDim.ProductDimName = productDimension.ProductDimensionName.Trim();
                    dproductDim.DimKey = productDimension.DimensionKey;
                    dproductDim.Continuous = Utilities.ConvertBoolToByte(productDimension.Continuous);
                    dproductDim.UnitLabel = productDimension.UnitLabel.Trim();
                    dproductDim.BeginDate = productDimension.BeginDate.Date;
                    dproductDim.EndDate = productDimension.EndDate.Date;

                    dproductDim.AppContext = parameters.ApplicationContext.Trim();
                    dproductDim.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DProductDim>().InsertOnSubmit(dproductDim);
                    ahamOper.SubmitChanges();

                    productDimension.ProductDimensionKey = dproductDim.ProductDimKey;
                    productDimension.RowVersion = dproductDim.RowVersion;

                    productDimension.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ProductDimensionUpdate(ProductDimension productDimension, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DProductDim dproductDim = ahamOper.DProductDims.Single<DProductDim>(x => (x.ProductDimKey == productDimension.ProductDimensionKey));

                    if (dproductDim.RowVersion != productDimension.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + productDimension.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dproductDim.ProductKey = productDimension.ProductKey;
                    dproductDim.ProductDimName = productDimension.ProductDimensionName.Trim();
                    dproductDim.DimKey = productDimension.DimensionKey;
                    dproductDim.Continuous = Utilities.ConvertBoolToByte(productDimension.Continuous);
                    dproductDim.UnitLabel = productDimension.UnitLabel.Trim();
                    dproductDim.BeginDate = productDimension.BeginDate.Date;
                    dproductDim.EndDate = productDimension.EndDate.Date;

                    dproductDim.AppContext = parameters.ApplicationContext.Trim();
                    dproductDim.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    productDimension.MarkOld();
                    productDimension.RowVersion = dproductDim.RowVersion;
                    result.Success = true;
                    
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ProductDimensionDelete(ProductDimension productDimension, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DProductDim dproductDim = ahamOper.DProductDims.Single<DProductDim>(x => (x.ProductDimKey == productDimension.ProductDimensionKey));

                    if (dproductDim.RowVersion != productDimension.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + productDimension.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dproductDim.AppContext = parameters.ApplicationContext.Trim();
                    dproductDim.EntryUid = parameters.UserName.Trim();

                    ahamOper.DProductDims.DeleteOnSubmit(dproductDim);
                    ahamOper.SubmitChanges();

                    productDimension.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion
    }
}
