﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal static class AhamDataServiceExpansionFactor
    {

        #region ExpansionFactor

        internal static DataAccessResult ExpansionFactorListGet(ExpansionFactor expansionFactorToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ExpansionFactor> expansionFactors = new HaiBindingList<ExpansionFactor>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<DExpFac>(x => x.DActivity);
                    dlo.LoadWith<DExpFac>(x => x.DCatalog);
                    dlo.LoadWith<DExpFac>(x => x.DMarket);
                    dlo.LoadWith<DExpFac>(x => x.DProduct);
                    ahamOper.LoadOptions = dlo;

                    List<DExpFac> DExpFacs;
                    if (expansionFactorToGet == null)
                        DExpFacs = ahamOper.DExpFacs.ToList<DExpFac>();
                    else
                    {
                        DExpFacs = new List<DExpFac>();
                        DExpFacs.Add(ahamOper.DExpFacs.Single<DExpFac>(x => (x.expfacKey == expansionFactorToGet.ExpansionFactorKey)));
                    }

                    foreach (DExpFac dexpansionFactor in DExpFacs)
                    {
                        ExpansionFactor expansionFactor = new ExpansionFactor();

                        expansionFactor.ExpansionFactorKey = dexpansionFactor.expfacKey;

                        expansionFactor.ActivityKey = dexpansionFactor.Activitykey;
                        expansionFactor.ActivityName = dexpansionFactor.DActivity.ActivityName;

                        expansionFactor.CatalogKey = dexpansionFactor.catalogkey;
                        expansionFactor.CatalogName = dexpansionFactor.DCatalog.CatalogName;

                        expansionFactor.MarketKey = dexpansionFactor.MarketKey;
                        expansionFactor.MarketName = dexpansionFactor.DMarket.MarketName;

                        expansionFactor.ProductKey = dexpansionFactor.ProductKey;
                        expansionFactor.ProductName = dexpansionFactor.DProduct.ProductName;

                        if (dexpansionFactor.AllowRelease!=0)
                            expansionFactor.AllowRelease = true;
                        else
                            expansionFactor.AllowRelease = false;

                        expansionFactor.BeginDate = dexpansionFactor.Begindate.Date;
                        expansionFactor.EndDate = dexpansionFactor.EndDate.Date;
                        expansionFactor.ExpansionFactorQuantity = dexpansionFactor.ExpFac;
                        expansionFactor.ExpansionFactorValue = dexpansionFactor.ExpFacVal;

                        expansionFactor.ApplicationContext = dexpansionFactor.AppContext.Trim();
                        expansionFactor.RowVersion = dexpansionFactor.RowVersion;
                        expansionFactor.UserName = dexpansionFactor.EntryUid.Trim();

                        expansionFactor.MarkOld();
                        expansionFactors.AddToList(expansionFactor);
                    }

                    result.DataList = expansionFactors;
                    result.Success = true;
                    result.BrowsablePropertyList = ExpansionFactor.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ExpansionFactorGet(ExpansionFactor expansionFactorToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ExpansionFactorListGet(expansionFactorToGet, parameters);
            if (result.Success)
            {
                ExpansionFactor expansionFactor = (ExpansionFactor)result.SingleItem;
                result.ItemIsChanged = (expansionFactorToGet.RowVersion != expansionFactor.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ExpansionFactorSave(ExpansionFactor expansionFactor, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (expansionFactor.IsSavable)
            {
                if (expansionFactor.IsDeleted)
                    result = ExpansionFactorDelete(expansionFactor, parameters);
                else if (expansionFactor.IsNew)
                    result = ExpansionFactorInsert(expansionFactor, parameters);
                else if (expansionFactor.IsDirty)
                    result = ExpansionFactorUpdate(expansionFactor, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ExpansionFactorInsert(ExpansionFactor expansionFactor, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DExpFac dexpansionFactor = new DExpFac();

                    dexpansionFactor.Activitykey = expansionFactor.ActivityKey;
                    dexpansionFactor.catalogkey = expansionFactor.CatalogKey;
                    dexpansionFactor.MarketKey = expansionFactor.MarketKey;
                    dexpansionFactor.ProductKey = expansionFactor.ProductKey;

                    dexpansionFactor.Begindate = expansionFactor.BeginDate.Date;
                    dexpansionFactor.EndDate = expansionFactor.EndDate.Date;
                    dexpansionFactor.ExpFac = (Single)expansionFactor.ExpansionFactorQuantity;
                    dexpansionFactor.ExpFacVal = (Single)expansionFactor.ExpansionFactorValue;

                    if (expansionFactor.AllowRelease)
                        dexpansionFactor.AllowRelease = 1;
                    else
                        dexpansionFactor.AllowRelease = 0;

                    dexpansionFactor.AppContext = parameters.ApplicationContext.Trim();
                    dexpansionFactor.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DExpFac>().InsertOnSubmit(dexpansionFactor);
                    ahamOper.SubmitChanges();

                    expansionFactor.ExpansionFactorKey = dexpansionFactor.expfacKey;
                    expansionFactor.RowVersion = dexpansionFactor.RowVersion;

                    expansionFactor.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ExpansionFactorUpdate(ExpansionFactor expansionFactor, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DExpFac dexpansionFactor = ahamOper.DExpFacs.Single<DExpFac>(x => (x.expfacKey == expansionFactor.ExpansionFactorKey));

                    if (dexpansionFactor.RowVersion != expansionFactor.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + expansionFactor.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dexpansionFactor.Activitykey = expansionFactor.ActivityKey;
                    dexpansionFactor.catalogkey = expansionFactor.CatalogKey;
                    dexpansionFactor.MarketKey = expansionFactor.MarketKey;
                    dexpansionFactor.ProductKey = expansionFactor.ProductKey;

                    dexpansionFactor.Begindate = expansionFactor.BeginDate.Date;
                    dexpansionFactor.EndDate = expansionFactor.EndDate.Date;
                    dexpansionFactor.ExpFac = (Single)expansionFactor.ExpansionFactorQuantity;
                    dexpansionFactor.ExpFacVal = (Single)expansionFactor.ExpansionFactorValue;

                    if (expansionFactor.AllowRelease)
                        dexpansionFactor.AllowRelease = 1;
                    else
                        dexpansionFactor.AllowRelease = 0;

                    dexpansionFactor.AppContext = parameters.ApplicationContext.Trim();
                    dexpansionFactor.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    expansionFactor.MarkOld();
                    expansionFactor.RowVersion = dexpansionFactor.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ExpansionFactorDelete(ExpansionFactor expansionFactor, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DExpFac dexpansionFactor = ahamOper.DExpFacs.Single<DExpFac>(x => (x.expfacKey == expansionFactor.ExpansionFactorKey));

                    if (dexpansionFactor.RowVersion != expansionFactor.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + expansionFactor.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dexpansionFactor.AppContext = parameters.ApplicationContext.Trim();
                    dexpansionFactor.EntryUid = parameters.UserName.Trim();

                    ahamOper.DExpFacs.DeleteOnSubmit(dexpansionFactor);
                    ahamOper.SubmitChanges();

                    expansionFactor.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

    }
}
