﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal static class AhamDataServiceGeography
    {
        #region Geography

        internal static DataAccessResult GeographyListGet(Geography geographyToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Geography> geographies = new HaiBindingList<Geography>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DGeo> DGeos;
                    if (geographyToGet == null)
                        DGeos = ahamOper.DGeos.ToList<DGeo>();
                    else
                    {
                        DGeos = new List<DGeo>();
                        DGeos.Add(ahamOper.DGeos.Single<DGeo>(x => (x.GeoKey == geographyToGet.GeographyKey)));
                    }

                    foreach (DGeo dgeography in DGeos)
                    {
                        Geography geography = new Geography();

                        geography.GeographyAbbreviation = dgeography.GeoAbbrev.Trim();
                        geography.GeographyKey = dgeography.GeoKey;
                        geography.GeographyName = dgeography.GeoName.Trim();

                        // additional properties
                        geography.RegionID = dgeography.RegionID;
                        geography.NationISO = dgeography.NationISO;
                        geography.StateFIPS = dgeography.StateFIPS;
                        geography.CountyFIPS = dgeography.CountyFips;
                        geography.GeographyID = dgeography.GeoID.Trim();
                        geography.GeographySuffix = dgeography.GeoSuffix.Trim();
                        geography.GeographyNameSuffix = dgeography.GeoNameSuffix.Trim();
                        geography.StateVertex = dgeography.StateVertex;
                        geography.CountyVertex = dgeography.CountyVertex;
                        geography.CensusRegionNumber = dgeography.CensusRegionNo;

                        geography.ApplicationContext = dgeography.AppContext.Trim();
                        geography.RowVersion = dgeography.RowVersion;
                        geography.UserName = dgeography.EntryUid.Trim();

                        geography.MarkOld();
                        geographies.AddToList(geography);
                    }

                    result.DataList = geographies;
                    result.Success = true;
                    result.BrowsablePropertyList = Geography.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult GeographyGet(Geography geographyToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = GeographyListGet(geographyToGet, parameters);
            if (result.Success)
            {
                Geography geography = (Geography)result.SingleItem;
                result.ItemIsChanged = (geographyToGet.RowVersion != geography.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult GeographySave(Geography geography, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (geography.IsSavable)
            {
                if (geography.IsDeleted)
                    result = GeographyDelete(geography, parameters);
                else if (geography.IsNew)
                    result = GeographyInsert(geography, parameters);
                else if (geography.IsDirty)
                    result = GeographyUpdate(geography, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult GeographyInsert(Geography geography, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DGeo dgeography = new DGeo();
                    dgeography.GeoAbbrev = geography.GeographyAbbreviation.Trim();
                    dgeography.GeoName = geography.GeographyName.Trim();

                    // additional properties
                    dgeography.RegionID = geography.RegionID;
                    dgeography.NationISO = geography.NationISO;
                    dgeography.StateFIPS = geography.StateFIPS;
                    dgeography.CountyFips = geography.CountyFIPS;
                    dgeography.GeoID = geography.GeographyID;
                    dgeography.GeoSuffix = geography.GeographySuffix;
                    //dgeography.GeoNameSuffix = geography.GeographyNameSuffix;  read-only; value competed in database
                    dgeography.StateVertex = geography.StateVertex;
                    dgeography.CountyVertex = geography.CountyVertex;
                    dgeography.CensusRegionNo = geography.CensusRegionNumber;

                    dgeography.AppContext = parameters.ApplicationContext.Trim();
                    dgeography.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DGeo>().InsertOnSubmit(dgeography);
                    ahamOper.SubmitChanges();

                    geography.GeographyKey = dgeography.GeoKey;
                    geography.RowVersion = dgeography.RowVersion;

                    geography.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult GeographyUpdate(Geography geography, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DGeo dgeography = ahamOper.DGeos.Single<DGeo>(x => (x.GeoKey == geography.GeographyKey));

                    if (dgeography.RowVersion != geography.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + geography.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dgeography.GeoAbbrev = geography.GeographyAbbreviation.Trim();
                    dgeography.GeoName = geography.GeographyName.Trim();

                    // additional properties
                    dgeography.RegionID = geography.RegionID;
                    dgeography.NationISO = geography.NationISO;
                    dgeography.StateFIPS = geography.StateFIPS;
                    dgeography.CountyFips = geography.CountyFIPS;
                    dgeography.GeoID = geography.GeographyID;
                    dgeography.GeoSuffix = geography.GeographySuffix;
                    //dgeography.GeoNameSuffix = geography.GeographyNameSuffix;  read-only; value competed in database
                    dgeography.StateVertex = geography.StateVertex;
                    dgeography.CountyVertex = geography.CountyVertex;
                    dgeography.CensusRegionNo = geography.CensusRegionNumber;

                    dgeography.AppContext = parameters.ApplicationContext.Trim();
                    dgeography.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    geography.MarkOld();
                    geography.RowVersion = dgeography.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult GeographyDelete(Geography geography, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DGeo dgeography = ahamOper.DGeos.Single<DGeo>(x => (x.GeoKey == geography.GeographyKey));

                    if (dgeography.RowVersion != geography.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + geography.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dgeography.AppContext = parameters.ApplicationContext.Trim();
                    dgeography.EntryUid = parameters.UserName.Trim();

                    ahamOper.DGeos.DeleteOnSubmit(dgeography);
                    ahamOper.SubmitChanges();

                    geography.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region GeographySet

        internal static DataAccessResult GeographySetListGet(GeographySet geographySetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<GeographySet> geographySetList = new HaiBindingList<GeographySet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SDGeo> SDGeos;
                    if (geographySetToGet == null)
                        SDGeos = ahamOper.SDGeos.ToList<SDGeo>();
                    else
                    {
                        SDGeos = new List<SDGeo>();
                        SDGeos.Add(ahamOper.SDGeos.Single<SDGeo>(x => (x.GeoSetKey == geographySetToGet.GeographySetKey)));
                    }

                    foreach (SDGeo sdgeography in SDGeos)
                    {
                        GeographySet geographySet = new GeographySet();

                        geographySet.GeographySetAbbreviation = sdgeography.GeoSetAbbrev.Trim();
                        geographySet.GeographySetKey = sdgeography.GeoSetKey;
                        geographySet.GeographySetName = sdgeography.GeoSetName.Trim();
                        geographySet.GeographyLevel = sdgeography.GeoLevel.ToString();
                        //geographySet.HeirarchyId = sdgeography.HeirarchyId;

                        geographySet.ApplicationContext = sdgeography.AppContext.Trim();
                        geographySet.RowVersion = sdgeography.RowVersion;
                        geographySet.UserName = sdgeography.EntryUid.Trim();

                        geographySet.MarkOld();
                        geographySetList.AddToList(geographySet);
                    }

                    result.DataList = geographySetList;
                    result.Success = true;
                    result.BrowsablePropertyList = GeographySet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult GeographySetGet(GeographySet geographySetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = GeographySetListGet(geographySetToGet, parameters);
            if (result.Success)
            {
                GeographySet geographySet = (GeographySet)result.SingleItem;
                result.ItemIsChanged = (geographySetToGet.RowVersion != geographySet.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult GeographySetSave(GeographySet geographySet, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (geographySet.IsSavable)
            {
                if (geographySet.IsDeleted)
                    result = GeographySetDelete(geographySet, parameters);
                else if (geographySet.IsNew)
                    result = GeographySetInsert(geographySet, parameters);
                else if (geographySet.IsDirty)
                    result = GeographySetUpdate(geographySet, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult GeographySetInsert(GeographySet geographySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDGeo sdgeography = new SDGeo();
                    sdgeography.GeoSetAbbrev = geographySet.GeographySetAbbreviation.Trim();
                    sdgeography.GeoSetName = geographySet.GeographySetName.Trim();
                    sdgeography.GeoLevel = (char)geographySet.GeographyLevel[0];
                    //sdgeography.HeirarchyId = geographySet.HeirarchyId;

                    sdgeography.AppContext = parameters.ApplicationContext.Trim();
                    sdgeography.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SDGeo>().InsertOnSubmit(sdgeography);
                    ahamOper.SubmitChanges();

                    geographySet.GeographySetKey = sdgeography.GeoSetKey;
                    geographySet.RowVersion = sdgeography.RowVersion;

                    geographySet.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult GeographySetUpdate(GeographySet geographySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDGeo sdgeography = ahamOper.SDGeos.Single<SDGeo>(x => (x.GeoSetKey == geographySet.GeographySetKey));

                    if (sdgeography.RowVersion != geographySet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + geographySet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdgeography.GeoSetAbbrev = geographySet.GeographySetAbbreviation.Trim();
                    sdgeography.GeoSetName = geographySet.GeographySetName.Trim();
                    sdgeography.GeoLevel = (char)geographySet.GeographyLevel[0];
                    //sdgeography.HeirarchyId = geographySet.HeirarchyId;

                    sdgeography.AppContext = parameters.ApplicationContext.Trim();
                    sdgeography.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    geographySet.MarkOld();
                    geographySet.RowVersion = sdgeography.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult GeographySetDelete(GeographySet geographySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDGeo sdgeography = ahamOper.SDGeos.Single<SDGeo>(x => (x.GeoSetKey == geographySet.GeographySetKey));

                    if (sdgeography.RowVersion != geographySet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + geographySet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdgeography.AppContext = parameters.ApplicationContext.Trim();
                    sdgeography.EntryUid = parameters.UserName.Trim();

                    ahamOper.SDGeos.DeleteOnSubmit(sdgeography);
                    ahamOper.SubmitChanges();

                    geographySet.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region GeographySetMember

        internal static DataAccessResult GeographySetMemberListGet(GeographySetMember geographySetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<GeographySetMember> geographySetMemberList = new HaiBindingList<GeographySetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMGeo> SMGeos;
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<SMGeo>(x => x.DGeo);
                    dlo.LoadWith<SMGeo>(x => x.SDGeo);
                    ahamOper.LoadOptions = dlo;

                    if (geographySetMemberToGet == null)
                    {
                        SMGeos = ahamOper.SMGeos.ToList<SMGeo>();
                    }
                    else
                    {
                        SMGeos = new List<SMGeo>();
                        SMGeos.Add(ahamOper.SMGeos.Single<SMGeo>(x => (x.GeoSetMemberKey == geographySetMemberToGet.GeographySetMemberKey)));
                    }

                    foreach (SMGeo smgeography in SMGeos)
                    {
                        GeographySetMember geographySetMember = new GeographySetMember();

                        geographySetMember.GeographyKey = smgeography.GeoKey;
                        geographySetMember.GeographySetKey = smgeography.GeoSetKey;
                        geographySetMember.GeographySetMemberKey = smgeography.GeoSetMemberKey;
                        geographySetMember.DateRange.BeginDate = smgeography.BeginDate.Date;
                        geographySetMember.DateRange.EndDate = smgeography.EndDate.Date;

                        geographySetMember.GeographyName = smgeography.DGeo.GeoName.Trim();
                        geographySetMember.GeographyAbbreviation = smgeography.DGeo.GeoAbbrev.Trim();
                        geographySetMember.GeographySetAbbreviation = smgeography.SDGeo.GeoSetAbbrev.Trim();
                        geographySetMember.GeographySetName = smgeography.SDGeo.GeoSetName.Trim();


                        // additional properties
                        geographySetMember.RegionID = smgeography.DGeo.RegionID;
                        geographySetMember.NationISO = smgeography.DGeo.NationISO;
                        geographySetMember.StateFIPS = smgeography.DGeo.StateFIPS;
                        geographySetMember.CountyFIPS = smgeography.DGeo.CountyFips;
                        geographySetMember.GeographyID = smgeography.DGeo.GeoID.Trim();
                        geographySetMember.GeographySuffix = smgeography.DGeo.GeoSuffix.Trim();
                        geographySetMember.GeographyNameSuffix = smgeography.DGeo.GeoNameSuffix.Trim();
                        geographySetMember.StateVertex = smgeography.DGeo.StateVertex;
                        geographySetMember.CountyVertex = smgeography.DGeo.CountyVertex;
                        geographySetMember.CensusRegionNumber = smgeography.DGeo.CensusRegionNo;


                        geographySetMember.ApplicationContext = smgeography.AppContext.Trim();
                        geographySetMember.RowVersion = smgeography.RowVersion;
                        geographySetMember.UserName = smgeography.EntryUid.Trim();

                        geographySetMember.MarkOld();
                        geographySetMemberList.AddToList(geographySetMember);
                    }

                    result.DataList = geographySetMemberList;
                    result.Success = true;
                    result.BrowsablePropertyList = GeographySetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult GeographySetMemberGet(GeographySetMember geographySetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = GeographySetMemberListGet(geographySetMemberToGet, parameters);
            if (result.Success)
            {
                GeographySetMember geographySetMember = (GeographySetMember)result.SingleItem;
                result.ItemIsChanged = (geographySetMemberToGet.RowVersion != geographySetMember.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult GeographySetMemberSave(GeographySetMember geographySetMember, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (geographySetMember.IsSavable)
            {
                if (geographySetMember.IsDeleted)
                    result = GeographySetMemberDelete(geographySetMember, parameters);
                else if (geographySetMember.IsNew)
                    result = GeographySetMemberInsert(geographySetMember, parameters);
                else if (geographySetMember.IsDirty)
                    result = GeographySetMemberUpdate(geographySetMember, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult GeographySetMemberInsert(GeographySetMember geographySetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMGeo smgeography = new SMGeo();
                    smgeography.GeoKey = geographySetMember.GeographyKey;
                    smgeography.GeoSetKey = geographySetMember.GeographySetKey;
                    smgeography.BeginDate = geographySetMember.DateRange.BeginDate.Date;
                    smgeography.EndDate = geographySetMember.DateRange.EndDate.Date;

                    smgeography.AppContext = parameters.ApplicationContext.Trim();
                    smgeography.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SMGeo>().InsertOnSubmit(smgeography);
                    ahamOper.SubmitChanges();

                    geographySetMember.GeographySetMemberKey = smgeography.GeoSetMemberKey;
                    geographySetMember.RowVersion = smgeography.RowVersion;

                    geographySetMember.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult GeographySetMemberUpdate(GeographySetMember geographySetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMGeo smgeography = ahamOper.SMGeos.Single<SMGeo>(x => (x.GeoSetMemberKey == geographySetMember.GeographySetMemberKey));

                    if (smgeography.RowVersion != geographySetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + geographySetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smgeography.GeoKey = geographySetMember.GeographyKey;
                    smgeography.GeoSetKey = geographySetMember.GeographySetKey;
                    smgeography.BeginDate = geographySetMember.DateRange.BeginDate.Date;
                    smgeography.EndDate = geographySetMember.DateRange.EndDate.Date;

                    smgeography.AppContext = parameters.ApplicationContext.Trim();
                    smgeography.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    geographySetMember.MarkOld();
                    geographySetMember.RowVersion = smgeography.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult GeographySetMemberDelete(GeographySetMember geographySetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMGeo smgeography = ahamOper.SMGeos.Single<SMGeo>(x => (x.GeoSetMemberKey == geographySetMember.GeographySetMemberKey));

                    if (smgeography.RowVersion != geographySetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + geographySetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smgeography.AppContext = parameters.ApplicationContext.Trim();
                    smgeography.EntryUid = parameters.UserName.Trim();

                    ahamOper.SMGeos.DeleteOnSubmit(smgeography);
                    ahamOper.SubmitChanges();

                    geographySetMember.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult GeographySetMembersDuplicate(GeographySet sourceGeographySet, GeographySet targetGeographySet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMGeo> smGeos;
                    List<SMGeo> duplicatedItems = new List<SMGeo>();

                    smGeos = ahamOper.SMGeos.Where(x => (x.GeoSetKey == sourceGeographySet.GeographySetKey)).ToList<SMGeo>();

                    foreach (SMGeo smGeo in smGeos)
                    {
                        SMGeo duplicateSMGeography = new SMGeo();

                        duplicateSMGeography.GeoSetKey = targetGeographySet.GeographySetKey;
                        duplicateSMGeography.GeoKey = smGeo.GeoKey;
                        duplicateSMGeography.BeginDate = smGeo.BeginDate.Date;
                        duplicateSMGeography.EndDate = smGeo.EndDate.Date;
                        duplicateSMGeography.EntryUid = parameters.UserName.Trim();
                        duplicateSMGeography.AppContext = parameters.ApplicationContext.Trim();

                        duplicatedItems.Add(duplicateSMGeography);
                    }

                    ahamOper.GetTable<SMGeo>().InsertAllOnSubmit<SMGeo>(duplicatedItems);
                    ahamOper.SubmitChanges();

                    result.Success = true;
                    result.BrowsablePropertyList = GeographySetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion
    }
}
