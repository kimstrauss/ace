﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    public class AhamDataServiceGroup
    {

        #region Group

        internal static DataAccessResult GroupListGet(Group groupToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Group> groups = new HaiBindingList<Group>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DGroup> DGroups;
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<DGroup>(x => x.DCouncil);
                    ahamOper.LoadOptions = dlo;

                    if (groupToGet == null)
                        DGroups = ahamOper.DGroups.ToList<DGroup>();
                    else
                    {
                        DGroups = new List<DGroup>();
                        DGroups.Add(ahamOper.DGroups.Single<DGroup>(x => (x.GroupKey == groupToGet.GroupKey)));
                    }

                    foreach (DGroup dgroup in DGroups)
                    {
                        Group group = new Group();

                        group.GroupAbbreviation = dgroup.GroupAbbrev.Trim();
                        group.GroupKey = dgroup.GroupKey;
                        group.GroupName = dgroup.GroupName.Trim();

                        group.CouncilKey = dgroup.CouncilKey;
                        group.RevisionReleaseDate = dgroup.RevisionReleaseDate.Date;
                        group.PreviewRevisionReleaseDate = dgroup.PreviewRevisionReleaseDate.Date;
                        group.CouncilName = dgroup.DCouncil.CouncilName;

                        group.ApplicationContext = dgroup.AppContext.Trim();
                        group.RowVersion = dgroup.RowVersion;
                        group.UserName = dgroup.EntryUid.Trim();

                        group.MarkOld();
                        groups.AddToList(group);
                    }

                    result.DataList = groups;
                    result.Success = true;
                    result.BrowsablePropertyList = Group.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult GroupGet(Group groupToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = GroupListGet(groupToGet, parameters);
            if (result.Success)
            {
                Group group = (Group)result.SingleItem;
                result.ItemIsChanged = (groupToGet.RowVersion != group.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult GroupSave(Group group, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (group.IsSavable)
            {
                if (group.IsDeleted)
                    result = GroupDelete(group, parameters);
                else if (group.IsNew)
                    result = GroupInsert(group, parameters);
                else if (group.IsDirty)
                    result = GroupUpdate(group, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult GroupInsert(Group group, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DGroup dgroup = new DGroup();
                    dgroup.GroupAbbrev = group.GroupAbbreviation.Trim();
                    dgroup.GroupName = group.GroupName.Trim();

                    dgroup.CouncilKey = group.CouncilKey;
                    dgroup.RevisionReleaseDate = group.RevisionReleaseDate.Date;
                    dgroup.PreviewRevisionReleaseDate = group.PreviewRevisionReleaseDate.Date;

                    dgroup.AppContext = parameters.ApplicationContext.Trim();
                    dgroup.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DGroup>().InsertOnSubmit(dgroup);
                    ahamOper.SubmitChanges();

                    group.GroupKey = dgroup.GroupKey;
                    group.RowVersion = dgroup.RowVersion;

                    group.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult GroupUpdate(Group group, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DGroup dgroup = ahamOper.DGroups.Single<DGroup>(x => (x.GroupKey == group.GroupKey));

                    if (dgroup.RowVersion != group.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + group.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dgroup.GroupAbbrev = group.GroupAbbreviation.Trim();
                    dgroup.GroupName = group.GroupName.Trim();

                    dgroup.CouncilKey = group.CouncilKey;
                    dgroup.RevisionReleaseDate = group.RevisionReleaseDate.Date; ;
                    dgroup.PreviewRevisionReleaseDate = group.PreviewRevisionReleaseDate.Date;

                    dgroup.AppContext = parameters.ApplicationContext.Trim();
                    dgroup.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    group.MarkOld();
                    group.RowVersion = dgroup.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult GroupDelete(Group group, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DGroup dgroup = ahamOper.DGroups.Single<DGroup>(x => (x.GroupKey == group.GroupKey));

                    if (dgroup.RowVersion != group.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + group.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dgroup.AppContext = parameters.ApplicationContext.Trim();
                    dgroup.EntryUid = parameters.UserName.Trim();

                    ahamOper.DGroups.DeleteOnSubmit(dgroup);
                    ahamOper.SubmitChanges();

                    group.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion
    }
}
