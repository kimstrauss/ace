﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    public class AhamDataServiceGroupMember
    {

        internal static DataAccessResult GroupMemberListGet(GroupMember groupMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<GroupMember> groupMembers = new HaiBindingList<GroupMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DMGroup> DMGroups;

                    if (groupMemberToGet == null)
                        DMGroups = ahamOper.DMGroups.ToList<DMGroup>();
                    else
                    {
                        DMGroups = new List<DMGroup>();
                        DMGroups.Add(ahamOper.DMGroups.Single<DMGroup>(x => (x.GroupMemberKey == groupMemberToGet.GroupMemberKey)));
                    }

                    foreach (DMGroup dmgroup in DMGroups)
                    {
                        GroupMember groupMember = new GroupMember();

                        groupMember.GroupMemberKey = dmgroup.GroupMemberKey;
                        groupMember.GroupKey = dmgroup.GroupKey;
                        groupMember.ProductKey = dmgroup.ProductKey;

                        groupMember.ApplicationContext = dmgroup.AppContext.Trim();
                        groupMember.RowVersion = dmgroup.RowVersion;
                        groupMember.UserName = dmgroup.EntryUid.Trim();

                        groupMember.MarkOld();
                        groupMembers.AddToList(groupMember);
                    }

                    result.DataList = groupMembers;
                    result.Success = true;
                    result.BrowsablePropertyList = Group.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult GroupMemberGet(GroupMember groupMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = GroupMemberListGet(groupMemberToGet, parameters);
            if (result.Success)
            {
                GroupMember groupMember = (GroupMember)result.SingleItem;
                result.ItemIsChanged = (groupMemberToGet.RowVersion != groupMember.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult GroupMemberSave(GroupMember groupMember, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (groupMember.IsSavable)
            {
                if (groupMember.IsDeleted)
                    result = GroupMemberDelete(groupMember, parameters);
                else if (groupMember.IsNew)
                    result = GroupMemberInsert(groupMember, parameters);
                else if (groupMember.IsDirty)
                    result = GroupMemberUpdate(groupMember, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult GroupMemberInsert(GroupMember groupMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMGroup dmgroup = new DMGroup();
                    dmgroup.GroupKey = groupMember.GroupKey;
                    dmgroup.ProductKey = groupMember.ProductKey;

                    dmgroup.AppContext = parameters.ApplicationContext.Trim();
                    dmgroup.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DMGroup>().InsertOnSubmit(dmgroup);
                    ahamOper.SubmitChanges();

                    groupMember.GroupMemberKey = dmgroup.GroupMemberKey;
                    groupMember.RowVersion = dmgroup.RowVersion;

                    groupMember.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult GroupMemberUpdate(GroupMember groupMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMGroup dmgroup = ahamOper.DMGroups.Single<DMGroup>(x => (x.GroupMemberKey == groupMember.GroupMemberKey));

                    if (dmgroup.RowVersion != groupMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + groupMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dmgroup.GroupKey = groupMember.GroupKey;
                    dmgroup.ProductKey = groupMember.ProductKey;

                    dmgroup.AppContext = parameters.ApplicationContext.Trim();
                    dmgroup.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    groupMember.MarkOld();
                    groupMember.RowVersion = dmgroup.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult GroupMemberDelete(GroupMember groupMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMGroup dmgroup = ahamOper.DMGroups.Single<DMGroup>(x => (x.GroupMemberKey == groupMember.GroupMemberKey));

                    if (dmgroup.RowVersion != groupMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + groupMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dmgroup.AppContext = parameters.ApplicationContext.Trim();
                    dmgroup.EntryUid = parameters.UserName.Trim();

                    ahamOper.DMGroups.DeleteOnSubmit(dmgroup);
                    ahamOper.SubmitChanges();

                    groupMember.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }
    }
}
