﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using AhamDataLinq;

using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal static class AhamDataServiceMarket
    {

        #region Market

        internal static DataAccessResult MarketListGet(Market marketToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Market> markets = new HaiBindingList<Market>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DMarket> DMarkets;
                    if (marketToGet == null)
                        DMarkets = ahamOper.DMarkets.ToList<DMarket>();
                    else
                    {
                        DMarkets = new List<DMarket>();
                        DMarkets.Add(ahamOper.DMarkets.Single<DMarket>(x => (x.MarketKey == marketToGet.MarketKey)));
                    }

                    foreach (DMarket dmarket in DMarkets)
                    {
                        Market market = new Market();

                        market.MarketAbbreviation = dmarket.MarketAbbrev.Trim();
                        market.MarketKey = dmarket.MarketKey;
                        market.MarketName = dmarket.MarketName.Trim();

                        market.ApplicationContext = dmarket.AppContext.Trim();
                        market.RowVersion = dmarket.RowVersion;
                        market.UserName = dmarket.EntryUid.Trim();

                        market.MarkOld();
                        markets.AddToList(market);
                    }

                    result.DataList = markets;
                    result.Success = true;
                    result.BrowsablePropertyList = Market.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult MarketGet(Market marketToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = MarketListGet(marketToGet, parameters);
            if (result.Success)
            {
                Market market = (Market)result.SingleItem;
                result.ItemIsChanged = (marketToGet.RowVersion != market.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult MarketSave(Market market, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (market.IsSavable)
            {
                if (market.IsDeleted)
                    result = MarketDelete(market, parameters);
                else if (market.IsNew)
                    result = MarketInsert(market, parameters);
                else if (market.IsDirty)
                    result = MarketUpdate(market, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult MarketInsert(Market market, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMarket dmarket = new DMarket();
                    dmarket.MarketAbbrev = market.MarketAbbreviation.Trim();
                    dmarket.MarketName = market.MarketName.Trim();

                    dmarket.AppContext = parameters.ApplicationContext.Trim();
                    dmarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DMarket>().InsertOnSubmit(dmarket);
                    ahamOper.SubmitChanges();

                    market.MarketKey = dmarket.MarketKey;
                    market.RowVersion = dmarket.RowVersion;

                    market.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MarketUpdate(Market market, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMarket dmarket = ahamOper.DMarkets.Single<DMarket>(x => (x.MarketKey == market.MarketKey));

                    if (dmarket.RowVersion != market.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + market.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dmarket.MarketAbbrev = market.MarketAbbreviation.Trim();
                    dmarket.MarketName = market.MarketName.Trim();

                    dmarket.AppContext = parameters.ApplicationContext.Trim();
                    dmarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    market.MarkOld();
                    market.RowVersion = dmarket.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MarketDelete(Market market, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMarket dmarket = ahamOper.DMarkets.Single<DMarket>(x => (x.MarketKey == market.MarketKey));

                    if (dmarket.RowVersion != market.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + market.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dmarket.AppContext = parameters.ApplicationContext.Trim();
                    dmarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.DMarkets.DeleteOnSubmit(dmarket);
                    ahamOper.SubmitChanges();

                    market.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region MarketSet

        internal static DataAccessResult MarketSetListGet(MarketSet marketSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<MarketSet> marketSetList = new HaiBindingList<MarketSet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SDMarket> SDMarkets;
                    if (marketSetToGet == null)
                        SDMarkets = ahamOper.SDMarkets.ToList<SDMarket>();
                    else
                    {
                        SDMarkets = new List<SDMarket>();
                        SDMarkets.Add(ahamOper.SDMarkets.Single<SDMarket>(x => (x.MarketSetKey == marketSetToGet.MarketSetKey)));
                    }

                    foreach (SDMarket sdmarket in SDMarkets)
                    {
                        MarketSet marketSet = new MarketSet();

                        marketSet.MarketSetAbbreviation = sdmarket.MarketSetAbbrev.Trim();
                        marketSet.MarketSetKey = sdmarket.MarketSetKey;
                        marketSet.MarketSetName = sdmarket.MarketSetName.Trim();

                        marketSet.ApplicationContext = sdmarket.AppContext.Trim();
                        marketSet.RowVersion = sdmarket.RowVersion;
                        marketSet.UserName = sdmarket.EntryUid.Trim();

                        marketSet.MarkOld();
                        marketSetList.AddToList(marketSet);
                    }

                    result.DataList = marketSetList;
                    result.Success = true;
                    result.BrowsablePropertyList = MarketSet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult MarketSetGet(MarketSet marketSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = MarketSetListGet(marketSetToGet, parameters);
            if (result.Success)
            {
                MarketSet marketSet = (MarketSet)result.SingleItem;
                result.ItemIsChanged = (marketSetToGet.RowVersion != marketSet.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult MarketSetSave(MarketSet marketSet, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (marketSet.IsSavable)
            {
                if (marketSet.IsDeleted)
                    result = MarketSetDelete(marketSet, parameters);
                else if (marketSet.IsNew)
                    result = MarketSetInsert(marketSet, parameters);
                else if (marketSet.IsDirty)
                    result = MarketSetUpdate(marketSet, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult MarketSetInsert(MarketSet marketSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDMarket sdmarket = new SDMarket();
                    sdmarket.MarketSetAbbrev = marketSet.MarketSetAbbreviation.Trim();
                    sdmarket.MarketSetName = marketSet.MarketSetName.Trim();

                    sdmarket.AppContext = parameters.ApplicationContext.Trim();
                    sdmarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SDMarket>().InsertOnSubmit(sdmarket);
                    ahamOper.SubmitChanges();

                    marketSet.MarketSetKey = sdmarket.MarketSetKey;
                    marketSet.RowVersion = sdmarket.RowVersion;

                    marketSet.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MarketSetUpdate(MarketSet marketSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDMarket sdmarket = ahamOper.SDMarkets.Single<SDMarket>(x => (x.MarketSetKey == marketSet.MarketSetKey));

                    if (sdmarket.RowVersion != marketSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + marketSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdmarket.MarketSetAbbrev = marketSet.MarketSetAbbreviation.Trim();
                    sdmarket.MarketSetName = marketSet.MarketSetName.Trim();

                    sdmarket.AppContext = parameters.ApplicationContext.Trim();
                    sdmarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    marketSet.MarkOld();
                    marketSet.RowVersion = sdmarket.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MarketSetDelete(MarketSet marketSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDMarket sdmarket = ahamOper.SDMarkets.Single<SDMarket>(x => (x.MarketSetKey == marketSet.MarketSetKey));

                    if (sdmarket.RowVersion != marketSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + marketSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdmarket.AppContext = parameters.ApplicationContext.Trim();
                    sdmarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.SDMarkets.DeleteOnSubmit(sdmarket);
                    ahamOper.SubmitChanges();

                    marketSet.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region MarketSetMember

        internal static DataAccessResult MarketSetMemberListGet(MarketSetMember marketSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<MarketSetMember> marketSetMemberList = new HaiBindingList<MarketSetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMMarket> SMMarkets;
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<SMMarket>(x => x.DMarket);
                    dlo.LoadWith<SMMarket>(x => x.SDMarket);
                    ahamOper.LoadOptions = dlo;

                    if (marketSetMemberToGet == null)
                    {
                        SMMarkets = ahamOper.SMMarkets.ToList<SMMarket>();
                    }
                    else
                    {
                        SMMarkets = new List<SMMarket>();
                        SMMarkets.Add(ahamOper.SMMarkets.Single<SMMarket>(x => (x.MarketSetMemberKey == marketSetMemberToGet.MarketSetMemberKey)));
                    }

                    foreach (SMMarket smmarket in SMMarkets)
                    {
                        MarketSetMember marketSetMember = new MarketSetMember();

                        marketSetMember.MarketKey = smmarket.MarketKey;
                        marketSetMember.MarketSetKey = smmarket.MarketSetKey;
                        marketSetMember.MarketSetMemberKey = smmarket.MarketSetMemberKey;
                        marketSetMember.DateRange.BeginDate = smmarket.BeginDate.Date;
                        marketSetMember.DateRange.EndDate = smmarket.EndDate.Date;

                        marketSetMember.MarketName = smmarket.DMarket.MarketName.Trim();
                        marketSetMember.MarketAbbreviation = smmarket.DMarket.MarketAbbrev.Trim();
                        marketSetMember.MarketSetAbbreviation = smmarket.SDMarket.MarketSetAbbrev.Trim();
                        marketSetMember.MarketSetName = smmarket.SDMarket.MarketSetName.Trim();

                        marketSetMember.ApplicationContext = smmarket.AppContext.Trim();
                        marketSetMember.RowVersion = smmarket.RowVersion;
                        marketSetMember.UserName = smmarket.EntryUid.Trim();

                        marketSetMember.MarkOld();
                        marketSetMemberList.AddToList(marketSetMember);
                    }

                    result.DataList = marketSetMemberList;
                    result.Success = true;
                    result.BrowsablePropertyList = MarketSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult MarketSetMemberGet(MarketSetMember marketSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = MarketSetMemberListGet(marketSetMemberToGet, parameters);
            if (result.Success)
            {
                MarketSetMember marketSetMember = (MarketSetMember)result.SingleItem;
                result.ItemIsChanged = (marketSetMemberToGet.RowVersion != marketSetMember.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult MarketSetMemberSave(MarketSetMember marketSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (marketSetMember.IsSavable)
            {
                if (marketSetMember.IsDeleted)
                    result = MarketSetMemberDelete(marketSetMember, parameters);
                else if (marketSetMember.IsNew)
                    result = MarketSetMemberInsert(marketSetMember, parameters);
                else if (marketSetMember.IsDirty)
                    result = MarketSetMemberUpdate(marketSetMember, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult MarketSetMemberInsert(MarketSetMember marketSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMMarket smmarket = new SMMarket();
                    smmarket.MarketKey = marketSetMember.MarketKey;
                    smmarket.MarketSetKey = marketSetMember.MarketSetKey;
                    smmarket.BeginDate = marketSetMember.DateRange.BeginDate.Date;
                    smmarket.EndDate = marketSetMember.DateRange.EndDate.Date;

                    smmarket.AppContext = parameters.ApplicationContext.Trim();
                    smmarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SMMarket>().InsertOnSubmit(smmarket);
                    ahamOper.SubmitChanges();

                    marketSetMember.MarketSetMemberKey = smmarket.MarketSetMemberKey;
                    marketSetMember.RowVersion = smmarket.RowVersion;

                    marketSetMember.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MarketSetMemberUpdate(MarketSetMember marketSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMMarket smmarket = ahamOper.SMMarkets.Single<SMMarket>(x => (x.MarketSetMemberKey == marketSetMember.MarketSetMemberKey));

                    if (smmarket.RowVersion != marketSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + marketSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smmarket.MarketKey = marketSetMember.MarketKey;
                    smmarket.MarketSetKey = marketSetMember.MarketSetKey;
                    smmarket.BeginDate = marketSetMember.DateRange.BeginDate.Date;
                    smmarket.EndDate = marketSetMember.DateRange.EndDate.Date;

                    smmarket.AppContext = parameters.ApplicationContext.Trim();
                    smmarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    marketSetMember.MarkOld();
                    marketSetMember.RowVersion = smmarket.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MarketSetMemberDelete(MarketSetMember marketSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMMarket smmarket = ahamOper.SMMarkets.Single<SMMarket>(x => (x.MarketSetMemberKey == marketSetMember.MarketSetMemberKey));

                    if (smmarket.RowVersion != marketSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + marketSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smmarket.AppContext = parameters.ApplicationContext.Trim();
                    smmarket.EntryUid = parameters.UserName.Trim();

                    ahamOper.SMMarkets.DeleteOnSubmit(smmarket);
                    ahamOper.SubmitChanges();

                    marketSetMember.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult MarketSetMembersDuplicate(MarketSet sourceMarketSet, MarketSet targetMarketSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMMarket> smMarkets;
                    List<SMMarket> duplicatedItems = new List<SMMarket>();

                    smMarkets = ahamOper.SMMarkets.Where(x => (x.MarketSetKey == sourceMarketSet.MarketSetKey)).ToList<SMMarket>();

                    foreach (SMMarket smMarket in smMarkets)
                    {
                        SMMarket duplicateSMMarket = new SMMarket();

                        duplicateSMMarket.MarketSetKey = targetMarketSet.MarketSetKey;
                        duplicateSMMarket.MarketKey = smMarket.MarketKey;
                        duplicateSMMarket.BeginDate = smMarket.BeginDate.Date;
                        duplicateSMMarket.EndDate = smMarket.EndDate.Date;
                        duplicateSMMarket.EntryUid = parameters.UserName.Trim();
                        duplicateSMMarket.AppContext = parameters.ApplicationContext.Trim();

                        duplicatedItems.Add(duplicateSMMarket);
                    }

                    ahamOper.GetTable<SMMarket>().InsertAllOnSubmit<SMMarket>(duplicatedItems);
                    ahamOper.SubmitChanges();

                    result.Success = true;
                    result.BrowsablePropertyList = MarketSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

    }
}
