﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal static class AhamDataServiceMeasure
    {

        #region Measure

        internal static DataAccessResult MeasureListGet(Measure measureToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Measure> measures = new HaiBindingList<Measure>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DMeasure> DMeasures;
                    if (measureToGet == null)
                        DMeasures = ahamOper.DMeasures.ToList<DMeasure>();
                    else
                    {
                        DMeasures = new List<DMeasure>();
                        DMeasures.Add(ahamOper.DMeasures.Single<DMeasure>(x => (x.MeasureKey == measureToGet.MeasureKey)));
                    }

                    foreach (DMeasure dmeasure in DMeasures)
                    {
                        Measure measure = new Measure();

                        measure.MeasureAbbreviation = dmeasure.MeasureAbbrev.Trim();
                        measure.MeasureKey = dmeasure.MeasureKey;
                        measure.MeasureName = dmeasure.MeasureName.Trim();

                        measure.ApplicationContext = dmeasure.AppContext.Trim();
                        measure.RowVersion = dmeasure.RowVersion;
                        measure.UserName = dmeasure.EntryUid.Trim();

                        measure.MarkOld();
                        measures.AddToList(measure);
                    }

                    result.DataList = measures;
                    result.Success = true;
                    result.BrowsablePropertyList = Measure.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult MeasureGet(Measure measureToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = MeasureListGet(measureToGet, parameters);
            if (result.Success)
            {
                Measure measure = (Measure)result.SingleItem;
                result.ItemIsChanged = (measureToGet.RowVersion != measure.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult MeasureSave(Measure measure, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (measure.IsSavable)
            {
                if (measure.IsDeleted)
                    result = MeasureDelete(measure, parameters);
                else if (measure.IsNew)
                    result = MeasureInsert(measure, parameters);
                else if (measure.IsDirty)
                    result = MeasureUpdate(measure, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult MeasureInsert(Measure measure, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMeasure dmeasure = new DMeasure();
                    dmeasure.MeasureAbbrev = measure.MeasureAbbreviation.Trim();
                    dmeasure.MeasureName = measure.MeasureName.Trim();

                    dmeasure.AppContext = parameters.ApplicationContext.Trim();
                    dmeasure.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DMeasure>().InsertOnSubmit(dmeasure);
                    ahamOper.SubmitChanges();

                    measure.MeasureKey = dmeasure.MeasureKey;
                    measure.RowVersion = dmeasure.RowVersion;

                    measure.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MeasureUpdate(Measure measure, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMeasure dmeasure = ahamOper.DMeasures.Single<DMeasure>(x => (x.MeasureKey == measure.MeasureKey));

                    if (dmeasure.RowVersion != measure.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + measure.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dmeasure.MeasureAbbrev = measure.MeasureAbbreviation.Trim();
                    dmeasure.MeasureName = measure.MeasureName.Trim();

                    dmeasure.AppContext = parameters.ApplicationContext.Trim();
                    dmeasure.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    measure.MarkOld();
                    measure.RowVersion = dmeasure.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MeasureDelete(Measure measure, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DMeasure dmeasure = ahamOper.DMeasures.Single<DMeasure>(x => (x.MeasureKey == measure.MeasureKey));

                    if (dmeasure.RowVersion != measure.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + measure.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dmeasure.AppContext = parameters.ApplicationContext.Trim();
                    dmeasure.EntryUid = parameters.UserName.Trim();

                    ahamOper.DMeasures.DeleteOnSubmit(dmeasure);
                    ahamOper.SubmitChanges();

                    measure.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region MeasureSet

        internal static DataAccessResult MeasureSetListGet(MeasureSet measureSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<MeasureSet> measureSetList = new HaiBindingList<MeasureSet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SDMeasure> SDMeasures;
                    if (measureSetToGet == null)
                        SDMeasures = ahamOper.SDMeasures.ToList<SDMeasure>();
                    else
                    {
                        SDMeasures = new List<SDMeasure>();
                        SDMeasures.Add(ahamOper.SDMeasures.Single<SDMeasure>(x => (x.MeasureSetKey == measureSetToGet.MeasureSetKey)));
                    }

                    foreach (SDMeasure sdmeasure in SDMeasures)
                    {
                        MeasureSet measureSet = new MeasureSet();

                        measureSet.MeasureSetAbbreviation = sdmeasure.MeasureSetAbbrev.Trim();
                        measureSet.MeasureSetKey = sdmeasure.MeasureSetKey;
                        measureSet.MeasureSetName = sdmeasure.MeasureSetName.Trim();

                        measureSet.ApplicationContext = sdmeasure.AppContext.Trim();
                        measureSet.RowVersion = sdmeasure.RowVersion;
                        measureSet.UserName = sdmeasure.EntryUid.Trim();

                        measureSet.MarkOld();
                        measureSetList.AddToList(measureSet);
                    }

                    result.DataList = measureSetList;
                    result.Success = true;
                    result.BrowsablePropertyList = MeasureSet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult MeasureSetGet(MeasureSet measureSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = MeasureSetListGet(measureSetToGet, parameters);
            if (result.Success)
            {
                MeasureSet measureSet = (MeasureSet)result.SingleItem;
                result.ItemIsChanged = (measureSetToGet.RowVersion != measureSet.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult MeasureSetSave(MeasureSet measureSet, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (measureSet.IsSavable)
            {
                if (measureSet.IsDeleted)
                    result = MeasureSetDelete(measureSet, parameters);
                else if (measureSet.IsNew)
                    result = MeasureSetInsert(measureSet, parameters);
                else if (measureSet.IsDirty)
                    result = MeasureSetUpdate(measureSet, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult MeasureSetInsert(MeasureSet measureSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDMeasure sdmeasure = new SDMeasure();
                    sdmeasure.MeasureSetAbbrev = measureSet.MeasureSetAbbreviation.Trim();
                    sdmeasure.MeasureSetName = measureSet.MeasureSetName.Trim();

                    sdmeasure.AppContext = parameters.ApplicationContext.Trim();
                    sdmeasure.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SDMeasure>().InsertOnSubmit(sdmeasure);
                    ahamOper.SubmitChanges();

                    measureSet.MeasureSetKey = sdmeasure.MeasureSetKey;
                    measureSet.RowVersion = sdmeasure.RowVersion;

                    measureSet.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MeasureSetUpdate(MeasureSet measureSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDMeasure sdmeasure = ahamOper.SDMeasures.Single<SDMeasure>(x => (x.MeasureSetKey == measureSet.MeasureSetKey));

                    if (sdmeasure.RowVersion != measureSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + measureSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdmeasure.MeasureSetAbbrev = measureSet.MeasureSetAbbreviation.Trim();
                    sdmeasure.MeasureSetName = measureSet.MeasureSetName.Trim();

                    sdmeasure.AppContext = parameters.ApplicationContext.Trim();
                    sdmeasure.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    measureSet.MarkOld();
                    measureSet.RowVersion = sdmeasure.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MeasureSetDelete(MeasureSet measureSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDMeasure sdmeasure = ahamOper.SDMeasures.Single<SDMeasure>(x => (x.MeasureSetKey == measureSet.MeasureSetKey));

                    if (sdmeasure.RowVersion != measureSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + measureSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdmeasure.AppContext = parameters.ApplicationContext.Trim();
                    sdmeasure.EntryUid = parameters.UserName.Trim();

                    ahamOper.SDMeasures.DeleteOnSubmit(sdmeasure);
                    ahamOper.SubmitChanges();

                    measureSet.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region MeasureSetMember

        internal static DataAccessResult MeasureSetMemberListGet(MeasureSetMember measureSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<MeasureSetMember> measureSetMemberList = new HaiBindingList<MeasureSetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMMeasure> SMMeasures;
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<SMMeasure>(x => x.DMeasure);
                    dlo.LoadWith<SMMeasure>(x => x.SDMeasure);
                    ahamOper.LoadOptions = dlo;

                    if (measureSetMemberToGet == null)
                    {
                        SMMeasures = ahamOper.SMMeasures.ToList<SMMeasure>();
                    }
                    else
                    {
                        SMMeasures = new List<SMMeasure>();
                        SMMeasures.Add(ahamOper.SMMeasures.Single<SMMeasure>(x => (x.MeasureSetMemberKey == measureSetMemberToGet.MeasureSetMemberKey)));
                    }

                    foreach (SMMeasure smmeasure in SMMeasures)
                    {
                        MeasureSetMember measureSetMember = new MeasureSetMember();

                        measureSetMember.MeasureKey = smmeasure.MeasureKey;
                        measureSetMember.MeasureSetKey = smmeasure.MeasureSetKey;
                        measureSetMember.MeasureSetMemberKey = smmeasure.MeasureSetMemberKey;
                        measureSetMember.DateRange.BeginDate = smmeasure.BeginDate.Date;
                        measureSetMember.DateRange.EndDate = smmeasure.EndDate.Date;

                        measureSetMember.MeasureName = smmeasure.DMeasure.MeasureName.Trim();
                        measureSetMember.MeasureAbbreviation = smmeasure.DMeasure.MeasureAbbrev.Trim();
                        measureSetMember.MeasureSetAbbreviation = smmeasure.SDMeasure.MeasureSetAbbrev.Trim();
                        measureSetMember.MeasureSetName = smmeasure.SDMeasure.MeasureSetName.Trim();

                        // Additional properties
                        measureSetMember.GroupNumber = smmeasure.GroupNo;

                        measureSetMember.ApplicationContext = smmeasure.AppContext.Trim();
                        measureSetMember.RowVersion = smmeasure.RowVersion;
                        measureSetMember.UserName = smmeasure.EntryUid.Trim();

                        measureSetMember.MarkOld();
                        measureSetMemberList.AddToList(measureSetMember);
                    }

                    result.DataList = measureSetMemberList;
                    result.Success = true;
                    result.BrowsablePropertyList = MeasureSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult MeasureSetMemberGet(MeasureSetMember measureSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = MeasureSetMemberListGet(measureSetMemberToGet, parameters);
            if (result.Success)
            {
                MeasureSetMember measureSetMember = (MeasureSetMember)result.SingleItem;
                result.ItemIsChanged = (measureSetMemberToGet.RowVersion != measureSetMember.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult MeasureSetMemberSave(MeasureSetMember measureSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (measureSetMember.IsSavable)
            {
                if (measureSetMember.IsDeleted)
                    result = MeasureSetMemberDelete(measureSetMember, parameters);
                else if (measureSetMember.IsNew)
                    result = MeasureSetMemberInsert(measureSetMember, parameters);
                else if (measureSetMember.IsDirty)
                    result = MeasureSetMemberUpdate(measureSetMember, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult MeasureSetMemberInsert(MeasureSetMember measureSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMMeasure smmeasure = new SMMeasure();
                    smmeasure.MeasureKey = measureSetMember.MeasureKey;
                    smmeasure.MeasureSetKey = measureSetMember.MeasureSetKey;
                    smmeasure.BeginDate = measureSetMember.DateRange.BeginDate.Date;
                    smmeasure.EndDate = measureSetMember.DateRange.EndDate.Date;

                    // Addtional properties
                    smmeasure.GroupNo = (short)measureSetMember.GroupNumber;

                    smmeasure.AppContext = parameters.ApplicationContext.Trim();
                    smmeasure.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SMMeasure>().InsertOnSubmit(smmeasure);
                    ahamOper.SubmitChanges();

                    measureSetMember.MeasureSetMemberKey = smmeasure.MeasureSetMemberKey;
                    measureSetMember.RowVersion = smmeasure.RowVersion;

                    measureSetMember.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MeasureSetMemberUpdate(MeasureSetMember measureSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMMeasure smmeasure = ahamOper.SMMeasures.Single<SMMeasure>(x => (x.MeasureSetMemberKey == measureSetMember.MeasureSetMemberKey));

                    if (smmeasure.RowVersion != measureSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + measureSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smmeasure.MeasureKey = measureSetMember.MeasureKey;
                    smmeasure.MeasureSetKey = measureSetMember.MeasureSetKey;
                    smmeasure.BeginDate = measureSetMember.DateRange.BeginDate.Date;
                    smmeasure.EndDate = measureSetMember.DateRange.EndDate.Date;

                    // Addtional properties
                    smmeasure.GroupNo = (short)measureSetMember.GroupNumber;

                    smmeasure.AppContext = parameters.ApplicationContext.Trim();
                    smmeasure.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    measureSetMember.MarkOld();
                    measureSetMember.RowVersion = smmeasure.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult MeasureSetMemberDelete(MeasureSetMember measureSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMMeasure smmeasure = ahamOper.SMMeasures.Single<SMMeasure>(x => (x.MeasureSetMemberKey == measureSetMember.MeasureSetMemberKey));

                    if (smmeasure.RowVersion != measureSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + measureSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smmeasure.AppContext = parameters.ApplicationContext.Trim();
                    smmeasure.EntryUid = parameters.UserName.Trim();

                    ahamOper.SMMeasures.DeleteOnSubmit(smmeasure);
                    ahamOper.SubmitChanges();

                    measureSetMember.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult MeasureSetMembersDuplicate(MeasureSet sourceMeasureSet, MeasureSet targetMeasureSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMMeasure> smMeasures;
                    List<SMMeasure> duplicatedItems = new List<SMMeasure>();

                    smMeasures = ahamOper.SMMeasures.Where(x => (x.MeasureSetKey == sourceMeasureSet.MeasureSetKey)).ToList<SMMeasure>();

                    foreach (SMMeasure smMeasure in smMeasures)
                    {
                        SMMeasure duplicateSMMeasure = new SMMeasure();

                        duplicateSMMeasure.MeasureSetKey = targetMeasureSet.MeasureSetKey;
                        duplicateSMMeasure.MeasureKey = smMeasure.MeasureKey;

                        duplicateSMMeasure.GroupNo = smMeasure.GroupNo;
                        duplicateSMMeasure.MeasureRulesKey = smMeasure.MeasureRulesKey;
                        duplicateSMMeasure.MeasureGroupRulesKey = smMeasure.MeasureGroupRulesKey;

                        duplicateSMMeasure.BeginDate = smMeasure.BeginDate.Date;
                        duplicateSMMeasure.EndDate = smMeasure.EndDate.Date;
                        duplicateSMMeasure.EntryUid = parameters.UserName.Trim();
                        duplicateSMMeasure.AppContext = parameters.ApplicationContext.Trim();

                        duplicatedItems.Add(duplicateSMMeasure);
                    }

                    ahamOper.GetTable<SMMeasure>().InsertAllOnSubmit<SMMeasure>(duplicatedItems);
                    ahamOper.SubmitChanges();

                    result.Success = true;
                    result.BrowsablePropertyList = MeasureSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

    }
}
