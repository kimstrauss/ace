﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal static class AhamDataServiceProduct
    {

        #region Product

        internal static DataAccessResult ProductListGet(Product productToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Product> products = new HaiBindingList<Product>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataService dataService = new AhamDataService(parameters);

                    DataAccessResult groupResult = dataService.GetDataList(HaiBusinessObjectType.Group);
                    Dictionary<int, Group> groupDict = new Dictionary<int, Group>();
                    if (groupResult.Success)
                    {
                        HaiBindingList<Group> groupList = (HaiBindingList<Group>)groupResult.DataList;
                        foreach (Group group in groupList)
                        {
                            groupDict.Add(group.GroupKey, group);
                        }
                    }
                    else
                    {
                        return groupResult;
                    }

                    DataAccessResult groupMemberResult = dataService.GetDataList(HaiBusinessObjectType.GroupMember);
                    Dictionary<int, GroupMember> groupMemberDict = new Dictionary<int, GroupMember>();
                    if (groupMemberResult.Success)
                    {
                        HaiBindingList<GroupMember> groupMemberList = (HaiBindingList<GroupMember>)groupMemberResult.DataList;
                        foreach (GroupMember groupMember in groupMemberList)
                        {
                            groupMemberDict.Add(groupMember.ProductKey, groupMember);
                        }
                    }
                    else
                    {
                        return groupMemberResult;
                    }

                    List<DProduct> DProducts;
                    List<DProduct> ProductLookupList = ahamOper.DProducts.ToList<DProduct>();

                    // use a dictionary to get values for the self-reference join to get "ProductOwnerName"
                    Dictionary<int, DProduct> productDict = ProductLookupList.ToDictionary(x => x.ProductKey);

                    if (productToGet == null)
                    {
                        DProducts = ProductLookupList;
                    }
                    else
                    {
                        // get the single object from from the dictionary
                        DProducts = new List<DProduct>();
                        DProducts.Add(productDict[productToGet.ProductKey]);
                    }

                    foreach (DProduct dproduct in DProducts)
                    {
                        Product product = new Product();

                        product.ProductAbbreviation = dproduct.ProductAbbrev.Trim();
                        product.ProductKey = dproduct.ProductKey;
                        product.ProductName = dproduct.ProductName.Trim();

                        product.ProductOwnerKey = dproduct.ProductOwnerKey;

                        // get the "ProductOwnerName" from the dictionary of products, if the key is not null
                        if (dproduct.ProductOwnerKey.HasValue)
                            product.ProductOwnerName = productDict[dproduct.ProductOwnerKey.Value].ProductName.Trim();
                        else
                            product.ProductOwnerName = "";

                        if (groupMemberDict.ContainsKey(product.ProductKey))
                        {
                            GroupMember groupMember = groupMemberDict[product.ProductKey];
                            product.GroupKey = groupMember.GroupKey;
                            product.GroupMemberKey = groupMember.GroupMemberKey;
                            if (groupDict.ContainsKey(groupMember.GroupKey))
                                product.GroupName = groupDict[groupMember.GroupKey].GroupName;
                            else
                                product.GroupName = string.Empty;
                        }
                        else
                        {
                            product.GroupKey = null;
                            product.GroupMemberKey = null;
                            product.GroupName = string.Empty;
                        }

                        product.ApplicationContext = dproduct.AppContext.Trim();
                        product.RowVersion = dproduct.RowVersion;
                        product.UserName = dproduct.EntryUid.Trim();

                        product.MarkOld();
                        products.AddToList(product);
                    }

                    result.DataList = products;
                    result.Success = true;
                    result.BrowsablePropertyList = Product.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ProductGet(Product productToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ProductListGet(productToGet, parameters);
            if (result.Success)
            {
                Product product = (Product)result.SingleItem;
                result.ItemIsChanged = (productToGet.RowVersion != product.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ProductSave(Product product, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (product.IsSavable)
            {
                if (product.IsDeleted)
                    result = ProductDelete(product, parameters);
                else if (product.IsNew)
                    result = ProductInsert(product, parameters);
                else if (product.IsDirty)
                    result = ProductUpdate(product, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ProductInsert(Product product, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DProduct dproduct = new DProduct();

                    dproduct.ProductAbbrev = product.ProductAbbreviation.Trim();
                    dproduct.ProductName = product.ProductName.Trim();
                    dproduct.ProductNameForFlash = string.Empty;

                    dproduct.ProductOwnerKey = product.ProductOwnerKey;

                    dproduct.AppContext = parameters.ApplicationContext.Trim();
                    dproduct.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DProduct>().InsertOnSubmit(dproduct);
                    ahamOper.SubmitChanges();

                    product.ProductKey = dproduct.ProductKey;
                    product.RowVersion = dproduct.RowVersion;

                    product.MarkOld();
                    result.Success = true;

                    if (product.GroupKey != null)
                    {
                        AhamDataService dataService = new AhamDataService(parameters);

                        GroupMember groupMember = new GroupMember();
                        groupMember.GroupKey = product.GroupKey.Value;
                        groupMember.ProductKey = product.ProductKey;

                        DataAccessResult groupResult = dataService.Save(groupMember);
                        if (groupResult.Success)
                        {
                            product.GroupMemberKey = groupMember.GroupMemberKey;
                        }
                        else
                        {
                            return groupResult;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ProductUpdate(Product product, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DProduct dproduct = ahamOper.DProducts.Single<DProduct>(x => (x.ProductKey == product.ProductKey));

                    if (dproduct.RowVersion != product.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + product.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dproduct.ProductAbbrev = product.ProductAbbreviation.Trim();
                    dproduct.ProductName = product.ProductName.Trim();

                    dproduct.ProductOwnerKey = product.ProductOwnerKey;

                    dproduct.AppContext = parameters.ApplicationContext.Trim();
                    dproduct.EntryUid = parameters.UserName.Trim();

                    // get the DMGroup (group member) record for this product
                    DMGroup dmgroup;

                    int memberCount = (ahamOper.DMGroups.Where<DMGroup>(x => (x.ProductKey == product.ProductKey))).Count<DMGroup>();
                    if (memberCount == 1)
                        dmgroup = ahamOper.DMGroups.Single<DMGroup>(x => (x.ProductKey == product.ProductKey));
                    else
                        dmgroup = null;

                    if ((dmgroup == null) && (product.GroupKey == null))
                    {
                        // there was no group member for this product and none is being set,
                        // so do nothing because there is no change
                    }

                    AhamDataService dataService = new AhamDataService(parameters);
                    if ((dmgroup == null) && (product.GroupKey.HasValue))
                    {
                        // there was no group for this product, but has been defined
                        // so save a new group member for this product
                        DataAccessResult saveResult;

                        GroupMember groupMember = new GroupMember();

                        groupMember.ProductKey = product.ProductKey;
                        groupMember.GroupKey = product.GroupKey.Value;

                        saveResult = dataService.Save(groupMember);
                        if (saveResult.Success)
                            product.GroupMemberKey = groupMember.GroupMemberKey;
                        else
                            return saveResult;
                    }

                    if ((dmgroup != null) && (!product.GroupKey.HasValue))
                    {
                        // there was an existing group member for this product, but now there is none
                        // so delete the existing group member
                        DataAccessResult deleteResult;

                        GroupMember groupMember = new GroupMember();
                        groupMember.GroupMemberKey = dmgroup.GroupMemberKey;

                        deleteResult = dataService.DataItemReGet(groupMember);
                        if (!deleteResult.Success)
                            return deleteResult;

                        groupMember = (GroupMember)deleteResult.SingleItem;
                        groupMember.Delete();

                        deleteResult = dataService.Save(groupMember);
                        if (deleteResult.Success)
                        {
                            product.GroupMemberKey = null;
                            product.GroupName=string.Empty;
                        }
                        else
                            return deleteResult;
                    }

                    if ((dmgroup != null) && (product.GroupKey.HasValue))
                    {
                        // there was an existing group member for this product, and there still is
                        // so update the group member if the group for the product has changed
                        if (dmgroup.GroupKey != product.GroupKey)
                        {
                            DataAccessResult updateResult;

                            GroupMember groupMember = new GroupMember();
                            groupMember.GroupMemberKey = dmgroup.GroupMemberKey;

                            updateResult = dataService.DataItemReGet(groupMember);
                            if (!updateResult.Success)
                                return updateResult;

                            groupMember = (GroupMember)updateResult.SingleItem;
                            groupMember.GroupKey = product.GroupKey.Value;
                            groupMember.ProductKey = product.ProductKey;
                            groupMember.MarkDirty();

                            updateResult = dataService.Save(groupMember);
                            if (!updateResult.Success)
                                return updateResult;
                        }
                    }

                    ahamOper.SubmitChanges();

                    product.MarkOld();
                    product.RowVersion = dproduct.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ProductDelete(Product product, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DProduct dproduct = ahamOper.DProducts.Single<DProduct>(x => (x.ProductKey == product.ProductKey));

                    if (dproduct.RowVersion != product.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + product.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dproduct.AppContext = parameters.ApplicationContext.Trim();
                    dproduct.EntryUid = parameters.UserName.Trim();

                    // delete the associated DMGroup (GroupMember), if there is one
                    if (product.GroupMemberKey.HasValue)
                    {
                        // get the DMGroup (group member) record for this product
                        int memberCount = (ahamOper.DMGroups.Where<DMGroup>(x => (x.ProductKey == product.ProductKey))).Count<DMGroup>();
                        if (memberCount == 1)       // if there is one, it must be deleted before the product can be deleted.
                        {
                            GroupMember groupMember = new GroupMember();                    // create a dummy GroupMember that will be the target of the delete
                            groupMember.MarkOld();                                          // ... mark it as old,
                            groupMember.GroupMemberKey = product.GroupMemberKey.Value;      // ... and give it the key held by the product.

                            AhamDataService dataService = new AhamDataService(parameters);  // create a data service
                            DataAccessResult groupMemberResult;                             // ... and an object to hold the result

                            groupMemberResult = dataService.DataItemReGet(groupMember);     // Call the service to "ReGet" the dummy
                            if (!groupMemberResult.Success)
                                return groupMemberResult;

                            groupMember = (GroupMember)groupMemberResult.SingleItem;        // Extract the GroupMember that has been fetched

                            groupMember.Delete();                                           // ... mark it for deletion.
                            groupMemberResult= dataService.Save(groupMember);               // ... and "Save" it to delete it.
                            if (!groupMemberResult.Success)
                                return groupMemberResult;                                   // If deleting the GroupMember failed, abort
                                                                                            // ... deleting the product to avoid creating orphan
                        }
                    }

                    ahamOper.DProducts.DeleteOnSubmit(dproduct);
                    ahamOper.SubmitChanges();

                    product.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region ProductSet

        internal static DataAccessResult ProductSetListGet(ProductSet productSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ProductSet> productSetList = new HaiBindingList<ProductSet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SDProduct> SDProducts;
                    if (productSetToGet == null)
                        SDProducts = ahamOper.SDProducts.ToList<SDProduct>();
                    else
                    {
                        SDProducts = new List<SDProduct>();
                        SDProducts.Add(ahamOper.SDProducts.Single<SDProduct>(x => (x.ProductSetKey == productSetToGet.ProductSetKey)));
                    }

                    foreach (SDProduct sdproduct in SDProducts)
                    {
                        ProductSet productSet = new ProductSet();

                        productSet.ProductSetAbbreviation = sdproduct.ProductSetAbbrev.Trim();
                        productSet.ProductSetKey = sdproduct.ProductSetKey;
                        productSet.ProductSetName = sdproduct.ProductSetName.Trim();

                        productSet.ApplicationContext = sdproduct.AppContext.Trim();
                        productSet.RowVersion = sdproduct.RowVersion;
                        productSet.UserName = sdproduct.EntryUid.Trim();

                        productSet.MarkOld();
                        productSetList.AddToList(productSet);
                    }

                    result.DataList = productSetList;
                    result.Success = true;
                    result.BrowsablePropertyList = ProductSet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ProductSetGet(ProductSet productSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ProductSetListGet(productSetToGet, parameters);
            if (result.Success)
            {
                ProductSet productSet = (ProductSet)result.SingleItem;
                result.ItemIsChanged = (productSetToGet.RowVersion != productSet.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ProductSetSave(ProductSet productSet, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (productSet.IsSavable)
            {
                if (productSet.IsDeleted)
                    result = ProductSetDelete(productSet, parameters);
                else if (productSet.IsNew)
                    result = ProductSetInsert(productSet, parameters);
                else if (productSet.IsDirty)
                    result = ProductSetUpdate(productSet, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ProductSetInsert(ProductSet productSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDProduct sdproduct = new SDProduct();
                    sdproduct.ProductSetAbbrev = productSet.ProductSetAbbreviation.Trim();
                    sdproduct.ProductSetName = productSet.ProductSetName.Trim();

                    sdproduct.AppContext = parameters.ApplicationContext.Trim();
                    sdproduct.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SDProduct>().InsertOnSubmit(sdproduct);
                    ahamOper.SubmitChanges();

                    productSet.ProductSetKey = sdproduct.ProductSetKey;
                    productSet.RowVersion = sdproduct.RowVersion;

                    productSet.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ProductSetUpdate(ProductSet productSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDProduct sdproduct = ahamOper.SDProducts.Single<SDProduct>(x => (x.ProductSetKey == productSet.ProductSetKey));

                    if (sdproduct.RowVersion != productSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + productSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdproduct.ProductSetAbbrev = productSet.ProductSetAbbreviation.Trim();
                    sdproduct.ProductSetName = productSet.ProductSetName.Trim();

                    sdproduct.AppContext = parameters.ApplicationContext.Trim();
                    sdproduct.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    productSet.MarkOld();
                    productSet.RowVersion = sdproduct.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ProductSetDelete(ProductSet productSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDProduct sdproduct = ahamOper.SDProducts.Single<SDProduct>(x => (x.ProductSetKey == productSet.ProductSetKey));

                    if (sdproduct.RowVersion != productSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + productSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdproduct.AppContext = parameters.ApplicationContext.Trim();
                    sdproduct.EntryUid = parameters.UserName.Trim();

                    ahamOper.SDProducts.DeleteOnSubmit(sdproduct);
                    ahamOper.SubmitChanges();

                    productSet.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region ProductSetMember

        internal static DataAccessResult ProductSetMemberListGet(ProductSetMember productSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ProductSetMember> productSetMemberList = new HaiBindingList<ProductSetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMProduct> SMProducts;
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<SMProduct>(x => x.DProduct);
                    dlo.LoadWith<SMProduct>(x => x.SDProduct);
                    ahamOper.LoadOptions = dlo;

                    if (productSetMemberToGet == null)
                    {
                        SMProducts = ahamOper.SMProducts.ToList<SMProduct>();
                    }
                    else
                    {
                        SMProducts = new List<SMProduct>();
                        SMProducts.Add(ahamOper.SMProducts.Single<SMProduct>(x => (x.ProductSetMemberKey == productSetMemberToGet.ProductSetMemberKey)));
                    }

                    foreach (SMProduct smproduct in SMProducts)
                    {
                        ProductSetMember productSetMember = new ProductSetMember();

                        productSetMember.ProductKey = smproduct.ProductKey;
                        productSetMember.ProductSetKey = smproduct.ProductSetKey;
                        productSetMember.ProductSetMemberKey = smproduct.ProductSetMemberKey;
                        productSetMember.DateRange.BeginDate = smproduct.BeginDate.Date;
                        productSetMember.DateRange.EndDate = smproduct.EndDate.Date;

                        productSetMember.ProductName = smproduct.DProduct.ProductName.Trim();
                        productSetMember.ProductAbbreviation = smproduct.DProduct.ProductAbbrev.Trim();
                        productSetMember.ProductSetAbbreviation = smproduct.SDProduct.ProductSetAbbrev.Trim();
                        productSetMember.ProductSetName = smproduct.SDProduct.ProductSetName.Trim();

                        if (smproduct.DProduct.ProductNameForFlash == null)
                            productSetMember.ProductNameForFlash = string.Empty;
                        else
                            productSetMember.ProductNameForFlash = smproduct.DProduct.ProductNameForFlash.Trim();

                        productSetMember.ProductOwnerKey = smproduct.DProduct.ProductOwnerKey;

                        productSetMember.ApplicationContext = smproduct.AppContext.Trim();
                        productSetMember.RowVersion = smproduct.RowVersion;
                        productSetMember.UserName = smproduct.EntryUid.Trim();

                        productSetMember.MarkOld();
                        productSetMemberList.AddToList(productSetMember);
                    }

                    result.DataList = productSetMemberList;
                    result.Success = true;
                    result.BrowsablePropertyList = ProductSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ProductSetMemberGet(ProductSetMember productSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ProductSetMemberListGet(productSetMemberToGet, parameters);
            if (result.Success)
            {
                ProductSetMember productSetMember = (ProductSetMember)result.SingleItem;
                result.ItemIsChanged = (productSetMemberToGet.RowVersion != productSetMember.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ProductSetMemberSave(ProductSetMember productSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (productSetMember.IsSavable)
            {
                if (productSetMember.IsDeleted)
                    result = ProductSetMemberDelete(productSetMember, parameters);
                else if (productSetMember.IsNew)
                    result = ProductSetMemberInsert(productSetMember, parameters);
                else if (productSetMember.IsDirty)
                    result = ProductSetMemberUpdate(productSetMember, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ProductSetMemberInsert(ProductSetMember productSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMProduct smproduct = new SMProduct();
                    smproduct.ProductKey = productSetMember.ProductKey;
                    smproduct.ProductSetKey = productSetMember.ProductSetKey;
                    smproduct.BeginDate = productSetMember.DateRange.BeginDate.Date;
                    smproduct.EndDate = productSetMember.DateRange.EndDate.Date;

                    smproduct.AppContext = parameters.ApplicationContext.Trim();
                    smproduct.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SMProduct>().InsertOnSubmit(smproduct);
                    ahamOper.SubmitChanges();

                    productSetMember.ProductSetMemberKey = smproduct.ProductSetMemberKey;
                    productSetMember.RowVersion = smproduct.RowVersion;

                    productSetMember.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ProductSetMemberUpdate(ProductSetMember productSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMProduct smproduct = ahamOper.SMProducts.Single<SMProduct>(x => (x.ProductSetMemberKey == productSetMember.ProductSetMemberKey));

                    if (smproduct.RowVersion != productSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + productSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smproduct.ProductKey = productSetMember.ProductKey;
                    smproduct.ProductSetKey = productSetMember.ProductSetKey;
                    smproduct.BeginDate = productSetMember.DateRange.BeginDate.Date;
                    smproduct.EndDate = productSetMember.DateRange.EndDate.Date;

                    smproduct.AppContext = parameters.ApplicationContext.Trim();
                    smproduct.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    productSetMember.MarkOld();
                    productSetMember.RowVersion = smproduct.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ProductSetMemberDelete(ProductSetMember productSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMProduct smproduct = ahamOper.SMProducts.Single<SMProduct>(x => (x.ProductSetMemberKey == productSetMember.ProductSetMemberKey));

                    if (smproduct.RowVersion != productSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + productSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smproduct.AppContext = parameters.ApplicationContext.Trim();
                    smproduct.EntryUid = parameters.UserName.Trim();

                    ahamOper.SMProducts.DeleteOnSubmit(smproduct);
                    ahamOper.SubmitChanges();

                    productSetMember.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ProductSetMembersDuplicate(ProductSet sourceProductSet, ProductSet targetProductSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMProduct> smProducts;
                    List<SMProduct> duplicatedItems = new List<SMProduct>();

                    smProducts = ahamOper.SMProducts.Where(x => (x.ProductSetKey == sourceProductSet.ProductSetKey)).ToList<SMProduct>();

                    foreach (SMProduct smProduct in smProducts)
                    {
                        SMProduct duplicateSMProduct = new SMProduct();

                        duplicateSMProduct.ProductSetKey = targetProductSet.ProductSetKey;
                        duplicateSMProduct.ProductKey = smProduct.ProductKey;
                        duplicateSMProduct.BeginDate = smProduct.BeginDate.Date;
                        duplicateSMProduct.EndDate = smProduct.EndDate.Date;
                        duplicateSMProduct.EntryUid = parameters.UserName.Trim();
                        duplicateSMProduct.AppContext = parameters.ApplicationContext.Trim();

                        duplicatedItems.Add(duplicateSMProduct);
                    }

                    ahamOper.GetTable<SMProduct>().InsertAllOnSubmit<SMProduct>(duplicatedItems);
                    ahamOper.SubmitChanges();

                    result.Success = true;
                    result.BrowsablePropertyList = ProductSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion


        #region ProductSum

        internal static DataAccessResult ProductSumListGet(ProductSum productSumToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ProductSum> productSums = new HaiBindingList<ProductSum>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<DproductSum>(x => (x.DProduct));
                    dlo.LoadWith<DproductSum>(x => (x.DProduct1));
                    ahamOper.LoadOptions = dlo;

                    List<DproductSum> DProductSums;
                    if (productSumToGet == null)
                        DProductSums = ahamOper.DproductSums.ToList<DproductSum>();
                    else
                    {
                        DProductSums = new List<DproductSum>();
                        DProductSums.Add(ahamOper.DproductSums.Single<DproductSum>(x => (x.ProductSumKey == productSumToGet.ProductSumKey)));
                    }

                    foreach (DproductSum dProductSum in DProductSums)
                    {
                        ProductSum productSum = new ProductSum();

                        productSum.ProductSumKey = dProductSum.ProductSumKey;
                        productSum.ProductKey = dProductSum.Parent;
                        productSum.ProductName = dProductSum.DProduct1.ProductName.Trim();
                        productSum.ComponentProductKey = dProductSum.Child;
                        productSum.ComponentProductName = dProductSum.DProduct.ProductName.Trim();

                        productSum.ApplicationContext = dProductSum.AppContext.Trim();
                        productSum.UserName = dProductSum.EntryUid.Trim();
                        productSum.RowVersion = dProductSum.RowVersion;

                        productSum.MarkOld();
                        productSums.AddToList(productSum);
                    }

                    result.DataList = productSums;
                    result.Success = true;
                    result.BrowsablePropertyList = ProductSum.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ProductSumGet(ProductSum productSumToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ProductSumListGet(productSumToGet, parameters);
            if (result.Success)
            {
                ProductSum productSum = (ProductSum)result.SingleItem;
                result.ItemIsChanged = (productSumToGet.RowVersion != productSum.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ProductSumSave(ProductSum productSum, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            if (productSum.IsSavable)
            {
                if (productSum.IsDeleted)
                    result = ProductSumDelete(productSum, parameters);
                else if (productSum.IsNew)
                    result = ProductSumInsert(productSum, parameters);
                else if (productSum.IsDirty)
                    result = ProductSumUpdate(productSum, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        internal static DataAccessResult ProductSumInsert(ProductSum productSum, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            if (productSum.ProductKey == productSum.ComponentProductKey)
            {
                result.Success = false;
                result.Message = "Direct insert not allowed.  ProductSum is inserted when a new product is created.";
                return result;
            }

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DproductSum dProductSum = new DproductSum();

                    dProductSum.Parent = productSum.ProductKey;
                    dProductSum.Child = productSum.ComponentProductKey;

                    dProductSum.AppContext = parameters.ApplicationContext.Trim();
                    dProductSum.EntryUid = parameters.UserName.Trim();

                    ahamOper.DproductSums.InsertOnSubmit(dProductSum);
                    ahamOper.SubmitChanges();

                    productSum.ProductSumKey = dProductSum.ProductSumKey;
                    productSum.RowVersion = dProductSum.RowVersion;

                    productSum.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ProductSumUpdate(ProductSum productSum, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DproductSum dProductSum = ahamOper.DproductSums.Single<DproductSum>(x => (x.ProductSumKey == productSum.ProductSumKey));

                    if (dProductSum.RowVersion != productSum.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + productSum.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dProductSum.Parent = productSum.ProductKey;
                    dProductSum.Child = productSum.ComponentProductKey;

                    dProductSum.AppContext = parameters.ApplicationContext.Trim();
                    dProductSum.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    productSum.MarkOld();
                    productSum.RowVersion = dProductSum.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ProductSumDelete(ProductSum productSum, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            if (productSum.ProductKey == productSum.ComponentProductKey)
            {
                result.Success = false;
                result.Message = "Direct delete not allowed.  ProductSum is deleted in cascade from Product delete.";
                return result;
            }

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DproductSum dProductSum = ahamOper.DproductSums.Single<DproductSum>(x => (x.ProductSumKey == productSum.ProductSumKey));

                    if (dProductSum.RowVersion != productSum.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + productSum.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dProductSum.EntryUid = parameters.UserName.Trim();
                    dProductSum.AppContext = parameters.ApplicationContext.Trim();

                    ahamOper.DproductSums.DeleteOnSubmit(dProductSum);
                    ahamOper.SubmitChanges();

                    productSum.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion
    }
}
