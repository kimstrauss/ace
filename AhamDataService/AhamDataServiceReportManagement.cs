﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    public class AhamDataServiceReportManagement
    {
        #region HolidayDate

        internal static DataAccessResult HolidayDateListGet(HolidayDate holidayDateToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<HolidayDate> holidayDates = new HaiBindingList<HolidayDate>();


            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<Holiday> Holidays;
                    if (holidayDateToGet == null)
                    {
                        Holidays = ahamOper.Holidays.ToList<Holiday>();
                    }
                    else
                    {
                        Holidays = new List<Holiday>();
                        Holidays.Add(ahamOper.Holidays.Single<Holiday>(x => (x.HolidayKey == holidayDateToGet.HolidayDateKey)));
                    }

                    foreach (Holiday holiday in Holidays)
                    {
                        HolidayDate holidayDate = new HolidayDate();

                        holidayDate.DateOfHoliday = holiday.HolidayDate.Date;
                        holidayDate.HolidayDateKey = holiday.HolidayKey;
                        holidayDate.Name = holiday.Name;

                        holidayDate.ApplicationContext = holiday.AppContext.Trim();
                        holidayDate.UserName = holiday.EntryUid.Trim();
                        holidayDate.RowVersion = holiday.RowVersion;

                        holidayDate.MarkOld();
                        holidayDates.AddToList(holidayDate);
                    }

                    result.DataList = holidayDates;
                    result.Success = true;
                    result.BrowsablePropertyList = HolidayDate.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult HolidayDateGet(HolidayDate holidayDateToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = HolidayDateListGet(holidayDateToGet, parameters);
            if (result.Success)
            {
                HolidayDate holidayDate = (HolidayDate)result.SingleItem;
                result.ItemIsChanged = (holidayDateToGet.RowVersion != holidayDate.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult HolidayDateSave(HolidayDate holidayDate, DataServiceParameters parameters)
        {
            DataAccessResult result;

            if (holidayDate.IsSavable)
            {
                if (holidayDate.IsDeleted)
                    result = HolidayDateDelete(holidayDate, parameters);
                else if (holidayDate.IsNew)
                    result = HolidayDateInsert(holidayDate, parameters);
                else if (holidayDate.IsDirty)
                    result = HolidayDateUpdate(holidayDate, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult HolidayDateUpdate(HolidayDate holidayDate, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    Holiday holiday = ahamOper.Holidays.Single<Holiday>(x => (x.HolidayKey == holidayDate.HolidayDateKey));

                    if (holiday.RowVersion != holidayDate.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + holidayDate.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    holiday.HolidayDate = holidayDate.DateOfHoliday.Date;
                    holiday.Name = holidayDate.Name;

                    holiday.AppContext = parameters.ApplicationContext.Trim();
                    holiday.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    holidayDate.MarkOld();
                    holidayDate.RowVersion = holiday.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult HolidayDateInsert(HolidayDate holidayDate, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    Holiday holiday = new Holiday();
                    holiday.HolidayDate = holidayDate.DateOfHoliday.Date;
                    holiday.Name = holidayDate.Name;

                    holiday.AppContext = parameters.ApplicationContext.Trim();
                    holiday.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<Holiday>().InsertOnSubmit(holiday);
                    ahamOper.SubmitChanges();

                    holidayDate.HolidayDateKey = holiday.HolidayKey;
                    holidayDate.RowVersion = holiday.RowVersion;

                    holidayDate.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult HolidayDateDelete(HolidayDate holidayDate, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    Holiday holiday = ahamOper.Holidays.Single<Holiday>(x => (x.HolidayKey == holidayDate.HolidayDateKey));

                    if (holidayDate.RowVersion != holidayDate.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + holidayDate.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    holiday.AppContext = parameters.ApplicationContext.Trim();
                    holiday.EntryUid = parameters.UserName.Trim();

                    ahamOper.Holidays.DeleteOnSubmit(holiday);
                    ahamOper.SubmitChanges();

                    holidayDate.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region HolidayCADate

        internal static DataAccessResult HolidayCADateListGet(HolidayCADate holidayCADateToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<HolidayCADate> holidayCADates = new HaiBindingList<HolidayCADate>();


            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<HolidayCA> HolidayCAs;
                    if (holidayCADateToGet == null)
                    {
                        HolidayCAs = ahamOper.HolidayCAs.ToList<HolidayCA>();
                    }
                    else
                    {
                        HolidayCAs = new List<HolidayCA>();
                        HolidayCAs.Add(ahamOper.HolidayCAs.Single<HolidayCA>(x => (x.HolidayKey == holidayCADateToGet.HolidayDateKey)));
                    }

                    foreach (HolidayCA holidayCA in HolidayCAs)
                    {
                        HolidayCADate holidayCADate = new HolidayCADate();

                        holidayCADate.DateOfHoliday = holidayCA.HolidayDate.Date;
                        holidayCADate.HolidayDateKey = holidayCA.HolidayKey;
                        holidayCADate.Name = holidayCA.Name;

                        holidayCADate.ApplicationContext = holidayCA.AppContext.Trim();
                        holidayCADate.UserName = holidayCA.EntryUid.Trim();
                        holidayCADate.RowVersion = holidayCA.RowVersion;

                        holidayCADate.MarkOld();
                        holidayCADates.AddToList(holidayCADate);
                    }

                    result.DataList = holidayCADates;
                    result.Success = true;
                    result.BrowsablePropertyList = HolidayCADate.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult HolidayCADateGet(HolidayCADate holidayCADateToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = HolidayCADateListGet(holidayCADateToGet, parameters);
            if (result.Success)
            {
                HolidayCADate holidayCADate = (HolidayCADate)result.SingleItem;
                result.ItemIsChanged = (holidayCADateToGet.RowVersion != holidayCADate.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult HolidayCADateSave(HolidayCADate holidayCADate, DataServiceParameters parameters)
        {
            DataAccessResult result;

            if (holidayCADate.IsSavable)
            {
                if (holidayCADate.IsDeleted)
                    result = HolidayCADateDelete(holidayCADate, parameters);
                else if (holidayCADate.IsNew)
                    result = HolidayCADateInsert(holidayCADate, parameters);
                else if (holidayCADate.IsDirty)
                    result = HolidayCADateUpdate(holidayCADate, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult HolidayCADateUpdate(HolidayCADate holidayCADate, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    HolidayCA holidayCA = ahamOper.HolidayCAs.Single<HolidayCA>(x => (x.HolidayKey == holidayCADate.HolidayDateKey));

                    if (holidayCA.RowVersion != holidayCADate.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + holidayCADate.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    holidayCA.HolidayDate = holidayCADate.DateOfHoliday.Date;
                    holidayCA.Name = holidayCADate.Name;

                    holidayCA.AppContext = parameters.ApplicationContext.Trim();
                    holidayCA.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    holidayCADate.MarkOld();
                    holidayCADate.RowVersion = holidayCA.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult HolidayCADateInsert(HolidayCADate holidayCADate, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    HolidayCA holidayCA = new HolidayCA();
                    holidayCA.HolidayDate = holidayCADate.DateOfHoliday.Date;
                    holidayCA.Name = holidayCADate.Name;

                    holidayCA.AppContext = parameters.ApplicationContext.Trim();
                    holidayCA.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<HolidayCA>().InsertOnSubmit(holidayCA);
                    ahamOper.SubmitChanges();

                    holidayCADate.HolidayDateKey = holidayCA.HolidayKey;
                    holidayCADate.RowVersion = holidayCA.RowVersion;

                    holidayCADate.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult HolidayCADateDelete(HolidayCADate holidayCADate, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    HolidayCA holidayCA = ahamOper.HolidayCAs.Single<HolidayCA>(x => (x.HolidayKey == holidayCADate.HolidayDateKey));

                    if (holidayCADate.RowVersion != holidayCADate.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + holidayCADate.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    holidayCA.AppContext = parameters.ApplicationContext.Trim();
                    holidayCA.EntryUid = parameters.UserName.Trim();

                    ahamOper.HolidayCAs.DeleteOnSubmit(holidayCA);
                    ahamOper.SubmitChanges();

                    holidayCADate.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region ReleaseException

        internal static DataAccessResult ReleaseExceptionListGet(ReleaseException releaseExceptionToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ReleaseException> releaseExceptions = new HaiBindingList<ReleaseException>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<orReleaseException>(x => (x.DOutPutReport));
                    ahamOper.LoadOptions = dlo;

                    List<orReleaseException> orReleaseExceptions;
                    if (releaseExceptionToGet == null)
                    {
                        orReleaseExceptions = ahamOper.orReleaseExceptions.ToList<orReleaseException>();
                    }
                    else
                    {
                        orReleaseExceptions = new List<orReleaseException>();
                        orReleaseExceptions.Add(ahamOper.orReleaseExceptions.Single<orReleaseException>(x => (x.OrReleaseExceptionKey==releaseExceptionToGet.ReleaseExceptionKey)));
                    }
                    foreach (orReleaseException orreleaseException in orReleaseExceptions)
                    {
                        ReleaseException releaseException = new ReleaseException();

                        releaseException.ReleaseExceptionKey = orreleaseException.OrReleaseExceptionKey;
                        releaseException.OutputReportKey = orreleaseException.OutputReportKey;
                        releaseException.OutputReportID = orreleaseException.DOutPutReport.OutputReportID.Trim();
                        releaseException.ReportEndDate = orreleaseException.ReportEndDate.Date;

                        if (orreleaseException.HideFinal==0)
                            releaseException.HideFinal = false;
                        else
                            releaseException.HideFinal = true;

                        if (orreleaseException.HidePublic == 0)
                            releaseException.HidePublic = false;
                        else
                            releaseException.HidePublic = true;

                        releaseException.ApplicationContext = orreleaseException.AppContext.Trim();
                        releaseException.RowVersion = orreleaseException.RowVersion;
                        releaseException.UserName = orreleaseException.EntryUid.Trim();

                        releaseException.MarkOld();
                        releaseExceptions.AddToList(releaseException);
                    }

                    result.DataList = releaseExceptions;
                    result.Success = true;
                    result.BrowsablePropertyList = ReleaseException.GetBrowsablePropertyList();
                }

                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ReleaseExceptionGet(ReleaseException releaseExceptionToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ReleaseExceptionListGet(releaseExceptionToGet, parameters);
            if (result.Success)
            {
                ReleaseException releaseException = (ReleaseException)result.SingleItem;
                result.ItemIsChanged = (releaseExceptionToGet.RowVersion != releaseException.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ReleaseExceptionSave(ReleaseException releaseException, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (releaseException.IsSavable)
            {
                if (releaseException.IsDeleted)
                    result = ReleaseExceptionDelete(releaseException, parameters);
                else if (releaseException.IsNew)
                    result = ReleaseExceptionInsert(releaseException, parameters);
                else if (releaseException.IsDirty)
                    result = ReleaseExceptionUpdate(releaseException, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ReleaseExceptionInsert(ReleaseException releaseException, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    orReleaseException orreleaseException = new orReleaseException();
                    orreleaseException.OutputReportKey = releaseException.OutputReportKey;
                    orreleaseException.ReportEndDate = releaseException.ReportEndDate.Date;

                    if (releaseException.HideFinal)
                        orreleaseException.HideFinal = 1;
                    else
                        orreleaseException.HideFinal = 0;

                    if (releaseException.HidePublic)
                        orreleaseException.HidePublic = 1;
                    else
                        orreleaseException.HidePublic = 0;

                    orreleaseException.AppContext = parameters.ApplicationContext.Trim();
                    orreleaseException.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<orReleaseException>().InsertOnSubmit(orreleaseException);
                    ahamOper.SubmitChanges();

                    releaseException.ReleaseExceptionKey = orreleaseException.OrReleaseExceptionKey;
                    releaseException.RowVersion = orreleaseException.RowVersion;

                    releaseException.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ReleaseExceptionUpdate(ReleaseException releaseException, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    orReleaseException orreleaseException = ahamOper.orReleaseExceptions.Single<orReleaseException>(x => (x.OrReleaseExceptionKey == releaseException.ReleaseExceptionKey));
                    if (orreleaseException.RowVersion != releaseException.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + releaseException.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    orreleaseException.OutputReportKey = releaseException.OutputReportKey;
                    orreleaseException.ReportEndDate = releaseException.ReportEndDate.Date;

                    if (releaseException.HideFinal)
                        orreleaseException.HideFinal = 1;
                    else
                        orreleaseException.HideFinal = 0;

                    if (releaseException.HidePublic)
                        orreleaseException.HidePublic = 1;
                    else
                        orreleaseException.HidePublic = 0;

                    orreleaseException.AppContext = parameters.ApplicationContext.Trim();
                    orreleaseException.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    releaseException.MarkOld();
                    releaseException.RowVersion = orreleaseException.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ReleaseExceptionDelete(ReleaseException releaseException, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    orReleaseException orreleaseException = ahamOper.orReleaseExceptions.Single<orReleaseException>(x => (x.OrReleaseExceptionKey == releaseException.ReleaseExceptionKey));
                    if (orreleaseException.RowVersion != releaseException.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + releaseException.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    orreleaseException.AppContext = parameters.ApplicationContext.Trim();
                    orreleaseException.EntryUid = parameters.UserName.Trim();

                    ahamOper.orReleaseExceptions.DeleteOnSubmit(orreleaseException);
                    ahamOper.SubmitChanges();

                    releaseException.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }
        
        #endregion

        #region ManufacturerBrands

        internal static DataAccessResult ManufacturerBrandListGet(ManufacturerBrand manufacturerBrandToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ManufacturerBrand> manufacturerBrands = new HaiBindingList<ManufacturerBrand>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<AhamDataLinq.MfgBrand>(x => (x.DMarket));
                    dlo.LoadWith<AhamDataLinq.MfgBrand>(x => (x.DMfg));         // ReportingManufacturer
                    dlo.LoadWith<AhamDataLinq.MfgBrand>(x => (x.DMfg1));        // FromManufacturer
                    dlo.LoadWith<AhamDataLinq.MfgBrand>(x => (x.DMfg2));        // ToManufacturer
                    dlo.LoadWith<AhamDataLinq.MfgBrand>(x => (x.DProduct));
                    ahamOper.LoadOptions = dlo;

                    List<AhamDataLinq.MfgBrand> dmanufacturerBrands;
                    if (manufacturerBrandToGet == null)
                    {
                        dmanufacturerBrands = ahamOper.MfgBrands.ToList<AhamDataLinq.MfgBrand>();
                    }
                    else
                    {
                        dmanufacturerBrands = new List<AhamDataLinq.MfgBrand>();
                        dmanufacturerBrands.Add(ahamOper.MfgBrands.Single<MfgBrand>(x => (x.mfgBrandKey == manufacturerBrandToGet.ManufacturerBrandKey)));
                    }

                    foreach (AhamDataLinq.MfgBrand dmanufacturerBrand in dmanufacturerBrands)
                    {
                        ManufacturerBrand mfrBrand = new ManufacturerBrand();

                        mfrBrand.ManufacturerBrandKey = dmanufacturerBrand.mfgBrandKey;
                        mfrBrand.ReportingManufacturerKey = dmanufacturerBrand.ReportingMfgKey;
                        mfrBrand.ReportingManufacturerName = dmanufacturerBrand.DMfg.MfgName.Trim();
                        mfrBrand.ProductKey = dmanufacturerBrand.ProductKey;
                        mfrBrand.ProductName = dmanufacturerBrand.DProduct.ProductName.Trim();
                        mfrBrand.MarketKey = dmanufacturerBrand.MarketKey;
                        mfrBrand.MarketName = dmanufacturerBrand.DMarket.MarketName.Trim();
                        mfrBrand.BrandName = dmanufacturerBrand.BrandName.Trim();

                        mfrBrand.FromManufacturerKey = dmanufacturerBrand.FromMfgKey;
                        if (dmanufacturerBrand.FromMfgKey.HasValue)
                            mfrBrand.FromManufacturerName = dmanufacturerBrand.DMfg1.MfgName.Trim();
                        else
                            mfrBrand.FromManufacturerName = dmanufacturerBrand.FromMfgName.Trim();

                        mfrBrand.ToManufacturerKey = dmanufacturerBrand.ToMfgKey;
                        if (dmanufacturerBrand.ToMfgKey.HasValue)
                            mfrBrand.ToManufacturerName = dmanufacturerBrand.DMfg2.MfgName.Trim();
                        else
                            mfrBrand.ToManufacturerName = dmanufacturerBrand.ToMfgName.Trim();

                        mfrBrand.Included = Utilities.ConvertByteToBool(dmanufacturerBrand.Included);
                        mfrBrand.DisplayBrand = Utilities.ConvertByteToBool(dmanufacturerBrand.DisplayBrand);
                        mfrBrand.DateRange.BeginDate = dmanufacturerBrand.Begindate.Date;
                        mfrBrand.DateRange.EndDate = dmanufacturerBrand.EndDate.Date;

                        mfrBrand.ApplicationContext = dmanufacturerBrand.AppContext.Trim();
                        mfrBrand.RowVersion = dmanufacturerBrand.RowVersion;
                        mfrBrand.UserName = dmanufacturerBrand.EntryUid.Trim();

                        mfrBrand.MarkOld();
                        manufacturerBrands.AddToList(mfrBrand);
                    }

                    result.DataList = manufacturerBrands;
                    result.Success = true;
                    result.BrowsablePropertyList = ManufacturerBrand.GetBrowsablePropertyList();

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ManufacturerBrandGet(ManufacturerBrand manufacturerBrandToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ManufacturerBrandListGet(manufacturerBrandToGet, parameters);
            if (result.Success)
            {
                ManufacturerBrand manufacturerBrand = (ManufacturerBrand)result.SingleItem;
                result.ItemIsChanged = (manufacturerBrandToGet.RowVersion != manufacturerBrand.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ManufacturerBrandSave(ManufacturerBrand manufacturerBrand, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (manufacturerBrand.IsSavable)
            {
                if (manufacturerBrand.IsDeleted)
                    result = ManufacturerBrandDelete(manufacturerBrand, parameters);
                else if (manufacturerBrand.IsNew)
                    result = ManufacturerBrandInsert(manufacturerBrand, parameters);
                else if (manufacturerBrand.IsDirty)
                    result = ManufacturerBrandUpdate(manufacturerBrand, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ManufacturerBrandInsert(ManufacturerBrand manufacturerBrand, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    MfgBrand dMfgBrand = new MfgBrand();

                    dMfgBrand.ReportingMfgKey = manufacturerBrand.ReportingManufacturerKey;
                    dMfgBrand.ProductKey = manufacturerBrand.ProductKey;
                    dMfgBrand.MarketKey = manufacturerBrand.MarketKey;
                    dMfgBrand.BrandName = manufacturerBrand.BrandName.Trim();

                    dMfgBrand.FromMfgKey = manufacturerBrand.FromManufacturerKey;
                    if (dMfgBrand.FromMfgKey.HasValue)
                        dMfgBrand.FromMfgName = string.Empty;
                    else
                        dMfgBrand.FromMfgName = manufacturerBrand.FromManufacturerName.Trim();

                    dMfgBrand.ToMfgKey = manufacturerBrand.ToManufacturerKey;
                    if (dMfgBrand.ToMfgKey.HasValue)
                        dMfgBrand.ToMfgName = string.Empty;
                    else
                        dMfgBrand.ToMfgName = manufacturerBrand.ToManufacturerName.Trim();

                    dMfgBrand.Included = Utilities.ConvertBoolToByte(manufacturerBrand.Included);
                    dMfgBrand.DisplayBrand = Utilities.ConvertBoolToByte(manufacturerBrand.DisplayBrand);
                    dMfgBrand.Begindate = manufacturerBrand.DateRange.BeginDate.Date;
                    dMfgBrand.EndDate = manufacturerBrand.DateRange.EndDate.Date;
                    
                    dMfgBrand.AppContext = parameters.ApplicationContext.Trim();
                    dMfgBrand.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<MfgBrand>().InsertOnSubmit(dMfgBrand);
                    ahamOper.SubmitChanges();

                    manufacturerBrand.ManufacturerBrandKey = dMfgBrand.mfgBrandKey;
                    manufacturerBrand.RowVersion = dMfgBrand.RowVersion;

                    manufacturerBrand.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ManufacturerBrandUpdate(ManufacturerBrand manufacturerBrand, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    MfgBrand dMfgBrand = ahamOper.MfgBrands.Single<MfgBrand>(x => (x.mfgBrandKey == manufacturerBrand.ManufacturerBrandKey));

                    if (dMfgBrand.RowVersion != manufacturerBrand.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + manufacturerBrand.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dMfgBrand.ReportingMfgKey = manufacturerBrand.ReportingManufacturerKey;
                    dMfgBrand.ProductKey = manufacturerBrand.ProductKey;
                    dMfgBrand.MarketKey = manufacturerBrand.MarketKey;
                    dMfgBrand.BrandName = manufacturerBrand.BrandName.Trim();

                    dMfgBrand.FromMfgKey = manufacturerBrand.FromManufacturerKey;
                    if (dMfgBrand.FromMfgKey.HasValue)
                        dMfgBrand.FromMfgName = string.Empty;
                    else
                        dMfgBrand.FromMfgName = manufacturerBrand.FromManufacturerName.Trim();

                    dMfgBrand.ToMfgKey = manufacturerBrand.ToManufacturerKey;
                    if (dMfgBrand.ToMfgKey.HasValue)
                        dMfgBrand.ToMfgName = string.Empty;
                    else
                        dMfgBrand.ToMfgName = manufacturerBrand.ToManufacturerName.Trim();

                    dMfgBrand.Included = Utilities.ConvertBoolToByte(manufacturerBrand.Included);
                    dMfgBrand.DisplayBrand = Utilities.ConvertBoolToByte(manufacturerBrand.DisplayBrand);
                    dMfgBrand.Begindate = manufacturerBrand.DateRange.BeginDate.Date;
                    dMfgBrand.EndDate = manufacturerBrand.DateRange.EndDate.Date;

                    dMfgBrand.AppContext = parameters.ApplicationContext.Trim();
                    dMfgBrand.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    manufacturerBrand.MarkOld();
                    manufacturerBrand.RowVersion = dMfgBrand.RowVersion;
                    result.Success = true;

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ManufacturerBrandDelete(ManufacturerBrand manufacturerBrand, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    MfgBrand dMfgBrand = ahamOper.MfgBrands.Single<MfgBrand>(x => (x.mfgBrandKey == manufacturerBrand.ManufacturerBrandKey));

                    if (dMfgBrand.RowVersion != manufacturerBrand.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + manufacturerBrand.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dMfgBrand.AppContext = parameters.ApplicationContext.Trim();
                    dMfgBrand.EntryUid = parameters.UserName.Trim();

                    ahamOper.MfgBrands.DeleteOnSubmit(dMfgBrand);
                    ahamOper.SubmitChanges();

                    manufacturerBrand.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region ManufacturerMissingBrands

        internal static DataAccessResult ManufacturerMissingBrandListGet(ManufacturerMissingBrand manufacturerMissingBrandToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ManufacturerMissingBrand> manufacturerMissingBrands = new HaiBindingList<ManufacturerMissingBrand>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<AhamDataLinq.MfgMissingBrand>(x => (x.DMarket));
                    dlo.LoadWith<AhamDataLinq.MfgMissingBrand>(x => (x.DProduct));
                    ahamOper.LoadOptions = dlo;

                    List<AhamDataLinq.MfgMissingBrand> mfgMissingBrands;
                    if (manufacturerMissingBrandToGet == null)
                        mfgMissingBrands = ahamOper.MfgMissingBrands.ToList<MfgMissingBrand>();
                    else
                    {
                        mfgMissingBrands = new List<MfgMissingBrand>();
                        mfgMissingBrands.Add(ahamOper.MfgMissingBrands.Single<MfgMissingBrand>(x => (x.MfgMissingBrandsKey == manufacturerMissingBrandToGet.ManufacturerMissingBrandKey)));
                    }

                    foreach (AhamDataLinq.MfgMissingBrand mfgMissingBrand in mfgMissingBrands)
                    {
                        ManufacturerMissingBrand manufacturerMissingBrand = new ManufacturerMissingBrand();

                        manufacturerMissingBrand.ManufacturerMissingBrandKey = mfgMissingBrand.MfgMissingBrandsKey;
                        manufacturerMissingBrand.ProductKey = mfgMissingBrand.Productkey;
                        manufacturerMissingBrand.ProductName = mfgMissingBrand.DProduct.ProductName.Trim();
                        manufacturerMissingBrand.BrandName = mfgMissingBrand.BrandName.Trim();
                        manufacturerMissingBrand.MarketKey = mfgMissingBrand.MarketKey;
                        manufacturerMissingBrand.MarketName = mfgMissingBrand.DMarket.MarketName.Trim();
                        manufacturerMissingBrand.IsBrand = Utilities.ConvertByteToBool(mfgMissingBrand.IsBrand);
                        manufacturerMissingBrand.BeginDate = mfgMissingBrand.Begindate.Date;
                        manufacturerMissingBrand.EndDate = mfgMissingBrand.EndDate.Date;

                        manufacturerMissingBrand.ApplicationContext = mfgMissingBrand.AppContext.Trim();
                        manufacturerMissingBrand.RowVersion = mfgMissingBrand.RowVersion;
                        manufacturerMissingBrand.UserName = mfgMissingBrand.EntryUid.Trim();

                        manufacturerMissingBrand.MarkOld();
                        manufacturerMissingBrands.AddToList(manufacturerMissingBrand);
                    }

                    result.DataList = manufacturerMissingBrands;
                    result.Success = true;
                    result.BrowsablePropertyList = ManufacturerMissingBrand.GetBrowsablePropertyList();

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ManufacturerMissingBrandGet(ManufacturerMissingBrand manufacturerMissingBrandToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ManufacturerMissingBrandListGet(manufacturerMissingBrandToGet, parameters);
            if (result.Success)
            {
                ManufacturerMissingBrand manufacturerMissingBrand = (ManufacturerMissingBrand)result.SingleItem;
                result.ItemIsChanged = (manufacturerMissingBrandToGet.RowVersion != manufacturerMissingBrand.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ManufacturerMissingBrandSave(ManufacturerMissingBrand manufacturerMissingBrand, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (manufacturerMissingBrand.IsSavable)
            {
                if (manufacturerMissingBrand.IsDeleted)
                    result = ManufacturerMissingBrandDelete(manufacturerMissingBrand, parameters);
                else if (manufacturerMissingBrand.IsNew)
                    result = ManufacturerMissingBrandInsert(manufacturerMissingBrand, parameters);
                else if (manufacturerMissingBrand.IsDirty)
                    result = ManufacturerMissingBrandUpdate(manufacturerMissingBrand, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ManufacturerMissingBrandInsert(ManufacturerMissingBrand manufacturerMissingBrand, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    MfgMissingBrand mfgMissingBrand = new MfgMissingBrand();

                    mfgMissingBrand.Productkey = manufacturerMissingBrand.ProductKey;
                    mfgMissingBrand.MarketKey = manufacturerMissingBrand.MarketKey;
                    mfgMissingBrand.BrandName = manufacturerMissingBrand.BrandName.Trim();
                    mfgMissingBrand.IsBrand = Utilities.ConvertBoolToByte(manufacturerMissingBrand.IsBrand);
                    mfgMissingBrand.Begindate = manufacturerMissingBrand.DateRange.BeginDate.Date;
                    mfgMissingBrand.EndDate = manufacturerMissingBrand.DateRange.EndDate.Date;

                    mfgMissingBrand.EntryUid = parameters.UserName.Trim();
                    mfgMissingBrand.AppContext = parameters.ApplicationContext.Trim();

                    ahamOper.GetTable<MfgMissingBrand>().InsertOnSubmit(mfgMissingBrand);
                    ahamOper.SubmitChanges();

                    manufacturerMissingBrand.ManufacturerMissingBrandKey = mfgMissingBrand.MfgMissingBrandsKey;
                    manufacturerMissingBrand.RowVersion = mfgMissingBrand.RowVersion;

                    manufacturerMissingBrand.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ManufacturerMissingBrandUpdate(ManufacturerMissingBrand manufacturerMissingBrand, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    MfgMissingBrand mfgMissingBrand = ahamOper.MfgMissingBrands.Single<MfgMissingBrand>(x => (x.MfgMissingBrandsKey == manufacturerMissingBrand.ManufacturerMissingBrandKey));

                    if (mfgMissingBrand.RowVersion != manufacturerMissingBrand.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + manufacturerMissingBrand.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    mfgMissingBrand.MfgMissingBrandsKey = manufacturerMissingBrand.ManufacturerMissingBrandKey;
                    mfgMissingBrand.Productkey = manufacturerMissingBrand.ProductKey;
                    mfgMissingBrand.MarketKey = manufacturerMissingBrand.MarketKey;
                    mfgMissingBrand.BrandName = manufacturerMissingBrand.BrandName.Trim();
                    mfgMissingBrand.IsBrand = Utilities.ConvertBoolToByte(manufacturerMissingBrand.IsBrand);
                    mfgMissingBrand.Begindate = manufacturerMissingBrand.DateRange.BeginDate.Date;
                    mfgMissingBrand.EndDate = manufacturerMissingBrand.DateRange.EndDate.Date;

                    mfgMissingBrand.EntryUid = parameters.UserName.Trim();
                    mfgMissingBrand.AppContext = parameters.ApplicationContext.Trim();

                    ahamOper.SubmitChanges();

                    manufacturerMissingBrand.MarkOld();
                    manufacturerMissingBrand.RowVersion = mfgMissingBrand.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ManufacturerMissingBrandDelete(ManufacturerMissingBrand manufacturerMissingBrand, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    MfgMissingBrand mfgMissingBrand = ahamOper.MfgMissingBrands.Single<MfgMissingBrand>(x => (x.MfgMissingBrandsKey == manufacturerMissingBrand.ManufacturerMissingBrandKey));

                    if (mfgMissingBrand.RowVersion != manufacturerMissingBrand.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + manufacturerMissingBrand.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    mfgMissingBrand.AppContext = parameters.ApplicationContext.Trim();
                    mfgMissingBrand.EntryUid = parameters.UserName.Trim();

                    ahamOper.MfgMissingBrands.DeleteOnSubmit(mfgMissingBrand);
                    ahamOper.SubmitChanges();

                    manufacturerMissingBrand.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region Frequency

        internal static DataAccessResult FrequencyListGet(Frequency frequencyToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Frequency> frequencies = new HaiBindingList<Frequency>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<AhamDataLinq.DFreq> dfreqs;
                    if (frequencyToGet == null)
                    {
                        dfreqs = ahamOper.DFreqs.ToList<DFreq>();
                    }
                    else
                    {
                        dfreqs = new List<DFreq>();
                        dfreqs.Add(ahamOper.DFreqs.Single<DFreq>(x => (x.Freqkey == frequencyToGet.FrequencyKey)));
                    }

                    foreach (AhamDataLinq.DFreq dfreq in dfreqs)
                    {
                        Frequency frequency = new Frequency();

                        frequency.FrequencyKey = dfreq.Freqkey;
                        frequency.FrequencyName = dfreq.FreqName.Trim();
                        frequency.FrequencyID = dfreq.FreqID.ToString().Trim();
                        frequency.PeriodsPerYear = (decimal)dfreq.PeriodsPerYear;

                        frequency.ApplicationContext = dfreq.AppContext.Trim();
                        frequency.RowVersion = dfreq.RowVersion;
                        frequency.UserName = dfreq.EntryUid.Trim();

                        frequency.MarkOld();
                        frequencies.AddToList(frequency);
                    }

                    result.DataList = frequencies;
                    result.Success = true;
                    result.BrowsablePropertyList = Frequency.GetBrowsablePropertyList();

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region InputForm

        internal static DataAccessResult InputFormListGet(InputForm inputFormToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<InputForm> inputForms = new HaiBindingList<InputForm>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DInputForm> dInputForms = new List<DInputForm>();
                    if (inputFormToGet == null)
                    {
                        dInputForms = ahamOper.DInputForms.ToList<AhamDataLinq.DInputForm>();
                    }
                    else
                    {
                        dInputForms = new List<DInputForm>();
                        dInputForms.Add(ahamOper.DInputForms.Single<DInputForm>(x => (x.DInputFormKey == inputFormToGet.InputFormKey)));
                    }

                    foreach (AhamDataLinq.DInputForm dInputForm in dInputForms)
                    {
                        InputForm inputForm = new InputForm();

                        inputForm.InputFormKey = dInputForm.DInputFormKey;
                        inputForm.Title = dInputForm.Title.Trim();
                        inputForm.Title2 = dInputForm.Title2.Trim();
                        inputForm.ShowTotals = Utilities.ConvertByteToBool(dInputForm.ShowTotals);

                        inputForm.ApplicationContext = dInputForm.AppContext.Trim();
                        inputForm.RowVersion = dInputForm.RowVersion;
                        inputForm.UserName = dInputForm.EntryUid.Trim();

                        inputForm.MarkOld();
                        inputForms.AddToList(inputForm);
                    }

                    result.DataList = inputForms;
                    result.Success = true;
                    result.BrowsablePropertyList = InputForm.GetBrowsablePropertyList();

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult InputFormGet(InputForm inputFormToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = InputFormListGet(inputFormToGet, parameters);
            if (result.Success)
            {
                InputForm inputForm = (InputForm)result.SingleItem;
                result.ItemIsChanged = (inputFormToGet.RowVersion != inputForm.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult InputFormSave(InputForm inputForm, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            if (inputForm.IsSavable)
            {
                if (inputForm.IsDeleted)
                    result = InputFormDelete(inputForm, parameters);
                else if (inputForm.IsNew)
                    result = InputFormInsert(inputForm, parameters);
                else if (inputForm.IsDirty)
                    result = InputFormUpdate(inputForm, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult InputFormInsert(InputForm inputForm, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DInputForm dInputForm = new DInputForm();

                    dInputForm.DInputFormKey = inputForm.InputFormKey;
                    dInputForm.Title = inputForm.Title.Trim();
                    dInputForm.Title2 = inputForm.Title2.Trim();
                    dInputForm.ShowTotals = Utilities.ConvertBoolToByte(inputForm.ShowTotals);

                    dInputForm.AppContext = parameters.ApplicationContext.Trim();
                    dInputForm.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DInputForm>().InsertOnSubmit(dInputForm);
                    ahamOper.SubmitChanges();

                    inputForm.InputFormKey = dInputForm.DInputFormKey;
                    inputForm.RowVersion = dInputForm.RowVersion;

                    inputForm.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult InputFormUpdate(InputForm inputForm, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DInputForm dInputForm = ahamOper.DInputForms.Single<DInputForm>(x => (x.DInputFormKey == inputForm.InputFormKey));

                    if (dInputForm.RowVersion != inputForm.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + inputForm.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dInputForm.Title = inputForm.Title.Trim();
                    dInputForm.Title2 = inputForm.Title2.Trim();
                    dInputForm.ShowTotals = Utilities.ConvertBoolToByte(inputForm.ShowTotals);

                    dInputForm.AppContext = parameters.ApplicationContext.Trim();
                    dInputForm.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    inputForm.MarkOld();
                    inputForm.RowVersion = dInputForm.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult InputFormDelete(InputForm inputForm, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DInputForm dInputForm = ahamOper.DInputForms.Single<DInputForm>(x => (x.DInputFormKey == inputForm.InputFormKey));

                    if (dInputForm.RowVersion != inputForm.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + inputForm.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dInputForm.AppContext = parameters.ApplicationContext.Trim();
                    dInputForm.EntryUid = parameters.UserName.Trim();

                    ahamOper.DInputForms.DeleteOnSubmit(dInputForm);
                    ahamOper.SubmitChanges();

                    inputForm.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region InputColumn

        internal static DataAccessResult InputColumnListGet(InputColumn inputColumnToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<InputColumn> inputColumns = new HaiBindingList<InputColumn>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();

                    dlo.LoadWith<DInputColumn>(x => (x.DActivity));
                    dlo.LoadWith<DInputColumn>(x => (x.DGeo));
                    dlo.LoadWith<DInputColumn>(x => (x.DUse));
                    dlo.LoadWith<DInputColumn>(x => (x.DChannel));
                    dlo.LoadWith<DInputColumn>(x => (x.DMeasure));
                    dlo.LoadWith<DInputColumn>(x => (x.DCatalog));
                    dlo.LoadWith<DInputColumn>(x => (x.DMarket));
                    dlo.LoadWith<DInputColumn>(x => (x.DSize));
                    dlo.LoadWith<DInputColumn>(x => (x.DSize1));
                    dlo.LoadWith<DInputColumn>(x => (x.DSize2));
                    dlo.LoadWith<DInputColumn>(x => (x.DSize3));
                    dlo.LoadWith<DInputColumn>(x => (x.DSize4));

                    ahamOper.LoadOptions = dlo;

                    List<DInputColumn> dInputColumns = new List<DInputColumn>();
                    if (inputColumnToGet == null)
                    {
                        dInputColumns = ahamOper.DInputColumns.ToList<DInputColumn>();
                    }
                    else
                    {
                        dInputColumns.Add(ahamOper.DInputColumns.Single<DInputColumn>(x => (x.DInputColumnKey == inputColumnToGet.InputColumnKey)));
                    }

                    foreach (DInputColumn dInputColumn in dInputColumns)
                    {
                        InputColumn inputColumn = new InputColumn();

                        inputColumn.InputColumnKey = dInputColumn.DInputColumnKey;

                        inputColumn.InputFormKey = dInputColumn.DInputFormKey;
                        inputColumn.InputFormName = dInputColumn.DInputForm.Title.Trim() + " | " + dInputColumn.DInputForm.Title2.Trim();
                        inputColumn.ColumnHeader = dInputColumn.ColumnHeader1.Trim();
                        inputColumn.ColumnHeader2 = dInputColumn.ColumnHeader2;
                        inputColumn.SortKey = dInputColumn.SortKey;
                        inputColumn.TargetYear = dInputColumn.targetyear;
                        inputColumn.ActivityKey = dInputColumn.activitykey;
                        if (inputColumn.ActivityKey.HasValue)
                            inputColumn.ActivityName = dInputColumn.DActivity.ActivityName.Trim();
                        inputColumn.GeographyKey = dInputColumn.geokey;
                        if (inputColumn.GeographyKey.HasValue)
                            inputColumn.GeographyName = dInputColumn.DGeo.GeoAbbrev.Trim();
                        inputColumn.UseKey = dInputColumn.usekey;
                        if (inputColumn.UseKey.HasValue)
                            inputColumn.UseName = dInputColumn.DUse.UseName.Trim();
                        inputColumn.ChannelKey = dInputColumn.channelkey;
                        if (inputColumn.ChannelKey.HasValue)
                            inputColumn.ChannelName = dInputColumn.DChannel.ChannelName.Trim();
                        inputColumn.MeasureKey = dInputColumn.measurekey;
                        if (inputColumn.MeasureKey.HasValue)
                            inputColumn.MeasureName = dInputColumn.DMeasure.MeasureName.Trim();
                        inputColumn.CatalogKey = dInputColumn.catalogkey;
                        if (inputColumn.CatalogKey.HasValue)
                            inputColumn.CatalogName = dInputColumn.DCatalog.CatalogName.Trim();
                        inputColumn.MarketKey = dInputColumn.marketkey;
                        if (inputColumn.MarketKey.HasValue)
                            inputColumn.MarketName = dInputColumn.DMarket.MarketName.Trim();
                        inputColumn.Size1Key = dInputColumn.size1key;
                        if (inputColumn.Size1Key.HasValue)
                            inputColumn.Size1Name = "[" + dInputColumn.DSize.DDim.DimName.Trim() + "].[" + dInputColumn.DSize.SizeRowLabel.Trim() + "]";
                        inputColumn.Size2Key = dInputColumn.size2key;
                        if (inputColumn.Size2Key.HasValue)
                            inputColumn.Size2Name = "[" + dInputColumn.DSize1.DDim.DimName.Trim() + "].[" + dInputColumn.DSize1.SizeRowLabel.Trim() + "]";
                        inputColumn.Size3Key = dInputColumn.size3key;
                        if (inputColumn.Size3Key.HasValue)
                            inputColumn.Size3Name = "[" + dInputColumn.DSize2.DDim.DimName.Trim() + "].[" + dInputColumn.DSize2.SizeRowLabel.Trim() + "]";
                        inputColumn.Size4Key = dInputColumn.size4key;
                        if (inputColumn.Size4Key.HasValue)
                            inputColumn.Size4Name = "[" + dInputColumn.DSize3.DDim.DimName.Trim() + "].[" + dInputColumn.DSize3.SizeRowLabel.Trim() + "]";
                        inputColumn.Size5Key = dInputColumn.size5key;
                        if (inputColumn.Size5Key.HasValue)
                            inputColumn.Size5Name = "[" + dInputColumn.DSize4.DDim.DimName.Trim() + "].[" + dInputColumn.DSize4.SizeRowLabel.Trim() + "]";

                        inputColumn.ApplicationContext = dInputColumn.AppContext.Trim();
                        inputColumn.UserName = dInputColumn.EntryUid.Trim();
                        inputColumn.RowVersion = dInputColumn.RowVersion;

                        inputColumn.MarkOld();
                        inputColumns.AddToList(inputColumn);
                    }

                    result.DataList = inputColumns;
                    result.Success = true;
                    result.BrowsablePropertyList = InputColumn.GetBrowsablePropertyList();

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult InputColumnGet(InputColumn inputColumnToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = InputColumnListGet(inputColumnToGet, parameters);
            if (result.Success)
            {
                InputColumn inputColumn = (InputColumn)result.SingleItem;
                result.ItemIsChanged = (inputColumnToGet.RowVersion != inputColumn.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult InputColumnSave(InputColumn inputColumn, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            if (inputColumn.IsSavable)
            {
                if (inputColumn.IsDeleted)
                    result = InputColumnDelete(inputColumn, parameters);
                else if (inputColumn.IsNew)
                    result = InputColumnInsert(inputColumn, parameters);
                else if (inputColumn.IsDirty)
                    result = InputColumnUpdate(inputColumn, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult InputColumnInsert(InputColumn inputColumn, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DInputColumn dInputColumn = new DInputColumn();

                    dInputColumn.DInputFormKey = inputColumn.InputFormKey;
                    dInputColumn.ColumnHeader1 = inputColumn.ColumnHeader.Trim();
                    dInputColumn.ColumnHeader2 = inputColumn.ColumnHeader2.Trim();
                    dInputColumn.SortKey = inputColumn.SortKey;
                    dInputColumn.targetyear = inputColumn.TargetYear;
                    dInputColumn.activitykey = inputColumn.ActivityKey;
                    dInputColumn.geokey = inputColumn.GeographyKey;
                    dInputColumn.usekey = inputColumn.UseKey;
                    dInputColumn.channelkey = inputColumn.ChannelKey;
                    dInputColumn.measurekey = inputColumn.MeasureKey;
                    dInputColumn.catalogkey = inputColumn.CatalogKey;
                    dInputColumn.marketkey = inputColumn.MarketKey;
                    dInputColumn.size1key = inputColumn.Size1Key;
                    dInputColumn.size2key = inputColumn.Size2Key;
                    dInputColumn.size3key = inputColumn.Size3Key;
                    dInputColumn.size4key = inputColumn.Size4Key;
                    dInputColumn.size5key = inputColumn.Size5Key;

                    dInputColumn.AppContext = parameters.ApplicationContext.Trim();
                    dInputColumn.EntryUid = parameters.UserName.Trim();

                    ahamOper.DInputColumns.InsertOnSubmit(dInputColumn);
                    ahamOper.SubmitChanges();

                    inputColumn.InputColumnKey = dInputColumn.DInputColumnKey;
                    inputColumn.RowVersion = dInputColumn.RowVersion;

                    inputColumn.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult InputColumnUpdate(InputColumn inputColumn, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DInputColumn dInputColumn = ahamOper.DInputColumns.Single<DInputColumn>(x => (x.DInputColumnKey == inputColumn.InputColumnKey));

                    if (dInputColumn.RowVersion != inputColumn.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + inputColumn.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dInputColumn.DInputFormKey = inputColumn.InputFormKey;
                    dInputColumn.ColumnHeader1 = inputColumn.ColumnHeader.Trim();
                    dInputColumn.ColumnHeader2 = inputColumn.ColumnHeader2.Trim();
                    dInputColumn.SortKey = inputColumn.SortKey;
                    dInputColumn.targetyear = inputColumn.TargetYear;
                    dInputColumn.activitykey = inputColumn.ActivityKey;
                    dInputColumn.geokey = inputColumn.GeographyKey;
                    dInputColumn.usekey = inputColumn.UseKey;
                    dInputColumn.channelkey = inputColumn.ChannelKey;
                    dInputColumn.measurekey = inputColumn.MeasureKey;
                    dInputColumn.catalogkey = inputColumn.CatalogKey;
                    dInputColumn.marketkey = inputColumn.MarketKey;
                    dInputColumn.size1key = inputColumn.Size1Key;
                    dInputColumn.size2key = inputColumn.Size2Key;
                    dInputColumn.size3key = inputColumn.Size3Key;
                    dInputColumn.size4key = inputColumn.Size4Key;
                    dInputColumn.size5key = inputColumn.Size5Key;

                    dInputColumn.AppContext = parameters.ApplicationContext.Trim();
                    dInputColumn.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    inputColumn.MarkOld();
                    inputColumn.RowVersion = dInputColumn.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult InputColumnDelete(InputColumn inputColumn, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DInputColumn dInputColumn = ahamOper.DInputColumns.Single<DInputColumn>(x => (x.DInputColumnKey == inputColumn.InputColumnKey));

                    if (dInputColumn.RowVersion != inputColumn.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + inputColumn.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dInputColumn.AppContext = parameters.ApplicationContext.Trim();
                    dInputColumn.EntryUid = parameters.UserName.Trim();

                    ahamOper.DInputColumns.DeleteOnSubmit(dInputColumn);
                    ahamOper.SubmitChanges();

                    inputColumn.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region InputRow

        internal static DataAccessResult InputRowListGet(InputRow inputRowToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<InputRow> inputRows = new HaiBindingList<InputRow>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();

                    dlo.LoadWith<DInputRow>(x => (x.DActivity));
                    dlo.LoadWith<DInputRow>(x => (x.DGeo));
                    dlo.LoadWith<DInputRow>(x => (x.DUse));
                    dlo.LoadWith<DInputRow>(x => (x.DChannel));
                    dlo.LoadWith<DInputRow>(x => (x.DMeasure));
                    dlo.LoadWith<DInputRow>(x => (x.DCatalog));
                    dlo.LoadWith<DInputRow>(x => (x.DMarket));
                    dlo.LoadWith<DInputRow>(x => (x.DSize));
                    dlo.LoadWith<DInputRow>(x => (x.DSize1));
                    dlo.LoadWith<DInputRow>(x => (x.DSize2));
                    dlo.LoadWith<DInputRow>(x => (x.DSize3));
                    dlo.LoadWith<DInputRow>(x => (x.DSize4));

                    ahamOper.LoadOptions = dlo;

                    List<DInputRow> dInputRows = new List<DInputRow>();
                    if (inputRowToGet == null)
                    {
                        dInputRows = ahamOper.DInputRows.ToList<DInputRow>();
                    }
                    else
                    {
                        dInputRows.Add(ahamOper.DInputRows.Single<DInputRow>(x => (x.DInputRowKey == inputRowToGet.InputRowKey)));
                    }

                    foreach (DInputRow dInputRow in dInputRows)
                    {
                        InputRow inputRow = new InputRow();

                        inputRow.InputRowKey = dInputRow.DInputRowKey;

                        inputRow.InputFormKey = dInputRow.DInputFormKey;
                        inputRow.InputFormName = (dInputRow.DInputForm.Title.Trim() + " | " + dInputRow.DInputForm.Title2.Trim()).Trim();
                        inputRow.RowHeader = dInputRow.RowHeader1.Trim();
                        inputRow.RowHeader2 = dInputRow.RowHeader2;
                        inputRow.SortKey = dInputRow.sortkey;
                        inputRow.TargetYear = dInputRow.TargetYear;
                        inputRow.ActivityKey = dInputRow.activitykey;
                        if (inputRow.ActivityKey.HasValue)
                            inputRow.ActivityName = dInputRow.DActivity.ActivityName.Trim();
                        inputRow.GeographyKey = dInputRow.geokey;
                        if (inputRow.GeographyKey.HasValue)
                            inputRow.GeographyName = dInputRow.DGeo.GeoAbbrev.Trim();
                        inputRow.UseKey = dInputRow.geokey;
                        if (inputRow.UseKey.HasValue)
                            inputRow.UseName = dInputRow.DUse.UseName.Trim();
                        inputRow.ChannelKey = dInputRow.channelkey;
                        if (inputRow.ChannelKey.HasValue)
                            inputRow.ChannelName = dInputRow.DChannel.ChannelName.Trim();
                        inputRow.MeasureKey = dInputRow.measurekey;
                        if (inputRow.MeasureKey.HasValue)
                            inputRow.MeasureName = dInputRow.DMeasure.MeasureName.Trim();
                        inputRow.CatalogKey = dInputRow.catalogkey;
                        if (inputRow.CatalogKey.HasValue)
                            inputRow.CatalogName = dInputRow.DCatalog.CatalogName.Trim();
                        inputRow.MarketKey = dInputRow.marketkey;
                        if (inputRow.MarketKey.HasValue)
                            inputRow.MarketName = dInputRow.DMarket.MarketName.Trim();

                        inputRow.Size1Key = dInputRow.size1key;
                        if (inputRow.Size1Key.HasValue)
                            inputRow.Size1Name = "[" + dInputRow.DSize.DDim.DimName.Trim() + "].[" + dInputRow.DSize.SizeRowLabel.Trim() + "]";
                        inputRow.Size2Key = dInputRow.size2key;
                        if (inputRow.Size2Key.HasValue)
                            inputRow.Size2Name = "[" + dInputRow.DSize1.DDim.DimName.Trim() + "].[" + dInputRow.DSize1.SizeRowLabel.Trim() + "]";
                        inputRow.Size3Key = dInputRow.size3key;
                        if (inputRow.Size3Key.HasValue)
                            inputRow.Size3Name = "[" + dInputRow.DSize2.DDim.DimName.Trim() + "].[" + dInputRow.DSize2.SizeRowLabel.Trim() + "]";
                        inputRow.Size4Key = dInputRow.size4key;
                        if (inputRow.Size4Key.HasValue)
                            inputRow.Size4Name = "[" + dInputRow.DSize3.DDim.DimName.Trim() + "].[" + dInputRow.DSize3.SizeRowLabel.Trim() + "]";
                        inputRow.Size5Key = dInputRow.size5key;
                        if (inputRow.Size5Key.HasValue)
                            inputRow.Size5Name = "[" + dInputRow.DSize4.DDim.DimName.Trim() + "].[" + dInputRow.DSize4.SizeRowLabel.Trim() + "]";

                        inputRow.ApplicationContext = dInputRow.AppContext.Trim();
                        inputRow.UserName = dInputRow.EntryUid.Trim();
                        inputRow.RowVersion = dInputRow.RowVersion;

                        inputRow.MarkOld();
                        inputRows.AddToList(inputRow);
                    }

                    result.DataList = inputRows;
                    result.Success = true;
                    result.BrowsablePropertyList = InputRow.GetBrowsablePropertyList();

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult InputRowGet(InputRow inputRowToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = InputRowListGet(inputRowToGet, parameters);
            if (result.Success)
            {
                InputRow inputRow = (InputRow)result.SingleItem;
                result.ItemIsChanged = (inputRowToGet.RowVersion != inputRow.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult InputRowSave(InputRow inputRow, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            if (inputRow.IsSavable)
            {
                if (inputRow.IsDeleted)
                    result = InputRowDelete(inputRow, parameters);
                else if (inputRow.IsNew)
                    result = InputRowInsert(inputRow, parameters);
                else if (inputRow.IsDirty)
                    result = InputRowUpdate(inputRow, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult InputRowInsert(InputRow inputRow, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DInputRow dInputRow = new DInputRow();

                    dInputRow.DInputFormKey = inputRow.InputFormKey;
                    dInputRow.RowHeader1 = inputRow.RowHeader.Trim();
                    dInputRow.RowHeader2 = inputRow.RowHeader2.Trim();
                    dInputRow.sortkey = inputRow.SortKey;
                    dInputRow.TargetYear = inputRow.TargetYear;
                    dInputRow.activitykey = inputRow.ActivityKey;
                    dInputRow.geokey = inputRow.GeographyKey;
                    dInputRow.usekey = inputRow.UseKey;
                    dInputRow.channelkey = inputRow.ChannelKey;
                    dInputRow.measurekey = inputRow.MeasureKey;
                    dInputRow.catalogkey = inputRow.CatalogKey;
                    dInputRow.marketkey = inputRow.MarketKey;
                    dInputRow.size1key = inputRow.Size1Key;
                    dInputRow.size2key = inputRow.Size2Key;
                    dInputRow.size3key = inputRow.Size3Key;
                    dInputRow.size4key = inputRow.Size4Key;
                    dInputRow.size5key = inputRow.Size5Key;

                    dInputRow.AppContext = parameters.ApplicationContext.Trim();
                    dInputRow.EntryUid = parameters.UserName.Trim();

                    ahamOper.DInputRows.InsertOnSubmit(dInputRow);
                    ahamOper.SubmitChanges();

                    inputRow.InputRowKey = dInputRow.DInputRowKey;
                    inputRow.RowVersion = dInputRow.RowVersion;

                    inputRow.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult InputRowUpdate(InputRow inputRow, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DInputRow dInputRow = ahamOper.DInputRows.Single<DInputRow>(x => (x.DInputRowKey == inputRow.InputRowKey));

                    if (dInputRow.RowVersion != inputRow.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + inputRow.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dInputRow.DInputFormKey = inputRow.InputFormKey;
                    dInputRow.RowHeader1 = inputRow.RowHeader.Trim();
                    dInputRow.RowHeader2 = inputRow.RowHeader2.Trim();
                    dInputRow.sortkey = inputRow.SortKey;
                    dInputRow.TargetYear = inputRow.TargetYear;
                    dInputRow.activitykey = inputRow.ActivityKey;
                    dInputRow.geokey = inputRow.GeographyKey;
                    dInputRow.usekey = inputRow.UseKey;
                    dInputRow.channelkey = inputRow.ChannelKey;
                    dInputRow.measurekey = inputRow.MeasureKey;
                    dInputRow.catalogkey = inputRow.CatalogKey;
                    dInputRow.marketkey = inputRow.MarketKey;
                    dInputRow.size1key = inputRow.Size1Key;
                    dInputRow.size2key = inputRow.Size2Key;
                    dInputRow.size3key = inputRow.Size3Key;
                    dInputRow.size4key = inputRow.Size4Key;
                    dInputRow.size5key = inputRow.Size5Key;

                    dInputRow.AppContext = parameters.ApplicationContext.Trim();
                    dInputRow.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    inputRow.MarkOld();
                    inputRow.RowVersion = dInputRow.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult InputRowDelete(InputRow inputRow, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DInputRow dInputRow = ahamOper.DInputRows.Single<DInputRow>(x => (x.DInputRowKey == inputRow.InputRowKey));

                    if (dInputRow.RowVersion != inputRow.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + inputRow.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dInputRow.AppContext = parameters.ApplicationContext.Trim();
                    dInputRow.EntryUid = parameters.UserName.Trim();

                    ahamOper.DInputRows.DeleteOnSubmit(dInputRow);
                    ahamOper.SubmitChanges();

                    inputRow.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region ManfacturerReport

        internal static DataAccessResult ManufacturerReportListGet(ManufacturerReport manufacturerReportToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ManufacturerReport> manufacturerReports = new HaiBindingList<ManufacturerReport>();

            // Create a dictionary of SizeSets. This will be used to lookup the SizeSetName since this cannot be
            // looked up directly from dRMfg.SDSize.SizeSetName or dRMfg.SDSize.SizeSetName2.  The only useable name
            // is *constructed* in the SizeSet class
            
            Dictionary<int, SizeSet> sizeSetDict = new Dictionary<int, SizeSet>();
            DataAccessResult sizeSetResult = AhamDataServiceSize.SizeSetListGet(null, parameters);
            if (sizeSetResult.Success)
            {
                HaiBindingList<SizeSet> sizeSetList = (HaiBindingList<SizeSet>)sizeSetResult.DataList;
                sizeSetDict = sizeSetList.ToDictionary(x => (x.SizeSetKey));
            }
            else
            {
                result.Success = false;
                result.Message = "SizeSetListGet() failed.  " + sizeSetResult.Message;
                return result;
            }

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.DMfg));
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.DProduct));
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.DFreq));
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.SDActivity));
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.SDGeo));
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.SDUse));
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.SDChannel));
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.MarketSetKey));
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.SDMeasure));
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.SDCatalog));
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.SDSize));
                    dlo.LoadWith<AhamDataLinq.RMfg>(x => (x.DInputForm));
                    ahamOper.LoadOptions = dlo;

                    List<AhamDataLinq.RMfg> dRMfgs;
                    if (manufacturerReportToGet == null)
                    {
                        dRMfgs = ahamOper.RMfgs.ToList<AhamDataLinq.RMfg>();
                    }
                    else
                    {
                        dRMfgs = new List<RMfg>();
                        dRMfgs.Add(ahamOper.RMfgs.Single<AhamDataLinq.RMfg>(x => (x.MfgReportKey==manufacturerReportToGet.ManufacturerReportKey)));
                    }

                    foreach (AhamDataLinq.RMfg dRMfg in dRMfgs)
                    {
                        ManufacturerReport manufacturerReport = new ManufacturerReport();

                        manufacturerReport.ManufacturerReportKey = dRMfg.MfgReportKey;
                        manufacturerReport.ManufacturerKey = dRMfg.MfgKey;
                        manufacturerReport.ManufacturerName = dRMfg.DMfg.MfgName.Trim();
                        manufacturerReport.ProductKey = dRMfg.ProductKey;
                        manufacturerReport.ProductName = dRMfg.DProduct.ProductName.Trim();
                        manufacturerReport.FrequencyKey = dRMfg.FreqKey;
                        manufacturerReport.FrequencyName = dRMfg.DFreq.FreqName.Trim();
                        manufacturerReport.ActivitySetKey = dRMfg.ActivitySetKey;
                        manufacturerReport.ActivitySetName = dRMfg.SDActivity.ActivitySetName.Trim();
                        manufacturerReport.GeographySetKey = dRMfg.GeoSetKey;
                        manufacturerReport.GeographySetName = dRMfg.SDGeo.GeoSetName.Trim();
                        manufacturerReport.UseSetKey = dRMfg.UseSetKey;
                        manufacturerReport.UseSetName = dRMfg.SDUse.UseSetName.Trim();
                        manufacturerReport.ChannelSetKey = dRMfg.ChannelSetKey;
                        manufacturerReport.ChannelSetName = dRMfg.SDChannel.ChannelSetName.Trim();
                        manufacturerReport.MarketSetKey = dRMfg.MarketSetKey;
                        manufacturerReport.MarketSetName = dRMfg.SDMarket.MarketSetName.Trim();
                        manufacturerReport.MeasureSetKey = dRMfg.MeasureSetKey;
                        manufacturerReport.MeasureSetName = dRMfg.SDMeasure.MeasureSetName.Trim();
                        manufacturerReport.CatalogSetKey = dRMfg.CatalogSetKey;
                        manufacturerReport.CatalogSetName = dRMfg.SDCatalog.CatalogSetName.Trim();
                        manufacturerReport.DateRange.BeginDate = dRMfg.BeginDate.Date;
                        manufacturerReport.DateRange.EndDate = dRMfg.EndDate.Date;
                        manufacturerReport.Status = dRMfg.Status.ToString().Trim();
                        manufacturerReport.SerialNumber = Utilities.ConvertByteToBool(dRMfg.SerialNo);
                        manufacturerReport.ReportName = dRMfg.RptName.Trim();
                        manufacturerReport.SizeSetKey = dRMfg.SizeSetKey;

                        // N.B. The size set name used here is a constructed name must match the name
                        //... that is constructed by SP ceSdSizeLookup().  See AhamDataServiceSize.SizeSetListGet()
                        manufacturerReport.SizeSetName = (sizeSetDict[manufacturerReport.SizeSetKey]).SizeSetName;      // set the SizeSetName using the dictionary of SizeSets created in the prolog

                        manufacturerReport.InputFormKey = dRMfg.dInputFormKey;
                        manufacturerReport.InputFormName = (dRMfg.DInputForm.Title.Trim() + " | " + dRMfg.DInputForm.Title2.Trim()).Trim();
                        manufacturerReport.YTD = Utilities.ConvertByteToBool(dRMfg.ytd);
                        manufacturerReport.Master = Utilities.ConvertByteToBool(dRMfg.master);
                        manufacturerReport.CreateWeekly = Utilities.ConvertByteToBool(dRMfg.CreateWeekly);
                        manufacturerReport.Reconcile = Utilities.ConvertByteToBool(dRMfg.reconcile);
                        manufacturerReport.Calendar = Utilities.ConvertByteToBool(dRMfg.Calendar);
                        manufacturerReport.Forecasted = Utilities.ConvertByteToBool(dRMfg.forecasted);
                        manufacturerReport.ParticipationChecked = Utilities.ConvertByteToBool(dRMfg.ParticipationChecked);
                        manufacturerReport.SkipReportCard = Utilities.ConvertByteToBool(dRMfg.SkipReportCard);

                        manufacturerReport.ApplicationContext = dRMfg.AppContext.Trim();
                        manufacturerReport.RowVersion = dRMfg.RowVersion;
                        manufacturerReport.UserName = dRMfg.EntryUid.Trim();

                        manufacturerReport.MarkOld();
                        manufacturerReports.AddToList(manufacturerReport);
                    }

                    result.DataList = manufacturerReports;
                    result.Success = true;
                    result.BrowsablePropertyList = ManufacturerReport.GetBrowsablePropertyList();

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        internal static DataAccessResult ManufacturerReportGet(ManufacturerReport manufacturerReportToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ManufacturerReportListGet(manufacturerReportToGet, parameters);
            if (result.Success)
            {
                ManufacturerReport manufacturerReport = (ManufacturerReport)result.SingleItem;
                result.ItemIsChanged = (manufacturerReportToGet.RowVersion != manufacturerReport.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ManufacturerReportSave(ManufacturerReport manufacturerReport, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (manufacturerReport.IsSavable)
            {
                if (manufacturerReport.IsDeleted)
                    result = ManufacturerReportDelete(manufacturerReport, parameters);
                else if (manufacturerReport.IsNew)
                    result = ManufacturerReportInsert(manufacturerReport, parameters);
                else if (manufacturerReport.IsDirty)
                    result = ManufacturerReportUpdate(manufacturerReport, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ManufacturerReportInsert(ManufacturerReport manufacturerReport, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.RMfg dRMfg = new AhamDataLinq.RMfg();

                    dRMfg.MfgKey = manufacturerReport.ManufacturerKey;
                    dRMfg.ProductKey = manufacturerReport.ProductKey;
                    dRMfg.FreqKey = manufacturerReport.FrequencyKey;
                    dRMfg.ActivitySetKey = manufacturerReport.ActivitySetKey;
                    dRMfg.GeoSetKey = manufacturerReport.GeographySetKey;
                    dRMfg.UseSetKey = manufacturerReport.UseSetKey;
                    dRMfg.ChannelSetKey = manufacturerReport.ChannelSetKey;
                    dRMfg.MarketSetKey = manufacturerReport.MarketSetKey;
                    dRMfg.MeasureSetKey = manufacturerReport.MeasureSetKey;
                    dRMfg.CatalogSetKey = manufacturerReport.CatalogSetKey;
                    dRMfg.BeginDate = manufacturerReport.BeginDate.Date;
                    dRMfg.EndDate = manufacturerReport.EndDate.Date;
                    dRMfg.Status = manufacturerReport.Status;
                    dRMfg.SerialNo = Utilities.ConvertBoolToByte(manufacturerReport.SerialNumber);
                    dRMfg.RptName = manufacturerReport.ReportName.Trim();
                    dRMfg.SizeSetKey = manufacturerReport.SizeSetKey;
                    dRMfg.dInputFormKey = manufacturerReport.InputFormKey;
                    dRMfg.ytd = Utilities.ConvertBoolToByte(manufacturerReport.YTD);
                    dRMfg.master = Utilities.ConvertBoolToByte(manufacturerReport.Master);
                    dRMfg.CreateWeekly = Utilities.ConvertBoolToByte(manufacturerReport.CreateWeekly);
                    dRMfg.reconcile = Utilities.ConvertBoolToByte(manufacturerReport.Reconcile);
                    dRMfg.Calendar = Utilities.ConvertBoolToByte(manufacturerReport.Calendar);
                    dRMfg.forecasted = Utilities.ConvertBoolToByte(manufacturerReport.Forecasted);
                    dRMfg.ParticipationChecked = Utilities.ConvertBoolToByte(manufacturerReport.ParticipationChecked);
                    dRMfg.SkipReportCard = Utilities.ConvertBoolToByte(manufacturerReport.SkipReportCard);

                    dRMfg.AppContext = parameters.ApplicationContext.Trim();
                    dRMfg.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<AhamDataLinq.RMfg>().InsertOnSubmit(dRMfg);
                    ahamOper.SubmitChanges();

                    manufacturerReport.ManufacturerReportKey = dRMfg.MfgReportKey;
                    manufacturerReport.RowVersion = dRMfg.RowVersion;

                    manufacturerReport.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ManufacturerReportUpdate(ManufacturerReport manufacturerReport, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.RMfg dRMfg = ahamOper.RMfgs.Single<AhamDataLinq.RMfg>(x => (x.MfgReportKey == manufacturerReport.ManufacturerReportKey));

                    if (dRMfg.RowVersion != manufacturerReport.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + manufacturerReport.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dRMfg.MfgKey = manufacturerReport.ManufacturerKey;
                    dRMfg.ProductKey = manufacturerReport.ProductKey;
                    dRMfg.FreqKey = manufacturerReport.FrequencyKey;
                    dRMfg.ActivitySetKey = manufacturerReport.ActivitySetKey;
                    dRMfg.GeoSetKey = manufacturerReport.GeographySetKey;
                    dRMfg.UseSetKey = manufacturerReport.UseSetKey;
                    dRMfg.ChannelSetKey = manufacturerReport.ChannelSetKey;
                    dRMfg.MarketSetKey = manufacturerReport.MarketSetKey;
                    dRMfg.MeasureSetKey = manufacturerReport.MeasureSetKey;
                    dRMfg.CatalogSetKey = manufacturerReport.CatalogSetKey;
                    dRMfg.BeginDate = manufacturerReport.BeginDate.Date;
                    dRMfg.EndDate = manufacturerReport.EndDate.Date;
                    dRMfg.Status = manufacturerReport.Status;
                    dRMfg.SerialNo = Utilities.ConvertBoolToByte(manufacturerReport.SerialNumber);
                    dRMfg.RptName = manufacturerReport.ReportName.Trim();
                    dRMfg.SizeSetKey = manufacturerReport.SizeSetKey;
                    dRMfg.dInputFormKey = manufacturerReport.InputFormKey;
                    dRMfg.ytd = Utilities.ConvertBoolToByte(manufacturerReport.YTD);
                    dRMfg.master = Utilities.ConvertBoolToByte(manufacturerReport.Master);
                    dRMfg.CreateWeekly = Utilities.ConvertBoolToByte(manufacturerReport.CreateWeekly);
                    dRMfg.reconcile = Utilities.ConvertBoolToByte(manufacturerReport.Reconcile);
                    dRMfg.Calendar = Utilities.ConvertBoolToByte(manufacturerReport.Calendar);
                    dRMfg.forecasted = Utilities.ConvertBoolToByte(manufacturerReport.Forecasted);
                    dRMfg.ParticipationChecked = Utilities.ConvertBoolToByte(manufacturerReport.ParticipationChecked);
                    dRMfg.SkipReportCard = Utilities.ConvertBoolToByte(manufacturerReport.SkipReportCard);

                    dRMfg.AppContext = parameters.ApplicationContext.Trim();
                    dRMfg.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    manufacturerReport.MarkOld();
                    manufacturerReport.RowVersion = dRMfg.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ManufacturerReportDelete(ManufacturerReport manufacturerReport, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.RMfg dRMfg = ahamOper.RMfgs.Single<AhamDataLinq.RMfg>(x => (x.MfgReportKey == manufacturerReport.ManufacturerReportKey));

                    if (dRMfg.RowVersion != manufacturerReport.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + manufacturerReport.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dRMfg.AppContext = parameters.ApplicationContext.Trim();
                    dRMfg.EntryUid = parameters.UserName.Trim();

                    ahamOper.RMfgs.DeleteOnSubmit(dRMfg);
                    ahamOper.SubmitChanges();

                    manufacturerReport.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessTestResult TestReportForSaveConflicts(ManufacturerReport manufacturerReport, DataServiceParameters parameters)
        {
            DataAccessTestResult result = new DataAccessTestResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                string message = string.Empty;
                int? manufacturerReportKey;
                manufacturerReportKey=manufacturerReport.ManufacturerReportKey;
                int intAsBoolean = 0;

                try
                {
                    intAsBoolean = ahamOper.IsPlogOutOfRange(manufacturerReportKey, manufacturerReport.BeginDate.Date, manufacturerReport.EndDate.Date, ref message);

                    if (intAsBoolean == 0)
                    {
                        result.Passed = true;
                        result.Success = true;
                        result.Message = string.Empty;
                    }
                    else
                    {
                        result.Passed = false;
                        result.Success = true;
                        result.Message = message;
                    }
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = "Test for save conflicts failed:\n\n" + ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region SuppressReport

        internal static DataAccessResult SuppressReportListGet(SuppressReport suppressReportToGet, DataServiceParameters parameters)
        {
            Dictionary<int, IndustryReportMember> industryReportDict;
            DataAccessResult industryReportMemberResult = AhamDataServiceReportManagement.IndustryReportMemberListGet(null, parameters);
            if (industryReportMemberResult.Success)
            {
                HaiBindingList<IndustryReportMember> industryReportList = (HaiBindingList<IndustryReportMember>)industryReportMemberResult.DataList;
                industryReportDict = industryReportList.ToDictionary(x => (x.IndustryReportMemberKey));
            }
            else
            {
                return industryReportMemberResult;
            }

            DataAccessResult result = new DataAccessResult();
            HaiBindingList<SuppressReport> suppressReports = new HaiBindingList<SuppressReport>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<AhamDataLinq.SupressReport> dSuppressReports;
                    if (suppressReportToGet == null)
                        dSuppressReports = ahamOper.SupressReports.ToList<AhamDataLinq.SupressReport>();
                    else
                    {
                        dSuppressReports = new List<AhamDataLinq.SupressReport>();
                        dSuppressReports.Add(ahamOper.SupressReports.Single<AhamDataLinq.SupressReport>(x => (x.SupressReportKey == suppressReportToGet.SuppressReportKey)));
                    }

                    foreach (AhamDataLinq.SupressReport dSuppressReport in dSuppressReports)
                    {
                        SuppressReport suppressReport = new SuppressReport();

                        suppressReport.SuppressReportKey = dSuppressReport.SupressReportKey;
                        suppressReport.IndustryReportMemberKey = dSuppressReport.RindustrymemberKey;
                        suppressReport.IndustryReportMemberName = industryReportDict[suppressReport.IndustryReportMemberKey].IndustryReportMemberName.Trim();
                        suppressReport.BeginDate = dSuppressReport.begindate.Date;
                        suppressReport.EndDate = dSuppressReport.Enddate.Date;

                        suppressReport.ApplicationContext = dSuppressReport.AppContext.Trim();
                        suppressReport.UserName = dSuppressReport.EntryUid.Trim();
                        suppressReport.RowVersion = dSuppressReport.RowVersion;

                        suppressReport.MarkOld();
                        suppressReports.AddToList(suppressReport);
                    }

                    result.DataList = suppressReports;
                    result.Success = true;
                    result.BrowsablePropertyList = SuppressReport.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult SuppressReportGet(SuppressReport suppressReportToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = SuppressReportListGet(suppressReportToGet, parameters);
            if (result.Success)
            {
                SuppressReport suppressReport = (SuppressReport)result.SingleItem;
                result.ItemIsChanged = (suppressReportToGet.RowVersion != suppressReport.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult SuppressReportSave(SuppressReport suppressReport, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (suppressReport.IsSavable)
            {
                if (suppressReport.IsDeleted)
                    result = SuppressReportDelete(suppressReport, parameters);
                else if (suppressReport.IsNew)
                    result = SuppressReportInsert(suppressReport, parameters);
                else if (suppressReport.IsDirty)
                    result = SuppressReportUpdate(suppressReport, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult SuppressReportInsert(SuppressReport suppressReport, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.SupressReport dSuppressReport = new AhamDataLinq.SupressReport();
                    dSuppressReport.RindustrymemberKey = suppressReport.IndustryReportMemberKey;
                    dSuppressReport.begindate = suppressReport.DateRange.BeginDate.Date;
                    dSuppressReport.Enddate = suppressReport.DateRange.EndDate.Date;

                    dSuppressReport.AppContext = parameters.ApplicationContext.Trim();
                    dSuppressReport.EntryUid = parameters.UserName.Trim();

                    ahamOper.SupressReports.InsertOnSubmit(dSuppressReport);
                    ahamOper.SubmitChanges();

                    suppressReport.SuppressReportKey = dSuppressReport.SupressReportKey;
                    suppressReport.RowVersion = dSuppressReport.RowVersion;

                    suppressReport.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult SuppressReportUpdate(SuppressReport suppressReport, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.SupressReport dSuppressReport = ahamOper.SupressReports.Single<AhamDataLinq.SupressReport>(x => (x.SupressReportKey == suppressReport.SuppressReportKey));

                    if (dSuppressReport.RowVersion != suppressReport.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + suppressReport.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dSuppressReport.RindustrymemberKey = suppressReport.IndustryReportMemberKey;
                    dSuppressReport.begindate = suppressReport.DateRange.BeginDate.Date;
                    dSuppressReport.Enddate = suppressReport.DateRange.EndDate.Date;

                    dSuppressReport.AppContext = parameters.ApplicationContext.Trim();
                    dSuppressReport.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    suppressReport.MarkOld();
                    suppressReport.RowVersion = dSuppressReport.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult SuppressReportDelete(SuppressReport suppressReport, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.SupressReport dSuppressReport = ahamOper.SupressReports.Single<AhamDataLinq.SupressReport>(x => (x.SupressReportKey == suppressReport.SuppressReportKey));

                    if (dSuppressReport.RowVersion != suppressReport.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + suppressReport.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dSuppressReport.AppContext = parameters.ApplicationContext.Trim();
                    dSuppressReport.EntryUid = parameters.UserName.Trim();

                    ahamOper.SupressReports.DeleteOnSubmit(dSuppressReport);
                    ahamOper.SubmitChanges();

                    suppressReport.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion


        public static DataAccessResult IndustryReportMemberListGet(IndustryReportMember industryReportMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<IndustryReportMember> industryReportMembers = new HaiBindingList<IndustryReportMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    int key;
                    if (industryReportMemberToGet == null)
                        key = 0;
                    else
                        key = industryReportMemberToGet.IndustryReportMemberKey;

                    var industryNameResult = ahamOper.ceRindustryMembersLookupNoOutput(0);
                    foreach (ceRindustryMembersLookupNoOutputResult industryReportMemberRs in industryNameResult)
                    {
                        IndustryReportMember industryReportMember = new IndustryReportMember();

                        industryReportMember.IndustryReportMemberKey = industryReportMemberRs.RIndustryMemberKey;
                        industryReportMember.IndustryReportMemberName = industryReportMemberRs.RindustryMemberName.Trim();

                        industryReportMember.RowVersion = industryReportMemberRs.RowVersion;
                        industryReportMember.ApplicationContext = industryReportMemberRs.AppContext;
                        industryReportMember.UserName = industryReportMemberRs.EntryUid;

                        industryReportMember.MarkOld();
                        industryReportMembers.AddToList(industryReportMember);
                    }

                    industryNameResult.Dispose();

                    result.DataList = industryReportMembers;
                    result.Success = true;
                    result.BrowsablePropertyList = IndustryReportMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #region ManufacturerPLog

        public static DataAccessResult ManufacturerPLogListGet(int manufacturerReportKey, int manufacturerPLogKey, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ManufacturerPLog> manufacturerPLogs = new HaiBindingList<ManufacturerPLog>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    var manufacturerPLogResult = ahamOper.ceMfgPlogLookup(manufacturerReportKey, manufacturerPLogKey);
                    foreach (ceMfgPlogLookupResult manufacturerPLogRs in manufacturerPLogResult)
                    {
                        ManufacturerPLog manufacturerPLog = new ManufacturerPLog();

                        manufacturerPLog.ManufacturerPLogKey = manufacturerPLogRs.MfgPlogKey;

                        manufacturerPLog.HasData = manufacturerPLogRs.DataExists.Value;
                        manufacturerPLog.EndDate = manufacturerPLogRs.EndDate;
                        manufacturerPLog.PostPeriod = manufacturerPLogRs.PostPeriod;
                        manufacturerPLog.StatusName = manufacturerPLogRs.StatusName.Trim();

                        manufacturerPLog.ApplicationContext = manufacturerPLogRs.AppContext.Trim();
                        manufacturerPLog.UserName = manufacturerPLogRs.EntryUid.Trim();
                        manufacturerPLog.RowVersion = manufacturerPLogRs.RowVersion;

                        manufacturerPLog.MarkOld();
                        manufacturerPLogs.AddToList(manufacturerPLog);
                    }

                    manufacturerPLogResult.Dispose();

                    result.DataList = manufacturerPLogs;
                    result.Success = true;
                    result.BrowsablePropertyList = ManufacturerPLog.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        public static DataAccessResult ManufacturerPLogListDelete(HaiBindingList<ManufacturerPLog> manufacturerPlogsToDelete, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ManufacturerPLog> manufacturerPLogs = new HaiBindingList<ManufacturerPLog>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<MfgPlog> deleteList = new List<MfgPlog>();

                    // check validity of each item to delete and build list of items to delete
                    foreach (ManufacturerPLog pLogToDelete in manufacturerPlogsToDelete)
                    {
                        MfgPlog mfgPlogToDelete = ahamOper.MfgPlogs.Single<MfgPlog>(x => (x.MfgPlogKey == pLogToDelete.ManufacturerPLogKey));
                        if (mfgPlogToDelete.RowVersion != pLogToDelete.RowVersion)
                        {
                            string message = "Concurrency error for PLog with key: " + pLogToDelete.ManufacturerPLogKey.ToString() + "\n\rDeletions aborted.";
                            result.Success = false;
                            result.Message = message;
                            return result;
                        }
                        else
                        {
                            mfgPlogToDelete.AppContext = parameters.ApplicationContext.Trim();
                            mfgPlogToDelete.EntryUid = parameters.UserName.Trim();
                            deleteList.Add(mfgPlogToDelete);
                        }
                    }

                    ahamOper.MfgPlogs.DeleteAllOnSubmit(deleteList);
                    ahamOper.SubmitChanges();

                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region OutputDistributionManufacturer

        internal static DataAccessResult OutputDistributionManufacturerListGet(OutputDistributionManufacturer outputDistributionManufacturerToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<OutputDistributionManufacturer> ODMs = new HaiBindingList<OutputDistributionManufacturer>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<AhamDataLinq.OutputDistributionMfg>(x => (x.DOutPutReport));
                    dlo.LoadWith<AhamDataLinq.OutputDistributionMfg>(x => (x.DMfg));
                    ahamOper.LoadOptions = dlo;

                    List<AhamDataLinq.OutputDistributionMfg> dODMs = new List<OutputDistributionMfg>();
                    if (outputDistributionManufacturerToGet == null)
                    {
                        dODMs = ahamOper.OutputDistributionMfgs.ToList<AhamDataLinq.OutputDistributionMfg>();
                    }
                    else
                    {
                        dODMs = new List<OutputDistributionMfg>();
                        dODMs.Add(ahamOper.OutputDistributionMfgs.Single<AhamDataLinq.OutputDistributionMfg>(x => (x.OutputDistributionMfgKey == outputDistributionManufacturerToGet.OutputDistributionManufacturerKey)));
                    }

                    foreach (AhamDataLinq.OutputDistributionMfg dODM in dODMs)
                    {
                        OutputDistributionManufacturer ODM = new OutputDistributionManufacturer();

                        ODM.OutputDistributionManufacturerKey = dODM.OutputDistributionMfgKey;
                        ODM.OutputReportKey = dODM.OutputReportKey;
                        ODM.OutputReportID = dODM.DOutPutReport.OutputReportID.Trim();
                        ODM.ManufacturerKey = dODM.mfgkey;
                        ODM.ManufacturerName = dODM.DMfg.MfgName.Trim();

                        if (dODM.disabled == 0)
                            ODM.Disabled = false;
                        else
                            ODM.Disabled = true;

                        ODM.BeginDate = dODM.begindate.Date;
                        ODM.EndDate = dODM.Enddate.Date;

                        ODM.ApplicationContext = dODM.AppContext.Trim();
                        ODM.UserName = dODM.EntryUid.Trim();
                        ODM.RowVersion = dODM.RowVersion;

                        ODM.MarkOld();
                        ODMs.AddToList(ODM);
                    }

                    result.DataList = ODMs;
                    result.Success = true;
                    result.BrowsablePropertyList = OutputDistributionManufacturer.GetBrowsablePropertyList();

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult OutputDistributionManufacturerGet(OutputDistributionManufacturer outputDistributionManufacturerToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = OutputDistributionManufacturerListGet(outputDistributionManufacturerToGet, parameters);
            if (result.Success)
            {
                OutputDistributionManufacturer ODM = (OutputDistributionManufacturer)result.SingleItem;
                result.ItemIsChanged = (outputDistributionManufacturerToGet.RowVersion != ODM.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult OutputDistributionManufacturerSave(OutputDistributionManufacturer outputDistributionManufacturer, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            if (outputDistributionManufacturer.IsSavable)
            {
                if (outputDistributionManufacturer.IsDeleted)
                    result = OutputDistributionManufacturerDelete(outputDistributionManufacturer, parameters);
                else if (outputDistributionManufacturer.IsNew)
                    result = OutputDistributionManufacturerInsert(outputDistributionManufacturer, parameters);
                else if (outputDistributionManufacturer.IsDirty)
                    result = OutputDistributionManufacturerUpdate(outputDistributionManufacturer, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult OutputDistributionManufacturerInsert(OutputDistributionManufacturer outputDistributionManufacturer, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.OutputDistributionMfg dODM = new OutputDistributionMfg();

                    dODM.mfgkey = outputDistributionManufacturer.ManufacturerKey;
                    dODM.OutputReportKey = outputDistributionManufacturer.OutputReportKey;

                    if (outputDistributionManufacturer.Disabled)
                        dODM.disabled = 1;
                    else
                        dODM.disabled = 0;

                    dODM.begindate = outputDistributionManufacturer.BeginDate.Date;
                    dODM.Enddate = outputDistributionManufacturer.EndDate.Date;

                    dODM.AppContext = parameters.ApplicationContext.Trim();
                    dODM.EntryUid = parameters.UserName.Trim();

                    ahamOper.OutputDistributionMfgs.InsertOnSubmit(dODM);
                    ahamOper.SubmitChanges();

                    outputDistributionManufacturer.OutputDistributionManufacturerKey = dODM.OutputDistributionMfgKey;
                    outputDistributionManufacturer.RowVersion = dODM.RowVersion;

                    outputDistributionManufacturer.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult OutputDistributionManufacturerUpdate(OutputDistributionManufacturer outputDistributionManufacturer, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.OutputDistributionMfg dODM = ahamOper.OutputDistributionMfgs.Single<AhamDataLinq.OutputDistributionMfg>(x => (x.OutputDistributionMfgKey == outputDistributionManufacturer.OutputDistributionManufacturerKey));

                    if (dODM.RowVersion != outputDistributionManufacturer.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + outputDistributionManufacturer.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dODM.mfgkey = outputDistributionManufacturer.ManufacturerKey;
                    dODM.OutputReportKey = outputDistributionManufacturer.OutputReportKey;

                    if (outputDistributionManufacturer.Disabled)
                        dODM.disabled = 1;
                    else
                        dODM.disabled = 0;

                    dODM.begindate = outputDistributionManufacturer.BeginDate.Date;
                    dODM.Enddate = outputDistributionManufacturer.EndDate.Date;

                    dODM.AppContext = parameters.ApplicationContext.Trim();
                    dODM.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    outputDistributionManufacturer.MarkOld();
                    outputDistributionManufacturer.RowVersion = dODM.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult OutputDistributionManufacturerDelete(OutputDistributionManufacturer outputDistributionManufacturer, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.OutputDistributionMfg dODM = ahamOper.OutputDistributionMfgs.Single<AhamDataLinq.OutputDistributionMfg>(x => (x.OutputDistributionMfgKey == outputDistributionManufacturer.OutputDistributionManufacturerKey));

                    if (dODM.RowVersion != outputDistributionManufacturer.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + outputDistributionManufacturer.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dODM.AppContext = parameters.ApplicationContext.Trim();
                    dODM.EntryUid = parameters.UserName.Trim();

                    ahamOper.OutputDistributionMfgs.DeleteOnSubmit(dODM);
                    ahamOper.SubmitChanges();

                    outputDistributionManufacturer.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        #endregion

        #region OutputDistributionUser

        internal static DataAccessResult OutputDistributionUserListGet(OutputDistributionUser outputDistributionUserToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<OutputDistributionUser> ODUs = new HaiBindingList<OutputDistributionUser>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<AhamDataLinq.OutputDistributionUser>(x => (x.DOutPutReport));
                    ahamOper.LoadOptions = dlo;

                    List<AhamDataLinq.OutputDistributionUser> dODUs = new List<AhamDataLinq.OutputDistributionUser>();
                    if (outputDistributionUserToGet == null)
                    {
                        dODUs = ahamOper.OutputDistributionUsers.ToList<AhamDataLinq.OutputDistributionUser>();
                    }
                    else
                    {
                        dODUs = new List<AhamDataLinq.OutputDistributionUser>();
                        dODUs.Add(ahamOper.OutputDistributionUsers.Single<AhamDataLinq.OutputDistributionUser>(x => (x.OutputDistributionKey == outputDistributionUserToGet.OutputDistributionUserKey)));
                    }

                    foreach (AhamDataLinq.OutputDistributionUser dODU in dODUs)
                    {
                        OutputDistributionUser ODU = new OutputDistributionUser();

                        ODU.OutputDistributionUserKey = dODU.OutputDistributionKey;
                        ODU.UserCode = dODU.Ntuser.Trim();
                        ODU.OutputReportKey = dODU.OutPutReportKey;
                        ODU.OutputReportID = dODU.DOutPutReport.OutputReportID.Trim();

                        if (dODU.Disabled == 0)
                            ODU.Disabled = false;
                        else
                            ODU.Disabled = true;

                        ODU.BeginDate = dODU.Begindate.Date;
                        ODU.EndDate = dODU.Enddate.Date;

                        ODU.ApplicationContext = dODU.AppContext.Trim();
                        ODU.UserName = dODU.EntryUid.Trim();
                        ODU.RowVersion = dODU.RowVersion;

                        ODU.MarkOld();
                        ODUs.AddToList(ODU);
                    }

                    result.DataList = ODUs;
                    result.Success = true;
                    result.BrowsablePropertyList = OutputDistributionUser.GetBrowsablePropertyList();

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult OutputDistributionUserGet(OutputDistributionUser outputDistributionUserToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = OutputDistributionUserListGet(outputDistributionUserToGet, parameters);
            if (result.Success)
            {
                OutputDistributionUser ODU = (OutputDistributionUser)result.SingleItem;
                result.ItemIsChanged = (outputDistributionUserToGet.RowVersion != ODU.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult OutputDistributionUserSave(OutputDistributionUser outputDistributionUser, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            if (outputDistributionUser.IsSavable)
            {
                if (outputDistributionUser.IsDeleted)
                    result = OutputDistributionUserDelete(outputDistributionUser, parameters);
                else if (outputDistributionUser.IsNew)
                    result = OutputDistributionUserInsert(outputDistributionUser, parameters);
                else if (outputDistributionUser.IsDirty)
                    result = OutputDistributionUserUpdate(outputDistributionUser, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult OutputDistributionUserInsert(OutputDistributionUser outputDistributionUser, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.OutputDistributionUser dODU = new AhamDataLinq.OutputDistributionUser();

                    dODU.Ntuser = outputDistributionUser.UserCode.Trim();
                    dODU.OutPutReportKey = outputDistributionUser.OutputReportKey;

                    if (outputDistributionUser.Disabled)
                        dODU.Disabled = 1;
                    else
                        dODU.Disabled = 0;

                    dODU.Begindate = outputDistributionUser.BeginDate.Date;
                    dODU.Enddate = outputDistributionUser.EndDate.Date;

                    dODU.AppContext = parameters.ApplicationContext.Trim();
                    dODU.EntryUid = parameters.UserName.Trim();

                    ahamOper.OutputDistributionUsers.InsertOnSubmit(dODU);
                    ahamOper.SubmitChanges();

                    outputDistributionUser.OutputDistributionUserKey = dODU.OutputDistributionKey; ;
                    outputDistributionUser.RowVersion = dODU.RowVersion;

                    outputDistributionUser.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult OutputDistributionUserUpdate(OutputDistributionUser outputDistributionUser, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.OutputDistributionUser dODU = ahamOper.OutputDistributionUsers.Single<AhamDataLinq.OutputDistributionUser>(x => (x.OutputDistributionKey == outputDistributionUser.OutputDistributionUserKey));

                    if (dODU.RowVersion != outputDistributionUser.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + outputDistributionUser.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dODU.OutPutReportKey = outputDistributionUser.OutputReportKey;

                    dODU.Ntuser = outputDistributionUser.UserCode.Trim();
                    if (outputDistributionUser.Disabled)
                        dODU.Disabled = 1;
                    else
                        dODU.Disabled = 0;

                    dODU.Begindate = outputDistributionUser.BeginDate.Date;
                    dODU.Enddate = outputDistributionUser.EndDate.Date;

                    dODU.AppContext = parameters.ApplicationContext.Trim();
                    dODU.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    outputDistributionUser.MarkOld();
                    outputDistributionUser.RowVersion = dODU.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult OutputDistributionUserDelete(OutputDistributionUser outputDistributionUser, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.OutputDistributionUser dODU = ahamOper.OutputDistributionUsers.Single<AhamDataLinq.OutputDistributionUser>(x => (x.OutputDistributionKey == outputDistributionUser.OutputDistributionUserKey));

                    if (dODU.RowVersion != outputDistributionUser.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + outputDistributionUser.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dODU.AppContext = parameters.ApplicationContext.Trim();
                    dODU.EntryUid = parameters.UserName.Trim();

                    ahamOper.OutputDistributionUsers.DeleteOnSubmit(dODU);
                    ahamOper.SubmitChanges();

                    outputDistributionUser.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        #endregion

        #region OutputReportPublic

        internal static DataAccessResult OutputReportPublicListGet(OutputReportPublic outputReportPublicToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<OutputReportPublic> ORPs = new HaiBindingList<OutputReportPublic>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<AhamDataLinq.OutputReportPublic>(x => (x.DOutPutReport));
                    ahamOper.LoadOptions = dlo;

                    List<AhamDataLinq.OutputReportPublic> dORPs = new List<AhamDataLinq.OutputReportPublic>();
                    if (outputReportPublicToGet == null)
                    {
                        dORPs = ahamOper.OutputReportPublics.ToList<AhamDataLinq.OutputReportPublic>();
                    }
                    else
                    {
                        dORPs = new List<AhamDataLinq.OutputReportPublic>();
                        dORPs.Add(ahamOper.OutputReportPublics.Single<AhamDataLinq.OutputReportPublic>(x => (x.OutputReportPublicKey == outputReportPublicToGet.OutputReportPublicKey)));
                    }

                    foreach (AhamDataLinq.OutputReportPublic dORP in dORPs)
                    {
                        OutputReportPublic ORP = new OutputReportPublic();

                        ORP.OutputReportPublicKey = dORP.OutputReportPublicKey;
                        ORP.OutputReportKey = dORP.outputreportkey;
                        ORP.OutputReportID = dORP.DOutPutReport.OutputReportID.Trim();
                        ORP.BeginDate = dORP.begindate.Date;
                        ORP.EndDate = dORP.enddate.Date;

                        ORP.ApplicationContext = dORP.AppContext.Trim();
                        ORP.UserName = dORP.EntryUid.Trim();
                        ORP.RowVersion = dORP.RowVersion;

                        ORP.MarkOld();
                        ORPs.AddToList(ORP);
                    }

                    result.DataList = ORPs;
                    result.Success = true;
                    result.BrowsablePropertyList = OutputReportPublic.GetBrowsablePropertyList();

                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult OutputReportPublicGet(OutputReportPublic outputReportPublicToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = OutputReportPublicListGet(outputReportPublicToGet, parameters);
            if (result.Success)
            {
                OutputReportPublic ORP = (OutputReportPublic)result.SingleItem;
                result.ItemIsChanged = (outputReportPublicToGet.RowVersion != ORP.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult OutputReportPublicSave(OutputReportPublic outputReportPublic, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            if (outputReportPublic.IsSavable)
            {
                if (outputReportPublic.IsDeleted)
                    result = OutputReportPublicDelete(outputReportPublic, parameters);
                else if (outputReportPublic.IsNew)
                    result = OutputReportPublicInsert(outputReportPublic, parameters);
                else if (outputReportPublic.IsDirty)
                    result = OutputReportPublicUpdate(outputReportPublic, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult OutputReportPublicInsert(OutputReportPublic outputReportPublic, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.OutputReportPublic dORP = new AhamDataLinq.OutputReportPublic();

                    dORP.outputreportkey = outputReportPublic.OutputReportKey;

                    dORP.begindate = outputReportPublic.BeginDate.Date;
                    dORP.enddate = outputReportPublic.EndDate.Date;

                    dORP.AppContext = parameters.ApplicationContext.Trim();
                    dORP.EntryUid = parameters.UserName.Trim();

                    ahamOper.OutputReportPublics.InsertOnSubmit(dORP);
                    ahamOper.SubmitChanges();

                    outputReportPublic.OutputReportPublicKey = dORP.OutputReportPublicKey;
                    outputReportPublic.RowVersion = dORP.RowVersion;

                    outputReportPublic.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult OutputReportPublicUpdate(OutputReportPublic outputReportPublic, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.OutputReportPublic dORP = ahamOper.OutputReportPublics.Single<AhamDataLinq.OutputReportPublic>(x => (x.OutputReportPublicKey == outputReportPublic.OutputReportPublicKey));

                    if (dORP.RowVersion != outputReportPublic.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + outputReportPublic.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dORP.outputreportkey = outputReportPublic.OutputReportKey;

                    dORP.begindate = outputReportPublic.BeginDate.Date;
                    dORP.enddate = outputReportPublic.EndDate.Date;

                    dORP.AppContext = parameters.ApplicationContext.Trim();
                    dORP.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    outputReportPublic.MarkOld();
                    outputReportPublic.RowVersion = dORP.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult OutputReportPublicDelete(OutputReportPublic outputReportPublic, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.OutputReportPublic dORP = ahamOper.OutputReportPublics.Single<AhamDataLinq.OutputReportPublic>(x => (x.OutputReportPublicKey == outputReportPublic.OutputReportPublicKey));

                    if (dORP.RowVersion != outputReportPublic.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + outputReportPublic.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dORP.AppContext = parameters.ApplicationContext.Trim();
                    dORP.EntryUid = parameters.UserName.Trim();

                    ahamOper.OutputReportPublics.DeleteOnSubmit(dORP);
                    ahamOper.SubmitChanges();

                    outputReportPublic.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }


        #endregion

        #region ReportCompareMaster

        internal static DataAccessResult ReportCompareMasterListGet(ReportCompareMaster reportCompareMasterToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ReportCompareMaster> RCMs = new HaiBindingList<ReportCompareMaster>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<AhamDataLinq.rptCompareMaster>(x => (x.DMfg));
                    dlo.LoadWith<AhamDataLinq.rptCompareMaster>(x => (x.RMfg));
                    dlo.LoadWith<AhamDataLinq.rptCompareMaster>(x => (x.RMfg1));
                    dlo.LoadWith<AhamDataLinq.rptCompareMaster>(x => (x.RMfg2));
                    dlo.LoadWith<AhamDataLinq.rptCompareMaster>(x => (x.RMfg3));
                    dlo.LoadWith<AhamDataLinq.rptCompareMaster>(x => (x.RMfg4));
                    ahamOper.LoadOptions = dlo;

                    List<AhamDataLinq.rptCompareMaster> dRCMs;
                    if (reportCompareMasterToGet == null)
                    {
                        dRCMs = ahamOper.rptCompareMasters.ToList<AhamDataLinq.rptCompareMaster>();
                    }
                    else
                    {
                        dRCMs = new List<rptCompareMaster>();
                        dRCMs.Add(ahamOper.rptCompareMasters.Single<AhamDataLinq.rptCompareMaster>(x => (x.rptcompKey == reportCompareMasterToGet.ReportCompareMasterKey)));
                    }

                    foreach (AhamDataLinq.rptCompareMaster dRCM in dRCMs)
                    {
                        ReportCompareMaster RCM = new ReportCompareMaster();

                        RCM.ReportCompareMasterKey = dRCM.rptcompKey;
                        RCM.ManufacturerKey = dRCM.MfgKey;
                        RCM.ManufacturerName = dRCM.DMfg.MfgName;
                        RCM.Report1Key = dRCM.mfgreportkey1;
                        RCM.Report1Name = dRCM.RMfg.RptName.Trim();
                        RCM.Report2Key = dRCM.mfgreportkey2;
                        RCM.Report2Name = dRCM.RMfg1.RptName.Trim();
                        RCM.Report3Key = dRCM.mfgreportkey3;
                        if (RCM.Report3Key.HasValue)
                            RCM.Report3Name = dRCM.RMfg2.RptName.Trim();
                        RCM.Report4Key = dRCM.mfgreportkey4;
                        if (RCM.Report4Key.HasValue)
                            RCM.Report4Name = dRCM.RMfg3.RptName.Trim();
                        RCM.Report5Key = dRCM.mfgreportkey5;
                        if (RCM.Report5Key.HasValue)
                            RCM.Report5Name = dRCM.RMfg4.RptName.Trim();
                        RCM.ErrorMessage = dRCM.ErrorMsg.Trim();
                        RCM.Relationship = dRCM.Relationship.Trim();
                        RCM.IsError = Utilities.ConvertByteToBool(dRCM.Error);
                        RCM.YTD = Utilities.ConvertByteToBool(dRCM.Ytd);
                        RCM.Reconcile = Utilities.ConvertByteToBool(dRCM.Rec);

                        RCM.ApplicationContext = dRCM.AppContext.Trim();
                        RCM.UserName = dRCM.EntryUid.Trim();
                        RCM.RowVersion = dRCM.RowVersion;

                        RCM.MarkOld();
                        RCMs.AddToList(RCM);
                    }

                    result.DataList = RCMs;
                    result.Success = true;
                    result.BrowsablePropertyList = ReportCompareMaster.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ReportCompareMasterGet(ReportCompareMaster reportCompareMasterToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ReportCompareMasterListGet(reportCompareMasterToGet, parameters);
            if (result.Success)
            {
                ReportCompareMaster RCM = (ReportCompareMaster)result.SingleItem;
                result.ItemIsChanged = (reportCompareMasterToGet.RowVersion != RCM.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ReportCompareMasterSave(ReportCompareMaster reportCompareMaster, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            if (reportCompareMaster.IsSavable)
            {
                if (reportCompareMaster.IsDeleted)
                    result = ReportCompareMasterDelete(reportCompareMaster, parameters);
                else if (reportCompareMaster.IsNew)
                    result = ReportCompareMasterInsert(reportCompareMaster, parameters);
                else if (reportCompareMaster.IsDirty)
                    result = ReportCompareMasterUpdate(reportCompareMaster, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ReportCompareMasterInsert(ReportCompareMaster reportCompareMaster, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.rptCompareMaster dRCM = new rptCompareMaster();

                    dRCM.MfgKey = reportCompareMaster.ManufacturerKey;
                    dRCM.mfgreportkey1 = reportCompareMaster.Report1Key;
                    dRCM.mfgreportkey2 = reportCompareMaster.Report2Key;
                    dRCM.mfgreportkey3 = reportCompareMaster.Report3Key;
                    dRCM.mfgreportkey4 = reportCompareMaster.Report4Key;
                    dRCM.mfgreportkey5 = reportCompareMaster.Report5Key;
                    dRCM.ErrorMsg = reportCompareMaster.ErrorMessage.Trim();
                    dRCM.Relationship = reportCompareMaster.Relationship.Trim();
                    dRCM.Error = Utilities.ConvertBoolToByte(reportCompareMaster.IsError);
                    dRCM.Ytd = Utilities.ConvertBoolToByte(reportCompareMaster.YTD);
                    dRCM.Rec = Utilities.ConvertBoolToByte(reportCompareMaster.Reconcile);

                    dRCM.AppContext = parameters.ApplicationContext.Trim();
                    dRCM.EntryUid = parameters.UserName.Trim();

                    ahamOper.rptCompareMasters.InsertOnSubmit(dRCM);
                    ahamOper.SubmitChanges();

                    reportCompareMaster.ReportCompareMasterKey = dRCM.rptcompKey;
                    reportCompareMaster.RowVersion = dRCM.RowVersion;

                    reportCompareMaster.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult ReportCompareMasterUpdate(ReportCompareMaster reportCompareMaster, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.rptCompareMaster dRCM = ahamOper.rptCompareMasters.Single<rptCompareMaster>(x => (x.rptcompKey == reportCompareMaster.ReportCompareMasterKey));

                    if (dRCM.RowVersion != reportCompareMaster.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + reportCompareMaster.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dRCM.MfgKey = reportCompareMaster.ManufacturerKey;
                    dRCM.mfgreportkey1 = reportCompareMaster.Report1Key;
                    dRCM.mfgreportkey2 = reportCompareMaster.Report2Key;
                    dRCM.mfgreportkey3 = reportCompareMaster.Report3Key;
                    dRCM.mfgreportkey4 = reportCompareMaster.Report4Key;
                    dRCM.mfgreportkey5 = reportCompareMaster.Report5Key;
                    dRCM.ErrorMsg = reportCompareMaster.ErrorMessage.Trim();
                    dRCM.Relationship = reportCompareMaster.Relationship.Trim();
                    dRCM.Error = Utilities.ConvertBoolToByte(reportCompareMaster.IsError);
                    dRCM.Ytd = Utilities.ConvertBoolToByte(reportCompareMaster.YTD);
                    dRCM.Rec = Utilities.ConvertBoolToByte(reportCompareMaster.Reconcile);

                    dRCM.AppContext = parameters.ApplicationContext.Trim();
                    dRCM.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    reportCompareMaster.RowVersion = dRCM.RowVersion;
                    reportCompareMaster.MarkOld();

                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult ReportCompareMasterDelete(ReportCompareMaster reportCompareMaster, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.rptCompareMaster dRCM = ahamOper.rptCompareMasters.Single<rptCompareMaster>(x => (x.rptcompKey == reportCompareMaster.ReportCompareMasterKey));

                    if (dRCM.RowVersion != reportCompareMaster.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + reportCompareMaster.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dRCM.AppContext = parameters.ApplicationContext.Trim();
                    dRCM.EntryUid = parameters.UserName.Trim();

                    ahamOper.rptCompareMasters.DeleteOnSubmit(dRCM);
                    ahamOper.SubmitChanges();

                    reportCompareMaster.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        #endregion

        #region ReportCompareDetail

        private static DataAccessResult ReportCompareDetailListGetGeneral(ReportCompareDetail reportCompareDetailToGet, DataServiceParameters parameters, int? reportCompareMasterKey)
        {
            // ensure that request for a specific object does not specify a particular ReportCompareMaster
            if (reportCompareDetailToGet != null && reportCompareMasterKey.HasValue)
            {
                throw new Exception("Cannot specify a ReportCompareMaster when retrieving a specific ReportCompareDetail");
            }

            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ReportCompareDetail> RCDs = new HaiBindingList<ReportCompareDetail>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<AhamDataLinq.RptCompareDetail>(x => (x.RMfg));
                    dlo.LoadWith<AhamDataLinq.RptCompareDetail>(x => (x.DSize));
                    dlo.LoadWith<AhamDataLinq.RptCompareDetail>(x => (x.DSize1));
                    dlo.LoadWith<AhamDataLinq.RptCompareDetail>(x => (x.DSize2));
                    dlo.LoadWith<AhamDataLinq.RptCompareDetail>(x => (x.DSize3));
                    dlo.LoadWith<AhamDataLinq.RptCompareDetail>(x => (x.DSize4));
                    dlo.LoadWith<AhamDataLinq.RptCompareDetail>(x => (x.DActivity));
                    dlo.LoadWith<AhamDataLinq.RptCompareDetail>(x => (x.DCatalog));
                    dlo.LoadWith<AhamDataLinq.RptCompareDetail>(x => (x.DMeasure));
                    dlo.LoadWith<AhamDataLinq.RptCompareDetail>(x => (x.DChannel));
                    dlo.LoadWith<AhamDataLinq.RptCompareDetail>(x => (x.DUse));
                    ahamOper.LoadOptions=dlo;

                    List<AhamDataLinq.RptCompareDetail> dRCDs;
                    if (!reportCompareMasterKey.HasValue)
                    {
                        if (reportCompareDetailToGet == null)
                        {
                            dRCDs = ahamOper.RptCompareDetails.ToList<AhamDataLinq.RptCompareDetail>();
                        }
                        else
                        {
                            dRCDs = new List<RptCompareDetail>();
                            dRCDs.Add(ahamOper.RptCompareDetails.Single<AhamDataLinq.RptCompareDetail>(x => (x.rptDetailKey == reportCompareDetailToGet.ReportCompareDetailKey)));
                        }
                    }
                    else
                    {
                        dRCDs = ahamOper.RptCompareDetails
                            .Where<RptCompareDetail>(y => (y.RptCompKey == reportCompareMasterKey.Value)).ToList<AhamDataLinq.RptCompareDetail>();
                    }

                    foreach (AhamDataLinq.RptCompareDetail dRCD in dRCDs)
                    {
                        ReportCompareDetail RCD = new ReportCompareDetail();

                        RCD.ReportCompareDetailKey = dRCD.rptDetailKey;
                        RCD.ReportCompareMasterKey = dRCD.RptCompKey;
                        RCD.ManufacturerReportKey = dRCD.mfgreportkey;
                        RCD.ManufacturerReportName = dRCD.RMfg.RptName.Trim();
                        RCD.Size1Key = dRCD.size1key;
                        if (RCD.Size1Key.HasValue)
                            RCD.Size1Name = "[" + dRCD.DSize.DDim.DimName.Trim() + "].[" + dRCD.DSize.SizeRowLabel.Trim() + "]";
                        RCD.Size2Key = dRCD.size2key;
                        if (RCD.Size2Key.HasValue)
                            RCD.Size2Name = "[" + dRCD.DSize1.DDim.DimName.Trim() + "].[" + dRCD.DSize1.SizeRowLabel.Trim() + "]";
                        RCD.Size3Key = dRCD.size3key;
                        if (RCD.Size3Key.HasValue)
                            RCD.Size3Name = "[" + dRCD.DSize2.DDim.DimName.Trim() + "].[" + dRCD.DSize2.SizeRowLabel.Trim() + "]";
                        RCD.Size4Key = dRCD.size4key;
                        if (RCD.Size4Key.HasValue)
                            RCD.Size4Name = "[" + dRCD.DSize3.DDim.DimName.Trim() + "].[" + dRCD.DSize3.SizeRowLabel.Trim() + "]";
                        RCD.Size5Key = dRCD.size5key;
                        if (RCD.Size5Key.HasValue)
                            RCD.Size5Name = "[" + dRCD.DSize4.DDim.DimName.Trim() + "].[" + dRCD.DSize4.SizeRowLabel.Trim() + "]";
                        RCD.ActivityKey = dRCD.actkey;
                        if (RCD.ActivityKey.HasValue)
                            RCD.ActivityName = dRCD.DActivity.ActivityName.Trim();
                        RCD.CatalogKey = dRCD.catalogkey;
                        if (RCD.CatalogKey.HasValue)
                            RCD.CatalogName = dRCD.DCatalog.CatalogName.Trim();
                        RCD.MeasureKey = dRCD.measurekey;
                        if (RCD.MeasureKey.HasValue)
                            RCD.MeasureName = dRCD.DMeasure.MeasureName.Trim();
                        RCD.ChannelKey = dRCD.channelkey;
                        if (RCD.ChannelKey.HasValue)
                            RCD.ChannelName = dRCD.DChannel.ChannelName.Trim();
                        RCD.UseKey = dRCD.usekey;
                        if (RCD.UseKey.HasValue)
                            RCD.UseName = dRCD.DUse.UseName.Trim();
                        RCD.IsCalendar = Utilities.ConvertByteToBool(dRCD.calendar);
                        if (dRCD.side == 0)
                            RCD.Side = 0;
                        else
                            RCD.Side = 1;

                        RCD.ApplicationContext = dRCD.AppContext.Trim();
                        RCD.UserName = dRCD.EntryUid.Trim();
                        RCD.RowVersion = dRCD.RowVersion;

                        RCD.MarkOld();
                        RCDs.AddToList(RCD);
                    }

                    result.DataList = RCDs;
                    result.Success = true;
                    result.BrowsablePropertyList = ReportCompareDetail.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ReportCompareDetailListGet(ReportCompareDetail reportCompareDetailToGet, DataServiceParameters parameters)
        {
            DataAccessResult result;

            result = ReportCompareDetailListGetGeneral(reportCompareDetailToGet, parameters, null);

            return result;
        }

        public static DataAccessResult ReportCompareDetailForMasterListGet(int reportCompareMasterKey, DataServiceParameters parameters)
        {
            DataAccessResult result;

            result = ReportCompareDetailListGetGeneral(null, parameters, reportCompareMasterKey);

            return result;
        }

        internal static DataAccessResult ReportCompareDetailGet(ReportCompareDetail reportCompareDetailToGet, DataServiceParameters parameters)
        {
            DataAccessResult result;

            result = ReportCompareDetailListGetGeneral(reportCompareDetailToGet, parameters, null);

            return result;
        }

        internal static DataAccessResult ReportCompareDetailSave(ReportCompareDetail reportCompareDetail, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            if (reportCompareDetail.IsSavable)
            {
                if (reportCompareDetail.IsDeleted)
                    result = ReportCompareDetailDelete(reportCompareDetail, parameters);
                else if (reportCompareDetail.IsNew)
                    result = ReportCompareDetailInsert(reportCompareDetail, parameters);
                else if (reportCompareDetail.IsDirty)
                    result = ReportCompareDetailUpdate(reportCompareDetail, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ReportCompareDetailInsert(ReportCompareDetail reportCompareDetail, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.RptCompareDetail dRCD = new RptCompareDetail();

                    dRCD.RptCompKey = reportCompareDetail.ReportCompareMasterKey;
                    dRCD.mfgreportkey = reportCompareDetail.ManufacturerReportKey;
                    dRCD.size1key = reportCompareDetail.Size1Key;
                    dRCD.size2key = reportCompareDetail.Size2Key;
                    dRCD.size3key = reportCompareDetail.Size3Key;
                    dRCD.size4key = reportCompareDetail.Size4Key;
                    dRCD.size5key = reportCompareDetail.Size5Key;
                    dRCD.actkey = reportCompareDetail.ActivityKey;
                    dRCD.catalogkey = reportCompareDetail.CatalogKey;
                    dRCD.measurekey = reportCompareDetail.MeasureKey;
                    dRCD.channelkey = reportCompareDetail.ChannelKey;
                    dRCD.usekey = reportCompareDetail.UseKey;
                    dRCD.calendar = Utilities.ConvertBoolToByte(reportCompareDetail.IsCalendar);
                    dRCD.side = (byte)reportCompareDetail.Side;

                    dRCD.AppContext = parameters.ApplicationContext.Trim();
                    dRCD.EntryUid = parameters.UserName.Trim();

                    ahamOper.RptCompareDetails.InsertOnSubmit(dRCD);
                    ahamOper.SubmitChanges();

                    reportCompareDetail.ReportCompareDetailKey = dRCD.rptDetailKey;
                    reportCompareDetail.RowVersion = dRCD.RowVersion;

                    reportCompareDetail.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult ReportCompareDetailUpdate(ReportCompareDetail reportCompareDetail, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.RptCompareDetail dRCD = ahamOper.RptCompareDetails.Single<RptCompareDetail>(x => (x.rptDetailKey == reportCompareDetail.ReportCompareDetailKey));

                    if (dRCD.RowVersion != reportCompareDetail.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + reportCompareDetail.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dRCD.RptCompKey = reportCompareDetail.ReportCompareMasterKey;
                    dRCD.mfgreportkey = reportCompareDetail.ManufacturerReportKey;
                    dRCD.size1key = reportCompareDetail.Size1Key;
                    dRCD.size2key = reportCompareDetail.Size2Key;
                    dRCD.size3key = reportCompareDetail.Size3Key;
                    dRCD.size4key = reportCompareDetail.Size4Key;
                    dRCD.size5key = reportCompareDetail.Size5Key;
                    dRCD.actkey = reportCompareDetail.ActivityKey;
                    dRCD.catalogkey = reportCompareDetail.CatalogKey;
                    dRCD.measurekey = reportCompareDetail.MeasureKey;
                    dRCD.channelkey = reportCompareDetail.ChannelKey;
                    dRCD.usekey = reportCompareDetail.UseKey;
                    dRCD.calendar = Utilities.ConvertBoolToByte(reportCompareDetail.IsCalendar);
                    dRCD.side = (byte)reportCompareDetail.Side;

                    dRCD.AppContext = parameters.ApplicationContext.Trim();
                    dRCD.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    reportCompareDetail.RowVersion = dRCD.RowVersion;
                    reportCompareDetail.MarkOld();

                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;

        }

        private static DataAccessResult ReportCompareDetailDelete(ReportCompareDetail reportCompareDetail, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    AhamDataLinq.RptCompareDetail dRCD = ahamOper.RptCompareDetails.Single<RptCompareDetail>(x => (x.rptDetailKey == reportCompareDetail.ReportCompareDetailKey));

                    if (dRCD.RowVersion != reportCompareDetail.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + reportCompareDetail.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dRCD.AppContext = parameters.ApplicationContext.Trim();
                    dRCD.EntryUid = parameters.UserName.Trim();

                    ahamOper.RptCompareDetails.DeleteOnSubmit(dRCD);
                    ahamOper.SubmitChanges();

                    reportCompareDetail.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion
    }
}
