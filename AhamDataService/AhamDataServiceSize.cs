﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    public class AhamDataServiceSize
    {

        #region Size

        internal static DataAccessResult SizeListGet(Size sizeToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Size> sizes = new HaiBindingList<Size>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<DSize>(x => x.DDim);
                    ahamOper.LoadOptions = dlo;

                    List<DSize> dSizes;
                    if (sizeToGet == null)
                        dSizes = ahamOper.DSizes.ToList<DSize>();
                    else
                    {
                        dSizes = new List<DSize>();
                        dSizes.Add(ahamOper.DSizes.Single<DSize>(x => (x.SizeKey == sizeToGet.SizeKey)));
                    }

                    foreach (DSize dSize in dSizes)
                    {
                        Size size = new Size();

                        size.SizeKey = dSize.SizeKey;
                        size.DimensionKey = dSize.DDim.DimKey;
                        size.DimensionName = dSize.DDim.DimName.Trim();
                        size.SizeLowerLimit = dSize.SizeLowerLimit;
                        size.SizeUpperLimit = dSize.SizeUpperLimit;
                        size.SizeLabel1 = dSize.SizeLabel1.Trim();
                        if (dSize.SizeLabel2 != null)
                            size.SizeLabel2 = dSize.SizeLabel2.Trim();
                        if (dSize.SizeLabel3 != null)
                            size.SizeLabel3 = dSize.SizeLabel3.Trim();
                        if (dSize.SizeLabel4 != null)
                            size.SizeLabel4 = dSize.SizeLabel4.Trim();
                        if (dSize.SizeRowLabel != null)
                            size.SizeRowLabel = dSize.SizeRowLabel.Trim();
                        if (dSize.SizeColumnLabel != null)
                            size.SizeColumnLabel = dSize.SizeColumnLabel.Trim();

                        size.SizeName = "[" + dSize.DDim.DimName.Trim() + "].[" + dSize.SizeRowLabel.Trim() + "]";

                        size.ApplicationContext = dSize.AppContext.Trim();
                        size.UserName = dSize.EntryUid.Trim();
                        size.RowVersion = dSize.RowVersion;

                        size.MarkOld();
                        sizes.AddToList(size);
                    }

                    result.DataList = sizes;
                    result.Success = true;
                    result.BrowsablePropertyList = Size.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult SizeGet(Size sizeToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = SizeListGet(sizeToGet, parameters);
            if (result.Success)
            {
                Size size = (Size)result.SingleItem;
                result.ItemIsChanged = (sizeToGet.RowVersion != size.RowVersion);
            }

            return result;
        }

        #endregion

        #region SizeSet

        #region Methods that are to be called from AhamDataService
        internal static DataAccessResult SizeSetListGet(SizeSet sizeSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<SizeSet> sizeSetList = new HaiBindingList<SizeSet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<AhamDataLinq.SDSize>(x => (x.DMarket));
                    ahamOper.LoadOptions = dlo;

                    List<SDSize> dSizeSets;
                    if (sizeSetToGet == null)
                        dSizeSets = ahamOper.SDSizes.ToList<SDSize>();
                    else
                    {
                        dSizeSets = new List<SDSize>();
                        dSizeSets.Add(ahamOper.SDSizes.Single<SDSize>(x => (x.SizeSetKey == sizeSetToGet.SizeSetKey)));
                    }
                    foreach (SDSize dSizeSet in dSizeSets)
                    {
                        SizeSet sizeSet = new SizeSet();

                        sizeSet.SizeSetKey = dSizeSet.SizeSetKey;
                        sizeSet.MarketKey = dSizeSet.MarketKey;
                        sizeSet.MarketName = dSizeSet.DMarket.MarketName.Trim();

                        // N.B. SizeSetName is contructed in this block.  The constructed name must match the name
                        //... that is constructed by SP ceSdSizeLookup() that is called in SizeSetFilteredListGet().
                        string sizeSetName = dSizeSet.SizeSetName.Trim();
                        string sizeSetName2;
                        if (dSizeSet.SizeSetName2 == null)
                            sizeSetName2 = string.Empty;
                        else
                            sizeSetName2 = dSizeSet.SizeSetName2.Trim();
                        string marketName = sizeSet.MarketName;
                        sizeSet.SizeSetName = sizeSetName + " " + sizeSetName2 + " " + marketName;

                        sizeSet.RowVersion = dSizeSet.RowVersion;
                        sizeSet.ApplicationContext = dSizeSet.AppContext.Trim();
                        sizeSet.UserName = dSizeSet.EntryUid.Trim();

                        sizeSet.MarkOld();
                        sizeSetList.Add(sizeSet);
                    }

                    result.DataList = sizeSetList;
                    result.Success = true;
                    result.BrowsablePropertyList = SizeSet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult SizeSetGet(SizeSet sizeSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = SizeSetListGet(sizeSetToGet, parameters);
            if (result.Success)
            {
                SizeSet sizeSet = (SizeSet)result.SingleItem;
                result.ItemIsChanged = (sizeSetToGet.RowVersion != sizeSet.RowVersion);
            }

            return result;
        }
        #endregion

        #region Support methods that can be called directly from AhamDataServiceSize

        public static DataAccessResult SizeSetFilteredListGet(int productKey, int marketSetKey, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<SizeSet> sizeSetList = new HaiBindingList<SizeSet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    var sizeSetResult = ahamOper.ceSdSizeLookup(productKey, marketSetKey, 0);
                    foreach (ceSdSizeLookupResult sizeSetRs in sizeSetResult)
                    {
                        SizeSet sizeSet = new SizeSet();

                        sizeSet.SizeSetKey = sizeSetRs.sizesetkey;
                        sizeSet.SizeSetName = sizeSetRs.Sizesetname.Trim();

                        sizeSet.RowVersion = sizeSetRs.RowVersion;
                        sizeSet.ApplicationContext = sizeSetRs.AppContext.Trim();
                        sizeSet.UserName = sizeSetRs.EntryUid.Trim();

                        sizeSet.MarkOld();
                        sizeSetList.AddToList(sizeSet);
                    }

                    sizeSetResult.Dispose();

                    result.DataList = sizeSetList;
                    result.Success = true;
                    result.BrowsablePropertyList = SizeSet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        public static DataAccessResult SizeLookupManufacturerReport(int manufacturerReportKey, int sizeCategory, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Size> sizeList = new HaiBindingList<Size>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    var sizeResult = ahamOper.ceRmfgSizeLookup(manufacturerReportKey, sizeCategory);
                    foreach (ceRmfgSizeLookupResult sizeRs in sizeResult)
                    {
                        Size size = new Size();

                        size.SizeKey = sizeRs.sizekey;
                        size.SizeName = sizeRs.SizeName.Trim();
                        size.SizeRowLabel = sizeRs.SizeRowLabel.Trim();
                        size.DimensionName = sizeRs.DimName.Trim();

                        size.MarkOld();
                        sizeList.AddToList(size);
                    }

                    sizeResult.Dispose();

                    result.DataList = sizeList;
                    result.Success = true;
                    result.BrowsablePropertyList = Size.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }
        #endregion

        #endregion

        #region SizeSetMember
        #endregion


        #region ModelSizeSet

        internal static DataAccessResult ModelSizeSetListGet(ModelSizeSet modelSizeSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ModelSizeSet> modelSizeSetList = new HaiBindingList<ModelSizeSet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<SDPSize>(x => x.DMarket);
                    dlo.LoadWith<SDPSize>(x => x.DProductDim);
                    ahamOper.LoadOptions = dlo;

                    List<SDPSize> SDPSizes;
                    if (modelSizeSetToGet == null)
                        SDPSizes = ahamOper.SDPSizes.ToList<SDPSize>();
                    else
                    {
                        SDPSizes = new List<SDPSize>();
                        SDPSizes.Add(ahamOper.SDPSizes.Single<SDPSize>(x => (x.SizeSetPKey == modelSizeSetToGet.ModelSizeSetKey)));
                    }

                    foreach (SDPSize sdpSize in SDPSizes)
                    {
                        ModelSizeSet modelSizeSet = new ModelSizeSet();

                        modelSizeSet.ModelSizeSetKey = sdpSize.SizeSetPKey;
                        modelSizeSet.ModelSizeSetName = sdpSize.SizeSetName.Trim();
                        modelSizeSet.ProductDimensionKey = sdpSize.DProductDim.ProductDimKey;
                        modelSizeSet.ProductDimensionName = sdpSize.DProductDim.ProductDimName.Trim();
                        modelSizeSet.MarketKey = sdpSize.MarketKey;
                        modelSizeSet.MarketName = sdpSize.DMarket.MarketName.Trim();
                        modelSizeSet.DimKey = sdpSize.DProductDim.DimKey;

                        modelSizeSet.ApplicationContext = sdpSize.AppContext.Trim();
                        modelSizeSet.RowVersion = sdpSize.RowVersion;
                        modelSizeSet.UserName = sdpSize.EntryUid.Trim();

                        modelSizeSet.MarkOld();
                        modelSizeSetList.AddToList(modelSizeSet);
                    }

                    result.DataList = modelSizeSetList;
                    result.Success = true;
                    result.BrowsablePropertyList = ModelSizeSet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ModelSizeSetGet(ModelSizeSet modelSizeSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ModelSizeSetListGet(modelSizeSetToGet, parameters);
            if (result.Success)
            {
                ModelSizeSet modelSizeSet = (ModelSizeSet)result.SingleItem;
                result.ItemIsChanged = (modelSizeSetToGet.RowVersion != modelSizeSet.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ModelSizeSetSave(ModelSizeSet modelSizeSet, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (modelSizeSet.IsSavable)
            {
                if (modelSizeSet.IsDeleted)
                    result = ModelSizeSetDelete(modelSizeSet, parameters);
                else if (modelSizeSet.IsNew)
                    result = ModelSizeSetInsert(modelSizeSet, parameters);
                else if (modelSizeSet.IsDirty)
                    result = ModelSizeSetUpdate(modelSizeSet, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ModelSizeSetInsert(ModelSizeSet modelSizeSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDPSize sdpSize = new SDPSize();

                    sdpSize.ProductDimKey = modelSizeSet.ProductDimensionKey;
                    sdpSize.MarketKey = modelSizeSet.MarketKey;
                    sdpSize.SizeSetName = modelSizeSet.ModelSizeSetName.Trim();

                    sdpSize.EntryUid = parameters.UserName.Trim();
                    sdpSize.AppContext = parameters.ApplicationContext.Trim();

                    ahamOper.GetTable<SDPSize>().InsertOnSubmit(sdpSize);
                    ahamOper.SubmitChanges();

                    modelSizeSet.ModelSizeSetKey = sdpSize.SizeSetPKey;
                    modelSizeSet.RowVersion = sdpSize.RowVersion;

                    modelSizeSet.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ModelSizeSetUpdate(ModelSizeSet modelSizeSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDPSize sdpSize = ahamOper.SDPSizes.Single<SDPSize>(x => (x.SizeSetPKey == modelSizeSet.ModelSizeSetKey));

                    if (sdpSize.RowVersion != modelSizeSet.RowVersion)
                    {
                        result.Message = "Concurrency error: \"" + modelSizeSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdpSize.ProductDimKey = modelSizeSet.ProductDimensionKey;
                    sdpSize.MarketKey = modelSizeSet.MarketKey;
                    sdpSize.SizeSetName = modelSizeSet.ModelSizeSetName.Trim();

                    sdpSize.EntryUid = parameters.UserName.Trim();
                    sdpSize.AppContext = parameters.ApplicationContext.Trim();

                    ahamOper.SubmitChanges();

                    modelSizeSet.MarkOld();
                    modelSizeSet.RowVersion = sdpSize.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ModelSizeSetDelete(ModelSizeSet modelSizeSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDPSize sdpSize = ahamOper.SDPSizes.Single<SDPSize>(x => (x.SizeSetPKey == modelSizeSet.ModelSizeSetKey));

                    if (sdpSize.RowVersion != modelSizeSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + modelSizeSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sdpSize.AppContext = parameters.ApplicationContext.Trim();
                    sdpSize.EntryUid = parameters.UserName.Trim();

                    ahamOper.SDPSizes.DeleteOnSubmit(sdpSize);
                    ahamOper.SubmitChanges();

                    modelSizeSet.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region ModelSizeSetMember

        internal static DataAccessResult ModelSizeSetMemberListGet(ModelSizeSetMember modelSizeSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ModelSizeSetMember> modelSizeSetMembers = new HaiBindingList<ModelSizeSetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMPSize> dSMPSizes;
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<SMPSize>(x => x.DSize);
                    dlo.LoadWith<SMPSize>(x => x.SDPSize);
                    ahamOper.LoadOptions = dlo;

                    // get the table data and convert to a List
                    dSMPSizes = ahamOper.SMPSizes.ToList<SMPSize>();

                    // create list of ModelSizeSetMembers from the table, filling in lookup information
                    foreach (SMPSize dSMPSize in dSMPSizes)
                    {
                        ModelSizeSetMember modelSizeSetMember = new ModelSizeSetMember();

                        modelSizeSetMember.ModelSizeSetMemberKey = dSMPSize.SizeSetMemberPKey;
                        modelSizeSetMember.ModelSizeSetMemberName = "[" + dSMPSize.SDPSize.SizeSetName.Trim() + "].[" + dSMPSize.DSize.SizeRowLabel.Trim() + "].[" + "SizeLevel" + dSMPSize.SizeLevel.ToString()
                            + "] -- " + modelSizeSetMember.DateRange.ToString();
                        modelSizeSetMember.ModelSizeSetKey = dSMPSize.SizeSetPKey;
                        modelSizeSetMember.ModelSizeSetName = dSMPSize.SDPSize.SizeSetName.Trim();
                        modelSizeSetMember.SizeKey = dSMPSize.SizeKey;
                        // CAUTION: the SizeName is a name contructed to match the UniqueName for the Size
                        // b/c there is no DSize.SizeName defined.
                        modelSizeSetMember.SizeName = "[" + dSMPSize.DSize.DDim.DimName.Trim() + "].[" + dSMPSize.DSize.SizeRowLabel.Trim() + "]";
                        modelSizeSetMember.SizeLevel = dSMPSize.SizeLevel;
                        modelSizeSetMember.BeginDate = dSMPSize.Begindate.Date;
                        modelSizeSetMember.EndDate = dSMPSize.EndDate.Date;
                        modelSizeSetMember.ParentSizeKey = dSMPSize.ParentSizeKey;

                        modelSizeSetMember.ApplicationContext = dSMPSize.AppContext.Trim();
                        modelSizeSetMember.RowVersion = dSMPSize.RowVersion;
                        modelSizeSetMember.UserName = dSMPSize.EntryUid.Trim();

                        modelSizeSetMember.MarkOld();
                        modelSizeSetMembers.AddToList(modelSizeSetMember);
                    }

                    // create a Dictionary from the List so that individual items can be looked up
                    Dictionary<int, ModelSizeSetMember> modelSizeSetMemberDict = modelSizeSetMembers.ToDictionary<ModelSizeSetMember, int>(x => (x.ModelSizeSetMemberKey));

                    // for each ModelSizeSetMember that has a (non-null) parent, look up the parent member name
                    foreach (ModelSizeSetMember mssm in modelSizeSetMembers)
                    {
                        if (mssm.ParentSizeKey.HasValue)
                            mssm.ParentSizeName = modelSizeSetMemberDict[mssm.ModelSizeSetMemberKey].ModelSizeSetMemberName;
                    }
                    // ModelSizeSetMember data is now complete

                    
                    if (modelSizeSetMemberToGet == null)
                    {
                        result.DataList = modelSizeSetMembers;  // return the full list
                    }
                    else
                    {
                        HaiBindingList<ModelSizeSetMember> singeModelSizeSetMember = new HaiBindingList<ModelSizeSetMember>();
                        singeModelSizeSetMember.AddToList(modelSizeSetMemberDict[modelSizeSetMemberToGet.ModelSizeSetMemberKey]);
                        result.DataList = singeModelSizeSetMember;  // return a list that contains only the requested member
                    }

                    result.Success = true;
                    result.BrowsablePropertyList = ModelSizeSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ModelSizeSetMemberGet(ModelSizeSetMember modelSizeSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = ModelSizeSetMemberListGet(modelSizeSetMemberToGet, parameters);
            if (result.Success)
            {
                ModelSizeSetMember modelSizeSetMember = (ModelSizeSetMember)result.SingleItem;
                result.ItemIsChanged = (modelSizeSetMemberToGet.RowVersion != modelSizeSetMember.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult ModelSizeSetMemberSave(ModelSizeSetMember modelSizeSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (modelSizeSetMember.IsSavable)
            {
                if (modelSizeSetMember.IsDeleted)
                    result=ModelSizeSetMemberDelete(modelSizeSetMember, parameters);
                else if (modelSizeSetMember.IsNew)
                    result = ModelSizeSetMemberInsert(modelSizeSetMember, parameters);
                else if (modelSizeSetMember.IsDirty)
                    result = ModelSizeSetMemberUpdate(modelSizeSetMember, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult ModelSizeSetMemberInsert(ModelSizeSetMember modelSizeSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMPSize dSMPSize = new SMPSize();

                    dSMPSize.SizeSetPKey = modelSizeSetMember.ModelSizeSetKey;
                    dSMPSize.SizeKey = modelSizeSetMember.SizeKey;
                    dSMPSize.SizeLevel = (short)modelSizeSetMember.SizeLevel;
                    dSMPSize.Begindate = modelSizeSetMember.BeginDate.Date;
                    dSMPSize.EndDate = modelSizeSetMember.EndDate.Date;
                    dSMPSize.ParentSizeKey = modelSizeSetMember.ParentSizeKey;

                    dSMPSize.AppContext = parameters.ApplicationContext.Trim();
                    dSMPSize.EntryUid = parameters.UserName.Trim();

                    ahamOper.SMPSizes.InsertOnSubmit(dSMPSize);
                    ahamOper.SubmitChanges();

                    modelSizeSetMember.ModelSizeSetMemberKey = dSMPSize.SizeSetMemberPKey;
                    modelSizeSetMember.RowVersion = dSMPSize.RowVersion;

                    modelSizeSetMember.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ModelSizeSetMemberUpdate(ModelSizeSetMember modelSizeSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMPSize dSMPSize = ahamOper.SMPSizes.Single<SMPSize>(x => (x.SizeSetMemberPKey == modelSizeSetMember.ModelSizeSetMemberKey));

                    if (dSMPSize.RowVersion != modelSizeSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + modelSizeSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dSMPSize.SizeSetPKey = modelSizeSetMember.ModelSizeSetKey;
                    dSMPSize.SizeKey = modelSizeSetMember.SizeKey;
                    dSMPSize.SizeLevel = (short)modelSizeSetMember.SizeLevel;
                    dSMPSize.Begindate = modelSizeSetMember.BeginDate.Date;
                    dSMPSize.EndDate = modelSizeSetMember.EndDate.Date;
                    dSMPSize.ParentSizeKey = modelSizeSetMember.ParentSizeKey;

                    dSMPSize.AppContext = parameters.ApplicationContext.Trim();
                    dSMPSize.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    modelSizeSetMember.MarkOld();
                    modelSizeSetMember.RowVersion = dSMPSize.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult ModelSizeSetMemberDelete(ModelSizeSetMember modelSizeSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMPSize dSMPSize = ahamOper.SMPSizes.Single<SMPSize>(x => (x.SizeSetMemberPKey == modelSizeSetMember.ModelSizeSetMemberKey));

                    if (dSMPSize.RowVersion != modelSizeSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + modelSizeSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    dSMPSize.AppContext = parameters.ApplicationContext.Trim();
                    dSMPSize.EntryUid = parameters.UserName.Trim();

                    ahamOper.SMPSizes.DeleteOnSubmit(dSMPSize);
                    ahamOper.SubmitChanges();

                    modelSizeSetMember.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult ModelSizeSetMembersDuplicate(ModelSizeSet sourceModelSizeSet, ModelSizeSet targetModelSizeSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMPSize> dSMPSizes;
                    List<SMPSize> duplicatedItems = new List<SMPSize>();

                    dSMPSizes = ahamOper.SMPSizes.Where(x => (x.SizeSetPKey == sourceModelSizeSet.ModelSizeSetKey)).ToList<SMPSize>();

                    foreach (SMPSize dSMPSize in dSMPSizes)
                    {
                        SMPSize duplicateSMPSize = new SMPSize();

                        duplicateSMPSize.SizeSetPKey = targetModelSizeSet.ModelSizeSetKey;
                        duplicateSMPSize.SizeKey = dSMPSize.SizeKey;
                        duplicateSMPSize.SizeLevel = dSMPSize.SizeLevel;
                        duplicateSMPSize.Begindate = dSMPSize.Begindate.Date;
                        duplicateSMPSize.EndDate = dSMPSize.EndDate.Date;
                        duplicateSMPSize.ParentSizeKey = dSMPSize.ParentSizeKey;

                        duplicatedItems.Add(duplicateSMPSize);
                    }

                    ahamOper.SMPSizes.InsertAllOnSubmit<SMPSize>(duplicatedItems);
                    ahamOper.SubmitChanges();

                    result.Success = true;
                    result.BrowsablePropertyList = ModelSizeSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion
    }
}
