﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

using AhamDataLinq;
using HaiBusinessObject;
using HaiMetaDataDAL;

namespace AhamMetaDataDAL
{
    internal static class AhamDataServiceUse
    {

        #region Use

        internal static DataAccessResult UseListGet(Use useToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<Use> uses = new HaiBindingList<Use>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<DUse> DUses;
                    if (useToGet == null)
                        DUses = ahamOper.DUses.ToList<DUse>();
                    else
                    {
                        DUses = new List<DUse>();
                        DUses.Add(ahamOper.DUses.Single<DUse>(x => (x.UseKey == useToGet.UseKey)));
                    }

                    foreach (DUse duse in DUses)
                    {
                        Use use = new Use();

                        use.UseAbbreviation = duse.UseAbbrev.Trim();
                        use.UseKey = duse.UseKey;
                        use.UseName = duse.UseName.Trim();

                        use.ApplicationContext = duse.AppContext.Trim();
                        use.RowVersion = duse.RowVersion;
                        use.UserName = duse.EntryUid.Trim();

                        use.MarkOld();
                        uses.AddToList(use);
                    }

                    result.DataList = uses;
                    result.Success = true;
                    result.BrowsablePropertyList = Use.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult UseGet(Use useToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = UseListGet(useToGet, parameters);
            if (result.Success)
            {
                Use use = (Use)result.SingleItem;
                result.ItemIsChanged = (useToGet.RowVersion != use.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult UseSave(Use use, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (use.IsSavable)
            {
                if (use.IsDeleted)
                    result = UseDelete(use, parameters);
                else if (use.IsNew)
                    result = UseInsert(use, parameters);
                else if (use.IsDirty)
                    result = UseUpdate(use, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult UseInsert(Use use, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DUse duse = new DUse();
                    duse.UseAbbrev = use.UseAbbreviation.Trim();
                    duse.UseName = use.UseName.Trim();

                    duse.AppContext = parameters.ApplicationContext.Trim();
                    duse.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<DUse>().InsertOnSubmit(duse);
                    ahamOper.SubmitChanges();

                    use.UseKey = duse.UseKey;
                    use.RowVersion = duse.RowVersion;

                    use.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult UseUpdate(Use use, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DUse duse = ahamOper.DUses.Single<DUse>(x => (x.UseKey == use.UseKey));

                    if (duse.RowVersion != use.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + use.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    duse.UseAbbrev = use.UseAbbreviation.Trim();
                    duse.UseName = use.UseName.Trim();

                    duse.AppContext = parameters.ApplicationContext.Trim();
                    duse.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    use.MarkOld();
                    use.RowVersion = duse.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult UseDelete(Use use, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    DUse duse = ahamOper.DUses.Single<DUse>(x => (x.UseKey == use.UseKey));

                    if (duse.RowVersion != use.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + use.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    duse.AppContext = parameters.ApplicationContext.Trim();
                    duse.EntryUid = parameters.UserName.Trim();

                    ahamOper.DUses.DeleteOnSubmit(duse);
                    ahamOper.SubmitChanges();

                    use.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region UseSet

        internal static DataAccessResult UseSetListGet(UseSet useSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<UseSet> useSetList = new HaiBindingList<UseSet>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SDUse> SDUses;
                    if (useSetToGet == null)
                        SDUses = ahamOper.SDUses.ToList<SDUse>();
                    else
                    {
                        SDUses = new List<SDUse>();
                        SDUses.Add(ahamOper.SDUses.Single<SDUse>(x => (x.UseSetKey == useSetToGet.UseSetKey)));
                    }

                    foreach (SDUse sduse in SDUses)
                    {
                        UseSet useSet = new UseSet();

                        useSet.UseSetAbbreviation = sduse.UseSetAbbrev.Trim();
                        useSet.UseSetKey = sduse.UseSetKey;
                        useSet.UseSetName = sduse.UseSetName.Trim();

                        useSet.ApplicationContext = sduse.AppContext.Trim();
                        useSet.RowVersion = sduse.RowVersion;
                        useSet.UserName = sduse.EntryUid.Trim();

                        useSet.MarkOld();
                        useSetList.AddToList(useSet);
                    }

                    result.DataList = useSetList;
                    result.Success = true;
                    result.BrowsablePropertyList = UseSet.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult UseSetGet(UseSet useSetToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = UseSetListGet(useSetToGet, parameters);
            if (result.Success)
            {
                UseSet useSet = (UseSet)result.SingleItem;
                result.ItemIsChanged = (useSetToGet.RowVersion != useSet.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult UseSetSave(UseSet useSet, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (useSet.IsSavable)
            {
                if (useSet.IsDeleted)
                    result = UseSetDelete(useSet, parameters);
                else if (useSet.IsNew)
                    result = UseSetInsert(useSet, parameters);
                else if (useSet.IsDirty)
                    result = UseSetUpdate(useSet, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult UseSetInsert(UseSet useSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDUse sduse = new SDUse();
                    sduse.UseSetAbbrev = useSet.UseSetAbbreviation.Trim();
                    sduse.UseSetName = useSet.UseSetName.Trim();

                    sduse.AppContext = parameters.ApplicationContext.Trim();
                    sduse.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SDUse>().InsertOnSubmit(sduse);
                    ahamOper.SubmitChanges();

                    useSet.UseSetKey = sduse.UseSetKey;
                    useSet.RowVersion = sduse.RowVersion;

                    useSet.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult UseSetUpdate(UseSet useSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDUse sduse = ahamOper.SDUses.Single<SDUse>(x => (x.UseSetKey == useSet.UseSetKey));

                    if (sduse.RowVersion != useSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + useSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sduse.UseSetAbbrev = useSet.UseSetAbbreviation.Trim();
                    sduse.UseSetName = useSet.UseSetName.Trim();

                    sduse.AppContext = parameters.ApplicationContext.Trim();
                    sduse.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    useSet.MarkOld();
                    useSet.RowVersion = sduse.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult UseSetDelete(UseSet useSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SDUse sduse = ahamOper.SDUses.Single<SDUse>(x => (x.UseSetKey == useSet.UseSetKey));

                    if (sduse.RowVersion != useSet.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + useSet.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    sduse.AppContext = parameters.ApplicationContext.Trim();
                    sduse.EntryUid = parameters.UserName.Trim();

                    ahamOper.SDUses.DeleteOnSubmit(sduse);
                    ahamOper.SubmitChanges();

                    useSet.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

        #region UseSetMember

        internal static DataAccessResult UseSetMemberListGet(UseSetMember useSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<UseSetMember> useSetMemberList = new HaiBindingList<UseSetMember>();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMUse> SMUses;
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<SMUse>(x => x.DUse);
                    dlo.LoadWith<SMUse>(x => x.SDUse);
                    ahamOper.LoadOptions = dlo;

                    if (useSetMemberToGet == null)
                    {
                        SMUses = ahamOper.SMUses.ToList<SMUse>();
                    }
                    else
                    {
                        SMUses = new List<SMUse>();
                        SMUses.Add(ahamOper.SMUses.Single<SMUse>(x => (x.UseSetMemberKey == useSetMemberToGet.UseSetMemberKey)));
                    }

                    foreach (SMUse smuse in SMUses)
                    {
                        UseSetMember useSetMember = new UseSetMember();

                        useSetMember.UseKey = smuse.UseKey;
                        useSetMember.UseSetKey = smuse.UseSetKey;
                        useSetMember.UseSetMemberKey = smuse.UseSetMemberKey;
                        useSetMember.DateRange.BeginDate = smuse.BeginDate.Date;
                        useSetMember.DateRange.EndDate = smuse.EndDate.Date;

                        useSetMember.UseName = smuse.DUse.UseName.Trim();
                        useSetMember.UseAbbreviation = smuse.DUse.UseAbbrev.Trim();
                        useSetMember.UseSetAbbreviation = smuse.SDUse.UseSetAbbrev.Trim();
                        useSetMember.UseSetName = smuse.SDUse.UseSetName.Trim();

                        useSetMember.ApplicationContext = smuse.AppContext.Trim();
                        useSetMember.RowVersion = smuse.RowVersion;
                        useSetMember.UserName = smuse.EntryUid.Trim();

                        useSetMember.MarkOld();
                        useSetMemberList.AddToList(useSetMember);
                    }

                    result.DataList = useSetMemberList;
                    result.Success = true;
                    result.BrowsablePropertyList = UseSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult UseSetMemberGet(UseSetMember useSetMemberToGet, DataServiceParameters parameters)
        {
            DataAccessResult result = UseSetMemberListGet(useSetMemberToGet, parameters);
            if (result.Success)
            {
                UseSetMember useSetMember = (UseSetMember)result.SingleItem;
                result.ItemIsChanged = (useSetMemberToGet.RowVersion != useSetMember.RowVersion);
            }

            return result;
        }

        internal static DataAccessResult UseSetMemberSave(UseSetMember useSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result;
            if (useSetMember.IsSavable)
            {
                if (useSetMember.IsDeleted)
                    result = UseSetMemberDelete(useSetMember, parameters);
                else if (useSetMember.IsNew)
                    result = UseSetMemberInsert(useSetMember, parameters);
                else if (useSetMember.IsDirty)
                    result = UseSetMemberUpdate(useSetMember, parameters);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        private static DataAccessResult UseSetMemberInsert(UseSetMember useSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMUse smuse = new SMUse();
                    smuse.UseKey = useSetMember.UseKey;
                    smuse.UseSetKey = useSetMember.UseSetKey;
                    smuse.BeginDate = useSetMember.DateRange.BeginDate.Date;
                    smuse.EndDate = useSetMember.DateRange.EndDate.Date;

                    smuse.AppContext = parameters.ApplicationContext.Trim();
                    smuse.EntryUid = parameters.UserName.Trim();

                    ahamOper.GetTable<SMUse>().InsertOnSubmit(smuse);
                    ahamOper.SubmitChanges();

                    useSetMember.UseSetMemberKey = smuse.UseSetMemberKey;
                    useSetMember.RowVersion = smuse.RowVersion;

                    useSetMember.MarkOld();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult UseSetMemberUpdate(UseSetMember useSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMUse smuse = ahamOper.SMUses.Single<SMUse>(x => (x.UseSetMemberKey == useSetMember.UseSetMemberKey));

                    if (smuse.RowVersion != useSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + useSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smuse.UseKey = useSetMember.UseKey;
                    smuse.UseSetKey = useSetMember.UseSetKey;
                    smuse.BeginDate = useSetMember.DateRange.BeginDate.Date;
                    smuse.EndDate = useSetMember.DateRange.EndDate.Date;

                    smuse.AppContext = parameters.ApplicationContext.Trim();
                    smuse.EntryUid = parameters.UserName.Trim();

                    ahamOper.SubmitChanges();

                    useSetMember.MarkOld();
                    useSetMember.RowVersion = smuse.RowVersion;
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        private static DataAccessResult UseSetMemberDelete(UseSetMember useSetMember, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    SMUse smuse = ahamOper.SMUses.Single<SMUse>(x => (x.UseSetMemberKey == useSetMember.UseSetMemberKey));

                    if (smuse.RowVersion != useSetMember.RowVersion)
                    {
                        result.Success = false;
                        result.Message = "Concurrency error: \"" + useSetMember.UniqueIdentifier
                            + "\" has been altered since being retrieved for this operation."
                            + "\n\n\rYour changes have NOT been saved.";
                        return result;
                    }

                    smuse.AppContext = parameters.ApplicationContext.Trim();
                    smuse.EntryUid = parameters.UserName.Trim();

                    ahamOper.SMUses.DeleteOnSubmit(smuse);
                    ahamOper.SubmitChanges();

                    useSetMember.MarkClean();
                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        internal static DataAccessResult UseSetMembersDuplicate(UseSet sourceUseSet, UseSet targetUseSet, DataServiceParameters parameters)
        {
            DataAccessResult result = new DataAccessResult();

            using (AhamoperClassesDataContext ahamOper = new AhamoperClassesDataContext(parameters.ConnectionString))
            {
                try
                {
                    List<SMUse> smUses;
                    List<SMUse> duplicatedItems = new List<SMUse>();

                    smUses = ahamOper.SMUses.Where(x => (x.UseSetKey == sourceUseSet.UseSetKey)).ToList<SMUse>();

                    foreach (SMUse smUse in smUses)
                    {
                        SMUse duplicateSMUse = new SMUse();

                        duplicateSMUse.UseSetKey = targetUseSet.UseSetKey;
                        duplicateSMUse.UseKey = smUse.UseKey;
                        duplicateSMUse.BeginDate = smUse.BeginDate.Date;
                        duplicateSMUse.EndDate = smUse.EndDate.Date;
                        duplicateSMUse.EntryUid = parameters.UserName.Trim();
                        duplicateSMUse.AppContext = parameters.ApplicationContext.Trim();

                        duplicatedItems.Add(duplicateSMUse);
                    }

                    ahamOper.GetTable<SMUse>().InsertAllOnSubmit<SMUse>(duplicatedItems);
                    ahamOper.SubmitChanges();

                    result.Success = true;
                    result.BrowsablePropertyList = UseSetMember.GetBrowsablePropertyList();
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
            }

            return result;
        }

        #endregion

    }
}
