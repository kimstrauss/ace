﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class Catalog : HaiBusinessObjectBase, IItem        // UNDERLYING TABLE: DCatalog
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public Catalog(): base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Catalog;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            CatalogName = string.Empty;
            CatalogAbbreviation = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
	        get 
	        {
                return CatalogName + " [" + CatalogAbbreviation + "]";
	        }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return CatalogKey;
            }
            set
            {
                CatalogKey = value;
            }
        }

        private int _catalogKey;
        public int CatalogKey
        {
            get
            {
                return _catalogKey;
            }
            set
            {
                _catalogKey = value;
            }
        }

        private string _catalogName;
        public string CatalogName
        {
            get
            {
                return _catalogName;
            }
            set
            {
                _catalogName = value;
            }
        }

        private string _catalogAbbreviation;
        public string CatalogAbbreviation
        {
            get
            {
                return _catalogAbbreviation;
            }
            set
            {
                _catalogAbbreviation = value;
            }
        }

        #region IItem Members

        [Browsable(false)]
        public int ItemKey
        {
            get
            {
                return CatalogKey;
            }
        }

        [Browsable(false)]
        public string ItemName
        {
            get
            {
                return CatalogName;
            }
        }

        [Browsable(false)]
        public string ItemAbbreviation
        {
            get
            {
                return CatalogAbbreviation;
            }
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("CatalogKey", "CatalogKey", "Key for the catalog", "Catalog key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogName", "CatalogName", "Name of the catalog", "Catalog name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogAbbreviation", "CatalogAbbrev", "Abbreviation for the catalog", "Catalog abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region Geneic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
