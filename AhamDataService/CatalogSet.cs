﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class CatalogSet : HaiBusinessObjectBase, ISet        // UNDERLYING TABLE: SDCatalog
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public CatalogSet(): base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.CatalogSet;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            CatalogSetAbbreviation = string.Empty;
            CatalogSetName = string.Empty;
        }

        #region Properties
        private int _catalogSetKey;
        public int CatalogSetKey
        {
            get
            {
                return _catalogSetKey;
            }
            set
            {
                _catalogSetKey = value;
            }
        }

        private string _catalogSetName;
        public string CatalogSetName
        {
            get
            {
                return _catalogSetName;
            }
            set
            {
                _catalogSetName = value;
            }
        }

        private string _catalogSetAbbreviation;
        public string CatalogSetAbbreviation
        {
            get
            {
                return _catalogSetAbbreviation;
            }
            set
            {
                _catalogSetAbbreviation = value;
            }
        }

        #endregion

        public override string UniqueIdentifier
        {
            get
            {
                return CatalogSetName + " [" + CatalogSetAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return CatalogSetKey;
            }
            set
            {
                CatalogSetKey = value;
            }
        }

        #region ISet Members

        [Browsable(false)]
        int ISet.SetKey
        {
            get
            {
                return CatalogSetKey;
            }
        }

        [Browsable(false)]
        string ISet.SetName
        {
            get
            {
                return CatalogSetName;
            }
        }

        [Browsable(false)]
        string ISet.SetAbbreviation
        {
            get
            {
                return CatalogSetAbbreviation;
            }
        }

        #endregion


        #region Generic Code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("CatalogSetKey", "CatalogSetKey", "Key for the catalog set", "Catalog set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogSetName", "CatalogSetName", "Name of the catalog set", "Catalog set name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogSetAbbreviation", "CatalogSetAbbrev", "Abbreviation for the catalog set", "Catalog set abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
