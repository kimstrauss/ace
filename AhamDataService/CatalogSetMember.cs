﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class CatalogSetMember : HaiBusinessObjectBase, IMember        // UNDERLYING TABLE: SMCatalog
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public CatalogSetMember(): base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.CatalogSetMember;
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();
        }

        private int _catalogSetMemberKey;
        public int CatalogSetMemberKey
        {
            get
            {
                return _catalogSetMemberKey;
            }
            set
            {
                _catalogSetMemberKey = value;
            }
        }

        private int _catalogKey;
        public int CatalogKey
        {
            get
            {
                return _catalogKey;
            }
            set
            {
                _catalogKey = value;
            }
        }

        private int _catalogSetKey;
        public int CatalogSetKey
        {
            get
            {
                return _catalogSetKey;
            }
            set
            {
                _catalogSetKey = value;
            }
        }

        private string _catalogAbbreviation;
        public string CatalogAbbreviation
        {
            get
            {
                return _catalogAbbreviation;
            }
            set
            {
                _catalogAbbreviation = value;
            }
        }

        private string _catalogName;
        public string CatalogName
        {
            get
            {
                return _catalogName;
            }
            set
            {
                _catalogName = value;
            }
        }

        private string _catalogSetName;
        public string CatalogSetName
        {
            get
            {
                return _catalogSetName;
            }
            set
            {
                _catalogSetName = value;
            }
        }

        private string _catalogSetAbbreviation;
        public string CatalogSetAbbreviation
        {
            get
            {
                return _catalogSetAbbreviation;
            }
            set
            {
                _catalogSetAbbreviation = value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return CatalogName + "<->" + CatalogSetName + " [" + DateRange.ToString() + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return CatalogSetMemberKey;
            }
            set
            {
                CatalogSetMemberKey = value;
            }
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("CatalogSetMemberKey", "CatalogSetMemberKey", "Key for this catalog set member", "Catalog set member key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogKey", "CatalogKey", "Key for the catalog", "Catalog key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogSetKey", "CatalogSetKey", "Key for the catalog set", "Catalog set key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogName", "", "Name of the catalog", "Catalog name");
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogAbbreviation", "CatalogAbbrev", "Abbreviation for the catalog", "Catalog abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogSetName", "CatalogSetName", "Name of the catalog set", "Catalog set name");
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogSetAbbreviation", "CatalogSetAbbrev", "Abbreviation for the catalog set", "Catalog set abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region IItem Members

        public int ItemKey
        {
            get
            {
                return CatalogKey;
            }
            set
            {
                CatalogKey = value;
            }
        }

        public int SetKey
        {
            get
            {
                return CatalogSetKey;
            }
            set
            {
                CatalogSetKey = value;
            }
        }

        public string ItemAbbreviation
        {
            get
            {
                return CatalogAbbreviation;
            }
            set
            {
                CatalogAbbreviation = value;
            }
        }

        public string ItemName
        {
            get
            {
                return CatalogName;
            }
            set
            {
                CatalogName = value;
            }
        }

        public string SetAbbreviation
        {
            get
            {
                return CatalogSetAbbreviation;
            }
            set
            {
                CatalogSetAbbreviation = value;
            }
        }

        public string SetName
        {
            get
            {
                return CatalogSetName;
            }
            set
            {
                CatalogSetName = value;
            }
        }

        #endregion


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
