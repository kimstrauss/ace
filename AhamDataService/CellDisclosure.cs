﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class CellDisclosure : HaiBusinessObjectBase, IDateRange        // UNDERLYING TABLE: DCellDisclosure
    {
        public CellDisclosure() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.CellDisclosure;

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            OutputReportID = string.Empty;
            IndustryMemberName = string.Empty;
            SourceSizeSetMemberName = string.Empty;
            DestinationSizeSetMemberName = string.Empty;
            Footnote = string.Empty;


            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());
            if (_kindredPropertyNames == null)
            {
                _kindredPropertyNames = new string[3] { "SourceSizeSetMemberKey", "OutputReportKey", "IndustryMemberKey" };
            }
        }

        #region Properties

        public string OutputReportID
        {
            get;
            set;
        }

        public string IndustryMemberName
        {
            get;
            set;
        }

        public string SourceSizeSetMemberName
        {
            get;
            set;
        }

        public string DestinationSizeSetMemberName
        {
            get;
            set;
        }

        public string Footnote
        {
            get;
            set;
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        private static string[] _kindredPropertyNames;
        [Browsable(false)]
        public string[] KindredPropertyNames
        {
            get
            {
                return _kindredPropertyNames;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return CellDisclosureKey;
            }
            set
            {
                CellDisclosureKey = value;
            }
        }

        public int OutputReportKey
        {
            get;
            set;
        }

        public int IndustryMemberKey
        {
            get;
            set;
        }

        public int SourceSizeSetMemberKey
        {
            get;
            set;
        }

        public int DestinationSizeSetMemberKey
        {
            get;
            set;
        }

        public int FootnoteKey
        {
            get;
            set;
        }

        public int CellDisclosureKey
        {
            get;
            set;
        }

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "CellDisclosureKey[" + CellDisclosureKey.ToString() + "]" ;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;
            bp = new BrowsableProperty("CellDisclosureKey", "CellDisclosureKey", "Key for the CellDisclosure", "CellDisclosure key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("OutputReportID", "outputreportid", "Name of the ouput report", "Output report name");
            bp.MustBeUnique = false;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("OutputReportKey", "outputreportkey", "Key for the output report", "Output report key");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("IndustryMemberName", "RIndustryMemberName", "Name of the industry member", "Industry member name");
            bp.MustBeUnique = false;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("IndustryMemberKey", "rindustrymemberkey", "Key for the industry member", "Industry member key");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SourceSizeSetMemberName", "SourceSizeSetName", "Name of the source size set", "Source size set name");
            bp.MustBeUnique = false;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SourceSizeSetMemberKey", "sourceSizeSetMemberKey", "Key for the source size set", "Source size set key");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("DestinationSizeSetMemberName", "DestSizeSetName", "Name of the destination size set", "Destination size set name");
            bp.MustBeUnique = false;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("DestinationSizeSetMemberKey", "DestSizesetmemberkey", "Key for the destination size set", "Destination size set key");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Footnote", "footnote", "Footnote", "Footnote");
            bp.MustBeUnique = false;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("FootnoteKey", "footnotekey", "Key for the footnote", "Footnote key");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

                
        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
