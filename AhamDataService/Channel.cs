﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class Channel : HaiBusinessObjectBase, IItem        // UNDERLYING TABLE: DChannel
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public Channel()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Channel;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ChannelName = string.Empty;
            ChannelAbbreviation = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return ChannelName + " [" + ChannelAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ChannelKey;
            }
            set
            {
                ChannelKey = value;
            }
        }

        private int _channelKey;
        public int ChannelKey
        {
            get
            {
                return _channelKey;
            }
            set
            {
                _channelKey = value;
            }
        }

        private string _channelName;
        public string ChannelName
        {
            get
            {
                return _channelName;
            }
            set
            {
                _channelName = value;
            }
        }

        private string _channelAbbreviation;
        public string ChannelAbbreviation
        {
            get
            {
                return _channelAbbreviation;
            }
            set
            {
                _channelAbbreviation = value;
            }
        }

        #region IItem Members

        [Browsable(false)]
        public int ItemKey
        {
            get
            {
                return ChannelKey;
            }
        }

        [Browsable(false)]
        public string ItemName
        {
            get
            {
                return ChannelName;
            }
        }

        [Browsable(false)]
        public string ItemAbbreviation
        {
            get
            {
                return ChannelAbbreviation;
            }
        }

        #endregion


        #region Generic Code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ChannelKey", "ChannelKey", "Key for the channel", "Channel key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelName", "ChannelName", "Name of the channel", "Channel name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelAbbreviation", "ChannelAbbrev", "Abbreviation for the channel", "Channel abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
