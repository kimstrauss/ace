﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ChannelHeirarchySet : HaiBusinessObjectBase, ISet        // UNDERLYING TABLE: HDChannel
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ChannelHeirarchySet()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ChannelHeirarchySet;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ChannelHeirarchySetName = string.Empty;
            ChannelHeirarchySetAbbreviation = string.Empty;
            ChannelSetName = string.Empty;
            ParentChannelSetName = string.Empty;

        }

        #region Properties
        private int _channelHeirarchySetKey;
        public int ChannelHeirarchySetKey
        {
            get
            {
                return _channelHeirarchySetKey;
            }
            set
            {
                _channelHeirarchySetKey = value;
            }
        }

        private string _channelHeirarchySetName;
        public string ChannelHeirarchySetName
        {
            get
            {
                return _channelHeirarchySetName;
            }
            set
            {
                _channelHeirarchySetName = value;
            }
        }

        private string _channelHeirarchySetAbbreviation;
        public string ChannelHeirarchySetAbbreviation
        {
            get
            {
                return _channelHeirarchySetAbbreviation;
            }
            set
            {
                _channelHeirarchySetAbbreviation = value;
            }
        }
        private int _channelSetKey;
        public int ChannelSetKey
        {
            get
            {
                return _channelSetKey;
            }
            set
            {
                _channelSetKey = value;
            }
        }
        public string ChannelSetName
        {
            get;
            set;
        }
        private int _parentChannelSetKey;
        public int ParentChannelSetKey
        {
            get
            {
                return _parentChannelSetKey;
            }
            set
            {
                _parentChannelSetKey = value;
            }
        }
        public string ParentChannelSetName
        {
            get;
            set;
        }
        #endregion

        public override string UniqueIdentifier
        {
            get
            {
                return ChannelHeirarchySetName + " [" + ChannelHeirarchySetAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ChannelHeirarchySetKey;
            }
            set
            {
                ChannelHeirarchySetKey = value;
            }
        }

        #region ISet Members

        [Browsable(false)]
        int ISet.SetKey
        {
            get
            {
                return ChannelHeirarchySetKey;
            }
        }

        [Browsable(false)]
        string ISet.SetName
        {
            get
            {
                return ChannelHeirarchySetName;
            }
        }

        [Browsable(false)]
        string ISet.SetAbbreviation
        {
            get
            {
                return ChannelHeirarchySetAbbreviation;
            }
        }

        #endregion


        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ChannelHeirarchySetKey", "ChannelHeirarchySetKey", "Key for the channel Heirarchy set", "Channel Heirarchy set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelHeirarchySetName", "ChannelHeirarchySetName", "Name of the channel Heirarchy set", "Channel Heirarchy set name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelHeirarchySetAbbreviation", "ChannelHeirarchySetAbbrev", "Abbreviation for the channel Heirarchy set", "Channel Heirarchy set abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelSetKey", "ChannelSetKey", "Channel Set to get the Children from", "Child Channel Set Key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);
            bp = new BrowsableProperty("ChannelSetName", "ChannelSetName", "Channel Set to get the Children from", "Child Channel Set Name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
            bp = new BrowsableProperty("ParentChannelSetKey", "ParentChannelSetKey", "Channel Set to get the Parentren from", "Parent Channel Set Key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);
            bp = new BrowsableProperty("ParentChannelSetName", "ParentChannelSetName", "Channel Set to get the Parentren from", "Parent Channel Set Name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
