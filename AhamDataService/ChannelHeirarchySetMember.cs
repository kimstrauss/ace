﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ChannelHeirarchySetMember : HaiBusinessObjectBase, IItem
        //IMember        // UNDERLYING TABLE: HMChannel
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ChannelHeirarchySetMember()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ChannelHeirarchySetMember;

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();
        }
        private string _channelHeirarchySetName;
        public string ChannelHeirarchySetName
        {
            get
            {
                return _channelHeirarchySetName;
            }
            set
            {
                _channelHeirarchySetName = value;
            }
        }


        private string _childChannelName;
        public string ChildChannelName
        {
            get
            {
                return _childChannelName;
            }
            set
            {
                _childChannelName = value;
            }
        }

        private string _channelName;
        public string ChannelName       //parent
        {
            get
            {
                return _channelName;
            }
            set
            {
                _channelName = value;
            }
        }
        private bool _isArbitraryParent;
        public bool IsArbitraryParent
        {
            get
            {
                return _isArbitraryParent;
            }
            set
            {
                _isArbitraryParent = value;
            }
        }

        private string _channelHeirarchySetAbbreviation;
        public string ChannelHeirarchySetAbbreviation
        {
            get
            {
                return _channelHeirarchySetAbbreviation;
            }
            set
            {
                _channelHeirarchySetAbbreviation = value;
            }
        }
        private string _channelAbbreviation;
        public string ChannelAbbreviation
        {
            get
            {
                return _channelAbbreviation;
            }
            set
            {
                _channelAbbreviation = value;
            }
        }


        private string _childChannelAbbreviation;
        public string ChildChannelAbbreviation
        {
            get
            {
                return _childChannelAbbreviation;
            }
            set
            {
                _childChannelAbbreviation = value;
            }
        }
        private int _channelHeirarchySetMemberKey;
        public int ChannelHeirarchySetMemberKey
        {
            get
            {
                return _channelHeirarchySetMemberKey;
            }
            set
            {
                _channelHeirarchySetMemberKey = value;
            }
        }

        private int _channelSetMemberKey;
        public int ChannelSetMemberKey
        {
            get
            {
                return _channelSetMemberKey;
            }
            set
            {
                _channelSetMemberKey = value;
            }
        }

        private int _childChannelSetMemberKey;
        public int ChildChannelSetMemberKey
        {
            get
            {
                return _childChannelSetMemberKey;
            }
            set
            {
                _childChannelSetMemberKey = value;
            }
        }

        [Browsable(false)]
        private int _parentChannelSetKey;
        public int ParentChannelSetKey
        {
            get
            {
                return _parentChannelSetKey;
            }
            set
            {
                _parentChannelSetKey = value;
            }
        }


        private int _channelHeirarchySetKey;
        public int ChannelHeirarchySetKey
        {
            get
            {
                return _channelHeirarchySetKey;
            }
            set
            {
                _channelHeirarchySetKey = value;
            }
        }



        public override string UniqueIdentifier
        {
            get
            {
                return ChildChannelName + "<->" + ChannelName + "<->" + ChannelHeirarchySetName;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ChannelHeirarchySetMemberKey;
            }
            set
            {
                ChannelHeirarchySetMemberKey = value;
            }
        }
        //[Browsable(false)]
        //public DateRange DateRange
        //{
        //    get;
        //    set;
        //}

        //public DateTime BeginDate
        //{
        //    get
        //    {
        //        return DateRange.BeginDate.Date;
        //    }
        //    set
        //    {
        //        DateRange.BeginDate = value.Date;
        //    }
        //}

        //public DateTime EndDate
        //{
        //    get
        //    {
        //        return DateRange.EndDate.Date;
        //    }
        //    set
        //    {
        //        DateRange.EndDate = value.Date;
        //    }
        //}
        #region IItem Members

        [Browsable(false)]
        public int ItemKey
        {
            get
            {
                return ChannelSetMemberKey;
            }
        }

        //public int SetKey
        //{
        //    get
        //    {
        //        return ChannelHeirarchySetKey;
        //    }
        //}

        [Browsable(false)]
        public string ItemAbbreviation
        {
            get
            {
                return ChannelAbbreviation;
            }
        }

        [Browsable(false)]
        public string ItemName
        {
            get
            {
                return ChannelName;
            }
        }

        //public string SetAbbreviation
        //{
        //    get
        //    {
        //        return ChannelHeirarchySetAbbreviation;
        //    }
        //    set
        //    {
        //        ChannelHeirarchySetAbbreviation = value;
        //    }
        //}

        //public string SetName
        //{
        //    get
        //    {
        //        return ChannelHeirarchySetName;
        //    }
        //    set
        //    {
        //        ChannelHeirarchySetName = value;
        //    }
        //}

        #endregion
        
        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ChannelHeirarchySetMemberKey", "ChannelHeirarchySetMemberKey", "Key for this channel heirarchy member", "Channel heirarchy member key");
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelHeirarchySetKey", "ChannelHeirarchySetKey", "Key for the channel heirarchy", "Channel heirarchy key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelHeirarchySetName", "ChannelHeirarchySetName", "Name of the channel heirarchy", "Channel heirarchy name");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelHeirarchySetAbbreviation", "ChannelHeirarchySetAbbrev", "Abbreviation for the channel heirarchy", "Channel heirarchy abbreviation");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 12;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChildChannelSetMemberKey", "ChildChannelSetMemberKey", "Key for the Child channel", "Child Channel key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChildChannelName", "", "Name of the Child channel", "Child Channel name");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChildChannelAbbreviation", "ChildChannelAbbrev", "Abbreviation for the Child channel", "Child Channel abbreviation");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 12;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelSetMemberKey", "ChannelSetMemberKey", "Key for the parent channel", "Parent Channel key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelName", "", "Name of the parent channel", "Parent Channel name");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelAbbreviation", "ChannelAbbrev", "Abbreviation for the parent channel", "Parent Channel abbreviation");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 12;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ParentChannelSetKey", "ParentChannelSetKey", "Key for the parent channel Set", "Parent Channel Set key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("IsArbitraryParent", "IsArbitraryParent", "Parent set arbitrarily", "Parent set arbitrarily");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            //  bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            //bp.IsReadonly = true;
            //bp.DataType = TypeCode.DateTime;
            //_browsablePropertyList.Add(bp.PropertyName, bp);

            //bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            //bp.IsReadonly = true;
            //bp.DataType = TypeCode.DateTime;
            //_browsablePropertyList.Add(bp.PropertyName, bp);

        }

 

 
        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
