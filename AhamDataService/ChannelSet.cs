﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ChannelSet : HaiBusinessObjectBase, ISet        // UNDERLYING TABLE: SDChannel
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ChannelSet()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ChannelSet;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ChannelSetAbbreviation = string.Empty;
            ChannelSetAbbreviation = string.Empty;
        }

        #region Properties
        private int _channelSetKey;
        public int ChannelSetKey
        {
            get
            {
                return _channelSetKey;
            }
            set
            {
                _channelSetKey = value;
            }
        }

        private string _channelSetName;
        public string ChannelSetName
        {
            get
            {
                return _channelSetName;
            }
            set
            {
                _channelSetName = value;
            }
        }

        private string _channelSetAbbreviation;
        public string ChannelSetAbbreviation
        {
            get
            {
                return _channelSetAbbreviation;
            }
            set
            {
                _channelSetAbbreviation = value;
            }
        }
        #endregion

        public override string UniqueIdentifier
        {
            get
            {
                return ChannelSetName + " [" + ChannelSetAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ChannelSetKey;
            }
            set
            {
                ChannelSetKey = value;
            }
        }

        #region ISet Members

        [Browsable(false)]
        int ISet.SetKey
        {
            get
            {
                return ChannelSetKey;
            }
        }

        [Browsable(false)]
        string ISet.SetName
        {
            get
            {
                return ChannelSetName;
            }
        }

        [Browsable(false)]
        string ISet.SetAbbreviation
        {
            get
            {
                return ChannelSetAbbreviation;
            }
        }

        #endregion


        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ChannelSetKey", "ChannelSetKey", "Key for the channel set", "Channel set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelSetName", "ChannelSetName", "Name of the channel set", "Channel set name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelSetAbbreviation", "ChannelSetAbbrev", "Abbreviation for the channel set", "Channel set abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
