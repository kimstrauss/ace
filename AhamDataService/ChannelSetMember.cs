﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ChannelSetMember : HaiBusinessObjectBase, IMember        // UNDERLYING TABLE: SMChannel
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ChannelSetMember()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ChannelSetMember;
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();
        }

        private int _channelSetMemberKey;
        public int ChannelSetMemberKey
        {
            get
            {
                return _channelSetMemberKey;
            }
            set
            {
                _channelSetMemberKey = value;
            }
        }

        private int _channelKey;
        public int ChannelKey
        {
            get
            {
                return _channelKey;
            }
            set
            {
                _channelKey = value;
            }
        }

        private int _channelSetKey;
        public int ChannelSetKey
        {
            get
            {
                return _channelSetKey;
            }
            set
            {
                _channelSetKey = value;
            }
        }

        private string _channelAbbreviation;
        public string ChannelAbbreviation
        {
            get
            {
                return _channelAbbreviation;
            }
            set
            {
                _channelAbbreviation = value;
            }
        }

        private string _channelName;
        public string ChannelName
        {
            get
            {
                return _channelName;
            }
            set
            {
                _channelName = value;
            }
        }

        private string _channelSetName;
        public string ChannelSetName
        {
            get
            {
                return _channelSetName;
            }
            set
            {
                _channelSetName = value;
            }
        }

        private string _channelSetAbbreviation;
        public string ChannelSetAbbreviation
        {
            get
            {
                return _channelSetAbbreviation;
            }
            set
            {
                _channelSetAbbreviation = value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return ChannelName + "<->" + ChannelSetName + " [" + DateRange.ToString() + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ChannelSetMemberKey;
            }
            set
            {
                ChannelSetMemberKey = value;
            }
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ChannelSetMemberKey", "ChannelSetMemberKey", "Key for this channel set member", "Channel set member key");
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelKey", "ChannelKey", "Key for the channel", "Channel key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelSetKey", "ChannelSetKey", "Key for the channel set", "Channel set key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelName", "", "Name of the channel", "Channel name");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelAbbreviation", "ChannelAbbrev", "Abbreviation for the channel", "Channel abbreviation");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 12;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelSetName", "ChannelSetName", "Name of the channel set", "Channel set name");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelSetAbbreviation", "ChannelSetAbbrev", "Abbreviation for the channel set", "Channel set abbreviation");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 12;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region IItem Members

        public int ItemKey
        {
            get
            {
                return ChannelKey;
            }
            set
            {
                ChannelKey = value;
            }
        }

        public int SetKey
        {
            get
            {
                return ChannelSetKey;
            }
            set
            {
                ChannelSetKey = value;
            }
        }

        public string ItemAbbreviation
        {
            get
            {
                return ChannelAbbreviation;
            }
            set
            {
                ChannelAbbreviation = value;
            }
        }

        public string ItemName
        {
            get
            {
                return ChannelName;
            }
            set
            {
                ChannelName = value;
            }
        }

        public string SetAbbreviation
        {
            get
            {
                return ChannelSetAbbreviation;
            }
            set
            {
                ChannelSetAbbreviation = value;
            }
        }

        public string SetName
        {
            get
            {
                return ChannelSetName;
            }
            set
            {
                ChannelSetName = value;
            }
        }

        #endregion


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
