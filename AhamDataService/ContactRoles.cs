﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class ContactRoles : HaiBusinessObjectBase        // UNDERLYING TABLE: ContactRoles
    {
        public ContactRoles() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ContactRoles;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            AssociationName = string.Empty;
            ManufacturerName = string.Empty;
            UserCode = string.Empty;
            ProductName = string.Empty;
            CatalogName = string.Empty;
        }

        #region Properties

        public int ContactRolesKey
        {
            get;
            set;
        }

        public string UserCode
        {
            get;
            set;
        }

        public int AssociationKey
        {
            get;
            set;
        }

        public string AssociationName
        {
            get;
            set;
        }

        public int ManufacturerKey
        {
            get;
            set;
        }

        public string ManufacturerName
        {
            get;
            set;
        }

        public int ProductKey
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public int CatalogKey
        {
            get;
            set;
        }

        public string CatalogName
        {
            get;
            set;
        }

        public bool SeeCompanyData
        {
            get;
            set;
        }

        public bool EditCompanyData
        {
            get;
            set;
        }

        public bool SeeIndustryData
        {
            get;
            set;
        }

        public bool SeeModels
        {
            get;
            set;
        }

        public bool EditModels
        {
            get;
            set;
        }

        public bool SubmitData
        {
            get;
            set;
        }

        public bool Responsible
        {
            get;
            set;
        }

        public bool EditCodes
        {
            get;
            set;
        }

        public bool SeePublic
        {
            get;
            set;
        }

        public bool SeeAll
        {
            get;
            set;
        }

        public bool SeeStatusDetails
        {
            get;
            set;
        }

        public bool Billable
        {
            get;
            set;
        }

        public int SecurityLevel
        {
            get;
            set;
        }



        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "Key = " + ContactRolesKey.ToString(); ;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ContactRolesKey;
            }
            set
            {
                ContactRolesKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ContactRolesKey", "ContactRolesKey", "Key for the contact roles", "Contact roles key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UserCode", "NTUser", "NT User code", "User code");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 50;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("AssociationName", "", "Name of the association", "Name of the association");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("AssociationKey", "AssnKey", "Key for the association", "Key for the association");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ManufacturerName", "Name", "Name of the manufacturer", "Name of the manufacturer");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ManufacturerKey", "MfgKey", "Key for the manufacturer", "Key for the manufacturer");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductName", "", "Name of the product", "Name of the product");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductKey", "ProductKey", "Key for the product", "Key for the product");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogName", "", "Name of the catalog", "Name of the catalog");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogKey", "CatalogKey", "Key for the product", "Key for the product");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);


            bp = new BrowsableProperty("SeeCompanyData", "SeeCompanyData", "Allow seeing company data", "See company data");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EditCompanyData", "EditCompanyData", "Allow edit company data ", "Edit company data");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SeeIndustryData", "SeeIndustryData", "Allow seeing industry data", "See industry data");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SeeModels", "SeeModels", "Allow seeing models", "See models");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EditModels", "EditModels", "Allow edit models ", "Edit models");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SubmitData", "SubmitData", "Allow submitting data", "Submit data");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Responsible", "Responsible", "Contact is responsible", "Responsible contact");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EditCodes", "EditCodes", "Allow edit codes", "Edit codes");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SeePublic", "SeePublic", "Allow seeing public data", "See public data");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SeeAll", "SeeAll", "Allow seeing all company report definitions", "See all report def'n");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SeeStatusDetails", "SeeStatusDetails", "Allow seeing status details", "See status details");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Billable", "Billable", "Is billable", "Is billable");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SecurityLevel", "SecLevel", "Contact's security level", "Security level");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
