﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class Council : HaiBusinessObjectBase        // UNDERLYING TABLE: Dcouncil
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public Council() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Council;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            CouncilAbbreviation = string.Empty;
            CouncilName = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return CouncilName + " [" + CouncilAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return CouncilKey;
            }
            set
            {
                CouncilKey = value;
            }
        }

        #region Properties
        private int _councilKey;
        public int CouncilKey
        {
            get
            {
                return _councilKey;
            }
            set
            {
                _councilKey = value;
            }
        }

        private string _councilName;
        public string CouncilName
        {
            get
            {
                return _councilName;
            }
            set
            {
                _councilName = value;
            }
        }

        private string _councilAbbreviation;
        public string CouncilAbbreviation
        {
            get
            {
                return _councilAbbreviation;
            }
            set
            {
                _councilAbbreviation = value;
            }
        }
        #endregion


        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("CouncilKey", "CouncilKey", "Key for the council", "Council key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CouncilName", "CouncilName", "Name of the council", "Council name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CouncilAbbreviation", "CouncilAbbrev", "Abbreviation for the council", "Council abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion

    }
}
