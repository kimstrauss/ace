﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class Dimension : HaiBusinessObjectBase        // UNDERLYING TABLE: DDim
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public Dimension() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Dimension;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            DimensionAbbreviation = string.Empty;
            DimensionName = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return DimensionName + " [" + DimensionAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return DimensionKey;
            }
            set
            {
                DimensionKey = value;
            }
        }

        public int DimensionKey
        {
            get;
            set;
        }

        public string DimensionName
        {
            get;
            set;
        }

        public string DimensionAbbreviation
        {
            get;
            set;
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("DimensionKey", "DimKey", "Key for the dimension", "Dimension key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("DimensionName", "DimName", "Name of the dimension", "Dimension name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("DimensionAbbreviation", "DimAbbrev", "Abbreviation for the dimension", "Dimension abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region Geneic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion

    }
}
