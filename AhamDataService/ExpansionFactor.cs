﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ExpansionFactor : HaiBusinessObjectBase, IDateRange        // UNDERLYING TABLE: DExpFac
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ExpansionFactor(): base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ExpansionFactor;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ProductName = string.Empty;
            MarketName = string.Empty;
            CatalogName = string.Empty;
            ActivityName = string.Empty;

            // Initializations for IDateRange implementation
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());
            if (_kindredPropertyNames == null)
            {
                _kindredPropertyNames = new string[4] { "ProductKey", "MarketKey", "CatalogKey", "ActivityKey" };
            }
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
	        get 
	        {
                return "[" + MarketName + "].[" + CatalogName + "].[" + ProductName + "].[" + ActivityName + "]";
	        }
        }

        public int ExpansionFactorKey
        {
            get;
            set;
        }

        public int ProductKey
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public int MarketKey
        {
            get;
            set;
        }

        public string MarketName
        {
            get;
            set;
        }

        public int CatalogKey
        {
            get;
            set;
        }

        public string CatalogName
        {
            get;
            set;
        }

        public int ActivityKey
        {
            get;
            set;
        }

        public string ActivityName
        {
            get;
            set;
        }

        public bool AllowRelease
        {
            get;
            set;
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        private static string[] _kindredPropertyNames;
        [Browsable(false)]
        public string[] KindredPropertyNames
        {
            get
            {
                return _kindredPropertyNames;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ExpansionFactorKey;
            }
            set
            {
                ExpansionFactorKey = value;
            }
        }

        public Double ExpansionFactorQuantity
        {
            get;
            set;
        }

        public Double ExpansionFactorValue
        {
            get;
            set;
        }


        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ExpansionFactorKey", "ExpansionFactorKey", "Key for the expansion factor", "Expansion factor key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityKey", "ActivityKey", "Key for the activity", "Key for the activity");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogKey", "CatalogKey", "Key for the catalog", "Key for the catalog");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketKey", "MarketKey", "Key for the market", "Key for the market");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductKey", "ProductKey", "Key for the product", "Key for the product");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyDescription, bp);

            bp = new BrowsableProperty("ExpansionFactorValue", "ExpansionFactorValue", "Expansion factor for value", "Expansion factor for value");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Double;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ExpansionFactorQuantity", "ExpansionFactorQuantity", "Expansion factor for quantity", "Expansion factor for quantity");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Double;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityName", "ActivityName", "Name of the activity", "Name of the activity");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogName", "CatalogName", "Name of the catalog", "Name of the catalog");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketName", "MarketName", "Name of the market", "Name of the market");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductName", "ProductName", "Name of the product", "Name of the product");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("AllowRelease", "AllowRelease", "Allow release", "Allow release");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion

        #region IDateRange Members

        DateRange IDateRange.DateRange
        {
            get
            {
                return this.DateRange;
            }
            set
            {
                this.DateRange = value;
            }
        }

        DateTime IDateRange.BeginDate
        {
            get
            {
                return this.BeginDate;
            }
            set
            {
                this.BeginDate = value;
            }
        }

        DateTime IDateRange.EndDate
        {
            get
            {
                return this.EndDate;
            }
            set
            {
                this.EndDate = value;
            }
        }

        string IDateRange.UniqueIdentifier
        {
            get
            {
                return this.UniqueIdentifier;
            }
        }

        #endregion
    }
}
