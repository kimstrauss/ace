﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class FSList : HaiBusinessObjectBase, IDateRange        // UNDERLYING TABLE: FSList
    {
        public FSList()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.FSList;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            FullName = string.Empty;
            UserCode = string.Empty;
            Company = string.Empty;
            WorkPhoneNumber = string.Empty;
            FaxNumber = string.Empty;
            EmailAddress = string.Empty;
            ProductCode = string.Empty;
            Notes = string.Empty;

            ChargeDateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());

            // Initializations for IDateRange implementation
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());
            if (_kindredPropertyNames == null)
            {
                _kindredPropertyNames = new string[0] {};   // no kindreds
            }
        }

        #region Properties

        public int FSListKey
        {
            get;
            set;
        }

        public string FullName
        {
            get;
            set;
        }

        public string UserCode
        {
            get;
            set;
        }

        public string Company
        {
            get;
            set;
        }

        public string WorkPhoneNumber
        {
            get;
            set;
        }

        public string FaxNumber
        {
            get;
            set;
        }

        public string EmailAddress
        {
            get;
            set;
        }

        public string ProductCode
        {
            get;
            set;
        }

        public bool FS
        {
            get;
            set;
        }

        public bool Web
        {
            get;
            set;
        }

        public bool SendEmail
        {
            get;
            set;
        }

        public bool SendFax
        {
            get;
            set;
        }

        public string Notes
        {
            get;
            set;
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        private static string[] _kindredPropertyNames;
        [Browsable(false)]
        public string[] KindredPropertyNames
        {
            get
            {
                return _kindredPropertyNames;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return FSListKey;
            }
            set
            {
                FSListKey = value;
            }
        }

        [Browsable(false)]
        public DateRange ChargeDateRange
        {
            get;
            set;
        }

        public DateTime BeginChargeDate
        {
            get
            {
                return ChargeDateRange.BeginDate.Date;
            }
            set
            {
                ChargeDateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndChargeDate
        {
            get
            {
                return ChargeDateRange.EndDate.Date;
            }
            set
            {
                ChargeDateRange.EndDate = value.Date;
            }
        }

        public bool DenyAccess
        {
            get;
            set;
        }

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "fslistid = " + FSListKey.ToString();
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("FSListKey", "fslistid", "Key for the FSList", "FSList key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("FullName", "FULL_NAME", "Full name", "Full name");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 50;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UserCode", "NTUser", "User logon account", "NT user code");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 50;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Company", "COMPANY", "Company name", "Company name");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 50;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("WorkPhoneNumber", "WORK_PHONE", "Work phone number", "Work phone number");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 25;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("FaxNumber", "FAX", "Fax phone number", "Fax phone number");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 25;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EmailAddress", "EMAIL", "Email address", "Email address");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 50;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductCode", "PRODUCT_CODE", "Product code", "Product code");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 50;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("FS", "FS", "FS", "FS");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Web", "WEB", "Web", "Web");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SendEmail", "SendEmail", "Send email?", "Send email?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SendFax", "SendFax", "Send fax?", "Send fax?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Notes", "Notes", "Notes", "Notes");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 100;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginChargeDate", "Charge", "Begin charging date", "Begin charging date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndChargeDate", "EndChargeDate", "End charging date", "End charging date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("DenyAccess", "DenyAccess", "Deny access?", "Deny access?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
