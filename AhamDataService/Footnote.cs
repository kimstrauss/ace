﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class Footnote : HaiBusinessObjectBase        // UNDERLYING TABLE: dFootnote
    {
        public Footnote() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Footnote;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            FootnoteText = string.Empty;
        }

        #region Properties

        public int FootnoteKey
        {
            get;
            set;
        }

        public string FootnoteText
        {
            get;
            set;
        }

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return FootnoteText + " [" + FootnoteKey + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return FootnoteKey;
            }
            set
            {
                FootnoteKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("FootnoteKey", "FootnoteKey", "Key for the footnote", "Footnote key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("FootnoteText", "Footnote", "Text of the footnote", "Text of the footnote");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 350;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
