﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class Frequency : HaiBusinessObjectBase        // UNDERLYING TABLE: DFreq
    {
        public Frequency() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Frequency;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            FrequencyID = string.Empty;
            FrequencyName = string.Empty;
        }

        #region Properties

        public int FrequencyKey
        {
            get;
            set;
        }

        public string FrequencyID
        {
            get;
            set;
        }

        public string FrequencyName
        {
            get;
            set;
        }

        public decimal PeriodsPerYear
        {
            get;
            set;
        }

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return FrequencyName + " [" + FrequencyID + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return FrequencyKey;
            }
            set
            {
                FrequencyKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("FrequencyKey", "FreqKey", "Key for the frequency", "Frequency key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("FrequencyName", "FrequencyName", "Name of the frequency", "Frequency name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 16;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("FrequencyID", "FrequencyId", "ID for the frequency", "Frequency ID");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 1;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("PeriodsPerYear", "PeriodsPerYear", "Number of periods in a year", "Periods per year");
            bp.MustBeUnique = false;
            bp.DataType = TypeCode.Decimal;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
