﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class Geography : HaiBusinessObjectBase, IItem        // UNDERLYING TABLE: DGeo
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public Geography() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Geography;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            GeographyAbbreviation = string.Empty;
            GeographyName = string.Empty;
            GeographyID = string.Empty;
            GeographySuffix = string.Empty;
            GeographyNameSuffix = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return GeographyName + " [" + GeographyAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return GeographyKey;
            }
            set
            {
                GeographyKey = value;
            }
        }

        private int _geographyKey;
        public int GeographyKey
        {
            get
            {
                return _geographyKey;
            }
            set
            {
                _geographyKey = value;
            }
        }

        private string _geographyName;
        public string GeographyName
        {
            get
            {
                return _geographyName;
            }
            set
            {
                _geographyName = value;
            }
        }

        private string _geographyAbbreviation;
        public string GeographyAbbreviation
        {
            get
            {
                return _geographyAbbreviation;
            }
            set
            {
                _geographyAbbreviation = value;
            }
        }



        // additional properties
        public int RegionID
        {
            get;
            set;
        }

        public int NationISO
        {
            get;
            set;
        }

        public int StateFIPS
        {
            get;
            set;
        }

        public int CountyFIPS
        {
            get;
            set;
        }

        public string GeographyID
        {
            get;
            set;
        }

        public string GeographySuffix
        {
            get;
            set;
        }

        public string GeographyNameSuffix
        {
            get;
            set;
        }

        public int StateVertex
        {
            get;
            set;
        }

        public int CountyVertex
        {
            get;
            set;
        }
        public int CensusRegionNumber
        {
            get;
            set;
        }


        #region IItem Members

        [Browsable(false)]
        public int ItemKey
        {
            get
            {
                return GeographyKey;
            }
        }

        [Browsable(false)]
        public string ItemName
        {
            get
            {
                return GeographyName;
            }
        }

        [Browsable(false)]
        public string ItemAbbreviation
        {
            get
            {
                return GeographyAbbreviation;
            }
        }

        #endregion

        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("GeographyKey", "GeographyKey", "Key for the geography", "Geography key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyName", "GeographyName", "Name of the geographic place", "Geographic name");
            bp.MustBeUnique = false;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyAbbreviation", "GeographyAbbrev", "Abbreviation for the geographic place", "Geographic place abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);


            // additional properties
            bp = new BrowsableProperty("RegionID", "RegionID", "Region identifier", "Region identifier");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("NationISO", "NationISO", "Nation ISO", "Nation ISO");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("StateFIPS", "StateFIPS", "State FIPS code", "State FIPS code");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CountyFIPS", "CountyFIPS", "County FIPS code", "County FIPS code");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyID", "GeoID", "Geography identifier", "Geographic area identifier");
            bp.MaxStringLength = 16;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographySuffix", "GeoSuffix", "Geography suffix", "Geography suffix");
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyNameSuffix", "GeoNameSuffix", "Geography name suffix", "Geography name suffix");
            bp.MaxStringLength = 94;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("StateVertex", "StateVertex", "State vertex", "State vertex");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CountyVertex", "CountyVertex", "County vertex", "County vertex");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CensusRegionNumber", "CensusRegionNo", "Census region number", "Census region number");
            bp.DataType = TypeCode.Int32;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
