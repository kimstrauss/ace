﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class GeographySet : HaiBusinessObjectBase, ISet        // UNDERLYING TABLE: SDGeo
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public GeographySet() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.GeographySet;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            GeographySetAbbreviation = string.Empty;
            GeographySetName = string.Empty;
            GeographyLevel = "A";
        }

        #region Properties

        private int _geographySetKey;
        public int GeographySetKey
        {
            get
            {
                return _geographySetKey;
            }
            set
            {
                _geographySetKey = value;
            }
        }

        private string _geographySetName;
        public string GeographySetName
        {
            get
            {
                return _geographySetName;
            }
            set
            {
                _geographySetName = value;
            }
        }

        private string _geographySetAbbreviation;
        public string GeographySetAbbreviation
        {
            get
            {
                return _geographySetAbbreviation;
            }
            set
            {
                _geographySetAbbreviation = value;
            }
        }

        public string GeographyLevel
        {
            get;
            set;
        }

        #endregion

        public int HeirarchyId
        {
            get;
            set;
        }

        public override string UniqueIdentifier
        {
            get
            {
                return GeographySetName + " [" + GeographySetAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return GeographySetKey;
            }
            set
            {
                GeographySetKey = value;
            }
        }


        #region ISet Members

        [Browsable(false)]
        int ISet.SetKey
        {
            get
            {
                return GeographySetKey;
            }
        }

        [Browsable(false)]
        string ISet.SetName
        {
            get
            {
                return GeographySetName;
            }
        }

        [Browsable(false)]
        string ISet.SetAbbreviation
        {
            get
            {
                return GeographySetAbbreviation;
            }
        }

        #endregion

        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("GeographySetKey", "GeographySetKey", "Key for the geography set", "Geography set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographySetName", "GeographySetName", "Name of the geography set", "Geography set name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographySetAbbreviation", "GeographySetAbbrev", "Abbreviation for the geography set", "Geography set abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyLevel", "GeoLevel", "Geography level", "Geography level");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 1;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
