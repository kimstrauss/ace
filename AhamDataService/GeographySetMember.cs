﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class GeographySetMember : HaiBusinessObjectBase, IMember        // UNDERLYING TABLE: SMGeo
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public GeographySetMember() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.GeographySetMember;
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();
        }

        private int _geographySetMemberKey;
        public int GeographySetMemberKey
        {
            get
            {
                return _geographySetMemberKey;
            }
            set
            {
                _geographySetMemberKey = value;
            }
        }

        private int _geographyKey;
        public int GeographyKey
        {
            get
            {
                return _geographyKey;
            }
            set
            {
                _geographyKey = value;
            }
        }

        private int _geographySetKey;
        public int GeographySetKey
        {
            get
            {
                return _geographySetKey;
            }
            set
            {
                _geographySetKey = value;
            }
        }

        private string _geographyAbbreviation;
        public string GeographyAbbreviation
        {
            get
            {
                return _geographyAbbreviation;
            }
            set
            {
                _geographyAbbreviation = value;
            }
        }

        private string _geographyName;
        public string GeographyName
        {
            get
            {
                return _geographyName;
            }
            set
            {
                _geographyName = value;
            }
        }

        private string _geographySetName;
        public string GeographySetName
        {
            get
            {
                return _geographySetName;
            }
            set
            {
                _geographySetName = value;
            }
        }

        private string _geographySetAbbreviation;
        public string GeographySetAbbreviation
        {
            get
            {
                return _geographySetAbbreviation;
            }
            set
            {
                _geographySetAbbreviation = value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return GeographyName + "<->" + GeographySetName + " [" + DateRange.ToString() + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return GeographySetMemberKey;
            }
            set
            {
                GeographySetMemberKey = value;
            }
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }



        // additional properties
        public int RegionID
        {
            get;
            set;
        }

        public int NationISO
        {
            get;
            set;
        }

        public int StateFIPS
        {
            get;
            set;
        }

        public int CountyFIPS
        {
            get;
            set;
        }

        public string GeographyID
        {
            get;
            set;
        }

        public string GeographySuffix
        {
            get;
            set;
        }

        public string GeographyNameSuffix
        {
            get;
            set;
        }

        public int StateVertex
        {
            get;
            set;
        }

        public int CountyVertex
        {
            get;
            set;
        }

        public int? CensusRegionNumber
        {
            get;
            set;
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("GeographySetMemberKey", "GeographySetMemberKey", "Key for this geography set member", "Geography set member key");
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyKey", "GeographyKey", "Key for the geography", "Geography key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographySetKey", "GeographySetKey", "Key for the geography set", "Geography set key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyName", "", "Name of the geography", "Geography name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyAbbreviation", "GeographyAbbrev", "Abbreviation for the geography", "Geography abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographySetName", "GeographySetName", "Name of the geography set", "Geography set name");
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographySetAbbreviation", "GeographySetAbbrev", "Abbreviation for the geography set", "Geography set abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);


            // additional properties
            bp = new BrowsableProperty("RegionID", "RegionID", "Region identifier", "Region identifier");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("NationISO", "NationISO", "Nation ISO", "Nation ISO");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("StateFIPS", "StateFIPS", "State FIPS code", "State FIPS code");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CountyFIPS", "CountyFIPS", "County FIPS code", "County FIPS code");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyID", "GeoID", "Geography identifier", "Geographic area identifier");
            bp.IsReadonly = true;
            bp.MaxStringLength = 16;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographySuffix", "GeoSuffix", "Geography suffix", "Geography suffix");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographyNameSuffix", "GeoNameSuffix", "Geography name suffix", "Geography name suffix");
            bp.IsReadonly = true;
            bp.MaxStringLength = 94;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("StateVertex", "StateVertex", "State vertex", "State vertex");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CountyVertex", "CountyVertex", "County vertex", "County vertex");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CensusRegionNumber", "CensusRegionNo", "Census region number", "Census region number");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region IMember Members

        public int ItemKey
        {
            get
            {
                return GeographyKey;
            }
            set
            {
                GeographyKey = value;
            }
        }

        public int SetKey
        {
            get
            {
                return GeographySetKey;
            }
            set
            {
                GeographySetKey = value;
            }
        }

        public string ItemAbbreviation
        {
            get
            {
                return GeographyAbbreviation;
            }
            set
            {
                GeographyAbbreviation = value;
            }
        }

        public string ItemName
        {
            get
            {
                return GeographyName;
            }
            set
            {
                GeographyName = value;
            }
        }

        public string SetAbbreviation
        {
            get
            {
                return GeographySetAbbreviation;
            }
            set
            {
                GeographySetAbbreviation = value;
            }
        }

        public string SetName
        {
            get
            {
                return GeographySetName;
            }
            set
            {
                GeographySetName = value;
            }
        }

        #endregion


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
