﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class Group : HaiBusinessObjectBase        // UNDERLYING TABLE: DGroup
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public Group(): base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Group;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            GroupAbbreviation = string.Empty;
            GroupName = string.Empty;
            CouncilName = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
	        get 
	        {
                return GroupName + " [" + GroupAbbreviation + "]";
	        }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return GroupKey;
            }
            set
            {
                GroupKey = value;
            }
        }

        private int _groupKey;
        public int GroupKey
        {
            get
            {
                return _groupKey;
            }
            set
            {
                _groupKey = value;
            }
        }

        private string _groupName;
        public string GroupName
        {
            get
            {
                return _groupName;
            }
            set
            {
                _groupName = value;
            }
        }

        private string _groupAbbreviation;
        public string GroupAbbreviation
        {
            get
            {
                return _groupAbbreviation;
            }
            set
            {
                _groupAbbreviation = value;
            }
        }


        // Additional properties

        private int _councilKey;
        public int CouncilKey
        {
            get
            {
                return _councilKey;
            }
            set
            {
                _councilKey = value;
            }
        }

        public string CouncilName
        {
            get;
            set;
        }

        private DateTime _revisionReleaseDate;
        public DateTime RevisionReleaseDate
        {
            get
            {
                return _revisionReleaseDate;
            }
            set
            {
                _revisionReleaseDate = value;
            }
        }

        private DateTime _previewRevisionReleaseDate;
        public DateTime PreviewRevisionReleaseDate
        {
            get
            {
                return _previewRevisionReleaseDate;
            }
            set
            {
                _previewRevisionReleaseDate = value;
            }
        }


        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("GroupKey", "GroupKey", "Key for the group", "Group key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GroupName", "GroupName", "Name of the group", "Group name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GroupAbbreviation", "GroupAbbrev", "Abbreviation for the group", "Group abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CouncilKey", "CouncilKey", "Key for council", "Key for the council");
            bp.MustBeUnique =false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CouncilName", "CouncilName", "Council name", "Name of the council");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("RevisionReleaseDate", "RevisionReleaseDate", "Revision release date", "Revision release date");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("PreviewRevisionReleaseDate", "PreviewRevisionReleaseDate", "Preview revision release date", "Preview revision release date");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
