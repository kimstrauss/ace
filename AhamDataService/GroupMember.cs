﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class GroupMember : HaiBusinessObjectBase        // UNDERLYING TABLE: DMGroup
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public GroupMember() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.GroupMember;

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();
        }

        private int _groupMemberKey;
        public int GroupMemberKey
        {
            get
            {
                return _groupMemberKey;
            }
            set
            {
                _groupMemberKey = value;
            }
        }

        private int _groupKey;
        public int GroupKey
        {
            get
            {
                return _groupKey;
            }
            set
            {
                _groupKey = value;
            }
        }

        private int _productKey;
        public int ProductKey
        {
            get
            {
                return _productKey;
            }
            set
            {
                _productKey = value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return "GroupKey(" + GroupKey.ToString() + ") <-> ProductKey(" + ProductKey.ToString() + ")";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return GroupMemberKey;
            }
            set
            {
                GroupMemberKey = value;
            }
        }


        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("GroupMemberKey", "GroupMemberKey", "Key for this group member", "Group member key");
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductKey", "ProductKey", "Key for the product", "Product key");
            bp.IsReadonly = true;
            bp.MustBeUnique = false;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GroupKey", "GroupKey", "Key for this group", "Group key");
            bp.IsReadonly = true;
            bp.MustBeUnique = false;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
