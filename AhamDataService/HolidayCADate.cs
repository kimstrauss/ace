﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class HolidayCADate : HaiBusinessObjectBase        // UNDERLYING TABLE: HolidayCA
    {
        public HolidayCADate() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.HolidayCADate;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            this.Name = "";
        }

        #region Properties

        public int HolidayDateKey
        {
            get;
            set;
        }

        public DateTime DateOfHoliday
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return DateOfHoliday.ToShortDateString();
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return HolidayDateKey;
            }
            set
            {
                HolidayDateKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("DateOfHoliday", "HolidayDate", "Date of the holiday", "Date of the holiday");
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("HolidayDateKey", "HolidayKey", "Key for the holiday row", "Key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Name", "Name", "Name of the holiday", "Holiday name");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 128;
            _browsablePropertyList.Add(bp.PropertyName, bp);
            
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
