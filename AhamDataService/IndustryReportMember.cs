﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class IndustryReportMember : HaiBusinessObjectBase     // UNDERLYING TABLE: (sp) ceRindustryMembersLookup
    {
        public IndustryReportMember() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.IndustryReportMember;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            IndustryReportMemberName = string.Empty;
        }

        #region Properties

        public string IndustryReportMemberName
        {
            get;
            set;
        }

        public int IndustryReportMemberKey
        {
            get;
            set;
        }

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return IndustryReportMemberName;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return IndustryReportMemberKey;
            }
            set
            {
                IndustryReportMemberKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("IndustryReportMemberKey", "RIndustryMemberKey", "Key for the industry report member", "Industry report member key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("IndustryReportMemberName", "IndustryMemberName", "Name of the industry report member", "Industry report member name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
