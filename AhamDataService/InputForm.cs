﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class InputForm : HaiBusinessObjectBase     // UNDERLYING TABLE: DInputForm
    {
        public InputForm() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.InputForm;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            Title = string.Empty;
            Title2 = string.Empty;
        }

        #region Properties

        public int InputFormKey
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Title2
        {
            get;
            set;
        }

        public bool ShowTotals
        {
            get;
            set;
        }

        private string _inputFormName;
        public string InputFormName
        {
            get
            {
                if (_inputFormName == null)
                    _inputFormName = UniqueIdentifier;
                return _inputFormName;
            }
            set
            {
                _inputFormName = value;
            }
        }
        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return (Title + " | " + Title2).Trim();
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return InputFormKey;
            }
            set
            {
                InputFormKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("InputFormKey", "DInputFormKey", "Key for the input form", "Input form key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("InputFormName", "", "Input form name", "Input form name");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Title", "Title", "First title line for the report", "First title line");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Title2", "Title2", "Second title line for the report", "Second title line");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ShowTotals", "ShowTotals", "Show totals?", "Show totals?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
