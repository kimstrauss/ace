﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class Manufacturer : HaiBusinessObjectBase     // UNDERLYING TABLE: DMfg
    {
        public Manufacturer()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Manufacturer;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            this.URL = string.Empty;
            ManufacturerAbbreviation = string.Empty;
            ManufacturerName = string.Empty;
        }

        #region Properties

        public int ManufacturerKey
        {
            get;
            set;
        }

        public string ManufacturerName
        {
            get;
            set;
        }

        public string ManufacturerAbbreviation
        {
            get;
            set;
        }

        public string URL
        {
            get;
            set;
        }

        public bool Billable
        {
            get;
            set;
        }

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return ManufacturerName + " [" + ManufacturerAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ManufacturerKey;
            }
            set
            {
                ManufacturerKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ManufacturerKey", "ManufacturerKey", "Key for the manufacturer", "Manufacturer key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ManufacturerName", "ManufacturerName", "Name of the manufacturer", "Manufacturer name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ManufacturerAbbreviation", "ManufacturerAbbrev", "Abbreviation for the manufacturer", "Manufacturer abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("URL", "URL", "Website URL", "URL");
            bp.MustBeUnique = false;
            bp.MaxStringLength = 128;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Billable", "Billable", "Is this manufacturer billable?", "Is billable?");
            bp.DataType= TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
