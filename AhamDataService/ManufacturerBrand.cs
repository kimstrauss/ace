﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ManufacturerBrand : HaiBusinessObjectBase, IDateRange     // UNDERLYING TABLE: MfgBrands
    {
        public ManufacturerBrand() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ManufacturerBrand;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ReportingManufacturerName = string.Empty;
            ProductName = string.Empty;
            MarketName = string.Empty;
            BrandName = string.Empty;
            FromManufacturerName = string.Empty;
            ToManufacturerName = string.Empty;

            // Initializations for IDateRange implementation
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());
            if (_kindredPropertyNames == null)
            {
                _kindredPropertyNames = new string[0] { };   // no kindreds
            }
        }

        #region Properties

        public int ManufacturerBrandKey
        {
            get;
            set;
        }

        public int ReportingManufacturerKey
        {
            get;
            set;
        }

        public string ReportingManufacturerName
        {
            get;
            set;
        }

        public int ProductKey  
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public int MarketKey
        {
            get;
            set;
        }

        public string MarketName
        {
            get;
            set;
        }

        public string BrandName
        {
            get;
            set;
        }

        public int? FromManufacturerKey
        {
            get;
            set;
        }

        public string FromManufacturerName
        {
            get;
            set;
        }

        public int? ToManufacturerKey
        {
            get;
            set;
        }

        public string ToManufacturerName
        {
            get;
            set;
        }

        public bool Included
        {
            get;
            set;
        }

        public bool DisplayBrand
        {
            get;
            set;
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        private static string[] _kindredPropertyNames;
        [Browsable(false)]
        public string[] KindredPropertyNames
        {
            get
            {
                return _kindredPropertyNames;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ManufacturerBrandKey;
            }
            set
            {
                ManufacturerBrandKey = value;
            }
        }

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "ManufacturerBrandKey = " + ManufacturerBrandKey.ToString();
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ManufacturerBrandKey", "mfgBrandKey", "Key for the brand", "Key for the brand");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ReportingManufacturerKey", "ReportingMfgKey", "Key for the reporting manufacturer", "Reporting manufacturer key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ReportingManufacturerName", "", "Name of the reporting manufacturer", "Reporting manufacturer name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductKey", "ProductKey", "Key for the product", "Product key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductName", "", "Name of the product", "Product name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketKey", "MarketKey", "Key for the market", "Market key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketName", "", "Name of the market", "Market name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BrandName", "BrandName", "Name of the brand", "Brand name");
            bp.MaxStringLength = 250;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("FromManufacturerKey", "FromMfgKey", "Key for the source manufacturer", "Source manufacturer key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("FromManufacturerName", "", "Name of the source manufacturer", "Source manufacturer name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ToManufacturerKey", "ToMfgKey", "Key for the destination manufacturer", "Destination manufacturer key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ToManufacturerName", "", "Name of the destination manufacturer", "Destination manufacturer name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Included", "Included", "Is included?", "Is included?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("DisplayBrand", "DisplayBrand", "Display brand?", "Display brand?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
