﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ManufacturerPLog : HaiBusinessObjectBase        // UNDERLYING TABLE: (sp) ceMfgPlogLookup()
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ManufacturerPLog() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ManufacturerPLog;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            StatusName = string.Empty;
        }

        public int ManufacturerPLogKey
        {
            get;
            set;
        }

        public bool HasData
        {
            get;
            set;
        }

        public string StatusName
        {
            get;
            set;
        }

        public DateTime EndDate
        {
            get;
            set;
        }

        public DateTime? PostPeriod
        {
            get;
            set;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "ManufacturerPLogKey = " + ManufacturerPLogKey.ToString();
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ManufacturerPLogKey;
            }
            set
            {
                ManufacturerPLogKey = value;
            }
        }
        
        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ManufacturerPLogKey", "MfgPlogKey", "Key for the PLog", "PLog key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("StatusName", "", "Status name", "Status name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("PostPeriod", "PostPeriod", "Post period", "Post period");
            bp.DataType = TypeCode.DateTime;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("HasData", "", "Has data?", "Contains data?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region Geneic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
