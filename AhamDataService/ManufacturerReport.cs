﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ManufacturerReport : HaiBusinessObjectBase, IDateRange     // UNDERLYING TABLE: RMfg
    {
        public ManufacturerReport() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ManufacturerReport;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ManufacturerName = string.Empty;
            ProductName = string.Empty;
            FrequencyName = string.Empty;
            ActivitySetName = string.Empty;
            GeographySetName = string.Empty;
            UseSetName = string.Empty;
            ChannelSetName = string.Empty;
            MeasureSetName = string.Empty;
            MarketSetName = string.Empty;
            CatalogSetName = string.Empty;
            ReportName = string.Empty;
            SizeSetName = string.Empty;
            InputFormName = string.Empty;
            Status = "A";

            // Initializations for IDateRange implementation
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());
            if (_kindredPropertyNames == null)
            {
                _kindredPropertyNames = new string[11] { "ManufacturerKey", "ActivitySetKey", "CatalogSetKey", "ChannelSetKey", 
                    "FrequencyKey", "GeographySetKey", "MeasureSetKey", "ProductKey", "UseSetKey", "MarketSetKey", "SizeSetKey" };
            }
        }

        #region Properties

        public int ManufacturerReportKey
        {
            get;
            set;
        }

        public int ManufacturerKey
        {
            get;
            set;
        }

        public string ManufacturerName
        {
            get;
            set;
        }

        public int ProductKey
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public int FrequencyKey
        {
            get;
            set;
        }

        public string FrequencyName
        {
            get;
            set;
        }

        public int ActivitySetKey
        {
            get;
            set;
        }

        public string ActivitySetName
        {
            get;
            set;
        }

        public int GeographySetKey
        {
            get;
            set;
        }

        public string GeographySetName
        {
            get;
            set;
        }

        public int UseSetKey
        {
            get;
            set;
        }

        public string UseSetName
        {
            get;
            set;
        }

        public int ChannelSetKey
        {
            get;
            set;
        }

        public string ChannelSetName
        {
            get;
            set;
        }

        public int MeasureSetKey
        {
            get;
            set;
        }

        public string MeasureSetName
        {
            get;
            set;
        }

        public int MarketSetKey
        {
            get;
            set;
        }

        public string MarketSetName
        {
            get;
            set;
        }

        public int CatalogSetKey
        {
            get;
            set;
        }

        public string CatalogSetName
        {
            get;
            set;
        }

        public int SizeSetKey
        {
            get;
            set;
        }

        public string SizeSetName
        {
            get;
            set;
        }

        public int InputFormKey
        {
            get;
            set;
        }

        public string InputFormName
        {
            get;
            set;
        }


        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        private static string[] _kindredPropertyNames;
        [Browsable(false)]
        public string[] KindredPropertyNames
        {
            get
            {
                return _kindredPropertyNames;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ManufacturerReportKey;
            }
            set
            {
                ManufacturerReportKey = value;
            }
        }

        public string Status
        {
            get;
            set;
        }

        public bool SerialNumber
        {
            get;
            set;
        }

        public string ReportName
        {
            get;
            set;
        }

        public bool YTD
        {
            get;
            set;
        }

        public bool Master
        {
            get;
            set;
        }

        public bool CreateWeekly
        {
            get;
            set;
        }

        public bool Reconcile
        {
            get;
            set;
        }

        public bool Calendar
        {
            get;
            set;
        }

        public bool Forecasted
        {
            get;
            set;
        }

        public bool ParticipationChecked
        {
            get;
            set;
        }

        public bool SkipReportCard
        {
            get;
            set;
        }
        #endregion


        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "ManufacturerReportKey: " + ManufacturerReportKey.ToString();
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ManufacturerReportKey", "ManufacturerReportKey", "Key for the manufacturer report", "Manufacturer report key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ManufacturerKey", "MfgKey", "Key for the manufacturer", "Manufacturer key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ManufacturerName", "", "Name of the manufacturer", "Manufacturer name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductKey", "ProductKey", "Key for the product", "Product key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductName", "", "Name of the product", "Product name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("FrequencyKey", "FreqKey", "Frequency key", "Frequency key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("FrequencyName", "", "Name of the frequency", "Frequency name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivitySetKey", "ActivitySetKey", "Key for the Activity Set", "Activity Set key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivitySetName", "", "Name of the Activity Set", "Activity Set name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographySetKey", "GeoSetKey", "Key for the Geography Set", "Geography Set key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GeographySetName", "", "Name of the Geography Set", "Geography Set name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseSetKey", "UseSetKey", "Key for the Use Set", "Use Set key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseSetName", "", "Name of the Use Set", "Use Set name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelSetKey", "ChannelSetKey", "Key for the Channel Set", "Channel Set key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelSetName", "", "Name of the Channel Set", "Channel Set name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketSetKey", "MarketSetKey", "Key for the Market Set", "Market Set key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketSetName", "", "Name of the Market Set", "Market Set name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureSetKey", "MeasureSetKey", "Key for the Measure Set", "Measure Set key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureSetName", "", "Name of the Measure Set", "Measure Set name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogSetKey", "CatalogSetKey", "Key for the Catalog Set", "Catalog Set key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogSetName", "", "Name of the Catalog Set", "Catalog Set name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeSetKey", "SizeSetKey", "Key for the Size Set", "Size Set key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeSetName", "", "Name of the Size Set", "Size Set name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("InputFormKey", "InputFormKey", "Key for the Input Form", "Input Form key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("InputFormName", "", "Name of the Input Form", "Input Form name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Status", "Status", "Status code", "Status code");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 4;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SerialNumber", "SerialNo", "Serial number?", "Serial number?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ReportName", "RptName", "Report name", "Report name");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 128;
            bp.MustBeUnique = false;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("YTD", "ytd", "Year to date?", "Year to date?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Master", "master", "Master?", "Master?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CreateWeekly", "CreateWeekly", "Create weekly?", "Create weekly?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Reconcile", "reconcile", "Reconcile?", "Reconcile?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Calendar", "Calendar", "Calendar?", "Calendar?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Forecasted", "forecasted", "Forecasted?", "Forecasted?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ParticipationChecked", "ParticipationChecked", "Participation checked?", "Paticipation checked?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SkipReportCard", "SkipReportCard", "Skip Report Card?", "Skip Report Card?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
