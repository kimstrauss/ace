﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class Market : HaiBusinessObjectBase, IItem     // UNDERLYING TABLE: DMarket
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public Market(): base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Market;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            MarketAbbreviation = string.Empty;
            MarketName = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return MarketName + " [" + MarketAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return MarketKey;
            }
            set
            {
                MarketKey = value;
            }
        }

        private int _marketKey;
        public int MarketKey
        {
            get
            {
                return _marketKey;
            }
            set
            {
                _marketKey = value;
            }
        }

        private string _marketName;
        public string MarketName
        {
            get
            {
                return _marketName;
            }
            set
            {
                _marketName = value;
            }
        }

        private string _marketAbbreviation;
        public string MarketAbbreviation
        {
            get
            {
                return _marketAbbreviation;
            }
            set
            {
                _marketAbbreviation = value;
            }
        }

        #region IItem Members

        [Browsable(false)]
        public int ItemKey
        {
            get
            {
                return MarketKey;
            }
        }

        [Browsable(false)]
        public string ItemName
        {
            get
            {
                return MarketName;
            }
        }

        [Browsable(false)]
        public string ItemAbbreviation
        {
            get
            {
                return MarketAbbreviation;
            }
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("MarketKey", "MarketKey", "Key for the market", "Market key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketName", "MarketName", "Name of the market", "Market name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketAbbreviation", "MarketAbbrev", "Abbreviation for the market", "Market abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
