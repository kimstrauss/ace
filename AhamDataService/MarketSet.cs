﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class MarketSet : HaiBusinessObjectBase, ISet     // UNDERLYING TABLE: SDMarket
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public MarketSet()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.MarketSet;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            MarketSetAbbreviation = string.Empty;
            MarketSetName = string.Empty;
        }

        #region Properties
        private int _marketSetKey;
        public int MarketSetKey
        {
            get
            {
                return _marketSetKey;
            }
            set
            {
                _marketSetKey = value;
            }
        }

        private string _marketSetName;
        public string MarketSetName
        {
            get
            {
                return _marketSetName;
            }
            set
            {
                _marketSetName = value;
            }
        }

        private string _marketSetAbbreviation;
        public string MarketSetAbbreviation
        {
            get
            {
                return _marketSetAbbreviation;
            }
            set
            {
                _marketSetAbbreviation = value;
            }
        }
        #endregion

        public override string UniqueIdentifier
        {
            get
            {
                return MarketSetName + " [" + MarketSetAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return MarketSetKey;
            }
            set
            {
                MarketSetKey = value;
            }
        }

        #region ISet Members

        [Browsable(false)]
        int ISet.SetKey
        {
            get
            {
                return MarketSetKey;
            }
        }

        [Browsable(false)]
        string ISet.SetName
        {
            get
            {
                return MarketSetName;
            }
        }

        [Browsable(false)]
        string ISet.SetAbbreviation
        {
            get
            {
                return MarketSetAbbreviation;
            }
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("MarketSetKey", "MarketSetKey", "Key for the market set", "Market set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketSetName", "MarketSetName", "Name of the market set", "Market set name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketSetAbbreviation", "MarketSetAbbrev", "Abbreviation for the market set", "Market set abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
