﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class MarketSetMember : HaiBusinessObjectBase, IMember     // UNDERLYING TABLE: SMMarket
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public MarketSetMember()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.MarketSetMember;
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();
        }

        private int _marketSetMemberKey;
        public int MarketSetMemberKey
        {
            get
            {
                return _marketSetMemberKey;
            }
            set
            {
                _marketSetMemberKey = value;
            }
        }

        private int _marketKey;
        public int MarketKey
        {
            get
            {
                return _marketKey;
            }
            set
            {
                _marketKey = value;
            }
        }

        private int _marketSetKey;
        public int MarketSetKey
        {
            get
            {
                return _marketSetKey;
            }
            set
            {
                _marketSetKey = value;
            }
        }

        private string _marketAbbreviation;
        public string MarketAbbreviation
        {
            get
            {
                return _marketAbbreviation;
            }
            set
            {
                _marketAbbreviation = value;
            }
        }

        private string _marketName;
        public string MarketName
        {
            get
            {
                return _marketName;
            }
            set
            {
                _marketName = value;
            }
        }

        private string _marketSetName;
        public string MarketSetName
        {
            get
            {
                return _marketSetName;
            }
            set
            {
                _marketSetName = value;
            }
        }

        private string _marketSetAbbreviation;
        public string MarketSetAbbreviation
        {
            get
            {
                return _marketSetAbbreviation;
            }
            set
            {
                _marketSetAbbreviation = value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return MarketName + "<->" + MarketSetName + " [" + DateRange.ToString() + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return MarketSetMemberKey;
            }
            set
            {
                MarketSetMemberKey = value;
            }
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("MarketSetMemberKey", "MarketSetMemberKey", "Key for this market set member", "Market set member key");
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketKey", "MarketKey", "Key for the market", "Market key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketSetKey", "MarketSetKey", "Key for the market set", "Market set key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketName", "", "Name of the market", "Market name");
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketAbbreviation", "MarketAbbrev", "Abbreviation for the market", "Market abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketSetName", "MarketSetName", "Name of the market set", "Market set name");
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketSetAbbreviation", "MarketSetAbbrev", "Abbreviation for the market set", "Market set abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region IItem Members

        public int ItemKey
        {
            get
            {
                return MarketKey;
            }
            set
            {
                MarketKey = value;
            }
        }

        public int SetKey
        {
            get
            {
                return MarketSetKey;
            }
            set
            {
                MarketSetKey = value;
            }
        }

        public string ItemAbbreviation
        {
            get
            {
                return MarketAbbreviation;
            }
            set
            {
                MarketAbbreviation = value;
            }
        }

        public string ItemName
        {
            get
            {
                return MarketName;
            }
            set
            {
                MarketName = value;
            }
        }

        public string SetAbbreviation
        {
            get
            {
                return MarketSetAbbreviation;
            }
            set
            {
                MarketSetAbbreviation = value;
            }
        }

        public string SetName
        {
            get
            {
                return MarketSetName;
            }
            set
            {
                MarketSetName = value;
            }
        }

        #endregion


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
