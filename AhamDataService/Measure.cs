﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class Measure : HaiBusinessObjectBase, IItem     // UNDERLYING TABLE: DMeasure
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public Measure()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Measure;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            MeasureAbbreviation = string.Empty;
            MeasureName = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return MeasureName + " [" + MeasureAbbreviation + "]";
            }
        }

        private int _measureKey;
        public int MeasureKey
        {
            get
            {
                return _measureKey;
            }
            set
            {
                _measureKey = value;
            }
        }

        private string _measureName;
        public string MeasureName
        {
            get
            {
                return _measureName;
            }
            set
            {
                _measureName = value;
            }
        }

        private string _measureAbbreviation;
        public string MeasureAbbreviation
        {
            get
            {
                return _measureAbbreviation;
            }
            set
            {
                _measureAbbreviation = value;
            }
        }

        #region IItem Members

        [Browsable(false)]
        public int ItemKey
        {
            get
            {
                return MeasureKey;
            }
        }

        [Browsable(false)]
        public string ItemName
        {
            get
            {
                return MeasureName;
            }
        }

        [Browsable(false)]
        public string ItemAbbreviation
        {
            get
            {
                return MeasureAbbreviation;
            }
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("MeasureKey", "MeasureKey", "Key for the measure", "Measure key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureName", "MeasureName", "Name of the measure", "Measure name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureAbbreviation", "MeasureAbbrev", "Abbreviation for the measure", "Measure abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return MeasureKey;
            }
            set
            {
                MeasureKey = value;
            }
        }


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
