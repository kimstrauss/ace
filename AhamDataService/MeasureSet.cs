﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class MeasureSet : HaiBusinessObjectBase, ISet     // UNDERLYING TABLE: SDMeasure
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public MeasureSet()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.MeasureSet;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            MeasureSetAbbreviation = string.Empty;
            MeasureSetName = string.Empty;
        }

        #region Properties
        private int _measureSetKey;
        public int MeasureSetKey
        {
            get
            {
                return _measureSetKey;
            }
            set
            {
                _measureSetKey = value;
            }
        }

        private string _measureSetName;
        public string MeasureSetName
        {
            get
            {
                return _measureSetName;
            }
            set
            {
                _measureSetName = value;
            }
        }

        private string _measureSetAbbreviation;
        public string MeasureSetAbbreviation
        {
            get
            {
                return _measureSetAbbreviation;
            }
            set
            {
                _measureSetAbbreviation = value;
            }
        }
        #endregion

        public override string UniqueIdentifier
        {
            get
            {
                return MeasureSetName + " [" + MeasureSetAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return MeasureSetKey;
            }
            set
            {
                MeasureSetKey = value;
            }
        }

        #region ISet Members

        [Browsable(false)]
        int ISet.SetKey
        {
            get
            {
                return MeasureSetKey;
            }
        }

        [Browsable(false)]
        string ISet.SetName
        {
            get
            {
                return MeasureSetName;
            }
        }

        [Browsable(false)]
        string ISet.SetAbbreviation
        {
            get
            {
                return MeasureSetAbbreviation;
            }
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("MeasureSetKey", "MeasureSetKey", "Key for the measure set", "Measure set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureSetName", "MeasureSetName", "Name of the measure set", "Measure set name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureSetAbbreviation", "MeasureSetAbbrev", "Abbreviation for the measure set", "Measure set abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
