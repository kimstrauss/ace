﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class MeasureSetMember : HaiBusinessObjectBase, IMember     // UNDERLYING TABLE: SMMeasure
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public MeasureSetMember()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.MeasureSetMember;
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();
        }

        private int _measureSetMemberKey;
        public int MeasureSetMemberKey
        {
            get
            {
                return _measureSetMemberKey;
            }
            set
            {
                _measureSetMemberKey = value;
            }
        }

        private int _measureKey;
        public int MeasureKey
        {
            get
            {
                return _measureKey;
            }
            set
            {
                _measureKey = value;
            }
        }

        private int _measureSetKey;
        public int MeasureSetKey
        {
            get
            {
                return _measureSetKey;
            }
            set
            {
                _measureSetKey = value;
            }
        }

        private string _measureAbbreviation;
        public string MeasureAbbreviation
        {
            get
            {
                return _measureAbbreviation;
            }
            set
            {
                _measureAbbreviation = value;
            }
        }

        private string _measureName;
        public string MeasureName
        {
            get
            {
                return _measureName;
            }
            set
            {
                _measureName = value;
            }
        }

        private string _measureSetName;
        public string MeasureSetName
        {
            get
            {
                return _measureSetName;
            }
            set
            {
                _measureSetName = value;
            }
        }

        private string _measureSetAbbreviation;
        public string MeasureSetAbbreviation
        {
            get
            {
                return _measureSetAbbreviation;
            }
            set
            {
                _measureSetAbbreviation = value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return MeasureName + "<->" + MeasureSetName + " [" + DateRange.ToString() + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return MeasureSetMemberKey;
            }
            set
            {
                MeasureSetMemberKey = value;
            }
        }


        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }


        // Additional properties

        public int GroupNumber
        {
            get;
            set;
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("MeasureSetMemberKey", "MeasureSetMemberKey", "Key for this measure set member", "Measure set member key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureKey", "MeasureKey", "Key for the measure", "Measure key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureSetKey", "MeasureSetKey", "Key for the measure set", "Measure set key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureName", "", "Name of the measure", "Measure name");
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureAbbreviation", "MeasureAbbrev", "Abbreviation for the measure", "Measure abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureSetName", "MeasureSetName", "Name of the measure set", "Measure set name");
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureSetAbbreviation", "MeasureSetAbbrev", "Abbreviation for the measure set", "Measure set abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);


            // Additional properties

            bp = new BrowsableProperty("GroupNumber", "GroupNo", "Group number", "Group number");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region IItem Members

        public int ItemKey
        {
            get
            {
                return MeasureKey;
            }
            set
            {
                MeasureKey = value;
            }
        }

        public int SetKey
        {
            get
            {
                return MeasureSetKey;
            }
            set
            {
                MeasureSetKey = value;
            }
        }

        public string ItemAbbreviation
        {
            get
            {
                return MeasureAbbreviation;
            }
            set
            {
                MeasureAbbreviation = value;
            }
        }

        public string ItemName
        {
            get
            {
                return MeasureName;
            }
            set
            {
                MeasureName = value;
            }
        }

        public string SetAbbreviation
        {
            get
            {
                return MeasureSetAbbreviation;
            }
            set
            {
                MeasureSetAbbreviation = value;
            }
        }

        public string SetName
        {
            get
            {
                return MeasureSetName;
            }
            set
            {
                MeasureSetName = value;
            }
        }

        #endregion

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }


        #region Generic code

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }


        #endregion
    }
}
