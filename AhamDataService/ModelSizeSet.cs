﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ModelSizeSet : HaiBusinessObjectBase, ISet     // UNDERLYING TABLE: SDPSize
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ModelSizeSet() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ModelSizeSet;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ModelSizeSetName = string.Empty;
            ProductDimensionName = string.Empty;
            MarketName = string.Empty;
        }

        #region Properties

        public int ModelSizeSetKey
        {
            get;
            set;
        }

        public string ModelSizeSetName
        {
            get;
            set;
        }

        public int ProductDimensionKey
        {
            get;
            set;
        }

        public string ProductDimensionName
        {
            get;
            set;
        }

        public int MarketKey
        {
            get;
            set;
        }

        public string MarketName
        {
            get;
            set;
        }

        public int DimKey
        {
            get;
            set;
        }

        #endregion

        public override string UniqueIdentifier
        {
            get
            {
                return ModelSizeSetName;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ModelSizeSetKey;
            }
            set
            {
                ModelSizeSetKey = value;
            }
        }

        #region ISet Members

        [Browsable(false)]
        public int SetKey
        {
            get
            {
                return ModelSizeSetKey;
            }
        }

        [Browsable(false)]
        public string SetName
        {
            get
            {
                return ModelSizeSetName;
            }
        }

        [Browsable(false)]
        public string SetAbbreviation
        {
            get
            {
                return ModelSizeSetName;
            }
        }

        #endregion

        #region Generic Code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ModelSizeSetKey", "SizeSetPKey", "Key for the model size set", "Model size set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ModelSizeSetName", "SizeSetName", "Name of the model size set", "Model size set name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 128;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductDimensionKey", "ProductDimKey", "Key for the product dimension", "Product dimension key");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductDimensionName", "", "Name of the product dimension", "Product dimension name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketKey", "MarketKey", "Key for the market", "Market key");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketName", "", "Name of the market", "Market name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("DimKey", "", "Key for the dimension", "Dimension key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
