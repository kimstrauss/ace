﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ModelSizeSetMember : HaiBusinessObjectBase, IMember        // UNDERLYING TABLE: SMPSize
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;


        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ModelSizeSetMember() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ModelSizeSetMember;
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ModelSizeSetMemberName = string.Empty;
            SizeName = string.Empty;
            ModelSizeSetName = string.Empty;
            ParentSizeName = string.Empty;
        }

        public int ModelSizeSetMemberKey
        {
            get;
            set;
        }

        public string ModelSizeSetMemberName
        {
            get;
            set;
        }

        public int SizeKey
        {
            get;
            set;
        }

        public string SizeName
        {
            get;
            set;
        }

        public int ModelSizeSetKey
        {
            get;
            set;
        }

        public string ModelSizeSetName
        {
            get;
            set;
        }

        public override string UniqueIdentifier
        {
            get
            {
                return SizeName + "<->" + ModelSizeSetName + " [" + DateRange.ToString() + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ModelSizeSetMemberKey;
            }
            set
            {
                ModelSizeSetMemberKey = value;
            }
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }



        // additional properties

        public int? ParentSizeKey
        {
            get;
            set;
        }

        public string ParentSizeName
        {
            get;
            set;
        }

        public int SizeLevel
        {
            get;
            set;
        }


        #region IMember Members

        public int ItemKey
        {
            get
            {
                return SizeKey;
            }
            set
            {
                SizeKey = value;
            }
        }

        public int SetKey
        {
            get
            {
                return ModelSizeSetKey;
            }
            set
            {
                ModelSizeSetKey = value;
            }
        }

        public string ItemAbbreviation
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public string ItemName
        {
            get
            {
                return SizeName;
            }
            set
            {
                SizeName = value;
            }
        }

        public string SetAbbreviation
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public string SetName
        {
            get
            {
                return ModelSizeSetName;
            }
            set
            {
                ModelSizeSetName = value;
            }
        }

        #endregion


        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ModelSizeSetMemberKey", "SizeSetPKey", "Key for this model size set member", "Model size set member key");
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ModelSizeSetMemberName", "", "Name for this model size set member", "Model size set member name");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeKey", "SizeKey", "Key for the size", "Size key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeName", "", "Name of the size", "Size name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ModelSizeSetKey", "ModelSizeSetKey", "Key for the model size set", "Model size set key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ModelSizeSetName", "ModelSizeSetName", "Name of the model size set", "Model size set name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ParentSizeKey", "ParentSizeKey", "Parent size key", "Parent size key");
            bp.DataType = TypeCode.Int32;
            bp.IsNullable=true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ParentSizeName", "", "Parent size name", "Parent size name");
            bp.DataType = TypeCode.String;
            bp.IsNullable = false;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeLevel", "SizeLevel", "Size level", "Size level");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
