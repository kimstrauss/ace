﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class Product : HaiBusinessObjectBase, IItem     // UNDERLYING TABLE: DProduct
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public Product() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Product_Aham;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ProductName = string.Empty;
            GroupName = string.Empty;
            ProductOwnerName = string.Empty;
            ProductAbbreviation = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return ProductName + " [" + ProductAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ProductKey;
            }
            set
            {
                ProductKey = value;
            }
        }

        private int _productKey;
        public int ProductKey
        {
            get
            {
                return _productKey;
            }
            set
            {
                _productKey = value;
            }
        }

        private string _productName;
        public string ProductName
        {
            get
            {
                return _productName;
            }
            set
            {
                _productName = value;
            }
        }

        private string _productAbbreviation;
        public string ProductAbbreviation
        {
            get
            {
                return _productAbbreviation;
            }
            set
            {
                _productAbbreviation = value;
            }
        }


        // Additional properties

        public int? ProductOwnerKey
        {
            get;
            set;
        }

        public string ProductOwnerName
        {
            get;
            set;
        }

        public int? GroupMemberKey
        {
            get;
            set;
        }

        public int? GroupKey
        {
            get;
            set;
        }

        private string _groupName;
        public string GroupName
        {
            get
            {
                return _groupName;
            }
            set
            {
                _groupName = value;
            }
        }

        #region IItem Members

        [Browsable(false)]
        public int ItemKey
        {
            get
            {
                return ProductKey;
            }
        }

        [Browsable(false)]
        public string ItemName
        {
            get
            {
                return ProductName;
            }
        }

        [Browsable(false)]
        public string ItemAbbreviation
        {
            get
            {
                return ProductAbbreviation;
            }
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ProductKey", "ProductKey", "Key for the product", "Product key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductName", "ProductName", "Name of the product", "Product name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductAbbreviation", "ProductAbbrev", "Abbreviation for the product", "Product abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductOwnerKey", "ProductOwnerKey", "Key for the product owner", "Product owner key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductOwnerName", "", "Name of the owning product", "Name of the product that owns this product");
            bp.MustBeUnique = false;
            bp.IsNullable = false;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GroupName", "", "Name of the owning group", "Name of the group that owns this product");
            bp.MustBeUnique = false;
            bp.IsNullable = false;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GroupKey", "", "Key for the group owner", "Group owner key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("GroupMemberKey", "", "Key for the group member", "Group member key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

        }


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
