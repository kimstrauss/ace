﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ProductMarket : HaiBusinessObjectBase, IDateRange        // UNDERLYING TABLE: ProductMarket
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ProductMarket() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ProductMarket;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ProductName = string.Empty;
            MarketName = string.Empty;
            ChannelName = string.Empty;

            // Initializations for IDateRange implementation
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());
            if (_kindredPropertyNames == null)
            {
                _kindredPropertyNames = new string[3] { "ProductKey", "MarketKey", "ChannelKey" };
            }
        }

        #region Properties

        public int ProductMarketKey
        {
            get;
            set;
        }

        public int ProductKey
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }

        public int MarketKey
        {
            get;
            set;
        }

        public string MarketName
        {
            get;
            set;
        }

        public int ChannelKey
        {
            get;
            set;
        }

        public string ChannelName
        {
            get;
            set;
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        #endregion

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ProductMarketKey;
            }
            set
            {
                ProductMarketKey = value;
            }
        }

        private static string[] _kindredPropertyNames;
        [Browsable(false)]
        public string[] KindredPropertyNames
        {
            get
            {
                return _kindredPropertyNames;
            }
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "[" + MarketName + "].[" + ChannelName + "].[" + ProductName + "]";
            }
        }


        #region Generic code

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ProductMarketKey", "ProductMarketKey", "Key for the product/market", "Product/market key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelKey", "ChannelKey", "Key for the channel", "Key for the channel");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketKey", "MarketKey", "Key for the market", "Key for the market");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductKey", "ProductKey", "Key for the product", "Key for the product");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelName", "ChannelName", "Name of the channel", "Name of the channel");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketName", "MarketName", "Name of the market", "Name of the market");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductName", "ProductName", "Name of the product", "Name of the product");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 64;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
