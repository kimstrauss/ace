﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ProductSet : HaiBusinessObjectBase, ISet     // UNDERLYING TABLE: SDProduct
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ProductSet()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ProductSet_Aham;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ProductSetName = string.Empty;
            ProductSetAbbreviation = string.Empty;
        }

        #region Properties
        private int _productSetKey;
        public int ProductSetKey
        {
            get
            {
                return _productSetKey;
            }
            set
            {
                _productSetKey = value;
            }
        }

        private string _productSetName;
        public string ProductSetName
        {
            get
            {
                return _productSetName;
            }
            set
            {
                _productSetName = value;
            }
        }

        private string _productSetAbbreviation;
        public string ProductSetAbbreviation
        {
            get
            {
                return _productSetAbbreviation;
            }
            set
            {
                _productSetAbbreviation = value;
            }
        }
        #endregion

        public override string UniqueIdentifier
        {
            get
            {
                return ProductSetName + " [" + ProductSetAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ProductSetKey;
            }
            set
            {
                ProductSetKey = value;
            }
        }

        #region ISet Members

        [Browsable(false)]
        int ISet.SetKey
        {
            get
            {
                return ProductSetKey;
            }
        }

        [Browsable(false)]
        string ISet.SetName
        {
            get
            {
                return ProductSetName;
            }
        }

        [Browsable(false)]
        string ISet.SetAbbreviation
        {
            get
            {
                return ProductSetAbbreviation;
            }
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ProductSetKey", "ProductSetKey", "Key for the product set", "Product set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductSetName", "ProductSetName", "Name of the product set", "Product set name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductSetAbbreviation", "ProductSetAbbrev", "Abbreviation for the product set", "Product set abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion

    }
}
