﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ProductSetMember : HaiBusinessObjectBase, IMember     // UNDERLYING TABLE: SMProduct
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public ProductSetMember()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ProductSetMember_Aham;
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();
        }

        private int _productSetMemberKey;
        public int ProductSetMemberKey
        {
            get
            {
                return _productSetMemberKey;
            }
            set
            {
                _productSetMemberKey = value;
            }
        }

        private int _productKey;
        public int ProductKey
        {
            get
            {
                return _productKey;
            }
            set
            {
                _productKey = value;
            }
        }

        private int _productSetKey;
        public int ProductSetKey
        {
            get
            {
                return _productSetKey;
            }
            set
            {
                _productSetKey = value;
            }
        }

        private string _productAbbreviation;
        public string ProductAbbreviation
        {
            get
            {
                return _productAbbreviation;
            }
            set
            {
                _productAbbreviation = value;
            }
        }

        private string _productName;
        public string ProductName
        {
            get
            {
                return _productName;
            }
            set
            {
                _productName = value;
            }
        }

        private string _productSetName;
        public string ProductSetName
        {
            get
            {
                return _productSetName;
            }
            set
            {
                _productSetName = value;
            }
        }

        private string _productSetAbbreviation;
        public string ProductSetAbbreviation
        {
            get
            {
                return _productSetAbbreviation;
            }
            set
            {
                _productSetAbbreviation = value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return ProductName + "<->" + ProductSetName + " [" + DateRange.ToString() + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ProductSetMemberKey;
            }
            set
            {
                ProductSetMemberKey = value;
            }
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }


        // Additional properties

        public int? ProductOwnerKey
        {
            get;
            set;
        }

        public string ProductNameForFlash
        {
            get;
            set;
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ProductSetMemberKey", "ProductSetMemberKey", "Key for this product set member", "Product set member key");
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductKey", "ProductKey", "Key for the product", "Product key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductSetKey", "ProductSetKey", "Key for the product set", "Product set key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductName", "", "Name of the product", "Product name");
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductAbbreviation", "ProductAbbrev", "Abbreviation for the product", "Product abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductSetName", "ProductSetName", "Name of the product set", "Product set name");
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductSetAbbreviation", "ProductSetAbbrev", "Abbreviation for the product set", "Product set abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductOwnerKey", "ProductOwnerKey", "Key for the product owner", "Product owner key");
            bp.IsReadonly = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProductNameForFlash", "ProductNameForFlash", "Name of the product for reports", "Product name for flash");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region IItem Members

        public int ItemKey
        {
            get
            {
                return ProductKey;
            }
            set
            {
                ProductKey = value;
            }
        }

        public int SetKey
        {
            get
            {
                return ProductSetKey;
            }
            set
            {
                ProductSetKey = value;
            }
        }

        public string ItemAbbreviation
        {
            get
            {
                return ProductAbbreviation;
            }
            set
            {
                ProductAbbreviation = value;
            }
        }

        public string ItemName
        {
            get
            {
                return ProductName;
            }
            set
            {
                ProductName = value;
            }
        }

        public string SetAbbreviation
        {
            get
            {
                return ProductSetAbbreviation;
            }
            set
            {
                ProductSetAbbreviation = value;
            }
        }

        public string SetName
        {
            get
            {
                return ProductSetName;
            }
            set
            {
                ProductSetName = value;
            }
        }

        #endregion


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
