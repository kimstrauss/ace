﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class ReleaseException : HaiBusinessObjectBase     // UNDERLYING TABLE: orReleaseExceptions
    {
        public ReleaseException() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ReleaseException;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            OutputReportID = string.Empty;
        }

        #region Properties

        public int ReleaseExceptionKey
        {
            get;
            set;
        }

        public int OutputReportKey
        {
            get;
            set;
        }

        public string OutputReportID
        {
            get;
            set;
        }

        public DateTime ReportEndDate
        {
            get;
            set;
        }

        public bool HideFinal
        {
            get;
            set;
        }

        public bool HidePublic
        {
            get;
            set;
        }

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "Release exception key = " + ReleaseExceptionKey.ToString();
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ReleaseExceptionKey;
            }
            set
            {
                ReleaseExceptionKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ReleaseExceptionKey", "OrReleaseExceptionKey", "Release Exception key", "Release Exception key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("OutputReportKey", "OutputReportKey", "Output Report key", "Output Report key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("OutputReportID", "", "Output Report ID", "Output Report ID");
            bp.MaxStringLength = 0;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ReportEndDate", "ReportEndDate", "Output Report end date", "Output Report end date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("HideFinal", "HideFinal", "Hide final", "Hide final");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("HidePublic", "HidePublic", "Hide public", "Hide public");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
