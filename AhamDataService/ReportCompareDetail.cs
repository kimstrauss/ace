﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ReportCompareDetail : HaiBusinessObjectBase        // UNDERLYING TABLE: rptCompareDetail
    {

        public ReportCompareDetail() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ReportCompareDetail;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ManufacturerReportName = string.Empty;
            Size1Name = string.Empty;
            Size2Name = string.Empty;
            Size3Name = string.Empty;
            Size4Name = string.Empty;
            Size5Name = string.Empty;
            ActivityName = string.Empty;
            CatalogName = string.Empty;
            MeasureName = string.Empty;
            ChannelName = string.Empty;
            UseName = string.Empty; ;
        }

        #region Properties

        public int ReportCompareDetailKey
        {
            get;
            set;
        }

        public int ReportCompareMasterKey
        {
            get;
            set;
        }

        public int ManufacturerReportKey
        {
            get;
            set;
        }

        public string ManufacturerReportName
        {
            get;
            set;
        }

        public int? Size1Key
        {
            get;
            set;
        }

        public string Size1Name
        {
            get;
            set;
        }

        public int? Size2Key
        {
            get;
            set;
        }

        public string Size2Name
        {
            get;
            set;
        }

        public int? Size3Key
        {
            get;
            set;
        }

        public string Size3Name
        {
            get;
            set;
        }

        public int? Size4Key
        {
            get;
            set;
        }

        public string Size4Name
        {
            get;
            set;
        }

        public int? Size5Key
        {
            get;
            set;
        }

        public string Size5Name
        {
            get;
            set;
        }

        public int? ActivityKey
        {
            get;
            set;
        }

        public string ActivityName
        {
            get;
            set;
        }

        public int? CatalogKey
        {
            get;
            set;
        }

        public string CatalogName
        {
            get;
            set;
        }

        public int? MeasureKey
        {
            get;
            set;
        }

        public string MeasureName
        {
            get;
            set;
        }

        public int? ChannelKey
        {
            get;
            set;
        }

        public string ChannelName
        {
            get;
            set;
        }

        public int? UseKey
        {
            get;
            set;
        }

        public string UseName
        {
            get;
            set;
        }

        public bool IsCalendar
        {
            get;
            set;
        }

        public int Side
        {
            get;
            set;
        }



        #endregion

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ReportCompareDetailKey;
            }
            set
            {
                ReportCompareDetailKey = value;
            }
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "ReportCompareDetailKey: " + ReportCompareDetailKey;
            }
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ReportCompareDetailKey", "rptDetailKey", "Key for the report compare detail", "Report compare detail key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ReportCompareMasterKey", "RptCompKey", "Key for the report compare master", "Report compare master key");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ManufacturerReportKey", "mfgreportkey", "Key for the manufacturer report", "Manufacturer report key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ManufacturerReportName", "", "Name of the manufacturer report", "Manufacturer report name");
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size1Key", "size1key", "Key for size 1", "Size 1 key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size1Name", "", "Name of size 1", "Size 1 name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size2Key", "size2key", "Key for size 2", "Size 2 key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size2Name", "", "Name of size 2", "Size 2 name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size3Key", "size3key", "Key for size 3", "Size 3 key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size3Name", "", "Name of size 3", "Size 3 name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size4Key", "size4key", "Key for size 4", "Size 4 key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size4Name", "", "Name of size 4", "Size 4 name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size5Key", "size5key", "Key for size 5", "Size 5 key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Size5Name", "", "Name of size 5", "Size 5 name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityKey", "actkey", "Key for the activity", "Activity key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ActivityName", "", "Name of the activity", "Activity name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogKey", "catalogkey", "Key for the catalog", "Catalog key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CatalogName", "", "Name of the catalog", "Catalog name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureKey", "measurekey", "Key for the measure", "Measure key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureName", "", "Name of the measure", "Measure name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelKey", "channelkey", "Key for the channel", "Channel key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ChannelName", "", "Name of the channel", "Channel name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseKey", "usekey", "Key for the use", "Use key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseName", "", "Name of the use", "Use name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("IsCalendar", "calendar", "Is a calendar year", "Is calendar?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Side", "side", "Side", "Side");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }
    }
}
