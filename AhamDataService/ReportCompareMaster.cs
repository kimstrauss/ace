﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class ReportCompareMaster : HaiBusinessObjectBase        // UNDERLYING TABLE: rptCompareMaster
    {
        public ReportCompareMaster() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.ReportCompareMaster;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            ManufacturerName = string.Empty;
            Report1Name = string.Empty;
            Report2Name = string.Empty;
            Report3Name = string.Empty;
            Report4Name = string.Empty;
            Report5Name = string.Empty;
            ErrorMessage = string.Empty;
            Relationship = string.Empty;
        }

        #region Properties

        public int ReportCompareMasterKey
        {
            get;
            set;
        }

        public int ManufacturerKey
        {
            get;
            set;
        }

        public string ManufacturerName
        {
            get;
            set;
        }

        public int Report1Key
        {
            get;
            set;
        }

        public string Report1Name
        {
            get;
            set;
        }

        public int Report2Key
        {
            get;
            set;
        }

        public string Report2Name
        {
            get;
            set;
        }

        public int? Report3Key
        {
            get;
            set;
        }

        public string Report3Name
        {
            get;
            set;
        }

        public int? Report4Key
        {
            get;
            set;
        }

        public string Report4Name
        {
            get;
            set;
        }

        public int? Report5Key
        {
            get;
            set;
        }

        public string Report5Name
        {
            get;
            set;
        }

        public string ErrorMessage
        {
            get;
            set;
        }

        public string Relationship
        {
            get;
            set;
        }

        public bool IsError
        {
            get;
            set;
        }

        public bool YTD
        {
            get;
            set;
        }

        public bool Reconcile
        {
            get;
            set;
        }

        #endregion

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return ReportCompareMasterKey;
            }
            set
            {
                ReportCompareMasterKey = value;
            }
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "ReportCompareMasterKey: " + ReportCompareMasterKey;
            }
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("ReportCompareMasterKey", "rptcompKey", "Key for the report compare master", "Report compare master key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ManufacturerKey", "MfgKey", "Key for the manufacturer", "Manufacturer key");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ManufacturerName", "", "Name of the manufacturer", "Manufacturer name");
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Report1Key", "mfgreportkey1", "Key for report 1", "Report 1 key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = false;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Report1Name", "", "Name of report 1", "Report 1 name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Report2Key", "mfgreportkey2", "Key for report 2", "Report 2 key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = false;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Report2Name", "", "Name of report 2", "Report 2 name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Report3Key", "mfgreportkey3", "Key for report 3", "Report 3 key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Report3Name", "", "Name of report 3", "Report 3 name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Report4Key", "mfgreportkey4", "Key for report 4", "Report 4 key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Report4Name", "", "Name of report 4", "Report 4 name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Report5Key", "mfgreportkey5", "Key for report 5", "Report 5 key");
            bp.DataType = TypeCode.Int32;
            bp.IsReadonly = true;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Report5Name", "", "Name of report 5", "Report 5 name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
            
            bp = new BrowsableProperty("Relationship", "Relationship", "Type of relationship", "Relationship");
            bp.MaxStringLength = 5;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("IsError", "Error", "Is an error", "Is error?");
            bp.DataType= TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ErrorMessage", "ErrorMsg", "Error message", "Error message");
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 256;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("YTD", "Ytd", "Year to date?", "Year to date?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Reconcile", "Rec", "Reconcile?", "Reconcile?");
            bp.DataType = TypeCode.Boolean;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }
    }
}
