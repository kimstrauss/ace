﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class Size : HaiBusinessObjectBase, IItem     // UNDERLYING TABLE: DSize
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public Size() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Size;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            SizeLabel1 = string.Empty;
            SizeLabel2 = string.Empty;
            SizeLabel3 = string.Empty;
            SizeLabel4 = string.Empty;
            SizeRowLabel = string.Empty;
            SizeColumnLabel = string.Empty;
            DimensionName = string.Empty;
            SizeName = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "[" + DimensionName + "].[" + SizeRowLabel.Trim() + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return SizeKey;
            }
            set
            {
                SizeKey = value;
            }
        }

        #region Properties
        private int _sizeKey;
        public int SizeKey
        {
            get
            {
                return _sizeKey;
            }
            set
            {
                _sizeKey = value;
            }
        }

        public string SizeName
        {
            get;
            set;
        }

        public int DimensionKey
        {
            get;
            set;
        }

        public string DimensionName
        {
            get;
            set;
        }

        public Decimal SizeLowerLimit
        {
            get;
            set;
        }

        public Decimal SizeUpperLimit
        {
            get;
            set;
        }

        public string SizeLabel1
        {
            get;
            set;
        }

        public string SizeLabel2
        {
            get;
            set;
        }

        public string SizeLabel3
        {
            get;
            set;
        }

        public string SizeLabel4
        {
            get;
            set;
        }

        // calculated from the four size labels
        public string SizeRowLabel
        {
            get;
            set;
        }

        public string SizeColumnLabel
        {
            get;
            set;
        }



        #endregion

        #region IItem Members

        [Browsable(false)]
        int IItem.ItemKey
        {
            get
            {
                return SizeKey;
            }
        }

        [Browsable(false)]
        string IItem.ItemName
        {
            get
            {
                return SizeName;
            }
        }

        [Browsable(false)]
        string IItem.ItemAbbreviation
        {
            get
            {
                return "";
            }
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("SizeKey", "SizeKey", "Key for the size", "Size key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeName", "", "Name of the size", "Size name");
            bp.DataType = TypeCode.String;
            bp.IsReadonly = true;
            bp.MaxStringLength = 0;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("DimensionKey", "DimKey", "Key for the dimension", "Dimension key");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("DimensionName", "DimName", "Dimension name", "Dimension name");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeRowLabel", "SizeRowLabel", "Size row label", "Size row label");
            bp.DataType = TypeCode.String;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeColumnLabel", "SizeColumnLabel", "Size column label", "Size column label");
            bp.DataType = TypeCode.String;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeLowerLimit", "SizeLowerLimit", "Size lower limit", "Size lower limit");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Decimal;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeUpperLimit", "SizeUpperLimit", "Size upper limit", "Size uppper limit");
            bp.MustBeUnique = false;
            bp.IsReadonly = false;
            bp.DataType = TypeCode.Decimal;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeLabel1", "SizeLabel1", "Size label 1", "Size label 1");
            bp.MustBeUnique = true;
            bp.IsNullable = false;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 50;

            bp = new BrowsableProperty("SizeLabel2", "SizeLabel2", "Size label 2", "Size label 2");
            bp.MustBeUnique = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 50;

            bp = new BrowsableProperty("SizeLabel3", "SizeLabel3", "Size label 3", "Size label 3");
            bp.MustBeUnique = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 50;

            bp = new BrowsableProperty("SizeLabel4", "SizeLabel4", "Size label 4", "Size label 4");
            bp.MustBeUnique = true;
            bp.IsNullable = true;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 50;

        }


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
