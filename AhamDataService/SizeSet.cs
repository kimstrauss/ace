﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class SizeSet : HaiBusinessObjectBase, ISet     // UNDERLYING TABLE: SDSize
    {
        public SizeSet() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.SizeSet;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            SizeSetName = string.Empty;
            MarketName = string.Empty;
        }

        #region Properties

        private int _sizeSetKey;
        public int SizeSetKey
        {
            get
            {
                return _sizeSetKey;
            }
            set
            {
                _sizeSetKey = value;
            }
        }

        private string _sizeSetName;
        public string SizeSetName
        {
            get
            {
                return _sizeSetName;
            }
            set
            {
                _sizeSetName = value;
            }
        }

        public int MarketKey
        {
            get;
            set;
        }

        public string MarketName
        {
            get;
            set;
        } 

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return SizeSetName;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return SizeSetKey;
            }
            set
            {
                SizeSetKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("SizeSetKey", "SizeSetKey", "Key for the size set ", "Size set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeSetName", "SizeSetName", "Name of the size set", "Size set name");
            bp.MaxStringLength = 128;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketKey", "MarketKey", "Market key", "Market key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MarketName", "", "Market name", "Market name");
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region ISet Members

        int ISet.SetKey
        {
            get
            {
                return SizeSetKey;
            }
        }

        string ISet.SetName
        {
            get
            {
                return SizeSetName;
            }
        }

        string ISet.SetAbbreviation
        {
            get
            {
                return "(not defined)";
            }
        }

        #endregion


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
