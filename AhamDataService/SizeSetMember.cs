﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;

namespace AhamMetaDataDAL
{
    public class SizeSetMember : HaiBusinessObjectBase     // UNDERLYING TABLE: SMSize
    {
        public SizeSetMember() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.SizeSetMember;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();
        }

        #region Properties

        public string SizeSetMemberName
        {
            get;
            set;
        }

        public int SizeSetMemberKey
        {
            get;
            set;
        }

        #endregion

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return SizeSetMemberKey + "[" + SizeSetMemberKey.ToString() + "]"; ;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return SizeSetMemberKey;
            }
            set
            {
                SizeSetMemberKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("SizeSetMemberName", "SizeSetMemberName", "Name for this size set member", "Size set member name");
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.String;
            bp.MaxStringLength = 0;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SizeSetMemberKey", "SizeSetMemberKey", "Key for this size set member", "Size set member key");
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
