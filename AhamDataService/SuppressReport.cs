﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class SuppressReport : HaiBusinessObjectBase, IDateRange // UNDERLYING TABLE: SuppressReport
    {
        public SuppressReport() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.SuppressReport;

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            IndustryReportMemberName = string.Empty;


            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());
            if (_kindredPropertyNames == null)
            {
                _kindredPropertyNames = new string[1] { "IndustryReportMemberKey" };
            }
        }

        #region Properties

        public int SuppressReportKey
        {
            get;
            set;
        }

        public int IndustryReportMemberKey
        {
            get;
            set;
        }

        public string IndustryReportMemberName
        {
            get;
            set;
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return SuppressReportKey;
            }
            set
            {
                SuppressReportKey = value;
            }
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return "SuppressReportKey[" + SuppressReportKey.ToString() + "]";
            }
        }

        #endregion

        private static string[] _kindredPropertyNames;
        [Browsable(false)]
        public string[] KindredPropertyNames
        {
            get
            {
                return _kindredPropertyNames;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;
            bp = new BrowsableProperty("SuppressReportKey", "SuppressReportKey", "Key for the SuppressReport", "SuppressReport key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("IndustryReportMemberKey", "rindustrymemberkey", "Key for the industry report member", "Industry report member key");
            bp.MustBeUnique = false;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("IndustryReportMemberName", "RIndustryMemberName", "Name of the industry report member", "Industry report member name");
            bp.MustBeUnique = false;
            bp.MaxStringLength = 0;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
