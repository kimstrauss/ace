﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class Use : HaiBusinessObjectBase, IItem     // UNDERLYING TABLE: DUse
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public Use()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Use;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            UseAbbreviation = string.Empty;
            UseName = string.Empty;
        }

        [Browsable(false)]
        public override string UniqueIdentifier
        {
            get
            {
                return UseName + " [" + UseAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return UseKey;
            }
            set
            {
                UseKey = value;
            }
        }

        private int _useKey;
        public int UseKey
        {
            get
            {
                return _useKey;
            }
            set
            {
                _useKey = value;
            }
        }

        private string _useName;
        public string UseName
        {
            get
            {
                return _useName;
            }
            set
            {
                _useName = value;
            }
        }

        private string _useAbbreviation;
        public string UseAbbreviation
        {
            get
            {
                return _useAbbreviation;
            }
            set
            {
                _useAbbreviation = value;
            }
        }

        #region IItem Members

        [Browsable(false)]
        public int ItemKey
        {
            get
            {
                return UseKey;
            }
        }

        [Browsable(false)]
        public string ItemName
        {
            get
            {
                return UseName;
            }
        }

        [Browsable(false)]
        public string ItemAbbreviation
        {
            get
            {
                return UseAbbreviation;
            }
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("UseKey", "UseKey", "Key for the use", "Use key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseName", "UseName", "Name of the use", "Use name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseAbbreviation", "UseAbbrev", "Abbreviation for the use", "Use abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
