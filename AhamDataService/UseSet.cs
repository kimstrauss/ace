﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class UseSet : HaiBusinessObjectBase, ISet     // UNDERLYING TABLE: SDUse
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public UseSet()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.UseSet;
            if (_reportSpecificationList == null)
                BuildReportSpecificationList();

            UseSetAbbreviation = string.Empty;
            UseSetName = string.Empty;
        }

        #region Properties
        private int _useSetKey;
        public int UseSetKey
        {
            get
            {
                return _useSetKey;
            }
            set
            {
                _useSetKey = value;
            }
        }

        private string _useSetName;
        public string UseSetName
        {
            get
            {
                return _useSetName;
            }
            set
            {
                _useSetName = value;
            }
        }

        private string _useSetAbbreviation;
        public string UseSetAbbreviation
        {
            get
            {
                return _useSetAbbreviation;
            }
            set
            {
                _useSetAbbreviation = value;
            }
        }
        #endregion

        public override string UniqueIdentifier
        {
            get
            {
                return UseSetName + " [" + UseSetAbbreviation + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return UseSetKey;
            }
            set
            {
                UseSetKey = value;
            }
        }

        #region ISet Members

        [Browsable(false)]
        int ISet.SetKey
        {
            get
            {
                return UseSetKey;
            }
        }

        [Browsable(false)]
        string ISet.SetName
        {
            get
            {
                return UseSetName;
            }
        }

        [Browsable(false)]
        string ISet.SetAbbreviation
        {
            get
            {
                return UseSetAbbreviation;
            }
        }

        #endregion

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("UseSetKey", "UseSetKey", "Key for the use set", "Use set key");
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseSetName", "UseSetName", "Name of the use set", "Use set name");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseSetAbbreviation", "UseSetAbbrev", "Abbreviation for the use set", "Use set abbreviation");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
