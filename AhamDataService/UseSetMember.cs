﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using HaiBusinessObject;
using ReportManager;
using HaiInterfaces;

namespace AhamMetaDataDAL
{
    public class UseSetMember : HaiBusinessObjectBase, IMember     // UNDERLYING TABLE: SMUse
    {
        private static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public UseSetMember()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.UseSetMember;
            DateRange = new DateRange(Utilities.GetOpenBeginDate(), Utilities.GetOpenEndDate());

            if (_reportSpecificationList == null)
                BuildReportSpecificationList();
        }

        private int _useSetMemberKey;
        public int UseSetMemberKey
        {
            get
            {
                return _useSetMemberKey;
            }
            set
            {
                _useSetMemberKey = value;
            }
        }

        private int _useKey;
        public int UseKey
        {
            get
            {
                return _useKey;
            }
            set
            {
                _useKey = value;
            }
        }

        private int _useSetKey;
        public int UseSetKey
        {
            get
            {
                return _useSetKey;
            }
            set
            {
                _useSetKey = value;
            }
        }

        private string _useAbbreviation;
        public string UseAbbreviation
        {
            get
            {
                return _useAbbreviation;
            }
            set
            {
                _useAbbreviation = value;
            }
        }

        private string _useName;
        public string UseName
        {
            get
            {
                return _useName;
            }
            set
            {
                _useName = value;
            }
        }

        private string _useSetName;
        public string UseSetName
        {
            get
            {
                return _useSetName;
            }
            set
            {
                _useSetName = value;
            }
        }

        private string _useSetAbbreviation;
        public string UseSetAbbreviation
        {
            get
            {
                return _useSetAbbreviation;
            }
            set
            {
                _useSetAbbreviation = value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return UseName + "<->" + UseSetName + " [" + DateRange.ToString() + "]";
            }
        }

        [Browsable(false)]
        public override int PrimaryKey
        {
            get
            {
                return UseSetMemberKey;
            }
            set
            {
                UseSetMemberKey = value;
            }
        }

        [Browsable(false)]
        public DateRange DateRange
        {
            get;
            set;
        }

        public DateTime BeginDate
        {
            get
            {
                return DateRange.BeginDate.Date;
            }
            set
            {
                DateRange.BeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateRange.EndDate.Date;
            }
            set
            {
                DateRange.EndDate = value.Date;
            }
        }

        private static void BuildPropertyList()
        {
            _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            BrowsableProperty bp;

            bp = new BrowsableProperty("UseSetMemberKey", "UseSetMemberKey", "Key for this use set member", "Use set member key");
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseKey", "UseKey", "Key for the use", "Use key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseSetKey", "UseSetKey", "Key for the use set", "Use set key");
            bp.DataType = TypeCode.Int32;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseName", "", "Name of the use", "Use name");
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseAbbreviation", "UseAbbrev", "Abbreviation for the use", "Use abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseSetName", "UseSetName", "Name of the use set", "Use set name");
            bp.IsReadonly = true;
            bp.MaxStringLength = 64;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UseSetAbbreviation", "UseSetAbbrev", "Abbreviation for the use set", "Use set abbreviation");
            bp.IsReadonly = true;
            bp.MaxStringLength = 12;
            bp.DataType = TypeCode.String;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BeginDate", "BeginDate", "Begin date", "Begin date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("EndDate", "EndDate", "End date", "End date");
            bp.IsReadonly = true;
            bp.DataType = TypeCode.DateTime;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        #region IItem Members

        public int ItemKey
        {
            get
            {
                return UseKey;
            }
            set
            {
                UseKey = value;
            }
        }

        public int SetKey
        {
            get
            {
                return UseSetKey;
            }
            set
            {
                UseSetKey = value;
            }
        }

        public string ItemAbbreviation
        {
            get
            {
                return UseAbbreviation;
            }
            set
            {
                UseAbbreviation = value;
            }
        }

        public string ItemName
        {
            get
            {
                return UseName;
            }
            set
            {
                UseName = value;
            }
        }

        public string SetAbbreviation
        {
            get
            {
                return UseSetAbbreviation;
            }
            set
            {
                UseSetAbbreviation = value;
            }
        }

        public string SetName
        {
            get
            {
                return UseSetName;
            }
            set
            {
                UseSetName = value;
            }
        }

        #endregion


        #region Generic code

        private void BuildReportSpecificationList()
        {
            ReportDataAccess rda = new ReportDataAccess();
            ReportAccessResult result = rda.GetReportList(this.ObjectType);
            if (result.Success)
                _reportSpecificationList = result.ReportSpecificationList;
            else
                _reportSpecificationList = new Dictionary<string, ReportSpecification>();
        }

        protected static Dictionary<string, ReportSpecification> _reportSpecificationList;
        public override Dictionary<string, ReportSpecification> GetReportSpecificationDict()
        {
            return _reportSpecificationList;
        }

        public override ReportSpecification GetDefaultReportSpecification()
        {
            // call helper method in base class (it eliminates repeating the code to get default)
            return base.GetDefaultReportSpecification(_reportSpecificationList);
        }

        public override Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        #endregion
    }
}
