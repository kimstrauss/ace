﻿namespace IstatMetaDataPL
{
    partial class ActivityEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label lacidKeyLabel;
            System.Windows.Forms.Label measureLabel;
            System.Windows.Forms.Label parentIDLabel;
            System.Windows.Forms.Label rollLevelLabel;
            System.Windows.Forms.Label sortLabel;
            System.Windows.Forms.Label stockFlowLabel;
            this.activityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.iDTextBox = new System.Windows.Forms.TextBox();
            this.lacidKeyTextBox = new System.Windows.Forms.TextBox();
            this.measureTextBox = new System.Windows.Forms.TextBox();
            this.parentIDTextBox = new System.Windows.Forms.TextBox();
            this.rollLevelTextBox = new System.Windows.Forms.TextBox();
            this.sortTextBox = new System.Windows.Forms.TextBox();
            this.stockFlowTextBox = new System.Windows.Forms.TextBox();
            descriptionLabel = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            lacidKeyLabel = new System.Windows.Forms.Label();
            measureLabel = new System.Windows.Forms.Label();
            parentIDLabel = new System.Windows.Forms.Label();
            rollLevelLabel = new System.Windows.Forms.Label();
            sortLabel = new System.Windows.Forms.Label();
            stockFlowLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.activityBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(363, 454);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(444, 454);
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 454);
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Location = new System.Drawing.Point(18, 34);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(63, 13);
            descriptionLabel.TabIndex = 29;
            descriptionLabel.Text = "Description:";
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Location = new System.Drawing.Point(18, 60);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(21, 13);
            iDLabel.TabIndex = 31;
            iDLabel.Text = "ID:";
            // 
            // lacidKeyLabel
            // 
            lacidKeyLabel.AutoSize = true;
            lacidKeyLabel.Location = new System.Drawing.Point(18, 86);
            lacidKeyLabel.Name = "lacidKeyLabel";
            lacidKeyLabel.Size = new System.Drawing.Size(57, 13);
            lacidKeyLabel.TabIndex = 33;
            lacidKeyLabel.Text = "Lacid Key:";
            // 
            // measureLabel
            // 
            measureLabel.AutoSize = true;
            measureLabel.Location = new System.Drawing.Point(18, 112);
            measureLabel.Name = "measureLabel";
            measureLabel.Size = new System.Drawing.Size(51, 13);
            measureLabel.TabIndex = 35;
            measureLabel.Text = "Measure:";
            // 
            // parentIDLabel
            // 
            parentIDLabel.AutoSize = true;
            parentIDLabel.Location = new System.Drawing.Point(18, 138);
            parentIDLabel.Name = "parentIDLabel";
            parentIDLabel.Size = new System.Drawing.Size(55, 13);
            parentIDLabel.TabIndex = 37;
            parentIDLabel.Text = "Parent ID:";
            // 
            // rollLevelLabel
            // 
            rollLevelLabel.AutoSize = true;
            rollLevelLabel.Location = new System.Drawing.Point(18, 164);
            rollLevelLabel.Name = "rollLevelLabel";
            rollLevelLabel.Size = new System.Drawing.Size(57, 13);
            rollLevelLabel.TabIndex = 39;
            rollLevelLabel.Text = "Roll Level:";
            // 
            // sortLabel
            // 
            sortLabel.AutoSize = true;
            sortLabel.Location = new System.Drawing.Point(18, 190);
            sortLabel.Name = "sortLabel";
            sortLabel.Size = new System.Drawing.Size(29, 13);
            sortLabel.TabIndex = 41;
            sortLabel.Text = "Sort:";
            // 
            // stockFlowLabel
            // 
            stockFlowLabel.AutoSize = true;
            stockFlowLabel.Location = new System.Drawing.Point(18, 216);
            stockFlowLabel.Name = "stockFlowLabel";
            stockFlowLabel.Size = new System.Drawing.Size(63, 13);
            stockFlowLabel.TabIndex = 43;
            stockFlowLabel.Text = "Stock Flow:";
            // 
            // activityBindingSource
            // 
            this.activityBindingSource.DataSource = typeof(IstatMetaDataDAL.Activity);
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "Description", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(87, 31);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.descriptionTextBox.TabIndex = 30;
            // 
            // iDTextBox
            // 
            this.iDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "ID", true));
            this.iDTextBox.Location = new System.Drawing.Point(87, 57);
            this.iDTextBox.Name = "iDTextBox";
            this.iDTextBox.Size = new System.Drawing.Size(100, 20);
            this.iDTextBox.TabIndex = 32;
            // 
            // lacidKeyTextBox
            // 
            this.lacidKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "LacidKey", true));
            this.lacidKeyTextBox.Location = new System.Drawing.Point(87, 83);
            this.lacidKeyTextBox.Name = "lacidKeyTextBox";
            this.lacidKeyTextBox.Size = new System.Drawing.Size(100, 20);
            this.lacidKeyTextBox.TabIndex = 34;
            // 
            // measureTextBox
            // 
            this.measureTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "Measure", true));
            this.measureTextBox.Location = new System.Drawing.Point(87, 109);
            this.measureTextBox.Name = "measureTextBox";
            this.measureTextBox.Size = new System.Drawing.Size(100, 20);
            this.measureTextBox.TabIndex = 36;
            // 
            // parentIDTextBox
            // 
            this.parentIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "ParentID", true));
            this.parentIDTextBox.Location = new System.Drawing.Point(87, 135);
            this.parentIDTextBox.Name = "parentIDTextBox";
            this.parentIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.parentIDTextBox.TabIndex = 38;
            // 
            // rollLevelTextBox
            // 
            this.rollLevelTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "RollLevel", true));
            this.rollLevelTextBox.Location = new System.Drawing.Point(87, 161);
            this.rollLevelTextBox.Name = "rollLevelTextBox";
            this.rollLevelTextBox.Size = new System.Drawing.Size(100, 20);
            this.rollLevelTextBox.TabIndex = 40;
            // 
            // sortTextBox
            // 
            this.sortTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "Sort", true));
            this.sortTextBox.Location = new System.Drawing.Point(87, 187);
            this.sortTextBox.Name = "sortTextBox";
            this.sortTextBox.Size = new System.Drawing.Size(100, 20);
            this.sortTextBox.TabIndex = 42;
            // 
            // stockFlowTextBox
            // 
            this.stockFlowTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.activityBindingSource, "StockFlow", true));
            this.stockFlowTextBox.Location = new System.Drawing.Point(87, 213);
            this.stockFlowTextBox.Name = "stockFlowTextBox";
            this.stockFlowTextBox.Size = new System.Drawing.Size(100, 20);
            this.stockFlowTextBox.TabIndex = 44;
            // 
            // ActivityEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 494);
            this.Controls.Add(descriptionLabel);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(iDLabel);
            this.Controls.Add(this.iDTextBox);
            this.Controls.Add(lacidKeyLabel);
            this.Controls.Add(this.lacidKeyTextBox);
            this.Controls.Add(measureLabel);
            this.Controls.Add(this.measureTextBox);
            this.Controls.Add(parentIDLabel);
            this.Controls.Add(this.parentIDTextBox);
            this.Controls.Add(rollLevelLabel);
            this.Controls.Add(this.rollLevelTextBox);
            this.Controls.Add(sortLabel);
            this.Controls.Add(this.sortTextBox);
            this.Controls.Add(stockFlowLabel);
            this.Controls.Add(this.stockFlowTextBox);
            this.Name = "ActivityEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Activity";
            this.Load += new System.EventHandler(this.ActivityEditForm_Load);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.stockFlowTextBox, 0);
            this.Controls.SetChildIndex(stockFlowLabel, 0);
            this.Controls.SetChildIndex(this.sortTextBox, 0);
            this.Controls.SetChildIndex(sortLabel, 0);
            this.Controls.SetChildIndex(this.rollLevelTextBox, 0);
            this.Controls.SetChildIndex(rollLevelLabel, 0);
            this.Controls.SetChildIndex(this.parentIDTextBox, 0);
            this.Controls.SetChildIndex(parentIDLabel, 0);
            this.Controls.SetChildIndex(this.measureTextBox, 0);
            this.Controls.SetChildIndex(measureLabel, 0);
            this.Controls.SetChildIndex(this.lacidKeyTextBox, 0);
            this.Controls.SetChildIndex(lacidKeyLabel, 0);
            this.Controls.SetChildIndex(this.iDTextBox, 0);
            this.Controls.SetChildIndex(iDLabel, 0);
            this.Controls.SetChildIndex(this.descriptionTextBox, 0);
            this.Controls.SetChildIndex(descriptionLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.activityBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource activityBindingSource;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.TextBox iDTextBox;
        private System.Windows.Forms.TextBox lacidKeyTextBox;
        private System.Windows.Forms.TextBox measureTextBox;
        private System.Windows.Forms.TextBox parentIDTextBox;
        private System.Windows.Forms.TextBox rollLevelTextBox;
        private System.Windows.Forms.TextBox sortTextBox;
        private System.Windows.Forms.TextBox stockFlowTextBox;

    }
}