﻿using System;
using System.Windows.Forms;

using IstatMetaDataDAL;
using HaiBusinessUI;

namespace IstatMetaDataPL
{
    public partial class ActivityEditForm : HaiObjectEditForm
    {
        private Activity _activityToEdit;
        private Activity _workingActivity;

        public ActivityEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _activityToEdit = (Activity)parameters.EditObject;
            _workingActivity = new Activity();

            // set the values of a working object to the values of the object that is the target of the edit
            SetPropertiesInAnyObject(_workingActivity, _activityToEdit, _browsablePropertyList);

            activityBindingSource.DataSource = _workingActivity;
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void ActivityEditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            // set the property values in the object that was the target of the edit
            HarvestInputValues(_workingActivity);
            SetPropertiesInAnyObject(_activityToEdit, _workingActivity, _browsablePropertyList);

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}
