﻿using System;
using System.Drawing;
using System.Windows.Forms;

using HaiBusinessUI;

namespace IstatMetaDataPL
{
    public partial class AttributeEditForm : HaiObjectEditForm
    {
        private IstatMetaDataDAL.HaiAttribute _attributeToEdit;
        private IstatMetaDataDAL.HaiAttribute _workingAttribute;
        private System.Collections.Hashtable _avgTotHashTbl;

        public AttributeEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _attributeToEdit = (IstatMetaDataDAL.HaiAttribute)parameters.EditObject;
            _workingAttribute = new IstatMetaDataDAL.HaiAttribute();

            // set the values of a working object to the values of the object that is the target of the edit
            SetPropertiesInAnyObject(_workingAttribute, _attributeToEdit, _browsablePropertyList);

            attributeBindingSource.DataSource = _workingAttribute;
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void AttributeEditForm_Load(object sender, EventArgs e)
        {
            averageOrTotalComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _avgTotHashTbl = new System.Collections.Hashtable();
            _avgTotHashTbl.Add("Average", "A");
            _avgTotHashTbl.Add("Total", "T");
            _avgTotHashTbl.Add("YTD Total", "Y");
            averageOrTotalComboBox.Tag = _avgTotHashTbl;

            // if the edit item is multivalued then assign handler to lock/unlock the ComboBox
            // ... and set the background color to "readonly"
            EditFormDisplayController avgTotController = _controllerList["AverageOrTotal"];
            if (avgTotController.IsMultivalued)
            {
                averageOrTotalComboBox.KeyDown+=new KeyEventHandler(ComboBox_KeyDown);
                averageOrTotalComboBox.BackColor = SystemColors.Control;
            }
            else
            {
                SetListForComboBox(averageOrTotalComboBox);

                averageOrTotalComboBox.Text = GetKeyForValueInHashTbl(_avgTotHashTbl, _attributeToEdit.AverageOrTotal);
                averageOrTotalComboBox.BackColor = SystemColors.Window;
            }

            PrepareTheFormToShow(_parameters);
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            // set the property values in the object that was the target of the edit
            HarvestInputValues(_workingAttribute);
            SetPropertiesInAnyObject(_attributeToEdit, _workingAttribute, _browsablePropertyList);

            if (averageOrTotalComboBox.SelectedItem != null)
            {
                _attributeToEdit.AverageOrTotal = (string)(_avgTotHashTbl[averageOrTotalComboBox.SelectedItem.ToString()]);
            }

            this.DialogResult = DialogResult.OK;
            Close();
        }

    }
}
