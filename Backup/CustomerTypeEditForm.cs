using System;
using System.Windows.Forms;

using IstatMetaDataDAL;
using HaiBusinessUI;

namespace IstatMetaDataPL
{
    public partial class CustomerTypeEditForm : HaiObjectEditForm
    {
        private CustomerType _customerTypeToEdit;
        private CustomerType _workingCustomerType;

        public CustomerTypeEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _customerTypeToEdit = (CustomerType)parameters.EditObject;
            _workingCustomerType = new CustomerType();

            // set the values of a working object to the values of the object that is the target of the edit
            SetPropertiesInAnyObject(_workingCustomerType, _customerTypeToEdit, _browsablePropertyList);

            customerTypeBindingSource.DataSource = _workingCustomerType;
            btnOK.Click += new EventHandler(btnOK_Click);
        }

        private void CustomerTypeEditForm_Load(object sender, EventArgs e)
        {
            PrepareTheFormToShow(_parameters);
        }

        void btnOK_Click(object sender, EventArgs e)
        {
            // set the property values in the object that was the target of the edit
            HarvestInputValues(_workingCustomerType);
            SetPropertiesInAnyObject(_customerTypeToEdit, _workingCustomerType, _browsablePropertyList);

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}

