﻿using System;
using System.Collections.Generic;

using HaiBusinessObject;

namespace IstatMetaDataDAL
{
    public class Activity : HaiBusinessObjectBase
    {
        protected static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
            {
                _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            }
            if (_browsablePropertyList.Count == 0)
                BuildPropertyList();
            return _browsablePropertyList;
        }

        public Activity() : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Activity_Istat;
        }

        private int _ID;
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }

        private string _description;
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public override string  UniqueIdentifier
        {
	        get
            {
                return Description + " [" + ID.ToString() + "]";
            }
        }

        private string _stockFlow;
        public string StockFlow
        {
            get
            {
                return _stockFlow;
            }
            set
            {
                _stockFlow = value;
            }
        }

        private string _measure;
        public string Measure
        {
            get
            {
                return _measure;
            }
            set
            {
                _measure = value;
            }
        }

        private string _sort;
        public string Sort
        {
            get
            {
                return _sort;
            }
            set
            {
                _sort = value;
            }
        }

        private int? _rollLevel;
        public int? RollLevel
        {
            get
            {
                return _rollLevel; 
            }
            set
            {
                _rollLevel = value;
            }
        }

        private int? _parentID;
        public int? ParentID
        {
            get
            {
                return _parentID;
            }
            set
            {
                _parentID = value;
            }
        }

        private int _lacidKey;
        public int LACIDKey
        {
            get
            {
                return _lacidKey;
            }
            set
            {
                _lacidKey = value;
            }
        }

        private static void BuildPropertyList()
        {
            BrowsableProperty bp;

            bp = new BrowsableProperty("ID", "ID", "Actvity unique identifier", "Identifier", TypeCode.Int32);
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Description", "DESC", "Activity descriptions", "Descriptions", TypeCode.String);
            bp.MaxStringLength = 36;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("StockFlow", "STKFLW", "Code for stock/flow", "Stock/Flow", TypeCode.String);
            bp.MaxStringLength = 1;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Measure", "MEAS", "Measure type", "Measure", TypeCode.String);
            bp.IsNullable = true;
            bp.MaxStringLength = 1;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Sort", "SORT", "Text used for sorting", "Sort text", TypeCode.String);
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("RollLevel", "RollLevel", "Roll up level", "Roll-up level", TypeCode.Int32);
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ParentID", "ParentID", "Identifier for parent", "Parent ID", TypeCode.Int32);
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("LACIDKey", "LACIDKey", "Primary key", "Key", TypeCode.Int32);
            bp.MustBeUnique = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }
    }
}
