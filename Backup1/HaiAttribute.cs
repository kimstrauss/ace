using System;
using System.Collections.Generic;

using HaiBusinessObject;

namespace IstatMetaDataDAL
{
    public class HaiAttribute : HaiBusinessObjectBase
    {
        protected static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
            {
                _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            }
            if (_browsablePropertyList.Count == 0)
                BuildPropertyList();
            return _browsablePropertyList;
        }

        public HaiAttribute()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Attribute;
        }

        private string _description;
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        private string _shortDescription;
        public string ShortDescription
        {
            get
            {
                return _shortDescription;
            }
            set
            {
                _shortDescription = value;
            }
        }

        private int _key;
        public int Key
        {
            get
            {
                return _key;
            }
            set
            {
                _key = value;
            }
        }

        private int _ID;
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return Description + " [" + ID.ToString() + "]";
            }
        }

        private string _averageOrTotal;
        public string AverageOrTotal
        {
            get
            {
                return _averageOrTotal;
            }
            set
            {
                _averageOrTotal = value;
            }
        }

        private string _subCnt;
        public string SubCnt
        {
            get
            {
                return _subCnt;
            }
            set
            {
                _subCnt = value;
            }
        }

        private string _labels;
        public string Labels
        {
            get
            {
                return _labels;
            }
            set
            {
                _labels = value;
            }

        }

        private string _CHLMCDESC;
        public string CHLMCDESC
        {
            get
            {
                return _CHLMCDESC;
            }
            set
            {
                _CHLMCDESC = value;
            }
        }

        private string _CHLMCMOSI;
        public string CHLMCMOSI
        {
            get
            {
                return _CHLMCMOSI;
            }
            set
            {
                _CHLMCMOSI = value;
            }
        }

        private string _unitsDescription;
        public string UnitsDescription
        {
            get
            {
                return _unitsDescription;
            }
            set
            {
                _unitsDescription = value;
            }
        }

        private string _measureName;
        public string MeasureName
        {
            get
            {
                return _measureName;
            }
            set
            {
                _measureName = value;
            }
        }

        private int? _dataOrder;
        public int? DataOrder
        {
            get
            {
                return _dataOrder;
            }
            set
            {
                _dataOrder = value;
            }
        }

        private int? _modelOrder;
        public int? ModelOrder
        {
            get
            {
                return _modelOrder;
            }
            set
            {
                _modelOrder = value;
            }
        }

        private static void BuildPropertyList()
        {
            BrowsableProperty bp;
            bp = new BrowsableProperty("Description", "DESC", "Attribute description", "Description", TypeCode.String);
            bp.MustBeUnique = true;
            bp.MaxStringLength = 36;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ShortDescription", "SDESC", "Short description", "Short description", TypeCode.String);
            bp.MaxStringLength = 6;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Key", "LATIDkey", "Primary key", "Key", TypeCode.Int32);
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ID", "ID", "ID", "ID", TypeCode.Int32);
            bp.MustBeUnique = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("AverageOrTotal", "AVGTOT", "Average or total code", "Rollup type", TypeCode.String);
            bp.MaxStringLength = 1;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SubCnt", "SUBCNT", "SUBCNT", "SubCnt", TypeCode.String);
            bp.MaxStringLength = 1;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Labels", "LABELS", "Labels for attribute", "Labels", TypeCode.String);
            bp.MaxStringLength = 64;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CHLMCDESC", "CHLMCDESC", "CHLMCDESC", "CHLMCDESC", TypeCode.String);
            bp.MaxStringLength = 1;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("CHLMCMOSI", "CLMCMOSI", "CHLMCMOSI", "CHLMCMOSI", TypeCode.String);
            bp.MaxStringLength = 1;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("UnitsDescription", "UDESC", "Units description", "Units description", TypeCode.String);
            bp.MaxStringLength = 80;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("MeasureName", "MeasureName", "Name of the measure", "Measure name", TypeCode.String);
            bp.MaxStringLength = 24;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("DataOrder", "DataOrder", "DataOrder", "Data order", TypeCode.Int32);
            bp.IsNullable=true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ModelOrder", "ModelOrder", "ModelOrder", "Model order", TypeCode.Int32);
            bp.IsNullable=true;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }
    }
}
