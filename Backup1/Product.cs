using System;
using System.Collections.Generic;

using HaiBusinessObject;

namespace IstatMetaDataDAL
{
    public class Product : HaiBusinessObjectBase
    {
        protected static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
            {
                _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            }
            if (_browsablePropertyList.Count == 0)
                BuildPropertyList();
            return _browsablePropertyList;
        }

        public Product()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Product_Istat;
            _programName = string.Empty;
            _ID = string.Empty;
            _PGPD = string.Empty;
            _shortDescription = string.Empty;
            _longDescription = string.Empty;
        }

        private int _productKey;
        public int ProductKey
        {
            get
            {
                return _productKey;
            }
            set
            {
                _productKey = value;
            }
        }

        private int _programKey;
        public int ProgramKey
        {
            get
            {
                return _programKey;
            }
            set
            {
                _programKey = value;
            }
        }

        private string _programName;
        public string ProgramName
        {
            get
            {
                return _programName;
            }
            set
            {
                _programName = value;
            }
        }

        private string _ID;
        public string ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }

        private string _PGPD;
        public string PGPD
        {
            get
            {
                return _PGPD;
            }
            set
            {
                _PGPD=value;
            }
        }

        private string _shortDescription;
        public string ShortDescription
        {
            get
            {
                return _shortDescription;
            }
            set
            {
                _shortDescription = value;
            }
        }

        private string _longDescription;
        public string LongDescription
        {
            get
            {
                return _longDescription;
            }
            set
            {
                _longDescription=value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return LongDescription + " [" + ID.ToString() + "]";
            }
        }

        private string _oldID1;
        public string OldID1
        {
            get
            {
                return _oldID1;
            }
            set
            {
                _oldID1 = value;
            }
        }

        private string _oldID2;
        public string OldID2
        {
            get
            {
                return _oldID2;
            }
            set
            {
                _oldID2 = value;
            }
        }

        private bool? _batchPrint;
        public bool? BatchPrint
        {
            get
            {
                return _batchPrint;
            }
            set
            {
                _batchPrint = value;
            }
        }

        private string _modelEditMOSIText;
        public string ModelEditMOSIText
        {
            get
            {
                return _modelEditMOSIText;
            }
            set
            {
                _modelEditMOSIText = value;
            }
        }

        private string _modelEditSizeValueText;
        public string ModelEditSizeValueText
        {
            get
            {
                return _modelEditSizeValueText;
            }
            set
            {
                _modelEditSizeValueText = value;
            }
        }

        private string _collabel;
        public string ColumnLabel
        {
            get
            {
                return _collabel;
            }
            set
            {
                _collabel = value;
            }
        }

        private static void BuildPropertyList()
        {
            BrowsableProperty bp;

            bp = new BrowsableProperty("ProductKey", "LPDSYSID", "Primary key", "Key", TypeCode.Int32);
            bp.MustBeUnique = true;
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProgramKey", "LPGSysid", "Foreign key for program", "Program key", TypeCode.Int32);
            bp.IsReadonly = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ProgramName", "ProgramName", "Program name", "Program name", TypeCode.String);
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ID", "ID", "ID", "ID", TypeCode.String);
            bp.MustBeUnique = true;
            bp.MaxStringLength = 2;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("PGPD", "PGPD", "PGPD", "PGPD", TypeCode.String);
            bp.MustBeUnique = true;
            bp.MaxStringLength = 4;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ShortDescription", "SDESC", "Short description", "Short description", TypeCode.String);
            bp.MaxStringLength = 22;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("LongDescription", "LDESC", "Long description", "Long description", TypeCode.String);
            bp.MaxStringLength = 80;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("OldID1", "OID1", "Old ID 1", "Old ID 1", TypeCode.String);
            bp.MaxStringLength = 4;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("OldID2", "OID2", "Old ID 2", "Old ID 2", TypeCode.String);
            bp.MaxStringLength = 4;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("BatchPrint", "BP", "Enable batch printing", "Batch print", TypeCode.Boolean);
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ModelEditMOSIText", "ModelEditMOSIText", "ModelEditMOSIText", "ModelEditMOSIText", TypeCode.String);
            bp.MaxStringLength = 50;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ModelEditSizeValueText", "ModelEditSizeValueText", "ModelEditSizeValueText", "ModelEditSizeValueText", TypeCode.String);
            bp.MaxStringLength = 50;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ColumnLabel", "collabel", "Column label text", "Column label", TypeCode.String);
            bp.MaxStringLength = 128;
            bp.IsNullable = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }
    }
}
