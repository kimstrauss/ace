using System;
using System.Collections.Generic;
using System.Text;

using HaiBusinessObject;

namespace IstatMetaDataDAL
{
    public class TestingType : HaiBusinessObjectBase
    {
        protected static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
            {
                _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            }
            if (_browsablePropertyList.Count == 0)
                BuildPropertyList();
            return _browsablePropertyList;
        }

        public TestingType()
            : base()
        {
        }

        private string _description;
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return Description;
            }
        }

        private string _sortText;
        public string SortText
        {
            get
            {
                return _sortText;
            }
            set
            {
                _sortText = value;
            }
        }

        private int _key;
        public int Key
        {
            get
            {
                return _key;
            }
            set
            {
                _key = value;
            }
        }

        private DateTime _anyDate;
        public DateTime AnyDate
        {
            get
            {
                return _anyDate;
            }

            set
            {
                _anyDate = value;
            }
        }

        private bool _trueOrFalse;
        public bool TrueOrFalse
        {
            get
            {
                return _trueOrFalse;
            }
            set
            {
                _trueOrFalse = value;
            }
        }


        private static void BuildPropertyList()
        {

            BrowsableProperty bp;
            bp = new BrowsableProperty("Description", "DESC", "Customer description", "Description");
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SortText", "SORT", "Text to use for sorting", "Sort text");
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Key", "lcuidKey", "Primary key", "Key");
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("AnyDate", "zdt", "Some random date", "Any date");
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("TrueOrFalse", "tf", "Truth or consequences", "Truth or consequences");
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
    }
}
