﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HaiBusinessObject;
using System.Windows.Forms;

namespace HaiBusinessUI
{
    public enum ControlAccessState
    {
        NotInitialized,
        ValueIsReadOnly,
        SingleValueIsWriteable,
        MultiValueIsLocked,
        MultiValueIsUnlocked
    }

    public class ControlTag
    {
        public ControlTag()
        {
            ControlAccessState = ControlAccessState.NotInitialized;
        }

        public ControlAccessState ControlAccessState
        {
            get;
            set;
        }

        public BrowsableProperty BrowsableProperty
        {
            get;
            set;
        }

        public bool IsReadOnlyForMultiEdit
        {
            get;
            set;
        }

        public ComboBoxListType ComboBoxListType
        {
            get;
            set;
        }

        public object ComboBoxListItems
        {
            get;
            set;
        }

        public object BindingList
        {
            get;
            set;
        }

        public string DisplayMemberName
        {
            get;
            set;
        }

        public string ValueMemberName
        {
            get;
            set;
        }

        public bool MultipleValuesInUnderlyingData
        {
            get;
            set;
        }

        public Control RelatedControl
        {
            get;
            set;
        }

        public string TargetRelatedItemTextPropertyName
        {
            get;
            set;
        }
    }
}
