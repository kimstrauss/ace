﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using HaiBusinessObject;

namespace HaiBusinessUI
{
    public partial class EditDateRange : Form
    {
        private DateRange _anyDateRange;

        public EditDateRange(DateRange anyDateRange)
        {
            InitializeComponent();

            _anyDateRange = anyDateRange;

            Binding aBinding;
            aBinding = beginDateTextBox.DataBindings["Text"];
            aBinding.Parse += new ConvertEventHandler(HandleDateParse);
            aBinding.Format += new ConvertEventHandler(HandleDateFormat);
            beginDateTextBox.Validating += new CancelEventHandler(HandleDateValidating);

            aBinding = endDateTextBox.DataBindings["Text"];
            aBinding.Parse += new ConvertEventHandler(HandleDateParse);
            aBinding.Format += new ConvertEventHandler(HandleDateFormat);
            endDateTextBox.Validating += new CancelEventHandler(HandleDateValidating);

        }

        private void EditDateRange_Load(object sender, EventArgs e)
        {
            dateRangeBindingSource.DataSource = _anyDateRange;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string message = string.Empty;

            message = _anyDateRange.ValidateRangeForType(DateRangeValidationType.Monthly);
            if (message == string.Empty)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(message, "Date Range Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        void HandleDateValidating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (btnCancel.Focused)
            {
                e.Cancel = true;
                this.Close();
                return;
            }

            TextBox theTextBox = (TextBox)sender;

            if (theTextBox.ReadOnly)
                return;

            DateTime theDate;
            string text = theTextBox.Text.Trim();

            if (text == "<<" || text == "open begin date" || text == "(open begin date)")
            {
                theTextBox.Text = "(open begin date)";
                return;
            }

            if (text == ">>" || text == "open end date" || text == "(open end date)")
            {
                theTextBox.Text = "(open end date)";
                return;
            }

            BindingSource bindingSource = (BindingSource)theTextBox.DataBindings["Text"].DataSource;
            string propertyName = theTextBox.DataBindings["Text"].BindingMemberInfo.BindingField;
            object target = bindingSource.Current;
            System.Reflection.PropertyInfo pInfo = target.GetType().GetProperty(propertyName);
            object oldValue = pInfo.GetValue(target, null);

            bool ok = DateTime.TryParse(text, out theDate);

            if (ok)
            {
                if (theDate < Utilities.GetOpenBeginDate().Date || theDate > Utilities.GetOpenEndDate().Date)
                {
                    MessageBox.Show("Date must be between " + Utilities.GetOpenBeginDate().Date.ToShortDateString()
                        + " and " + Utilities.GetOpenEndDate().Date.ToShortDateString(), "Data entry error",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    theTextBox.Text = ((DateTime)oldValue).ToShortDateString();
                    theTextBox.SelectAll();
                    theTextBox.Focus();

                    e.Cancel = true;
                }
                else
                    theTextBox.Text = theDate.ToShortDateString();
            }
            else
            {
                MessageBox.Show("A valid date, << \"(open begin date)\" or >> \"(open end date)\" must be entered.", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                theTextBox.Text = ((DateTime)oldValue).ToShortDateString();
                theTextBox.SelectAll();
                theTextBox.Focus();

                e.Cancel = true;
            }
        }

        void HandleDateParse(object sender, ConvertEventArgs e)
        {
            string theText = e.Value.ToString().Trim();
            DateTime theDate;

            bool ok = DateTime.TryParse(theText, out theDate);

            if (ok)
                e.Value = theDate;
            else
            {
                if (theText == "(open begin date)")
                    e.Value = Utilities.GetOpenBeginDate();

                if (theText == "(open end date)")
                    e.Value = Utilities.GetOpenEndDate();
            }
        }

        void HandleDateFormat(object sender, ConvertEventArgs e)
        {
            object aValue = e.Value;

            if (aValue is DateTime)
            {
                DateTime aDate = (DateTime)aValue;
                if (aDate == Utilities.GetOpenBeginDate())
                    e.Value = "(open begin date)";

                if (aDate == Utilities.GetOpenEndDate())
                    e.Value = "(open end date)";

                if (aDate == DateTime.MinValue.Date || aDate == DateTime.MaxValue.Date)
                    e.Value = "";
            }
        }
    }
}
