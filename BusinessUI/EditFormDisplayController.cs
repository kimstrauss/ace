using System;
using System.Collections.Generic;
using System.Text;

namespace HaiBusinessUI
{
	public class EditFormDisplayController
    {
        private string _propertyName;
        public string PropertyName
        {
            get
            {
                return _propertyName;
            }
            set
            {
                _propertyName = value;
            }
        }

        private bool _isHidden;
        public bool IsHidden
        {
            get
            {
                return _isHidden;
            }
            set
            {
                _isHidden = value;
            }
        }

        private bool _isReadonly;
        public bool IsReadonly
        {
            get
            {
                return _isReadonly;
            }
            set
            {
                _isReadonly = value;
            }
        }

        private bool _mustBeUnique;
        public bool MustBeUnique
        {
            get
            {
                return _mustBeUnique;
            }
            set
            {
                _mustBeUnique = value;
            }
        }

        private bool _isMultivalued;
        public bool IsMultivalued
        {
            get
            {
                return _isMultivalued;
            }
            set
            {
                _isMultivalued = value;
            }
        }

        private bool _isChangedByEdit;
        public bool IsChangedByEdit
        {
            get
            {
                return _isChangedByEdit;
            }
            set
            {
                _isChangedByEdit = value;
            }
        }

        public bool DoNotApplyFormattingToBoundValue
        {
            get;
            set;
        }
    }
}
