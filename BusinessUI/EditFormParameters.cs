using System;
using System.Collections.Generic;
using System.Text;

using HaiBusinessObject;

namespace HaiBusinessUI
{
    public class EditFormParameters
    {
        private int numberOfItemsToEdit;

        public EditFormParameters(GloballyUsefulStuff gus)
        {
            GloballyUsefulStuff = gus;
        }

        public int NumberOfItemsToEdit
        {
            get
            {
                return numberOfItemsToEdit;
            }
            set
            {
                numberOfItemsToEdit = value;
            }
        }

        public List<HaiBusinessObjectBase> UnfilteredItems
        {
            get;
            set;
        }

        public List<HaiBusinessObjectBase> ItemsSelectedForEdit
        {
            get;
            set;
        }

        public HaiBusinessObjectBase EditObject
        {
            get;
            set;
        }

        public Dictionary<string, EditFormDisplayController> ControllerDictionary
        {
            get;
            set;
        }

        public Dictionary<string, BrowsableProperty> BrowsablePropertyList
        {
            get;
            set;
        }

        public GloballyUsefulStuff GloballyUsefulStuff
        {
            get;
            set;
        }

    }
}
