﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HaiBusinessObject;

namespace HaiBusinessUI
{
    public class GloballyUsefulStuff
    {
        public bool HideReportSelection
        {
            get;
            set;
        }

        public string UserName
        {
            get
            {
                string shortName;
                string qualifiedName = this.UserNameQualified;
                int posLastBackslash = qualifiedName.LastIndexOf(@"\");
                shortName = qualifiedName.Substring(posLastBackslash + 1);
                return shortName;
            }
        }

        public string UserNameQualified
        {
            get
            {
                return HaiBusinessObject.Utilities.GetUserName();
            }
        }

        public string ApplicationContext
        {
            get
            {
                return HaiBusinessObject.Utilities.GetApplicationContext();
            }
        }

        public int DatabaseContext      // AHAM = 1; OPEI = 2
        {
            get;
            set;
        }
    }
}
