using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using HaiBusinessObject;
using HaiBusinessUI;
using HaiMetaDataDAL;

namespace HaiBusinessUI
{
    public partial class HaiMetaDataEditor : Form
    {
        protected GloballyUsefulStuff _gus;
        protected ucReadonlyDatagridForm _currentGridControl;
        protected IMetaDataDS _ds;
        protected DataGridView _datagrid;
        protected bool _cacheIsDirty = false;

        protected delegate void AddProcedure(IUntypedBindingList bindingList);
        protected Dictionary<HaiBusinessObjectType, AddProcedure> _callAddProcedure;

        protected delegate void EditProcedure();
        protected Dictionary<HaiBusinessObjectType, EditProcedure> _callEditProcedure;

        protected delegate void DuplicateProcedure();
        protected Dictionary<HaiBusinessObjectType, DuplicateProcedure> _callDuplicateProcedure;

        private int _rightMargin = 30;
        private int _bottomMargin = 110;

        protected MenuStrip _mainMenu;       // make the Main Menu available to sub-classes

        public HaiMetaDataEditor()
        {
            InitializeComponent();

            KeyPreview = true;
            _mainMenu = menuMain;           // make the Main Menu available to sub-classes
            _gus = new GloballyUsefulStuff();
        }

        #region FormEventHandlers

        private void MainForm_Load(object sender, EventArgs e)
        {
            // buttons are not enabled until data has been loaded and grid is visible
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            btnDuplicate.Enabled = false;
            btnPrint.Enabled = false;

            // "close" menu item not enabled until data has been loaded and grid is visible
            closeToolStripMenuItem.Enabled = false;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (_currentGridControl != null)
                _currentGridControl.Size = new Size(this.Width - _rightMargin, this.Height - _bottomMargin);
        }

        #endregion

        #region DataGridView event handlers

        void datagrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!(e.RowIndex < 0))
            {
                _datagrid.Rows[e.RowIndex].Selected = true;
                EditItemsInTheGrid();
            }
        }

        #endregion

        #region Button event handlers

        // Edit button
        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditItemsInTheGrid();
        }

        // Add button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddAnItemToGrid();
        }

        // Delete button
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridView datagrid = _currentGridControl.DataGridView;

            HaiBindingList<HaiBusinessObjectBase> selectedObjects = _currentGridControl.GetReadonlyBindingListOfSelectedItems(true);
            if (selectedObjects.Count==0)           // nothing selected
            {
                MessageBox.Show("Nothing selected.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                DialogResult yesNo = MessageBox.Show(selectedObjects.Count.ToString() + " item(s) will be deleted. \n\r Are you sure?", "", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (yesNo == DialogResult.No)
                    return;
            }

            IUntypedBindingList datalist = (IUntypedBindingList)((BindingSource)(datagrid.DataSource)).DataSource;
            foreach (HaiBusinessObjectBase haiObject in selectedObjects)
            {
                haiObject.MarkDeleted();
                DataAccessResult result = _ds.Save(haiObject);
                if (result.Success)
                    datalist.RemoveFromList(haiObject);
                else
                {
                    MessageBox.Show("Delete failed. -- " + result.Message);
                    haiObject.MarkClean();
                }
            }
        }

        // Copy button
        private void btnCopy_Click(object sender, EventArgs e)
        {
            DuplicateItemsInTheGrid();
        }

        // Print button
        private void btnPrint_Click(object sender, EventArgs e)
        {
            _currentGridControl.PrintGridReport();
        }

        #endregion

        #region Utilities

        protected void CreateDataGridControl<T>(DataAccessResult dataFetchResult, List<string> columnList, 
            string mainFormTitle, HaiReports.ListType listType)
        {
            Cursor = Cursors.WaitCursor;
            _cacheIsDirty = true;

            RemoveDataGridControl();

            menuMain.Items.Add(gridToolStripMenuItem);

            ucReadonlyDatagridForm dataGridControl = new ucReadonlyDatagridForm(dataFetchResult, columnList, _gus);
            _datagrid = dataGridControl.DataGridView;
            _datagrid.CellDoubleClick += new DataGridViewCellEventHandler(datagrid_CellDoubleClick);

            _currentGridControl = dataGridControl;
            dataGridControl.Location = new Point(12, 33);
            dataGridControl.Size = new Size(this.Width - _rightMargin, this.Height - _bottomMargin);
            this.Controls.Add(dataGridControl);
            dataGridControl.Show();

            ContextMenuStrip contextMenu = _currentGridControl.GetContextMenu();
            gridToolStripMenuItem.DropDown = contextMenu;
            gridToolStripMenuItem.Enabled = true;

            btnAdd.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
            btnDuplicate.Enabled = true;

            // test to see if data and reports are available; set "Print" button accordingly
            if (dataFetchResult.Success && (dataFetchResult.DataList).Count > 0)
            {
                bool reportsAreAvailable = false;       // assume no reports are available

                if (((HaiBusinessObjectBase)((IUntypedBindingList)dataFetchResult.DataList)[0]).GetReportSpecificationDict() != null)           // check that report dict is there
                    if (((HaiBusinessObjectBase)((IUntypedBindingList)dataFetchResult.DataList)[0]).GetReportSpecificationDict().Count != 0)    // see if it contains reports
                        reportsAreAvailable = true;

                // enable/disable print button
                if (reportsAreAvailable)
                    btnPrint.Enabled = true;
                else
                    btnPrint.Enabled = false;

                // create a ReportParameters object and inject it into the binding list object using list type as passed in.
                HaiReports.ReportParameters reportParameters = new HaiReports.ReportParameters();
                reportParameters.SetBasicProperties(listType);
                ((IUntypedBindingList)(dataFetchResult.DataList)).ReportParameters = reportParameters;
            }
            else
            {
                btnPrint.Enabled = false;
            }

            closeToolStripMenuItem.Enabled = true;
            this.Text = mainFormTitle + "  [Server: " + Utilities.GetServerDisplayName() + "   Database: " + Utilities.GetDatabaseDisplayName() + "]";
            Cursor = Cursors.Default;
        }

        protected void RemoveDataGridControl()
        {
            if (_currentGridControl != null)
            {
                this.Controls.Remove(_currentGridControl);
                _currentGridControl.Dispose();
                _currentGridControl = null;

                menuMain.Items.Remove(gridToolStripMenuItem);

                btnAdd.Enabled = false;
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
                btnDuplicate.Enabled = false;
                btnPrint.Enabled = false;

                closeToolStripMenuItem.Enabled = false;
            }
        }


        protected List<HaiBusinessObjectBase> GetItemsSelectedForEdit<T>(DataGridView datagrid)
        {
            // verify that at least one item is selected for editing
            if (datagrid.SelectedRows.Count < 1)
            {
                throw new Exception("Cannot edit zero selected items.");
            }

            HaiBindingList<T> theBindingList = (HaiBindingList<T>)(((BindingSource)datagrid.DataSource).List);        // get the data collection

            // verify that the list contains objects derived from HaiBusinessObjectBase
            T firstObject = (T)theBindingList[datagrid.SelectedRows[0].Index];
            if (!(firstObject is HaiBusinessObjectBase))
            {
                throw new Exception("Edit object must be derived from HaiBusinessObjectBase");
            }

            // collect the objects that will be edited
            List<HaiBusinessObjectBase> objectsSelectedForEdit = new List<HaiBusinessObjectBase>();
            foreach (DataGridViewRow selectedRow in datagrid.SelectedRows)
            {
                objectsSelectedForEdit.Add(theBindingList[selectedRow.Index] as HaiBusinessObjectBase);
            }

            return objectsSelectedForEdit;
        }

        protected List<HaiBusinessObjectBase> GetUnfilteredGridItems<T>(DataGridView datagrid)
        {
            List<HaiBusinessObjectBase> unfilteredList = new List<HaiBusinessObjectBase>();

            HaiBindingList<T> theBindingList = (HaiBindingList<T>)(((BindingSource)datagrid.DataSource).List);        // get the data collection
            List<T> typedUnfilteredList = theBindingList.GetUnfilteredList();

            foreach (T anyBusinessObject in typedUnfilteredList)
            {
                unfilteredList.Add(anyBusinessObject as HaiBusinessObjectBase);
            }

            return unfilteredList;
        }

        protected void EditItemsInTheGrid()
        {
            HaiBindingList<HaiBusinessObjectBase> selectedObjects = _currentGridControl.GetReadonlyBindingListOfSelectedItems(true);
            if (selectedObjects.Count == 0)           // nothing selected
            {
                MessageBox.Show("Nothing selected.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (_currentGridControl.ActionRedirectionObjectType == HaiBusinessObjectType.UnknownHaiBusinessObjectType)
            {
                IUntypedBindingList bindingList = (IUntypedBindingList)((BindingSource)_datagrid.DataSource).List;  // get the data collection
                HaiBusinessObjectType type = bindingList.TypeOfBussinessObjectsInList;
                if (_callEditProcedure.ContainsKey(type))
                    _callEditProcedure[type]();                                                                     // call the edit procedure for the type of business object in the binding list
                else
                    MessageBox.Show("Edit procedure does not exist.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                HaiBusinessObjectType type = _currentGridControl.ActionRedirectionObjectType;
                if (_callEditProcedure.ContainsKey(type))
                    _callEditProcedure[type]();                                                                     // call the edit procedure for the type of business object in the binding list
                else
                    MessageBox.Show("Edit procedure does not exist.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            _datagrid.Refresh();
        }

        protected void DuplicateItemsInTheGrid()
        {
            HaiBindingList<HaiBusinessObjectBase> selectedObjects = _currentGridControl.GetReadonlyBindingListOfSelectedItems(true);
            if (selectedObjects.Count == 0)           // nothing selected
            {
                MessageBox.Show("Nothing selected.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            IUntypedBindingList bindingList = (IUntypedBindingList)((BindingSource)_datagrid.DataSource).List;  // get the data collection
            HaiBusinessObjectType type = bindingList.TypeOfBussinessObjectsInList;
            if (_callDuplicateProcedure.ContainsKey(type))
                _callDuplicateProcedure[type]();                                                                     // call the edit procedure for the type of business object in the binding list
            else
                MessageBox.Show("Duplicate function is not available.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        protected void AddAnItemToGrid()
        {
            IUntypedBindingList bindingList = (IUntypedBindingList)((BindingSource)_datagrid.DataSource).List;  // get the data collection
            HaiBusinessObjectType type = bindingList.TypeOfBussinessObjectsInList;

            if (_callAddProcedure.ContainsKey(type))
                _callAddProcedure[type](bindingList);                                                           // call the edit procedure for the type of business object in the binding list
            else
                MessageBox.Show("Add procedure does not exist.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            _datagrid.Refresh();
        }


        protected EditFormParameters CreateEditFormParametersForNew<T>(List<HaiBusinessObjectBase> unfilteredItems)
        {
            HaiBusinessObjectBase newHaiObject = System.Activator.CreateInstance<T>() as HaiBusinessObjectBase;
            Dictionary<string, BrowsableProperty> bpList = newHaiObject.GetClassBrowsablePropertyList();

            EditFormParameters formParameters = new EditFormParameters(_gus);   // container for return objects
            Dictionary<string, EditFormDisplayController> controllerDict = new Dictionary<string, EditFormDisplayController>(); // dict of items to control form display

            // iterate across the properties of all objects to edit
            // ... to determine if any are multivalued, etc.
            foreach (string propertyName in bpList.Keys)
            {
                BrowsableProperty bp = bpList[propertyName];                            // get the name of the property
                EditFormDisplayController controller = new EditFormDisplayController(); // create a controller for this property
                controller.PropertyName = propertyName;                                 // ... set the PropertyName
                controllerDict.Add(controller.PropertyName, controller);                // ... and add to the collection

                // set other properties needed by the controller
                controller.IsReadonly = bp.IsReadonly;
                controller.MustBeUnique = bp.MustBeUnique;
            }

            formParameters.EditObject = newHaiObject as HaiBusinessObjectBase;
            formParameters.ControllerDictionary = controllerDict;
            formParameters.BrowsablePropertyList = bpList;
            formParameters.NumberOfItemsToEdit = 0;
            formParameters.ItemsSelectedForEdit = new List<HaiBusinessObjectBase>();
            formParameters.UnfilteredItems = unfilteredItems;

            return formParameters;
        }

        protected EditFormParameters CreateEditFormParametersForDirectEdit(HaiBusinessObjectBase directEditObject, List<HaiBusinessObjectBase> unfilteredItems, Dictionary<string, BrowsableProperty> bpList)
        {
            List<HaiBusinessObjectBase> theList=new List<HaiBusinessObjectBase>();
            theList.Add(directEditObject);                  // create a "host" list to hold the object that will be directly edited
            EditFormParameters parameters = CreateEditFormParametersForProxyEdit(theList, unfilteredItems, bpList);
            parameters.EditObject = directEditObject;       // override the proxy/working (new) object created by CreateEditFormParametersForProxyEdit(); use actual edit target

            return parameters;
        }

        protected EditFormParameters CreateEditFormParametersForProxyEdit(List<HaiBusinessObjectBase> itemsSelectedForEdit, List<HaiBusinessObjectBase> unfilteredItems, Dictionary<string, BrowsableProperty> bpList)
        {
            EditFormParameters formParameters = new EditFormParameters(_gus);           // container for return objects
            HaiBusinessObjectBase anyHaiBusinessObjectToEdit;                           // object with properties set by the edit
            Dictionary<string, EditFormDisplayController> controllerDict = new Dictionary<string, EditFormDisplayController>(); // dict of items to control form display

            // create a working object that will be used as the target of the edit
            anyHaiBusinessObjectToEdit = (HaiBusinessObjectBase)Activator.CreateInstance(itemsSelectedForEdit[0].GetType());
            anyHaiBusinessObjectToEdit.MarkOld();

            // iterate across the properties of all objects to edit
            // ... to determine if any are multivalued, etc.
            foreach (string propertyName in bpList.Keys)
            {
                BrowsableProperty bp = bpList[propertyName];                            // get the name of the property
                EditFormDisplayController controller = new EditFormDisplayController(); // create a controller for this property
                controller.PropertyName = propertyName;                                 // ... set the PropertyName
                controllerDict.Add(controller.PropertyName, controller);                // ... and add to the collection

                // determine whether this property has more than one value among the objects to be editted.
                bool isMultivalued = false;
                object refValue = Utilities.CallByName(itemsSelectedForEdit[0], propertyName, CallType.Get);  // get the value of the first object as a reference

                // see if any objects have a value for this property that is different than the value for the reference
                foreach (HaiBusinessObjectBase anyHaiBusinessObject in itemsSelectedForEdit)
                {
                    if (refValue == null)
                    {
                        if (Utilities.CallByName(anyHaiBusinessObject, propertyName, CallType.Get)!=null)
                        {
                            isMultivalued = true;
                            break;
                        }
                    }
                    else
                    {
                        if (!refValue.Equals(Utilities.CallByName(anyHaiBusinessObject, propertyName, CallType.Get)))
                        {
                            isMultivalued = true;
                            break;
                        }
                    }
                }

                if (isMultivalued)
                {   // mark this property as having multiple values
                    controller.IsMultivalued = true;
                }
                else
                {   // mark this property as having a single value, and set the edit object's property to the common value
                    controller.IsMultivalued = false;
                    Utilities.CallByName(anyHaiBusinessObjectToEdit, propertyName, CallType.Set, refValue);
                }

                // set other properties needed by the controller
                controller.IsReadonly = bp.IsReadonly;
                controller.MustBeUnique = bp.MustBeUnique;
            }

            formParameters.EditObject = anyHaiBusinessObjectToEdit as HaiBusinessObjectBase;
            formParameters.ControllerDictionary = controllerDict;
            formParameters.BrowsablePropertyList = bpList;
            formParameters.NumberOfItemsToEdit = itemsSelectedForEdit.Count;
            formParameters.ItemsSelectedForEdit = itemsSelectedForEdit;
            formParameters.UnfilteredItems = unfilteredItems;

            return formParameters;
        }

        protected EditFormParameters CreateEditFormParametersForDuplicate(List<HaiBusinessObjectBase> itemsSelectedForEdit, List<HaiBusinessObjectBase> unfilteredItems, Dictionary<string, BrowsableProperty> bpList)
        {
            EditFormParameters formParameters = new EditFormParameters(_gus);   // container for return objects
            HaiBusinessObjectBase haiProxyObject;                                   // object with properties set by the edit
            Dictionary<string, EditFormDisplayController> controllerDict = new Dictionary<string, EditFormDisplayController>(); // dict of items to control form display

            // create a working object that will be used as the target of the edit
            haiProxyObject = (HaiBusinessObjectBase)Activator.CreateInstance(itemsSelectedForEdit[0].GetType());
            //haiProxyObject.MarkOld();

            // iterate across the properties of all objects to edit
            // ... to determine if any are multivalued, etc.
            foreach (string propertyName in bpList.Keys)
            {
                BrowsableProperty bp = bpList[propertyName];                            // get the name of the property
                EditFormDisplayController controller = new EditFormDisplayController(); // create a controller for this property
                controller.PropertyName = propertyName;                                 // ... set the PropertyName
                controllerDict.Add(controller.PropertyName, controller);                // ... and add to the collection

                // determine whether this property has more than one value among the objects to be editted.
                bool isMultivalued = false;
                object refValue = Utilities.CallByName(itemsSelectedForEdit[0], propertyName, CallType.Get);  // get the value of the first object as a reference

                // see if any objects have a value for this property that is different than the value for the reference
                foreach (HaiBusinessObjectBase anyHaiBusinessObject in itemsSelectedForEdit)
                {
                    if (refValue == null)
                    {
                        if (Utilities.CallByName(anyHaiBusinessObject, propertyName, CallType.Get) != null)
                        {
                            isMultivalued = true;
                            break;
                        }
                    }
                    else
                    {
                        if (!refValue.Equals(Utilities.CallByName(anyHaiBusinessObject, propertyName, CallType.Get)))
                        {
                            isMultivalued = true;
                            break;
                        }
                    }
                }

                if (isMultivalued)
                {   // mark this property as having multiple values
                    controller.IsMultivalued = true;
                }
                else
                {   // mark this property as having a single value, and set the edit object's property to the common value
                    controller.IsMultivalued = false;
                    Utilities.CallByName(haiProxyObject, propertyName, CallType.Set, refValue);
                }

                // set other properties needed by the controller
                controller.IsReadonly = bp.IsReadonly;
                controller.MustBeUnique = bp.MustBeUnique;
            }

            haiProxyObject.PrimaryKey = 0;  // give the proxy object a PrimaryKey = 0 for display purposes.

            formParameters.EditObject = haiProxyObject as HaiBusinessObjectBase;
            formParameters.ControllerDictionary = controllerDict;
            formParameters.BrowsablePropertyList = bpList;
            formParameters.NumberOfItemsToEdit = (-1) * (itemsSelectedForEdit.Count);
            formParameters.ItemsSelectedForEdit = itemsSelectedForEdit;
            formParameters.UnfilteredItems = unfilteredItems;

            return formParameters;
        }

        protected bool ContinueAfterCheckForChangedItems(List<HaiBusinessObjectBase> items, Dictionary<string, BrowsableProperty> bpList)
        {
            // refresh the items that have been selected for editing
            // if any have changed, update the items (from the binding list)
            // and give the user an opportunity to cancel further action
            // if there are items that have changed.

            bool isChanged = false;

            foreach (HaiBusinessObjectBase anyHaiBusinessObject in items)
            {
                DataAccessResult result = _ds.DataItemReGet(anyHaiBusinessObject);
                if (!result.Success)
                {
                    MessageBox.Show(result.Message, "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (result.ItemIsChanged)
                {
                    isChanged = true;
                    HaiBusinessObjectBase updatedHaiBusinessObject = result.SingleItem;

                    // update the objects browsable properties
                    foreach (string propertyName in bpList.Keys)
                    {
                        object newValue = Utilities.CallByName(updatedHaiBusinessObject, propertyName, CallType.Get);
                        Utilities.CallByName(anyHaiBusinessObject, propertyName, CallType.Set, newValue);
                    }

                    // update the tracking properties
                    anyHaiBusinessObject.RowVersion = updatedHaiBusinessObject.RowVersion;
                    anyHaiBusinessObject.UserName = updatedHaiBusinessObject.UserName;
                    anyHaiBusinessObject.ApplicationContext = updatedHaiBusinessObject.ApplicationContext;
                }
            }

            if (isChanged)
            {
                DialogResult answer = MessageBox.Show("The item(s) selected for edit have changed.\n\rDo you want to continue?", "Changed items",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (answer == DialogResult.No)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }

        protected void EditSingleHaiBusinessObject<TItemToEdit, TEditForm>()
        {
            List<HaiBusinessObjectBase> itemsToEdit = GetItemsSelectedForEdit<TItemToEdit>(_datagrid);

            if (itemsToEdit.Count > 1)
            {
                MessageBox.Show("Only one item can be selected for editing.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            HaiBusinessObjectBase anyHaiBusinessObject = System.Activator.CreateInstance<TItemToEdit>() as HaiBusinessObjectBase;
            Dictionary<string, BrowsableProperty> bpList = anyHaiBusinessObject.GetClassBrowsablePropertyList();

            if (ContinueAfterCheckForChangedItems(itemsToEdit, bpList))
            {
                _datagrid.Refresh();

                List<HaiBusinessObjectBase> unfilteredItems = GetUnfilteredGridItems<TItemToEdit>(_datagrid);
                EditFormParameters editParameters = CreateEditFormParametersForDirectEdit(itemsToEdit[0], unfilteredItems, bpList);

                Form theForm = System.Activator.CreateInstance(typeof(TEditForm), editParameters) as Form;
                theForm.ShowDialog(this);

                if (theForm.DialogResult == DialogResult.OK)
                {
                    Cursor = Cursors.WaitCursor;

                    // check for external conflicts that would prevent saving the object
                    DataAccessTestResult testResult = _ds.TestObjectForSaveConflicts(anyHaiBusinessObject);
                    if (testResult.Success)
                    {
                        if (!testResult.Passed)
                        {
                            Cursor = Cursors.Default;
                            anyHaiBusinessObject.CancelEdit();
                            MessageBox.Show("Item cannot be saved, external conflicts detected!\n\n" + testResult.Message, "Data conflict", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Nothing saved.  Test for external conflicts failed.\n\n" + testResult.Message, "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    DataAccessResult result = _ds.Save(editParameters.EditObject);
                    Cursor = Cursors.Default;

                    if (result.Success)
                        editParameters.EditObject.ApplyEdit();
                    else
                    {
                        editParameters.EditObject.CancelEdit();
                        MessageBox.Show(result.Message, "Data save error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                    editParameters.EditObject.CancelEdit();

                theForm.Dispose();
            }
        }

        protected void EditMultipleHaiBusinessObjects<TItemToEdit, TEditForm>(DataGridView theDatagrid, DateRangeValidationType validationType)
        {
            List<HaiBusinessObjectBase> itemsToEdit = GetItemsSelectedForEdit<TItemToEdit>(theDatagrid);

            HaiBusinessObjectBase anyHaiBusinessObject = System.Activator.CreateInstance<TItemToEdit>() as HaiBusinessObjectBase;
            Dictionary<string, BrowsableProperty> bpList = anyHaiBusinessObject.GetClassBrowsablePropertyList();

            if (ContinueAfterCheckForChangedItems(itemsToEdit, bpList))
            {
                theDatagrid.Refresh();
                List<HaiBusinessObjectBase> unfilteredItems = GetUnfilteredGridItems<TItemToEdit>(_datagrid);
                EditFormParameters editParameters = CreateEditFormParametersForProxyEdit(itemsToEdit, unfilteredItems, bpList);

                Form theForm = System.Activator.CreateInstance(typeof(TEditForm), editParameters) as Form;
                theForm.ShowDialog(this);

                if (theForm.DialogResult == DialogResult.OK)
                    ApplyEditsValidateSave(itemsToEdit, editParameters, validationType);

                theForm.Dispose();
            }
        }

        protected List<HaiBusinessObjectBase> DuplicateHaiBusinessObject<TItemToDuplicate, TEditForm>(DataGridView theDatagrid, DateRangeValidationType validationType)
        {
            IUntypedBindingList bindingList = (IUntypedBindingList)((BindingSource)_datagrid.DataSource).List;  // get the data collection
            List<HaiBusinessObjectBase> itemsToDuplicate = GetItemsSelectedForEdit<TItemToDuplicate>(theDatagrid);
            List<HaiBusinessObjectBase> itemsDuplicated = null;

            HaiBusinessObjectBase anyHaiBusinessObject = System.Activator.CreateInstance<TItemToDuplicate>() as HaiBusinessObjectBase;
            Dictionary<string, BrowsableProperty> bpList = anyHaiBusinessObject.GetClassBrowsablePropertyList();

            if (ContinueAfterCheckForChangedItems(itemsToDuplicate, bpList))
            {
                theDatagrid.Refresh();
                List<HaiBusinessObjectBase> unfilteredItems = GetUnfilteredGridItems<TItemToDuplicate>(_datagrid);
                EditFormParameters editParameters = CreateEditFormParametersForDuplicate(itemsToDuplicate, unfilteredItems, bpList);

                HaiObjectEditForm theForm = System.Activator.CreateInstance(typeof(TEditForm), editParameters) as HaiObjectEditForm;
                theForm.ShowDialog(this);

                if (theForm.DialogResult == DialogResult.OK)
                    itemsDuplicated = DuplicateValidateSave(editParameters, bindingList, validationType);

                theForm.Dispose();
            }

            return itemsDuplicated;
        }

        protected void ApplyEditsValidateSave(List<HaiBusinessObjectBase> objectsSelectedForEdit, EditFormParameters editParameters, DateRangeValidationType validationType)
        {
            Cursor = Cursors.WaitCursor;
            string message = string.Empty;

            Dictionary<string, BrowsableProperty> bpList = editParameters.BrowsablePropertyList;
            
            // update property values to those that could have been changed by the user
            foreach (HaiBusinessObjectBase anyHaiBusinessObject in objectsSelectedForEdit)
            {
                bool somePropertyChanged = false;
                anyHaiBusinessObject.BeginEdit();           // BeginEdit() allows for later "undo" by a CancelEdit()

                // update each property
                foreach (string propertyName in bpList.Keys)
                {
                    EditFormDisplayController controllerForProperty = editParameters.ControllerDictionary[propertyName];
                    if (controllerForProperty.IsChangedByEdit)
                    {
                        object propertyValue;
                        propertyValue = Utilities.CallByName(editParameters.EditObject, propertyName, CallType.Get);
                        Utilities.CallByName(anyHaiBusinessObject, propertyName, CallType.Set, propertyValue);
                        somePropertyChanged = true;
                    }
                }

                if (!somePropertyChanged)
                    anyHaiBusinessObject.CancelEdit();      // nothing was changed, so there is no edit -- CancelEdit() the earlier BeginEdit()
                else
                    anyHaiBusinessObject.MarkDirty();       // inidate that the object is now dirty
            }

            // validate date ranges for reverse chronologies and date range overlaps with kindred items
            if (validationType != DateRangeValidationType.DoesNotApply)
            {
                foreach (HaiBusinessObjectBase anyHaiBusinessObject in objectsSelectedForEdit)
                {
                    string checkMessage = UIUtilities.CheckDateRangeForReverseChronologyAndOverlaps((HaiInterfaces.IDateRange)anyHaiBusinessObject, editParameters.UnfilteredItems, validationType);
                    if (checkMessage != string.Empty)
                    {
                        message = message + "\r\n" + checkMessage;
                    }
                }

                if (message != string.Empty)
                {
                    foreach (HaiBusinessObjectBase anyHaiBusinessObject in objectsSelectedForEdit)
                    {
                        anyHaiBusinessObject.CancelEdit();  // aborting the edit/save; undo all edits
                    }

                    MessageBox.Show("Nothing saved.   Date range errors detected.\r\n" + message, "Date range errors", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    Cursor = Cursors.Default;
                    return;
                }
            }

            /* 
             * 
             * NOTES ABOUT TESTING OBJECTS (REPORTS, FOR THE MOMENT) FOR CONFLICTS THAT SHOULD PREVENT SAVING 1/19/2010
             * (THIS ALSO APPLIES TO SAVING A SINGLE OBJECT in EditSingleHaiBusinessObject<TItemToEdit, TEditForm>())
             * 
             * There could be an iteration here that tests each dirty object in the list to test its validity 
             * (for external conflicts).  Each object found to have a conflict will be added to a conflicted list
             * of objects that will NOT be saved.
             * 
             * The user could be presented with a list of conflicted objects and then elect to abort all saving,
             * or just to abort the saving for only those that are conflicted.
             * 
             * If choice is to abort all saving, then each item in the list must be rolled back with .CancelEdit()
             * and then return from here (and reset cursor).
             * 
             * If the user elects to cancel saving for only conflicted items, then the following block should
             * do a .CancelEdit() and "Continue;" for each anyHaiBusinessObject that is in the conflicted list
             * 
             */

            // check for external conflicts that would prevent saving one or more objects
            List<HaiBusinessObjectBase> unsaveableObjectList = new List<HaiBusinessObjectBase>();
            string testFailureMessages = string.Empty;
            foreach (HaiBusinessObjectBase anyHaiBusinessObject in objectsSelectedForEdit)
            {
                if (anyHaiBusinessObject.IsDirty)
                {
                    DataAccessTestResult testResult = _ds.TestObjectForSaveConflicts(anyHaiBusinessObject);
                    if (testResult.Success)
                    {
                        if (testResult.Passed)
                            continue;

                        unsaveableObjectList.Add(anyHaiBusinessObject);
                        testFailureMessages += testResult.ObjectName + ":\n" + testResult.Message + "\n\n";
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show(testResult.ObjectName + " was not saved.  Test for external conflicts failed.\n\n" + testResult.Message, "Data conflict", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }
            }

            // if any conflicted objects are found ask to user whether to abort all saving, or just those that are conflicted.
            if (unsaveableObjectList.Count > 0)
            {
                testFailureMessages = "External conflicts detected!\n\n" + testFailureMessages;
                testFailureMessages = testFailureMessages + "Conflicted items cannot be saved.  Continue and save non-conflicted items?\n\nSelect 'Yes' to save, 'No' to cancel saving all items.";
                DialogResult yesNo = MessageBox.Show(testFailureMessages, "Select save action", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (yesNo == DialogResult.No)
                {
                    Cursor = Cursors.Default;
                    return;
                }
            }

            // save all objects seleced for edit
            // ... each of these must be ApplyEdit() if save was good, or CancelEdit() if save failed
            foreach (HaiBusinessObjectBase anyHaiBusinessObject in objectsSelectedForEdit)
            {
                if (unsaveableObjectList.Contains(anyHaiBusinessObject))
                {
                    anyHaiBusinessObject.CancelEdit();
                    continue;
                }

                if (anyHaiBusinessObject.IsDirty)
                {
                    DataAccessResult result = _ds.Save(anyHaiBusinessObject);

                    if (result.Success)
                        anyHaiBusinessObject.ApplyEdit();       // apply the updates
                    else                                        // UPDATE FAILED: changes need to be rolled back and object made clean (if it was before Save attempt)
                    {
                        anyHaiBusinessObject.CancelEdit();      // undo (roll back) the edits

                        string failedMessage;
                        failedMessage = "Update operation for \"" + anyHaiBusinessObject.UniqueIdentifier + "\" failed.\n\n\rError message: " + result.Message;
                        MessageBox.Show(failedMessage, "Data updating error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }

            Cursor = Cursors.Default;

            // advise user if some objects were not saved.
            if (unsaveableObjectList.Count > 0)
            {
                MessageBox.Show(unsaveableObjectList.Count.ToString() + " item(s) with contflicts were not saved.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected void AddHaiBusinessObject<TNewItem, TEditForm>(IUntypedBindingList untypedBindingList)
        {
            List<HaiBusinessObjectBase> unfilteredItems = GetUnfilteredGridItems<TNewItem>(_datagrid);
            EditFormParameters editParameters = CreateEditFormParametersForNew<TNewItem>(unfilteredItems);

            Form theForm = Activator.CreateInstance(typeof(TEditForm), editParameters) as Form;
            theForm.ShowDialog(this);

            if (theForm.DialogResult == DialogResult.OK)
            {
                Cursor = Cursors.WaitCursor;
                DataAccessResult result = _ds.Save(editParameters.EditObject);
                Cursor = Cursors.Default;

                if (result.Success)
                {
                    editParameters.EditObject.ApplyEdit();
                    untypedBindingList.AddToList(editParameters.EditObject);
                }
                else
                {
                    editParameters.EditObject.CancelEdit();
                    MessageBox.Show(result.Message, "Data save error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
                editParameters.EditObject.CancelEdit();

            theForm.Dispose();
        }

        protected List<HaiBusinessObjectBase> DuplicateValidateSave(EditFormParameters editParameters, IUntypedBindingList untypedBindingList, DateRangeValidationType validationType)
        {
            Dictionary<string, BrowsableProperty> bpList = editParameters.BrowsablePropertyList;

            object propertyValue;
            string message = string.Empty;

            List<HaiBusinessObjectBase> itemsToDuplicate = new List<HaiBusinessObjectBase>();
            List<HaiBusinessObjectBase> itemsDuplicated = null;

            // create a (new) target object for each item that was selected for duplicate
            // ... and add to a list of duplicated items
            foreach (HaiBusinessObjectBase sourceItem in editParameters.ItemsSelectedForEdit)
            {
                HaiBusinessObjectBase itemToDuplicate = (HaiBusinessObjectBase)Activator.CreateInstance(sourceItem.GetType());

                // determine if object has any properties that have been changed; if so, update propertis and mark object as dirty
                foreach (string propertyName in bpList.Keys)
                {
                    propertyValue = Utilities.CallByName(sourceItem, propertyName, CallType.Get);
                    Utilities.CallByName(itemToDuplicate, propertyName, CallType.Set, propertyValue);
                }

                itemsToDuplicate.Add(itemToDuplicate);
            }

            // update each item in the list with properties in the proxy that have been changed by the edit.
            object proxyObject = editParameters.EditObject;
            foreach (HaiBusinessObjectBase itemToDuplicate in itemsToDuplicate)
            {
                // determine if object has any properties that have been changed; if so, update propertis and mark object as dirty
                foreach (string propertyName in bpList.Keys)
                {
                    EditFormDisplayController controllerForProperty = editParameters.ControllerDictionary[propertyName];

                    // if the property was changed by the edit, update the property in anyHaiBusinessObject
                    if (controllerForProperty.IsChangedByEdit)
                    {
                        propertyValue = Utilities.CallByName(proxyObject, propertyName, CallType.Get);
                        Utilities.CallByName(itemToDuplicate, propertyName, CallType.Set, propertyValue);
                    }
                }
            }

            // validate date ranges for reverse chronologies and date range overlaps with kindred items
            if (validationType != DateRangeValidationType.DoesNotApply)
            {
                // create a List<HaiBusinessObjectBase> to hold the universe that will be used to extract kindreds for date range overlap testing
                List<HaiBusinessObjectBase> comparisionUniverse = new List<HaiBusinessObjectBase>(editParameters.UnfilteredItems);
                comparisionUniverse.AddRange(itemsToDuplicate);

                foreach (HaiBusinessObjectBase anyHaiBusinessObject in itemsToDuplicate)
                {
                    string checkMessage = UIUtilities.CheckDateRangeForReverseChronologyAndOverlaps((HaiInterfaces.IDateRange)anyHaiBusinessObject, comparisionUniverse, validationType);
                    if (checkMessage != string.Empty)
                    {
                        message = message + "\r\n" + checkMessage;
                    }
                }

                if (message != string.Empty)
                {
                    foreach (HaiBusinessObjectBase anyHaiBusinessObject in itemsToDuplicate)
                    {
                        anyHaiBusinessObject.CancelEdit();  // aborting the edit/save; undo all edits
                    }

                    MessageBox.Show("Nothing saved.  Date range errors detected.\r\n" + message, "Date range errors", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    Cursor = Cursors.Default;
                    return itemsDuplicated;
                }
            }

            // save each item in the list and add it to the grid if the save was successful
            itemsDuplicated = new List<HaiBusinessObjectBase>();
            foreach (HaiBusinessObjectBase itemToDuplicate in itemsToDuplicate)
            {
                DataAccessResult result = _ds.Save(itemToDuplicate);
                if (result.Success)
                {
                    itemToDuplicate.ApplyEdit();
                    untypedBindingList.AddToList(itemToDuplicate);
                    itemsDuplicated.Add(itemToDuplicate);
                }
                else
                    MessageBox.Show("Save failed for: " + itemToDuplicate.UniqueIdentifier + "\r\r" + result.Message, "Data save error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return itemsDuplicated;
        }

        protected void DuplicateSet<TSet, TSetEditForm>()
        {
            List<HaiBusinessObjectBase> objectsToEdit = GetItemsSelectedForEdit<TSet>(_datagrid);
            if (objectsToEdit.Count != 1)
            {
                MessageBox.Show("A single object must be selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            List<HaiBusinessObjectBase> duplicatedSet = DuplicateHaiBusinessObject<TSet, TSetEditForm>(_datagrid, DateRangeValidationType.DoesNotApply);

            if (duplicatedSet == null)
                return;                     // user cancelled

            if (duplicatedSet.Count == 0)
            {
                MessageBox.Show("Duplication of set failed.", "Duplication error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Cursor = Cursors.WaitCursor;
            DataAccessResult result = _ds.DuplicateSetMembers(objectsToEdit[0], duplicatedSet[0]);
            Cursor = Cursors.Default;
            if (!result.Success)
            {
                MessageBox.Show("Duplication of set members failed.\n\r" + result.Message, "Duplication error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
        }


        protected void ButtonAddDeleteDuplicateDisable()
        {
            btnAdd.Enabled = false;
            btnDelete.Enabled = false;
            btnDuplicate.Enabled = false;
        }

        protected void ButtonAddDeleteDuplicateEnable()
        {
            btnAdd.Enabled = true;
            btnDelete.Enabled = true;
            btnDuplicate.Enabled = true;
        }

        protected void ShowNothingImplemented()
        {
            MessageBox.Show("Nothing implemented.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        protected virtual void SetupForm()
        {
            throw new Exception("SetupForm() procedure has not been overridden");
        }

        #endregion

        #region Menu event handlers

        //Menu: File > Close
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveDataGridControl();

            this.Text = "HAI Meta Data Code Editor";
        }

        //Menu:  File > Exit
        private void menuMainFileExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Menu: Grid  --  set visibility of drop down items on Grid menu
        private void gridToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gridToolStripMenuItem.DropDown.Items["toolStripSeparatorColumnItems"].Visible = true;
            gridToolStripMenuItem.DropDown.Items["columnHeadingStyleToolStripMenuItem"].Visible = true;
            gridToolStripMenuItem.DropDown.Items["columnWidthToolStripMenuItem"].Visible = true;
        }

        // Menu: Tools -- set options
        private void toolsToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (_cacheIsDirty)
                databaseOptionsToolStripMenuItem.Enabled = false;
            else
                databaseOptionsToolStripMenuItem.Enabled = true;
        }

        private void hideReportSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _gus.HideReportSelection = hideReportSelectionToolStripMenuItem.Checked;
        }

        private void databaseOptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OptionsForm theForm = new OptionsForm(_gus);
            theForm.ShowDialog();

            if (theForm.DialogResult == DialogResult.OK)
            {
                closeToolStripMenuItem_Click(null, null);
                this.Text = "";     // force TextChanged event so that subclass will refresh its title
                SetupForm();
            }

            theForm.Dispose();
        }

        #endregion

        private void menuMain_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

    }
}