using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;

using System.ComponentModel;

using HaiBusinessObject;
using HaiInterfaces;

namespace HaiBusinessUI
{
    public enum ComboBoxListType
    {
        DataListOnly,
        DataListWithNullItem,
        DataListWithBlankItem
    }

    public partial class HaiObjectEditForm : Form
    {
        protected const string NoValueToShow = "--";
        protected EditFormParameters _parameters;
        protected Dictionary<string, BrowsableProperty> _browsablePropertyList;
        protected Dictionary<string, EditFormDisplayController> _controllerList;
        protected Dictionary<string, Control> _allPropertyControls;
        protected Dictionary<string, TextBox> _integerControls;
        protected Dictionary<string, TextBox> _floatControls;
        protected Dictionary<string, TextBox> _textControls;
        protected Dictionary<string, ComboBox> _comboControls;
        protected Dictionary<string, TextBox> _dateControls;
        protected Dictionary<string, CheckBox> _booleanControls;

        private string _nullString = Utilities.GetStringForNull();
        private Control _previousControl;

        public HaiObjectEditForm()
        {
            InitializeComponent();

            this.CancelButton = btnCancel;
            this.AcceptButton = btnOK;
        }

        #region Event Handlers

        void HandleIntegerFormat(object sender, ConvertEventArgs e)
        {
            Control sendingControl = ((Binding)(sender)).Control;
            TextBox anyTextBox = (TextBox)sendingControl;

            ControlTag controlTag = (ControlTag)(anyTextBox.Tag);
            if (controlTag.ControlAccessState == ControlAccessState.NotInitialized || controlTag.ControlAccessState == ControlAccessState.MultiValueIsLocked)
            {
                e.Value = NoValueToShow;
            }
        }

        void HandleIntegerParse(object sender, ConvertEventArgs e)
        {
            Control sendingControl = ((Binding)(sender)).Control;

            // allow Cancel button to bypass validation and close the form
            if (btnCancel.Focused)
            {
                this.Close();
                return;
            }

            // if the sending control is readonly, bypass validation b/c user cannot change
            if (sendingControl is TextBox)
            {
                if (((TextBox)sendingControl).ReadOnly && (((ControlTag)sendingControl.Tag).IsReadOnlyForMultiEdit || ((TextBox)sendingControl).Text == NoValueToShow))
                {
                    e.Value = null;
                    return;
                }
            }

            // validate input as something that can convert to an integer
            string value = e.Value.ToString();
            int i;
            if (!int.TryParse(value, out i))
            {
                if (value == _nullString)
                {
                    e.Value = null;
                }
                else
                {
                    MessageBox.Show("Numeric entry required.", "Input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    ((TextBox)sendingControl).Undo();
                }
            }

        }

        void HandleDateValidating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (btnCancel.Focused)
            {
                e.Cancel = true;
                this.Close();
                return;
            }

            TextBox theTextBox = (TextBox)sender;

            if (theTextBox.ReadOnly)
                return;

            DateTime theDate;
            string text = theTextBox.Text.Trim();

            if (text == "<<" || text == "open begin date" || text == "(open begin date)")
            {
                theTextBox.Text = "(open begin date)";
                return;
            }

            if (text == ">>" || text == "open end date" || text == "(open end date)")
            {
                theTextBox.Text = "(open end date)";
                return;
            }

            BindingSource bindingSource = (BindingSource)theTextBox.DataBindings["Text"].DataSource;
            string propertyName = theTextBox.DataBindings["Text"].BindingMemberInfo.BindingField;
            object target = bindingSource.Current;
            System.Reflection.PropertyInfo pInfo = target.GetType().GetProperty(propertyName);
            object oldValue = pInfo.GetValue(target, null);

            bool ok = DateTime.TryParse(text, out theDate);

            if (ok)
            {
                if (theDate < Utilities.GetOpenBeginDate().Date || theDate > Utilities.GetOpenEndDate().Date)
                {
                    MessageBox.Show("Date must be between " + Utilities.GetOpenBeginDate().Date.ToShortDateString()
                        + " and " + Utilities.GetOpenEndDate().Date.ToShortDateString(), "Data entry error",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    theTextBox.Text = ((DateTime)oldValue).ToShortDateString();
                    theTextBox.SelectAll();
                    theTextBox.Focus();

                    e.Cancel = true;
                }
                else
                    theTextBox.Text = theDate.ToShortDateString();
            }
            else
            {
                MessageBox.Show("A valid date, << \"(open begin date)\" or >> \"(open end date)\" must be entered.", "Data entry error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                theTextBox.Text = ((DateTime)oldValue).ToShortDateString();
                theTextBox.SelectAll();
                theTextBox.Focus();

                e.Cancel = true;
            }
        }

        void HandleDateParse(object sender, ConvertEventArgs e)
        {
            string theText = e.Value.ToString().Trim();
            DateTime theDate;

            bool ok = DateTime.TryParse(theText, out theDate);

            if (ok)
                e.Value = theDate.Date;
            else
            {
                if (theText == NoValueToShow)
                    e.Value = null;

                if (theText == "(open begin date)")
                    e.Value = Utilities.GetOpenBeginDate();

                if (theText == "(open end date)")
                    e.Value = Utilities.GetOpenEndDate();
            }
        }

        void HandleDateFormat(object sender, ConvertEventArgs e)
        {
            if (((TextBox)(((Binding)sender).Control)).ReadOnly == true)    // the TextBox is multivalued; format as no value
            {
                e.Value = NoValueToShow;
                return;
            }

            object aValue = e.Value;

            if (aValue is DateTime)
            {
                DateTime aDate = (DateTime)aValue;
                if (aDate == Utilities.GetOpenBeginDate())
                    e.Value = "(open begin date)";

                if (aDate == Utilities.GetOpenEndDate())
                    e.Value = "(open end date)";

                if (aDate == DateTime.MinValue.Date || aDate == DateTime.MaxValue.Date)
                    e.Value = "";
            }
            else
            {
                e.Value = NoValueToShow;
            }
        }

        void HandleFloatFormat(object sender, ConvertEventArgs e)
        {
            Control sendingControl = ((Binding)(sender)).Control;
            TextBox anyTextBox = (TextBox)sendingControl;

            ControlTag controlTag = (ControlTag)(anyTextBox.Tag);
            if (controlTag.ControlAccessState == ControlAccessState.NotInitialized || controlTag.ControlAccessState == ControlAccessState.MultiValueIsLocked)
            {
                e.Value = NoValueToShow;
            }
        }

        void HandleFloatParse(object sender, ConvertEventArgs e)
        {
            Control sendingControl = ((Binding)(sender)).Control;

            // allow Cancel button to bypass validation and close the form
            if (btnCancel.Focused)
            {
                this.Close();
                return;
            }

            // if the sending control is readonly, bypass validation b/c user cannot change
            if (sendingControl is TextBox)
            {
                if (((TextBox)sendingControl).ReadOnly)
                {
                    e.Value = null;
                    return;
                }
            }

            // validate input as something that can convert to double
            string value = e.Value.ToString();
            double x;
            if (!Double.TryParse(value, out x))
            {
                if (value == _nullString)
                {
                    e.Value = null;
                }
                else
                {
                    MessageBox.Show("Numeric entry required.", "Input error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    ((TextBox)sendingControl).Undo();
                }
            }
        }

        void HandleStringParse(object sender, ConvertEventArgs e)
        {
            Control sendingControl = ((Binding)(sender)).Control;
            if (e.Value.ToString() == _nullString)
            {
                e.Value = null;
            }
        }

        private void btnInsertNull_Click(object sender, EventArgs e)
        {
            TextBox anyTextBox = (TextBox)_previousControl;
            anyTextBox.Text = _nullString;
            anyTextBox.DataBindings["Text"].WriteValue();                       // data value was set by code above, must be forced to bound object
        }

        protected void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox anyTextBox = (TextBox)sender;
            ControlTag controlTag = (ControlTag)anyTextBox.Tag;

            if (e.Control && e.KeyCode == Keys.U)                               // This is a Ctrl-U keystroke
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                if (anyTextBox.ReadOnly)
                {
                    anyTextBox.DataBindings["Text"].NullValue = _nullString;    // can now edit, so show "real" null string
                    anyTextBox.ReadOnly = false;
                    controlTag.ControlAccessState = ControlAccessState.MultiValueIsUnlocked;
                    anyTextBox.Text = string.Empty;
                    anyDataControl_GotFocus(sender, null);
                }
                else
                {
                    anyTextBox.DataBindings["Text"].NullValue = "";             // cannot edit, so use "" as null b/c property in edit obj for multi-value is null
                    anyTextBox.Text = NoValueToShow;
                    anyTextBox.Select(anyTextBox.Text.Length, 0);
                    anyTextBox.ReadOnly = true;
                    controlTag.ControlAccessState = ControlAccessState.MultiValueIsLocked;
                    anyDataControl_GotFocus(sender, null);
                }
            }
        }

        protected void ComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.U)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;

                ComboBox anyComboBox = (ComboBox)sender;
                ControlTag comboBoxControlTag = (ControlTag)anyComboBox.Tag;

                Control dependentControl = comboBoxControlTag.RelatedControl;
                ControlTag dependentControlTag = (ControlTag)dependentControl.Tag;

                if (anyComboBox.Items.Count == 0)       // unlock the combobox
                {
                    anyComboBox.BackColor = SystemColors.Window;
                    comboBoxControlTag.ControlAccessState = ControlAccessState.MultiValueIsUnlocked;
                    dependentControlTag.ControlAccessState = ControlAccessState.MultiValueIsUnlocked;
                    SetListForComboBox(anyComboBox);
                    anyComboBox.SelectedIndex = 0;
                }
                else                                    // lock the combobox
                {
                    anyComboBox.DroppedDown = false;
                    anyComboBox.BackColor = SystemColors.Control;
                    comboBoxControlTag.ControlAccessState = ControlAccessState.MultiValueIsLocked;
                    dependentControlTag.ControlAccessState = ControlAccessState.MultiValueIsLocked;
                    anyComboBox.SelectedItem = null;
                    if (anyComboBox.DataSource == null)
                        anyComboBox.Items.Clear();          // old style
                    else
                        anyComboBox.DataSource = null;      // new style

                    anyComboBox.Text = "";
                }
            }
        }

        #endregion

        #region Utililites

        protected void PrepareTheFormToShow(EditFormParameters parameters)
        {
            _controllerList = parameters.ControllerDictionary;
            _browsablePropertyList = parameters.BrowsablePropertyList;

            _allPropertyControls = new Dictionary<string, Control>();
            _integerControls = new Dictionary<string, TextBox>();
            _floatControls = new Dictionary<string, TextBox>();
            _textControls = new Dictionary<string, TextBox>();
            _comboControls = new Dictionary<string, ComboBox>();
            _dateControls = new Dictionary<string, TextBox>();
            _booleanControls = new Dictionary<string, CheckBox>();

            foreach (string propertyName in _browsablePropertyList.Keys)
            {
                BrowsableProperty bp = _browsablePropertyList[propertyName];
                System.Windows.Forms.Form.ControlCollection allControls = (System.Windows.Forms.Form.ControlCollection)this.Controls;
                Control anyControl = null;
                ControlTag controlTag = new ControlTag();

                if (allControls.ContainsKey(propertyName + "TextBox"))
                {
                    anyControl = allControls[propertyName + "TextBox"];
                    _allPropertyControls.Add(propertyName, anyControl);
                }

                if (allControls.ContainsKey(propertyName + "ComboBox"))
                {
                    anyControl = allControls[propertyName + "ComboBox"];
                    _allPropertyControls.Add(propertyName, anyControl);
                }

                if (allControls.ContainsKey(propertyName + "DateTimePicker"))
                {
                    anyControl = allControls[propertyName + "DateTimePicker"];
                    _allPropertyControls.Add(propertyName, anyControl);
                }

                if (anyControl != null )     // some sort of text-based control has been found
                {
                    anyControl.Tag = controlTag;

                    controlTag.BrowsableProperty = bp;
                    if (bp.IsReadonly)
                        controlTag.ControlAccessState = ControlAccessState.ValueIsReadOnly;
                    else
                        controlTag.ControlAccessState = ControlAccessState.NotInitialized;
                      

                    switch (bp.DataType)
                    {
                        case TypeCode.DateTime :
                            if (anyControl is TextBox)
                            {
                                TextBox anyTextBox = (TextBox)anyControl;
                                Binding textBinding = anyTextBox.DataBindings["Text"];

                                _dateControls.Add(propertyName,(TextBox)anyControl);
                                if (bp.IsNullable)
                                {
                                    textBinding.Parse += new ConvertEventHandler(HandleDateParse);
                                    textBinding.NullValue = _nullString;
                                }

                                if (bp.DataType == TypeCode.DateTime)
                                {
                                    textBinding.Format += new ConvertEventHandler(HandleDateFormat);
                                    textBinding.Parse += new ConvertEventHandler(HandleDateParse);
                                    anyTextBox.Validating += new System.ComponentModel.CancelEventHandler(HandleDateValidating);
                                }
                            }

                            break;

                        case TypeCode.String :
                            if (anyControl is TextBox)
                            {
                                _textControls.Add(propertyName, (TextBox)anyControl);
                                if (bp.IsNullable)
                                {
                                    if (((TextBox)anyControl).DataBindings["Text"] != null)
                                    {
                                        ((TextBox)anyControl).DataBindings["Text"].Parse += new ConvertEventHandler(HandleStringParse);
                                        ((TextBox)anyControl).DataBindings["Text"].NullValue = _nullString;
                                    }
                                }

                                if (bp.MaxStringLength>0)
                                    ((TextBox)anyControl).MaxLength = bp.MaxStringLength;
                            }
                            if (anyControl is ComboBox)
                            {
                                _comboControls.Add(propertyName, (ComboBox)anyControl);
                                if (bp.IsNullable)
                                {
                                    if (((ComboBox)anyControl).DataBindings["Text"] != null)
                                    {
                                        ((ComboBox)anyControl).DataBindings["Text"].Parse += new ConvertEventHandler(HandleStringParse);
                                        ((ComboBox)anyControl).DataBindings["Text"].NullValue = _nullString;
                                    }
                                }
                            }
                            break;

                        case TypeCode.Int32 :
                            _integerControls.Add(propertyName, (TextBox)anyControl);
                            if (bp.IsNullable)
                            {
                                if (((TextBox)anyControl).DataBindings["Text"] != null)
                                {
                                    ((TextBox)anyControl).DataBindings["Text"].NullValue = _nullString;
                                }
                            }
                            if (((TextBox)anyControl).DataBindings["Text"] != null)
                            {
                                ((TextBox)anyControl).DataBindings["Text"].Parse += new ConvertEventHandler(HandleIntegerParse);
                                ((TextBox)anyControl).DataBindings["Text"].Format+=new ConvertEventHandler(HandleIntegerFormat);
                            }

                            ((TextBox)anyControl).TextAlign = HorizontalAlignment.Right;

                            break;

                        case TypeCode.Double :
                            _floatControls.Add(propertyName, (TextBox)anyControl);
                            ((TextBox)anyControl).DataBindings["Text"].Parse += new ConvertEventHandler(HandleFloatParse);
                            if (bp.IsNullable)
                            {
                                if (((TextBox)anyControl).DataBindings["Text"] != null)
                                {
                                    ((TextBox)anyControl).DataBindings["Text"].NullValue = _nullString;
                                }
                            }

                            if (((TextBox)anyControl).DataBindings["Text"] != null)
                            {
                                ((TextBox)anyControl).DataBindings["Text"].Format+=new ConvertEventHandler(HandleFloatFormat);
                            }

                            ((TextBox)anyControl).TextAlign = HorizontalAlignment.Right;

                            break;

                        default :
                            throw new Exception("Unexpected data type: " + bp.DataType.ToString());
                    }
                }
                else                // not tested; is it a checkbox?
                {
                    if (allControls.ContainsKey(propertyName + "CheckBox"))
                    {
                        anyControl = allControls[propertyName + "CheckBox"];
                        _allPropertyControls.Add(propertyName, anyControl);
                        _booleanControls.Add(propertyName, (CheckBox)anyControl);
                        if (bp.IsNullable)
                        {
                            CheckBox cb = (CheckBox)anyControl;
                            cb.ThreeState = true;
                        }
                    }
                }


                if (anyControl == null) continue;       // no control (of above types) found for this property

                Control thisControl = _allPropertyControls[propertyName];
                EditFormDisplayController controller = _controllerList[propertyName];

                if (controller.IsHidden)
                {
                    thisControl.Visible = false;
                    continue;
                }

                if (controller.IsReadonly)
                {
                    if (thisControl is TextBox)
                        ((TextBox)thisControl).ReadOnly = true;
                    else
                        thisControl.Enabled = false;

                    continue;
                }

                controlTag.MultipleValuesInUnderlyingData = controller.IsMultivalued;
                if (!controller.IsMultivalued)
                {
                    if (controlTag.ControlAccessState != ControlAccessState.ValueIsReadOnly)
                        controlTag.ControlAccessState = ControlAccessState.SingleValueIsWriteable;
                }
                else
                {
                    if (controller.MustBeUnique)                    // nothing to edit with multiple values that must be unique
                    {
                        thisControl.Enabled = false;
                    }
                    else
                    {
                        if (thisControl is TextBox)
                        {
                            thisControl.KeyDown += new KeyEventHandler(TextBox_KeyDown);
                            ((TextBox)thisControl).ReadOnly = true;
                            ((TextBox)thisControl).DataBindings["Text"].NullValue = ""; // make  null = "" b/c edit obj has value as null for multi-values
                            ((TextBox)thisControl).Text = NoValueToShow;
                        }

                        if (thisControl is CheckBox)
                        {
                            ((CheckBox)thisControl).ThreeState = true;
                            ((CheckBox)thisControl).CheckState = CheckState.Indeterminate;
                            thisControl.DataBindings.Clear();       // cannot use databinding for a ThreeState checkbox b/c object property is Boolean.
                        }

                        if (thisControl is ComboBox)
                        {
                            ((ComboBox)thisControl).Text = string.Empty;
                            ((ComboBox)thisControl).Items.Clear();
                            ((ComboBox)thisControl).KeyDown += new KeyEventHandler(ComboBox_KeyDown);
                            ((ComboBox)thisControl).BackColor = SystemColors.Control;
                        }

                        if (controlTag.ControlAccessState != ControlAccessState.ValueIsReadOnly)
                            controlTag.ControlAccessState = ControlAccessState.MultiValueIsLocked;
                    }
                }

                if (parameters.NumberOfItemsToEdit > 1 && controller.MustBeUnique)
                    anyControl.Enabled = false;

                anyControl.GotFocus += new EventHandler(anyDataControl_GotFocus);
                btnInsertNull.Enabled = false;
            }

            string titleText;
            switch (parameters.NumberOfItemsToEdit)
            {
                case 0:
                    titleText = "Edit item to add";
                    break;

                case 1:
                    titleText = "Edit one existing item";
                    break;

                case -1:
                    titleText = "Duplicate one existing item";
                    break;

                default:
                    if (parameters.NumberOfItemsToEdit > 0)
                        titleText = "Edit " + parameters.NumberOfItemsToEdit.ToString() + " existing items";
                    else
                        titleText = "Duplicate " + (Math.Abs(parameters.NumberOfItemsToEdit)).ToString() + " existing items";

                    break;
            }

            this.Text = this.Text + ": " + titleText;

        }

        protected void anyDataControl_GotFocus(object sender, EventArgs e)
        {
            _previousControl = (Control)sender;
            Control focusedControl = (Control)sender;
            if (focusedControl is TextBox)
            {
                BrowsableProperty bp = ((ControlTag)(focusedControl.Tag)).BrowsableProperty;
                if (bp.IsNullable && !((TextBox)focusedControl).ReadOnly)
                    btnInsertNull.Enabled = true;
                else
                    btnInsertNull.Enabled = false;
            }
            else
            {
                btnInsertNull.Enabled = false;
            }
        }

        protected void SetFormVariables(EditFormParameters parameters)
        {
            _parameters = parameters;
            _controllerList = parameters.ControllerDictionary;
            _browsablePropertyList = parameters.BrowsablePropertyList;
        }

        protected void MarkChangedProperties(object targetObject)
        {
            foreach (string propertyName in _allPropertyControls.Keys)
            {
                Control propertyControl = _allPropertyControls[propertyName];

                if (propertyControl is TextBox)
                {
                    TextBox anyTextBox = (TextBox)propertyControl;
                    ControlTag controlTag = (ControlTag)anyTextBox.Tag;
                    if ((controlTag.ControlAccessState == ControlAccessState.SingleValueIsWriteable) || (controlTag.ControlAccessState == ControlAccessState.MultiValueIsUnlocked))
                    {
                        if (anyTextBox.DataBindings["Text"] != null)
                            anyTextBox.DataBindings["Text"].WriteValue();
                        _controllerList[propertyName].IsChangedByEdit = true;
                    }
                }

                if (propertyControl is ComboBox)
                {
                    ComboBox anyComboBox = (ComboBox)propertyControl;
                    if (anyComboBox.Items.Count > 0 && anyComboBox.Enabled)     // count==0 indicates multivalue; do not set property
                    {
                        if (anyComboBox.DataBindings["Text"] != null)
                            anyComboBox.DataBindings["Text"].WriteValue();
                        _controllerList[propertyName].IsChangedByEdit = true;
                    }
                }

                if (propertyControl is DateTimePicker)
                {
                    DateTimePicker anyDateTimePicker = (DateTimePicker)propertyControl;
                    if (anyDateTimePicker.Enabled)
                    {
                        if (anyDateTimePicker.DataBindings["Value"] != null)
                            anyDateTimePicker.DataBindings["Value"].WriteValue();
                        _controllerList[propertyName].IsChangedByEdit = true;
                    }
                }

                if (propertyControl is CheckBox)
                {
                    CheckBox anyCheckBox = (CheckBox)propertyControl;
                    if (anyCheckBox.CheckState != CheckState.Indeterminate && anyCheckBox.Enabled)
                    {
                        // since a ThreeState CheckBox control cannot be databound the value in the target is set here.
                        bool isTrue = (anyCheckBox.CheckState == CheckState.Checked);
                        Utilities.CallByName(targetObject, propertyName, CallType.Set, isTrue);
                        _controllerList[propertyName].IsChangedByEdit = true;
                    }

                    // N.B. _parameters.NumberOfItemsToEdit <- negative means this is from a duplicate operation; but really need,
                    // ... the number of items, so take the absolute value
                    if (Math.Abs(_parameters.NumberOfItemsToEdit) < 2 && anyCheckBox.CheckState == CheckState.Indeterminate)
                    {
                        Utilities.CallByName(targetObject, propertyName, CallType.Set, null);
                    }
                }
            }
        }

        protected void SetListForComboBox(ComboBox anyComboBox)
        {
            ControlTag controlTag = (ControlTag)(anyComboBox.Tag);

            // old style
            if (controlTag.ComboBoxListItems != null)
            {
                if (anyComboBox.BackColor == SystemColors.Window)
                {
                    anyComboBox.Items.Clear();
                    anyComboBox.Items.AddRange((HaiBusinessObjectBase[])(controlTag.ComboBoxListItems));

                    // enable autocomplete functionality
                    anyComboBox.Sorted = true;
                    anyComboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
                    anyComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                }
                else
                {
                    anyComboBox.SelectedItem = null;
                    anyComboBox.Items.Clear();
                    anyComboBox.Text = "";
                }
            }

            // new style
            if (controlTag.BindingList != null)
            {
                if (anyComboBox.BackColor == SystemColors.Window)
                {
                    anyComboBox.DataSource = null;
                    anyComboBox.DataSource = controlTag.BindingList;
                    anyComboBox.DisplayMember = controlTag.DisplayMemberName;
                    anyComboBox.ValueMember = controlTag.ValueMemberName;
                }
                else
                {
                    anyComboBox.SelectedItem = null;
                    anyComboBox.DataSource = null;
                }
            }
        }

        protected string GetKeyForValueInHashTbl(System.Collections.Hashtable anyHashTbl, object valueToFind)
        {
            string key = "";

            if (valueToFind != null)
            {
                foreach (string anyKey in anyHashTbl.Keys)
                {
                    if (valueToFind.Equals(anyHashTbl[anyKey]))
                    {
                        return anyKey;
                    }
                }
            }

            return key;
        }

        protected string ValidateDaterange(HaiBusinessObjectBase anyBusinessObject, DateRangeValidationType validationType)
        {
            IDateRange anyObjectWithDaterange = anyBusinessObject as IDateRange;
            if (anyObjectWithDaterange == null)
                return string.Empty;                // IDateRange is not implemented, nothing to validate
            else
                return anyObjectWithDaterange.DateRange.ValidateRangeForType(validationType);
        }

        protected void SetupAnyComboBoxForDropDownList<T>(ComboBox anyComboBox, DataAccessResult result, ComboBoxListType listType, string displayPropertyName, string dependentPropertyName, TextBox dependentValueTextBox, string initialText)
        {
            SetupAnyComboBoxForDropDownList<T>(anyComboBox, result, listType, displayPropertyName, dependentPropertyName, dependentValueTextBox, initialText, null);
        }

        protected void SetupAnyComboBoxForDropDownList<T>(ComboBox anyComboBox, DataAccessResult result, ComboBoxListType listType, string displayPropertyName, string dependentPropertyName, TextBox dependentValueTextBox, string initialText, string targetDependentItemTextPropertyName)
        {
            if (!result.Success)
            {
                throw new Exception("Failed to retrieve data for lookup");
            }

            HaiBindingList<T> bindingList = (HaiBindingList<T>)result.DataList;

            // if null is an allowed value, then add a new object to the list
            if (listType == ComboBoxListType.DataListWithNullItem || listType == ComboBoxListType.DataListWithBlankItem)
            {
                T anyHBO = Activator.CreateInstance<T>();
                bindingList.AddToList(anyHBO);
            }

            // setup the textBox that will show the dependent value
            dependentValueTextBox.ReadOnly = true;

            // set properties in controlTag
            ControlTag comboboxControlTag = (ControlTag)anyComboBox.Tag;
            comboboxControlTag.ComboBoxListType = listType;
            comboboxControlTag.BindingList = bindingList;
            comboboxControlTag.DisplayMemberName = displayPropertyName;
            comboboxControlTag.ValueMemberName = dependentPropertyName;
            comboboxControlTag.RelatedControl = dependentValueTextBox;    // attach a reference to the TextBox that will display the related value
            if (targetDependentItemTextPropertyName == null || targetDependentItemTextPropertyName.Trim() == string.Empty)
            {
                comboboxControlTag.TargetRelatedItemTextPropertyName = displayPropertyName;
            }
            else
            {
                comboboxControlTag.TargetRelatedItemTextPropertyName = targetDependentItemTextPropertyName.Trim();
            }

            // sort in binding list b/c cannot use Sort property of combobox for bound data
            bindingList.SortOnProperty(displayPropertyName, ListSortDirection.Ascending);

            // setup the comboBox as a DropDown listbox with AutoComplete
            anyComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            anyComboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
            anyComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            // bind the lookup data list to the combobox
            ControlTag dependentControlTag = (ControlTag)dependentValueTextBox.Tag;
            dependentControlTag.ControlAccessState = comboboxControlTag.ControlAccessState;
            if (comboboxControlTag.MultipleValuesInUnderlyingData)
            {
                anyComboBox.DataSource = null;
                dependentValueTextBox.Text = NoValueToShow;
            }
            else
            {
                anyComboBox.DataSource = bindingList;
            }

            anyComboBox.DisplayMember = displayPropertyName;
            anyComboBox.ValueMember = dependentPropertyName;

            // setup event handler(s) for selection changes
            anyComboBox.SelectedIndexChanged += new EventHandler(anyComboBoxDropDownListHasChanged);
            anyComboBox.TextChanged += new EventHandler(anyComboBoxDropDownListHasChanged);
            try
            {
                // set the initial text in the comboBox
                anyComboBox.Text = initialText;
                if (initialText == string.Empty)    // if initialText is empty string, force a call for changed combobox
                {
                    anyComboBoxDropDownListHasChanged(anyComboBox, null);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void anyComboBoxDropDownListHasChanged(object sender, EventArgs e)
        {
            ComboBox anyComboBox = (ComboBox)sender;
            ControlTag comboBoxControlTag = (ControlTag)anyComboBox.Tag;
            TextBox dependentValueTextBox = (TextBox)((comboBoxControlTag).RelatedControl);
            ControlTag dependentValueControlTag= (ControlTag)(dependentValueTextBox.Tag);

            HaiBusinessObjectBase anyHBO = (HaiBusinessObjectBase)anyComboBox.SelectedItem;

            // bail out b/c the ComboBox is empty
            if (anyHBO == null)
            {
                dependentValueTextBox.Text = NoValueToShow;
                return;
            }

            if (dependentValueTextBox.DataBindings["Text"] != null)
            {
                dependentValueTextBox.DataBindings["Text"].WriteValue();

                // get the object bound to the textbox and update the related text value
                BindingSource bindingSource = (BindingSource)dependentValueTextBox.DataBindings["Text"].DataSource;
                object editObject = bindingSource.DataSource;
                string textPropertyName = comboBoxControlTag.TargetRelatedItemTextPropertyName;
                Utilities.CallByName(editObject, textPropertyName, CallType.Set, anyComboBox.Text.Trim());
            }

            if (anyHBO.IsNew)       // A "new" object is used to indicate a null selection or that "nothing" has been selected
            {
                if (comboBoxControlTag.ComboBoxListType == ComboBoxListType.DataListWithNullItem)
                {
                    dependentValueTextBox.Text = _nullString;
                    dependentValueControlTag.ControlAccessState = comboBoxControlTag.ControlAccessState;
                    if (dependentValueTextBox.DataBindings["Text"] != null)
                        dependentValueTextBox.DataBindings["Text"].WriteValue();
                }

                if (comboBoxControlTag.ComboBoxListType == ComboBoxListType.DataListWithBlankItem)
                {
                    dependentValueTextBox.Text = NoValueToShow;
                    dependentValueControlTag.ControlAccessState = ControlAccessState.NotInitialized;
                }
            }
            else
            {
                object relatedValue = anyComboBox.SelectedValue;
                dependentValueTextBox.Text = relatedValue.ToString();
                if (dependentValueTextBox.DataBindings["Text"]!=null)
                    dependentValueTextBox.DataBindings["Text"].WriteValue();
                dependentValueControlTag.ControlAccessState = comboBoxControlTag.ControlAccessState;
            }

        }

        protected void SetupAnyComboBoxForDropDown<T>(ComboBox anyComboBox, DataAccessResult result, ComboBoxListType listType, string displayPropertyName, string dependentPropertyName, TextBox dependentValueTextBox, string initialText)
        {
            SetupAnyComboBoxForDropDown<T>(anyComboBox, result, listType, displayPropertyName, dependentPropertyName, dependentValueTextBox, initialText, null);
        }

        protected void SetupAnyComboBoxForDropDown<T>(ComboBox anyComboBox, DataAccessResult result, ComboBoxListType listType, string displayPropertyName, string dependentPropertyName, TextBox dependentValueTextBox, string initialText, string targetDependentItemTextPropertyName)
        {
            if (!result.Success)
            {
                throw new Exception("Failed to retrieve data for lookup");
            }

            HaiBindingList<T> bindingList = (HaiBindingList<T>)result.DataList;

            // if null is an allowed value, then add a new object to the list
            if (listType == ComboBoxListType.DataListWithNullItem || listType == ComboBoxListType.DataListWithBlankItem)
            {
                T anyHBO = Activator.CreateInstance<T>();
                bindingList.AddToList(anyHBO);
            }

            // setup the textBox that will show the dependent value
            dependentValueTextBox.ReadOnly = true;

            // set properties in controlTag
            ControlTag controlTag = (ControlTag)anyComboBox.Tag;
            controlTag.ComboBoxListType = listType;
            controlTag.BindingList = bindingList;
            controlTag.DisplayMemberName = displayPropertyName;
            controlTag.ValueMemberName = dependentPropertyName;
            controlTag.RelatedControl = dependentValueTextBox;    // attach a reference to the TextBox that will display the related value
            if (targetDependentItemTextPropertyName == null || targetDependentItemTextPropertyName.Trim() == string.Empty)
            {
                controlTag.TargetRelatedItemTextPropertyName = displayPropertyName;
            }
            else
            {
                controlTag.TargetRelatedItemTextPropertyName = targetDependentItemTextPropertyName.Trim();
            }

            // sort in binding list b/c cannot use Sort property of combobox for bound data
            bindingList.SortOnProperty(displayPropertyName, ListSortDirection.Ascending);

            // setup the comboBox as a DropDown listbox with AutoComplete
            anyComboBox.DropDownStyle = ComboBoxStyle.DropDown;
            anyComboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
            anyComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            // bind the lookup data list to the combobox
            if (controlTag.MultipleValuesInUnderlyingData)
            {
                anyComboBox.DataSource = null;
                dependentValueTextBox.Text = NoValueToShow;
            }
            else
            {
                anyComboBox.DataSource = bindingList;
            }

            anyComboBox.DisplayMember = displayPropertyName;
            anyComboBox.ValueMember = dependentPropertyName;

            // setup event handler(s) for selection changes
            anyComboBox.TextChanged += new EventHandler(anyComboBoxDropDownHasChanged);
            anyComboBox.SelectedIndexChanged += new EventHandler(anyComboBoxDropDownHasChanged);
            anyComboBox.Leave += new EventHandler(anyComboBoxDropDownHasChanged);

            // set the initial text in the comboBox
            anyComboBox.SelectedItem = null;
            anyComboBox.Text = initialText;
        }

        protected void anyComboBoxDropDownHasChanged(object sender, EventArgs e)
        {
            ComboBox anyComboBox = (ComboBox)sender;
            TextBox dependentValueTextBox = (TextBox)((ControlTag)anyComboBox.Tag).RelatedControl;

            HaiBusinessObjectBase anyHBO = (HaiBusinessObjectBase)anyComboBox.SelectedItem;

            //((ControlTag)dependentValueTextBox.Tag).DoNotAllowWrite = false;

            if (anyComboBox.SelectedItem == null)       // Unrelated text has been entered
            {
                dependentValueTextBox.Text = _nullString;
            }
            else
            {
                object relatedValue = anyComboBox.SelectedValue;
                dependentValueTextBox.Text = relatedValue.ToString();
            }

            if (dependentValueTextBox.DataBindings["Text"] != null)
            {
                dependentValueTextBox.DataBindings["Text"].WriteValue();

                // get the object bound to the textbox and update the related text value
                BindingSource bindingSource = (BindingSource)dependentValueTextBox.DataBindings["Text"].DataSource;
                object editObject = bindingSource.DataSource;
                ControlTag controlTag = (ControlTag)anyComboBox.Tag;
                string textPropertyName = controlTag.TargetRelatedItemTextPropertyName;
                Utilities.CallByName(editObject, textPropertyName, CallType.Set, anyComboBox.Text.Trim());
            }
        }

        protected bool IsControlPropertyMultivalued(Control anyControl)
        {
            string propertyName = ((ControlTag)(anyControl.Tag)).BrowsableProperty.PropertyName;
            bool isMultivalued = _controllerList[propertyName].IsMultivalued;

            return isMultivalued;
        }

        #endregion
    }
}