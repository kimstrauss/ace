﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Microsoft.Reporting.WinForms;
using System.Reflection;

using HaiMetaDataDAL;
using HaiBusinessObject;

namespace HaiBusinessUI
{
    public partial class MetaDataReportViewer : Form
    {
        private IUntypedBindingList _bindingList;
        private bool _hideChoiceDialog;
        LocalReport _theReport;

        // Constructor
        public MetaDataReportViewer(IUntypedBindingList bindingList, bool defaultReportOnly)
        {
            InitializeComponent();

            _bindingList = bindingList;
            _hideChoiceDialog = defaultReportOnly;
        }

        // Form load event
        private void MetaDataReportViewer_Load(object sender, EventArgs e)
        {
            ReportSpecification reportToShow = null;

            // set up a binding source for use in the report data source
            BindingSource bindingSource = new BindingSource();
            bindingSource.DataSource = _bindingList;

            // check to be sure there is at least on item in the binding list
            if (_bindingList.Count == 0)
            {
                MessageBox.Show("No data to show.\n\n\rReport not created.", "Report error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            // use the first item in the list to access the report specification dictionary
            HaiBusinessObjectBase anyBusinessObject = (HaiBusinessObjectBase)(_bindingList[0]);
            Dictionary<string, ReportSpecification> reportSpecificationDict = anyBusinessObject.GetReportSpecificationDict();
            if (reportSpecificationDict.Count == 0)
            {
                MessageBox.Show("No reports have been defined.\n\n\rReport not created.", "Report error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
                return;
            }

            // decide whether to use default report, or to present user with a list from which to choose
            if (_hideChoiceDialog)
            {
                reportToShow = anyBusinessObject.GetDefaultReportSpecification();   // no choice selected; return the default
            }
            else                    // user wants to make selection from list of available reports
            {
                if (reportSpecificationDict.Count > 1)
                {                   // there is a choice, so allow user to select from list of available reports
                    ReportSelectionForm theForm = new ReportSelectionForm(reportSpecificationDict);
                    theForm.ShowDialog();
                    reportToShow = theForm.SelectedReport;
                    theForm.Dispose();

                    if (reportToShow == null)
                    {
                        this.Close();
                        return;
                    }
                }
                else                // no choice b/c there is only one report, return that one
                {
                    List<string> keys = new List<string>(reportSpecificationDict.Keys);
                    reportToShow = reportSpecificationDict[keys[0]];
                }
            }

            // something went wrong, bail out
            if (reportToShow == null)
            {
               MessageBox.Show("No report was found.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
               this.Close();
               return;
            }

            _theReport = reportViewer.LocalReport;

            // populate the datasources for the report; there may be more than one
            foreach (string reportDataSourceName in reportToShow.DataSourceNames)
            {
                // create and set up the data source(s) for the report
                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource = new Microsoft.Reporting.WinForms.ReportDataSource();
                reportDataSource.Name = reportDataSourceName;
                reportDataSource.Value = bindingSource;
                _theReport.DataSources.Add(reportDataSource);
            }

            // set up the local report with a path to the report definitions
            _theReport.ReportPath = Utilities.GetPathToReports() + reportToShow.ReportFileName;

            // Apply options
            ReportParameterInfoCollection parameters;
            try
            {
                parameters = _theReport.GetParameters();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report at path " + _theReport.ReportPath + " could not be loaded.\n\r\n\r" + ex.Message, "Report processing error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            Dictionary<string, ReportParameterInfo> parmDict = parameters.ToDictionary<ReportParameterInfo, string>(x => x.Name);
            List<ReportParameter> parameterList = new List<ReportParameter>();

            if (parmDict.ContainsKey("XmlForParameters"))
            {
                _theReport.ExecuteReportInCurrentAppDomain(Assembly.GetExecutingAssembly().Evidence);
                _theReport.AddTrustedCodeModuleInCurrentAppDomain("HaiBusinessObject, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");

                HaiReports.ReportParameters reportParameters = _bindingList.ReportParameters;
                string reportParametersAsXml = reportParameters.SerializeMe();
                ReportParameter parameter = new ReportParameter("XmlForParameters", reportParametersAsXml);
                parameterList.Add(parameter);
            }

            if (parameterList.Count>0)
                _theReport.SetParameters(parameterList);

            // show the report
            this.reportViewer.RefreshReport();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (_theReport!=null)
                _theReport.Dispose();

            this.Close();
        }
    }
}
