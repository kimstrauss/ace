﻿namespace HaiBusinessUI
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbQA = new System.Windows.Forms.RadioButton();
            this.rbProduction = new System.Windows.Forms.RadioButton();
            this.rbDev = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbOpeiDev = new System.Windows.Forms.RadioButton();
            this.rbAhamOper = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(177, 242);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(258, 242);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbQA);
            this.groupBox1.Controls.Add(this.rbProduction);
            this.groupBox1.Controls.Add(this.rbDev);
            this.groupBox1.Location = new System.Drawing.Point(18, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(128, 156);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select server";
            // 
            // rbQA
            // 
            this.rbQA.AutoSize = true;
            this.rbQA.Location = new System.Drawing.Point(21, 50);
            this.rbQA.Name = "rbQA";
            this.rbQA.Size = new System.Drawing.Size(40, 17);
            this.rbQA.TabIndex = 7;
            this.rbQA.Text = "QA";
            this.rbQA.UseVisualStyleBackColor = true;
            // 
            // rbProduction
            // 
            this.rbProduction.AutoSize = true;
            this.rbProduction.Checked = true;
            this.rbProduction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbProduction.Location = new System.Drawing.Point(21, 27);
            this.rbProduction.Name = "rbProduction";
            this.rbProduction.Size = new System.Drawing.Size(86, 17);
            this.rbProduction.TabIndex = 5;
            this.rbProduction.TabStop = true;
            this.rbProduction.Text = "Production";
            this.rbProduction.UseVisualStyleBackColor = true;
            // 
            // rbDev
            // 
            this.rbDev.AutoSize = true;
            this.rbDev.Location = new System.Drawing.Point(21, 73);
            this.rbDev.Name = "rbDev";
            this.rbDev.Size = new System.Drawing.Size(88, 17);
            this.rbDev.TabIndex = 0;
            this.rbDev.Text = "Development";
            this.rbDev.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbOpeiDev);
            this.groupBox2.Controls.Add(this.rbAhamOper);
            this.groupBox2.Location = new System.Drawing.Point(181, 22);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(137, 156);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select database";
            // 
            // rbOpeiDev
            // 
            this.rbOpeiDev.AutoSize = true;
            this.rbOpeiDev.Location = new System.Drawing.Point(20, 50);
            this.rbOpeiDev.Name = "rbOpeiDev";
            this.rbOpeiDev.Size = new System.Drawing.Size(50, 17);
            this.rbOpeiDev.TabIndex = 2;
            this.rbOpeiDev.TabStop = true;
            this.rbOpeiDev.Text = "OPEI";
            this.rbOpeiDev.UseVisualStyleBackColor = true;
            // 
            // rbAhamOper
            // 
            this.rbAhamOper.AutoSize = true;
            this.rbAhamOper.Location = new System.Drawing.Point(20, 27);
            this.rbAhamOper.Name = "rbAhamOper";
            this.rbAhamOper.Size = new System.Drawing.Size(56, 17);
            this.rbAhamOper.TabIndex = 1;
            this.rbAhamOper.TabStop = true;
            this.rbAhamOper.Text = "AHAM";
            this.rbAhamOper.UseVisualStyleBackColor = true;
            // 
            // OptionsForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(345, 275);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Set database options";
            this.Load += new System.EventHandler(this.OptionsForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbDev;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbOpeiDev;
        private System.Windows.Forms.RadioButton rbAhamOper;
        private System.Windows.Forms.RadioButton rbProduction;
        private System.Windows.Forms.RadioButton rbQA;
    }
}