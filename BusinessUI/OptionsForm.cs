﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using HaiBusinessObject;

namespace HaiBusinessUI
{
    public partial class OptionsForm : Form
    {
        private GloballyUsefulStuff _gus;

        public OptionsForm(GloballyUsefulStuff gus)
        {
            InitializeComponent();

            _gus = gus;
        }

        private void OptionsForm_Load(object sender, EventArgs e)
        {
            string serverName = Utilities.GetServerDisplayName().ToLower();
            switch (serverName)
            {
                case "development":
                    rbDev.Checked = true;
                    break;

                case "production":
                    rbProduction.Checked = true;
                    break;

                case "qa":
                    rbQA.Checked = true;
                    break;

                throw new Exception("Unknown server name");
            }

            switch (Utilities.GetDatabaseDisplayName().ToLower())
            {
                case "aham":
                    rbAhamOper.Checked = true;
                    break;
                //case "ahamoper":
                //    rbAhamOper.Checked = true;
                //    break;
                case "opei":
                    rbOpeiDev.Checked = true;
                    break;
                //case "opeidev":
                //    rbOpeiDev.Checked = true;
                //    break;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (rbDev.Checked) Utilities.SetOverrideServerName("Development");
            if (rbProduction.Checked) Utilities.SetOverrideServerName("Production");
            if (rbQA.Checked) Utilities.SetOverrideServerName("QA");

            if (rbAhamOper.Checked)
            {
                Utilities.SetOverrideDatabaseName("AhamOper");
            }
            if (rbOpeiDev.Checked)
            {
                Utilities.SetOverrideDatabaseName("OpeiDev");
            }

            _gus.DatabaseContext = Utilities.GetDatabaseContext();

            DialogResult = DialogResult.OK;

            this.Close();
        }

  
    }
}
