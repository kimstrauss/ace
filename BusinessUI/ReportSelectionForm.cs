﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using HaiMetaDataDAL;
using HaiBusinessObject;


namespace HaiBusinessUI
{
    public partial class ReportSelectionForm : Form
    {
        Dictionary<string, ReportSpecification> _reportSpecificationDict;
        ReportSpecification _selectedReport = null;

        // Constructor
        public ReportSelectionForm(Dictionary<string, ReportSpecification> reportSpecificationDict)
        {
            InitializeComponent();
            _reportSpecificationDict = reportSpecificationDict;
        }

        #region Event handlers
        // Load event handler
        private void ReportSelectionForm_Load(object sender, EventArgs e)
        {
            ListViewItem item;

            lvReportList.FullRowSelect = true;
            lvReportList.View = View.Details;
            lvReportList.MultiSelect = false;
            lvReportList.HideSelection = false;

            lvReportList.Columns.Add("ID", 120);
            lvReportList.Columns.Add("Description", 360);

            foreach (string key in _reportSpecificationDict.Keys)
            {
                item = new ListViewItem(_reportSpecificationDict[key].Name);
                item.SubItems.Add(_reportSpecificationDict[key].Description);
                item.Tag = _reportSpecificationDict[key];

                lvReportList.Items.Add(item);
            }

            lvReportList.Items[0].Selected = true;
            lvReportList.Sorting = SortOrder.Ascending;
            lvReportList.Sort();
        }

        // OK
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (lvReportList.SelectedItems.Count == 0)
            {
                MessageBox.Show("Nothing selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            _selectedReport = (ReportSpecification)(lvReportList.SelectedItems[0].Tag);
            this.Close();
        }

        // Cancel
        private void btnCancel_Click(object sender, EventArgs e)
        {
            _selectedReport = null;
            this.Close();
        }

        // Double click
        private void lvReportList_DoubleClick(object sender, EventArgs e)
        {
            btnOK_Click(sender, e);
        }
        #endregion


        // property to allow caller to retrieve the report that was selected
        public ReportSpecification SelectedReport
        {
            get
            {
                return _selectedReport;
            }
        }
    }
}
