using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace HaiBusinessUI
{
    public partial class SetAdvancedFilterForm : Form
    {
        HaiBusinessObject.Filter _filter;
        DataGridView _datagrid;

        public SetAdvancedFilterForm(DataGridView datagrid, HaiBusinessObject.Filter filter)
        {
            InitializeComponent();
            _filter = filter;
            _datagrid = datagrid;
        }

        private void SetAdvancedFilterForm_Load(object sender, EventArgs e)
        {
            txtPropertyName.Text = _filter.PropertyName;

            if (_filter.DataTypeCode == TypeCode.DateTime)
            {
                string dateString = _filter.MatchValueList[0];
                string matchText = dateString;
                if ((dateString == HaiBusinessObject.Utilities.GetOpenBeginDate().ToString()) || (dateString == HaiBusinessObject.Utilities.GetOpenEndDate().ToString()))
                {
                    if (dateString == HaiBusinessObject.Utilities.GetOpenBeginDate().ToString())
                    {
                        matchText = "<<";
                    }
                    if (dateString == HaiBusinessObject.Utilities.GetOpenEndDate().ToString())
                    {
                        matchText = ">>";
                    }
                }
                else
                {
                    DateTime anyDate = DateTime.Parse(dateString);
                    matchText = anyDate.ToShortDateString();
                }

                txtMatchText.Text = matchText;
            }
            else
            {
                txtMatchText.Text = _filter.MatchValueList[0];
            }


            txtMatchText.SelectAll();

            if (_filter.DataTypeCode != TypeCode.String)
            {
                rbEqual.Checked = true;
                rbEquals.Enabled = false;
                rbBeginsWith.Enabled = false;
                rbEndsWith.Enabled = false;
                rbContains.Enabled = false;
                rbDoesNotContain.Enabled = false;
                rbDoesNotEqual.Enabled = false;
            }
            else
            {
                rbBeginsWith.Checked = true;
                rbEquals.Enabled = true;
                rbBeginsWith.Enabled = true;
                rbEndsWith.Enabled = true;
                rbContains.Enabled = true;
                rbDoesNotContain.Enabled = true;
                rbDoesNotEqual.Enabled = true;
            }

            txtMatchText.Enabled = true;

            this.AcceptButton = btnOK;
            this.CancelButton = btnCancel;

            treeUniqueText.Nodes.Add("All " + _filter.PropertyName);
            TreeNode rootNode = treeUniqueText.Nodes[0];
            List<string> uniqueNames = GetColumnUniqueItems();
            uniqueNames.Sort();

            string currentText = _datagrid.CurrentCell.Value.ToString();

            foreach (string name in uniqueNames)
            {
                rootNode.Nodes.Add(name);
            }

            treeUniqueText.Enabled = false;
            treeUniqueText.BackColor = this.BackColor;
            treeUniqueText.CollapseAll();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            List<string> matchItems = new List<string>();
            foreach (TreeNode node in treeUniqueText.Nodes[0].Nodes)
            {
                if (node.Checked)
                {
                    matchItems.Add(node.Text);
                }
            }

            // ensure that there is something to use as a filter
            if ((txtMatchText.Enabled && txtMatchText.Text.Trim()==string.Empty) 
                || (treeUniqueText.Enabled && (matchItems.Count == 0)))
            {
                MessageBox.Show("No items selected, nothing to filter", "Filter error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.DialogResult = DialogResult.Cancel;
                return;
            }

            _filter.MatchValueList.Clear();
            HaiBusinessObject.MatchType matchType;
            if (rbContains.Checked)
                matchType = HaiBusinessObject.MatchType.Contains;
            else if (rbEndsWith.Checked)
                matchType = HaiBusinessObject.MatchType.EndsWith;
            else if (rbEquals.Checked)
            {
                matchType = HaiBusinessObject.MatchType.EqualTo;
                foreach (TreeNode node in treeUniqueText.Nodes[0].Nodes)
                {
                    if (node.Checked)
                    {
                        _filter.MatchValueList.Add(node.Text);
                    }
                }
            }
            else if (rbBeginsWith.Checked)
                matchType = HaiBusinessObject.MatchType.BeginsWith;
            else if (rbDoesNotEqual.Checked)
                matchType = HaiBusinessObject.MatchType.NotEqual;
            else if (rbDoesNotContain.Checked)
                matchType = HaiBusinessObject.MatchType.NotContains;
            else if (rbLessThan.Checked)
                matchType = HaiBusinessObject.MatchType.LT;
            else if (rbLessThanOrEqual.Checked)
                matchType = HaiBusinessObject.MatchType.LE;
            else if (rbEqual.Checked)
                matchType = HaiBusinessObject.MatchType.EQ;
            else if (rbGreaterThanOrEqual.Checked)
                matchType = HaiBusinessObject.MatchType.GE;
            else if (rbGreaterThan.Checked)
                matchType = HaiBusinessObject.MatchType.GT;
            else if (rbNotEqual.Checked)
                matchType = HaiBusinessObject.MatchType.NE;
            else
                matchType = HaiBusinessObject.MatchType.Ignore;

            // "EqualTo" sets MatchValueList, otherwise set MatchValue
            string matchText = txtMatchText.Text;

            if (_filter.DataTypeCode == TypeCode.DateTime)
            {
                matchText = matchText.Trim().ToLower();
                if (matchText == "<<" || matchText == "open begin date" || matchText == "(open begin date)")
                {
                    matchText = HaiBusinessObject.Utilities.GetOpenBeginDate().ToString();
                }

                if (matchText == ">>" || matchText == "open end date" || matchText == "(open end date)")
                {
                    matchText = HaiBusinessObject.Utilities.GetOpenEndDate().ToString();
                }
            }

            if (matchType != HaiBusinessObject.MatchType.EqualTo)
            {
                _filter.MatchValueList.Add(matchText);
            }
            _filter.MatchType = matchType;
        }

        private List<string> GetColumnUniqueItems()
        {
            string cellText = string.Empty;
            int rowNumber = 0;
            int columnIndex = _datagrid.CurrentCell.ColumnIndex;
            Dictionary<string, int> uniqueRows = new Dictionary<string, int>(_datagrid.Rows.Count);

            // build a dictionary keyed by text with row index as value
            foreach (DataGridViewRow row in _datagrid.Rows)
            {
                cellText = row.Cells[columnIndex].Value.ToString();
                if (!uniqueRows.ContainsKey(cellText))
                {
                    uniqueRows.Add(cellText, rowNumber);
                }
                rowNumber++;
            }

            // open the lookup form with keys to be used as autocomplete values
            List<string> uniqueTextList = new List<string>(uniqueRows.Keys);

            return uniqueTextList;
        }

        private void treeUniqueText_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            bool IsChecked = false;
            if (e.Node.Parent == null)      // this is the root
            {
                TreeNode rootNode = e.Node;
                if (rootNode.Checked)
                {
                    IsChecked = true;
                }
                foreach (TreeNode anyNode in rootNode.Nodes)
                {
                    anyNode.Checked = IsChecked;
                }
            }
        }

        private void rbEquals_CheckedChanged(object sender, EventArgs e)
        {
            if (rbEquals.Checked)
            {
                txtMatchText.Enabled = false;
                treeUniqueText.Enabled = true;
                treeUniqueText.BackColor = Color.White;
                treeUniqueText.ExpandAll();
            }
            else
            {
                txtMatchText.Enabled = true;
                treeUniqueText.Enabled = false;
                treeUniqueText.BackColor = this.BackColor;
                treeUniqueText.CollapseAll();
            }
        }
    }
}