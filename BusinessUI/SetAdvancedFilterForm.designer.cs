namespace HaiBusinessUI
{
    partial class SetAdvancedFilterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtPropertyName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbDoesNotContain = new System.Windows.Forms.RadioButton();
            this.rbDoesNotEqual = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rbNotEqual = new System.Windows.Forms.RadioButton();
            this.rbGreaterThan = new System.Windows.Forms.RadioButton();
            this.rbGreaterThanOrEqual = new System.Windows.Forms.RadioButton();
            this.rbEqual = new System.Windows.Forms.RadioButton();
            this.rbLessThanOrEqual = new System.Windows.Forms.RadioButton();
            this.rbLessThan = new System.Windows.Forms.RadioButton();
            this.rbContains = new System.Windows.Forms.RadioButton();
            this.rbEndsWith = new System.Windows.Forms.RadioButton();
            this.rbBeginsWith = new System.Windows.Forms.RadioButton();
            this.rbEquals = new System.Windows.Forms.RadioButton();
            this.txtMatchText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.treeUniqueText = new System.Windows.Forms.TreeView();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(257, 373);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(68, 29);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(183, 373);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(68, 29);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtPropertyName
            // 
            this.txtPropertyName.Location = new System.Drawing.Point(8, 9);
            this.txtPropertyName.Name = "txtPropertyName";
            this.txtPropertyName.ReadOnly = true;
            this.txtPropertyName.Size = new System.Drawing.Size(318, 20);
            this.txtPropertyName.TabIndex = 2;
            this.txtPropertyName.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbDoesNotContain);
            this.groupBox1.Controls.Add(this.rbDoesNotEqual);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rbNotEqual);
            this.groupBox1.Controls.Add(this.rbGreaterThan);
            this.groupBox1.Controls.Add(this.rbGreaterThanOrEqual);
            this.groupBox1.Controls.Add(this.rbEqual);
            this.groupBox1.Controls.Add(this.rbLessThanOrEqual);
            this.groupBox1.Controls.Add(this.rbLessThan);
            this.groupBox1.Controls.Add(this.rbContains);
            this.groupBox1.Controls.Add(this.rbEndsWith);
            this.groupBox1.Controls.Add(this.rbBeginsWith);
            this.groupBox1.Controls.Add(this.rbEquals);
            this.groupBox1.Location = new System.Drawing.Point(8, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 133);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Match type";
            // 
            // rbDoesNotContain
            // 
            this.rbDoesNotContain.AutoSize = true;
            this.rbDoesNotContain.Location = new System.Drawing.Point(163, 57);
            this.rbDoesNotContain.Name = "rbDoesNotContain";
            this.rbDoesNotContain.Size = new System.Drawing.Size(106, 17);
            this.rbDoesNotContain.TabIndex = 5;
            this.rbDoesNotContain.TabStop = true;
            this.rbDoesNotContain.Text = "Does not contain";
            this.rbDoesNotContain.UseVisualStyleBackColor = true;
            // 
            // rbDoesNotEqual
            // 
            this.rbDoesNotEqual.AutoSize = true;
            this.rbDoesNotEqual.Location = new System.Drawing.Point(15, 57);
            this.rbDoesNotEqual.Name = "rbDoesNotEqual";
            this.rbDoesNotEqual.Size = new System.Drawing.Size(97, 17);
            this.rbDoesNotEqual.TabIndex = 4;
            this.rbDoesNotEqual.TabStop = true;
            this.rbDoesNotEqual.Text = "Does not equal";
            this.rbDoesNotEqual.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Match value";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Match text";
            // 
            // rbNotEqual
            // 
            this.rbNotEqual.AutoSize = true;
            this.rbNotEqual.Location = new System.Drawing.Point(272, 105);
            this.rbNotEqual.Name = "rbNotEqual";
            this.rbNotEqual.Size = new System.Drawing.Size(40, 17);
            this.rbNotEqual.TabIndex = 11;
            this.rbNotEqual.TabStop = true;
            this.rbNotEqual.Text = "NE";
            this.rbNotEqual.UseVisualStyleBackColor = true;
            // 
            // rbGreaterThan
            // 
            this.rbGreaterThan.AutoSize = true;
            this.rbGreaterThan.Location = new System.Drawing.Point(226, 105);
            this.rbGreaterThan.Name = "rbGreaterThan";
            this.rbGreaterThan.Size = new System.Drawing.Size(40, 17);
            this.rbGreaterThan.TabIndex = 10;
            this.rbGreaterThan.TabStop = true;
            this.rbGreaterThan.Text = "GT";
            this.rbGreaterThan.UseVisualStyleBackColor = true;
            // 
            // rbGreaterThanOrEqual
            // 
            this.rbGreaterThanOrEqual.AutoSize = true;
            this.rbGreaterThanOrEqual.Location = new System.Drawing.Point(169, 105);
            this.rbGreaterThanOrEqual.Name = "rbGreaterThanOrEqual";
            this.rbGreaterThanOrEqual.Size = new System.Drawing.Size(40, 17);
            this.rbGreaterThanOrEqual.TabIndex = 9;
            this.rbGreaterThanOrEqual.TabStop = true;
            this.rbGreaterThanOrEqual.Text = "GE";
            this.rbGreaterThanOrEqual.UseVisualStyleBackColor = true;
            // 
            // rbEqual
            // 
            this.rbEqual.AutoSize = true;
            this.rbEqual.Location = new System.Drawing.Point(121, 105);
            this.rbEqual.Name = "rbEqual";
            this.rbEqual.Size = new System.Drawing.Size(40, 17);
            this.rbEqual.TabIndex = 8;
            this.rbEqual.TabStop = true;
            this.rbEqual.Text = "EQ";
            this.rbEqual.UseVisualStyleBackColor = true;
            // 
            // rbLessThanOrEqual
            // 
            this.rbLessThanOrEqual.AutoSize = true;
            this.rbLessThanOrEqual.Location = new System.Drawing.Point(62, 105);
            this.rbLessThanOrEqual.Name = "rbLessThanOrEqual";
            this.rbLessThanOrEqual.Size = new System.Drawing.Size(38, 17);
            this.rbLessThanOrEqual.TabIndex = 7;
            this.rbLessThanOrEqual.TabStop = true;
            this.rbLessThanOrEqual.Text = "LE";
            this.rbLessThanOrEqual.UseVisualStyleBackColor = false;
            // 
            // rbLessThan
            // 
            this.rbLessThan.AutoSize = true;
            this.rbLessThan.Location = new System.Drawing.Point(14, 105);
            this.rbLessThan.Name = "rbLessThan";
            this.rbLessThan.Size = new System.Drawing.Size(38, 17);
            this.rbLessThan.TabIndex = 6;
            this.rbLessThan.TabStop = true;
            this.rbLessThan.Text = "LT";
            this.rbLessThan.UseVisualStyleBackColor = true;
            // 
            // rbContains
            // 
            this.rbContains.AutoSize = true;
            this.rbContains.Location = new System.Drawing.Point(240, 34);
            this.rbContains.Name = "rbContains";
            this.rbContains.Size = new System.Drawing.Size(66, 17);
            this.rbContains.TabIndex = 3;
            this.rbContains.TabStop = true;
            this.rbContains.Text = "Contains";
            this.rbContains.UseVisualStyleBackColor = true;
            // 
            // rbEndsWith
            // 
            this.rbEndsWith.AutoSize = true;
            this.rbEndsWith.Location = new System.Drawing.Point(163, 34);
            this.rbEndsWith.Name = "rbEndsWith";
            this.rbEndsWith.Size = new System.Drawing.Size(71, 17);
            this.rbEndsWith.TabIndex = 2;
            this.rbEndsWith.TabStop = true;
            this.rbEndsWith.Text = "Ends with";
            this.rbEndsWith.UseVisualStyleBackColor = true;
            // 
            // rbBeginsWith
            // 
            this.rbBeginsWith.AutoSize = true;
            this.rbBeginsWith.Location = new System.Drawing.Point(78, 34);
            this.rbBeginsWith.Name = "rbBeginsWith";
            this.rbBeginsWith.Size = new System.Drawing.Size(79, 17);
            this.rbBeginsWith.TabIndex = 1;
            this.rbBeginsWith.TabStop = true;
            this.rbBeginsWith.Text = "Begins with";
            this.rbBeginsWith.UseVisualStyleBackColor = true;
            // 
            // rbEquals
            // 
            this.rbEquals.AutoSize = true;
            this.rbEquals.Location = new System.Drawing.Point(15, 34);
            this.rbEquals.Name = "rbEquals";
            this.rbEquals.Size = new System.Drawing.Size(57, 17);
            this.rbEquals.TabIndex = 0;
            this.rbEquals.TabStop = true;
            this.rbEquals.Text = "Equals";
            this.rbEquals.UseVisualStyleBackColor = true;
            this.rbEquals.CheckedChanged += new System.EventHandler(this.rbEquals_CheckedChanged);
            // 
            // txtMatchText
            // 
            this.txtMatchText.Location = new System.Drawing.Point(8, 200);
            this.txtMatchText.Name = "txtMatchText";
            this.txtMatchText.Size = new System.Drawing.Size(318, 20);
            this.txtMatchText.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Text/Value";
            // 
            // treeUniqueText
            // 
            this.treeUniqueText.CheckBoxes = true;
            this.treeUniqueText.Location = new System.Drawing.Point(8, 226);
            this.treeUniqueText.Name = "treeUniqueText";
            this.treeUniqueText.Size = new System.Drawing.Size(317, 141);
            this.treeUniqueText.TabIndex = 15;
            this.treeUniqueText.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeUniqueText_NodeMouseClick);
            // 
            // SetAdvancedFilterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 414);
            this.Controls.Add(this.treeUniqueText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMatchText);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtPropertyName);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SetAdvancedFilterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Set advanced filter";
            this.Load += new System.EventHandler(this.SetAdvancedFilterForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox txtPropertyName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbContains;
        private System.Windows.Forms.RadioButton rbEndsWith;
        private System.Windows.Forms.RadioButton rbBeginsWith;
        private System.Windows.Forms.RadioButton rbEquals;
        private System.Windows.Forms.TextBox txtMatchText;
        private System.Windows.Forms.RadioButton rbNotEqual;
        private System.Windows.Forms.RadioButton rbGreaterThan;
        private System.Windows.Forms.RadioButton rbGreaterThanOrEqual;
        private System.Windows.Forms.RadioButton rbEqual;
        private System.Windows.Forms.RadioButton rbLessThanOrEqual;
        private System.Windows.Forms.RadioButton rbLessThan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbDoesNotEqual;
        private System.Windows.Forms.RadioButton rbDoesNotContain;
        private System.Windows.Forms.TreeView treeUniqueText;
    }
}