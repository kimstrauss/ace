using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using HaiBusinessObject;

namespace HaiBusinessUI
{
    public partial class ShowCurrentFiltersForm : Form
    {
        private Dictionary<string, List<Filter>> _gridFilters;

        public ShowCurrentFiltersForm(Dictionary<string, List<Filter>> gridFilters)
        {
            InitializeComponent();
            _gridFilters = gridFilters;
        }

        private void ShowCurrentFiltersForm_Load(object sender, EventArgs e)
        {
            List<Filter> filters;
            lvFilters.View = View.Details;
            lvFilters.FullRowSelect = true;
            lvFilters.GridLines = true;

            ColumnHeader column;
            column = lvFilters.Columns.Add("Property");
            column.Width=150;
            column = lvFilters.Columns.Add("Match type");
            column.Width = 75;
            column = lvFilters.Columns.Add("Match value");
            column.Width = 300;

            this.Text=@"Currently applied filters";

            int count = 0;
            foreach (string propertyName in _gridFilters.Keys)
            {
                Color backColor;

                if (count % 2 == 0)
                    backColor = Color.White;
                else
                    backColor = Color.LightGray;

                filters = _gridFilters[propertyName];
                foreach (Filter filter in filters)
                {
                    ListViewItem item;
                    item = lvFilters.Items.Add(filter.PropertyName);
                    item.SubItems.Add(filter.MatchType.ToString());
                    string text = string.Empty;

                    foreach (string matchString in filter.MatchValueList)
                    {
                        text = text + matchString + " OR ";
                    }

                    text = text.Remove(text.Length - 4);
                    item.SubItems.Add(text);
                    item.BackColor = backColor;
                }
                count++;
            }

            this.CancelButton = btnOK;
        }
    }
}