﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HaiBusinessObject;
using HaiInterfaces;

namespace HaiBusinessUI
{

    public static class UIUtilities
    {
        public static List<HaiBusinessObjectBase> ExtractKindredItems(string[] kindredPropertyNames, HaiBusinessObjectBase self, List<HaiBusinessObjectBase> comparisonUniverse)
        {
            List<HaiBusinessObjectBase> kindredItems = new List<HaiBusinessObjectBase>(); // return parameter; list of kindred items (that excludes the item itself)
            List<HaiBusinessObjectBase> itemsToKeep;                           // list of items to keep that is successively reduced by matching keys

            int propertyCount = kindredPropertyNames.Length;

            if (propertyCount == 0)     // there are no properties for identifying kindreds, so return empty list
                return kindredItems;

            // create array of property values that will used to select the kindred items
            object[] kindredPropertyValues;
            kindredPropertyValues = new object[propertyCount];
            for (int i = 0; i < propertyCount; i++)
            {
                kindredPropertyValues[i] = Utilities.CallByName(self, kindredPropertyNames[i], CallType.Get, null);
            }

            // initialize list of kindred items to all items in the universe
            foreach (HaiBusinessObjectBase anyItem in comparisonUniverse)
            {
                kindredItems.Add(anyItem);
            }

            // iterate across the properties that are used to identify kindred items
            for (int i = 0; i < propertyCount; i++)
            {
                itemsToKeep = new List<HaiBusinessObjectBase>();   // initialize a list of items that will be kept

                // keep only the items that have a matching value for this property
                foreach (HaiBusinessObjectBase item in kindredItems)
                {
                    string propertyName = kindredPropertyNames[i];
                    object propertyValue = kindredPropertyValues[i];
                    object value = Utilities.CallByName(item, propertyName, CallType.Get, null);

                    if (propertyValue.GetType() == typeof(string))  // string properties must be upcased for comparison
                    {
                        if ((((string)value).ToUpper()).Equals(((string)propertyValue).ToUpper()))
                            itemsToKeep.Add(item);
                    }
                    else
                    {
                        if (value.Equals(propertyValue))
                            itemsToKeep.Add(item);
                    }
                }

                // reset the list of kindred items to the reduced list of items to keep
                kindredItems = new List<HaiBusinessObjectBase>();
                foreach (HaiBusinessObjectBase item in itemsToKeep)
                {
                    kindredItems.Add(item);
                }
            }

            kindredItems.Remove(self);  // remove the item so that the list is only kindreds

            return kindredItems;
        }

        public static string CheckDateRangeForReverseChronologyAndOverlaps(IDateRange itemToCheck, List<HaiBusinessObjectBase> comparisonUniverse, DateRangeValidationType validationType)
        {
            // This procedure checks the new dateRange that will be created for each object in ItemsSelectedForEdit
            //   1. validates the dateRange (check for reverse chronology)
            //   2. checks that the new dateRange does not overlap with the dateRange of any kindred item

            string message = string.Empty;

            // 1. Validate the date range that has been set
            string testMessage = itemToCheck.DateRange.ValidateRangeForType(validationType);
            if (testMessage != string.Empty)
            {
                message += "\n\r" + testMessage + " for " + itemToCheck.UniqueIdentifier;
                return message;           // dateRange is not valid; do not test for overlaps.
            }

            // 2. The date range is valid for this object, so now test to see 
            // ... if the (valid) date range will create an overlap with any of its kindred items.
            List<HaiBusinessObjectBase> kindredItems = ExtractKindredItems(itemToCheck.KindredPropertyNames, itemToCheck as HaiBusinessObjectBase, comparisonUniverse);

            // Examine each kindred item to determine whether the test date range will create a date range overlap
            foreach (IDateRange anyKindredItemWithDateRange in kindredItems)
            {
                if (anyKindredItemWithDateRange.DateRange.OverlapsRange(itemToCheck.DateRange))
                {
                    message += "\r\nKey[" + ((HaiBusinessObjectBase)anyKindredItemWithDateRange).PrimaryKey.ToString() + "] " + anyKindredItemWithDateRange.UniqueIdentifier + " Creates overlap with " + anyKindredItemWithDateRange.DateRange.ToString();
                }
            }

            return message;
        }
    }
}
