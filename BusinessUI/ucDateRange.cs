﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;

using HaiBusinessObject;

namespace HaiBusinessUI
{
    public partial class ucDateRange : UserControl
    {
        private DateRange mDateRange;
        private bool _defaultBeginDateIsOpen = false;
        private bool _suppressDateRangeModified = false;

        public event EventHandler DateRangeModified;                    // event to signal that date(s) have changed

        // constructor
        public ucDateRange()
        {
            InitializeComponent();

            BeginDateTimePicker.ValueChanged += new EventHandler(BeginDateTimePicker_ValueChanged);
            EndDateTimePicker.ValueChanged += new EventHandler(EndDateTimePicker_ValueChanged);
        }

        public bool IsDirty()
        {
            return IsDirtyTest();
        }

        private bool IsDirtyTest()
        {
            DateRange LatentDateRange;
            bool IsDirty;
            string Message;

            LatentDateRange = new DateRange();
            Message = "";
            IsDirty = false;

            if (!UpdateDateRange(LatentDateRange, ref Message))
            {
                MessageBox.Show("Errors found in Date Range." + "\r\n" + Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return IsDirty;
            }

            {
                if (LatentDateRange.BeginDate.Date != mDateRange.BeginDate.Date)
                    IsDirty = true;
                if (LatentDateRange.EndDate.Date != mDateRange.EndDate.Date)
                    IsDirty = true;
            }

            return IsDirty;

        }

        public void InitializeWithOpenDates()
        {
            _suppressDateRangeModified = true;
            _defaultBeginDateIsOpen = true;
            mDateRange = new DateRange();

            rbDefaultBeginDate.Text = "open begin date";
            BeginDateTimePicker.CustomFormat = "MMM dd, yyyy";
            BeginDateTimePicker.MinDate = DateTime.Parse(ConfigurationManager.AppSettings["EarliestBeginDate"]);
            EndDateTimePicker.CustomFormat = "MMM dd, yyyy";
            EndDateTimePicker.MaxDate = DateTime.Parse(ConfigurationManager.AppSettings["LatestEndDate"]);

            BeginDateTimePicker.Value = System.DateTime.Today;
            BeginDateTimePicker.Enabled = false;
            rbDefaultBeginDate.Checked = true;
            rbFixedBeginDate.Checked = false;

            EndDateTimePicker.Value = DateTime.Today.AddDays(1);
            EndDateTimePicker.Enabled = false;
            rbOpenEndDate.Checked = true;
            rbFixedEndDate.Checked = false;

            _suppressDateRangeModified = false;
            if (DateRangeModified != null)
                DateRangeModified(this, new DateRangeModifiedEventArgs());
        }

        public void InitializeWithDates(DateTime BeginDate, DateTime EndDate)
        {
            DateRange dummyDateRage = new DateRange(BeginDate, EndDate);
            InitializeWithDateRange(dummyDateRage);
        }

        public void InitializeWithDateRange(DateRange DateRange)
        {
            _suppressDateRangeModified = true;
            _defaultBeginDateIsOpen = true;

            mDateRange = DateRange;
            BeginDateTimePicker.CustomFormat = "MMM dd, yyyy";
            BeginDateTimePicker.MinDate = DateTime.Parse(ConfigurationManager.AppSettings["EarliestBeginDate"]);
            EndDateTimePicker.CustomFormat = "MMM dd, yyyy";
            EndDateTimePicker.MaxDate = DateTime.Parse(ConfigurationManager.AppSettings["LatestEndDate"]);

            // set initial values for BeginDate
            if (mDateRange.BeginDate.Date == DateTime.Today.Date)   // BeginDate is Today
            {
                BeginDateTimePicker.Value = DateTime.Today.AddDays(1);
                BeginDateTimePicker.Enabled = false;
                rbDefaultBeginDate.Checked = true;
                rbFixedBeginDate.Checked = false;
            }
            else                                                    // BeginDate is not Today
            {
                if (mDateRange.BeginDate.Date == Utilities.GetOpenBeginDate())
                {
                    BeginDateTimePicker.Value = DateTime.Today;
                    BeginDateTimePicker.Enabled = false;
                    rbDefaultBeginDate.Checked = true;
                    rbDefaultBeginDate.Text = "open begin date";
                    rbFixedBeginDate.Checked = false;
                }
                else
                {
                    BeginDateTimePicker.Value = mDateRange.BeginDate.Date;
                    BeginDateTimePicker.Enabled = true;
                    rbDefaultBeginDate.Checked = false;
                    rbDefaultBeginDate.Text = "open begin date";
                    rbFixedBeginDate.Checked = true;
                }
                BeginDateTimePicker.Enabled = true;
            }

            // set initial values for EndDate
            if (mDateRange.EndDate.Date == Utilities.GetOpenEndDate())
            {
                EndDateTimePicker.Value = DateTime.Today.AddDays(1);
                EndDateTimePicker.Enabled = false;
                rbOpenEndDate.Checked = true;
                rbFixedEndDate.Checked = false;
            }
            else
            {
                EndDateTimePicker.Value = mDateRange.EndDate.Date;
                EndDateTimePicker.Enabled = true;
                rbOpenEndDate.Checked = false;
                rbFixedEndDate.Checked = true;
            }

            _suppressDateRangeModified = false;
            if (DateRangeModified!=null)
                DateRangeModified(this, new DateRangeModifiedEventArgs());
        }

        public bool UpdateDateRange(DateRange AnyDateRange)
        {
            string message = string.Empty;
            return this.UpdateDateRange(AnyDateRange, ref message);
        }

        public bool UpdateDateRange(DateRange AnyDateRange, ref string ErrorMessage)
        {
            DateTime BeginDate;
            DateTime EndDate;
            bool NoErrors;

            NoErrors = false;

            if (rbDefaultBeginDate.Checked)
            {
                if (_defaultBeginDateIsOpen)
                    BeginDate = Utilities.GetOpenBeginDate();
                else
                    BeginDate = DateTime.Today;
            }
            else
            {
                EndDateTimePicker.Focus();          // move the focus to force picker to update value
                BeginDate = BeginDateTimePicker.Value;
            }

            if (rbOpenEndDate.Checked)
            {
                EndDate = Utilities.GetOpenEndDate();
            }
            else
            {
                BeginDateTimePicker.Focus();        // move the focus to force picker to update value
                EndDate = EndDateTimePicker.Value;
            }

            if (EndDate < BeginDate)
            {
                ErrorMessage = "End date cannot be before begin date.";
                return NoErrors;
            }

            AnyDateRange.BeginDate = BeginDate;
            AnyDateRange.EndDate = EndDate;

            NoErrors = true;

            return NoErrors;

        }

        public string ValidateDateRange()
        {
            DateTime BeginDate;
            DateTime EndDate;

            if (rbDefaultBeginDate.Checked)
            {
                if (_defaultBeginDateIsOpen)
                    BeginDate = Utilities.GetOpenBeginDate();
                else
                    BeginDate = DateTime.Today;
            }
            else
                BeginDate = BeginDateTimePicker.Value;

            if (rbOpenEndDate.Checked)
                EndDate = Utilities.GetOpenEndDate();
            else
                EndDate = EndDateTimePicker.Value;

            if (EndDate < BeginDate)
                return "End date cannot be before begin date.";
            else
                return "";
        }

        private void rbOpenEndDate_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbOpenEndDate.Checked)
            {
                EndDateTimePicker.Enabled = false;
                EndDateTimePicker.Visible = false;
                rbFixedEndDate.Text = "set end date";
            }
            else
            {
                EndDateTimePicker.Enabled = true;
                EndDateTimePicker.Visible = true;
                rbFixedEndDate.Text = "";
            }

            if (!_suppressDateRangeModified)
                if (DateRangeModified != null)
                    DateRangeModified(this, new DateRangeModifiedEventArgs());
        }

        private void rbDefaultBeginDate_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbDefaultBeginDate.Checked)
            {
                BeginDateTimePicker.Enabled = false;
                BeginDateTimePicker.Visible = false;
                rbFixedBeginDate.Text = "set begin date";
            }
            else
            {
                BeginDateTimePicker.Enabled = true;
                BeginDateTimePicker.Visible = true;
                rbFixedBeginDate.Text = "";
            }

            if (!_suppressDateRangeModified)
                if (DateRangeModified != null)
                    DateRangeModified(this, new DateRangeModifiedEventArgs());
        }

        // fire an event if the end date has been changed
        void EndDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            if (!_suppressDateRangeModified)
                if (DateRangeModified != null)
                    DateRangeModified(this, new DateRangeModifiedEventArgs());
        }

        // fire an event if the begin date has been changed
        void BeginDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            if (!_suppressDateRangeModified)
                if (DateRangeModified != null)
                    DateRangeModified(this, new DateRangeModifiedEventArgs());
        }
    }

    public class DateRangeModifiedEventArgs : EventArgs
    {

    }
}