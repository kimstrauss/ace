using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;

using HaiBusinessObject;

namespace HaiBusinessUI
{
    public partial class ucReadonlyDatagridForm : UserControl
    {
        private string StringForNull = Utilities.GetStringForNull();

        #region Class variables

        DataAccessResult _dataFetchResult;
        internal Dictionary<string, BrowsableProperty> _browsablePropertyList = null;
        private IUntypedBindingList _untypedBindingList = null;
        private List<string> _visibleColumnOrder;
        private GloballyUsefulStuff _gus;

        internal BindingSource _bs;
        private bool _ctrlKeyIsDown = false;
        private Dictionary<string, List<Filter>> _gridFilters = new Dictionary<string,List<Filter>>();
        private Dictionary<string, ListSortDescription> _sortDescriptionDictionary = new Dictionary<string,ListSortDescription>();

        private HaiBusinessObjectType _specialObjectType;
        private DateTime _openBeginDate;
        private DateTime _openEndDate;

        private bool CancelContextMenu;     // keep track of whether ContextMenuStrip should be shown

        #endregion

        #region Constructors

        public ucReadonlyDatagridForm()
        {
            InitializeComponent();

            this.BorderStyle = BorderStyle.FixedSingle; // called by the designer to set the border style
        }

        public ucReadonlyDatagridForm(DataAccessResult dataFetchResult, GloballyUsefulStuff gus)
        {
            InitializeComponent();

            _dataFetchResult = dataFetchResult;
            _gus = gus;

            PrepareGridForDisplay();
        }

        public ucReadonlyDatagridForm(DataAccessResult dataFetchResult, List<string> visibleColumnOrder, GloballyUsefulStuff gus)
        {
            InitializeComponent();

            _visibleColumnOrder = visibleColumnOrder;
            _dataFetchResult = dataFetchResult;
            _gus = gus;

            PrepareGridForDisplay();
        }

        #endregion

        #region Form event handlers

        private void MainForm_Load(object sender, EventArgs e)
        {
            datagrid.Focus();       // using keys requires that the grid has the focus.

            // assign event handler for context menu so cell that was right-clicked is the current cell
            datagrid.CellContextMenuStripNeeded += new DataGridViewCellContextMenuStripNeededEventHandler(datagrid_CellContextMenuStripNeeded);

            // assign event handlers for keyboard entry
            datagrid.KeyPress += new KeyPressEventHandler(datagrid_KeyPress);
            datagrid.KeyDown += new KeyEventHandler(datagrid_KeyDown);
            datagrid.KeyUp += new KeyEventHandler(datagrid_KeyUp);
        }

        #endregion

        #region Column sorting management and execution

        private void DoColumnSort(IBindingListView datalistToSort, Dictionary<string, ListSortDescription> sortDict)
        {
            ListSortDescription sortDescription;

            // create a "List" from the dictionary, list is used in constructor of ListSortDescriptionCollection
            List<ListSortDescription> listSortDescriptions = new List<ListSortDescription>();
            foreach (string key in sortDict.Keys)
            {
                sortDescription = sortDict[key];
                listSortDescriptions.Add(sortDescription);
            }

            // do the sort
            ListSortDescriptionCollection sortDescriptionCollection = new ListSortDescriptionCollection(listSortDescriptions.ToArray());
            datalistToSort.ApplySort(sortDescriptionCollection);
        }

        #endregion 

        #region Column setup

        internal virtual void SetColumnHeadings(string headingStyle)
        {
            // set the grid column names to property description
            foreach (DataGridViewColumn column in datagrid.Columns)
            {
                string name = column.Name;
                HaiBusinessObject.BrowsableProperty bp = _browsablePropertyList[name];

                switch (headingStyle)
                {
                    case "FriendlyName":
                        column.HeaderText = bp.FriendlyName;
                        break;
                    case "PropertyName" :
                        column.HeaderText = bp.PropertyName;
                        break;
                    case "Description" :
                        column.HeaderText = bp.PropertyDescription;
                        break;
                    case "DatabaseName" :
                        column.HeaderText = bp.DbColumnName;
                        break;
                    default:
                        break;
                }
            }
        }

        internal virtual void SetColumnWidths(string widthMode)
        {
            switch (widthMode)
            {
                case "Auto" :
                    datagrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                    break;
                case "Default" :
                    datagrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.ColumnHeader);
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Mouse event handlers

        // handle column header click for sorting
        private void datagrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // do sort only if Left mouse button is pressed
            if ((e !=null) && (e.Button != MouseButtons.Left))
            {
                return;
            }

            DataGridViewColumnHeaderCell header;
            if (datagrid.Rows.Count == 0)       // nothing to sort
                return;

            int columnIndex = datagrid.CurrentCell.ColumnIndex;

       
            if (e == null)      // (e == null) if not called by event; use current cell to get column index
            {
                header = datagrid.Columns[datagrid.CurrentCell.ColumnIndex].HeaderCell;
            }
            else                // otherwise use column index from click event
            {
                header = datagrid.Columns[e.ColumnIndex].HeaderCell;
            }
            string columnName = datagrid.Columns[header.ColumnIndex].Name;

            bool clickedColumnIsSorted;
            if (_sortDescriptionDictionary.ContainsKey(columnName))
            {
                clickedColumnIsSorted = true;
            }
            else
                clickedColumnIsSorted = false;

            ListSortDescription sortDescription;
            ListSortDirection sortDirection;
            if (_ctrlKeyIsDown)
            {
                if (clickedColumnIsSorted)
                {
                    sortDescription = _sortDescriptionDictionary[columnName];
                    if (sortDescription.SortDirection == ListSortDirection.Ascending)
                        sortDescription.SortDirection = ListSortDirection.Descending;
                    else
                        sortDescription.SortDirection = ListSortDirection.Ascending;
                }
                else
                {
                    AddSortDescription(columnName, ListSortDirection.Ascending);
                }
            }
            else
            {
                if (clickedColumnIsSorted)
                {
                    sortDescription = _sortDescriptionDictionary[columnName];
                    if (sortDescription.SortDirection == ListSortDirection.Ascending)
                        sortDirection = ListSortDirection.Descending;
                    else
                        sortDirection = ListSortDirection.Ascending;
                }
                else
                {
                    sortDirection = ListSortDirection.Ascending;
                }
                _sortDescriptionDictionary.Clear();
                AddSortDescription(columnName, sortDirection);
            }

            DoColumnSort(_untypedBindingList, _sortDescriptionDictionary);

            datagrid.CurrentCell = datagrid[columnIndex, 0];
        }

        #endregion

        #region Menu event handlers

        // enable the content of the menu and set its location
        private void mnuGridMenu_Opening(object sender, CancelEventArgs e)
        {
            DataGridViewCell currentCell = datagrid.CurrentCell;

            if (CancelContextMenu)
            {
                e.Cancel = true;                // cancel display of the context menu
                CancelContextMenu = false;      // reset the showability of the context menu
                return;
            }

            // enable/disable certain menu items according to conditions
            if (currentCell == null)
            {
                applyEqualFilterToolStripMenuItem.Enabled = false;
                setAdvancedFilterToolStripMenuItem.Enabled = false;
                removeThisFilterToolStripMenuItem.Enabled = false;
                findInThisColumnToolStripMenuItem.Enabled = false;
                sortThisColumnToolStripMenuItem.Enabled = false;
                unsortToolStripMenuItem.Enabled = false;
                dropAllFiltersToolStripMenuItem.Enabled = true;
                viewCurrentFilterToolStripMenuItem.Enabled = true;
            }
            else
            {
                unsortToolStripMenuItem.Enabled = _untypedBindingList.IsSorted;

                applyEqualFilterToolStripMenuItem.Enabled = true;
                setAdvancedFilterToolStripMenuItem.Enabled = true;
                removeThisFilterToolStripMenuItem.Enabled = true;
                findInThisColumnToolStripMenuItem.Enabled = true;
                sortThisColumnToolStripMenuItem.Enabled = true;
                dropAllFiltersToolStripMenuItem.Enabled = true;
                viewCurrentFilterToolStripMenuItem.Enabled = true;


                if (((IUntypedBindingList)_untypedBindingList).IsFiltered)
                {
                    DataGridViewColumn column = datagrid.Columns[currentCell.ColumnIndex];
                    DataGridViewColumnHeaderCell header = column.HeaderCell;
                    if ((header.Style.Font == null) || !header.Style.Font.Italic)
                    {
                        removeThisFilterToolStripMenuItem.Enabled = false;
                    }
                    else
                    {
                        removeThisFilterToolStripMenuItem.Enabled = true;
                    }
                    dropAllFiltersToolStripMenuItem.Enabled = true;
                    viewCurrentFilterToolStripMenuItem.Enabled = true;
                }
                else
                {
                    removeThisFilterToolStripMenuItem.Enabled = false;
                    dropAllFiltersToolStripMenuItem.Enabled = false;
                    viewCurrentFilterToolStripMenuItem.Enabled = false;
                }
                

                // get the rectangle of the current cell and show the menu at the cell center
                if (!(currentCell.ColumnIndex < 0 || currentCell.RowIndex < 0))
                {
                    Rectangle rectangle = datagrid.GetCellDisplayRectangle(currentCell.ColumnIndex, currentCell.RowIndex, true);
                    int x = rectangle.Left + rectangle.Width / 2;
                    int y = rectangle.Top + rectangle.Height / 2;
                    mnuGridMenu.Show(datagrid, x, y);
                }
            }
        }

        // make the cell that was right-clicked the current cell
        void datagrid_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            toolStripSeparatorColumnItems.Visible = false;
            columnHeadingStyleToolStripMenuItem.Visible = false;
            columnWidthToolStripMenuItem.Visible = false;

            if (!((e.ColumnIndex < 0) || (e.RowIndex < 0)))
            {   // make the right-clicked cell the grid current cell
                datagrid.CurrentCell = datagrid[e.ColumnIndex, e.RowIndex];     // this forces a call to BeginEdit()
            }
            else
            {   // right click was on a header, ContextMenuStrip should not be shown.
                CancelContextMenu = true;
            }
        }

        // handle request for "Filter by selection"
        private void addToFilterCriteriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int columnIndex = datagrid.CurrentCell.ColumnIndex;
            string matchValue;

            if (datagrid.CurrentCell.Value == null)
                matchValue = null;
            else
                matchValue = datagrid.CurrentCell.Value.ToString();

            string propertyName = datagrid.Columns[datagrid.CurrentCell.ColumnIndex].Name;
            object value = datagrid.CurrentCell.Value;
            Filter filterItem = new Filter(propertyName, MatchType.EqualTo, matchValue);
            List<Filter> anyColumnFilterList = new List<Filter>();
            if (value == null)
                filterItem.DataTypeCode = TypeCode.Empty;
            else
                filterItem.DataTypeCode = Type.GetTypeCode(value.GetType());

            ApplyFilterToGrid(filterItem, propertyName);

            datagrid.CurrentCell = datagrid[columnIndex, 0];
        }

        // handle request for "Set advanced filter..."
        private void setAdvancedFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int columnIndex = datagrid.CurrentCell.ColumnIndex;

            string matchValue = datagrid.CurrentCell.Value.ToString();
            string propertyName = datagrid.Columns[datagrid.CurrentCell.ColumnIndex].Name;
            Filter filterItem = new Filter(propertyName, MatchType.BeginsWith, matchValue);
            object value = datagrid.CurrentCell.Value;
            filterItem.DataTypeCode = Type.GetTypeCode(value.GetType());

            SetAdvancedFilterForm advancedFilterForm = new SetAdvancedFilterForm(datagrid, filterItem);
            advancedFilterForm.ShowDialog();

            if (advancedFilterForm.DialogResult == DialogResult.OK)
            {
                ApplyFilterToGrid(filterItem, propertyName);
            }

            advancedFilterForm.Dispose();
            if (datagrid.Rows.Count > 0)
            {
                datagrid.CurrentCell = datagrid[columnIndex, 0];
            }
        }

        // remove all filters
        private void removeAllFiltersToolStripMenuItem_Click(object sender, EventArgs e)
        {

            _untypedBindingList.RemoveFilter();
            _gridFilters.Clear();

            foreach (DataGridViewColumn column in datagrid.Columns)
            {
                column.HeaderCell.Style.Font = new Font(this.Font, FontStyle.Regular);
            }

            if (_untypedBindingList.IsSorted)
            {
                DoColumnSort(_untypedBindingList, _sortDescriptionDictionary);
            }

            if (datagrid.CurrentCell != null)
            {
                datagrid.CurrentCell = datagrid[datagrid.CurrentCell.ColumnIndex, 0];
            }
        }

        // remove filters for selected column
        private void removeThisFiltersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int columnIndex = datagrid.CurrentCell.ColumnIndex;
            string targetPropertyName = datagrid.Columns[datagrid.CurrentCell.ColumnIndex].Name;

            _gridFilters.Remove(targetPropertyName);         // remove the filter for this column from the group
            DataGridViewColumn column = datagrid.Columns[targetPropertyName];
            column.HeaderCell.Style.Font = new Font(this.Font, FontStyle.Regular);  // update the column header to show no filter

            _untypedBindingList.RemoveFilter();                // remove all filters from the data list

            // set each remaining filter to indicate that it needs to be applied
            foreach (string propertyName in _gridFilters.Keys)
            {
                List<Filter> filters = _gridFilters[propertyName];
                foreach (Filter filter in filters)
                {
                    filter.IsApplied = false;
                }
            }

            _untypedBindingList.Filter = Utilities.SerializeGridFiltersToString(_gridFilters);  // apply the remaining filters

            // set each filter to indicate that it has been applied
            foreach (string propertyName in _gridFilters.Keys)
            {
                List<Filter> filters = _gridFilters[propertyName];
                foreach (Filter filter in filters)
                {
                    filter.IsApplied = true;
                }
            }

            if (_untypedBindingList.IsSorted)
            {
                DoColumnSort(_untypedBindingList, _sortDescriptionDictionary);
            }
            datagrid.CurrentCell = datagrid[columnIndex, 0];
        }

        // show the currently applied filters
        private void viewCurrentFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowCurrentFiltersForm filterSummary = new ShowCurrentFiltersForm(_gridFilters);
            filterSummary.ShowDialog();

            filterSummary.Dispose();
        }

        // remove sorting on the data
        private void unsortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int columnIndex = datagrid.CurrentCell.ColumnIndex;

            _untypedBindingList.RemoveSort();
            _sortDescriptionDictionary.Clear();
            SetColumnSortGlyphs();

            datagrid.CurrentCell = datagrid[columnIndex, 0];
        }

        // find an item in a column by full text match
        private void findInThisColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string cellText = string.Empty;
            string cellTextUpper = string.Empty;
            int rowNumber = 0;
            int columnIndex = datagrid.CurrentCell.ColumnIndex;
            Dictionary<string, int> uniqueRows = new Dictionary<string, int>(datagrid.Rows.Count);
            List<string> uniqueTextList = new List<string>(datagrid.Rows.Count);
            

            // build a dictionary keyed by text with row index as value
            foreach (DataGridViewRow row in datagrid.Rows)
            {
                cellText = row.Cells[columnIndex].Value.ToString();
                cellTextUpper = cellText.ToUpper();
                if (!uniqueRows.ContainsKey(cellTextUpper))
                {
                    uniqueRows.Add(cellTextUpper, rowNumber);
                    uniqueTextList.Add(cellText);
                }
                rowNumber++;
            }

            // open the lookup form with keys to be used as autocomplete values

            SelectTextToFind form = new SelectTextToFind(uniqueTextList);
            form.StartPosition = FormStartPosition.CenterParent;
            
            form.ShowDialog();

            if (form.DialogResult == DialogResult.OK)
            {
                // create a dictionary with keys in uppercase so that lookup will succeed even if user changed the case
                Dictionary<string, int> uniqueRowsUpperKeys = new Dictionary<string, int>(uniqueRows.Count);
                foreach (string key in uniqueRows.Keys)
                {
                    uniqueRowsUpperKeys.Add(key.ToUpper(), uniqueRows[key]);
                }
                // find lookup text and get the index of the corresponding row
                string findText = form.SelectedText.ToUpper();
                int rowIndex = uniqueRowsUpperKeys[findText];
                // set the current cell to the found row index and the current column
                datagrid.CurrentCell=datagrid.Rows[rowIndex].Cells[columnIndex];

                // make the current row the top displayed row
                datagrid.FirstDisplayedScrollingRowIndex = datagrid.CurrentCell.RowIndex;
            }

            form.Dispose();
        }

        // handle menu request for column sorting
        private void sortThisColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            datagrid_ColumnHeaderMouseClick(null, null);
        }

        private void friendlyNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetColumnHeadings("FriendlyName");
        }

        private void propertyNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetColumnHeadings("PropertyName");
        }

        private void descriptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetColumnHeadings("Description");
        }

        private void databaseNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetColumnHeadings("DatabaseName");
        }

        private void autoSizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetColumnWidths("Auto");
        }

        private void defaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetColumnWidths("Default");
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintTheGrid();
        }

        private void exportToExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] titleLines = null;
            HAI.Core.Windows.UI.Controls.ExportToExcel.ExportDataGridView(datagrid, titleLines);
        }

        #endregion

        #region DataGridView event handlers

        // handle format event to set text for open begin date and open end date
        void datagrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.Value != null)
            {
                if (e.Value.GetType() == typeof(DateTime))
                {
                    if (((DateTime)e.Value) == _openBeginDate)
                    {
                        e.Value = "(open begin date)";
                        e.FormattingApplied = true;
                        return;
                    }
                    if (((DateTime)e.Value) == _openEndDate)
                    {
                        e.Value = "(open end date)";
                        e.FormattingApplied = true;
                        return;
                    }
                }
            }
        }

        #endregion

        #region Keyboard event handlers

        void datagrid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 17)
            {
                _ctrlKeyIsDown = false;
            }
        }

        void datagrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 17)
            {
                _ctrlKeyIsDown = true;
            }
        }

        void datagrid_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ((char)6))
            {
                findInThisColumnToolStripMenuItem_Click(null, null);
                _ctrlKeyIsDown = false;              // needed because ctrlKeyIsDown was set True when ctrl-F was pressed
            }
        }

        #endregion

        #region Utilities

        // put sort glyphs on the column headers
        private void SetColumnSortGlyphs()
        {
            ListSortDescription sortDescription;

            // remove sort glyphs on all columns
            foreach (DataGridViewColumn column in datagrid.Columns)
            {
                column.HeaderCell.SortGlyphDirection = SortOrder.None;
            }

            // set the glyphs for sorted columns
            foreach (string key in _sortDescriptionDictionary.Keys)
            {
                DataGridViewColumn column = datagrid.Columns[key];
                sortDescription = _sortDescriptionDictionary[key];
                if (sortDescription.SortDirection == ListSortDirection.Ascending)
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                }
                else
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.Descending;
                }
            }
        }

        // Apply the current filters
        private void ApplyFilterToGrid(Filter filterItem, string propertyName)
        {
            List<Filter> anyColumnFilterList = new List<Filter>();

            if (!_gridFilters.ContainsKey(propertyName))
            {
                _gridFilters.Add(propertyName, new List<Filter>());
            }
            anyColumnFilterList = _gridFilters[propertyName];
            anyColumnFilterList.Add(filterItem);

            try
            {
                string filtersAsString = Utilities.SerializeGridFiltersToString(_gridFilters);
                _untypedBindingList.Filter = filtersAsString;
                DataGridViewColumn column = datagrid.Columns[propertyName];
                column.HeaderCell.Style.Font = new Font(this.Font, FontStyle.Italic);

                // mark the filters as applied
                foreach (string key in _gridFilters.Keys)
                {
                    anyColumnFilterList = _gridFilters[key];
                    foreach (Filter filter in anyColumnFilterList)
                    {
                        filterItem.IsApplied = true;
                    }
                }

                if (_untypedBindingList.IsSorted)
                {
                    DoColumnSort(_untypedBindingList, _sortDescriptionDictionary);
                }

            }
            catch (HaiDataCompareException ex)
            {
                MessageBox.Show(ex.Message, "Filter specification error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        // configure the DataGridView control for display
        public void PrepareGridForDisplay(DataAccessResult dataFetchResult, List<string> visibleColumnOrder)
        {
            _dataFetchResult = dataFetchResult;
            _visibleColumnOrder = visibleColumnOrder;
            PrepareGridForDisplay();
        }

        // set up the datagrid so that is ready to show
        private void PrepareGridForDisplay()
        {
            _openBeginDate = Utilities.GetOpenBeginDate();
            _openEndDate = Utilities.GetOpenEndDate();

            // set up the grid and the binding source
            datagrid.AllowUserToAddRows = false;
            datagrid.AllowUserToDeleteRows = false;
            datagrid.AllowUserToOrderColumns = true;
            datagrid.RowHeadersVisible = true;
            datagrid.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
            datagrid.MultiSelect = true;
            this.BorderStyle = BorderStyle.None;
            _specialObjectType = HaiBusinessObjectType.UnknownHaiBusinessObjectType;

            if (_dataFetchResult.Success)
            {
                _untypedBindingList = (IUntypedBindingList)_dataFetchResult.DataList;
                _untypedBindingList.UpdateSortGlyphs += new EventHandler(ucReadonlyDatagridForm_UpdateSortGlyphs);
                _browsablePropertyList = _dataFetchResult.BrowsablePropertyList;
                if (_untypedBindingList.Count == 0 || ((HaiBusinessObjectBase)(_untypedBindingList[0])).GetReportSpecificationDict().Count == 0)
                {
                    printToolStripMenuItem.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("Data fetch failed.  " + _dataFetchResult.Message, "Error", 
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            _bs = new BindingSource();
            _bs.DataSource = _untypedBindingList;

            if (_untypedBindingList == null || _untypedBindingList.Count == 0)
            {
                MessageBox.Show("No data retrieved.", "Warning", 
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (_untypedBindingList==null)
                    return;
            }

            if (_visibleColumnOrder == null || _visibleColumnOrder.Count == 0)
            {    // autogenerate the columns
                if (_visibleColumnOrder!=null && _visibleColumnOrder.Count == 0)     // empty list; show warning
                {
                    MessageBox.Show("Column list has no entries.  Default applied.", "Error", 
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                datagrid.AutoGenerateColumns = true;
                datagrid.DataSource = _bs;

                // Boolean columns require special treatment for sorting and null values
                foreach (DataGridViewColumn column in datagrid.Columns)
                {
                    if (!_browsablePropertyList.ContainsKey(column.Name))
                    {
                        MessageBox.Show("Class definition error.", "System error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        BrowsableProperty bp = _browsablePropertyList[column.Name];
                        if (bp.DataType == TypeCode.Boolean)
                        {
                            column.SortMode = DataGridViewColumnSortMode.Programmatic;
                            ((DataGridViewCheckBoxCell)column.CellTemplate).ThreeState = true;
                            ((DataGridViewCheckBoxCell)column.CellTemplate).FalseValue = false;
                            ((DataGridViewCheckBoxCell)column.CellTemplate).TrueValue = true;
                            column.DefaultCellStyle.NullValue = CheckState.Indeterminate;
                        }
                    }
                }
            }
            else
            {   // create the columns and set their properties
                datagrid.AutoGenerateColumns = false;
                datagrid.DataSource = _bs;

                int displayIndex = 0;

                foreach (string propertyName in _visibleColumnOrder)                            // iterate the properties   
                {
                    DataGridViewColumn column;
                    BrowsableProperty bp = _browsablePropertyList[propertyName];
                    if (bp.DataType == TypeCode.Boolean)
                    {                                       // booleans required special cell type
                        DataGridViewCheckBoxCell checkboxCell = new DataGridViewCheckBoxCell();
                        checkboxCell.ThreeState = true;
                        checkboxCell.ValueType = typeof(CheckState);
                        checkboxCell.FalseValue = false;
                        checkboxCell.TrueValue = true;
                        column = new DataGridViewColumn(checkboxCell);
                        column.DefaultCellStyle.NullValue = CheckState.Indeterminate;
                        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                        column.Name = propertyName;
                        column.HeaderText = propertyName;
                        column.SortMode = DataGridViewColumnSortMode.Programmatic;

                        datagrid.Columns.Add(column);
                    }
                    else
                    {
                        int columnIndex = datagrid.Columns.Add(propertyName, propertyName);
                        column = datagrid.Columns[columnIndex];
                    }

                    column.DataPropertyName = propertyName;                                     // set name for binding
                    column.DisplayIndex = displayIndex; ;                                       // set display position
                    displayIndex++;
                }
            }

            // set the string used to indicate null valus in the data grid; Booleans are handled by tri-state checkbox
            foreach (DataGridViewColumn column in datagrid.Columns)
            {
                string columnName = column.Name;
                BrowsableProperty bp = _browsablePropertyList[columnName];
                if (bp.DataType != TypeCode.Boolean)
                    column.DefaultCellStyle.NullValue = StringForNull;
            }


            // assign handler to format cells, especially DateTime
            datagrid.CellFormatting += new DataGridViewCellFormattingEventHandler(datagrid_CellFormatting);

            SetColumnHeadings("FriendlyName");

            datagrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }

        private void ucReadonlyDatagridForm_UpdateSortGlyphs(object sender, EventArgs e)
        {
            SetColumnSortGlyphs();
        }

        // utility to add a column to the list of sorted columns
        private void AddSortDescription(string columnName, ListSortDirection sortDirection)
        {
            ListSortDescription sortDescription;
            System.Collections.IList dataList = _bs.List;
            HaiBusinessObjectBase firstDataObject = (HaiBusinessObject.HaiBusinessObjectBase)dataList[0];
            Type dataObjectType = firstDataObject.GetType();
            PropertyDescriptorCollection dataObjectProperties = TypeDescriptor.GetProperties(firstDataObject);
            PropertyDescriptor dataObjectProperty = dataObjectProperties.Find(columnName, false);
            sortDescription = new ListSortDescription(dataObjectProperty, sortDirection);
            _sortDescriptionDictionary.Add(columnName, sortDescription);
        }

        private void PrintTheGrid()
        {
            BindingSource bs = (BindingSource)datagrid.DataSource;
            IUntypedBindingList bindingList = (IUntypedBindingList)bs.DataSource;
            MetaDataReportViewer reportViewer = new MetaDataReportViewer(bindingList, _gus.HideReportSelection);
            reportViewer.Show();
        }

        #endregion

        #region Properties

        public HaiBusinessObjectType ActionRedirectionObjectType
        {
            get
            {
                return _specialObjectType;
            }
            set
            {
                _specialObjectType = value;
            }
        }

        public DataGridView DataGridView
        {
            get
            {
                return datagrid;
            }
        }

        // this property is needed to inject GUS into the control, if it has been dropped onto the form (instead of built on the fly)
        public GloballyUsefulStuff GUS
        {
            set
            {
                _gus = value;
            }
        }

        #endregion

        #region Public Methods

        // open MetaDataReportViewer to show the current contents of the grid
        public void PrintGridReport()
        {
            PrintTheGrid();
        }

        // get a list of the HaiBusinessObjectBase items selected in the grid and ensure that current cell is selected
        public HaiBindingList<HaiBusinessObjectBase> GetReadonlyBindingListOfSelectedItems(bool ensureRowForCurrentCellIsSelected)
        {
            if (ensureRowForCurrentCellIsSelected)
            {
                SelectRowForCurrentCell();
            }

            return GetReadonlyBindingListOfSelectedItems();
        }

        // get a list of the HaiBusinessObjectBase items selected in the grid
        public HaiBindingList<HaiBusinessObjectBase> GetReadonlyBindingListOfSelectedItems()
        {
            HaiBindingList<HaiBusinessObjectBase> selectedItems = new HaiBindingList<HaiBusinessObjectBase>();

            foreach (DataGridViewRow row in datagrid.Rows)
            {
                if (row.Selected)
                {
                    selectedItems.AddToList((HaiBusinessObjectBase)_untypedBindingList[row.Index]);
                }
            }

            return selectedItems;
        }

        // get a stronly-typed list of the items selected in the grid and ensure that current cell is selected
        public HaiBindingList<T> GetReadonlyBindingListOfSelectedItems<T>(bool ensureRowForCurrentCellIsSelected)
        {
            if (ensureRowForCurrentCellIsSelected)
            {
                SelectRowForCurrentCell();
            }

            return GetReadonlyBindingListOfSelectedItems<T>();
        }

        // get a stronly-typed list of the items selected in the grid
        public HaiBindingList<T> GetReadonlyBindingListOfSelectedItems<T>()
        {
            HaiBindingList<T> selectedItems = new HaiBindingList<T>();

            foreach (DataGridViewRow row in datagrid.Rows)
            {
                if (row.Selected)
                {
                    selectedItems.AddToList((T)_untypedBindingList[row.Index]);
                }
            }

            return selectedItems;
        }

        public ContextMenuStrip GetContextMenu()
        {
            return mnuGridMenu;
        }

        public bool SelectRowForCurrentCell()
        {
            if (datagrid.CurrentCell == null)
            {
                return false;
            }

            if (datagrid.SelectedRows.Count == 0)
            {
                if (datagrid.CurrentCell.RowIndex > -1)
                {
                    datagrid.Rows[datagrid.CurrentCell.RowIndex].Selected = true;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        #endregion

        public void Dipose()
        {
            _bs.Dispose();
            base.Dispose();
        }


        

        // TESTING STUFF

    }
}