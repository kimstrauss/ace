namespace HaiBusinessUI
{
    partial class ucReadonlyDatagridForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.datagrid = new System.Windows.Forms.DataGridView();
            this.mnuGridMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.applyEqualFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAdvancedFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.removeThisFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dropAllFiltersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.viewCurrentFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.findInThisColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.sortThisColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unsortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparatorColumnItems = new System.Windows.Forms.ToolStripSeparator();
            this.columnHeadingStyleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.friendlyNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commonNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.descriptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.columnWidthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid)).BeginInit();
            this.mnuGridMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // datagrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.datagrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.datagrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagrid.ContextMenuStrip = this.mnuGridMenu;
            this.datagrid.Location = new System.Drawing.Point(3, 3);
            this.datagrid.Name = "datagrid";
            this.datagrid.RowHeadersWidth = 25;
            this.datagrid.Size = new System.Drawing.Size(750, 355);
            this.datagrid.TabIndex = 1;
            this.datagrid.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.datagrid_ColumnHeaderMouseClick);
            // 
            // mnuGridMenu
            // 
            this.mnuGridMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applyEqualFilterToolStripMenuItem,
            this.setAdvancedFilterToolStripMenuItem,
            this.toolStripSeparator2,
            this.removeThisFilterToolStripMenuItem,
            this.dropAllFiltersToolStripMenuItem,
            this.toolStripSeparator4,
            this.viewCurrentFilterToolStripMenuItem,
            this.toolStripSeparator3,
            this.findInThisColumnToolStripMenuItem,
            this.toolStripSeparator1,
            this.sortThisColumnToolStripMenuItem,
            this.unsortToolStripMenuItem,
            this.toolStripSeparatorColumnItems,
            this.columnHeadingStyleToolStripMenuItem,
            this.columnWidthToolStripMenuItem,
            this.toolStripSeparator5,
            this.exportToExcelToolStripMenuItem,
            this.printToolStripMenuItem});
            this.mnuGridMenu.Name = "mnuGridMenu";
            this.mnuGridMenu.ShowImageMargin = false;
            this.mnuGridMenu.Size = new System.Drawing.Size(166, 326);
            this.mnuGridMenu.Opening += new System.ComponentModel.CancelEventHandler(this.mnuGridMenu_Opening);
            // 
            // applyEqualFilterToolStripMenuItem
            // 
            this.applyEqualFilterToolStripMenuItem.Name = "applyEqualFilterToolStripMenuItem";
            this.applyEqualFilterToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.applyEqualFilterToolStripMenuItem.Text = "Fil&ter by selection";
            this.applyEqualFilterToolStripMenuItem.Click += new System.EventHandler(this.addToFilterCriteriaToolStripMenuItem_Click);
            // 
            // setAdvancedFilterToolStripMenuItem
            // 
            this.setAdvancedFilterToolStripMenuItem.Name = "setAdvancedFilterToolStripMenuItem";
            this.setAdvancedFilterToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.setAdvancedFilterToolStripMenuItem.Text = "Set &advanced filter...";
            this.setAdvancedFilterToolStripMenuItem.Click += new System.EventHandler(this.setAdvancedFilterToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(162, 6);
            // 
            // removeThisFilterToolStripMenuItem
            // 
            this.removeThisFilterToolStripMenuItem.Name = "removeThisFilterToolStripMenuItem";
            this.removeThisFilterToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.removeThisFilterToolStripMenuItem.Text = "&Remove this filter";
            this.removeThisFilterToolStripMenuItem.Click += new System.EventHandler(this.removeThisFiltersToolStripMenuItem_Click);
            // 
            // dropAllFiltersToolStripMenuItem
            // 
            this.dropAllFiltersToolStripMenuItem.Name = "dropAllFiltersToolStripMenuItem";
            this.dropAllFiltersToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.dropAllFiltersToolStripMenuItem.Text = "&Drop all filters";
            this.dropAllFiltersToolStripMenuItem.Click += new System.EventHandler(this.removeAllFiltersToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(162, 6);
            // 
            // viewCurrentFilterToolStripMenuItem
            // 
            this.viewCurrentFilterToolStripMenuItem.Name = "viewCurrentFilterToolStripMenuItem";
            this.viewCurrentFilterToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.viewCurrentFilterToolStripMenuItem.Text = "&View current filters";
            this.viewCurrentFilterToolStripMenuItem.Click += new System.EventHandler(this.viewCurrentFilterToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(162, 6);
            // 
            // findInThisColumnToolStripMenuItem
            // 
            this.findInThisColumnToolStripMenuItem.Name = "findInThisColumnToolStripMenuItem";
            this.findInThisColumnToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.findInThisColumnToolStripMenuItem.Text = "&Find in this column...";
            this.findInThisColumnToolStripMenuItem.Click += new System.EventHandler(this.findInThisColumnToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(162, 6);
            // 
            // sortThisColumnToolStripMenuItem
            // 
            this.sortThisColumnToolStripMenuItem.Name = "sortThisColumnToolStripMenuItem";
            this.sortThisColumnToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.sortThisColumnToolStripMenuItem.Text = "&Sort this column";
            this.sortThisColumnToolStripMenuItem.Click += new System.EventHandler(this.sortThisColumnToolStripMenuItem_Click);
            // 
            // unsortToolStripMenuItem
            // 
            this.unsortToolStripMenuItem.Name = "unsortToolStripMenuItem";
            this.unsortToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.unsortToolStripMenuItem.Text = "&Unsort";
            this.unsortToolStripMenuItem.Click += new System.EventHandler(this.unsortToolStripMenuItem_Click);
            // 
            // toolStripSeparatorColumnItems
            // 
            this.toolStripSeparatorColumnItems.Name = "toolStripSeparatorColumnItems";
            this.toolStripSeparatorColumnItems.Size = new System.Drawing.Size(162, 6);
            // 
            // columnHeadingStyleToolStripMenuItem
            // 
            this.columnHeadingStyleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.friendlyNameToolStripMenuItem,
            this.commonNameToolStripMenuItem,
            this.descriptionToolStripMenuItem,
            this.databaseNameToolStripMenuItem});
            this.columnHeadingStyleToolStripMenuItem.Name = "columnHeadingStyleToolStripMenuItem";
            this.columnHeadingStyleToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.columnHeadingStyleToolStripMenuItem.Text = "Column &heading style";
            // 
            // friendlyNameToolStripMenuItem
            // 
            this.friendlyNameToolStripMenuItem.Name = "friendlyNameToolStripMenuItem";
            this.friendlyNameToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.friendlyNameToolStripMenuItem.Text = "Friendly name";
            this.friendlyNameToolStripMenuItem.Click += new System.EventHandler(this.friendlyNameToolStripMenuItem_Click);
            // 
            // commonNameToolStripMenuItem
            // 
            this.commonNameToolStripMenuItem.Name = "commonNameToolStripMenuItem";
            this.commonNameToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.commonNameToolStripMenuItem.Text = "&Property name";
            this.commonNameToolStripMenuItem.Click += new System.EventHandler(this.propertyNameToolStripMenuItem_Click);
            // 
            // descriptionToolStripMenuItem
            // 
            this.descriptionToolStripMenuItem.Name = "descriptionToolStripMenuItem";
            this.descriptionToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.descriptionToolStripMenuItem.Text = "&Description";
            this.descriptionToolStripMenuItem.Click += new System.EventHandler(this.descriptionToolStripMenuItem_Click);
            // 
            // databaseNameToolStripMenuItem
            // 
            this.databaseNameToolStripMenuItem.Name = "databaseNameToolStripMenuItem";
            this.databaseNameToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.databaseNameToolStripMenuItem.Text = "Data&base name";
            this.databaseNameToolStripMenuItem.Click += new System.EventHandler(this.databaseNameToolStripMenuItem_Click);
            // 
            // columnWidthToolStripMenuItem
            // 
            this.columnWidthToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoSizeToolStripMenuItem,
            this.defaultToolStripMenuItem});
            this.columnWidthToolStripMenuItem.Name = "columnWidthToolStripMenuItem";
            this.columnWidthToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.columnWidthToolStripMenuItem.Text = "Column &width";
            // 
            // autoSizeToolStripMenuItem
            // 
            this.autoSizeToolStripMenuItem.Name = "autoSizeToolStripMenuItem";
            this.autoSizeToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.autoSizeToolStripMenuItem.Text = "&Auto size";
            this.autoSizeToolStripMenuItem.Click += new System.EventHandler(this.autoSizeToolStripMenuItem_Click);
            // 
            // defaultToolStripMenuItem
            // 
            this.defaultToolStripMenuItem.Name = "defaultToolStripMenuItem";
            this.defaultToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.defaultToolStripMenuItem.Text = "&Default";
            this.defaultToolStripMenuItem.Click += new System.EventHandler(this.defaultToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(162, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.printToolStripMenuItem.Text = "Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // exportToExcelToolStripMenuItem
            // 
            this.exportToExcelToolStripMenuItem.Name = "exportToExcelToolStripMenuItem";
            this.exportToExcelToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.exportToExcelToolStripMenuItem.Text = "Export to Excel";
            this.exportToExcelToolStripMenuItem.Click += new System.EventHandler(this.exportToExcelToolStripMenuItem_Click);
            // 
            // ucReadonlyDatagridForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.datagrid);
            this.Name = "ucReadonlyDatagridForm";
            this.Size = new System.Drawing.Size(758, 363);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.datagrid)).EndInit();
            this.mnuGridMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView datagrid;
        private System.Windows.Forms.ContextMenuStrip mnuGridMenu;
        private System.Windows.Forms.ToolStripMenuItem applyEqualFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findInThisColumnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewCurrentFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem setAdvancedFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dropAllFiltersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unsortToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeThisFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem sortThisColumnToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparatorColumnItems;
        private System.Windows.Forms.ToolStripMenuItem columnHeadingStyleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem columnWidthToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commonNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem descriptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoSizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem defaultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem friendlyNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToExcelToolStripMenuItem;
    }
}

