﻿namespace HAI.Core.Windows.UI.Controls
{
    partial class BasicEditDialogBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bindingSourceErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.propertyValidationErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.propertyValidationWarningProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.propertyValidationInformationProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyValidationErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyValidationWarningProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyValidationInformationProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // bindingSourceErrorProvider
            // 
            this.bindingSourceErrorProvider.ContainerControl = this;
            // 
            // propertyValidationErrorProvider
            // 
            this.propertyValidationErrorProvider.ContainerControl = this;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.CausesValidation = false;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(507, 339);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(426, 339);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // propertyValidationWarningProvider
            // 
            this.propertyValidationWarningProvider.ContainerControl = this;
            // 
            // propertyValidationInformationProvider
            // 
            this.propertyValidationInformationProvider.ContainerControl = this;
            // 
            // BasicEditDialogBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 374);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BasicEditDialogBase";
            this.Text = "BasicEditDialogBase";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyValidationErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyValidationWarningProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyValidationInformationProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Button btnCancel;
        protected System.Windows.Forms.Button btnOK;
        protected System.Windows.Forms.ErrorProvider bindingSourceErrorProvider;
        protected System.Windows.Forms.ErrorProvider propertyValidationErrorProvider;
        protected System.Windows.Forms.ErrorProvider propertyValidationWarningProvider;
        protected System.Windows.Forms.ErrorProvider propertyValidationInformationProvider;
    }
}