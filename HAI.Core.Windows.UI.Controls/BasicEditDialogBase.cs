﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HAI.Core.Windows.UI.Controls
{
    public partial class BasicEditDialogBase : Form
    {
        protected BindingSource targetObjectBindingSource;
        protected ToolTip lookupButtonToolTip = new ToolTip();

        public BasicEditDialogBase()
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.CenterParent;

            propertyValidationErrorProvider.Icon = Utilities.GetMessageIcon(MessageLevel.Error);
            propertyValidationErrorProvider.BlinkStyle = ErrorBlinkStyle.BlinkIfDifferentError;
            propertyValidationWarningProvider.Icon = Utilities.GetMessageIcon(MessageLevel.Warning);
            propertyValidationWarningProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;
            propertyValidationInformationProvider.Icon = Utilities.GetMessageIcon(MessageLevel.Information);
            propertyValidationInformationProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;

            bindingSourceErrorProvider.Icon = Utilities.GetMessageIcon(MessageLevel.Error);
            bindingSourceErrorProvider.BlinkStyle = ErrorBlinkStyle.BlinkIfDifferentError;

            // Set up the delays for the ToolTip.
            lookupButtonToolTip.AutoPopDelay = 5000;
            lookupButtonToolTip.InitialDelay = 500;
            lookupButtonToolTip.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            lookupButtonToolTip.ShowAlways = true;
        }

        protected void CompleteFormInitialization()
        {
            SetCheckBoxThreeState();
        }

        private void SetCheckBoxThreeState()
        {
            // Set CheckBox property ThreeState = true for object properties that are "bool?" (Nullable<bool>)

                //Type sourceType = (Type)(targetObjectBindingSource.DataSource);
                //Type sourceType = (targetObjectBindingSource.DataSource).GetType();
                //object instanceOfBoundType = System.Activator.CreateInstance(sourceType);

            object boundObject = targetObjectBindingSource.DataSource;

            foreach (Control aControl in this.Controls)
            {
                if (!(aControl is CheckBox))
                    continue;

                if (aControl.DataBindings.Count == 0)
                    continue;

                Binding theBinding = aControl.DataBindings[0];
                string boundPropertyName = theBinding.BindingMemberInfo.BindingMember;
                System.Reflection.PropertyInfo propertyInfo = boundObject.GetType().GetProperty(boundPropertyName);
                if (propertyInfo.PropertyType.IsGenericType)
                {
                    if (propertyInfo.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    {
                        bool propertyIsBoolean = propertyInfo.PropertyType.IsInstanceOfType(new bool());
                        if (propertyIsBoolean)
                            ((CheckBox)aControl).ThreeState = true;
                    }
                }
            }
        }

        protected void TextBoxValidator(object sender, CancelEventArgs e)
        {
            TextBox theTextBox = (TextBox)sender;
            theTextBox.DataBindings[0].WriteValue();
            if (!string.IsNullOrEmpty(bindingSourceErrorProvider.GetError(theTextBox)))   // there is a binding error, don't continue
            {
                theTextBox.SelectAll();
                return;
            }
            else
            {
                propertyValidationErrorProvider.SetError(theTextBox, string.Empty);
                propertyValidationWarningProvider.SetError(theTextBox, string.Empty);
                propertyValidationInformationProvider.SetError(theTextBox, string.Empty);
            }

            string propName = (theTextBox).DataBindings[0].BindingMemberInfo.BindingField;
            ValidationResult validationResult = ((IValidatableObject)(targetObjectBindingSource.DataSource)).ValidateSimpleProperty(propName);
            ErrorProvider messageProvider = null;

            if (validationResult.MessageCount > 0)
            {
                // prevent user from leaving the TextBox when there is an error
                if (validationResult.MaxMessageLevel == MessageLevel.Error)
                    e.Cancel = true;

                if (validationResult.MessageCount == 1  && validationResult.MaxMessageLevel != MessageLevel.None)
                {
                    string theError = validationResult.GetSingleMessage();

                    switch (validationResult.MaxMessageLevel)
                    {
                        case MessageLevel.None:
                            break;
                        case MessageLevel.Information:
                            messageProvider = propertyValidationInformationProvider;
                            break;
                        case MessageLevel.Warning:
                            messageProvider = propertyValidationWarningProvider;
                            break;
                        case MessageLevel.Error:
                            theTextBox.SelectAll();
                            messageProvider = propertyValidationErrorProvider;
                            break;
                        default:
                            throw new Exception("Unknown ValidationResult.MaxMessageLevel");
                    }

                    messageProvider.SetError(theTextBox, theError);
                }
                else
                {
                    string theMessages = validationResult.GetAllMessages();
                    MessageBoxIcon messageIcon;

                    switch (validationResult.MaxMessageLevel)
                    {
                        case MessageLevel.None:
                            messageIcon = MessageBoxIcon.None;
                            break;
                        case MessageLevel.Information:
                            messageIcon = MessageBoxIcon.Information;
                            break;
                        case MessageLevel.Warning:
                            messageIcon = MessageBoxIcon.Warning;
                            break;
                        case MessageLevel.Error:
                            messageIcon = MessageBoxIcon.Error;
                            break;
                        default:
                            messageIcon = MessageBoxIcon.None;
                            break;
                    }

                    propertyValidationInformationProvider.SetError(theTextBox, "Multiple messages");
                    MessageBox.Show(theMessages, "Validation messages", MessageBoxButtons.OK, messageIcon);
                }
            }
            else
            {
                propertyValidationErrorProvider.SetError(theTextBox, string.Empty);
                propertyValidationWarningProvider.SetError(theTextBox, string.Empty);
                propertyValidationInformationProvider.SetError(theTextBox, string.Empty);
            }
        }

        protected void TextBoxConstrainedValidator(object sender, CancelEventArgs e, List<ILookupItem> lookupList)
        {
            TextBoxConstrainedValidator(sender, e, lookupList, null);
        }

        protected void TextBoxConstrainedValidator(object sender, CancelEventArgs e, List<ILookupItem> lookupList, Button lookupButton)
        {
            TextBox theTextBox = (TextBox)sender;
            theTextBox.DataBindings[0].WriteValue();
            if (!string.IsNullOrEmpty(bindingSourceErrorProvider.GetError(theTextBox)))    // there is a binding error, don't continue
            {
                theTextBox.SelectAll();
                return;
            }
            else
            {
                propertyValidationErrorProvider.SetError(theTextBox, string.Empty);
                propertyValidationWarningProvider.SetError(theTextBox, string.Empty);
                propertyValidationInformationProvider.SetError(theTextBox, string.Empty);
            }

            string propName = (theTextBox).DataBindings[0].BindingMemberInfo.BindingField;
            ValidationResult validationResult = ((IValidatableObject)(targetObjectBindingSource.DataSource)).ValidateLookupProperty(propName, lookupList);
            ErrorProvider messageProvider = null;

            if (validationResult.MessageCount > 0)
            {
                // prevent user from leaving the TextBox when there is an Error unless he is trying to use the lookup
                if ((validationResult.MaxMessageLevel == MessageLevel.Error) && (this.ActiveControl != lookupButton))
                    e.Cancel = true;

                if (validationResult.MessageCount < 3 && validationResult.MaxMessageLevel != MessageLevel.None)
                {
                    string theError = validationResult.GetAllMessages();

                    switch (validationResult.MaxMessageLevel)
                    {
                        case MessageLevel.None:
                            break;
                        case MessageLevel.Information:
                            messageProvider = propertyValidationInformationProvider;
                            break;
                        case MessageLevel.Warning:
                            messageProvider = propertyValidationWarningProvider;
                            break;
                        case MessageLevel.Error:
                            theTextBox.SelectAll();
                            messageProvider = propertyValidationErrorProvider;
                            break;
                        default:
                            throw new Exception("Unknown ValidationResult.MaxMessageLevel");
                    }

                    messageProvider.SetError(theTextBox, theError);
                }
                else
                {
                    string theMessages = validationResult.GetAllMessages();
                    MessageBoxIcon messageIcon;

                    switch (validationResult.MaxMessageLevel)
                    {
                        case MessageLevel.None:
                            messageIcon = MessageBoxIcon.None;
                            break;
                        case MessageLevel.Information:
                            messageIcon = MessageBoxIcon.Information;
                            break;
                        case MessageLevel.Warning:
                            messageIcon = MessageBoxIcon.Warning;
                            break;
                        case MessageLevel.Error:
                            messageIcon = MessageBoxIcon.Error;
                            break;
                        default:
                            messageIcon = MessageBoxIcon.None;
                            break;
                    }

                    propertyValidationInformationProvider.SetError(theTextBox, "Multiple messages");
                    MessageBox.Show(theMessages, "Validation messages", MessageBoxButtons.OK, messageIcon);
                }

                return;
            }
            else
            {
                propertyValidationErrorProvider.SetError(theTextBox, string.Empty);
                propertyValidationWarningProvider.SetError(theTextBox, string.Empty);
                propertyValidationInformationProvider.SetError(theTextBox, string.Empty);

                return;
            }
        }

        protected void ComboBoxLookupValidator(object sender, CancelEventArgs e)
        {
            ComboBox theComboBox = (ComboBox)sender;
            theComboBox.DataBindings[0].WriteValue();
            if (!string.IsNullOrEmpty(bindingSourceErrorProvider.GetError(theComboBox)))    // there is a binding error, don't continue
            {
                theComboBox.SelectAll();
                return;
            }
            else
            {
                propertyValidationErrorProvider.SetError(theComboBox, string.Empty);
                propertyValidationWarningProvider.SetError(theComboBox, string.Empty);
                propertyValidationInformationProvider.SetError(theComboBox, string.Empty);
            }

            string propName = (theComboBox).DataBindings[0].BindingMemberInfo.BindingField;
            List<ILookupItem> lookupList = new List<ILookupItem>();
            foreach (object item in theComboBox.Items)
            {
                lookupList.Add((ILookupItem)item);
            }

            ValidationResult validationResult = ((IValidatableObject)(targetObjectBindingSource.DataSource)).ValidateLookupProperty(propName, lookupList);
            ErrorProvider messageProvider = null;

            if (validationResult.MessageCount > 0)
            {
                // prevent user from leaving the ComboBox when there is an error
                if (validationResult.MaxMessageLevel == MessageLevel.Error)
                    e.Cancel = true;

                if (validationResult.MessageCount == 1 && validationResult.MaxMessageLevel != MessageLevel.None)
                {
                    string theError = validationResult.GetSingleMessage();

                    switch (validationResult.MaxMessageLevel)
                    {
                        case MessageLevel.None:
                            break;
                        case MessageLevel.Information:
                            messageProvider = propertyValidationInformationProvider;
                            break;
                        case MessageLevel.Warning:
                            messageProvider = propertyValidationWarningProvider;
                            break;
                        case MessageLevel.Error:
                            theComboBox.SelectAll();
                            messageProvider = propertyValidationErrorProvider;
                            break;
                        default:
                            throw new Exception("Unknown ValidationResult.MaxMessageLevel");
                    }

                    messageProvider.SetError(theComboBox, theError);
                }
                else
                {
                    string theMessages = validationResult.GetAllMessages();
                    MessageBoxIcon messageIcon;

                    switch (validationResult.MaxMessageLevel)
                    {
                        case MessageLevel.None:
                            messageIcon = MessageBoxIcon.None;
                            break;
                        case MessageLevel.Information:
                            messageIcon = MessageBoxIcon.Information;
                            break;
                        case MessageLevel.Warning:
                            messageIcon = MessageBoxIcon.Warning;
                            break;
                        case MessageLevel.Error:
                            messageIcon = MessageBoxIcon.Error;
                            break;
                        default:
                            messageIcon = MessageBoxIcon.None;
                            break;
                    }

                    propertyValidationInformationProvider.SetError(theComboBox, "Multiple messages");
                    MessageBox.Show(theMessages, "Validation messages", MessageBoxButtons.OK, messageIcon);
                }

                return;
            }
            else
            {
                propertyValidationErrorProvider.SetError(theComboBox, string.Empty);
                propertyValidationWarningProvider.SetError(theComboBox, string.Empty);
                propertyValidationInformationProvider.SetError(theComboBox, string.Empty);
            }
        }

        protected bool IsObjectValid(Dictionary<string, List<HAI.Core.ILookupItem>> lookupLists)
        {
            ValidationResult validationResult = ((IValidatableObject)(targetObjectBindingSource.DataSource)).ValidateObject(lookupLists);

            if (validationResult.MessageCount == 0)
                return true;

            string theMessages = validationResult.GetAllMessages();
            MessageBoxIcon messageIcon;

            switch (validationResult.MaxMessageLevel)
            {
                case MessageLevel.None:
                    messageIcon = MessageBoxIcon.None;
                    break;
                case MessageLevel.Information:
                    messageIcon = MessageBoxIcon.Information;
                    break;
                case MessageLevel.Warning:
                    messageIcon = MessageBoxIcon.Warning;
                    break;
                case MessageLevel.Error:
                    messageIcon = MessageBoxIcon.Error;
                    break;
                default:
                    messageIcon = MessageBoxIcon.None;
                    break;
            }

            if (validationResult.MaxMessageLevel == MessageLevel.Information)
            {
                MessageBox.Show(theMessages, "Validation messages", MessageBoxButtons.OK, messageIcon);
                return true;
            }
            else
            {
                DialogResult yesNo;
                if (validationResult.MaxMessageLevel > MessageLevel.Warning)
                {
                    theMessages = theMessages + "\r\n\nErrors found.  Edits cannot be saved.";
                    MessageBox.Show(theMessages, "", MessageBoxButtons.OK, messageIcon);
                    yesNo = DialogResult.No;
                }
                else
                {
                    theMessages = theMessages + "\r\n\nContinue anyway?";
                    yesNo = MessageBox.Show(theMessages, "", MessageBoxButtons.YesNo, messageIcon);
                }

                if (yesNo == DialogResult.Yes)
                    return true;
                else
                    return false;
            }
        }

        protected ILookupItem GetLookupItem(object textBox, List<ILookupItem> lookupList)
        {
            string codeValue = ((TextBox)textBox).Text.Trim();
            ILookupItem item = lookupList.Find(x => x.Code.ToLower() == codeValue.ToLower());
            return item;
        }
    }
}
