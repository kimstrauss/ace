﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAI.Core.Windows.UI.Controls
{
    [Serializable]
    public class DGVColumnSettings
    {
        public DGVColumnSettings()
        {
            this.ColumnWidth = 100;
            this.ColumnName = string.Empty;
        }

        public string ColumnName { get; set; }

        public int DisplayIndex { get; set; }

        public int ColumnWidth { get; set; }
    }
}
