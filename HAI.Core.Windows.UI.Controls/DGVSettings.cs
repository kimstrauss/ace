﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HAI.Core.Windows.UI.Controls
{
    [Serializable]
    public enum DGVSettingMode
    {
        Automatic,
        ColumnHeaders,
        Manual
    }

    public enum ColumnHeadingStyle
    {
        Standard,
        FullText
    }

    [Serializable]
    public class DGVSettings
    {
        private List<DGVColumnSettings> _columnSettings;

        public DGVSettings()
        {
            this.SettingMode = DGVSettingMode.ColumnHeaders;
            _columnSettings = new List<DGVColumnSettings>();
        }
        
        public DGVSettingMode SettingMode { get; set; }

        public ColumnHeadingStyle ColumnHeadingStyle { get; set; }

        public List<DGVColumnSettings> ColumnSettings
        {
            get
            {
                return _columnSettings;
            }
            set
            {
                _columnSettings = value;
            }
        }

        public static bool operator ==(DGVSettings lhsSettings, DGVSettings rhsSettings)
        {
            if (((object)lhsSettings) == null)
            {
                if (((object)rhsSettings) == null)
                    return true;
                else
                    return false;
            }
            else
                return lhsSettings.Equals(rhsSettings);
        }

        public static bool operator !=(DGVSettings lhsSettings, DGVSettings rhsSettings)
        {
            return !lhsSettings.Equals(rhsSettings);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (!(obj is DGVSettings))
                return false;

            DGVSettings theOtherSettings = (DGVSettings)obj;

            if (theOtherSettings.SettingMode != this.SettingMode)
                return false;

            if (theOtherSettings.ColumnSettings.Count != _columnSettings.Count)
                return false;

            if (theOtherSettings.ColumnHeadingStyle != this.ColumnHeadingStyle)
                return false;

            Dictionary<string, DGVColumnSettings> otherColumnSettingsDict;
            try
            {
                otherColumnSettingsDict = theOtherSettings._columnSettings.ToDictionary<DGVColumnSettings, string>(x => x.ColumnName);
            }
            catch
            {
                return false;
            }
            foreach (DGVColumnSettings columnSettings in _columnSettings)
            {
                if (!otherColumnSettingsDict.ContainsKey(columnSettings.ColumnName))
                    return false;
                DGVColumnSettings otherColumnSettings = otherColumnSettingsDict[columnSettings.ColumnName];
                if (otherColumnSettings.ColumnWidth != columnSettings.ColumnWidth)
                    return false;
                if (otherColumnSettings.DisplayIndex != columnSettings.DisplayIndex)
                    return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
