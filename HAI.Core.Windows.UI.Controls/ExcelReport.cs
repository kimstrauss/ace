﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;


namespace HAI.Core.Windows.UI.Controls
{
    public static class ExportToExcel
    {
        public static void ExportDataGridView(DataGridView theGrid, string[] titleLines)
        {
            Excel.Application excelApplication = null;
            Excel.Workbooks workbooks = null;
            Excel._Workbook workbook = null;
            Excel.Sheets worksheets = null;
            Excel.Worksheet worksheet = null;

            try
            {
                int nRows = theGrid.Rows.Count;
                int nColumns = theGrid.Columns.Count;
                Dictionary<int, int> columnNumberMapping = new Dictionary<int, int>();

                // establish mapping of columns by their display index; ignore the hidden column(s)
                foreach (DataGridViewColumn aColumn in theGrid.Columns)
                {
                    if (aColumn.Visible)
                        columnNumberMapping.Add(aColumn.DisplayIndex, aColumn.Index);
                    else
                        nColumns--;
                }

                if (nRows < 1 || nColumns < 1)
                {
                    MessageBox.Show("No data for report.", "Data error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                // create the workbook; delete unneeded worksheets
                excelApplication = new Microsoft.Office.Interop.Excel.Application();
                workbooks = excelApplication.Workbooks;
                workbook = workbooks.Add(Missing.Value);
                worksheets = (Excel.Sheets)workbook.Worksheets;
                while (worksheets.Count > 1)
                {
                    ((Excel.Worksheet)worksheets.get_Item(worksheets.Count)).Delete();
                }
                worksheet = (Excel.Worksheet)worksheets.get_Item(1);

                // prepare control variables
                int topRow = 1;         // allow insertion to start at any row; could be a calling parameter
                int leftColumn = 1;     // allow insertion to start at any column; could be a calling parameter
                bool freezePanes = true;// freeze the row labels and column headers; could be a calling parameter
                string worksheetName = "Meta data";    // text for the sheet tab; could be a calling parameter
                int nColumnHeaders = 1; // number of rows dedicated to column headers; could be a calling parameter

                int nTitleLines = 0;    // number of lines in the title
                int columnNumber = 0;   // a counter for the visible columns

                if (titleLines != null)
                    nTitleLines = (titleLines.GetUpperBound(0) + 1) + 1;  // add a blank line btwn title and data

                worksheet.Name = worksheetName;

                // create the range on the worksheet; size must match size of values to be inserted
                Excel.Range entireRange = worksheet.get_Range(worksheet.Cells[topRow, leftColumn], worksheet.Cells[topRow - 1 + nTitleLines + nColumnHeaders + nRows, leftColumn - 1 + nColumns]);
                object[,] values = new object[nRows + nTitleLines + nColumnHeaders, nColumns];

                //create the title lines
                for (int i = 0; i < nTitleLines; i++)
                {
                    // initialize all title cells to empty strings
                    for (int j = 0; j < nColumns; j++)
                    {
                        values[i, j] = string.Empty;
                    }
                }

                // insert the data into the array
                int rowNumber = 0;
                for (int i = 0; i < nRows; i++)
                {
                    columnNumber = 0;
                    for (int j = 0; j < theGrid.Columns.Count; j++)
                    {
                        if (!columnNumberMapping.ContainsKey(j))
                            continue;   // skip this; it is not a visible/mapped column;

                        DataGridViewColumn theMappedColumn = theGrid.Columns[columnNumberMapping[j]];
                        object value = theGrid.Rows[i].Cells[theMappedColumn.Index].Value;  // get the cell value

                        // convert a null data value to an empty string
                        if (value == System.DBNull.Value)
                            value = string.Empty;

                        // if the column type is String, make sure that numbers are displayed in their string form
                        if (theMappedColumn.ValueType == typeof(string))
                        {
                            double aDouble = 0;
                            if (double.TryParse((string)value, out aDouble))
                                value = @"'" + value.ToString();    // it is numeric, so prepend an apostrophe
                        }

                        // if column type is boolen, convert the value to a string
                        if (theMappedColumn.ValueType == typeof(bool))
                        {
                            bool theBoolean = false;
                            if (bool.TryParse(value.ToString(), out theBoolean))
                            {
                                if (theBoolean)
                                    value = "Yes";
                                else
                                    value = "No";
                            }
                            else
                                value = string.Empty;
                        }

                        // assign the value to the array
                        values[rowNumber + nTitleLines + nColumnHeaders, columnNumber] = value;
                        columnNumber++;
                    }
                    rowNumber++;
                }

                // set the values into the range
                entireRange.set_Value(Missing.Value, values);

                //autosize the columns
                for (int j = 0; j < entireRange.Columns.Count; j++)
                {
                    Excel.Range dataRange = worksheet.get_Range(worksheet.Cells[topRow, leftColumn + j], worksheet.Cells[topRow, leftColumn + j]);
                    dataRange.EntireColumn.AutoFit();

                    if ((double)(dataRange.EntireColumn.ColumnWidth) < 12)
                        dataRange.EntireColumn.ColumnWidth = 12;
                }

                // set the text for the column headers
                for (int i = 0; i < nColumnHeaders; i++)
                {
                    columnNumber = 0;
                    for (int j = 0; j < theGrid.Columns.Count; j++)
                    {
                        if (!columnNumberMapping.ContainsKey(j))
                            continue;

                        DataGridViewColumn theMappedColumn = theGrid.Columns[columnNumberMapping[j]];
                        entireRange.Cells[i + 1 + nTitleLines, columnNumber + 1] = theMappedColumn.HeaderCell.Value.ToString();
                        columnNumber++;
                    }

                    // make the header font bold and allow text to wrap
                    Excel.Range headerRange = worksheet.get_Range(worksheet.Cells[topRow + nTitleLines, leftColumn], worksheet.Cells[topRow + nTitleLines + i, leftColumn - 1 + nColumns]);
                    headerRange.Font.Bold = true;
                    headerRange.WrapText = true;
                }

                // freeze the first column and the last column header
                Excel.Range activeCell = (Excel.Range)entireRange.Cells[topRow + nTitleLines + 1, leftColumn + 1];
                activeCell.Activate();

                excelApplication.ActiveWindow.FreezePanes = freezePanes;

                // make a "table" on the worksheet
                try
                {
                    worksheet.ListObjects.Add(Excel.XlListObjectSourceType.xlSrcRange, Missing.Value, Missing.Value, Excel.XlYesNoGuess.xlYes, Missing.Value);
                }
                catch
                {
                    // ignore error resulting from down-version of Excel cannot create tables on the sheet.
                }

                // set the row and column headers that are to repeat on printed pages
                activeCell = (Excel.Range)entireRange.Cells[nTitleLines + 1, 1];
                string addressBottom = string.Empty;
                addressBottom = activeCell.get_Address(Missing.Value, Missing.Value, Excel.XlReferenceStyle.xlA1, Missing.Value, Missing.Value);
                worksheet.PageSetup.PrintTitleRows = "$A$1:" + addressBottom;
                worksheet.PageSetup.PrintTitleColumns = "$A$1:" + addressBottom;
                // ... other page setup options
                worksheet.PageSetup.LeftFooter = @"Printed &D at &T";
                worksheet.PageSetup.RightFooter = @"Page &P of &N";

                // put title lines on each vertical strip of the area to print,
                // ... and merage and center each of these title lines 
                if (nTitleLines > 0)
                {
                    /*      *** Code implement titles on each "strip" that will be printed. ***
                     * The following commented-out code fails when there are more than 3 "strips".  Although 
                     * pageStripCount = worksheet.VPageBreaks.Count;
                     * may report more than 3, attempting to index past the third VPagePage throws an error
                     *      eg. Excel.VPageBreak nextStripBreak = worksheet.VPageBreaks[4];
                     * will fail even when pageStripCount equals 5.
                     * */

                    //Excel.Range titleRange = null;
                    //int pageStripCount = worksheet.VPageBreaks.Count;
                    //for (int titleLine = 0; titleLine < nTitleLines - 1; titleLine++)
                    //{
                    //    // set and format titles for strips that have page breaks (there may not be any)
                    //    int thisBreakColumn = leftColumn + 1;
                    //    for (int stripNumber = 0; stripNumber < pageStripCount; stripNumber++)
                    //    {
                    //        Excel.VPageBreak nextStripBreak = worksheet.VPageBreaks[stripNumber + 1];
                    //        int nextBreakColumn = nextStripBreak.Location.Column;
                    //        entireRange.Cells[titleLine + 1, thisBreakColumn - leftColumn + 1] = titleLines[titleLine];

                    //        titleRange = worksheet.get_Range(worksheet.Cells[topRow + titleLine, thisBreakColumn], worksheet.Cells[topRow + titleLine, nextBreakColumn - 1]);
                    //        titleRange.Merge(false);
                    //        titleRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    //        titleRange.Font.Bold = true;

                    //        thisBreakColumn = nextBreakColumn;
                    //    }

                    //    // if there is an orphan strip then set and format titles for it
                    //    if (theGrid.Columns.Count + leftColumn - 1 > thisBreakColumn)
                    //    {
                    //        entireRange.Cells[titleLine + 1, thisBreakColumn - leftColumn + 1] = titleLines[titleLine];

                    //        titleRange = worksheet.get_Range(worksheet.Cells[topRow + titleLine, thisBreakColumn], worksheet.Cells[topRow + titleLine, theGrid.Columns.Count + leftColumn - 2]);
                    //        titleRange.Merge(false);
                    //        titleRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    //        titleRange.Font.Bold = true;
                    //    }
                    //}


                    // Create a single title (may have multiple lines)
                    Excel.Range titleRange = null;
                    for (int titleLine = 0; titleLine < nTitleLines - 1; titleLine++)
                    {
                        entireRange.Cells[titleLine + 1, 1] = titleLines[titleLine];

                        titleRange = worksheet.get_Range(worksheet.Cells[topRow + titleLine, 1], worksheet.Cells[topRow + titleLine, 1]);
                        titleRange.Merge(false);
                        titleRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                        titleRange.Font.Bold = true;
                    }


                    activeCell = (Excel.Range)entireRange.Cells[nTitleLines, 1];    // put the "cursor" below the titles
                }
                else
                {
                    activeCell = (Excel.Range)entireRange.Cells[1, 1];  // put the cursor in the top-left cell
                }

                activeCell.Activate();  // show the "cursor"

                // let the user see and control the spreadsheet
                excelApplication.Visible = true;
                excelApplication.UserControl = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Excel automation error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                // see these links for releasing the objects that get created
                // http://social.msdn.microsoft.com/Forums/en-US/clr/thread/e93c3ed0-3331-4133-9b7c-e36fdb9b4942
                // http://geekswithblogs.net/jolson/archive/2004/02/03/1716.aspx

                // end the attached Excel processes
                if (worksheet != null)
                    Marshal.ReleaseComObject(worksheet);
                if (worksheets != null)
                    Marshal.ReleaseComObject(worksheets);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApplication != null)
                    Marshal.ReleaseComObject(excelApplication);
            }
        }
    }
}
