using System;
using System.Collections.Generic;
using System.Text;

namespace HAI.Core.Windows.UI.Controls
{
    [Serializable]
    public class Filter
    {
        public Filter()
        {
            _matchValueList = new List<string>();
        }

        public Filter(string propertyName, MatchType matchType, string matchValue)
        {
            _matchValueList = new List<string>();
            _propertyName = propertyName;
            _matchType = matchType;
            _matchValueList.Add(matchValue);
        }

        private bool _isApplied;
        public bool IsApplied
        {
            get
            {
                return _isApplied;
            }
            set
            {
                _isApplied = value;
            }
        }

        private string _propertyName;
        public string PropertyName
        {
            get
            {
                return _propertyName;
            }
            set
            {
                _propertyName = value;
            }
        }

        private MatchType _matchType;
        public MatchType MatchType
        {
            get
            {
                return _matchType;
            }
            set
            {
                _matchType = value;
            }
        }

        private List<string> _matchValueList;
        public List<string> MatchValueList
        {
            get
            {
                return _matchValueList;
            }
        }


        private System.TypeCode _dataTypeCode;
        public System.TypeCode DataTypeCode
        {
            get
            {
                return _dataTypeCode;
            }
            set
            {
                _dataTypeCode = value;
            }
        }

    }

    public enum MatchType
    {
        Ignore,
        EqualTo,
        BeginsWith,
        EndsWith,
        Contains,
        NotEqual,
        NotContains,
        LT,
        LE,
        EQ,
        GE,
        GT,
        NE
    }
}
