﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using System.Data;
using System.Reflection;

using HAI.Core;

namespace HAI.Core.Windows.UI.Controls
{
    public class HaiDataTable <T> : DataTable where T : IObjectId
    {
        private List<PropertyInfo> _browsableHardPropertyGetters;       // property getters for browsable hard properties
        private IPropertyBag _aPrototypeSoftPropertyObject = null;      // prototype of a soft property object of type T 
        private string[] _softPropertyKeys = null;                      // names of the soft properties
        private List<IPropertyBagPropertyBase> _browsableSoftProperties;    // collection of the soft properties
        private List<string> _namesOfOrderedColumns = new List<string>();

        private List<T> _items = new List<T>();

        #region Constructors
        public HaiDataTable()
            : base()
        {
        }

        public HaiDataTable(IEnumerable<T> items)
            : base()
        {
            if (TypeImplementsInterface(typeof(T), typeof(IPropertyBag)))
            {
                throw new Exception(typeof(T).Name + " implements IPropetyBag; use that constructor");
            }
            else
            {
                InitializeHardPropertyTable();
            }

            // add a row to this derived DataTable for each item in _items
            foreach (T item in items)
            {
                AddRow(item);
            }
        }

        public HaiDataTable(IEnumerable<T> items, List<string> namesOfOrderedColumns)
            : base()
        {
            _namesOfOrderedColumns = namesOfOrderedColumns;

            if (TypeImplementsInterface(typeof(T), typeof(IPropertyBag)))
            {
                throw new Exception(typeof(T).Name + " implements IPropetyBag; use that constructor");
            }
            else
            {
                InitializeHardPropertyTable();
            }

            // add a row to this derived DataTable for each item in _items
            foreach (T item in items)
            {
                AddRow(item);
            }
        }

        public HaiDataTable(IEnumerable<T> items, IPropertyBag aPrototypeSoftPropertyObject)
            : base()
        {
            if (aPrototypeSoftPropertyObject is IPropertyBag)
            {
                _aPrototypeSoftPropertyObject = aPrototypeSoftPropertyObject;
                InitializeSoftPropertyTable();
            }
            else
            {
                throw new Exception("Prototype object does not implement IPropertyBag"); ;
            }

            // add a row to this derived DataTable for each item in _items
            foreach (T item in items)
            {
                AddRow(item);
            }
        }
        #endregion

        #region Public methods
        public List<T> Items
        {
            get
            {
                return _items;
            }
        }

        public void AddRow(T item)
        {
            DataRow theNewRow = this.NewRow(); // create a new row
            this.Rows.Add(theNewRow);          // add the row to the table
            SetRowFields(theNewRow, item);     // set the table column values
            _items.Add(item);
        }

        public void UpdateRow(T item)
        {
            DataRow theRow = FindRow(item);
            SetRowFields(theRow, item);         // set the table column values
        }

        public void DeleteRow(T item)
        {
            DataRow theRow = FindRow(item);
            theRow.Delete();
            _items.Remove(item);
        }

        public void DeleteRow(IObjectId item)
        {
            T typedItem = default(T);

            for (int i = 0; i < _items.Count; i++)
            {
                if (_items[i].Id == item.Id)
                {
                    typedItem = _items[i];
                    break;
                }
            }

            if (typedItem == null)
            {
                throw new Exception("Item not found");
            }

            DataRow theRow = FindRow(typedItem);
            theRow.Delete();
            _items.Remove(typedItem);
        }

        private DataRow FindRow(T item)
        {
            DataRow theRow = null;
            IObjectId itemToFindAsBase = item as IObjectId;
            int idToFind = itemToFindAsBase.Id;

            foreach (DataRow anyRow in this.Rows)
            {
                IObjectId anyItemAsBase = anyRow[0] as IObjectId;
                if (anyItemAsBase.Id == idToFind)
                {
                    theRow = anyRow;
                    break;
                }
            }

            return theRow;
        }
        #endregion

        #region Private methods
        private void InitializeHardPropertyTable()
        {
            _browsableHardPropertyGetters = CreateBrowsableHardPropertyGetters();
            CreateTableColumns();
        }

        private void InitializeSoftPropertyTable()
        {
            _browsableSoftProperties = CreateBrowsableSoftProperties();
            CreateTableColumns();
        }

        private List<IPropertyBagPropertyBase> CreateBrowsableSoftProperties()
        {
            List<IPropertyBagPropertyBase> softProperties = new List<IPropertyBagPropertyBase>();

            // load properties from the prototype member into list; order the list by the column ordinal;
            // ... extract the propertyKey strings into an array -- propertyKeys used to get property values
            // ... in desired order for presentation in the grid.
            foreach (string key in _aPrototypeSoftPropertyObject.BaseProperties.Keys)
            {
                softProperties.Add(_aPrototypeSoftPropertyObject.BaseProperties[key]);
            }

            softProperties = softProperties.OrderBy<IPropertyBagPropertyBase, int>(x => x.DescriptorBase.UIOrdinal).ToList<IPropertyBagPropertyBase>();
            _softPropertyKeys = new String[softProperties.Count];

            for (int i = 0; i < softProperties.Count; i++)
            {
                _softPropertyKeys[i] = softProperties[i].DescriptorBase.PropertyKey;
            }

            return softProperties;
        }

        private List<PropertyInfo> CreateBrowsableHardPropertyGetters()
        {
            List<PropertyInfo> browsablePropertyGetters = new List<PropertyInfo>(); // list of Browsable(true) properties

            // get all public instance properties that are not marked as not browsable.
            PropertyInfo[] propertyGetters = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);
            foreach (PropertyInfo propertyGetter in propertyGetters)
            {
                bool useThisGetter = true;
                object[] attributes = propertyGetter.GetCustomAttributes(true);
                foreach (object attribute in attributes)    // check to see if this property is marked as not browsable.
                {
                    if (attribute is System.ComponentModel.BrowsableAttribute)
                    {
                        useThisGetter = ((System.ComponentModel.BrowsableAttribute)attribute).Browsable;
                    }
                }

                if (useThisGetter)
                    browsablePropertyGetters.Add(propertyGetter);
            }

            return browsablePropertyGetters;
        }

        private void CreateTableColumns()
        {
            // create the columns in theDataTable that correspond to the browsable properties
            DataColumn column;

            if (_browsableHardPropertyGetters != null)
            {
                column = this.Columns.Add("~Column1", typeof(T));   // the first column will hold the item 
                foreach (PropertyInfo propertyGetter in _browsableHardPropertyGetters)
                {
                    Type theType = propertyGetter.PropertyType;

                    // objects will be shown in the grid as strings using .ToString()
                    // ... so the data type of the column must be String
                    if (!theType.IsPrimitive)
                    {
                        theType = typeof(string);
                    }

                    column = this.Columns.Add(propertyGetter.Name, theType);
                    column.Caption = propertyGetter.Name;
                }

                // override column Captions to alternate text, if possible
                if (TypeImplementsInterface(typeof(T), typeof(IAlternateColumnNames)))
                {
                    Type typeOfT = typeof(T);
                    Object prototypeOfT = typeOfT.InvokeMember(null,
                        BindingFlags.DeclaredOnly |
                        BindingFlags.Public | BindingFlags.NonPublic |
                        BindingFlags.Instance | BindingFlags.CreateInstance, null, null, null);

                    Dictionary<string, string> alternateColumnNamesDict = ((IAlternateColumnNames)prototypeOfT).GetAlternateNamesDict();
                    if (alternateColumnNamesDict != null)
                    {
                        foreach (DataColumn aColumn in this.Columns)
                        {
                            if (alternateColumnNamesDict.ContainsKey(aColumn.ColumnName))
                            {
                                aColumn.Caption = alternateColumnNamesDict[aColumn.ColumnName];
                            }
                        }
                    }
                }
            }

            if (_browsableSoftProperties != null)
            {
                column = this.Columns.Add("~Column1", typeof(IPropertyBag));
                foreach (string propertyKey in _softPropertyKeys)
                {
                    IPropertyBagPropertyDescriptorBase descriptor = _aPrototypeSoftPropertyObject.BaseProperties[propertyKey].DescriptorBase;
                    Type columnType = Type.GetType(descriptor.DataType);
                    column = this.Columns.Add(descriptor.Name, columnType);
                    column.Caption = descriptor.Code;
                }
            }

            // set columns to the pre-specified order
            Dictionary<string, DataColumn> columnDictionary = new Dictionary<string, DataColumn>();
            foreach (DataColumn aColumn in this.Columns)
            {
                columnDictionary.Add(aColumn.ColumnName, aColumn);
            }

            List<DataColumn> newColumnList = new List<DataColumn>();
            newColumnList.Add(columnDictionary["~Column1"]);
            columnDictionary.Remove("~Column1");
            foreach (string columnName in _namesOfOrderedColumns)
            {
                if (columnDictionary.ContainsKey(columnName))
                {
                newColumnList.Add(columnDictionary[columnName]);
                columnDictionary.Remove(columnName);
                }
                else
                {
                    MessageBox.Show("Unknown property name for column ordering: '" + columnName + "'", "System error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            foreach (string columnName in columnDictionary.Keys)
            {
                newColumnList.Add(columnDictionary[columnName]);
            }

            this.Columns.Clear();
            foreach (DataColumn aColumn in newColumnList)
            {
                this.Columns.Add(aColumn);
            }
        }

        private DataRow SetRowFields(DataRow theRow, T item)
        {
            theRow.BeginEdit();     // suspend events
            theRow[0] = item;       // insert the item in the first column

            // insert (browsable) property values in the remaining columns
            if (item is IPropertyBag)
            {
                int columnCount = this.Columns.Count;
                object[] propertyValueArray = new object[columnCount];

                IPropertyBag member = item as IPropertyBag;
                for (int i = 1; i < columnCount; i++)   // N.B. column[0] of dataTable holds the object
                {
                    object value = null;
                    if (member.BaseProperties[_softPropertyKeys[i - 1]].Value == null)
                        value = DBNull.Value;
                    else
                        value = member.BaseProperties[_softPropertyKeys[i - 1]].Value;

                    // objects will be shown in the grid as strings using .ToString()
                    if (!value.GetType().IsPrimitive && (value != DBNull.Value))
                    {
                        value = value.ToString();
                    }

                    theRow[member.BaseProperties[_softPropertyKeys[i - 1]].DescriptorBase.Name] = value;
                }
            }
            else
            {
                foreach (PropertyInfo propertyGetter in _browsableHardPropertyGetters)
                {
                    theRow[propertyGetter.Name] = propertyGetter.GetGetMethod().Invoke(item, null);
                }
            }

            theRow.EndEdit();       // commit updates and resume event firings
            return theRow;
        }

        private bool TypeImplementsInterface(Type type, Type interfaceType)
        {
            string interfaceFullName = interfaceType.FullName;
            return type.GetInterface(interfaceFullName) != null;
        }
        #endregion
    }
}
