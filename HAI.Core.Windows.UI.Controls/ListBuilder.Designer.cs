﻿namespace HAI.Core.Windows.UI.Controls
{
    partial class ListBuilder<T>
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAssign = new System.Windows.Forms.Button();
            this.btnUnAssign = new System.Windows.Forms.Button();
            this.btnFlip = new System.Windows.Forms.Button();
            this.navAssigned = new HAI.Core.Windows.UI.Controls.NavigationGrid();
            this.navAvailable = new HAI.Core.Windows.UI.Controls.NavigationGrid();
            this.SuspendLayout();
            // 
            // btnAssign
            // 
            this.btnAssign.Font = new System.Drawing.Font("Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnAssign.Location = new System.Drawing.Point(300, 50);
            this.btnAssign.Name = "btnAssign";
            this.btnAssign.Size = new System.Drawing.Size(50, 23);
            this.btnAssign.TabIndex = 2;
            this.btnAssign.Text = ">>";
            this.btnAssign.UseVisualStyleBackColor = false;
            this.btnAssign.Click += new System.EventHandler(this.btnAssign_Click);
            // 
            // btnUnAssign
            // 
            this.btnUnAssign.Font = new System.Drawing.Font("Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnUnAssign.Location = new System.Drawing.Point(300, 80);
            this.btnUnAssign.Name = "btnUnAssign";
            this.btnUnAssign.Size = new System.Drawing.Size(50, 23);
            this.btnUnAssign.TabIndex = 3;
            this.btnUnAssign.Text = "<<";
            this.btnUnAssign.UseVisualStyleBackColor = false;
            this.btnUnAssign.Click += new System.EventHandler(this.btnUnAssign_Click);
            // 
            // btnFlip
            // 
            this.btnFlip.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFlip.Location = new System.Drawing.Point(300, 358);
            this.btnFlip.Name = "btnFlip";
            this.btnFlip.Size = new System.Drawing.Size(50, 23);
            this.btnFlip.TabIndex = 6;
            this.btnFlip.Text = "Flip";
            this.btnFlip.UseVisualStyleBackColor = false;
            this.btnFlip.Click += new System.EventHandler(this.btnFlip_Click);
            // 
            // navAssigned
            // 
            this.navAssigned.DisableSettingPersistence = false;
            this.navAssigned.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navAssigned.GridLabel = "Assigned items";
            this.navAssigned.Location = new System.Drawing.Point(350, 0);
            this.navAssigned.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.navAssigned.MultiSelect = true;
            this.navAssigned.Name = "navAssigned";
            this.navAssigned.Size = new System.Drawing.Size(300, 384);
            this.navAssigned.TabIndex = 5;
            this.navAssigned.RowDoubleClick += new HAI.Core.Windows.UI.Controls.NavigationGrid.RowDoubleClickEventHandler(this.navAssigned_RowDoubleClick);
            // 
            // navAvailable
            // 
            this.navAvailable.DisableSettingPersistence = false;
            this.navAvailable.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navAvailable.GridLabel = "Available items";
            this.navAvailable.Location = new System.Drawing.Point(0, 0);
            this.navAvailable.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.navAvailable.MultiSelect = true;
            this.navAvailable.Name = "navAvailable";
            this.navAvailable.Size = new System.Drawing.Size(300, 384);
            this.navAvailable.TabIndex = 4;
            this.navAvailable.RowDoubleClick += new HAI.Core.Windows.UI.Controls.NavigationGrid.RowDoubleClickEventHandler(this.navAvailable_RowDoubleClick);
            // 
            // ListBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnFlip);
            this.Controls.Add(this.navAssigned);
            this.Controls.Add(this.navAvailable);
            this.Controls.Add(this.btnUnAssign);
            this.Controls.Add(this.btnAssign);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MinimumSize = new System.Drawing.Size(330, 135);
            this.Name = "ListBuilder";
            this.Size = new System.Drawing.Size(650, 384);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAssign;
        private System.Windows.Forms.Button btnUnAssign;
        private NavigationGrid navAvailable;
        private NavigationGrid navAssigned;
        private System.Windows.Forms.Button btnFlip;

    }
}
