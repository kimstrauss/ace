﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HAI.Core.Windows.UI.Controls
{
    public partial class ListBuilder<T> : UserControl where T : IObjectId
    {
        HaiDataTable<T> _availableItemsTable;
        HaiDataTable<T> _assignedItemsTable;
        private string _availableItemsLabel = string.Empty;
        private string _assignedItemsLabel = string.Empty;
        private ToolTip tt = new ToolTip();
        private bool isOverUnderOrientation = false;

    #region  Symbols (from the Symbol font) for button characters.
        string upSymbol = '\u00DD'.ToString();      // "Ý";
        string downSymbol = '\u00DF'.ToString();    // "ß";
        string leftSymbol = '\u00DC'.ToString();    // "Ü";
        string rightSymbol = '\u00DE'.ToString();   // "Þ";

        string rightArrow = '\u00AE'.ToString();    // "®";
        string leftArrow = '\u00AC'.ToString();     // "¬";
        string upArrow = '\u00AD'.ToString();
        string downArrow = '\u00AF'.ToString();     // "­­­­­­¯";
    #endregion

    #region Constructor
        public ListBuilder(List<T> availableItems, string availableItemsLabel, List<T> assignedItems, string assignedItemsLabel, ListBuilderPlaceholder placeholder, bool disableSettingsPersistence)
        {
            InitializeComponent();

            _availableItemsLabel = availableItemsLabel;
            _assignedItemsLabel = assignedItemsLabel;

            this.DisableSettingPersistence = disableSettingsPersistence;
            this.Location = new Point(placeholder.Location.X, placeholder.Location.Y);
            this.Size = new Size(placeholder.Size.Width, placeholder.Size.Height);

            placeholder.Parent.Controls.Add(this);
            placeholder.Parent.Controls.Remove(placeholder);
            SetupControl(availableItems, assignedItems);

            isOverUnderOrientation = false;
        }
    #endregion

    #region Properties
        public bool DisableSettingPersistence
        { get; set; }
    #endregion

    #region Public methods
        // Toggle between OverUnder and LeftAndRight grid layout
        public void ToggleOrientation()
        {
            if (isOverUnderOrientation)
                OrientToLeftAndRight();
            else
                OrientToOverUnder();
        }

        // retrieve the items that are in the assigned grid
        public List<T> GetAssignedItems()
        {
            return ((HaiDataTable<T>)navAssigned.DataTable).Items;
        }
    #endregion
        
    #region Private methods
        // make the layout of the grids OverUnder
        private void OrientToOverUnder()
        {
            btnAssign.Size = new System.Drawing.Size(23, 50);
            btnUnAssign.Size = btnAssign.Size;
            btnFlip.Height = btnAssign.Height + btnAssign.Height / 2;
            btnFlip.Width = btnAssign.Width;

            int gridWidth = (this.Width - btnAssign.Width);
            int gridHeight = this.Height / 2;

            navAvailable.Top = 0;
            navAvailable.Left = 0;
            navAvailable.Width = gridWidth;
            navAvailable.Height = gridHeight;

            navAssigned.Top = navAvailable.Top + navAvailable.Height; ;
            navAssigned.Left = navAvailable.Left; ;
            navAssigned.Width = gridWidth;
            navAssigned.Height = gridHeight;

            btnAssign.Top = navAssigned.Top - btnAssign.Height;
            btnAssign.Left = gridWidth;

            btnUnAssign.Top = navAssigned.Top + navAssigned.datagrid.Top;
            btnUnAssign.Left = gridWidth;

            btnAssign.Text = downArrow;
            btnUnAssign.Text = upArrow;

            btnFlip.Top = navAssigned.Top + navAssigned.Height - btnFlip.Height;
            btnFlip.Left = btnAssign.Left;
            btnFlip.Text = "F\nl\ni\np";
            btnFlip.TextAlign = ContentAlignment.MiddleCenter;

            isOverUnderOrientation = true;
        }

        // make the layout of the grids LeftAndRight
        private void OrientToLeftAndRight()
        {
            btnAssign.Size = new System.Drawing.Size(50, 23);
            btnUnAssign.Size = btnAssign.Size;
            btnFlip.Size = btnAssign.Size;

            int gridWidth = (this.Width - btnAssign.Width) / 2;
            int gridHeight = this.Height;

            navAvailable.Top = 0;
            navAvailable.Left = 0;
            navAvailable.Width = gridWidth;
            navAvailable.Height = gridHeight;

            navAssigned.Top = navAvailable.Top;
            navAssigned.Left = navAvailable.Width + btnAssign.Width;
            navAssigned.Width = gridWidth;
            navAssigned.Height = gridHeight;

            btnAssign.Top = 50;
            btnAssign.Left = gridWidth;

            btnUnAssign.Top = 80;
            btnUnAssign.Left = gridWidth;

            btnAssign.Text = rightArrow;
            btnUnAssign.Text = leftArrow;

            btnFlip.Top = navAvailable.Top + navAvailable.Height - btnFlip.Height;
            btnFlip.Left = btnAssign.Left;
            btnFlip.Text = "Flip";
            btnFlip.TextAlign = ContentAlignment.MiddleCenter;

            isOverUnderOrientation = false;
        }

        // set up the control for display
        private void SetupControl(List<T> availableItems, List<T> assignedItems)
        {
            // ensure that lists are passed in
            if (assignedItems == null)
                throw new Exception("List of assignedItems cannot be null");

            if (availableItems == null)
                throw new Exception("List of availableItems cannot be null");

            // ensure that there are no duplicates
            List<int> idList = new List<int>(availableItems.Count + assignedItems.Count);

            foreach (IObjectId idObject in availableItems)
            {
                if (idList.Contains<int>(idObject.Id))
                    throw new Exception("Duplicate ID detected");
                idList.Add(idObject.Id);
            }

            foreach (IObjectId idObject in assignedItems)
            {
                if (idList.Contains<int>(idObject.Id))
                    throw new Exception("Duplicate ID detected");
                idList.Add(idObject.Id);
            }

            // put items into to respective controls
            navAvailable.DisableSettingPersistence = this.DisableSettingPersistence;
            navAvailable.GridLabel = _availableItemsLabel;
            _availableItemsTable = new HaiDataTable<T>(availableItems);
            navAvailable.ShowDataTable(_availableItemsTable);
            navAvailable.MultiSelect = true;

            navAssigned.DisableSettingPersistence = this.DisableSettingPersistence;
            navAssigned.GridLabel = _assignedItemsLabel;
            _assignedItemsTable = new HaiDataTable<T>(assignedItems);
            navAssigned.ShowDataTable(_assignedItemsTable);
            navAssigned.MultiSelect = true;

            // set the tootips
            tt.SetToolTip(btnAssign, "Assign selected available items");
            tt.SetToolTip(btnUnAssign, "Un-assign selected assigned items");
            tt.SetToolTip(btnFlip, "Change grid layout");
        }
    #endregion
        
    #region Event handlers
		private void btnAssign_Click(object sender, EventArgs e)
        {
            if (navAvailable.GetSelectedItems().Count == 0)
            {
                MessageBox.Show("Nothing selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            List<IObjectId> items = navAvailable.GetSelectedItems();
            foreach (IObjectId item in items)
            {
                _availableItemsTable.DeleteRow(item);
            }

            foreach (T item in items)
            {
                _assignedItemsTable.AddRow(item);
            }

            if (_availableItemsTable.Rows.Count == 0)
            {
                btnAssign.Enabled = false;
            }

            if (_assignedItemsTable.Rows.Count > 0)
            {
                btnUnAssign.Enabled = true;
            }
        }

        private void btnUnAssign_Click(object sender, EventArgs e)
        {
            if (navAssigned.GetSelectedItems().Count == 0)
            {
                MessageBox.Show("Nothing selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            List<IObjectId> items = navAssigned.GetSelectedItems();
            foreach (IObjectId item in items)
            {
                _assignedItemsTable.DeleteRow(item);
            }

            foreach (T item in items)
            {
                _availableItemsTable.AddRow(item);
            }

            if (_assignedItemsTable.Rows.Count == 0)
            {
                btnUnAssign.Enabled = false;
            }

            if (_availableItemsTable.Rows.Count > 0)
            {
                btnAssign.Enabled = true;
            }
        }

        private void btnFlip_Click(object sender, EventArgs e)
        {
            ToggleOrientation();
        }

        private void navAvailable_RowDoubleClick(NavigationGrid theGrid, RowDoubleClickEventArgs args)
        {
            btnAssign_Click(null, null);
        }

        private void navAssigned_RowDoubleClick(NavigationGrid theGrid, RowDoubleClickEventArgs args)
        {
            btnUnAssign_Click(null, null);
        }
    #endregion    
    }
}
