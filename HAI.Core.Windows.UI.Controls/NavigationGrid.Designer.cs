﻿namespace HAI.Core.Windows.UI.Controls
{
    partial class NavigationGrid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mnuGridMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.applyEqualFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAdvancedFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreOldFilterSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.removeThisFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dropAllFiltersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.viewCurrentFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewPropertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.findInThisColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.sortThisColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unsortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreOldSortSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparatorColumnItems = new System.Windows.Forms.ToolStripSeparator();
            this.columnHeadingStyleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.standardNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alternateNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.columnWidthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultColumnOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datagrid = new System.Windows.Forms.DataGridView();
            this.lblTitleLine = new System.Windows.Forms.Label();
            this.mnuGridMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuGridMenu
            // 
            this.mnuGridMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applyEqualFilterToolStripMenuItem,
            this.setAdvancedFilterToolStripMenuItem,
            this.restoreOldFilterSetToolStripMenuItem,
            this.toolStripSeparator2,
            this.removeThisFilterToolStripMenuItem,
            this.dropAllFiltersToolStripMenuItem,
            this.toolStripSeparator4,
            this.viewCurrentFilterToolStripMenuItem,
            this.viewPropertiesToolStripMenuItem,
            this.toolStripSeparator3,
            this.findInThisColumnToolStripMenuItem,
            this.toolStripSeparator1,
            this.sortThisColumnToolStripMenuItem,
            this.unsortToolStripMenuItem,
            this.restoreOldSortSetToolStripMenuItem,
            this.toolStripSeparatorColumnItems,
            this.columnHeadingStyleToolStripMenuItem,
            this.columnWidthToolStripMenuItem,
            this.defaultColumnOrderToolStripMenuItem,
            this.toolStripSeparator5,
            this.printToolStripMenuItem});
            this.mnuGridMenu.Name = "mnuGridMenu";
            this.mnuGridMenu.ShowImageMargin = false;
            this.mnuGridMenu.Size = new System.Drawing.Size(186, 392);
            // 
            // applyEqualFilterToolStripMenuItem
            // 
            this.applyEqualFilterToolStripMenuItem.Name = "applyEqualFilterToolStripMenuItem";
            this.applyEqualFilterToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.applyEqualFilterToolStripMenuItem.Text = "Fil&ter by selection";
            this.applyEqualFilterToolStripMenuItem.Click += new System.EventHandler(this.addToFilterCriteriaToolStripMenuItem_Click);
            // 
            // setAdvancedFilterToolStripMenuItem
            // 
            this.setAdvancedFilterToolStripMenuItem.Name = "setAdvancedFilterToolStripMenuItem";
            this.setAdvancedFilterToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.setAdvancedFilterToolStripMenuItem.Text = "Set &advanced filter...";
            this.setAdvancedFilterToolStripMenuItem.Click += new System.EventHandler(this.setAdvancedFilterToolStripMenuItem_Click);
            // 
            // restoreOldFilterSetToolStripMenuItem
            // 
            this.restoreOldFilterSetToolStripMenuItem.Name = "restoreOldFilterSetToolStripMenuItem";
            this.restoreOldFilterSetToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.restoreOldFilterSetToolStripMenuItem.Text = "Restore old filter set";
            this.restoreOldFilterSetToolStripMenuItem.Click += new System.EventHandler(this.restoreOldFilterSetToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(182, 6);
            // 
            // removeThisFilterToolStripMenuItem
            // 
            this.removeThisFilterToolStripMenuItem.Name = "removeThisFilterToolStripMenuItem";
            this.removeThisFilterToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.removeThisFilterToolStripMenuItem.Text = "&Remove this column filter";
            this.removeThisFilterToolStripMenuItem.Click += new System.EventHandler(this.removeThisFiltersToolStripMenuItem_Click);
            // 
            // dropAllFiltersToolStripMenuItem
            // 
            this.dropAllFiltersToolStripMenuItem.Name = "dropAllFiltersToolStripMenuItem";
            this.dropAllFiltersToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.dropAllFiltersToolStripMenuItem.Text = "&Drop all filters";
            this.dropAllFiltersToolStripMenuItem.Click += new System.EventHandler(this.removeAllFiltersToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(182, 6);
            // 
            // viewCurrentFilterToolStripMenuItem
            // 
            this.viewCurrentFilterToolStripMenuItem.Name = "viewCurrentFilterToolStripMenuItem";
            this.viewCurrentFilterToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.viewCurrentFilterToolStripMenuItem.Text = "&View current filters";
            this.viewCurrentFilterToolStripMenuItem.Click += new System.EventHandler(this.viewCurrentFilterToolStripMenuItem_Click);
            // 
            // viewPropertiesToolStripMenuItem
            // 
            this.viewPropertiesToolStripMenuItem.Name = "viewPropertiesToolStripMenuItem";
            this.viewPropertiesToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.viewPropertiesToolStripMenuItem.Text = "View properties";
            this.viewPropertiesToolStripMenuItem.Click += new System.EventHandler(this.viewPropertiesToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(182, 6);
            // 
            // findInThisColumnToolStripMenuItem
            // 
            this.findInThisColumnToolStripMenuItem.Name = "findInThisColumnToolStripMenuItem";
            this.findInThisColumnToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.findInThisColumnToolStripMenuItem.Text = "&Find in this column...";
            this.findInThisColumnToolStripMenuItem.Click += new System.EventHandler(this.findInThisColumnToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(182, 6);
            // 
            // sortThisColumnToolStripMenuItem
            // 
            this.sortThisColumnToolStripMenuItem.Name = "sortThisColumnToolStripMenuItem";
            this.sortThisColumnToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.sortThisColumnToolStripMenuItem.Text = "&Sort this column";
            this.sortThisColumnToolStripMenuItem.Click += new System.EventHandler(this.sortThisColumnToolStripMenuItem_Click);
            // 
            // unsortToolStripMenuItem
            // 
            this.unsortToolStripMenuItem.Name = "unsortToolStripMenuItem";
            this.unsortToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.unsortToolStripMenuItem.Text = "&Unsort";
            this.unsortToolStripMenuItem.Click += new System.EventHandler(this.unsortToolStripMenuItem_Click);
            // 
            // restoreOldSortSetToolStripMenuItem
            // 
            this.restoreOldSortSetToolStripMenuItem.Name = "restoreOldSortSetToolStripMenuItem";
            this.restoreOldSortSetToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.restoreOldSortSetToolStripMenuItem.Text = "Restore old sort set";
            this.restoreOldSortSetToolStripMenuItem.Click += new System.EventHandler(this.restoreOldSortSetToolStripMenuItem_Click);
            // 
            // toolStripSeparatorColumnItems
            // 
            this.toolStripSeparatorColumnItems.Name = "toolStripSeparatorColumnItems";
            this.toolStripSeparatorColumnItems.Size = new System.Drawing.Size(182, 6);
            // 
            // columnHeadingStyleToolStripMenuItem
            // 
            this.columnHeadingStyleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.standardNameToolStripMenuItem,
            this.alternateNameToolStripMenuItem});
            this.columnHeadingStyleToolStripMenuItem.Name = "columnHeadingStyleToolStripMenuItem";
            this.columnHeadingStyleToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.columnHeadingStyleToolStripMenuItem.Text = "Column &heading style";
            // 
            // standardNameToolStripMenuItem
            // 
            this.standardNameToolStripMenuItem.Name = "standardNameToolStripMenuItem";
            this.standardNameToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.standardNameToolStripMenuItem.Text = "Standard name";
            this.standardNameToolStripMenuItem.Click += new System.EventHandler(this.standardNameToolStripMenuItem_Click);
            // 
            // alternateNameToolStripMenuItem
            // 
            this.alternateNameToolStripMenuItem.Name = "alternateNameToolStripMenuItem";
            this.alternateNameToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.alternateNameToolStripMenuItem.Text = "Alternate name";
            this.alternateNameToolStripMenuItem.Click += new System.EventHandler(this.alternateNameToolStripMenuItem_Click);
            // 
            // columnWidthToolStripMenuItem
            // 
            this.columnWidthToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoSizeToolStripMenuItem,
            this.defaultToolStripMenuItem,
            this.manualToolStripMenuItem});
            this.columnWidthToolStripMenuItem.Name = "columnWidthToolStripMenuItem";
            this.columnWidthToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.columnWidthToolStripMenuItem.Text = "Column &width";
            // 
            // autoSizeToolStripMenuItem
            // 
            this.autoSizeToolStripMenuItem.Name = "autoSizeToolStripMenuItem";
            this.autoSizeToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.autoSizeToolStripMenuItem.Text = "&Auto size";
            this.autoSizeToolStripMenuItem.Click += new System.EventHandler(this.autoSizeToolStripMenuItem_Click);
            // 
            // defaultToolStripMenuItem
            // 
            this.defaultToolStripMenuItem.Name = "defaultToolStripMenuItem";
            this.defaultToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.defaultToolStripMenuItem.Text = "&Default";
            this.defaultToolStripMenuItem.Click += new System.EventHandler(this.defaultToolStripMenuItem_Click);
            // 
            // manualToolStripMenuItem
            // 
            this.manualToolStripMenuItem.Name = "manualToolStripMenuItem";
            this.manualToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.manualToolStripMenuItem.Text = "Manual";
            this.manualToolStripMenuItem.Click += new System.EventHandler(this.manualToolStripMenuItem_Click);
            // 
            // defaultColumnOrderToolStripMenuItem
            // 
            this.defaultColumnOrderToolStripMenuItem.Name = "defaultColumnOrderToolStripMenuItem";
            this.defaultColumnOrderToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.defaultColumnOrderToolStripMenuItem.Text = "Default column order";
            this.defaultColumnOrderToolStripMenuItem.Click += new System.EventHandler(this.defaultColumnOrderToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(182, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.printToolStripMenuItem.Text = "Export to Excel";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // datagrid
            // 
            this.datagrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.datagrid.BackgroundColor = System.Drawing.SystemColors.ControlDark;
            this.datagrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagrid.Location = new System.Drawing.Point(7, 19);
            this.datagrid.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.datagrid.Name = "datagrid";
            this.datagrid.Size = new System.Drawing.Size(705, 349);
            this.datagrid.TabIndex = 2;
            this.datagrid.ColumnDisplayIndexChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.datagrid_ColumnDisplayIndexChanged);
            // 
            // lblTitleLine
            // 
            this.lblTitleLine.AutoSize = true;
            this.lblTitleLine.Location = new System.Drawing.Point(11, 2);
            this.lblTitleLine.Name = "lblTitleLine";
            this.lblTitleLine.Size = new System.Drawing.Size(86, 14);
            this.lblTitleLine.TabIndex = 3;
            this.lblTitleLine.Text = "label for title";
            // 
            // NavigationGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContextMenuStrip = this.mnuGridMenu;
            this.Controls.Add(this.lblTitleLine);
            this.Controls.Add(this.datagrid);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "NavigationGrid";
            this.Size = new System.Drawing.Size(720, 376);
            this.Load += new System.EventHandler(this.NavigationGrid_Load);
            this.mnuGridMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.datagrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip mnuGridMenu;
        private System.Windows.Forms.ToolStripMenuItem applyEqualFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setAdvancedFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem removeThisFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dropAllFiltersToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem viewCurrentFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem findInThisColumnToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem sortThisColumnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unsortToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparatorColumnItems;
        private System.Windows.Forms.ToolStripMenuItem columnHeadingStyleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem columnWidthToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoSizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem defaultToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem standardNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alternateNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem defaultColumnOrderToolStripMenuItem;
        private System.Windows.Forms.Label lblTitleLine;
        private System.Windows.Forms.ToolStripMenuItem viewPropertiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreOldFilterSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreOldSortSetToolStripMenuItem;
        internal System.Windows.Forms.DataGridView datagrid;
    }
}
