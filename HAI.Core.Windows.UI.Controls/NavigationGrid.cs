﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using System.Configuration;

namespace HAI.Core.Windows.UI.Controls
{
    public partial class NavigationGrid : UserControl
    {
        #region Class Variables
        private DataTable _dataTable = null;
        private DataGridViewCell _rightClickedCell = null;
        private DataGridView.HitTestInfo _anyClickHitTestInfo = null;
        private DataGridViewRow _currentlySelectedRow = null;

        private Dictionary<string, ListSortDirection> _sortDescriptions = new Dictionary<string, ListSortDirection>();
        private Dictionary<string, List<Filter>> _gridFilters = new Dictionary<string, List<Filter>>();
        private string _serializedGridFiltersCache = null;
        private string _serializedSortDescriptionsCache = null;

        private bool _ctrlKeyIsDown = false;
        private bool _headerClicked = false;
        private bool _dataTableIsSorted = false;
        private bool _dataTableIsFiltered = false;
        private DataGridViewAutoSizeColumnsMode _gridColumnResizeMode = DataGridViewAutoSizeColumnsMode.AllCells;
        private ColumnHeadingStyle _columnHeadingStyle = ColumnHeadingStyle.Standard;

        private Color _rowBackColor;
        private Color _rowForeColor;
        private Color _headerBackColor;
        private Color _headerForeColor;

        private DGVSettings _savedDgvSettings = null;
        private string _qualifiedControlName = string.Empty;

        #endregion

        #region Constructors
        public NavigationGrid()
        {
            InitializeComponent();

            this.BorderStyle = BorderStyle.FixedSingle; // called by the designer to set the border style
        }

        public NavigationGrid(DataTable dataTable)
        {
            InitializeComponent();

            _dataTable = dataTable;
            PrepareGridForDisplay();
        }
        #endregion

        #region Form and DataGridView event handlers
        private void NavigationGrid_Load(object sender, EventArgs e)
        {
            datagrid.Focus();       // using keys requires that the grid has the focus.

            // assign event handler for context menu so cell that was right-clicked is the current cell
            datagrid.CellContextMenuStripNeeded += new DataGridViewCellContextMenuStripNeededEventHandler(datagrid_CellContextMenuStripNeeded);

            // assign event handlers for keyboard entry
            datagrid.KeyPress += new KeyPressEventHandler(datagrid_KeyPress);
            datagrid.KeyDown += new KeyEventHandler(datagrid_KeyDown);
            datagrid.KeyUp += new KeyEventHandler(datagrid_KeyUp);

            this.datagrid.ColumnHeaderMouseClick += new DataGridViewCellMouseEventHandler(datagrid_ColumnHeaderMouseClick);
            this.mnuGridMenu.Opening += new System.ComponentModel.CancelEventHandler(this.mnuGridMenu_Opening);
            this.datagrid.MouseDown += new MouseEventHandler(datagrid_MouseDown);

            datagrid.BorderStyle = BorderStyle.FixedSingle;
            this.BorderStyle = BorderStyle.None;

            //set up DataGridView event handlers
            datagrid.RowEnter += new DataGridViewCellEventHandler(datagrid_RowEnter);
            datagrid.CellDoubleClick += new DataGridViewCellEventHandler(datagrid_CellDoubleClick);

            _rowBackColor = this.DataGridView.RowsDefaultCellStyle.BackColor;
            _rowForeColor = this.DataGridView.RowsDefaultCellStyle.ForeColor;
            _headerBackColor = this.DataGridView.ColumnHeadersDefaultCellStyle.BackColor;
            _headerForeColor = this.DataGridView.ColumnHeadersDefaultCellStyle.ForeColor;

            // create event handlers to trigger persisting the grid settings
            this.Parent.ParentChanged += new EventHandler(NavigationGrid_ParentChanged);    // the user control is going away
            SetQualifiedControlName();
        }


        void NavigationGrid_FormClosing(object sender, FormClosingEventArgs e)
        {
            PersistDataGridViewSettings();
        }

        void NavigationGrid_ParentChanged(object sender, EventArgs e)
        {
            PersistDataGridViewSettings();
        }

        // record (right-button) mouse down event for later use
        // ... to determine what part of the grid was clicked
        void datagrid_MouseDown(object sender, MouseEventArgs e)
        {
            _anyClickHitTestInfo = datagrid.HitTest(e.X, e.Y);

            if (e.Button == MouseButtons.Right)
            {
                if (_anyClickHitTestInfo.Type == DataGridViewHitTestType.Cell)
                {
                    _rightClickedCell = datagrid.Rows[_anyClickHitTestInfo.RowIndex].Cells[_anyClickHitTestInfo.ColumnIndex];
                }
                else
                {
                    _rightClickedCell = null;
                }
            }

            if (_anyClickHitTestInfo.Type== DataGridViewHitTestType.ColumnHeader)
            {
                _headerClicked = true;
            }
            else
            {
                _headerClicked = false;
            }

        }

        void datagrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((RowDoubleClick != null) && (e.RowIndex >= 0))
            {
                DataGridViewRow theRow = datagrid.Rows[e.RowIndex];
                RowDoubleClickEventArgs args = new RowDoubleClickEventArgs();
                args.Item = (HAI.Core.IObjectId)theRow.Cells[0].Value;
                RowDoubleClick(this, args);
            }
        }

        void datagrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (_headerClicked)     // a data row was not clicked
            {
                return;     
            }

            DataGridViewRow theRow = datagrid.Rows[e.RowIndex];
            _currentlySelectedRow = theRow;

            if (SelectedRowChanged != null)
            {
                if (theRow != null)
                {
                    SelectedRowChangedEventArgs args = new SelectedRowChangedEventArgs();
                    object theValue = theRow.Cells[0].Value;
                    if (theValue != System.DBNull.Value)
                    {
                        args.Item = (HAI.Core.IObjectId)theValue;
                    }
                    SelectedRowChanged(this, args);
                }
            }
        }

        // columns were re-ordered, invalidate the HitTestInfo b/c column number may be incorrect
        private void datagrid_ColumnDisplayIndexChanged(object sender, DataGridViewColumnEventArgs e)
        {
            _anyClickHitTestInfo = null;
        }
        #endregion

        #region Mouse event handlers
        // sort a column when header is clicked
        void datagrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // do sort only if Left mouse button is pressed
            if ((e != null) && (e.Button != MouseButtons.Left))
            {
                return;
            }

            DataGridViewColumnHeaderCell header;
            if (datagrid.Rows.Count == 0)       // nothing to sort
                return;

            int columnIndex = _anyClickHitTestInfo.ColumnIndex;

            if (e == null)      // (e == null) if not called by event; use current cell to get column index
            {
                header = datagrid.Columns[columnIndex].HeaderCell;
            }
            else                // otherwise use column index from click event
            {
                header = datagrid.Columns[e.ColumnIndex].HeaderCell;
            }
            string columnName = datagrid.Columns[header.ColumnIndex].Name;

            bool clickedColumnIsSorted;
            if (_sortDescriptions.ContainsKey(columnName))
            {
                clickedColumnIsSorted = true;
            }
            else
                clickedColumnIsSorted = false;

            ListSortDirection sortDirection;
            if (_ctrlKeyIsDown)
            {
                if (clickedColumnIsSorted)
                {
                    sortDirection = _sortDescriptions[columnName];
                    if (sortDirection == ListSortDirection.Ascending)
                        _sortDescriptions[columnName] = ListSortDirection.Descending;
                    else
                        _sortDescriptions[columnName] = ListSortDirection.Ascending;
                }
                else
                {
                    AddSortDescription(columnName, ListSortDirection.Ascending);
                }
            }
            else
            {
                if (clickedColumnIsSorted)
                {
                    sortDirection = _sortDescriptions[columnName];
                    if (sortDirection == ListSortDirection.Ascending)
                        sortDirection = ListSortDirection.Descending;
                    else
                        sortDirection = ListSortDirection.Ascending;
                }
                else
                {
                    sortDirection = ListSortDirection.Ascending;
                }
                _sortDescriptions.Clear();
                AddSortDescription(columnName, sortDirection);
            }

            SortColumns();
        }
        #endregion

        #region Menu event handlers

        // enable the content of the menu and set its location
        private void mnuGridMenu_Opening(object sender, CancelEventArgs e)
        {
            switch (_columnHeadingStyle)
            {
                case ColumnHeadingStyle.Standard:
                    standardNameToolStripMenuItem.Checked = true;
                    alternateNameToolStripMenuItem.Checked = false;
                    break;
                case ColumnHeadingStyle.FullText:
                    standardNameToolStripMenuItem.Checked = false;
                    alternateNameToolStripMenuItem.Checked = true;
                    break;
                default:
                    throw new Exception("Unknown ColumnHeadingStyle" + _columnHeadingStyle.ToString());
            }

            switch (_gridColumnResizeMode)
            {
                case DataGridViewAutoSizeColumnsMode.AllCells:
                    autoSizeToolStripMenuItem.Checked = true;
                    defaultToolStripMenuItem.Checked = false;
                    manualToolStripMenuItem.Checked = false;
                    break;
                case DataGridViewAutoSizeColumnsMode.ColumnHeader:
                    autoSizeToolStripMenuItem.Checked = false;
                    defaultToolStripMenuItem.Checked = true;
                    manualToolStripMenuItem.Checked = false;
                    break;
                case DataGridViewAutoSizeColumnsMode.None:
                    autoSizeToolStripMenuItem.Checked = false;
                    defaultToolStripMenuItem.Checked = false;
                    manualToolStripMenuItem.Checked = true;
                    break;
                default:
                    throw new Exception("Unexpected DataGridViewAutoSizeColumnsMode: " + _gridColumnResizeMode.ToString());
            }

            if (string.IsNullOrEmpty(_serializedSortDescriptionsCache))
                restoreOldSortSetToolStripMenuItem.Enabled = false;
            else
                restoreOldSortSetToolStripMenuItem.Enabled = true;

            if (string.IsNullOrEmpty(_serializedGridFiltersCache))
                restoreOldFilterSetToolStripMenuItem.Enabled = false;
            else
                restoreOldFilterSetToolStripMenuItem.Enabled = true;

            // enable/disable certain menu items according to conditions

            switch (_anyClickHitTestInfo.Type)
            {
                case DataGridViewHitTestType.Cell:
                    unsortToolStripMenuItem.Enabled = _dataTableIsSorted;
                    applyEqualFilterToolStripMenuItem.Enabled = true;
                    setAdvancedFilterToolStripMenuItem.Enabled = true;
                    removeThisFilterToolStripMenuItem.Enabled = true;
                    findInThisColumnToolStripMenuItem.Enabled = true;
                    sortThisColumnToolStripMenuItem.Enabled = true;

                    if (_dataTableIsFiltered)
                    {
                        DataGridViewColumn column = datagrid.Columns[_rightClickedCell.ColumnIndex];
                        DataGridViewColumnHeaderCell header = column.HeaderCell;
                        if ((header.Style.Font == null) || !header.Style.Font.Italic)
                        {
                            removeThisFilterToolStripMenuItem.Enabled = false;
                        }
                        else
                        {
                            removeThisFilterToolStripMenuItem.Enabled = true;
                        }
                        dropAllFiltersToolStripMenuItem.Enabled = true;
                        viewCurrentFilterToolStripMenuItem.Enabled = true;
                    }
                    else
                    {
                        removeThisFilterToolStripMenuItem.Enabled = false;
                        dropAllFiltersToolStripMenuItem.Enabled = false;
                        viewCurrentFilterToolStripMenuItem.Enabled = false;
                    }
                    break;
                case DataGridViewHitTestType.ColumnHeader:
                    unsortToolStripMenuItem.Enabled = _dataTableIsSorted;
                    applyEqualFilterToolStripMenuItem.Enabled = false;
                    setAdvancedFilterToolStripMenuItem.Enabled = false;
                    removeThisFilterToolStripMenuItem.Enabled = true;
                    findInThisColumnToolStripMenuItem.Enabled = true;
                    sortThisColumnToolStripMenuItem.Enabled = true;

                    if (_dataTableIsFiltered)
                    {
                        DataGridViewColumn column = datagrid.Columns[_anyClickHitTestInfo.ColumnIndex];
                        DataGridViewColumnHeaderCell header = column.HeaderCell;
                        if ((header.Style.Font == null) || !header.Style.Font.Italic)
                        {
                            removeThisFilterToolStripMenuItem.Enabled = false;
                        }
                        else
                        {
                            removeThisFilterToolStripMenuItem.Enabled = true;
                        }
                        dropAllFiltersToolStripMenuItem.Enabled = true;
                        viewCurrentFilterToolStripMenuItem.Enabled = true;
                    }
                    else
                    {
                        removeThisFilterToolStripMenuItem.Enabled = false;
                        dropAllFiltersToolStripMenuItem.Enabled = false;
                        viewCurrentFilterToolStripMenuItem.Enabled = false;
                    }
                    break;
                case DataGridViewHitTestType.HorizontalScrollBar:
                    e.Cancel = true;
                    break;
                case DataGridViewHitTestType.None:
                    applyEqualFilterToolStripMenuItem.Enabled = false;
                    setAdvancedFilterToolStripMenuItem.Enabled = false;
                    removeThisFilterToolStripMenuItem.Enabled = false;
                    findInThisColumnToolStripMenuItem.Enabled = false;
                    sortThisColumnToolStripMenuItem.Enabled = false;
                    unsortToolStripMenuItem.Enabled = false;
                    dropAllFiltersToolStripMenuItem.Enabled = true;
                    viewCurrentFilterToolStripMenuItem.Enabled = true;
                    break;
                case DataGridViewHitTestType.RowHeader:
                    e.Cancel = true;
                    break;
                case DataGridViewHitTestType.TopLeftHeader:
                    e.Cancel = true;
                    break;
                case DataGridViewHitTestType.VerticalScrollBar:
                    e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        // make the cell that was right-clicked the current cell
        void datagrid_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            if (_anyClickHitTestInfo.Type == DataGridViewHitTestType.Cell)
            {
                datagrid.CurrentCell = _rightClickedCell;
            }
        }

        // remove sorting on the data
        private void unsortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int columnIndex = _anyClickHitTestInfo.ColumnIndex;
            UnSortColumns();
            datagrid.CurrentCell = datagrid[columnIndex, 0];
        }

        // handle menu request for column sorting
        private void sortThisColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            datagrid_ColumnHeaderMouseClick(null, null);
        }

        // find an item in a column by full text match
        private void findInThisColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_anyClickHitTestInfo == null)
                return;

            if (datagrid.Rows.Count > 100000)
            {
                DialogResult yesno = MessageBox.Show("Grid contains a large amount of data.\r\nFind could be time-consuming.\r\nConsider filtering rows before invoking find.\r\n\r\nContinue anyway?", 
                    "Confirm action", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (yesno == DialogResult.No)
                    return;
            }

            string cellText = string.Empty;
            int rowNumber = 0;
            int columnIndex = _anyClickHitTestInfo.ColumnIndex;

            // create list of unique text to find
            Dictionary<string, int> uniqueRows = new Dictionary<string, int>(datagrid.Rows.Count, StringComparer.CurrentCultureIgnoreCase);
            Cursor = Cursors.WaitCursor;
            foreach (DataGridViewRow row in datagrid.Rows)
            {
                cellText = row.Cells[columnIndex].Value.ToString();
                if (!uniqueRows.ContainsKey(cellText))
                {
                    uniqueRows.Add(cellText, rowNumber);
                }
                rowNumber++;
            }
            Cursor = Cursors.Default;
            List<string> uniqueTextList = uniqueRows.Keys.ToList<string>();

            // open the lookup form with keys to be used as autocomplete values
            SelectTextToFind form = new SelectTextToFind(uniqueTextList);
            form.StartPosition = FormStartPosition.CenterParent;

            form.ShowDialog();

            if (form.DialogResult == DialogResult.OK)
            {
                // create a dictionary with keys in uppercase so that lookup will succeed even if user changed the case
                Dictionary<string, int> uniqueRowsUpperKeys = new Dictionary<string, int>(uniqueRows.Count);
                foreach (string key in uniqueRows.Keys)
                {
                    uniqueRowsUpperKeys.Add(key.ToUpper(), uniqueRows[key]);
                }

                // find lookup text and get the index of the corresponding row
                string findText = form.SelectedText.ToUpper();
                int rowIndex = uniqueRowsUpperKeys[findText];

                // set the current cell to the found row index and the current column
                datagrid.CurrentCell = datagrid.Rows[rowIndex].Cells[columnIndex];
                if (SelectedRowChanged != null)
                {
                    SelectedRowChangedEventArgs args = new SelectedRowChangedEventArgs();
                    args.Item = (HAI.Core.IObjectId)datagrid.Rows[rowIndex].Cells[0].Value;
                    SelectedRowChanged(this, args);
                }

                // make the current row the top displayed row
                datagrid.FirstDisplayedScrollingRowIndex = datagrid.CurrentCell.RowIndex;
                _currentlySelectedRow = datagrid.Rows[datagrid.CurrentCell.RowIndex];
            }

            form.Dispose();
        }

        // set columns width to automatic
        private void autoSizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (datagrid.Rows.Count > 10000)
            {
                DialogResult yesno = MessageBox.Show("Grid contains a large amount of data.\r\nAuto-size could be time-consuming.\r\n\r\nContinue anyway?",
                    "Confirm action", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (yesno == DialogResult.No)
                    return;
            }
            _gridColumnResizeMode = DataGridViewAutoSizeColumnsMode.AllCells;
            SetColumnWidths();
        }

        // set columns width to default
        private void defaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _gridColumnResizeMode = DataGridViewAutoSizeColumnsMode.ColumnHeader;
            SetColumnWidths();
        }

        // manually set the column widths
        private void manualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _gridColumnResizeMode = DataGridViewAutoSizeColumnsMode.None;
            SetColumnWidths();
        }

        // set column heading text to the standard name
        private void standardNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewColumn aColumn in datagrid.Columns)
            {
                string columnName = aColumn.Name;
                string headerText = columnName;
                aColumn.HeaderText = headerText;
                aColumn.ToolTipText = _dataTable.Columns[aColumn.Name].Caption;
            }

            _columnHeadingStyle = ColumnHeadingStyle.Standard;
            SetColumnWidths();
        }

        // set column heading text to the alternate name
        private void alternateNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewColumn aColumn in datagrid.Columns)
            {
                string columnName = aColumn.Name;
                string headerText = _dataTable.Columns[columnName].Caption;
                aColumn.HeaderText = headerText;
                aColumn.ToolTipText = columnName;
            }

            _columnHeadingStyle = ColumnHeadingStyle.FullText;
            SetColumnWidths();
        }

        // show the currently applied filters
        private void viewCurrentFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filterPhrase = BuildFilterPhrase();
            using (ShowCurrentFiltersForm filterSummary = new ShowCurrentFiltersForm(_gridFilters, filterPhrase))
            {
                filterSummary.ShowDialog();
            }
        }

        // show the grid properties
        private void viewPropertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (NavigationGridProperties theForm = new NavigationGridProperties(this))
            {
                theForm.ShowDialog();
            }
        }

        // handle request for "Set advanced filter..."
        private void setAdvancedFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int columnIndex = datagrid.CurrentCell.ColumnIndex;

            string matchValue = datagrid.CurrentCell.Value.ToString();
            string propertyName = datagrid.Columns[datagrid.CurrentCell.ColumnIndex].Name;
            Filter filterItem = new Filter(propertyName, MatchType.BeginsWith, matchValue);
            object value = datagrid.CurrentCell.Value;
            filterItem.DataTypeCode = Type.GetTypeCode(value.GetType());

            using (SetAdvancedFilterForm advancedFilterForm = new SetAdvancedFilterForm(datagrid, filterItem))
            {
                advancedFilterForm.ShowDialog();

                if (advancedFilterForm.DialogResult == DialogResult.OK)
                {
                    UpdateFilterFilterList(filterItem, propertyName);
                }
            }

            FilterRows();
        }

        // handle request for "Filter by selection"
        private void addToFilterCriteriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int columnIndex = datagrid.CurrentCell.ColumnIndex;
            string matchValue;

            if (datagrid.CurrentCell.Value == null)
                matchValue = null;
            else
                matchValue = datagrid.CurrentCell.Value.ToString();

            string propertyName = datagrid.Columns[datagrid.CurrentCell.ColumnIndex].Name;
            object value = datagrid.CurrentCell.Value;
            Filter filterItem = new Filter(propertyName, MatchType.EqualTo, matchValue);

            if (value == null)
                filterItem.DataTypeCode = TypeCode.Empty;
            else
                filterItem.DataTypeCode = Type.GetTypeCode(value.GetType());

            UpdateFilterFilterList(filterItem, propertyName);
            FilterRows();
            datagrid.CurrentCell = datagrid[columnIndex, 0];
        }

        // remove all filters
        private void removeAllFiltersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _gridFilters.Clear();

            HAI.Core.IObjectId currentItem = GetCurrentItem();
            FilterRows();

            if (currentItem != null)
            {
                int columnIndex = datagrid.CurrentCell.ColumnIndex;

                foreach (DataGridViewRow aRow in datagrid.Rows)
                {
                    HAI.Core.IObjectId anyBaseItem = (HAI.Core.IObjectId)(aRow.Cells[0].Value);
                    if (currentItem.Id == anyBaseItem.Id)
                    {
                        datagrid.CurrentCell = aRow.Cells[columnIndex];
                        break;
                    }
                }
            }
        }

        // remove filters for selected column
        private void removeThisFiltersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int columnIndex = _anyClickHitTestInfo.ColumnIndex;
            string targetPropertyName = datagrid.Columns[columnIndex].Name;
            _gridFilters.Remove(targetPropertyName);         // remove the filter for this column from the group

            HAI.Core.IObjectId currentItem = GetCurrentItem();
            FilterRows();

            if (currentItem != null)
            {
                foreach (DataGridViewRow aRow in datagrid.Rows)
                {
                    HAI.Core.IObjectId anyBaseItem = (HAI.Core.IObjectId)(aRow.Cells[0].Value);
                    if (currentItem.Id == anyBaseItem.Id)
                    {
                        datagrid.CurrentCell = aRow.Cells[columnIndex];
                        break;
                    }
                }
            }
        }

        // export the contents of the grid to Excel
        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] titleLines = { this.GridLabel };
            ExportToExcel.ExportDataGridView(datagrid, titleLines);
        }

        // restore the column order as dictated by the object
        private void defaultColumnOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewColumn aColumn in datagrid.Columns)
            {
                aColumn.DisplayIndex = aColumn.Index;
            }
        }

        private void restoreOldFilterSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RestoreOldFilterSet();
        }

        private void restoreOldSortSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RestoreOldSortSet();
        }

        #endregion

        #region Keyboard event handlers

        void datagrid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 17)
            {
                _ctrlKeyIsDown = false;
            }
        }

        void datagrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 17)
            {
                _ctrlKeyIsDown = true;
            }
        }

        void datagrid_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ((char)6))
            {
                findInThisColumnToolStripMenuItem_Click(null, null);
                _ctrlKeyIsDown = false;              // needed because ctrlKeyIsDown was set True when ctrl-F was pressed
            }
        }

        #endregion

        #region Sorting
        private void SortColumns()
        {
            if (_sortDescriptions.Count == 0)
                return;

            string sortPhrase = string.Empty;
            foreach (string key_columnName in _sortDescriptions.Keys)
            {
                ListSortDirection sortDirection = _sortDescriptions[key_columnName];
                if (sortDirection== ListSortDirection.Ascending)
                    sortPhrase += key_columnName + " ASC, ";
                else
                    sortPhrase += key_columnName + " DESC, ";
            }

            sortPhrase = sortPhrase.Remove(sortPhrase.Length - 2);
            _dataTable.DefaultView.Sort = sortPhrase;
            SetColumnSortGlyphs();
            _dataTableIsSorted = true;
            datagrid.CurrentCell = null;
            if (SelectedRowChanged != null)
            {
                SelectedRowChangedEventArgs args = new SelectedRowChangedEventArgs();
                args.Item = null;
                SelectedRowChanged(this, args);
            }
        }

        private void UnSortColumns()
        {
            _sortDescriptions.Clear();
            _dataTable.DefaultView.Sort = "";
            SetColumnSortGlyphs();
            _dataTableIsSorted = false;
        }
        #endregion

        #region Filtering
        // Limit contents of the grid by applying filters to the dataTable
        private void FilterRows()
        {
            IObjectId beginItem = (IObjectId)GetCurrentItem();

            string filterPhrase = BuildFilterPhrase();
            _dataTable.DefaultView.RowFilter = filterPhrase;
            _dataTableIsFiltered = false;   // assume that no filters get applied

            // set column header fonts to indicate whether filtering is applied
            foreach (DataGridViewColumn aColumn in datagrid.Columns)
            {
                if (_gridFilters.ContainsKey(aColumn.Name))
                {
                    aColumn.HeaderCell.Style.Font = new Font(this.Font, FontStyle.Italic);
                    _dataTableIsFiltered = true;    // indicate whether table is filtered
                }
                else
                {
                    aColumn.HeaderCell.Style.Font = new Font(this.Font, FontStyle.Regular);
                }
            }

            IObjectId endItem = (IObjectId)GetCurrentItem();

            // if filtering changed the selected row, fire the SelectedRowChanged event.
            if ((endItem != beginItem) && (SelectedRowChanged != null))
            {
                SelectedRowChangedEventArgs args = new SelectedRowChangedEventArgs();
                args.Item = endItem;
                SelectedRowChanged(this, args);
            }
        }
        
        // Create the filtering string that is applied to the dataTable
        private string BuildFilterPhrase()
        {
            string filterPhrase = string.Empty;

            if (_gridFilters == null || _gridFilters.Count == 0)
                return filterPhrase;

            // build the filtering phrase
            foreach (string propertyName in _gridFilters.Keys)
            {
                List<Filter> filterList = _gridFilters[propertyName];
                DataColumn column = _dataTable.Columns[propertyName];

                string propertyPhrase = string.Empty;
                foreach (Filter aFilter in filterList)
                {
                    string valuePhrase = string.Empty;
                    foreach (string matchValue in aFilter.MatchValueList)
                    {
                        string sqlString = MatchAsSqlString(aFilter.MatchType, matchValue, aFilter.DataTypeCode);
                        if (sqlString!=string.Empty)
                            valuePhrase += "[" +aFilter.PropertyName + "]" + sqlString + " OR ";
                    }
                    valuePhrase = valuePhrase.Remove(valuePhrase.Length - 4);
                    valuePhrase = "(" + valuePhrase + ")";

                    propertyPhrase += valuePhrase + " AND ";
                }
                propertyPhrase = propertyPhrase.Remove(propertyPhrase.Length - 5);

                filterPhrase += propertyPhrase + " AND ";
            }
            filterPhrase = filterPhrase.Remove(filterPhrase.Length - 5);

            return filterPhrase;
        }

        // Convert match parameters into a SQL string
        private string MatchAsSqlString(MatchType matchType, string matchValue, TypeCode dataType)
        {
            switch (matchType)
            {
                case MatchType.Ignore:
                    return string.Empty;
                case MatchType.EqualTo:
                    return " = '" + matchValue + "' ";
                case MatchType.BeginsWith:
                    return " LIKE '" + matchValue + "%' ";
                case MatchType.EndsWith:
                    return " LIKE '%" + matchValue +"' ";
                case MatchType.Contains:
                    return " LIKE '%" + matchValue + "%' ";
                case MatchType.NotEqual:
                    return " <> '" + matchValue + "' ";
                case MatchType.NotContains:
                    return " NOT LIKE '%" + matchValue + "%' ";
                case MatchType.LT:
                    if (dataType== TypeCode.String)
                        return " < '" + matchValue + "'";
                    else
                        return " < " + matchValue;
                case MatchType.LE:
                    if (dataType == TypeCode.String)
                        return " <= '" + matchValue + "'";
                    else
                        return " <= " + matchValue;
                case MatchType.EQ:
                    if (dataType == TypeCode.String)
                        return " = '" + matchValue + "'";
                    else
                        return " = " + matchValue;
                case MatchType.GE:
                    if (dataType == TypeCode.String)
                        return " >= '" + matchValue + "'";
                    else
                        return " >= " + matchValue;
                case MatchType.GT:
                    if (dataType == TypeCode.String)
                        return " > '" + matchValue + "'";
                    else
                        return " > " + matchValue;
                case MatchType.NE:
                    if (dataType == TypeCode.String)
                        return " <> '" + matchValue + "'";
                    else
                        return " <> " + matchValue;
                default:
                    throw new Exception("Unexpected MatchType: " + matchType.ToString());
            }
        }

        // Add a filter item to the list of filters
        private void UpdateFilterFilterList(Filter filterItem, string propertyName)
        {
            List<Filter> anyColumnFilterList;

            if (!_gridFilters.ContainsKey(propertyName))
            {
                _gridFilters.Add(propertyName, new List<Filter>());
            }
            anyColumnFilterList = _gridFilters[propertyName];
            anyColumnFilterList.Add(filterItem);

            // ?? is this still needed ??
            // mark the filters as applied
            foreach (string key in _gridFilters.Keys)
            {
                anyColumnFilterList = _gridFilters[key];
                foreach (Filter filter in anyColumnFilterList)
                {
                    filterItem.IsApplied = true;
                }
            }
        }
        #endregion

        #region Utilities / private methods
        // utility to add a column to the list of sorted columns
        private void AddSortDescription(string columnName, ListSortDirection sortDirection)
        {
            _sortDescriptions.Add(columnName, sortDirection);
        }

        // put sort glyphs on the column headers
        private void SetColumnSortGlyphs()
        {
            ListSortDirection sortDirection;

            // remove sort glyphs on all columns
            foreach (DataGridViewColumn column in datagrid.Columns)
            {
                column.HeaderCell.SortGlyphDirection = SortOrder.None;
            }

            // set the glyphs for sorted columns
            foreach (string key in _sortDescriptions.Keys)
            {
                DataGridViewColumn column = datagrid.Columns[key];
                sortDirection = _sortDescriptions[key];
                if (sortDirection == ListSortDirection.Ascending)
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                }
                else
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.Descending;
                }
            }
        }

        // configure the DataGridView control for display
        private void PrepareGridForDisplay()
        {
            _ctrlKeyIsDown = false;
            _gridFilters.Clear();
            _dataTableIsFiltered = false;
            _sortDescriptions.Clear();
            _dataTableIsSorted = false;
            _rightClickedCell = null;
            _gridColumnResizeMode = DataGridViewAutoSizeColumnsMode.AllCells;

            if (this.Parent != null)
                this.Font = this.Parent.Font;
            datagrid.Font = this.Font;
            datagrid.DefaultCellStyle.Font = this.Font;
            datagrid.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;

            if (!string.IsNullOrEmpty(this.GridLabel))
            {
                this.lblTitleLine.Text = this.GridLabel;
                datagrid.Top = lblTitleLine.Top + lblTitleLine.Height;
                datagrid.Height = this.Height - datagrid.Top - 5;
                lblTitleLine.Visible = true;
            }
            else
            {
                lblTitleLine.Text = string.Empty;
                lblTitleLine.Visible = false;
                datagrid.Top = lblTitleLine.Top;
                datagrid.Height = this.Height - 5;
            }

            datagrid.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
            datagrid.RowHeadersVisible = true;
            datagrid.RowHeadersWidth = 25;
            datagrid.AllowUserToAddRows = false;
            datagrid.AllowUserToDeleteRows = false;
            datagrid.EditMode = DataGridViewEditMode.EditProgrammatically;
            datagrid.AllowUserToOrderColumns = true;
            datagrid.DataSource = _dataTable;
            datagrid.Columns[0].Visible = false;
            datagrid.AllowUserToResizeColumns = true;
            datagrid.CurrentCell = null;

            foreach (DataGridViewColumn aColumn in datagrid.Columns)
            {
                aColumn.SortMode = DataGridViewColumnSortMode.Programmatic;
                aColumn.HeaderCell.Style.Font = new Font(this.Font, FontStyle.Regular);

                if (_dataTable.Columns[aColumn.Name].DataType == typeof(int) || _dataTable.Columns[aColumn.Name].DataType == typeof(long) || _dataTable.Columns[aColumn.Name].DataType == typeof(short))
                {
                    if (aColumn.Name.ToLower() != "Id".ToLower())       // don't format Id integers
                        aColumn.DefaultCellStyle.Format = "N0";
                    aColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }

                if (_dataTable.Columns[aColumn.Name].DataType == typeof(float) || _dataTable.Columns[aColumn.Name].DataType == typeof(double) || _dataTable.Columns[aColumn.Name].DataType == typeof(decimal))
                {
                    aColumn.DefaultCellStyle.Format = "N2";
                    aColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }

                aColumn.ToolTipText = _dataTable.Columns[aColumn.Name].Caption;
            }
        }

        private void SetQualifiedControlName()
        {
            Control containingControl = this;
            while (!(containingControl is Form))   // find the form the contains this control
            {
                if (containingControl.Parent == null)   // contigency for Designer attempting to load control w/o parent
                    break;
                containingControl = containingControl.Parent;
            }
            if (containingControl is Form)              // contigency for Designer attempting to load control w/o parent
            {
                ((Form)containingControl).FormClosing += new FormClosingEventHandler(NavigationGrid_FormClosing);  // containing form is closing
                _qualifiedControlName = this.GetType().Namespace + "^" + this.GetType().Name + "^" + this.Name;
                _qualifiedControlName = this.ParentForm.GetType().Namespace + "^" + this.ParentForm.GetType().Name + "__" + _qualifiedControlName;
            }
        }

        private void LoadAndApplyDataGridViewSettings()
        {
            SetQualifiedControlName();

            if (!this.DisableSettingPersistence)
            {
                string classNameOfStoredObject = typeof(DGVSettings).FullName;                  // name of the class of the object being stored
                string appName = HAI.Core.Utilities.GetApplicationContext();
                string userName = HAI.Core.Utilities.GetUserName();

                string contextName = string.Empty;
                if (!string.IsNullOrEmpty(GridLabel))
                    contextName = GridLabel;
                string keyName = _qualifiedControlName + "__" + contextName;

                string serializedSettings = string.Empty;
                serializedSettings = ObjectStore.GetSerializedObject(appName, userName, keyName);
                if (!string.IsNullOrEmpty(serializedSettings))
                {
                    using (System.IO.StringWriter stringWriter = new System.IO.StringWriter())
                    {
                        System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(DGVSettings));
                        System.IO.StringReader stringReader = new System.IO.StringReader(serializedSettings);
                        _savedDgvSettings = (DGVSettings)(serializer.Deserialize(stringReader));
                    }
                }
            }

            if (_savedDgvSettings == null)
            {
                _savedDgvSettings = new DGVSettings();
                int numberOfCells=_dataTable.Rows.Count * _dataTable.Columns.Count;
                if (numberOfCells > 50000)
                    _savedDgvSettings.SettingMode = DGVSettingMode.ColumnHeaders;
                else
                    _savedDgvSettings.SettingMode = DGVSettingMode.Automatic;
            }

            switch (_savedDgvSettings.SettingMode)
            {
                case DGVSettingMode.Automatic:
                    _gridColumnResizeMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    break;
                case DGVSettingMode.ColumnHeaders:
                    _gridColumnResizeMode = DataGridViewAutoSizeColumnsMode.ColumnHeader;
                    break;
                case DGVSettingMode.Manual:
                    _gridColumnResizeMode = DataGridViewAutoSizeColumnsMode.None;
                    break;
                default:
                    throw new Exception("Unknown DGVSettingMode");
            }

            // collect the setting as the grid is configured by default 
            Dictionary<string, DGVColumnSettings> initialColumnSettings = new Dictionary<string, DGVColumnSettings>();
            foreach (DataGridViewColumn aColumn in datagrid.Columns)
            {
                if (aColumn.Index == 0)
                    continue;   // do not create settings object for hidden data object

                DGVColumnSettings settings = new DGVColumnSettings();
                settings.ColumnName = aColumn.Name;
                settings.ColumnWidth = aColumn.Width;
                settings.DisplayIndex = aColumn.Index;

                initialColumnSettings.Add(settings.ColumnName, settings);
            }

            // order the saved setting by the DisplayIndex (which may not be the final DisplayIndex)
            List<DGVColumnSettings> savedColumnSettingsOrderedByDisplayIndex = _savedDgvSettings.ColumnSettings.OrderBy<DGVColumnSettings, int>(x => x.DisplayIndex).ToList<DGVColumnSettings>();
            int displayIndex = 1;   // will not move the diplay position of the hidden object column.
            Dictionary<string, DGVColumnSettings> updatedColumnSettings = new Dictionary<string, DGVColumnSettings>();

            // apply saved settings to the settings object for the columns that match
            foreach (DGVColumnSettings savedSettings in savedColumnSettingsOrderedByDisplayIndex)
            {
                if (initialColumnSettings.ContainsKey(savedSettings.ColumnName))
                {
                    DGVColumnSettings updatedSettings = initialColumnSettings[savedSettings.ColumnName];
                    updatedSettings.ColumnWidth = savedSettings.ColumnWidth;
                    updatedSettings.DisplayIndex = displayIndex;
                    displayIndex++;
                }
            }

            // for the grid columns that do not have saved setting set DisplayIndex in settings ojbect to follow known columns
            foreach (DGVColumnSettings settings in initialColumnSettings.Values)
            {
                if (_savedDgvSettings.ColumnSettings.Find(x => x.ColumnName == settings.ColumnName) == null)
                {
                    settings.DisplayIndex = displayIndex;
                    displayIndex++;
                }
            }

            // apply the values in the setting objects to the columns in the grid
            foreach (DGVColumnSettings settings in initialColumnSettings.Values)
            {
                DataGridViewColumn column = datagrid.Columns[settings.ColumnName];
                column.DisplayIndex = settings.DisplayIndex;
                column.Width = settings.ColumnWidth;
            }

            _columnHeadingStyle = _savedDgvSettings.ColumnHeadingStyle;
            switch (_columnHeadingStyle)
            {
                case ColumnHeadingStyle.Standard:
                    standardNameToolStripMenuItem_Click(null, null);
                    break;
                case ColumnHeadingStyle.FullText:
                    alternateNameToolStripMenuItem_Click(null, null);
                    break;
                default:
                    throw new Exception("Unknown ColumnHeadingStyle: " + _columnHeadingStyle.ToString());
            }

        }

        private void PersistDataGridViewSettings()
        {
            if (this.DisableSettingPersistence)
                return;     // settings are not to be persisted for this grid

            if (datagrid.Rows.Count == 0)
                return;     // nothing to save

            DGVSettings newSettings = new DGVSettings();

            // set the mode for autosizing the columns
            switch (_gridColumnResizeMode)
            {
                case DataGridViewAutoSizeColumnsMode.AllCells:
                    newSettings.SettingMode = DGVSettingMode.Automatic;
                    break;
                case DataGridViewAutoSizeColumnsMode.ColumnHeader:
                    newSettings.SettingMode = DGVSettingMode.ColumnHeaders;
                    break;
                case DataGridViewAutoSizeColumnsMode.None:
                    newSettings.SettingMode = DGVSettingMode.Manual;
                    break;
                default:
                    throw new Exception("Unexpected DataGridViewAutoSizeColumnsMode: " + _gridColumnResizeMode.ToString());
            }

            newSettings.ColumnHeadingStyle = _columnHeadingStyle;

            // set the column settings objects
            bool columnsReordered = false;
            foreach (DataGridViewColumn aColumn in datagrid.Columns)
            {
                if (aColumn.DisplayIndex != aColumn.Index)
                    columnsReordered = true;
            }
            if (newSettings.SettingMode == DGVSettingMode.Manual || columnsReordered)
            {
                foreach (DataGridViewColumn aColumn in datagrid.Columns)
                {
                    DGVColumnSettings columnSettings = new DGVColumnSettings();
                    columnSettings.ColumnName = aColumn.Name;
                    columnSettings.ColumnWidth = aColumn.Width;
                    columnSettings.DisplayIndex = aColumn.DisplayIndex;

                    newSettings.ColumnSettings.Add(columnSettings);
                }
            }

            if (newSettings.Equals(_savedDgvSettings))
                return;

            if (this.Parent == null)
                return;

            // serialize the DGVSettings object
            string serializedSettings=string.Empty;
            using (System.IO.StringWriter stringWriter = new System.IO.StringWriter())
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(DGVSettings));
                serializer.Serialize(stringWriter, newSettings);
                serializedSettings = stringWriter.ToString();
            }

            string classNameOfStoredObject = typeof(DGVSettings).FullName;                  // name of the class of the object being stored
            string appName = HAI.Core.Utilities.GetApplicationContext();
            string userName = HAI.Core.Utilities.GetUserName();

            string contextName = string.Empty;
            if (!string.IsNullOrEmpty(GridLabel))
                contextName = GridLabel;
            string keyName = _qualifiedControlName + "__" + contextName;
            ObjectStore.PutSerializedObject(appName, userName, keyName, serializedSettings, classNameOfStoredObject);

            _savedDgvSettings = newSettings;
        }

        private void SetColumnWidths()
        {
            if (_gridColumnResizeMode != DataGridViewAutoSizeColumnsMode.None && _gridColumnResizeMode != DataGridViewAutoSizeColumnsMode.Fill)
            {
                Cursor saveCursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    datagrid.AutoResizeColumns(_gridColumnResizeMode);
                }
                finally
                {
                    Cursor.Current = saveCursor;
                }
            }
        }

        #endregion

        #region Properties
        public DataGridView DataGridView
        {
            get
            {
                return datagrid;
            }
        }

        public DataTable DataTable
        {
            get
            {
                return _dataTable;
            }
        }

        public bool MultiSelect
        {
            get
            {
                return datagrid.MultiSelect;
            }
            set
            {
                datagrid.MultiSelect = value;
            }
        }

        private string _gridLabel = string.Empty;
        public string GridLabel
        {
            get
            {
                return _gridLabel;
            }
            set
            {
                _gridLabel = value;

                if (string.IsNullOrEmpty(_gridLabel))
                {
                    lblTitleLine.Text = string.Empty;
                    lblTitleLine.Visible = false;
                    datagrid.Top = lblTitleLine.Top;
                    datagrid.Height = this.Height - 5;
                }
                else
                {
                    this.lblTitleLine.Text = _gridLabel;
                    lblTitleLine.Visible = true;
                    datagrid.Location = new Point(datagrid.Left, lblTitleLine.Top + lblTitleLine.Height);
                    datagrid.Height = this.Height - datagrid.Top - 5;
                }
            }
        }

        public bool DisableSettingPersistence { get; set; }

        internal Dictionary<string, List<Filter>> GridFilters
        {
            get
            {
                return _gridFilters;
            }
        }

        internal Dictionary<string, ListSortDirection> SortDescriptions
        {
            get
            {
                return _sortDescriptions;
            }
        }

        internal ColumnHeadingStyle ColumnHeadingStyle
        {
            get
            {
                return _columnHeadingStyle;
            }
        }

        internal DataGridViewAutoSizeColumnsMode GridColumnResizeMode
        {
            get
            {
                return _gridColumnResizeMode;
            }
        }
        #endregion

        #region Public methods
        public void ClearGrid()
        {
            PersistDataGridViewSettings();
            datagrid.DataSource = null;
        }

        public void SetTitle(string titleLine)
        {
            this.GridLabel = titleLine;

            if (!string.IsNullOrEmpty(GridLabel))
            {
                this.lblTitleLine.Text = GridLabel;
                datagrid.Top = lblTitleLine.Top + lblTitleLine.Height;
                datagrid.Height = this.Height - datagrid.Top - 5;
                lblTitleLine.Visible = true;
            }
            else
            {
                lblTitleLine.Text = string.Empty;
                lblTitleLine.Visible = false;
                datagrid.Top = lblTitleLine.Top;
                datagrid.Height = this.Height - 5;
            }
        }

        public void ShowDataTable(DataTable dataTable)
        {
            if (datagrid.Rows.Count > 0)
                PersistDataGridViewSettings();

            _savedDgvSettings = null;
            _dataTable = dataTable;
            PrepareGridForDisplay();

            LoadAndApplyDataGridViewSettings();

            if (datagrid.Rows.Count > 0)
                SetCurrentItem((HAI.Core.IObjectId)(datagrid.Rows[0].Cells[0].Value));
        }

        public void ShowDataTable(DataTable dataTable, string titleLine)
        {
            this.GridLabel = titleLine;
            this.ShowDataTable(dataTable);
        }

        public void ReplaceDataTableAndShow(DataTable dataTable, string titleLine)
        {
            this.ClearGrid();
            this.GridLabel = titleLine;
            this.ShowDataTable(dataTable);
        }

        public void ClearSelections()
        {
            foreach (DataGridViewCell aCell in datagrid.SelectedCells)
            {
                aCell.Selected = false;
            }
            datagrid.CurrentCell = null;
        }

        public HAI.Core.IObjectId GetCurrentItem()
        {
            HAI.Core.IObjectId item;

            DataGridViewRow currentRow = _currentlySelectedRow;
            if (currentRow == null)
                item = null;
            else
                item = (HAI.Core.IObjectId)(currentRow.Cells[0].Value);

            return item;
        }

        public object GetCurrentObject()
        {
            object item; 
            DataGridViewRow currentRow = _currentlySelectedRow;
            if (currentRow == null)
                item = null;
            else
                item = (currentRow.Cells[0].Value);

            return item;
        }

        public List<HAI.Core.IObjectId> GetSelectedItems()
        {
            List<HAI.Core.IObjectId> selectedItems = new List<HAI.Core.IObjectId>();

            foreach (DataGridViewCell aCell in datagrid.SelectedCells)
            {
                if (aCell.ColumnIndex == 0) // user cannot select a cell in column 0
                    continue;               // ... so ignore this "selected" cell.

                int rowIndex = aCell.RowIndex;
                DataGridViewRow aRow = datagrid.Rows[rowIndex];
                HAI.Core.IObjectId theObject = (HAI.Core.IObjectId)(aRow.Cells[0].Value);
                if (!selectedItems.Contains(theObject))
                    selectedItems.Add(theObject);
            }

            return selectedItems;
        }

        public void SetCurrentItem(HAI.Core.IObjectId item)
        {
            foreach (DataGridViewRow row in datagrid.Rows)
            {
                HAI.Core.IObjectId anyItem = (HAI.Core.IObjectId)(row.Cells[0].Value);
                if (anyItem.Id == item.Id)
                {
                    datagrid.CurrentCell = row.Cells[1];

                    if (SelectedRowChanged != null)
                    {
                        SelectedRowChangedEventArgs args = new SelectedRowChangedEventArgs();
                        args.Item = (HAI.Core.IObjectId)(row.Cells[0].Value);
                        SelectedRowChanged(this, args);
                    }

                    // make the current row the top displayed row
                    datagrid.FirstDisplayedScrollingRowIndex = datagrid.CurrentCell.RowIndex;
                    _currentlySelectedRow = datagrid.Rows[datagrid.CurrentCell.RowIndex];
                }
            }
        }

        public void Disable()
        {
            this.Enabled = false;
            this.DataGridView.RowsDefaultCellStyle.BackColor = this.BackColor;
            this.DataGridView.RowsDefaultCellStyle.ForeColor = Color.Gray;
            this.DataGridView.ColumnHeadersDefaultCellStyle.BackColor = this.BackColor;
            this.DataGridView.ColumnHeadersDefaultCellStyle.ForeColor = Color.Gray;
        }

        public void Enable()
        {
            this.Enabled = true;
            this.DataGridView.RowsDefaultCellStyle.BackColor = _rowBackColor;
            this.DataGridView.RowsDefaultCellStyle.ForeColor = _rowForeColor;
            this.DataGridView.ColumnHeadersDefaultCellStyle.BackColor = _headerBackColor;
            this.DataGridView.ColumnHeadersDefaultCellStyle.ForeColor = _headerForeColor;
        }

        public void CacheFiltersAndSorts()
        {
            // hold current settings for filters and sorts by serializing current values
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            using (System.IO.MemoryStream serializedObject = new System.IO.MemoryStream())
            {
                if (_gridFilters.Count == 0)
                {
                    _serializedGridFiltersCache = null;
                }
                else
                {
                    formatter.Serialize(serializedObject, _gridFilters);
                    byte[] cacheAsBytes = serializedObject.ToArray();
                    _serializedGridFiltersCache = Convert.ToBase64String(cacheAsBytes, 0, cacheAsBytes.Length);
                }
            }

            using (System.IO.MemoryStream serializedObject = new System.IO.MemoryStream())
            {
                if (_sortDescriptions.Count == 0)
                {
                    _serializedSortDescriptionsCache = null;
                }
                else
                {
                    formatter.Serialize(serializedObject, _sortDescriptions);
                    byte[] cacheAsBytes = serializedObject.ToArray();
                    _serializedSortDescriptionsCache = Convert.ToBase64String(cacheAsBytes, 0, cacheAsBytes.Length);
                }
            }

            // if future enchancement is needed to persist the settings
            // cache strings can be persisted into DB here
        }

        public void RestoreOldFilterSet()
        {
            // if future enchancement is needed to persist the settings
            // filter set cache string can be retrieved from DB here
            if (string.IsNullOrEmpty(_serializedGridFiltersCache))
                return;

            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            byte[] cacheAsBytes = Convert.FromBase64String(_serializedGridFiltersCache);
            using (System.IO.MemoryStream serializedGridFilters = new System.IO.MemoryStream(cacheAsBytes))
            {
                serializedGridFilters.Position = 0;
                _gridFilters = (Dictionary<string, List<Filter>>)(formatter.Deserialize(serializedGridFilters));
            }

            FilterRows();
        }

        public void RestoreOldSortSet()
        {
            // if future enchancement is needed to persist the settings
            // filter set cache string can be retrieved from DB here
            if (string.IsNullOrEmpty(_serializedSortDescriptionsCache))
                return;

            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            byte[] cacheAsBytes = Convert.FromBase64String(_serializedSortDescriptionsCache);
            using (System.IO.MemoryStream serializedSortDescriptions = new System.IO.MemoryStream(cacheAsBytes))
            {
                serializedSortDescriptions.Position = 0;
                _sortDescriptions = (Dictionary<string, ListSortDirection>)(formatter.Deserialize(serializedSortDescriptions));
            }

            SortColumns();
        }

        #endregion

        #region NavigationGrid Class Events
        public event RowDoubleClickEventHandler RowDoubleClick;

        public delegate void RowDoubleClickEventHandler(NavigationGrid theGrid, RowDoubleClickEventArgs args);

        public event SelectedRowChangedEventHandler SelectedRowChanged;

        public delegate void SelectedRowChangedEventHandler(NavigationGrid theGrid, SelectedRowChangedEventArgs args);
        #endregion
    }

    #region EventArgs for NavigationGrid Class Events
    public class RowDoubleClickEventArgs : EventArgs
    {
        public HAI.Core.IObjectId Item
        {
            get;
            set;
        }
    }

    public class SelectedRowChangedEventArgs : EventArgs
    {
        public HAI.Core.IObjectId Item
        {
            get;
            set;
        }
    }
    #endregion
}
