﻿namespace HAI.Core.Windows.UI.Controls
{
    partial class NavigationGridProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvSortedColumns = new System.Windows.Forms.ListView();
            this.lvFilteredColumns = new System.Windows.Forms.ListView();
            this.lblColumnHeadingStyle = new System.Windows.Forms.Label();
            this.lblColumnWidthStyle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFilteredRowCount = new System.Windows.Forms.Label();
            this.lblUnfilteredRowCount = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvSortedColumns
            // 
            this.lvSortedColumns.Location = new System.Drawing.Point(16, 25);
            this.lvSortedColumns.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lvSortedColumns.Name = "lvSortedColumns";
            this.lvSortedColumns.Size = new System.Drawing.Size(635, 128);
            this.lvSortedColumns.TabIndex = 0;
            this.lvSortedColumns.UseCompatibleStateImageBehavior = false;
            // 
            // lvFilteredColumns
            // 
            this.lvFilteredColumns.Location = new System.Drawing.Point(16, 179);
            this.lvFilteredColumns.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lvFilteredColumns.Name = "lvFilteredColumns";
            this.lvFilteredColumns.Size = new System.Drawing.Size(635, 128);
            this.lvFilteredColumns.TabIndex = 1;
            this.lvFilteredColumns.UseCompatibleStateImageBehavior = false;
            // 
            // lblColumnHeadingStyle
            // 
            this.lblColumnHeadingStyle.AutoSize = true;
            this.lblColumnHeadingStyle.Location = new System.Drawing.Point(293, 310);
            this.lblColumnHeadingStyle.Name = "lblColumnHeadingStyle";
            this.lblColumnHeadingStyle.Size = new System.Drawing.Size(151, 14);
            this.lblColumnHeadingStyle.TabIndex = 2;
            this.lblColumnHeadingStyle.Text = "lblColumnHeadingStyle";
            // 
            // lblColumnWidthStyle
            // 
            this.lblColumnWidthStyle.AutoSize = true;
            this.lblColumnWidthStyle.Location = new System.Drawing.Point(293, 330);
            this.lblColumnWidthStyle.Name = "lblColumnWidthStyle";
            this.lblColumnWidthStyle.Size = new System.Drawing.Size(136, 14);
            this.lblColumnWidthStyle.TabIndex = 3;
            this.lblColumnWidthStyle.Text = "lblColumnWidthStyle";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 14);
            this.label1.TabIndex = 4;
            this.label1.Text = "Sorted columns";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 14);
            this.label2.TabIndex = 5;
            this.label2.Text = "Filtered columns";
            // 
            // lblFilteredRowCount
            // 
            this.lblFilteredRowCount.AutoSize = true;
            this.lblFilteredRowCount.Location = new System.Drawing.Point(19, 330);
            this.lblFilteredRowCount.Name = "lblFilteredRowCount";
            this.lblFilteredRowCount.Size = new System.Drawing.Size(133, 14);
            this.lblFilteredRowCount.TabIndex = 6;
            this.lblFilteredRowCount.Text = "lblFilteredRowCount";
            // 
            // lblUnfilteredRowCount
            // 
            this.lblUnfilteredRowCount.AutoSize = true;
            this.lblUnfilteredRowCount.Location = new System.Drawing.Point(19, 310);
            this.lblUnfilteredRowCount.Name = "lblUnfilteredRowCount";
            this.lblUnfilteredRowCount.Size = new System.Drawing.Size(147, 14);
            this.lblUnfilteredRowCount.TabIndex = 7;
            this.lblUnfilteredRowCount.Text = "lblUnfilteredRowCount";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(577, 342);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // NavigationGridProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 377);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblUnfilteredRowCount);
            this.Controls.Add(this.lblFilteredRowCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblColumnWidthStyle);
            this.Controls.Add(this.lblColumnHeadingStyle);
            this.Controls.Add(this.lvFilteredColumns);
            this.Controls.Add(this.lvSortedColumns);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NavigationGridProperties";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Properties";
            this.Load += new System.EventHandler(this.NavigationGridProperties_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvSortedColumns;
        private System.Windows.Forms.ListView lvFilteredColumns;
        private System.Windows.Forms.Label lblColumnHeadingStyle;
        private System.Windows.Forms.Label lblColumnWidthStyle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFilteredRowCount;
        private System.Windows.Forms.Label lblUnfilteredRowCount;
        private System.Windows.Forms.Button btnOK;
    }
}