﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HAI.Core.Windows.UI.Controls
{
    public partial class NavigationGridProperties : Form
    {
        private NavigationGrid _navigationGrid;

        public NavigationGridProperties(NavigationGrid navigationGrid)
        {
            InitializeComponent();

            _navigationGrid = navigationGrid;
        }

        private void NavigationGridProperties_Load(object sender, EventArgs e)
        {
            lvSortedColumns.View = View.Details;
            lvSortedColumns.Columns.Add("Name", 200);
            lvSortedColumns.Columns.Add("Sort direction", 400);

            lvFilteredColumns.View = View.Details;
            lvFilteredColumns.Columns.Add("Name", 150);
            lvFilteredColumns.Columns.Add("Match type", 100);
            lvFilteredColumns.Columns.Add("Match value", 350);

            int count = 0;
            Color backColor;
            // fill sort listview
            List<string> keys;
            keys = new List<string>(_navigationGrid.SortDescriptions.Keys);
            keys.Sort();
            foreach (string key in keys)
            {

                if (count % 2 == 0)
                    backColor = Color.White;
                else
                    backColor = Color.LightGray;

                ListViewItem item = lvSortedColumns.Items.Add(key);
                item.SubItems.Add(_navigationGrid.SortDescriptions[key].ToString());

                item.BackColor = backColor;
                count++;
            }

            // fill filter list view
            keys = new List<string>(_navigationGrid.GridFilters.Keys);
            keys.Sort();
            backColor = Color.LightGray;
            string lastKey = string.Empty;
            foreach (string key in keys)
            {
                if (key != lastKey)
                {
                    if (backColor == Color.White)
                        backColor = Color.LightGray;
                    else
                        backColor = Color.White;

                    lastKey = key;
                }

                List<Filter> filters = _navigationGrid.GridFilters[key];
                foreach (Filter filter in filters)
                {
                    ListViewItem item = lvFilteredColumns.Items.Add(key);
                    item.SubItems.Add(filter.MatchType.ToString());
                    string text = string.Empty;

                    foreach (string matchString in filter.MatchValueList)
                    {
                        if (filter.DataTypeCode == TypeCode.String)
                            text = text + "'" + matchString + "' OR ";
                        else
                            text = text + matchString + " OR ";
                    }

                    text = text.Remove(text.Length - 4);
                    item.SubItems.Add(text);
                    item.BackColor = backColor;
                }
            }

            lblUnfilteredRowCount.Text= "Unfiltered rows: " + _navigationGrid.DataTable.Rows.Count.ToString();
            lblFilteredRowCount.Text = "Filtered rows: " + _navigationGrid.DataGridView.RowCount.ToString();

            switch (_navigationGrid.ColumnHeadingStyle)
            {
                case ColumnHeadingStyle.Standard:
                    lblColumnHeadingStyle.Text = "Column heading style: Standard name";
                    break;
                case ColumnHeadingStyle.FullText:
                    lblColumnHeadingStyle.Text = "Column heading style: Alternate name";
                    break;
                default:
                    throw new Exception("Unknown ColumnHeadingStyle" + _navigationGrid.ColumnHeadingStyle.ToString());
            }

            switch (_navigationGrid.GridColumnResizeMode)
            {
                case DataGridViewAutoSizeColumnsMode.AllCells:
                    lblColumnWidthStyle.Text = "Column width style: Auto size";
                    break;
                case DataGridViewAutoSizeColumnsMode.ColumnHeader:
                    lblColumnWidthStyle.Text = "Column width style: Default";
                    break;
                case DataGridViewAutoSizeColumnsMode.None:
                    lblColumnWidthStyle.Text = "Column width style: Manual";
                    break;
                default:
                    throw new Exception("Unexpected DataGridViewAutoSizeColumnsMode: " + _navigationGrid.GridColumnResizeMode.ToString());
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
