﻿using System;
using System.Windows.Forms;

namespace HAI.Core.Windows.UI.Controls
{
    public partial class NavigatorBasic : UserControl
    {
        public NavigatorBasic()
        {
            InitializeComponent();

            this.BorderStyle = BorderStyle.None;
        }

        public virtual void CompleteInitialization()
        {
            throw new Exception("Implementors must override CompleteInitialization()");
        }
    }
}
