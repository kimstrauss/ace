﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace HAI.Core.Windows.UI.Controls
{
    public partial class NavigatorResizable : UserControl
    {
        protected int _navigatorBottomMargin = 0;
        protected int _navigatorRightMargin = 0;

        private Size _gridSize = new Size();

        public NavigatorResizable()
        {
            InitializeComponent();

            this.BorderStyle = BorderStyle.None;
        }

        public virtual void CompleteInitialization()
        {
            navigationGrid.ClearSelections();

            btnAdd.Enabled = true;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
        }

        // resize the navigation grid when the form control is resized
        void Navigator_Resize(object sender, EventArgs e)
        {
            _gridSize.Width = this.Size.Width - 20;
            _gridSize.Height = this.Size.Height - 100;
            navigationGrid.Size = _gridSize;
        }

        private void NavigatorResizable_Load(object sender, EventArgs e)
        {
            _gridSize.Width = this.Size.Width - 20;
            _gridSize.Height = this.Size.Height - 100;
            Navigator_Resize(null, null);
            this.Resize += new EventHandler(Navigator_Resize);
        }
    }
}
