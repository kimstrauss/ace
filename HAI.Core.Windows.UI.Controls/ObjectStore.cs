﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace HAI.Core.Windows.UI.Controls
{
    public static class ObjectStore
    {
        public static string GetSerializedObject(string applicationName, string userName, string objectKey)
        {
            string serializedObject = string.Empty;
            UserCustomSettingsAccess access = new UserCustomSettingsAccess();

            try
            {
                serializedObject = access.GetSerializedObject(userName, applicationName, objectKey);
            }
            catch (Exception ex)
            {
                string message = "ObjectStore was unable to retrieve a serialized object.\r\n\n" + ex.Message;
                MessageBox.Show(message, "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return serializedObject;
        }

        public static void PutSerializedObject(string applicationName, string userName, string objectKey, string objectXML, string className)
        {
            UserCustomSettingsAccess access = new UserCustomSettingsAccess();

            try
            {
                access.PutSerializedObject(applicationName, userName, objectKey, objectXML, className);
            }
            catch (Exception ex)
            {
                string message = "ObjectStore was unable to save a serialized object.\r\n\n" + ex.Message;
                MessageBox.Show(message, "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
