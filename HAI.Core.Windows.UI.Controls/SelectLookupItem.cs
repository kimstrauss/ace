﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Reflection;

//using BlueDuck.Utilities;
using HAI.Core.Windows.UI.Controls;

namespace HAI.Core.Windows.UI.Controls
{
    public partial class SelectLookupItem<TLookupItem> : Form where TLookupItem : IObjectId
    {
        private List<TLookupItem> _lookupList;
        private HaiDataTable<TLookupItem> _lookupDT;
        private string _propertyNameToMatch = string.Empty;
        private string _propertyValueToMatch = null;

        public SelectLookupItem(List<TLookupItem> lookupList)
        {
            InitializeComponent();

            _lookupList = lookupList;
        }

        public SelectLookupItem(List<TLookupItem> lookupList, string propertyNameToMatch, object propertyValueToMatch)
        {
            InitializeComponent();

            _lookupList = lookupList;
            if (propertyNameToMatch != null)
                _propertyNameToMatch = propertyNameToMatch.Trim().ToLower();
            if (propertyValueToMatch != null)
                _propertyValueToMatch = propertyValueToMatch.ToString().Trim().ToLower();
        }

        private void SelectLookupItem_Shown(object sender, EventArgs e)
        {
            _lookupDT = new HaiDataTable<TLookupItem>(_lookupList);
            gridLookupItems.ShowDataTable(_lookupDT);

            if (_lookupList.Count > 0 && _lookupList[0] is IObjectId)
            {
                PropertyInfo[] properties = typeof(TLookupItem).GetProperties(BindingFlags.Instance | BindingFlags.Public);
                PropertyInfo theProperty = null;
                foreach (PropertyInfo aProperty in properties)
                {
                    if (aProperty.Name.ToLower() == _propertyNameToMatch)
                        theProperty = aProperty;
                }

                IObjectId matchingItem = null;
                if (theProperty != null)
                {
                    foreach (object anItem in _lookupList)
                    {
                        if (_propertyValueToMatch == theProperty.GetValue(anItem, null).ToString().ToLower())
                        {
                            matchingItem = (IObjectId)anItem;
                            break;
                        }
                    }
                }
                else
                {
                    throw new Exception("propertyNameToMatch does not exist: " + _propertyNameToMatch);
                }

                if (matchingItem != null)
                {
                    gridLookupItems.SetCurrentItem(matchingItem);
                }
            }
        }

        public TLookupItem SelectedLookupItem
        {
            get
            {
                TLookupItem item = (TLookupItem)gridLookupItems.GetCurrentObject();
                return item;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void gridLookupItems_RowDoubleClick(NavigationGrid theGrid, RowDoubleClickEventArgs args)
        {
            btnOK_Click(null, null);
        }
    }
}
