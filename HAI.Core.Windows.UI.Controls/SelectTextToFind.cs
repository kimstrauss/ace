using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace HAI.Core.Windows.UI.Controls
{
    public partial class SelectTextToFind : Form
    {
        private List<string> _uniqueNameList;
        private string _selectedText;

        public string SelectedText
        {
            get
            {
                return _selectedText;
            }
        }

        private SelectTextToFind()
        {
            InitializeComponent();
        }

        public SelectTextToFind(List<string> uniqueNameList)
        {
            InitializeComponent();
            _uniqueNameList = uniqueNameList;
            CancelButton = btnCancel;
            // AcceptButton = btnOK;  DO NOT set an AcceptButton; need to intercept <Enter> so text can be validated
        }

        private void SelectTextToFind_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;

            // set up the combobox to have typing autocomplete
            cbxUniqueTextList.DropDownStyle = ComboBoxStyle.DropDown;
            cbxUniqueTextList.AutoCompleteSource = AutoCompleteSource.ListItems;
            cbxUniqueTextList.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            Cursor saveCursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                _uniqueNameList.Sort();
                cbxUniqueTextList.BeginUpdate();
                cbxUniqueTextList.Items.AddRange(_uniqueNameList.ToArray());
                cbxUniqueTextList.EndUpdate();
            }
            finally
            {
                Cursor.Current = saveCursor;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _selectedText = string.Empty;
        }

        // handle click of OK button
        private void btnOK_Click(object sender, EventArgs e)
        {
            // validate the comboxbox Text against the list
            string text = cbxUniqueTextList.Text.Trim();
            int index = cbxUniqueTextList.FindStringExact(text);
            if (index < 0)
            {
                MessageBox.Show("Find text is not valid", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                _selectedText = text;
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        // intercept <Enter> to mimic OK as AcceptButton
        private void SelectTextToFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                btnOK_Click(null, null);
            }
        }

        private void FixText()
        {
            string text = cbxUniqueTextList.Text;
            int index = cbxUniqueTextList.FindString(text, 0);
            if (index < 0)
            {
                string newText = text.Remove(text.Length-1);
                int newTextIndex = cbxUniqueTextList.FindString(newText, 0);
                string goodText = (string)cbxUniqueTextList.Items[newTextIndex];

                cbxUniqueTextList.Text = goodText;
                cbxUniqueTextList.SelectionStart = newText.Length;
                cbxUniqueTextList.SelectionLength = goodText.Length - newText.Length;
                cbxUniqueTextList.Refresh();
            }
        }
    }
}