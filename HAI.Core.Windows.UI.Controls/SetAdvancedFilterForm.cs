using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using System.Linq;

namespace HAI.Core.Windows.UI.Controls
{
    public partial class SetAdvancedFilterForm : Form
    {
        private Filter _filter;
        private DataGridView _datagrid;
        List<string> _uniqueNames;

        public SetAdvancedFilterForm(DataGridView datagrid, Filter filter)
        {
            InitializeComponent();
            _filter = filter;
            _datagrid = datagrid;
        }

        private void SetAdvancedFilterForm_Load(object sender, EventArgs e)
        {
            txtPropertyName.Text = _filter.PropertyName;

            if (_filter.DataTypeCode == TypeCode.DateTime)
            {
                string dateString = _filter.MatchValueList[0];
                string matchText = dateString;
                txtMatchText.Text = matchText;
            }
            else
            {
                txtMatchText.Text = _filter.MatchValueList[0];
            }


            txtMatchText.SelectAll();

            if (_filter.DataTypeCode != TypeCode.String)
            {
                rbEqual.Checked = true;
                rbEquals.Enabled = false;
                rbBeginsWith.Enabled = false;
                rbEndsWith.Enabled = false;
                rbContains.Enabled = false;
                rbDoesNotContain.Enabled = false;
                rbDoesNotEqual.Enabled = false;
            }
            else
            {
                rbBeginsWith.Checked = true;
                rbEquals.Enabled = true;
                rbBeginsWith.Enabled = true;
                rbEndsWith.Enabled = true;
                rbContains.Enabled = true;
                rbDoesNotContain.Enabled = true;
                rbDoesNotEqual.Enabled = true;
            }

            txtMatchText.Enabled = true;

            this.AcceptButton = btnOK;
            this.CancelButton = btnCancel;

            treeUniqueText.Nodes.Add("All " + _filter.PropertyName);
            treeUniqueText.Enabled = false;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            List<string> matchItems = new List<string>();
            foreach (TreeNode node in treeUniqueText.Nodes[0].Nodes)
            {
                if (node.Checked)
                {
                    matchItems.Add(node.Text);
                }
            }

            // ensure that there is something to use as a filter
            if ((txtMatchText.Enabled && txtMatchText.Text.Trim()==string.Empty) 
                || (treeUniqueText.Enabled && (matchItems.Count == 0)))
            {
                MessageBox.Show("No items selected, nothing to filter", "Filter error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.DialogResult = DialogResult.Cancel;
                return;
            }

            _filter.MatchValueList.Clear();
            MatchType matchType;
            if (rbContains.Checked)
                matchType = MatchType.Contains;
            else if (rbEndsWith.Checked)
                matchType = MatchType.EndsWith;
            else if (rbEquals.Checked)
            {
                matchType = MatchType.EqualTo;
                foreach (TreeNode node in treeUniqueText.Nodes[0].Nodes)
                {
                    if (node.Checked)
                    {
                        _filter.MatchValueList.Add(node.Text);
                    }
                }
            }
            else if (rbBeginsWith.Checked)
                matchType = MatchType.BeginsWith;
            else if (rbDoesNotEqual.Checked)
                matchType = MatchType.NotEqual;
            else if (rbDoesNotContain.Checked)
                matchType = MatchType.NotContains;
            else if (rbLessThan.Checked)
                matchType = MatchType.LT;
            else if (rbLessThanOrEqual.Checked)
                matchType = MatchType.LE;
            else if (rbEqual.Checked)
                matchType = MatchType.EQ;
            else if (rbGreaterThanOrEqual.Checked)
                matchType = MatchType.GE;
            else if (rbGreaterThan.Checked)
                matchType = MatchType.GT;
            else if (rbNotEqual.Checked)
                matchType = MatchType.NE;
            else
                matchType = MatchType.Ignore;

            // "EqualTo" sets MatchValueList, otherwise set MatchValue
            string matchText = txtMatchText.Text;

            if (_filter.DataTypeCode == TypeCode.DateTime)
            {
                matchText = matchText.Trim().ToLower();
            }

            if (matchType != MatchType.EqualTo)
            {
                _filter.MatchValueList.Add(matchText);
            }
            _filter.MatchType = matchType;
        }

        private List<string> GetColumnUniqueItems()
        {
            int columnIndex = _datagrid.CurrentCell.ColumnIndex;
            List<string> rowText = new List<string>(_datagrid.Rows.Count);
            foreach (DataGridViewRow row in _datagrid.Rows)
            {
                rowText.Add(row.Cells[columnIndex].Value.ToString());
            }
            List<string> uniqueRowText = rowText.Distinct<string>(StringComparer.CurrentCultureIgnoreCase).ToList<string>();

            return uniqueRowText;
        }

        private void treeUniqueText_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            bool IsChecked = false;
            if (e.Node.Parent == null)      // this is the root
            {
                TreeNode rootNode = e.Node;
                if (rootNode.Checked)
                {
                    IsChecked = true;
                }
                foreach (TreeNode anyNode in rootNode.Nodes)
                {
                    anyNode.Checked = IsChecked;
                }
            }
        }

        private void rbEquals_CheckedChanged(object sender, EventArgs e)
        {
            if (rbEquals.Checked)
            {
                if (_uniqueNames == null)
                {
                    FillTree();
                }

                txtMatchText.Enabled = false;
                treeUniqueText.Enabled = true;
                treeUniqueText.BackColor = Color.White;
                treeUniqueText.ExpandAll();
            }
            else
            {
                txtMatchText.Enabled = true;
                treeUniqueText.Enabled = false;
                treeUniqueText.BackColor = this.BackColor;
                treeUniqueText.CollapseAll();
            }
        }

        private void FillTree()
        {
            Cursor saveCursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                TreeNode rootNode = treeUniqueText.Nodes[0];
                _uniqueNames = GetColumnUniqueItems();
                _uniqueNames.Sort();

                treeUniqueText.BeginUpdate();
                foreach (string name in _uniqueNames)
                {
                    rootNode.Nodes.Add(name);
                }
                treeUniqueText.EndUpdate();

                treeUniqueText.Enabled = false;
                treeUniqueText.BackColor = this.BackColor;
                treeUniqueText.CollapseAll();

            }
            finally
            {
                Cursor.Current = saveCursor;
            }
        }
    }
}