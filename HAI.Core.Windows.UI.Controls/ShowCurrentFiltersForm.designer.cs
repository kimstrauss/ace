namespace HAI.Core.Windows.UI.Controls
{
    partial class ShowCurrentFiltersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.lvFilters = new System.Windows.Forms.ListView();
            this.txtFilterPhrase = new System.Windows.Forms.TextBox();
            this.chkShowFilterExpression = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(490, 250);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(68, 25);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // lvFilters
            // 
            this.lvFilters.Location = new System.Drawing.Point(8, 11);
            this.lvFilters.Name = "lvFilters";
            this.lvFilters.Size = new System.Drawing.Size(550, 131);
            this.lvFilters.TabIndex = 1;
            this.lvFilters.UseCompatibleStateImageBehavior = false;
            // 
            // txtFilterPhrase
            // 
            this.txtFilterPhrase.Location = new System.Drawing.Point(8, 169);
            this.txtFilterPhrase.Multiline = true;
            this.txtFilterPhrase.Name = "txtFilterPhrase";
            this.txtFilterPhrase.ReadOnly = true;
            this.txtFilterPhrase.Size = new System.Drawing.Size(550, 70);
            this.txtFilterPhrase.TabIndex = 2;
            this.txtFilterPhrase.TabStop = false;
            // 
            // chkShowFilterExpression
            // 
            this.chkShowFilterExpression.AutoSize = true;
            this.chkShowFilterExpression.Location = new System.Drawing.Point(10, 150);
            this.chkShowFilterExpression.Name = "chkShowFilterExpression";
            this.chkShowFilterExpression.Size = new System.Drawing.Size(128, 17);
            this.chkShowFilterExpression.TabIndex = 4;
            this.chkShowFilterExpression.Text = "Show filter expression";
            this.chkShowFilterExpression.UseVisualStyleBackColor = true;
            this.chkShowFilterExpression.CheckedChanged += new System.EventHandler(this.chkShowFilterExpression_CheckedChanged);
            // 
            // ShowCurrentFiltersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 282);
            this.Controls.Add(this.chkShowFilterExpression);
            this.Controls.Add(this.txtFilterPhrase);
            this.Controls.Add(this.lvFilters);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ShowCurrentFiltersForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Current filters";
            this.Load += new System.EventHandler(this.ShowCurrentFiltersForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ListView lvFilters;
        private System.Windows.Forms.TextBox txtFilterPhrase;
        private System.Windows.Forms.CheckBox chkShowFilterExpression;
    }
}