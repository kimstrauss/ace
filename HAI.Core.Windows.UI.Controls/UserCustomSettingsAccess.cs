﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;

using HAI.UtilityServices.DataContracts;
using HAI.UtilityServices.ServiceContracts;
using HAI.UtilityServices.UserSettings;

namespace HAI.Core.Windows.UI.Controls
{
    internal class UserCustomSettingsAccess : IUserCustomSettingsAccess
    {
        public string GetSerializedObject(string userName, string applicationName, string settingKey)
        {
            UserCustomSetting settings = GetUserCustomSetting(userName, applicationName, settingKey);
            if (settings == null)
                return null;
            else
                return settings.Value;
        }

        public void PutSerializedObject(string applicationName, string userName, string objectKey, string objectXML, string className)
        {
            UserCustomSetting settings = new UserCustomSetting();
            settings.ApplicationName = applicationName;
            settings.UserName = userName;
            settings.Key = objectKey;
            settings.Value = objectXML;
            settings.SettingTypeName = className;
            PutUserCustomSetting(settings);
        }

        #region IUserCustomSettingsAccess Members

        public HAI.UtilityServices.DataContracts.UserCustomSetting GetUserCustomSetting(string userName, string applicationName, string settingKey)
        {
            IUserCustomSettingsAccess settingsManager = GetUserCustomSettingsAccess();
            try
            {
                return settingsManager.GetUserCustomSetting(userName, applicationName, settingKey);
            }
            finally
            {
                if (settingsManager is ICommunicationObject)
                    (settingsManager as ICommunicationObject).Close();
            }
        }

        public void PutUserCustomSetting(HAI.UtilityServices.DataContracts.UserCustomSetting setting)
        {
            IUserCustomSettingsAccess settingsManager = GetUserCustomSettingsAccess();
            try
            {
                settingsManager.PutUserCustomSetting(setting);
            }
            finally
            {
                if (settingsManager is ICommunicationObject)
                    (settingsManager as ICommunicationObject).Close();
            }
            
        }

        public void DeleteUserCustomSetting(HAI.UtilityServices.DataContracts.UserCustomSetting setting)
        {
            IUserCustomSettingsAccess settingsManager = GetUserCustomSettingsAccess();
            try
            {
                settingsManager.DeleteUserCustomSetting(setting);
            }
            finally
            {
                if (settingsManager is ICommunicationObject)
                    (settingsManager as ICommunicationObject).Close();
            }
            
        }

        public List<HAI.UtilityServices.DataContracts.UserCustomSetting> GetUserCustomSettingsForApplication(string userName, string applicationName)
        {
            IUserCustomSettingsAccess settingsManager = GetUserCustomSettingsAccess();
            try
            {
                return settingsManager.GetUserCustomSettingsForApplication(userName, applicationName);
            }
            finally
            {
                if (settingsManager is ICommunicationObject)
                    (settingsManager as ICommunicationObject).Close();
            }
            
        }

        public List<HAI.UtilityServices.DataContracts.UserCustomSetting> GetUserCustomSettingsForUser(string userName)
        {
            IUserCustomSettingsAccess settingsManager = GetUserCustomSettingsAccess();
            try
            {
                return settingsManager.GetUserCustomSettingsForUser(userName);
            }
            finally
            {
                if (settingsManager is ICommunicationObject)
                    (settingsManager as ICommunicationObject).Close();
            }
            
        }

        #endregion

        #region Private Methods
        private static IUserCustomSettingsAccess GetUserCustomSettingsAccess()
        {
            bool useWcf = false;
            bool useAppSettings = bool.TryParse(ConfigurationManager.AppSettings["UseWCF"], out useWcf);

            if (!useAppSettings)
            {
                return new UserSettingsManager();
            }
            else
            {
                if (useWcf)
                    return new HAI.UtilityServices.ServiceContracts.UserSettingsManagerProxy();
                else
                    return new UserSettingsManager();
            }
        }
        #endregion
    }
}
