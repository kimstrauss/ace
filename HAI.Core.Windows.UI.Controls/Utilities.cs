﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using System.Configuration;

namespace HAI.Core.Windows.UI.Controls
{
    public static class Utilities
    {
        private static System.Drawing.Icon errorIcon = null;
        private static System.Drawing.Icon warningIcon = null;
        private static System.Drawing.Icon informationIcon = null;
        private static System.Drawing.Icon noneIcon = null;

        public static System.Drawing.Icon GetMessageIcon(MessageLevel messageLevel)
        {
            System.Reflection.Assembly thisExe;
            thisExe = System.Reflection.Assembly.GetExecutingAssembly();
            object o = thisExe.GetManifestResourceNames();
            System.Resources.ResourceManager resourceManager = new System.Resources.ResourceManager("HAI.Core.Windows.UI.Controls.Properties.Resources", thisExe);

            switch (messageLevel)
            {
                case MessageLevel.None:
                    if (noneIcon == null)
                        noneIcon = (System.Drawing.Icon)resourceManager.GetObject("none");

                    return noneIcon;

                case MessageLevel.Information:
                    if (informationIcon == null)
                        informationIcon = (System.Drawing.Icon)resourceManager.GetObject("info");

                    return informationIcon;

                case MessageLevel.Warning:
                    if (warningIcon == null)
                        warningIcon = (System.Drawing.Icon)resourceManager.GetObject("warning");

                    return warningIcon;

                case MessageLevel.Error:
                    if (errorIcon == null)
                        errorIcon = (System.Drawing.Icon)resourceManager.GetObject("error");

                    return errorIcon;

                default:
                    throw new Exception("Unknown MessageLevel: " + messageLevel.ToString());
            }

            /*
            string iconPath = ConfigurationManager.AppSettings["IconPath"];
            if (!iconPath.EndsWith(@"\"))
                iconPath = iconPath + @"\";

            switch (messageLevel)
            {
                case MessageLevel.None:
                    if (noneIcon == null)
                        noneIcon = new System.Drawing.Icon(iconPath + "none.ico");

                    return noneIcon;

                case MessageLevel.Information:
                    if (informationIcon == null)
                        informationIcon = new System.Drawing.Icon(iconPath + "info.ico");

                    return informationIcon;

                case MessageLevel.Warning:
                    if (warningIcon == null)
                        warningIcon = new System.Drawing.Icon(iconPath + "warning.ico");

                    return warningIcon;

                case MessageLevel.Error:
                    if (errorIcon == null)
                        errorIcon = new System.Drawing.Icon(iconPath + "error.ico");

                    return errorIcon;

                default:
                    throw new Exception("Unknown MessageLevel: " + messageLevel.ToString());
            }
             * */
        }

        public static ILookupItem SelectLookupItem(List<ILookupItem> lookupList)
        {
            ILookupItem lookupItem = null;

            using (SelectLookupItem<ILookupItem> theForm = new SelectLookupItem<ILookupItem>(lookupList))
            {
                theForm.ShowDialog();
                if (theForm.DialogResult == System.Windows.Forms.DialogResult.OK)
                    lookupItem = theForm.SelectedLookupItem;
            }

            return lookupItem;
        }

        public static ILookupItem SelectLookupItem(List<ILookupItem> lookupList, string propertyNameToMatch, object propertyValueToMatch)
        {
            ILookupItem lookupItem = null;

            using (SelectLookupItem<ILookupItem> theForm = new SelectLookupItem<ILookupItem>(lookupList,propertyNameToMatch, propertyValueToMatch))
            {
                theForm.ShowDialog();
                if (theForm.DialogResult == System.Windows.Forms.DialogResult.OK)
                    lookupItem = theForm.SelectedLookupItem;
            }

            return lookupItem;
        }

        public static void SaveUserOptions(Type optionsObjectType, object options, string optionsKey)
        {
            string serializedSettings = string.Empty;
            using (System.IO.StringWriter stringWriter = new System.IO.StringWriter())
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(optionsObjectType);
                serializer.Serialize(stringWriter, options);
                serializedSettings = stringWriter.ToString();
            }

            string classNameOfStoredObject = optionsObjectType.FullName;                  // name of the class of the object being stored
            string appName = HAI.Core.Utilities.GetApplicationContext();
            string userName = HAI.Core.Utilities.GetUserName();

            ObjectStore.PutSerializedObject(appName, userName, optionsKey, serializedSettings, classNameOfStoredObject);
        }

        public static object LoadUserOptions(Type optionsObjectType, string optionsKey)
        {
            object options = null;
            string classNameOfStoredObject = optionsObjectType.FullName;                  // name of the class of the object being stored
            string appName = HAI.Core.Utilities.GetApplicationContext();
            string userName = HAI.Core.Utilities.GetUserName();

            string serializedSettings = string.Empty;
            serializedSettings = ObjectStore.GetSerializedObject(appName, userName, optionsKey);

            if (!string.IsNullOrEmpty(serializedSettings))
            {
                using (System.IO.StringWriter stringWriter = new System.IO.StringWriter())
                {
                    System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(optionsObjectType);
                    System.IO.StringReader stringReader = new System.IO.StringReader(serializedSettings);
                    options = serializer.Deserialize(stringReader);
                }
            }

            return options;
        }
    }
}
