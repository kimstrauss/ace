﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAI.Core
{
    public enum MessageLevel
    {
        None = 0,
        Information = 1,
        Warning = 2,
        Error = 3
    }

    public class ValidationResult
    {
        private List<string> _errorMessages;
        private List<string> _warningMessages;
        private List<string> _informationMessages;

        public int MessageCount
        {
            get
            {
                return ErrorCount + WarningCount + InformationCount;
            }
        }

        public int ErrorCount
        {
            get
            {
                if (_errorMessages == null)
                    return 0;
                else
                    return _errorMessages.Count;
            }
        }

        public int WarningCount
        {
            get
            {
                if (_warningMessages == null)
                    return 0;
                else
                    return _warningMessages.Count;
            }
        }

        public int InformationCount
        {
            get
            {
                if (_informationMessages == null)
                    return 0;
                else
                    return _informationMessages.Count;
            }
        }

        public MessageLevel MaxMessageLevel
        {
            get
            {
                if (ErrorCount > 0)
                    return MessageLevel.Error;
                if (WarningCount > 0)
                    return MessageLevel.Warning;
                if (InformationCount > 0)
                    return MessageLevel.Information;

                return MessageLevel.None;
            }
        }

        public List<string> ErrorMessages
        {
            get
            {
                return _errorMessages;
            }
        }

        public List<string> WarningMessages
        {
            get
            {
                return _warningMessages;
            }

        }

        public List<string> InformationMessages
        {
            get
            {
                return _informationMessages;
            }
        }

        public void AddErrorMessage(string errorMessage)
        {
            if (_errorMessages == null)
                _errorMessages = new List<string>();

            _errorMessages.Add(errorMessage);
        }

        public void AddWarningMessage(string warningMessage)
        {
            if (_warningMessages == null)
                _warningMessages = new List<string>();

            _warningMessages.Add(warningMessage);
        }

        public void AddInformationMessage(string informationMessage)
        {
            if (_informationMessages == null)
                _informationMessages = new List<string>();

            _informationMessages.Add(informationMessage);
        }

        public string GetSingleMessage()
        {
            if (MessageCount != 1)
                throw new Exception("Cannot GetSingleMessage -- MessageCount not equal 1");

            if (ErrorCount == 1)
                return _errorMessages[0];

            if (WarningCount == 1)
                return _warningMessages[0];

            if (InformationCount == 1)
                return _informationMessages[0];

            return string.Empty;
        }

        public string GetAllMessages()
        {
            string message = string.Empty;

            if (this.ErrorCount > 0)
            {
                message = "ERRORS:" + "\r\n";
                string[] messages = this.ErrorMessages.ToArray();
                message = message + string.Join("\n\r", messages);
            }

            if (this.WarningCount > 0)
            {
                if (message.Length > 0)
                    message = message + "\r\n\r\n";

                message = message + "WARNINGS:" + "\r\n";
                string[] messages = this.WarningMessages.ToArray();
                message = message + string.Join("\n\r", messages);
            }

            if (this.InformationCount > 0)
            {
                if (message.Length > 0)
                    message = message + "\r\n\r\n";

                message = message + "INFORMATION:" + "\r\n";
                string[] messages = this.InformationMessages.ToArray();
                message = message + string.Join("\n\r", messages);
            }

            return message;
        }

        public void CombineWith(ValidationResult resultToAdd)
        {
            if (resultToAdd.ErrorMessages != null)
            {
                foreach (string message in resultToAdd.ErrorMessages)
                {
                    this.AddErrorMessage(message);
                }
            }

            if (resultToAdd.WarningMessages != null)
            {
                foreach (string message in resultToAdd.WarningMessages)
                {
                    this.AddWarningMessage(message);
                }
            }

            if (resultToAdd.InformationMessages != null)
            {
                foreach (string message in resultToAdd.InformationMessages)
                {
                    this.AddInformationMessage(message);
                }
            }
        }
    }
}
