﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Globalization;

namespace HAI.Core
{
    public static class CommonValidations
    {
        public static ValidationResult EntryIsRequired(string propertyName, string theString, MessageLevel failureMessageLevel)
        {
            ValidationResult result = new ValidationResult();

            if (string.IsNullOrEmpty(theString) && failureMessageLevel == MessageLevel.Error)
                AddMessage(result, "An entry is required.", failureMessageLevel);

            if (string.IsNullOrEmpty(theString) && failureMessageLevel == MessageLevel.Warning)
                AddMessage(result, "An entry should be provided.", failureMessageLevel);

            if (string.IsNullOrEmpty(theString) && failureMessageLevel == MessageLevel.Information)
                AddMessage(result, "An entry was expected.", failureMessageLevel);

            return result;
        }

        public static ValidationResult StringLengthAcceptable(string propertyName, string theString, int minLength, int maxLength, MessageLevel failureMessageLevel)
        {
            ValidationResult result;

            if (minLength > 0)
            {
                ValidationResult requiredResult = EntryIsRequired(propertyName, theString, MessageLevel.Error);
                if (requiredResult.MessageCount > 0)
                {
                    result = new ValidationResult();
                    AddMessage(result, propertyName + ": Value is not provided.", failureMessageLevel);
                    return result;
                }
            }

            result = new ValidationResult();
            if (theString.Trim().Length < minLength)
            {
                if (failureMessageLevel == MessageLevel.Error)
                    AddMessage(result, propertyName + ": Entry cannot be less than " + minLength.ToString() + " characters.", failureMessageLevel);
                else
                    AddMessage(result, propertyName + ": Entry should not be less than " + minLength.ToString() + " characters.", failureMessageLevel);

                return result;
            }

            if (theString.Trim().Length > maxLength)
            {
                if (failureMessageLevel == MessageLevel.Error)
                    AddMessage(result, propertyName + ": Entry cannot be more than " + maxLength.ToString() + " characters.", failureMessageLevel);
                else
                    AddMessage(result, propertyName + ": Entry should not be more than " + maxLength.ToString() + " characters.", failureMessageLevel);
            }

            return result;
        }

        public static ValidationResult StringMinLengthAcceptable(string propertyName, string theString, int minLength, MessageLevel failureMessageLevel)
        {
            ValidationResult result;

            if (minLength > 0)
            {
                ValidationResult requiredResult = EntryIsRequired(propertyName, theString, MessageLevel.Error);
                if (requiredResult.MessageCount > 0)
                {
                    result = new ValidationResult();
                    AddMessage(result, propertyName + ": Value is not provided.", failureMessageLevel);
                    return result;
                }
            }

            result = new ValidationResult();
            if (theString.Trim().Length < minLength)
            {
                if (failureMessageLevel == MessageLevel.Error)
                    AddMessage(result, propertyName + ": Entry cannot be less than " + minLength.ToString() + " characters.", failureMessageLevel);
                else
                    AddMessage(result, propertyName + ": Entry should not be less than " + minLength.ToString() + " characters.", failureMessageLevel);
            }

            return result;
        }

        public static ValidationResult StringMaxLengthAcceptable(string propertyName, string theString, int maxLength, MessageLevel failureMessageLevel)
        {
            ValidationResult result;

            result = new ValidationResult();
            if (theString.Trim().Length > maxLength)
            {
                if (failureMessageLevel == MessageLevel.Error)
                    AddMessage(result, propertyName + ": Entry cannot be more than " + maxLength.ToString() + " characters.", failureMessageLevel);
                else
                    AddMessage(result, propertyName + ": Entry should not be more than " + maxLength.ToString() + " characters.", failureMessageLevel);
            }

            return result;
        }

        public static ValidationResult IsDate(string propertyName, string theString, MessageLevel failureMessageLevel)
        {
            ValidationResult result;

            ValidationResult requiredResult = EntryIsRequired(propertyName, theString, MessageLevel.Error);
            if (requiredResult.MessageCount > 0)
            {
                result = new ValidationResult();
                AddMessage(result, propertyName + ": Value is not provided.", failureMessageLevel);
                return result;
            }

            result = new ValidationResult();

            DateTime theDate;
            if (!DateTime.TryParse(theString, out theDate))
            {
                AddMessage(result, propertyName + "Entry cannot be converted to a date", failureMessageLevel);
            }

            return result;
        }

        public static ValidationResult DateValueIsAcceptable(string propertyName, string theString, DateTime minDate, DateTime maxDate, MessageLevel failureMessageLevel)
        {
            ValidationResult result;

            result = IsDate(propertyName, theString, failureMessageLevel);
            if (result.MessageCount > 0)
                return result;

            DateTime theDate = DateTime.Parse(theString);
            if (theDate < minDate)
            {
                if (failureMessageLevel == MessageLevel.Error)
                    AddMessage(result, propertyName + "Date cannot be before: " + minDate.ToString(), failureMessageLevel);
                else
                    AddMessage(result, propertyName + "Date should not be before: " + minDate.ToString(), failureMessageLevel);
            }

            if (theDate > maxDate)
            {
                if (failureMessageLevel == MessageLevel.Error)
                    AddMessage(result, propertyName + "Date cannot be after: " + maxDate.ToString(), failureMessageLevel);
                else
                    AddMessage(result, propertyName + "Date should not be after: " + maxDate.ToString(), failureMessageLevel);
            }

            return result;
        }

        public static ValidationResult IsNumeric(string propertyName, string theString, MessageLevel failureMessageLevel)
        {
            ValidationResult result;

            ValidationResult requiredResult = EntryIsRequired(propertyName, theString, MessageLevel.Error);
            if (requiredResult.MessageCount > 0)
            {
                result = new ValidationResult();
                AddMessage(result, propertyName + ": Value is not provided.", failureMessageLevel);
                return result;
            }

            result = new ValidationResult();

            float theNumber;
            if (!float.TryParse(theString, NumberStyles.AllowLeadingSign | NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingSign | NumberStyles.AllowTrailingWhite | NumberStyles.AllowDecimalPoint, null, out theNumber))
            {
                AddMessage(result, propertyName + "Entry cannot be converted to a number", failureMessageLevel);
            }

            return result;
        }

        public static ValidationResult NumericValueIsAcceptable(string propertyName, string theString, float minValue, float maxValue, MessageLevel failureMessageLevel)
        {
            ValidationResult result;

            result = IsNumeric(propertyName, theString, failureMessageLevel);
            if (result.MessageCount > 0)
                return result;

            float theNumber = float.Parse(theString, NumberStyles.AllowLeadingSign | NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingSign | NumberStyles.AllowTrailingWhite | NumberStyles.AllowDecimalPoint);
            if (theNumber < minValue)
            {
                if (failureMessageLevel == MessageLevel.Error)
                    AddMessage(result, propertyName + "Value cannot be less than: " + minValue.ToString(), failureMessageLevel);
                else
                    AddMessage(result, propertyName + "Value should not be less than: " + minValue.ToString(), failureMessageLevel);
            }

            if (theNumber > maxValue)
            {
                if (failureMessageLevel == MessageLevel.Error)
                    AddMessage(result, propertyName + "Value cannot be more than: " + maxValue.ToString(), failureMessageLevel);
                else
                    AddMessage(result, propertyName + "Value should not be more than: " + maxValue.ToString(), failureMessageLevel);
            }

            return result;
        }

        public static ValidationResult IsInteger(string propertyName, string theString, MessageLevel failureMessageLevel)
        {
            ValidationResult result;

            ValidationResult requiredResult = EntryIsRequired(propertyName, theString, MessageLevel.Error);
            if (requiredResult.MessageCount > 0)
            {
                result = new ValidationResult();
                AddMessage(result, propertyName + ": Value is not provided.", failureMessageLevel);
                return result;
            }

            result = new ValidationResult();

            int theNumber;
            if (!int.TryParse(theString, NumberStyles.AllowLeadingSign | NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingSign | NumberStyles.AllowTrailingWhite | NumberStyles.AllowDecimalPoint, null, out theNumber))
            {
                AddMessage(result, propertyName + "Entry cannot be converted to an integer", failureMessageLevel);
            }

            return result;
        }

        public static ValidationResult IntegerValueIsAcceptable(string propertyName, string theString, int minValue, int maxValue, MessageLevel failureMessageLevel)
        {
            ValidationResult result;

            result = IsInteger(propertyName, theString, failureMessageLevel);
            if (result.MessageCount > 0)
                return result;

            int theNumber = int.Parse(theString, NumberStyles.AllowLeadingSign | NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingSign | NumberStyles.AllowTrailingWhite | NumberStyles.AllowDecimalPoint);
            if (theNumber < minValue)
            {
                if (failureMessageLevel == MessageLevel.Error)
                    AddMessage(result, propertyName + "Value cannot be less than: " + minValue.ToString(), failureMessageLevel);
                else
                    AddMessage(result, "Value should not be less than: " + minValue.ToString(), failureMessageLevel);
            }

            if (theNumber > maxValue)
            {
                if (failureMessageLevel == MessageLevel.Error)
                    AddMessage(result, propertyName + "Value cannot be more than: " + maxValue.ToString(), failureMessageLevel);
                else
                    AddMessage(result, propertyName + "Value should not be more than: " + maxValue.ToString(), failureMessageLevel);
            }

            return result;
        }

        public static ValidationResult LookupListContainsCode(string propertyName, string theCode, List<ILookupItem> lookupList, MessageLevel failureMessageLevel)
        {
            ValidationResult result = new ValidationResult();

            HAI.Core.ILookupItem itemToFind = lookupList.Find(x => x.Code.ToLower() == theCode.ToLower());
            if (itemToFind == null)
            {
                AddMessage(result, propertyName + ": \"" + theCode + "\" is not found in the validation list.", failureMessageLevel);
            }

            return result;
        }


        private static void AddMessage(ValidationResult result, string message, MessageLevel failureMessageLevel)
        {
            switch (failureMessageLevel)
            {
                case MessageLevel.None:
                    break;

                case MessageLevel.Information:
                    result.AddInformationMessage(message);
                    break;

                case MessageLevel.Warning:
                    result.AddWarningMessage(message);
                    break;

                case MessageLevel.Error:
                    result.AddErrorMessage(message);
                    break;

                default:
                    throw new Exception("Unknown messageLevel: " + failureMessageLevel.ToString());
            }
        }
    }
}
