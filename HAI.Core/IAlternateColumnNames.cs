using System.Collections.Generic;

namespace HAI.Core
{
    public interface IAlternateColumnNames
    {
        Dictionary<string, string> GetAlternateNamesDict();
    }
}
