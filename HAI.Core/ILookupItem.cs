﻿using System;
namespace HAI.Core
{
    public interface ILookupItem : IObjectId
    {
        string Code { get; }
        string Name { get; }
        string Qualifier { get; }
        //int Id { get; }
    }
}
