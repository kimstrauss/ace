using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAI.Core
{
    public interface IPropertyBag
    {
        Properties<IPropertyBagPropertyBase> BaseProperties
        {
            get;
        }
    }
}
