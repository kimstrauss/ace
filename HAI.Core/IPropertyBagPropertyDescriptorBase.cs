﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAI.Core
{
    public interface IPropertyBagPropertyDescriptorBase
    {
        string PropertyKey { get; }
        string Name { get; set; }
        string Code { get; set; }
        string DataType { get; set; }
        int UIOrdinal { get; set; }
    }
}
