﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel;

namespace HAI.Core
{
    public class Properties<TProperty> : Dictionary<string, TProperty>, IEditableObject
    {
        #region IEditableObject Members

        public void BeginEdit()
        {
            foreach (TProperty aProperty in this.Values)
            {
                ((IEditableObject)(aProperty)).BeginEdit();
            }
        }

        public void CancelEdit()
        {
            foreach (TProperty aProperty in this.Values)
            {
                ((IEditableObject)(aProperty)).CancelEdit();
            }
        }

        public void EndEdit()
        {
            foreach (TProperty aProperty in this.Values)
            {
                ((IEditableObject)(aProperty)).EndEdit();
            }
        }

        #endregion
    }
}
