﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAI.Core
{
    public class MemoryTreeNode
    {
        private int _index;
        private string _name;
        private MemoryTreeNode _next;
        private MemoryTreeNode _parent;
        private MemoryTreeNode _prev;
        private MemoryTreeNodeCollection _nodes;

        // prevent creating a node without a name b/c new nodes are added
        // ... to the dictionary by name.
        private MemoryTreeNode()
        {
            initialize();
        }

        public MemoryTreeNode(string name)
        {
            initialize();
            _name = name.Trim();
        }

        private void initialize()
        {
            _next = null;
            _prev = null;
            _parent = null;
            _nodes = new MemoryTreeNodeCollection(this);
        }

        // "Checked" is not a UI feature, but rather will be used to mark nodes that
        // ... match "selections" from data stored in the database
        public bool Checked { get; set; }

        public MemoryTreeNode FirstNode
        {
            get
            {
                MemoryTreeNode theFirstNode = this;

                while (theFirstNode.PrevNode != null)
                {
                    theFirstNode = theFirstNode.PrevNode;
                }

                return theFirstNode;
            }
        }

        public int Index
        {
            get
            {
                return _index;
            }
        }

        public MemoryTreeNode LastNode
        {
            get
            {
                MemoryTreeNode theLastNode = this;

                while (theLastNode.NextNode != null)
                {
                    theLastNode = theLastNode.NextNode;
                }

                return theLastNode;
            }
        }

        public int Level
        {
            get
            {
                int theLevel = 0;
                MemoryTreeNode rootNode = this;

                while (rootNode != null)
                {
                    rootNode = rootNode.Parent;
                    theLevel++;
                }

                return theLevel;
            }
        }

        public string Name
        {
            // This property is ReadOnly to protect against a renaming that would
            // ... invalidate the key in the dictionary held by TreeNodeCollection.
            // If the property must be ReadWrite, then adjustments will be needed
            // ... to correctly update the dictionary and protect against duplicate
            // ... key entries.
            get
            {
                return _name;
            }
        }

        public MemoryTreeNode NextNode
        {
            get
            {
                return _next;
            }
        }

        public MemoryTreeNodeCollection Nodes
        {
            get
            {
                return _nodes;
            }
        }

        public MemoryTreeNode Parent
        {
            get
            {
                return _parent;
            }
        }

        public MemoryTreeNode PrevNode
        {
            get
            {
                return _prev;
            }
        }

        // N.B. this is a different implementation that the FullPath property of 
        // ... parallel class in System.Windows.Forms.  This method requires a separator
        // ... parameter whereas the parallel class gets the separator from the owning
        // ... TreeView class.
        public string GetFullPath(string separator)
        {
            string fullPath = string.Empty;

            MemoryTreeNode rootNode = this;

            while (rootNode != null)
            {
                fullPath = separator + rootNode.Name + fullPath;
                rootNode = rootNode.Parent;
            }

            return fullPath;
        }

        public object Tag { get; set; }

        public override string ToString()
        {
            return base.ToString() + " [" + this.Name + "]";
        }

        public void Remove()
        {
            MemoryTreeNode parent = this.Parent;
            MemoryTreeNodeCollection siblings = parent.Nodes;
            siblings.Remove(this);
            if (_prev != null)
                _prev._next = this.NextNode;
            if (_next != null)
                _next._prev = this._prev;
        }

        public class MemoryTreeNodeCollection : List<MemoryTreeNode>
        {
            private MemoryTreeNode _parentNode;
            private Dictionary<string, MemoryTreeNode> _nodesKeyedByName; // provides means to access list items by name

            // prevent creating a TreeNodeCollection with does not reference a parent node
            private MemoryTreeNodeCollection()
            {
            }

            // TreeNodeCollection should only be created when a new node is created,
            // ... it should never be created publicly
            internal MemoryTreeNodeCollection(MemoryTreeNode parent)
            {
                _parentNode = parent;
                _nodesKeyedByName = new Dictionary<string, MemoryTreeNode>();
            }

            public new int Add(MemoryTreeNode node)
            {
                _nodesKeyedByName.Add(node.Name, node); // ensure that name is unique before going any further

                // set relationships for the node
                node._parent = _parentNode;
                if (this.Count != 0)
                    node._prev = this[this.Count - 1];
                if (this.Count != 0)
                    this[this.Count - 1]._next = node;
                base.Add(node);
                node._index = this.Count - 1;

                return  node.Index;
            }

            public MemoryTreeNode this[string name]
            {
                get
                {
                    return _nodesKeyedByName[name];
                }
            }

            public new void Clear()
            {
                base.Clear();
                _nodesKeyedByName.Clear();
            }

            public bool ContainsKey(string name)
            {
                return _nodesKeyedByName.ContainsKey(name);
            }
        }
    }
}
