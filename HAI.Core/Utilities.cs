﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAI.Core
{
    public static class Utilities
    {
        public static string GetApplicationContext()
        {
            string appName = string.Empty;
            try
            {
                appName = System.Reflection.Assembly.GetEntryAssembly().EntryPoint.Module.Name;
            }
            catch
            {
                appName = System.Configuration.ConfigurationManager.AppSettings.Get("AppName");
                if (string.IsNullOrEmpty(appName))
                {
                    throw new Exception("Config file does not contain a value for 'AppName'");
                }
            }

            return appName;
        }

        public static string GetUserName()
        {
            System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
            string userName = ((System.Security.Principal.IIdentity)identity).Name;
            return userName;
        }
    }
}
