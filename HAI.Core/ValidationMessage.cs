﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAI.Core
{
    public class ValidationMessage
    {
        public ValidationMessage()
        {
            Source = string.Empty;
            Text = string.Empty;
        }

        public ValidationMessage(string source, string text)
        {
            Source = source;
            Text = text;
        }

        public string Source
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }
    }
}
