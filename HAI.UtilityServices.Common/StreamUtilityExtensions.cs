﻿using System;
using System.IO;
using System.IO.Compression;


namespace HAI.UtilityServices.Common
{
	/// <summary>
	/// Utility class with Stream helper extensions.
	/// </summary>
	public static class StreamUtilityExtensions
	{
		/// <summary>
		/// Copies the contents of input to output. Doesn't close either stream.
		/// </summary>
		public static void CopyToStream(this Stream input, Stream output)
		{
			byte[] buffer = new byte[8 * 1024];
			int len;
			while ( (len = input.Read(buffer, 0, buffer.Length)) > 0)
			{
				output.Write(buffer, 0, len);
			}
		}
		/// <summary>
		/// Copies contents of stream to a file.  Does not close the stream.
		/// </summary>
		/// <param name="inputStream">Stream to be written to file.</param>
		/// <param name="outputFilePath">Output file path</param>
		public static void CopyToFile(this Stream inputStream,string outputFilePath)
		{
			using(Stream file = File.Create(outputFilePath))
			{
				inputStream.CopyToStream(file);
			}
		}
		/// <summary>
		/// Run this stream through a compression stream, then write output to file. Does not close the stream.
		/// </summary>
		/// <param name="inputStream">Stream to be compressed and written to file</param>
		/// <param name="outputFilePath">Output file path</param>
		public static void CompressAndCopyToFile(this Stream inputStream,string outputFilePath)
		{
			using(Stream fileCompressed = File.Create(outputFilePath))
			using(Stream gzStream = new GZipStream(fileCompressed,CompressionMode.Compress))
			{
				inputStream.CopyToStream(gzStream);
			}
		}

		/// <summary>
		/// Run this stream through a decompression stream, then write output to file.  Does not close the stream.
		/// </summary>
		/// <param name="inputStream"></param>
		/// <param name="outputFilePath"></param>
		public static void DecompressAndCopyToFile(this Stream inputStream,string outputFilePath)
		{
			using(Stream output = File.Create(outputFilePath))
			using(Stream gzStream = new GZipStream(inputStream,CompressionMode.Decompress))
			{
				gzStream.CopyToStream(output);
			}
		}
	}
}
