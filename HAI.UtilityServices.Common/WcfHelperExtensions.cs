﻿using System;
using System.ServiceModel;

namespace HAI.UtilityServices.Common
{
    /// <summary>
    /// Helper extensions for WCF operations.
    /// </summary>
    public static class WcfHelperExtensions
    {
        /// <summary>
        /// Cleans up a WCF proxy object connection in accordance with MS best practice recommendations.
        /// </summary>
        /// <param name="proxy">WCF Proxy object implementing ICommunicationObject.</param>
        public static void TryCloseOrAbort(this ICommunicationObject proxy)
        {
            if (proxy == null)
                return;

            if (proxy.State != CommunicationState.Faulted &&
                proxy.State != CommunicationState.Closed)
            {
                try { proxy.Close(); }
                catch (CommunicationObjectFaultedException)
                { proxy.Abort(); }
                catch (TimeoutException)
                { proxy.Abort(); }
                catch (Exception)
                {
                    proxy.Abort();
                    throw;
                }
            }
            else
                proxy.Abort();

        }
    }
}
