﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace HAI.UtilityServices.DataContracts
{
	[DataContract(Namespace="http://services.haiint.com/utility/filerepository/2009/10")]
	public class RepositoryFileInformation
	{
		[DataMember]
		public Guid Id
		{
			get;
			set;
		}
		[DataMember]
		public string Name
		{
			get;
			set;
		}
		[DataMember]
		public string CreatedByUser
		{
			get;
			set;
		}
		[DataMember]
		public bool IsCompressed
		{
			get;
			set;
		}
		[DataMember]
		public DateTime StoredDate
		{
			get;
			set;
		}
		[DataMember]
		public long TotalBytes
		{
			get;
			set;
		}
        [DataMember]
        public string RepositoryPath 
        { 
            get; 
            set; 
        }
	}
}
