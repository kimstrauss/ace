﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace HAI.UtilityServices.DataContracts
{
    [MessageContract()]
    public class RepositoryFileStreamUploadMessage : IDisposable
    {
        [MessageHeader(MustUnderstand = true)]
        public string Name { get; set; }

		[MessageHeader(MustUnderstand = true)]
        public string FromUserName { get; set; }

		[MessageHeader(MustUnderstand = true)]
        public long TotalBytes { get; set; }

		 [MessageHeader(MustUnderstand = true)]
        public Guid FileId { get; set; }

        [MessageBodyMember(Order=1)]
        public Stream FileStream{ get; set; }

        #region IDisposable Members

        public void Dispose()
        {
			  Debug.WriteLine(string.Format("RepositoryFileStreamMessage Dispose called.  Thread ID {0}", Process.GetCurrentProcess().Id));

			  try
			  {
				  if(this.FileStream != null)
				  {
					  this.FileStream.Close();
					  this.FileStream.Dispose();
					  this.FileStream = null;
				  }
			  }
			  catch(Exception)
			  {
				  // Something went wrong during disposal.  
				  // The stream is probably already closed, or a communication exception has occurred.
			  }
        }

        #endregion
    }
}
