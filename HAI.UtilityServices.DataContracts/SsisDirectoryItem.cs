﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace HAI.UtilityServices.DataContracts
{
    [DataContract(Namespace = "http://services.haiint.com/utility/sqlssispackageexec/2010/01")]
    [KnownType(typeof(SsisDirectoryFolder))]
    [KnownType(typeof(SsisPackage))]
    public abstract class SsisDirectoryItem
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Path { get; set; }
    }
}
