﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace HAI.UtilityServices.DataContracts
{
    [DataContract(Namespace = "http://services.haiint.com/utility/sqlssispackageexec/2010/01")]
    public class SsisPackage : SsisDirectoryItem
    {
        [DataMember]
        public List<SsisPackageVariable> Variables { get; set; }

    }
}
