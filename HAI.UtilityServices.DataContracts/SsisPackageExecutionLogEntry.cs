﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace HAI.UtilityServices.DataContracts
{
    [DataContract(Namespace = "http://services.haiint.com/utility/sqlssispackageexec/2010/01")]
    public class SsisPackageExecutionLogEntry
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string RequestUser { get; set; }
        [DataMember]
        public DateTime RequestDateTime { get; set; }
        [DataMember]
        public DateTime PackageStartDateTime { get; set; }
        [DataMember]
        public DateTime? PackageEndDateTime { get; set; }
        [DataMember]
        public int DtsExecResultCode { get; set; }
        [DataMember]
        public string PackageName { get; set; }
        [DataMember]
        public Guid PackageGuid { get; set; }
        [DataMember]
        public string SsisServerName { get; set; }
        [DataMember]
        public string ServiceHostName { get; set; }
        [DataMember]
        public string ClientName { get; set; }
        [DataMember]
        public List<SsisPackageVariable> InputParameters { get; set; }
    }
}
