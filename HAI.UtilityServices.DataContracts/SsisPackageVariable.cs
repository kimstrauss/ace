﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace HAI.UtilityServices.DataContracts
{
    [DataContract(Namespace = "http://services.haiint.com/utility/sqlssispackageexec/2010/01")]
    public class SsisPackageVariable
    {
        [DataMember]
        public string ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Namespace { get; set; }

        [DataMember]
        public bool ReadOnly { get; set; }

        [DataMember]
        public bool SystemVariable { get; set; }

        [DataMember]
        public TypeCode DataType { get; set; }

        [DataMember]
        public string Value { get; set; }
    }
}
