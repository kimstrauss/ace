﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace HAI.UtilityServices.DataContracts
{
	/// <summary>
	/// Data contract describing an application customization setting for a user.
	/// </summary>
	[DataContract(Namespace="http://services.haiint.com/utility/usercustomsetting/2009/08")]
	public class UserCustomSetting
	{
		/// <summary>
		/// NT User Name of owner of this setting.
		/// </summary>
		[DataMember()]
		public string UserName
		{
			get;
			set;
		}
		
		/// <summary>
		/// Name of application for which the setting applies.
		/// </summary>
		[DataMember()]
		public string ApplicationName
		{
			get;
			set;
		}

		/// <summary>
		/// .NET Object Type of persisted setting value.
		/// </summary>
		[DataMember()]
		public string SettingTypeName
		{
			get;
			set;
		}
		
		/// <summary>
		/// Key for this setting. 
		/// </summary>
		/// <remarks>This key should be unique within the context of this ApplicationName.  The persistence service can only store a single value for a key.</remarks>
		[DataMember()]
		public string Key
		{
			get;
			set;
		}
		
		/// <summary>
		/// Serialized value of setting.
		/// </summary>
		[DataMember()]
		public string Value
		{
			get;
			set;
		}
		
		/// <summary>
		/// Date and time stamp of when setting was last used (i.e. accessed by this user)
		/// </summary>
		[DataMember()]
		public DateTime LastUsed
		{
			get;
			set;
		}
		
		/// <summary>
		/// Date and time stamp of when setting was last updated/changed.
		/// </summary>
		[DataMember()]
		public DateTime LastUpdated
		{
			get;
			set;
		}
	}
}
