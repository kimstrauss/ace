﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

using HAI.UtilityServices.DataContracts;

namespace HAI.UtilityServices.ServiceContracts
{
    public class FileRepositoryDirectoryProxy : ClientBase<IFileRepositoryDirectory>,IFileRepositoryDirectory
	{
		public FileRepositoryDirectoryProxy()
		{
		}
		public FileRepositoryDirectoryProxy(string endpointName) : base(endpointName)
		{
		}

		#region IFileRepositoryDirectory Members

		public List<RepositoryFileInformation> GetRepositoryFileDirectory()
		{
			return Channel.GetRepositoryFileDirectory();
		}

		public RepositoryFileInformation GetRepositoryFileInformation(Guid fileId)
		{
			return Channel.GetRepositoryFileInformation(fileId);
		}

		public void DeleteRepositoryFile(Guid fileId)
		{
			Channel.DeleteRepositoryFile(fileId);
		}

		#endregion
	}
}
