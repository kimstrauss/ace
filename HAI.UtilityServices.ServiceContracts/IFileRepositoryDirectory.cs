﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

using HAI.UtilityServices.DataContracts;


namespace HAI.UtilityServices.ServiceContracts
{
	[ServiceContract(Namespace="http://services.haiint.com/utility/filerepository/2009/10")]
	public interface IFileRepositoryDirectory
	{
		[OperationContract()]
		List<RepositoryFileInformation> GetRepositoryFileDirectory();

		[OperationContract()]
		RepositoryFileInformation GetRepositoryFileInformation(Guid fileId);

		[OperationContract()]
		void DeleteRepositoryFile(Guid fileId);

	}
}
