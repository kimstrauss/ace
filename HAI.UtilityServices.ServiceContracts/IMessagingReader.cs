﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAI.UtilityServices.ServiceContracts
{

    /// <summary>
    /// Front-facing contract for Messaging subsystem read operations.
    /// </summary>
    interface IMessagingReader
    {
    }
}
