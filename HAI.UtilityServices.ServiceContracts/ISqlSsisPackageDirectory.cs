﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

using HAI.UtilityServices.DataContracts;

namespace HAI.UtilityServices.ServiceContracts
{
    [ServiceContract(Namespace = "http://services.haiint.com/utility/sqlssispackageexec/2010/01")]
    public interface ISqlSsisPackageDirectory
    {
        [OperationContract()]
        List<SsisDirectoryItem> GetSsisDirectoryListing(string directoryName);

        [OperationContract()]
        SsisPackage GetSsisPackageDetails(string packageDirectoryPath, string packageName);

    }
}
