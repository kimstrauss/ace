﻿using System;
using System.IO;
using System.ServiceModel;

using HAI.UtilityServices.DataContracts;

namespace HAI.UtilityServices.ServiceContracts
{
	/// <summary>
	/// Contract for a File Repository Interface supporting streamed uploads and downloads.
	/// </summary>
	/// <remarks>
	/// Streaming interfaces in WCF behave quite a bit differently than conventional service interfaces;
	/// consult MSDN documentation and Juval Lowy's book for more details.
	/// </remarks>
	[ServiceContract(Namespace="http://services.haiint.com/utility/filerepository/2009/10")]
    public interface IStreamingFileRepository : IDisposable
    {
        [OperationContract]
        Stream DownloadFile(Guid fileId);

        [OperationContract(Action="UploadFile", IsOneWay=true)]
        void UploadFile(RepositoryFileStreamUploadMessage fileMessage);

    }
}
