﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

using HAI.UtilityServices.DataContracts;

namespace HAI.UtilityServices.ServiceContracts
{
	/// <summary>
	/// Interface defining a UserCustomSetting persistence and retrieval service.
	/// </summary>
	[ServiceContract(Namespace="http://services.haiint.com/utility/usercustomsetting/2009/08")]
	public interface IUserCustomSettingsAccess
	{
		/// <summary>
		/// Gets a single UserCustomSetting.
		/// </summary>
		/// <param name="userName">User Name of owner.</param>
		/// <param name="applicationName">Application name for which setting applies.</param>
		/// <param name="settingKey">Key to setting</param>
		/// <returns>UserCustomSetting / Null</returns>
		[OperationContract()]
		UserCustomSetting GetUserCustomSetting(string userName,string applicationName,string settingKey);
		
		/// <summary>
		/// Persists a UserCustomSetting to storage.
		/// </summary>
		/// <param name="setting">UserCustomSetting object</param>
		[OperationContract()]
		void PutUserCustomSetting(UserCustomSetting setting);
		
		/// <summary>
		/// Deletes a UserCustomSetting from storage.
		/// </summary>
		/// <param name="setting">UserCustomSetting object</param>
		[OperationContract()]
		void DeleteUserCustomSetting(UserCustomSetting setting);
		
		/// <summary>
		/// Gets a list of UserCustomSettings for a user and an application.
		/// </summary>
		/// <param name="userName">User Name of owner.</param>
		/// <param name="applicationName">Application name for which setting applies.</param>
		/// <returns>List<UserCustomSetting> / Null</returns>
		[OperationContract()]
		List<UserCustomSetting> GetUserCustomSettingsForApplication(string userName,string applicationName);
	
		/// <summary>
		/// Gets a full list of UserCustomSettings for a user across all applications.
		/// </summary>
		/// <param name="userName">User Name of owner.</param>
		/// <returns>List<UserCustomSetting> / Null</returns>
		[OperationContract()]
		List<UserCustomSetting> GetUserCustomSettingsForUser(string userName);
	}
}
