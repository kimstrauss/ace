﻿using System;
using System.ServiceModel;

namespace HAI.UtilityServices.ServiceContracts
{
    public class SqlSsisExecutionEngineProxy : ClientBase<ISqlSsisExecutionEngine>, ISqlSsisExecutionEngine
    {
        public SqlSsisExecutionEngineProxy() { }
        public SqlSsisExecutionEngineProxy(string endpointName) : base(endpointName) { }

        #region ISqlSsisExecutionEngine Members

        public void RunSsisPackage(HAI.UtilityServices.DataContracts.SsisPackage package)
        {
            Channel.RunSsisPackage(package);
        }

        #endregion
    }

}
