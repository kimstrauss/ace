﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace HAI.UtilityServices.ServiceContracts
{
    public class SqlSsisPackageDirectoryProxy : ClientBase<ISqlSsisPackageDirectory>, ISqlSsisPackageDirectory
    {
        public SqlSsisPackageDirectoryProxy() { }
        public SqlSsisPackageDirectoryProxy(string endpointName) : base(endpointName) { }

        #region ISqlSsisPackageEngine Members

        public List<HAI.UtilityServices.DataContracts.SsisDirectoryItem> GetSsisDirectoryListing(string directoryName)
        {
            return Channel.GetSsisDirectoryListing(directoryName);
        }

        public HAI.UtilityServices.DataContracts.SsisPackage GetSsisPackageDetails(string packageDirectoryPath, string packageName)
        {
            return Channel.GetSsisPackageDetails(packageDirectoryPath, packageName);
        }

        #endregion
    }

}
