﻿using System;
using System.IO;
using System.ServiceModel;

using HAI.UtilityServices.DataContracts;


namespace HAI.UtilityServices.ServiceContracts
{
    public class StreamingFileRepositoryProxy : ClientBase<IStreamingFileRepository>, IStreamingFileRepository
    {
        public StreamingFileRepositoryProxy() { }
        public StreamingFileRepositoryProxy(string endpointName) : base(endpointName) { }

        #region IStreamingFileRepository Members

        public Stream DownloadFile(Guid fileId)
        {
            return Channel.DownloadFile(fileId);
        }

        public void UploadFile(RepositoryFileStreamUploadMessage fileMessage)
        {
            Channel.UploadFile(fileMessage);
        }

        #endregion
    }

}
