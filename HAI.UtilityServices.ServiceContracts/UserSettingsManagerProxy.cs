﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using HAI.UtilityServices.DataContracts;

namespace HAI.UtilityServices.ServiceContracts
{
    public class UserSettingsManagerProxy : ClientBase<IUserCustomSettingsAccess>, IUserCustomSettingsAccess
    {
        public UserSettingsManagerProxy()
        {
        }

        public UserSettingsManagerProxy(string endpointName)
            : base(endpointName)
        {
        }

        #region IUserCustomSettingsAccess Members

        public UserCustomSetting GetUserCustomSetting(string userName, string applicationName, string settingKey)
        {
            return Channel.GetUserCustomSetting(userName, applicationName, settingKey);
        }

        public void PutUserCustomSetting(UserCustomSetting setting)
        {
            Channel.PutUserCustomSetting(setting);
        }

        public void DeleteUserCustomSetting(UserCustomSetting setting)
        {
            throw new NotImplementedException();
        }

        public List<UserCustomSetting> GetUserCustomSettingsForApplication(string userName, string applicationName)
        {
            throw new NotImplementedException();
        }

        public List<UserCustomSetting> GetUserCustomSettingsForUser(string userName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
