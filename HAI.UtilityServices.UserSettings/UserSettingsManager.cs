﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.ServiceModel;

using HAI.UtilityServices.DataContracts;
using HAI.UtilityServices.ServiceContracts;


namespace HAI.UtilityServices.UserSettings
{

	/// <summary>
	/// Utility class for persistence and retrieval of user custom application settings.
	/// </summary>
	/// <remarks>
	/// Implemented with SQL Server storage.  Based on "GetCustom" system built for VB6 iSTAT, with some updates for .NET.
	/// Settings tables and stored procs were developed in HAIADMIN database in Summer 2009.
	/// </remarks>
	[ServiceBehavior(InstanceContextMode=InstanceContextMode.PerCall)]
	public class UserSettingsManager : IUserCustomSettingsAccess
	{
		/// <summary>
		/// Connection string to settings database.
		/// </summary>
		private string m_DatabaseConnectionString;
		
		/// <summary>
		/// Default constructor.  Uses ConfigurationManager to look for a ConnectionString named "UserSettings.Database".
		/// </summary>
		/// <remarks>
		/// Argument free; for use in WCF services.  
		/// NOTE : WCF will not allow for chaining of constructors, which is why this constructor does not call 
		/// the alternate constructor with the connectionString parameter.
		/// </remarks>
		public UserSettingsManager()
		{
			if(ConfigurationManager.ConnectionStrings["UserSettings.Database"] == null || 
				string.IsNullOrEmpty(ConfigurationManager.ConnectionStrings["UserSettings.Database"].ConnectionString)
				)
				throw new ConfigurationErrorsException("UserSettings.Database connection string is not specified in configuration file, or is blank.");

			m_DatabaseConnectionString = ConfigurationManager.ConnectionStrings["UserSettings.Database"].ConnectionString;

		}

		/// <summary>
		/// Alternate constructor.
		/// </summary>
		/// <param name="settingsDatabaseConnectionString">SQL Connection string for settings database.</param>
		public UserSettingsManager(string settingsDatabaseConnectionString)
		{
			if(string.IsNullOrEmpty(settingsDatabaseConnectionString))
				throw new ArgumentException("Cannot connect to settings database.  Connection string to settings database is null or empty.");

			m_DatabaseConnectionString = settingsDatabaseConnectionString;
		}

		#region IUserCustomSettingsAccess Members

		UserCustomSetting IUserCustomSettingsAccess.GetUserCustomSetting(string userName,string applicationName,string settingKey)
		{
			UserCustomSetting userSetting = null;

			using(SqlConnection conn = new SqlConnection(m_DatabaseConnectionString))
			{
                conn.Open();
			    SqlCommand cmdGetSetting = BuildUcGetAsettingCommand(userName,applicationName,settingKey);
			    cmdGetSetting.Connection = conn;

			    SqlDataReader reader = cmdGetSetting.ExecuteReader();

			    while(reader.Read())
			    {
				    userSetting = ConvertRecordToUserCustomSetting(reader,applicationName,settingKey);
			    }

			    conn.Close();

			}

			return userSetting;
		}

		void IUserCustomSettingsAccess.PutUserCustomSetting(UserCustomSetting setting)
		{
            using (SqlConnection conn = new SqlConnection(m_DatabaseConnectionString))
            {
                conn.Open();

                SqlCommand cmdPutSetting = BuildUcPutAsettingCommand(setting.UserName, setting.ApplicationName, setting.Key, setting.SettingTypeName, setting.Value);

                cmdPutSetting.Connection = conn;

                cmdPutSetting.ExecuteNonQuery();

                conn.Close();
            }
		}

		void IUserCustomSettingsAccess.DeleteUserCustomSetting(UserCustomSetting setting)
		{
            using (SqlConnection conn = new SqlConnection(m_DatabaseConnectionString))
            {
                conn.Open();

                SqlCommand cmdDeleteSetting = BuildUcDeleteAsettingCommand(setting.UserName, setting.ApplicationName, setting.Key);
                cmdDeleteSetting.Connection = conn;
                cmdDeleteSetting.ExecuteNonQuery();

                conn.Close();
            }
		}

		List<UserCustomSetting> IUserCustomSettingsAccess.GetUserCustomSettingsForApplication(string userName,string applicationName)
		{
			List<UserCustomSetting> userAppSettings = null;

            using (SqlConnection conn = new SqlConnection(m_DatabaseConnectionString))
            {
                conn.Open();

                SqlCommand cmdGetSetting = BuildUcGetSettingsForUserApplicationCommand(userName, applicationName);
                cmdGetSetting.Connection = conn;

                SqlDataReader reader = cmdGetSetting.ExecuteReader();
                if (reader.HasRows)
                {
                    userAppSettings = new List<UserCustomSetting>();

                    while (reader.Read())
                    {
                        userAppSettings.Add(ConvertRecordToUserCustomSetting(reader, applicationName));
                    }
                }

                conn.Close();
            }

            return userAppSettings;
		}

		List<UserCustomSetting> IUserCustomSettingsAccess.GetUserCustomSettingsForUser(string userName)
		{
			List<UserCustomSetting> userSettings = null;

            using (SqlConnection conn = new SqlConnection(m_DatabaseConnectionString))
            {
                conn.Open();

                SqlCommand cmdGetSetting = BuildUcGetSettingsForUserCommand(userName);
                cmdGetSetting.Connection = conn;

                SqlDataReader reader = cmdGetSetting.ExecuteReader();

                if (reader.HasRows)
                {
                    userSettings = new List<UserCustomSetting>();

                    while (reader.Read())
                    {
                        userSettings.Add(ConvertRecordToUserCustomSetting(reader));
                    }
                }

                conn.Close();
            }

            return userSettings;
		}

		#endregion

		#region Record Read / Convert Methods
		/// <summary>
		/// Converts a database record to a UserCustomSetting object.
		/// </summary>
		/// <param name="reader">DataReader</param>
		/// <returns>UserCustomSetting object for current row.</returns>
		private static UserCustomSetting ConvertRecordToUserCustomSetting(SqlDataReader reader)
		{
			return ConvertRecordToUserCustomSetting(reader,
				(string)reader["ApplicationName"],
				(string)reader["CompositeKey"]);
		}
		/// <summary>
		/// Converts a database record to a UserCustomSetting object.
		/// </summary>
		/// <param name="reader">DataReader</param>
		/// <param name="applicationName">Application Name; used when known in advance.</param>
		/// <returns>UserCustomSetting object for current row.</returns>
		private static UserCustomSetting ConvertRecordToUserCustomSetting(SqlDataReader reader,string applicationName)
		{
			return ConvertRecordToUserCustomSetting(reader,
				applicationName,
				(string)reader["CompositeKey"]);
		}
		/// <summary>
		/// Converts a database record to a UserCustomSetting object.
		/// </summary>
		/// <param name="reader">DataReader</param>
		/// <param name="applicationName">Application Name; used when known in advance.</param>
		/// <param name="settingKey">Setting key; used when known in advance.</param>
		/// <returns>UserCustomSetting object for current row.</returns>
		private static UserCustomSetting ConvertRecordToUserCustomSetting(SqlDataReader reader,string applicationName,string settingKey)
		{
			UserCustomSetting userSetting = new UserCustomSetting
			{
				ApplicationName = applicationName,
				Key = settingKey,
				LastUpdated = (DateTime)reader["LastModifiedDateTime"],
				SettingTypeName = (string)reader["UcTypeName"],
				UserName = (string)reader["EntryUid"],
				Value = (string)reader["Value"]
			};
			
			if(reader["LastReadDtm"] == DBNull.Value)
				userSetting.LastUsed = DateTime.MinValue;
			else
				userSetting.LastUsed = (DateTime)reader["LastReadDtm"];

			return userSetting;
		}
		#endregion
		
		#region SqlCommand Builder Methods
		private SqlCommand BuildUcGetAsettingCommand(string userName,string applicationName,string compositeKey)
		{
			SqlCommand cmdUcGetAsetting = new SqlCommand
			{
				CommandText = "UcGetAsetting",
				CommandType = CommandType.StoredProcedure
			};

			SqlParameter paramreturnValue = new SqlParameter
			{
				ParameterName = "@RETURN_VALUE",
				Direction = ParameterDirection.ReturnValue,
				SqlDbType = SqlDbType.Int
			};
			cmdUcGetAsetting.Parameters.Add(paramreturnValue);

			SqlParameter paramUserName = new SqlParameter
			{
				ParameterName = "@UserName",
				SqlDbType = SqlDbType.VarChar
			};

			if(string.IsNullOrEmpty(userName))
				paramUserName.Value = DBNull.Value;
			else
				paramUserName.Value = userName;

			cmdUcGetAsetting.Parameters.Add(paramUserName);

			SqlParameter paramApplicationName = new SqlParameter
			{
				ParameterName = "@ApplicationName",
				SqlDbType = SqlDbType.VarChar
			};

			if(string.IsNullOrEmpty(applicationName))
				paramApplicationName.Value = DBNull.Value;
			else
				paramApplicationName.Value = applicationName;

			cmdUcGetAsetting.Parameters.Add(paramApplicationName);

			SqlParameter paramCompositeKey = new SqlParameter
			{
				ParameterName = "@CompositeKey",
				SqlDbType = SqlDbType.VarChar
			};

			if(string.IsNullOrEmpty(compositeKey))
				paramCompositeKey.Value = DBNull.Value;
			else
				paramCompositeKey.Value = compositeKey;

			cmdUcGetAsetting.Parameters.Add(paramCompositeKey);

			return cmdUcGetAsetting;
		}

		private SqlCommand BuildUcDeleteAsettingCommand(string userName,string applicationName,string compositeKey)
		{
			SqlCommand cmdUcDeleteAsetting = new SqlCommand
			{
				CommandText = "UcDeleteAsetting",
				CommandType = CommandType.StoredProcedure
			};

			SqlParameter paramreturnValue = new SqlParameter
			{
				ParameterName = "@RETURN_VALUE",
				Direction = ParameterDirection.ReturnValue,
				SqlDbType = SqlDbType.Int
			};
			cmdUcDeleteAsetting.Parameters.Add(paramreturnValue);

			SqlParameter paramUserName = new SqlParameter
			{
				ParameterName = "@UserName",
				SqlDbType = SqlDbType.VarChar
			};

			if(string.IsNullOrEmpty(userName))
				paramUserName.Value = DBNull.Value;
			else
				paramUserName.Value = userName;

			cmdUcDeleteAsetting.Parameters.Add(paramUserName);

			SqlParameter paramApplicationName = new SqlParameter
			{
				ParameterName = "@ApplicationName",
				SqlDbType = SqlDbType.VarChar
			};

			if(string.IsNullOrEmpty(applicationName))
				paramApplicationName.Value = DBNull.Value;
			else
				paramApplicationName.Value = applicationName;

			cmdUcDeleteAsetting.Parameters.Add(paramApplicationName);

			SqlParameter paramCompositeKey = new SqlParameter
			{
				ParameterName = "@CompositeKey",
				SqlDbType = SqlDbType.VarChar
			};

			if(string.IsNullOrEmpty(compositeKey))
				paramCompositeKey.Value = DBNull.Value;
			else
				paramCompositeKey.Value = compositeKey;

			cmdUcDeleteAsetting.Parameters.Add(paramCompositeKey);

			return cmdUcDeleteAsetting;
		}

		private SqlCommand BuildUcPutAsettingCommand(string userName,string applicationName,string compositeKey,string ucTypeName,string value)
		{
			SqlCommand cmdUcPutAsetting = new SqlCommand
			{
				CommandText = "UcPutAsetting",
				CommandType = CommandType.StoredProcedure
			};

			SqlParameter paramreturnValue = new SqlParameter
			{
				ParameterName = "@RETURN_VALUE",
				Direction = ParameterDirection.ReturnValue,
				SqlDbType = SqlDbType.Int
			};
			cmdUcPutAsetting.Parameters.Add(paramreturnValue);

			SqlParameter paramUserName = new SqlParameter
			{
				ParameterName = "@UserName",
				SqlDbType = SqlDbType.VarChar
			};

			if(string.IsNullOrEmpty(userName))
				paramUserName.Value = DBNull.Value;
			else
				paramUserName.Value = userName;

			cmdUcPutAsetting.Parameters.Add(paramUserName);

			SqlParameter paramApplicationName = new SqlParameter
			{
				ParameterName = "@ApplicationName",
				SqlDbType = SqlDbType.VarChar
			};

			if(string.IsNullOrEmpty(applicationName))
				paramApplicationName.Value = DBNull.Value;
			else
				paramApplicationName.Value = applicationName;

			cmdUcPutAsetting.Parameters.Add(paramApplicationName);

			SqlParameter paramCompositeKey = new SqlParameter
			{
				ParameterName = "@CompositeKey",
				SqlDbType = SqlDbType.VarChar
			};

			if(string.IsNullOrEmpty(compositeKey))
				paramCompositeKey.Value = DBNull.Value;
			else
				paramCompositeKey.Value = compositeKey;

			cmdUcPutAsetting.Parameters.Add(paramCompositeKey);

			SqlParameter paramUcTypeName = new SqlParameter
			{
				ParameterName = "@UcTypeName",
				SqlDbType = SqlDbType.VarChar
			};

			if(string.IsNullOrEmpty(ucTypeName))
				paramUcTypeName.Value = DBNull.Value;
			else
				paramUcTypeName.Value = ucTypeName;

			cmdUcPutAsetting.Parameters.Add(paramUcTypeName);

			SqlParameter paramValue = new SqlParameter
			{
				ParameterName = "@Value",
				SqlDbType = SqlDbType.VarChar
			};

			if(string.IsNullOrEmpty(value))
				paramValue.Value = DBNull.Value;
			else
				paramValue.Value = value;

			cmdUcPutAsetting.Parameters.Add(paramValue);

			return cmdUcPutAsetting;
		}

		private SqlCommand BuildUcGetSettingsForUserCommand(string userName)
		{
			SqlCommand cmdUcGetSettingsForUser = new SqlCommand
			{
				CommandText = "UcGetSettingsForUser",
				CommandType = CommandType.StoredProcedure
			};

			SqlParameter paramreturnValue = new SqlParameter
			{
				ParameterName = "@RETURN_VALUE",
				Direction = ParameterDirection.ReturnValue,
				SqlDbType = SqlDbType.Int
			};
			cmdUcGetSettingsForUser.Parameters.Add(paramreturnValue);

			SqlParameter paramUserName = new SqlParameter
			{
				ParameterName = "@UserName",
				SqlDbType = SqlDbType.VarChar
			};
			
			if(string.IsNullOrEmpty(userName))
				paramUserName.Value = DBNull.Value;
			else
				paramUserName.Value = userName;
			
			cmdUcGetSettingsForUser.Parameters.Add(paramUserName);
			
			return cmdUcGetSettingsForUser;
		}

		private SqlCommand BuildUcGetSettingsForUserApplicationCommand(string userName, string applicationName)
		{
			SqlCommand cmdUcGetSettingsForUserApplication = new SqlCommand
			{
				CommandText = "UcGetSettingsForUserApplication",
				CommandType = CommandType.StoredProcedure
			};

			SqlParameter paramreturnValue = new SqlParameter
			{
				ParameterName = "@RETURN_VALUE",
				Direction = ParameterDirection.ReturnValue,
				SqlDbType = SqlDbType.Int
			};
			cmdUcGetSettingsForUserApplication.Parameters.Add(paramreturnValue);

			SqlParameter paramUserName = new SqlParameter
			{
				ParameterName = "@UserName",
				SqlDbType = SqlDbType.VarChar
			};
			
			if(string.IsNullOrEmpty(userName))
				paramUserName.Value = DBNull.Value;
			else
				paramUserName.Value = userName;
			
			cmdUcGetSettingsForUserApplication.Parameters.Add(paramUserName);

			SqlParameter paramApplicationName = new SqlParameter
			{
				ParameterName = "@ApplicationName",
				SqlDbType = SqlDbType.VarChar
			};
			
			if(string.IsNullOrEmpty(applicationName))
				paramApplicationName.Value = DBNull.Value;
			else
				paramApplicationName.Value = applicationName;
			
			cmdUcGetSettingsForUserApplication.Parameters.Add(paramApplicationName);
			
			return cmdUcGetSettingsForUserApplication;
		}
				#endregion
	}
}
