using System;
using System.Collections.Generic;
using System.Text;

namespace HaiBusinessObject
{
    public class BrowsableProperty
    {
        public BrowsableProperty(string propertyName, string dbColumnName, string propertyDescription, string friendlyName)
        {
            _propertyName = propertyName;
            _dbColumnName = dbColumnName;
            _propertyDescription = propertyDescription;
            _friendlyName = friendlyName;
            
            // set default values
            _maxStringLength = 0;
            _mustBeUnique = false;
            _isReadonly = false;
            _dataType = TypeCode.String;
            _isNullable = false;
        }

        public BrowsableProperty(string propertyName, string dbColumnName, string propertyDescription, string friendlyName, TypeCode dataType)
        {
            _propertyName = propertyName;
            _dbColumnName = dbColumnName;
            _propertyDescription = propertyDescription;
            _friendlyName = friendlyName;
            _dataType = dataType;

            // set default values
            _maxStringLength = 0;
            _mustBeUnique = false;
            _isReadonly = false;
            _isNullable = false;
        }

        private TypeCode _dataType;
        public TypeCode DataType
        {
            get
            {
                return _dataType;
            }
            set
            {
                if (value == TypeCode.String || value == TypeCode.Int32 || value == TypeCode.Double || value == TypeCode.DateTime || value == TypeCode.Boolean || value== TypeCode.Decimal)
                    _dataType = value;
                else
                    throw new Exception("Data type not allowed: " + value.ToString());
            }
        }

        private bool _isNullable;
        public bool IsNullable
        {
            get
            {
                return _isNullable;
            }
            set
            {
                _isNullable = value;
            }
        }

        private bool _mustBeUnique;
        public bool MustBeUnique
        {
            get
            {
                return _mustBeUnique;
            }
            set
            {
                _mustBeUnique = value;
            }
        }

        private string _propertyName;
        public string PropertyName
        {
            get
            {
                return _propertyName;
            }
        }

        private string _dbColumnName;
        public string DbColumnName
        {
            get
            {
                return _dbColumnName;
            }
        }

        private string _propertyDescription;
        public string PropertyDescription
        {
            get
            {
                return _propertyDescription;
            }
        }

        private string _friendlyName;
        public string FriendlyName
        {
            get
            {
                return _friendlyName;
            }
        }

        private bool _isReadonly;
        public bool IsReadonly
        {
            get
            {
                return _isReadonly;
            }
            set
            {
                _isReadonly = value;
            }
        }

        private int _maxStringLength;
        public int MaxStringLength
        {
            get
            {
                return _maxStringLength;
            }
            set
            {
                _maxStringLength = value;
            }
        }
    }
}
