using System;
using System.Collections.Generic;

using System.ComponentModel;
using System.Xml.Serialization;

using HaiBusinessObject.Core;

namespace HaiBusinessObject
{
    // these methods allow certain operations on the binding list 
    // ... without the need to know the generic type, thus making
    // ... it easier to access the list in an untyped environment
    public interface IUntypedBindingList : IBindingListView
    {
        bool IsFiltered
        {
            get;
        }

        HaiBusinessObjectType TypeOfBussinessObjectsInList
        {
            get;
        }

        HaiReports.ReportParameters ReportParameters
        {
            get;
            set;
        }

        void RemoveFromList(object itemToDelete);

        void ResetItem(int indexOfItem);

        void AddToList(object itemToAdd);

        event EventHandler UpdateSortGlyphs;            // allow subscriber (e.g. ucReadonlyDatagridForm) to be notified that sort glyphs need updating
    }

    [Serializable]
    public class HaiBindingList<T> : BindingList<T>, IBindingListView, IUntypedBindingList
    {
        public event EventHandler UpdateSortGlyphs;     // allow subscriber (e.g. ucReadonlyDatagridForm) to be notified that sort glyphs need updating

        #region Fields

        [NonSerialized()]
        private bool _sorted = false;                       // is the list in a sorted state
        [NonSerialized()]
        private bool _filtered = false;                     // is the list in a filtered state
        [NonSerialized()]
        private bool _suspendOnListChangedEvent = false;

        [NonSerialized()]
        private ListSortDirection _sortDirection = ListSortDirection.Ascending;
        [NonSerialized()]
        private PropertyDescriptor _sortProperty = null;
        [NonSerialized()]
        private ListSortDescriptionCollection _sortDescriptions;

        [NonSerialized()]
        private List<T> _originalCollection = new List<T>();        // cache of the original collection; used to save original list during sorting/filtering
        [NonSerialized()]
        private Dictionary<string, List<Filter>> _gridFilters;      // filters that are being applied

        private HaiBusinessObjectType _thisType;                    // Hai "type" of objects held by this collection; simpler that using System Type.

        #endregion

        // constructor
        public HaiBindingList()
        {
            AllowEdit = false;
            AllowRemove = false;
            AllowNew = false;

            // create object of type T to determine if it is derived from HaiBusinessObjectBase, and return type
            if (typeof(T).IsSubclassOf(typeof(HaiBusinessObjectBase)))
            {
                HaiBusinessObjectBase anyHaiObject = (HaiBusinessObjectBase)Activator.CreateInstance(typeof(T));
                _thisType = anyHaiObject.ObjectType;
            }
            else
            {
                _thisType = HaiBusinessObjectType.UnknownHaiBusinessObjectType;
            }

        }

        #region Properties  

        private HaiReports.ReportParameters _reportParameters;
        public HaiReports.ReportParameters ReportParameters
        {
            get
            {
                return _reportParameters;
            }
            set
            {
                _reportParameters = value;
            }
        }

        public HaiBusinessObjectType TypeOfBussinessObjectsInList
        {
            get
            {
                return _thisType;
            }
        }

        public bool SupportsAdvancedSorting
        {
            get
            {
                return true;
            }
        }

        public ListSortDescriptionCollection SortDescriptions
        {
            get
            {
                return _sortDescriptions;
            }
        }

        public bool SupportsFiltering
        {
            get
            {
                return false;
            }
        }

        private string _filterXml;
        public string Filter
        {
            get
            {
                return _filterXml;
            }
            set
            {
                _filterXml = value;

                if (_filterXml != null)
                {
                    System.IO.StringReader xmlStream = new System.IO.StringReader(_filterXml);
                    Type[] extraTypes = new Type[3];
                    extraTypes[0] = typeof(KeyValueFilterListEntity);
                    extraTypes[1] = typeof(Filter);
                    extraTypes[2] = typeof(List<Filter>);
                    XmlSerializer serializer = new XmlSerializer(typeof(List<KeyValueFilterListEntity>), extraTypes);
                    List<KeyValueFilterListEntity> keyValueList = (List<KeyValueFilterListEntity>)serializer.Deserialize(xmlStream);

                    Dictionary<string, List<Filter>> gridFilters = new Dictionary<string, List<Filter>>();
                    foreach (KeyValueFilterListEntity keyValue in keyValueList)
                    {
                        gridFilters.Add(keyValue.Key, keyValue.FilterList);
                    }

                    UpdateFilter(gridFilters);
                }
            }
        }

        protected override bool SupportsSortingCore
        {
            get
            {
                return false;   // this is set to false sort that click on column header in data grid does not fire sorting.
            }
        }

        protected override bool IsSortedCore
        {
            get
            {
                return _sorted;
            }
        }

        protected override ListSortDirection SortDirectionCore
        {
            get
            {
                return _sortDirection;
            }
        }

        protected override PropertyDescriptor SortPropertyCore
        {
            get
            {
                return _sortProperty;
            }
        }

        public bool IsFiltered
        {
            get
            {
                return _filtered;
            }
        }

        #endregion

        #region Methods

        // this method must be used to add an item to the binding list -- DO NOT use Add()
        public void AddToList(T itemToAdd)
        {
            if (_originalCollection.Count > 0)      // _originalCollection is in use, so must Add the item
            {
                _originalCollection.Add(itemToAdd);
            }

            bool keepIt = true;
            if (_gridFilters != null)
            {
                foreach (string columnName in _gridFilters.Keys)
                {
                    List<Filter> anyColumnFilterList = _gridFilters[columnName];
                    foreach (Filter filter in anyColumnFilterList)
                    {
                        PropertyDescriptor propertyDescriptor = TypeDescriptor.GetProperties(typeof(T))[filter.PropertyName];
                        string propertyValue = propertyDescriptor.GetValue(itemToAdd).ToString();
                        if (!IsMatched(filter, propertyValue, itemToAdd))
                        {
                            keepIt = false;
                        }
                    }
                }
            }

            if (keepIt)
            {
                Add(itemToAdd);
                if (_sorted)
                {
                    ApplySort(_sortDescriptions);
                }
                else
                {
                    if (!_suspendOnListChangedEvent)
                    {
                        OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
                        OnUpdateSortGlyphs();
                    }
                }
            }
        }

        public void AddToList(object itemToAdd)
        {
            T typedItemToAdd;
            try
            {
                typedItemToAdd = (T)itemToAdd;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            AddToList(typedItemToAdd);
        }

        // this method must be used to remove an item from the binding list
        public void RemoveFromList(object itemToRemove)
        {
            T typedItemToRemove;
            try
            {
                typedItemToRemove = (T)itemToRemove;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            _originalCollection.Remove(typedItemToRemove);

            Remove(typedItemToRemove);

            if (!_suspendOnListChangedEvent)
            {
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
                OnUpdateSortGlyphs();
            }
        }

        // this method must be used to clear the binding list
        public void ClearList()
        {
            _originalCollection.Clear();
            Clear();
            OnUpdateSortGlyphs();
        }

        // this method must be used to test for an item contained in the list.
        public bool ContainedInList(T item)
        {
            return _originalCollection.Contains(item) || Contains(item);
        }

        public void BeginUpdate()
        {
            _suspendOnListChangedEvent = true;
        }

        public void EndUpdate()
        {
            _suspendOnListChangedEvent = false;
            OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
            OnUpdateSortGlyphs();
        }

        protected override void ApplySortCore(PropertyDescriptor prop, ListSortDirection direction)
        {
            ListSortDescription sortDescription = new ListSortDescription(prop, direction);
            List<ListSortDescription> listSortDescriptions = new List<ListSortDescription>();
            listSortDescriptions.Add(sortDescription);
            ListSortDescriptionCollection sortDescriptionCollection = new ListSortDescriptionCollection(listSortDescriptions.ToArray());
            _sortDescriptions = sortDescriptionCollection;
            ApplySort(sortDescriptionCollection);
        }

        public void ApplySort(ListSortDescriptionCollection sorts)
        {
            _sortDescriptions = sorts;
            SortComparer<T> comparer = new SortComparer<T>(sorts);
            ApplySortInternal(comparer);
        }

        // allow an initial sort on the binding list using a single property
        public void SortOnProperty(string PropertyName, ListSortDirection sortDirection)
        {
            if (this.Count == 0)
                return;

            PropertyDescriptorCollection propertyDescriptors = TypeDescriptor.GetProperties(this[0]);
            PropertyDescriptor propertyDescriptor = propertyDescriptors.Find(PropertyName, false);
            ApplySortCore(propertyDescriptor, sortDirection);
        }

        private void ApplySortInternal(SortComparer<T> comparer)
        {
            if (_originalCollection.Count == 0)
            {
                _originalCollection.AddRange(this);
            }
            List<T> listRef = this.Items as List<T>;
            if (listRef == null)
                return;

            // replace this sort with a call to LINQ to Objects?
            listRef.Sort(comparer);

            _sorted = true;

            if (!_suspendOnListChangedEvent)
            {
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
                OnUpdateSortGlyphs();
            }
        }

        protected override void RemoveSortCore()
        {
            if (!_sorted)
                return;

            Clear();
            foreach (T item in _originalCollection)
            {
                Add(item);
            }

            _originalCollection.Clear();
            _sortProperty = null;
            _sortDescriptions = null;
            _sorted = false;

            this.Filter=_filterXml;


            if (!_suspendOnListChangedEvent)
            {
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
                OnUpdateSortGlyphs();
            }
        }

        public void RemoveFilter()
        {
            if (!_filtered)
                return;

            _filterXml = null;
            _filtered = false;

            Clear();
            foreach (T item in _originalCollection)
            {
                Add(item);
            }

            _originalCollection.Clear();

            if (!_suspendOnListChangedEvent)
            {
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
                OnUpdateSortGlyphs();
            }
        }

        public int IndexOf(object anyHaiBusinessObject)
        {
            if (anyHaiBusinessObject is T)
                return base.IndexOf((T)anyHaiBusinessObject);
            else
                return -1;
        }

        public List<T> GetUnfilteredList()
        {
            List<T> theList;

            if (_originalCollection.Count == 0)
                theList = (List<T>)this.Items;
            else
                theList = _originalCollection;

            return theList;
        }

        public T GetItemWithPrimaryKey(int primaryKey)
        {
            List<T> allItems = GetUnfilteredList();
            foreach (T anyItem in allItems)
            {
                HaiBusinessObjectBase anyHBO = anyItem as HaiBusinessObjectBase;
                if (anyHBO.PrimaryKey == primaryKey)
                {
                    return anyItem;
                }
            }

            return default(T);
        }

        #endregion

        #region Private / Utilities

        // fire event so that subscribers know that sort glyphs (on the grid) need to be updated
        private void OnUpdateSortGlyphs()
        {
            if (UpdateSortGlyphs != null)
                UpdateSortGlyphs(this, new EventArgs());
        }

        // use the filtering specification to reduce the "currentCollection
        private void UpdateFilter(Dictionary<string, List<Filter>> gridFilters)
        {
            List<Filter> anyColumnFilterList;
            T testItem;
            bool IsListChanged = false;
            _gridFilters = gridFilters;

            if ((_gridFilters == null) || (_gridFilters.Count == 0))      // return if there are no filters
                return;

            foreach (string columnName in _gridFilters.Keys)
            {
                anyColumnFilterList = _gridFilters[columnName];
                foreach (Filter filter in anyColumnFilterList)
                {
                    if (!filter.IsApplied && (filter.MatchType != MatchType.Ignore))
                    {
                        List<T> currentCollection = new List<T>(this);
                        PropertyDescriptor propertyDescriptor = TypeDescriptor.GetProperties(typeof(T))[filter.PropertyName];
                        testItem = currentCollection[0];

                        if (filter.MatchType == MatchType.EqualTo)
                        {
                            foreach (string testingMatchValue in filter.MatchValueList)
                            {
                                // call CompareValues( ) to see if an error is thrown b/c of failure in "TryParse"
                                int testCompareValue;
                                if (filter.DataTypeCode != TypeCode.Empty)
                                    testCompareValue = CompareValues(filter.DataTypeCode, propertyDescriptor.GetValue(testItem), testingMatchValue);
                            }
                        }
                        else
                        {
                            // call CompareValues( ) to see if an error is thrown b/c of failure in "TryParse"
                            int testCompareValue;
                            if (filter.DataTypeCode != TypeCode.Empty)
                            {
                                string testingMatchValue = filter.MatchValueList[0];
                                testCompareValue = CompareValues(filter.DataTypeCode, propertyDescriptor.GetValue(testItem), testingMatchValue);
                            }
                        }
                         
                        if (_originalCollection.Count == 0)
                        {
                            _originalCollection.AddRange(this);
                        }

                        Clear();
                        IsListChanged = true;

                        foreach (T item in currentCollection)
                        {
                            object propertyValue = propertyDescriptor.GetValue(item);
                            if (IsMatched(filter, propertyValue, item))
                            {
                                Add(item);
                            }
                        }

                        _filtered = true;
                    }
                }
            }
            if (IsListChanged)
            {
                if (!_suspendOnListChangedEvent)
                {
                    OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
                    OnUpdateSortGlyphs();
                }
            }
        }

        // test an item to see if property value matches the filter
        private bool IsMatched(Filter filter, object propertyValue, T item)
        {
            bool keep;
            keep = false;
            int compareValue = 0;

            // handle mulls for match
            if ((propertyValue == null) || (filter.MatchValueList[0] == null))
            {
                if ((propertyValue == null) && (filter.MatchValueList[0] == null))
                    keep = true;    // both are nulls, so they match
                else
                    keep = false;   // only one is null, so there is no match
                return keep;
            }

            // there are no nulls, so compare the string values.
            // make case insensitive by converting to upper case
            string propertyValueAsString = propertyValue.ToString().ToUpper();
            string matchValueAsString = filter.MatchValueList[0].ToUpper();

            switch (filter.MatchType)
            {
                case MatchType.Ignore:
                    break;
                case MatchType.EqualTo:
                    List<string> matchValueList = filter.MatchValueList;
                    foreach (string aMatchValue in matchValueList)
                    {
                        if (propertyValueAsString.Equals(aMatchValue, StringComparison.OrdinalIgnoreCase))
                            keep = true;
                    }
                    break;
                case MatchType.BeginsWith: if (propertyValueAsString.StartsWith(matchValueAsString, StringComparison.OrdinalIgnoreCase)) keep = true;
                    break;
                case MatchType.EndsWith: if (propertyValueAsString.EndsWith(matchValueAsString, StringComparison.OrdinalIgnoreCase)) keep = true;
                    break;
                case MatchType.Contains: if (propertyValueAsString.Contains(matchValueAsString)) keep = true;
                    break;
                case MatchType.NotContains: if (!propertyValueAsString.Contains(matchValueAsString)) keep = true;
                    break;
                case MatchType.NotEqual: if (!propertyValueAsString.Equals(matchValueAsString, StringComparison.OrdinalIgnoreCase)) keep = true;
                    break;
                case MatchType.EQ:
                    compareValue = CompareValues(filter.DataTypeCode, propertyValueAsString, matchValueAsString);
                    if (compareValue == 0) keep = true;
                    break;
                case MatchType.GE:
                    compareValue = CompareValues(filter.DataTypeCode, propertyValueAsString, matchValueAsString);
                    if (compareValue > -1) keep = true;
                    break;
                case MatchType.GT:
                    compareValue = CompareValues(filter.DataTypeCode, propertyValueAsString, matchValueAsString);
                    if (compareValue == 1) keep = true;
                    break;
                case MatchType.LE:
                    compareValue = CompareValues(filter.DataTypeCode, propertyValueAsString, matchValueAsString);
                    if (compareValue < 1) keep = true;
                    break;
                case MatchType.LT:
                    compareValue = CompareValues(filter.DataTypeCode, propertyValueAsString, matchValueAsString);
                    if (compareValue == -1) keep = true;
                    break;
                case MatchType.NE:
                    compareValue = CompareValues(filter.DataTypeCode, propertyValueAsString, matchValueAsString);
                    if (compareValue != 0) keep = true;
                    break;
                default:
                    throw new Exception("Unknown ListFilterDescription.MatchType" + filter.MatchType.ToString());
            }
            return keep;
        }

        // method for comparing two values to see if a filter condition is met
        private int CompareValues(TypeCode dataTypeCode, object dataValue, object filterValue)
        {
            string dataValueAsString;
            string filterValueAsString;
            int compareResult = 0;
            bool filterOK = false;
            bool dataOK;

            // handle any null values
            if ((dataValue == null) || (filterValue == null))
            {
                if ((dataValue == null) && (filterValue == null))
                    compareResult = 0;          // both are null, treat as equal
                else
                {
                    if (dataValue == null)
                        compareResult = -1;     // data is null, so make compare "less than"
                    else
                        compareResult = 1;      // filter is null, so make compare "greater than"
                }
                return compareResult;
            }

            // comparison does not contain nulls; convert values to strings for comparison
            dataValueAsString = dataValue.ToString().ToUpper();
            filterValueAsString = filterValue.ToString().ToUpper();

            switch (dataTypeCode)
            {
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                    Int64 dataValueInt;
                    dataOK = Int64.TryParse(dataValueAsString, out dataValueInt);
                    Int64 filterValueInt;
                    filterOK = Int64.TryParse(filterValueAsString, out filterValueInt);
                    if (filterOK && dataOK)
                        compareResult = dataValueInt.CompareTo(filterValueInt);
                    break;

                case TypeCode.Boolean:
                    bool dataValueBool;
                    dataOK = Boolean.TryParse(dataValueAsString, out dataValueBool);
                    bool filterValueBool;
                    filterOK = Boolean.TryParse(filterValueAsString, out filterValueBool);
                    if (filterOK && dataOK)
                        compareResult = dataValueBool.CompareTo(filterValueBool);
                    break;

                case TypeCode.Char:
                case TypeCode.String:
                    compareResult = dataValueAsString.CompareTo(filterValueAsString);
                    filterOK = true;
                    break;

                case TypeCode.DateTime:
                    DateTime dataValueDate;
                    dataOK = DateTime.TryParse(dataValueAsString, out dataValueDate);
                    DateTime filterValueDate;
                    filterOK = DateTime.TryParse(filterValueAsString, out filterValueDate);
                    if (filterOK && dataOK)
                    {
                        dataValueDate = dataValueDate.Date;
                        filterValueDate = filterValueDate.Date;
                        compareResult = dataValueDate.Date.CompareTo(filterValueDate.Date);
                    }
                    break;

                case TypeCode.Empty:
                    if (filterValueAsString == dataValueAsString)
                        compareResult = 1;
                    else
                        compareResult = 0;
                    filterOK = true;
                    break;

                default:
                    throw new Exception("Unexpected DataTypeCode");
            }

            if (!filterOK)
            {
                throw new HaiDataCompareException(@"""" + filterValueAsString + @""" not valid for " + dataTypeCode.ToString());
            }

            return compareResult;
        }

        #endregion

        #region CSLA features


        /// <summary>
        /// Prevents clearing the collection.
        /// </summary>
        protected override void ClearItems()            // This is an OVERRIDE : NEEDED TO SUPPORT BindingList<T>
        {
            bool oldValue = AllowRemove;
            AllowRemove = true;
            base.ClearItems();
            AllowRemove = oldValue;
        }

        protected override void RemoveItem(int index)   // This is an OVERRIDE : NEEDED TO SUPPORT BindingList<T>
        {
            bool oldValue = AllowRemove;
            AllowRemove = true;
            base.RemoveItem(index);
            AllowRemove = oldValue;
        }

        #endregion
    }

    public class HaiDataCompareException : Exception
    {
        private string _message;
        public HaiDataCompareException(String message)
        {
            _message = message;
        }

        public override string  Message
        {
            get
            {
                return _message;
            }
        }
    }
}

