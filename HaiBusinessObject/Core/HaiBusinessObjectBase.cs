using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;

using System.Data.Linq;
using System.Xml.Serialization;

namespace HaiBusinessObject
{
    abstract public class HaiBusinessObjectBase : UndoableBase, IEditableObject, IDataErrorInfo, IEditableBusinessObject
        //ICloneable
    {
        [NotUndoable()]
        private bool _bindingEdit;

        public event PropertyChangedEventHandler PropertyChanged;

        #region Properties

        protected HaiBusinessObjectType _haiBusinessObjectType;
        [Browsable(false)]
        public HaiBusinessObjectType ObjectType
        {
            get
            {
                return _haiBusinessObjectType;
            }
        }

        [Browsable(false)]
        public abstract int PrimaryKey
        {
            get;
            set;
        }

        [Browsable(false)]
        public abstract string UniqueIdentifier
        {
            get;
        }

        protected string _userName;
        [Browsable(false)]
        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        protected string _applicationContext;
        [Browsable(false)]
        public string ApplicationContext
        {
            get
            {
                return _applicationContext;
            }
            set
            {
                _applicationContext = value;
            }
        }

        protected Binary _rowVersion;
        [Browsable(false)]
        [XmlIgnore]
        public Binary RowVersion
        {
            get
            {
                return _rowVersion;
            }
            set
            {
                _rowVersion = value;
            }
        }

        #endregion

        #region Constructors

        protected HaiBusinessObjectBase()
        {
          Initialize();

          AddBusinessRules();
        }

        #endregion

        #region Initialize

        /// <summary>
        /// Override this method to set up event handlers so user
        /// code in a partial class can respond to events raised by
        /// generated code.
        /// </summary>
        protected virtual void Initialize()
        { /* allows subclass to initialize events before any other activity occurs */
            _haiBusinessObjectType = HaiBusinessObjectType.UnknownHaiBusinessObjectType;
        }

        #endregion

        #region Methods

        public abstract Dictionary<string, ReportSpecification> GetReportSpecificationDict();

        public abstract ReportSpecification GetDefaultReportSpecification();

        public abstract Dictionary<string, BrowsableProperty> GetClassBrowsablePropertyList();

        // helper method to eliminate code redundancy in the derived classes
        protected ReportSpecification GetDefaultReportSpecification(Dictionary<string, ReportSpecification> reportSpecificationList)
        {
            // no reports defined; return null
            if (reportSpecificationList.Count == 0)
                return null;

            // there is a Default report, return it
            if (reportSpecificationList.ContainsKey("Default"))
                return reportSpecificationList["Default"];

            // no Default report, return first report found
            List<string> keyList = new List<string>(reportSpecificationList.Keys);
            return reportSpecificationList[keyList[0]];
        }

        #endregion

        #region System.ComponentModel.IEditableObject

        private bool _neverCommitted = true;

        /// <summary>
        /// Allow data binding to start a nested edit on the object.
        /// </summary>
        /// <remarks>
        /// Data binding may call this method many times. Only the first
        /// call should be honored, so we have extra code to detect this
        /// and do nothing for subsquent calls.
        /// </remarks>
        void System.ComponentModel.IEditableObject.BeginEdit()
        {
            if (!_bindingEdit)
                BeginEdit();
        }

        /// <summary>
        /// Allow data binding to cancel the current edit.
        /// </summary>
        /// <remarks>
        /// Data binding may call this method many times. Only the first
        /// call to either IEditableObject.CancelEdit or 
        /// IEditableObject.EndEdit
        /// should be honored. We include extra code to detect this and do
        /// nothing for subsequent calls.
        /// </remarks>
        void System.ComponentModel.IEditableObject.CancelEdit()
        {
            if (_bindingEdit)
            {
                CancelEdit();
                if (IsNew && _neverCommitted && EditLevel <= EditLevelAdded)
                {
                    // we're new and no EndEdit or ApplyEdit has ever been
                    // called on us, and now we've been cancelled back to
                    // where we were added so we should have ourselves
                    // removed from the parent collection
                    if (Parent != null)
                        Parent.RemoveChild(this);
                }
            }
        }

        /// <summary>
        /// Allow data binding to apply the current edit.
        /// </summary>
        /// <remarks>
        /// Data binding may call this method many times. Only the first
        /// call to either IEditableObject.EndEdit or 
        /// IEditableObject.CancelEdit
        /// should be honored. We include extra code to detect this and do
        /// nothing for subsequent calls.
        /// </remarks>
        void System.ComponentModel.IEditableObject.EndEdit()
        {
            if (_bindingEdit)
                ApplyEdit();
        }

        #endregion

        #region Edit Level Tracking (child only)

        // we need to keep track of the edit
        // level when we weere added so if the user
        // cancels below that level we can be destroyed
        private int _editLevelAdded;

        /// <summary>
        /// Gets or sets the current edit level of the
        /// object.
        /// </summary>
        /// <remarks>
        /// Allow the collection object to use the
        /// edit level as needed.
        /// </remarks>
        internal int EditLevelAdded
        {
            get
            {
                return _editLevelAdded;
            }
            set
            {
                _editLevelAdded = value;
            }
        }

        int IEditableBusinessObject.EditLevel
        {
            get
            {
                return this.EditLevel;
            }
        }

        #endregion

        #region Begin/Cancel/ApplyEdit

        /// <summary>
        /// Starts a nested edit on the object.
        /// </summary>
        /// <remarks>
        /// <para>
        /// When this method is called the object takes a snapshot of
        /// its current state (the values of its variables). This snapshot
        /// can be restored by calling CancelEdit
        /// or committed by calling ApplyEdit.
        /// </para><para>
        /// This is a nested operation. Each call to BeginEdit adds a new
        /// snapshot of the object's state to a stack. You should ensure that 
        /// for each call to BeginEdit there is a corresponding call to either 
        /// CancelEdit or ApplyEdit to remove that snapshot from the stack.
        /// </para><para>
        /// See Chapters 2 and 3 for details on n-level undo and state stacking.
        /// </para>
        /// </remarks>
        public void BeginEdit()
        {
            _bindingEdit = true;
            CopyState();
        }

        /// <summary>
        /// Cancels the current edit process, restoring the object's state to
        /// its previous values.
        /// </summary>
        /// <remarks>
        /// Calling this method causes the most recently taken snapshot of the 
        /// object's state to be restored. This resets the object's values
        /// to the point of the last BeginEdit call.
        /// </remarks>
        public void CancelEdit()
        {
            UndoChanges();
        }

        /// <summary>
        /// Commits the current edit process.
        /// </summary>
        /// <remarks>
        /// Calling this method causes the most recently taken snapshot of the 
        /// object's state to be discarded, thus committing any changes made
        /// to the object's state since the last BeginEdit call.
        /// </remarks>
        public void ApplyEdit()
        {
            _bindingEdit = false;
            _neverCommitted = false;
            AcceptChanges();
        }

        /// <summary>
        /// Called when an undo operation has completed.
        /// </summary>
        /// <remarks>
        /// This method resets the object as a result of
        /// deserialization and raises PropertyChanged events
        /// to notify data binding that the object has changed.
        /// </remarks>
        protected override void UndoChangesComplete()
        {
            _bindingEdit = false;
            ValidationRules.SetTarget(this);

            /* Removed b/c not using InstanceBusinessRules or SharedValidation Rules at this time */
            //AddInstanceBusinessRules();
            //if (!Validation.SharedValidationRules.RulesExistFor(this.GetType()))
            //{
            //    lock (this.GetType())
            //    {
            //        if (!Validation.SharedValidationRules.RulesExistFor(this.GetType()))
            //            AddBusinessRules();
            //    }
            //}


            OnUnknownPropertyChanged();
            base.UndoChangesComplete();
        }

        /// <summary>
        /// Notifies the parent object (if any) that this
        /// child object's edits have been accepted.
        /// </summary>
        protected override void AcceptChangesComplete()
        {
            if (Parent != null)
                Parent.ApplyEditChild(this);
            base.AcceptChangesComplete();
        }

        #endregion

        #region IsNew, IsDeleted, IsDirty

        private bool _isNew = true;
        private bool _isDeleted;
        private bool _isDirty = true;

        /// <summary>
        /// Returns <see langword="true" /> if this is a new object, 
        /// <see langword="false" /> if it is a pre-existing object.
        /// </summary>
        /// <remarks>
        /// An object is considered to be new if its primary identifying (key) value 
        /// doesn't correspond to data in the database. In other words, 
        /// if the data values in this particular
        /// object have not yet been saved to the database the object is considered to
        /// be new. Likewise, if the object's data has been deleted from the database
        /// then the object is considered to be new.
        /// </remarks>
        /// <returns>A value indicating if this object is new.</returns>
        [Browsable(false)]
        public bool IsNew
        {
            get
            {
                return _isNew;
            }
        }

        /// <summary>
        /// Returns <see langword="true" /> if this object is marked for deletion.
        /// </summary>
        /// <remarks>
        /// CSLA .NET supports both immediate and deferred deletion of objects. This
        /// property is part of the support for deferred deletion, where an object
        /// can be marked for deletion, but isn't actually deleted until the object
        /// is saved to the database. This property indicates whether or not the
        /// current object has been marked for deletion. If it is <see langword="true" />
        /// , the object will
        /// be deleted when it is saved to the database, otherwise it will be inserted
        /// or updated by the save operation.
        /// </remarks>
        /// <returns>A value indicating if this object is marked for deletion.</returns>
        [Browsable(false)]
        public bool IsDeleted
        {
            get
            {
                return _isDeleted;
            }
        }

        /// <summary>
        /// Returns <see langword="true" /> if this object's data has been changed.
        /// </summary>
        /// <remarks>
        /// <para>
        /// When an object's data is changed, CSLA .NET makes note of that change
        /// and considers the object to be 'dirty' or changed. This value is used to
        /// optimize data updates, since an unchanged object does not need to be
        /// updated into the database. All new objects are considered dirty. All objects
        /// marked for deletion are considered dirty.
        /// </para><para>
        /// Once an object's data has been saved to the database (inserted or updated)
        /// the dirty flag is cleared and the object is considered unchanged. Objects
        /// newly loaded from the database are also considered unchanged.
        /// </para>
        /// </remarks>
        /// <returns>A value indicating if this object's data has been changed.</returns>
        [Browsable(false)]
        public virtual bool IsDirty
        {
            get
            {
                return _isDirty;
            }
        }

        /// <summary>
        /// Returns <see langword="true" /> if this object is both dirty and valid.
        /// </summary>
        /// <remarks>
        /// An object is considered dirty (changed) if 
        /// <see cref="P:Csla.BusinessBase.IsDirty" /> returns <see langword="true" />. It is
        /// considered valid if IsValid
        /// returns <see langword="true" />. The IsSavable property is
        /// a combination of these two properties. 
        /// </remarks>
        /// <returns>A value indicating if this object is both dirty and valid.</returns>
        [Browsable(false)]
        public virtual bool IsSavable
        {
            get
            {
                return (IsDirty && IsValid);
            }
        }

        /// <summary>
        /// Marks the object as being a new object. This also marks the object
        /// as being dirty and ensures that it is not marked for deletion.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Newly created objects are marked new by default. You should call
        /// this method in the implementation of DataPortal_Update when the
        /// object is deleted (due to being marked for deletion) to indicate
        /// that the object no longer reflects data in the database.
        /// </para><para>
        /// If you override this method, make sure to call the base
        /// implementation after executing your new code.
        /// </para>
        /// </remarks>
        public virtual void MarkNew()
        {
            _isNew = true;
            _isDeleted = false;
            MarkDirty();
        }

        /// <summary>
        /// Marks the object as being an old (not new) object. This also
        /// marks the object as being unchanged (not dirty).
        /// </summary>
        /// <remarks>
        /// <para>
        /// You should call this method in the implementation of
        /// DataPortal_Fetch to indicate that an existing object has been
        /// successfully retrieved from the database.
        /// </para><para>
        /// You should call this method in the implementation of 
        /// DataPortal_Update to indicate that a new object has been successfully
        /// inserted into the database.
        /// </para><para>
        /// If you override this method, make sure to call the base
        /// implementation after executing your new code.
        /// </para>
        /// </remarks>
        public virtual void MarkOld()
        {
            _isNew = false;
            MarkClean();
        }

        /// <summary>
        /// Forces the object's IsDirty flag to <see langword="false" />.
        /// </summary>
        /// <remarks>
        /// This method is normally called automatically and is
        /// not intended to be called manually.
        /// </remarks>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void MarkClean()
        {
            _isDirty = false;
            _isDeleted = false;
            OnUnknownPropertyChanged();
        }

        /// <summary>
        /// Marks an object for deletion. This also marks the object
        /// as being dirty.
        /// </summary>
        /// <remarks>
        /// You should call this method in your business logic in the
        /// case that you want to have the object deleted when it is
        /// saved to the database.
        /// </remarks>
        public void MarkDeleted()
        {
            _isDeleted = true;
            MarkDirty();
        }

        /// <summary>
        /// Marks an object as being dirty, or changed.
        /// </summary>
        /// <remarks>
        /// <para>
        /// You should call this method in your business logic any time
        /// the object's internal data changes. Any time any instance
        /// variable changes within the object, this method should be called
        /// to tell CSLA .NET that the object's data has been changed.
        /// </para><para>
        /// Marking an object as dirty does two things. First it ensures
        /// that CSLA .NET will properly save the object as appropriate. Second,
        /// it causes CSLA .NET to tell Windows Forms data binding that the
        /// object's data has changed so any bound controls will update to
        /// reflect the new values.
        /// </para>
        /// </remarks>
        public void MarkDirty()
        {
            MarkDirty(false);
        }

        /// <summary>
        /// Marks an object as being dirty, or changed.
        /// </summary>
        /// <param name="supressEvent">
        /// <see langword="true" /> to supress the PropertyChanged event that is otherwise
        /// raised to indicate that the object's state has changed.
        /// </param>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void MarkDirty(bool suppressEvent)
        {
            _isDirty = true;
            if (!suppressEvent)
                OnUnknownPropertyChanged();
        }

        #endregion

        #region IsChild

        [NotUndoable()]
        private bool _isChild;

        /// <summary>
        /// Returns <see langword="true" /> if this is a child (non-root) object.
        /// </summary>
        protected internal bool IsChild
        {
            get
            {
                return _isChild;
            }
        }

        /// <summary>
        /// Marks the object as being a child object.
        /// </summary>
        protected void MarkAsChild()
        {
            _isChild = true;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Marks the object for deletion. The object will be deleted as part of the
        /// next save operation.
        /// </summary>
        /// <remarks>
        /// <para>
        /// CSLA .NET supports both immediate and deferred deletion of objects. This
        /// method is part of the support for deferred deletion, where an object
        /// can be marked for deletion, but isn't actually deleted until the object
        /// is saved to the database. This method is called by the UI developer to
        /// mark the object for deletion.
        /// </para><para>
        /// To 'undelete' an object, use n-level undo as discussed in Chapters 2 and 3.
        /// </para>
        /// </remarks>
        public virtual void Delete()
        {
            if (this.IsChild)
                throw new NotSupportedException("Delete() failed.  Object is a child.  DeleteChild() must be used.");

            MarkDeleted();
        }

        /// <summary>
        /// Called by a parent object to mark the child
        /// for deferred deletion.
        /// </summary>
        internal void DeleteChild()
        {
            if (!this.IsChild)
                throw new NotSupportedException("DeleteChild() failed.  Object is not a child");

            MarkDeleted();
        }

        #endregion

        #region Parent/Child link

        [NotUndoable()]
        [NonSerialized()]
        private IParent _parent;

        /// <summary>
        /// Provide access to the parent reference for use
        /// in child object code.
        /// </summary>
        /// <remarks>
        /// This value will be Nothing for root objects.
        /// </remarks>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        protected IParent Parent
        {
            get
            {
                return _parent;
            }
        }

        /// <summary>
        /// Used by BusinessListBase as a child object is 
        /// created to tell the child object about its
        /// parent.
        /// </summary>
        /// <param name="parent">A reference to the parent collection object.</param>
        internal void SetParent(IParent parent)
        {
            _parent = parent;
        }

        #endregion


        /// <summary>
        /// Provides access to the broken rules functionality.
        /// </summary>
        /// <remarks>
        /// This property is used within your business logic so you can
        /// easily call the AddRule() method to associate validation
        /// rules with your object's properties.
        /// </remarks>
        private HaiBusinessObject.ValidationRules _validationRules;
        [Browsable(false)]
        public HaiBusinessObject.ValidationRules ValidationRules
        {
            get
            {
                if (_validationRules == null)
                    _validationRules = new HaiBusinessObject.ValidationRules(this);
                return _validationRules;
            }
        }

        /// <summary>
        /// Override this method in your business class to
        /// be notified when you need to set up shared 
        /// business rules.
        /// </summary>
        /// <remarks>
        /// This method is automatically called by CSLA .NET
        /// when your object should associate per-type 
        /// validation rules with its properties.
        /// </remarks>
        protected virtual void AddBusinessRules()
        {

        }

        /// <summary>
        /// Returns <see langword="true" /> if the object is currently valid, <see langword="false" /> if the
        /// object has broken rules or is otherwise invalid.
        /// </summary>
        /// <remarks>
        /// <para>
        /// By default this property relies on the underling ValidationRules
        /// object to track whether any business rules are currently broken for this object.
        /// </para><para>
        /// You can override this property to provide more sophisticated
        /// implementations of the behavior. For instance, you should always override
        /// this method if your object has child objects, since the validity of this object
        /// is affected by the validity of all child objects.
        /// </para>
        /// </remarks>
        /// <returns>A value indicating if the object is currently valid.</returns>
        [Browsable(false)]
        public virtual bool IsValid
        {
            get
            {
                return ValidationRules.IsValid;
            }
        }

        protected virtual void OnUnknownPropertyChanged()
        {
            PropertyInfo[] properties = this.GetType().GetProperties();
            foreach (PropertyInfo item in properties )
            {
                OnPropertyChanged(item.Name);
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,new PropertyChangedEventArgs(propertyName));
            }
        }

        #region IDataErrorInfo

        string IDataErrorInfo.Error
        {
            get
            {
                if (!IsValid)
                    return ValidationRules.GetBrokenRules().ToString();
                else
                    return String.Empty;
            }
        }

        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                string result = string.Empty;
                if (!IsValid)
                {
                    BrokenRule rule = ValidationRules.GetBrokenRules().GetFirstBrokenRule(columnName);
                    if (rule != null)
                        result = rule.Description;
                }
                return result;
            }
        }

        #endregion

        #region IEditableBusinessObject Members

        int IEditableBusinessObject.EditLevelAdded
        {
            get
            {
                return this.EditLevelAdded;
            }
            set
            {
                this.EditLevelAdded = value;
            }
        }

        void IEditableBusinessObject.DeleteChild()
        {
            this.DeleteChild();
        }

        void IEditableBusinessObject.SetParent(IParent parent)
        {
            this.SetParent(parent);
        }

        #endregion

    }
}
