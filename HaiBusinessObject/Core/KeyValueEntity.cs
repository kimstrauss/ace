using System;
using System.Collections.Generic;
using System.Text;

namespace HaiBusinessObject.Core
{
    [Serializable]
    public class KeyValueFilterListEntity
    {
        private string _key;
        public string Key
        {
            get
            {
                return _key;
            }
            set
            {
                _key = value;
            }
        }

        private List<Filter> _filterList;
        public List<Filter> FilterList
        {
            get
            {
                return _filterList;
            }
            set
            {
                _filterList = value;
            }
        }
    }
}
