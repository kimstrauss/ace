using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel;

namespace HaiBusinessObject
{
    /* Compare procedures to support sorting in HaiBindingList.  Adapted from Brian Noyes */

    class SortComparer<T> : IComparer<T>
    {
        private ListSortDescriptionCollection _sortCollection = null;
        private PropertyDescriptor _propDesc = null;
        private ListSortDirection _direction = ListSortDirection.Ascending;

        public SortComparer(PropertyDescriptor propDesc, ListSortDirection direction)
        {
            _propDesc = propDesc;
            _direction = direction;
        }

        public SortComparer(ListSortDescriptionCollection sortCollection)
        {
            _sortCollection = sortCollection;
        }

        // implement the IComparer interface
        int IComparer<T>.Compare(T x, T y)
        {
            if (_propDesc != null)          // simple sort
            {
                object xValue = _propDesc.GetValue(x);
                object yValue = _propDesc.GetValue(y);
                return CompareValues(xValue, yValue, _direction);
            }
            else if ((_sortCollection != null) && (_sortCollection.Count > 0))
            {
                return RecursiveCompareInternal(x, y, 0);
            }
            else
                return 0;
        }

        // sort on multiple criteria
        private int RecursiveCompareInternal(T x, T y, int index)
        {
            if (index >= _sortCollection.Count)
                return 0;                           // no more sort criteria, compare as Equal

            ListSortDescription listSortDesc = _sortCollection[index];
            object xValue = listSortDesc.PropertyDescriptor.GetValue(x);
            object yValue = listSortDesc.PropertyDescriptor.GetValue(y);

            int retValue = CompareValues(xValue, yValue, listSortDesc.SortDirection);
            if (retValue == 0)                      // items compare as Equal, call recursively with index for next for next test
            {
                return RecursiveCompareInternal(x, y, ++index);
            }
            else                                    // items are not the same, comparison is done; return comparison value
            {
                return retValue;
            }
        }

        // compare two values; handle nulls in comparison
        private int CompareValues(object xValue, object yValue, ListSortDirection direction)
        {
            int retValue = 0;

            if ((xValue == null) || (yValue == null))       // one, or both, values null
            {
                if ((xValue == null) && (yValue == null))   // both are null, equality
                    retValue = 0;
                else                                        // ... null values compare as "less than" non-null
                {
                    if (xValue == null)
                        retValue = -1;
                    else
                        retValue = 1;
                }
            }
            else                                            // no nulls, so do a "real" comparison
            {
                try
                {
                    if (xValue is IComparable)                          // appeal to object's compare method
                        retValue = ((IComparable)xValue).CompareTo(yValue);
                    else if (yValue is IComparable)                     // appeal to object's compare method
                        retValue = ((IComparable)yValue).CompareTo(xValue);
                    else if (!xValue.Equals(yValue))                    // compare by name if objects not "equal"
                        retValue = xValue.ToString().CompareTo(yValue.ToString());
                }
                catch
                {
                    throw;
                }
            }

            if (direction == ListSortDirection.Ascending)       // set the sense of the comparison
                return retValue;
            else
                return retValue * -1;
        }
    }
}
