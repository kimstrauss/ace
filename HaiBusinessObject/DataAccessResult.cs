using System.Collections.Generic;
using System.ComponentModel;
using HaiBusinessObject;

namespace HaiBusinessObject
{
    public class DataAccessResult
    //public struct DataAccessResult
    {
        // Constructor for Class -- cannot be used for Struct
        public DataAccessResult()
        {
            Success = false;
            Message = string.Empty;
            ItemIsChanged = false;
        }

        public IBindingListView DataList
        {
            get;
            set;
        }

        public HaiBusinessObjectBase SingleItem
        {
            get
            {
                return (HaiBusinessObjectBase)DataList[0];
            }
        }

        public bool ItemIsChanged
        {
            get;
            set;
        }

        public bool Success
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public Dictionary<string, BrowsableProperty> BrowsablePropertyList
        {
            get;
            set;
        }

        public IBindingListView PassBackDataList
        {
            get;
            set;
        }
    }
}
