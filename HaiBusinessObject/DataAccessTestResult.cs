﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HaiBusinessObject
{
    public class DataAccessTestResult
    {
        public DataAccessTestResult()
        {
            this.Success = false;
            this.Passed = false;
            this.ObjectName = string.Empty;
            this.Message = string.Empty;
        }

        public bool Success
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public bool Passed
        {
            get;
            set;
        }

        public string ObjectName
        {
            get;
            set;
        }
    }
}
