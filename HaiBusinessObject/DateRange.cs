﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace HaiBusinessObject
{
    [Serializable]
    public enum DateRangeValidationType
    {
        DoesNotApply,
        Daily,
        Weekly,
        Monthly,
        Annual
    }

    [Serializable]
    public class DateRange : IComparable
    {
        protected static  Dictionary<string, BrowsableProperty> _browsablePropertyList;

        private System.DateTime mBeginDate;
        private System.DateTime mEndDate;

        public DateRange()
        {
            mBeginDate = DateTime.MinValue.Date;
            mEndDate = DateTime.MaxValue.Date;
        }

        public DateRange(DateTime BeginDate, DateTime EndDate)
        {
            mBeginDate = BeginDate.Date;
            mEndDate = EndDate.Date;
        }

        public void SetPropertiesUsing(DateRange SourceDateRange)
        {
            mBeginDate = SourceDateRange.BeginDate;
            mEndDate = SourceDateRange.EndDate;
        }

        public DateRange HasOverlapWithList(ref List<DateRange> DateList) 
        { 
            DateRange OverlappingDateRange = null; 
            
            foreach (DateRange AnyDateRange in DateList) { 
                if (this.OverlapsRange(AnyDateRange)) { 
                    OverlappingDateRange = AnyDateRange; 
                    break; 
                } 
            } 
            
            return OverlappingDateRange; 
            
        }

        public bool OverlapsRange(DateRange TestDateRange)
        {
            if (TestDateRange.EndDate < mBeginDate)
                return false;

            if (TestDateRange.BeginDate > mEndDate)
                return false;

            return true;
        }

        int IComparable.CompareTo(object DateRange)
        {

            DateRange OtherDateRange;

            OtherDateRange = (DateRange)DateRange;

            if (mBeginDate.Date > OtherDateRange.EndDate.Date)
            {
                return -1;
            }

            if (mEndDate.Date < OtherDateRange.BeginDate.Date)
            {
                return 1;

            }

            return 0;

        }

        public DateTime BeginDate
        {
            get
            {
                return mBeginDate;
            }
            set
            {
                mBeginDate = value.Date;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return mEndDate;
            }
            set
            {
                mEndDate = value.Date;
            }
        }

        public bool RangeIsCurrent()
        {
            System.DateTime ForDate;
            ForDate = System.DateTime.Today.Date;
            return RangeIsCurrent(ForDate);
        }

        public bool RangeIsCurrent(System.DateTime ForDate)
        {
            if ((mBeginDate.Date <= ForDate.Date) & (mEndDate.Date >= ForDate.Date))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsSet()
        {
            if ((mBeginDate.Date == Utilities.GetOpenBeginDate() | mBeginDate.Date == Utilities.GetOpenEndDate()) & (mEndDate.Date == Utilities.GetOpenEndDate() | mEndDate.Date == Utilities.GetOpenBeginDate()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool IsValid
        {
            get
            {
                return (mBeginDate <= mEndDate);
            }
        }

        public string ValidateRangeForType(DateRangeValidationType type)
        {
            string message = string.Empty;

            if (!IsValid)
                message += "Reverse chronology; ";

            switch (type)
            {
                case DateRangeValidationType.Daily:
                    break;

                case DateRangeValidationType.Weekly:
                    if (!((mBeginDate.DayOfWeek == DayOfWeek.Sunday) || (mBeginDate.Day == 1 && mBeginDate.Month == 1)) && (mBeginDate != Utilities.GetOpenBeginDate()))
                        message += "Begin date not a Sunday or Jan 1; ";
                    if (!((mEndDate.DayOfWeek == DayOfWeek.Saturday) || (mEndDate.Day == 31 && mEndDate.Month == 12)) && (mEndDate != Utilities.GetOpenEndDate()))
                        message += "End date not a Saturday or Dec 31; ";
                    break;

                case DateRangeValidationType.Monthly:
                    if ((mBeginDate.Day != 1) && (mBeginDate != Utilities.GetOpenBeginDate()))
                        message += "Begin date not the first day of the month; ";
                    if ((mEndDate.Day != Utilities.MonthEnd(mEndDate).Day) && (mEndDate != Utilities.GetOpenEndDate()))
                        message += "End date not the last day of the month; ";
                    break;

                case DateRangeValidationType.Annual:
                    if ((mBeginDate.Month != 1 || mBeginDate.Day != 1) && (mBeginDate != Utilities.GetOpenBeginDate()))
                        message += "Begin date not January 1; ";
                    if ((mEndDate.Month != 12 || mEndDate.Day != 31) && (mEndDate != Utilities.GetOpenEndDate()))
                        message += "End date not December 31; ";
                    break;

                default:
                    throw new Exception("Unknown DateRangeValidationType");
            }

            return message;
        }

        public string ValidateBeginDateForType(DateRangeValidationType type)
        {
            string message = string.Empty;

            switch (type)
            {
                case DateRangeValidationType.Daily:
                    break;

                case DateRangeValidationType.Weekly:
                    if (!((mBeginDate.DayOfWeek == DayOfWeek.Sunday) || (mBeginDate.Day == 1 && mBeginDate.Month == 1)))
                        message += "Begin date not a Sunday or Jan 1; ";
                    break;

                case DateRangeValidationType.Monthly:
                    if (mBeginDate.Day != 1)
                        message += "Begin date not the first day of the month; ";
                    break;

                case DateRangeValidationType.Annual:
                    if (mBeginDate.Month != 1 && mBeginDate.Day != 1)
                        message += "Begin date not January 1; ";
                    break;

                default:
                    throw new Exception("Unknown DateRangeValidationType");
            }

            return message;
        }

        public string ValidateEndDateForType(DateRangeValidationType type)
        {
            string message = string.Empty;

            switch (type)
            {
                case DateRangeValidationType.Daily:
                    break;

                case DateRangeValidationType.Weekly:
                    if (!((mEndDate.DayOfWeek == DayOfWeek.Saturday) || (mBeginDate.Day == 31 && mBeginDate.Month == 12)))
                        message += "End date not a Saturday or Dec 31; ";
                    break;

                case DateRangeValidationType.Monthly:
                    if (mEndDate.Day != Utilities.MonthEnd(mEndDate).Day)
                        message += "End date not the last day of the month; ";
                    break;

                case DateRangeValidationType.Annual:
                    if (mEndDate.Month != 12 && mEndDate.Day != 31)
                        message += "End date not December 31; ";
                    break;

                default:
                    throw new Exception("Unknown DateRangeValidationType");
            }

            return message;
        }

        public override string ToString() 
        { 
            string Text; 
            
            // no dates set
            if (!IsSet()) { 
                return "no dates"; 
            } 
            
            // format BeginDate
            if (mBeginDate.Date == Utilities.GetOpenBeginDate()) 
            { 
                Text = "(open) - "; 
            } 
            else { 
                Text = mBeginDate.ToShortDateString() + " - ";
            } 
            
            // format EndDate
            if (mEndDate.Date == Utilities.GetOpenEndDate()) 
            { 
                Text += "(open)"; 
            } 
            else {
                Text += mEndDate.ToShortDateString();
            } 
            
            return Text; 
        }

    }
}
