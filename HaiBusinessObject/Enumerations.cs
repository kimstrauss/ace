using System;
using System.Collections.Generic;
using System.Text;

namespace HaiBusinessObject
{
    public enum Language
    {
        English,
        Spanish,
        Russian
    }

    public enum HaiDomainEnumerationBase
    {
        AHAM =      10000,
        OPEI =      20000,
        ISTAT =     30000
    }

    //public enum DatabaseContext
    //{
    //    AHAM = 1,
    //    OPEI = 2
    //}

    public enum HaiBusinessObjectType
    {
        UnknownHaiBusinessObjectType = 0,


        // ------------- AHAM ------------
        AnyAhamObject =                 0 + HaiDomainEnumerationBase.AHAM,          // Aham

        Activity_Aham =                 1 + HaiDomainEnumerationBase.AHAM,          // Aham
        ActivitySet_Aham =              2 + HaiDomainEnumerationBase.AHAM,          // Aham
        ActivitySetMember_Aham =        3 + HaiDomainEnumerationBase.AHAM,          // Aham

        Catalog =                       4 + HaiDomainEnumerationBase.AHAM,          // Aham
        CatalogSet =                    5 + HaiDomainEnumerationBase.AHAM,          // Aham
        CatalogSetMember =              6 + HaiDomainEnumerationBase.AHAM,          // Aham

        Channel =                       7 + HaiDomainEnumerationBase.AHAM,          // Aham
        ChannelSet =                    8 + HaiDomainEnumerationBase.AHAM,          // Aham
        ChannelSetMember =              9 + HaiDomainEnumerationBase.AHAM,          // Aham

        Council =                       10 + HaiDomainEnumerationBase.AHAM,         // Aham

        ExpansionFactor =               11 + HaiDomainEnumerationBase.AHAM,         // Aham

        Geography =                     12 + HaiDomainEnumerationBase.AHAM,         // Aham
        GeographySet =                  13 + HaiDomainEnumerationBase.AHAM,         // Aham
        GeographySetMember =            14 + HaiDomainEnumerationBase.AHAM,         // Aham

        Group =                         15 + HaiDomainEnumerationBase.AHAM,         // Aham
        GroupMember =                   16 + HaiDomainEnumerationBase.AHAM,         // Aham

        Market =                        17 + HaiDomainEnumerationBase.AHAM,         // Aham
        MarketSet =                     18 + HaiDomainEnumerationBase.AHAM,         // Aham
        MarketSetMember =               19 + HaiDomainEnumerationBase.AHAM,         // Aham

        Measure =                       20 + HaiDomainEnumerationBase.AHAM,         // Aham
        MeasureSet =                    21 + HaiDomainEnumerationBase.AHAM,         // Aham
        MeasureSetMember =              22 + HaiDomainEnumerationBase.AHAM,         // Aham

        Product_Aham =                  23 + HaiDomainEnumerationBase.AHAM,         // Aham
        ProductSet_Aham =               24 + HaiDomainEnumerationBase.AHAM,         // Aham
        ProductSetMember_Aham =         25 + HaiDomainEnumerationBase.AHAM,         // Aham

        Use =                           26 + HaiDomainEnumerationBase.AHAM,         // Aham
        UseSet =                        27 + HaiDomainEnumerationBase.AHAM,         // Aham
        UseSetMember =                  28 + HaiDomainEnumerationBase.AHAM,         // Aham

        Size =                          29 + HaiDomainEnumerationBase.AHAM,         // Aham
        SizeSet =                       30 + HaiDomainEnumerationBase.AHAM,         // Aham
        SizeSetMember =                 31 + HaiDomainEnumerationBase.AHAM,         // Aham

        CellDisclosure =                32 + HaiDomainEnumerationBase.AHAM,         // Aham
        IndustryReportMember =          33 + HaiDomainEnumerationBase.AHAM,         // Aham
        OutputReport =                  34 + HaiDomainEnumerationBase.AHAM,         // Aham
        Footnote =                      35 + HaiDomainEnumerationBase.AHAM,         // Aham

        HolidayDate =                   36 + HaiDomainEnumerationBase.AHAM,         // Aham

        ReleaseException =              37 + HaiDomainEnumerationBase.AHAM,         // Aham

        ContactRoles =                  38 + HaiDomainEnumerationBase.AHAM,         // Aham

        Association =                   39 + HaiDomainEnumerationBase.AHAM,         // Aham
        
        Manufacturer =                  40 + HaiDomainEnumerationBase.AHAM,         // Aham

        FSList      =                   41 + HaiDomainEnumerationBase.AHAM,         // Aham

        ManufacturerBrand =             42 + HaiDomainEnumerationBase.AHAM,         // Aham

        Frequency =                     43 + HaiDomainEnumerationBase.AHAM,         // Aham

        InputForm =                     44 + HaiDomainEnumerationBase.AHAM,         // Aham

        ManufacturerReport =            45 + HaiDomainEnumerationBase.AHAM,         // Aham

        Dimension =                     46 + HaiDomainEnumerationBase.AHAM,         // Aham

        ProductDimension =              47 + HaiDomainEnumerationBase.AHAM,         // Aham

        ModelSizeSet =                  48 + HaiDomainEnumerationBase.AHAM,         // Aham

        ManufacturerMissingBrand =      49 + HaiDomainEnumerationBase.AHAM,         // Aham

        SuppressReport =                50 + HaiDomainEnumerationBase.AHAM,         // Aham

        IndustryReportMemberForOutput = 51 + HaiDomainEnumerationBase.AHAM,         // Aham

        ManufacturerPLog =              52 + HaiDomainEnumerationBase.AHAM,         // Aham

        OutputDistributionManufacturer= 53 + HaiDomainEnumerationBase.AHAM,         // Aham

        OutputDistributionUser =        54 + HaiDomainEnumerationBase.AHAM,         // Aham

        OutputReportPublic =            55 + HaiDomainEnumerationBase.AHAM,         // Aham

        ProductMarket =                 56 + HaiDomainEnumerationBase.AHAM,         // Aham

        ProductSum =                    57 + HaiDomainEnumerationBase.AHAM,         // Aham

        InputColumn =                   58 + HaiDomainEnumerationBase.AHAM,         // Aham

        InputRow =                      59 + HaiDomainEnumerationBase.AHAM,         // Aham

        ModelSizeSetMember =            60 + HaiDomainEnumerationBase.AHAM,         // Aham

        ReportCompareMaster =           61 + HaiDomainEnumerationBase.AHAM,         // Aham
        
        ReportCompareDetail =           62 + HaiDomainEnumerationBase.AHAM,         // Aham
        ChannelHeirarchySet =           63 + HaiDomainEnumerationBase.AHAM,         // Aham
        ChannelHeirarchySetMember =     64 + HaiDomainEnumerationBase.AHAM,         // Aham
        ParentChannelSet =              65 + HaiDomainEnumerationBase.AHAM,         // Aham
        HolidayCADate =                 66 + HaiDomainEnumerationBase.AHAM,         // Aham
        YearDates =                     67 + HaiDomainEnumerationBase.AHAM,         // Aham

        // ---------- ISTAT --------------

        Activity_Istat =                1 + HaiDomainEnumerationBase.ISTAT,         // Istat
        Attribute =                     2 + HaiDomainEnumerationBase.ISTAT,         // Istat

        CustomerType =                  3 + HaiDomainEnumerationBase.ISTAT,         // Istat
        Product_Istat =                 4 + HaiDomainEnumerationBase.ISTAT,         // Istat
        Program =                       5 + HaiDomainEnumerationBase.ISTAT          // Istat
    }
}
