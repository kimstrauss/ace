﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Serialization;
using HaiBusinessObject;

namespace HaiReports
{
    [Serializable]
    public class ReportParameters
    {
        public ReportParameters()
        {
        }

        #region Static Class Methods

        public static string GetDatabaseName(string serializedReportParameters)
        {
            return DeserializeParametersFromXML(serializedReportParameters).DatabaseName;
        }

        public static DateTime GetDateFilter(string serializedReportParameters)
        {
            return DeserializeParametersFromXML(serializedReportParameters).DateFilter;
        }

        public static DateRange GetDateRange(string serializedReportParameters)
        {
            return DeserializeParametersFromXML(serializedReportParameters).DateRange;
        }

        public static string GetServerName(string serializedReportParameters)
        {
            return DeserializeParametersFromXML(serializedReportParameters).ServerName;
        }

        public static string GetTitleQualifier(string serializedReportParameters)
        {
            return DeserializeParametersFromXML(serializedReportParameters).TitleQualifier;
        }

        public static ListType GetTypeOfList(string serializedReportParameters)
        {
            return DeserializeParametersFromXML(serializedReportParameters).TypeOfList; ;
        }

        public static string GetUserName(string serializedReportParameters)
        {
            return DeserializeParametersFromXML(serializedReportParameters).UserName;
        }

        public static string GetUserNameQualified(string serializedReportParameters)
        {
            return DeserializeParametersFromXML(serializedReportParameters).UserNameQualified;
        }

        #endregion

        #region Properties

        private string _associationName;
        public string AssociationName
        {
            get
            {
                return _associationName;
            }
            set
            {
                _associationName = value;
            }
        }

        private string _databaseName;
        public string DatabaseName
        {
            get
            {
                return _databaseName;
            }
            set
            {
                _databaseName = value;
            }
        }

        private DateTime _dateFilter;
        public DateTime DateFilter
        {
            get
            {
                return _dateFilter;
            }
            set
            {
                _dateFilter = value;
            }
        }

        private DateRange _dateRange;
        public DateRange DateRange
        {
            get
            {
                return _dateRange;
            }
            set
            {
                _dateRange = value;
            }
        }

        private string _serverName;
        public string ServerName
        {
            get
            {
                return _serverName;
            }
            set
            {
                _serverName = value;
            }
        }

        private string _titleQualifier;
        public string TitleQualifier
        {
            get
            {
                return _titleQualifier;
            }
            set
            {
                _titleQualifier = value;
            }
        }

        private ListType _typeOfList;
        public ListType TypeOfList
        {
            get
            {
                return _typeOfList;
            }
            set
            {
                _typeOfList = value;
            }
        }

        [XmlIgnore]
        public string UserName
        {
            get
            {
                string shortName;
                string qualifiedName = this.UserNameQualified;
                int posLastBackslash = qualifiedName.LastIndexOf(@"\");
                shortName = qualifiedName.Substring(posLastBackslash + 1);
                return shortName;
            }
        }

        private string _userNameQualified;
        public string UserNameQualified
        {
            get
            {
                return _userNameQualified;
            }
            set
            {
                _userNameQualified = value;
            }
        }

        #endregion

        #region Static serialization/deserialization procedures

        public static string SerializeReportParameters(ReportParameters reportParameters)
        {
            return SerializeParameters(reportParameters);
        }

        public static ReportParameters DeserializeToReportParameters(string xml)
        {
            return DeserializeParametersFromXML(xml);
        }

        private static string SerializeParameters(ReportParameters parameters)
        {
            System.IO.StringWriter xml = new System.IO.StringWriter();
            Type[] extraTypes = new Type[4];
            extraTypes[0] = typeof(DateRange);
            extraTypes[1] = typeof(Filter);
            extraTypes[2] = typeof(List<Filter>);
            extraTypes[3] = typeof(ListType);
            XmlSerializer serializer = new XmlSerializer(typeof(ReportParameters), extraTypes);
            serializer.Serialize(xml, parameters);

            return xml.ToString();
        }

        private static ReportParameters DeserializeParametersFromXML(string xml)
        {
            ReportParameters parameters;

            parameters = new ReportParameters();
            System.IO.StringReader xmlStream = new System.IO.StringReader(xml);

            Type[] extraTypes = new Type[4];
            extraTypes[0] = typeof(DateRange);
            extraTypes[1] = typeof(Filter);
            extraTypes[2] = typeof(List<Filter>);
            extraTypes[3] = typeof(ListType);
            XmlSerializer serializer = new XmlSerializer(typeof(ReportParameters), extraTypes);
            parameters = (ReportParameters)serializer.Deserialize(xmlStream);

            return parameters;
        }

        #endregion

        #region Instance Methods

        public void SetBasicProperties(ListType typeOfList)
        {
            this.UserNameQualified = Utilities.GetUserName();
            this.DatabaseName = Utilities.GetDatabaseName();
            this.ServerName = Utilities.GetServerName();
            this.AssociationName = Utilities.GetAssociationName();
            this.TypeOfList = typeOfList;
        }

        public string SerializeMe()
        {
            return SerializeParameters(this);
        }

        #endregion
    }

    [Serializable]
    public enum ListType
    {
        unknown,
        Dictionary,
        DictionaryNotInSet,
        Set,
        SetMembers
    }
}
