using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Xml.Serialization;

using HaiBusinessObject;
using HaiBusinessObject.Core;


namespace HaiBusinessObject
{
    public static class Utilities
    {

        public static string SerializeGridFiltersToString(Dictionary<string, List<Filter>> gridFilters)
        {
            // convert dictionary gridFilters to serializable list of key/value (columnName, filterList) objects
            System.IO.StringWriter xml = new System.IO.StringWriter();
            List<KeyValueFilterListEntity> keyValueList = new List<KeyValueFilterListEntity>();
            foreach (string key in gridFilters.Keys)
            {
                KeyValueFilterListEntity keyValuePair = new KeyValueFilterListEntity();
                keyValuePair.Key = key;
                keyValuePair.FilterList = gridFilters[key];
                keyValueList.Add(keyValuePair);
            }

            // serialize the filters
            Type[] extraTypes = new Type[3];
            extraTypes[0] = typeof(KeyValueFilterListEntity);
            extraTypes[1] = typeof(Filter);
            extraTypes[2] = typeof(List<Filter>);
            XmlSerializer serializer = new XmlSerializer(typeof(List<KeyValueFilterListEntity>), extraTypes);
            serializer.Serialize(xml, keyValueList);

            return xml.ToString();
        }

        public static string GetApplicationContext()
        {
            return ConfigurationManager.AppSettings["ApplicationContext"];
        }

        public static string GetUserName()
        {
            System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
            string userName = ((System.Security.Principal.IIdentity)identity).Name;
            return userName;
        }

        public static string GetPathToReports()
        {
            string reportPath = string.Empty;

            // if this is being run in the IDE the path must be normalized to the project directory
            string executionPath = AppDomain.CurrentDomain.BaseDirectory;
            executionPath = executionPath.ToLower();
            executionPath = executionPath.Replace(@"bin\debug\", "");
            executionPath = executionPath.Replace(@"bin\release\", "");

            // reports will be located in a subdirectory of the execution directory as 
            // ... a relative path; or they will be on an absolute path
            string relativeReportPath = ConfigurationManager.AppSettings["PathToReports"];
            if (!relativeReportPath.EndsWith(@"\"))             // ensure path ends with a backslash
                relativeReportPath = relativeReportPath + @"\";

            if (relativeReportPath.StartsWith(@"\"))            // ensure that there is no leading backslash
                relativeReportPath = relativeReportPath.Remove(0, 1);

            // if relative path is really an absolute path, so execution path is not used
            if (relativeReportPath.Contains(":"))
                executionPath = string.Empty;

            reportPath = executionPath + relativeReportPath;

            return reportPath;
        }

        public static DateTime GetOpenBeginDate()
        {
            string OpenBeginDate = ConfigurationManager.AppSettings["OpenBeginDate"];

            if (OpenBeginDate == string.Empty)
                return DateTime.MinValue.Date;                  // nothing supplied, use system minimum
            else
                return DateTime.Parse(OpenBeginDate).Date;
        }

        public static DateTime GetOpenEndDate()
        {
            string OpenEndDate = ConfigurationManager.AppSettings["OpenEndDate"];
            if (OpenEndDate == string.Empty)
                return DateTime.MaxValue.Date;                  // nothing supplied, use system maximum
            else
                return DateTime.Parse(OpenEndDate).Date;
        }

        public static DateTime GetEarliestBeginDate()
        {
            string earliestBeginDate = ConfigurationManager.AppSettings["EarliestBeginDate"];
            return DateTime.Parse(earliestBeginDate).Date;
        }

        public static DateTime GetLatestEndDate()
        {
            string latestEndDate = ConfigurationManager.AppSettings["LatestEndDate"];
            return DateTime.Parse(latestEndDate);
        }

        public static string GetAssociationName()
        {
            string associationName = ConfigurationManager.AppSettings["AssociationName"];
            if ((associationName == null) || (associationName == string.Empty))
                throw new Exception("Server name is missing in configuration file");

            return associationName;
        }

        private static string _overrideServerName = string.Empty;
        private static string _overrideServerDisplayName = string.Empty;
        public static void SetOverrideServerName(string overrideName)
        {
            if (overrideName.Trim() != string.Empty)
                _overrideServerDisplayName = overrideName.Trim();
            if (overrideName.Trim() == "Development")
                _overrideServerName = ConfigurationManager.AppSettings["DevelopmentServer"];
            else if (overrideName.Trim() == "Production")
                _overrideServerName = ConfigurationManager.AppSettings["ProductionServer"];
            else if (overrideName.Trim() == "QA")
                _overrideServerName = ConfigurationManager.AppSettings["QAServer"];
        }

        public static string GetServerName()
        {
            if (_overrideServerName != string.Empty)
                return _overrideServerName;
            else
            {
                string serverName = ConfigurationManager.AppSettings["ServerName"];
                if ((serverName == null) || (serverName == string.Empty))
                    throw new Exception("Server name is missing in configuration file");

                return serverName;
            }
        }
        public static string GetServerDisplayName()
        {
            if (_overrideServerDisplayName != string.Empty)
                return _overrideServerDisplayName;
            else
            {
                string serverDisplayName = ConfigurationManager.AppSettings["ServerDisplayName"];
                if ((serverDisplayName == null) || (serverDisplayName == string.Empty))
                    throw new Exception("Server display name is missing in configuration file");

                return serverDisplayName;
            }
        }

        public static string GetReportServerName()
        {
            string serverName = ConfigurationManager.AppSettings["ReportServerName"];
            if ((serverName == null) || (serverName == string.Empty))
                throw new Exception("ReportServerName is missing in configuration file");

            return serverName;
        }

        private static string _overrideDatabaseName = string.Empty;
        private static string _overrideDatabaseDisplayName = string.Empty;
        public static void SetOverrideDatabaseName(string overrideName)
        {
            if (overrideName.Trim() != string.Empty)
                _overrideDatabaseName = overrideName.Trim();

        }

        public static string GetDatabaseName()
        {
            if (_overrideDatabaseName != string.Empty)
                return _overrideDatabaseName;
            else
            {
                string databaseName = ConfigurationManager.AppSettings["DatabaseName"];
                if ((databaseName == null) || (databaseName == string.Empty))
                    throw new Exception("Database name is missing in configuration file");

                // check for known database names and set _overrideDatabaseName and databasecontext accordingly
                // otherwise throw error.

                databaseName = databaseName.ToLower();
                switch (databaseName)
                {
                    case "ahamoper":
                        _overrideDatabaseName = databaseName;
                        break;

                    case "opeidev":
                        _overrideDatabaseName = databaseName;
                        break;

                    default:
                        throw new Exception("Unknown database in in App.Config");
                }

                return databaseName;
            }
        }
        public static string GetDatabaseDisplayName()
        {
            string databaseName;
            if (_overrideDatabaseName != string.Empty)
                databaseName = _overrideDatabaseName;
            else
            {
                databaseName = ConfigurationManager.AppSettings["DatabaseName"];
                if ((databaseName == null) || (databaseName == string.Empty))
                    throw new Exception("Database name is missing in configuration file");
            }
                // check for known database names and set _overrideDatabaseName and databasecontext accordingly
                // otherwise throw error.

                databaseName = databaseName.ToLower();
                switch (databaseName)
                {
                    case "ahamoper":
                        _overrideDatabaseDisplayName = "AHAM";
                        break;

                    case "opeidev":
                        _overrideDatabaseDisplayName = "OPEI";
                        break;

                    default:
                        throw new Exception("Unknown database in in App.Config");
                }

                return _overrideDatabaseDisplayName;
            //}
        }

        public static int GetDatabaseContext()
        {
            string databaseName = GetDatabaseName();

            switch (databaseName.ToLower())
            {
                case "ahamoper":
                    return 1;

                case "opeidev":
                    return 2;

                default:
                    throw new Exception("Unknown database in in App.Config");
            }
        }

        public static string GetStringForNull()
        {
            return "�";                                         // display value for database nulls
        }

        public static bool ConvertByteToBool(byte value)
        {
            return (value != 0);
        }

        public static byte ConvertBoolToByte(bool value)
        {
            if (value)
                return 1;
            else
                return 0;
        }

        public static DateTime MonthBegin(DateTime anyDate)
        {
            DateTime monthBeginDate;

            monthBeginDate = new DateTime(anyDate.Year, anyDate.Month, 1);

            return monthBeginDate;
        }

        public static DateTime MonthEnd(DateTime anyDate)
        {
            DateTime monthEndDate;

            monthEndDate = new DateTime(anyDate.Year, anyDate.Month, 1);
            monthEndDate = monthEndDate.AddMonths(1);
            monthEndDate = monthEndDate.AddDays(-1);

            return monthEndDate;
        }

        #region Replacements for VB runtime functionality

        /// <summary>
        /// Determines whether the specified
        /// value can be converted to a valid number.
        /// </summary>
        public static bool IsNumeric(object value)
        {
            double dbl;
            return double.TryParse(value.ToString(), System.Globalization.NumberStyles.Any,
              System.Globalization.NumberFormatInfo.InvariantInfo, out dbl);
        }

        /// <summary>
        /// Allows late bound invocation of
        /// properties and methods.
        /// </summary>
        /// <param name="target">Object implementing the property or method.</param>
        /// <param name="methodName">Name of the property or method.</param>
        /// <param name="callType">Specifies how to invoke the property or method.</param>
        /// <param name="args">List of arguments to pass to the method.</param>
        /// <returns>The result of the property or method invocation.</returns>
        public static object CallByName(object target, string methodName, CallType callType, params object[] args)
        {
            switch (callType)
            {
                case CallType.Get:
                    {
                        PropertyInfo p = target.GetType().GetProperty(methodName);
                        return p.GetValue(target, args);
                    }
                case CallType.Let:
                case CallType.Set:
                    {
                        PropertyInfo p = target.GetType().GetProperty(methodName);
                        object[] index = null;
                        //args.CopyTo(index, 1);    <==  this appears to be a CSLA bug!!
                        try                         // try to set the property value
                        {
                            p.SetValue(target, args[0], index);
                        }
                        catch (Exception ex)        // "set" failed
                        {
                            if (ex.Message.Contains(@"Object of type 'System.DBNull' cannot be converted to type"))
                            {
                                // data value is null
                                switch (p.PropertyType.BaseType.Name)
                                {
                                    case "Object":  // this case handles null strings setting them to string.Empty
                                        p.SetValue(target, string.Empty, index);
                                        break;
                                    default:        // this is for ValueTypes: do nothing so that object property is set to default value
                                        break;
                                }
                            }
                            else if (ex.Message == "Object of type 'System.Byte' cannot be converted to type 'System.Boolean'.")
                            {
                                if ((byte)args[0] == 0)
                                {
                                        p.SetValue(target, false, index);
                                }
                                else
                                {
                                        p.SetValue(target, true, index);
                                }
                            }
                            else
                                throw;              // unexpected error
                        }
                        return null;
                    }
                case CallType.Method:
                    {
                        MethodInfo m = target.GetType().GetMethod(methodName);
                        return m.Invoke(target, args);
                    }
            }
            return null;
        }

        #endregion

        /// <summary>
        /// Returns a property's type, dealing with
        /// Nullable<T> if necessary.
        /// </summary>
        public static Type GetPropertyType(Type propertyType)
        {
            Type type = propertyType;
            if (type.IsGenericType &&
              (type.GetGenericTypeDefinition() == typeof(Nullable<>)))
                return Nullable.GetUnderlyingType(type);
            return type;
        }

    }

    /// <summary>
    /// Valid options for calling a property or method
    /// via the <see cref="Csla.Utilities.CallByName"/> method.
    /// </summary>
    public enum CallType
    {
        Get,
        Let,
        Method,
        Set
    }

}
