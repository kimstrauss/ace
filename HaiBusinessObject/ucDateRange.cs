﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace MyNamespace
{
    public partial class ucDateRange : UserControl
    {

        private DateRange mDateRange;

        public ucDateRange()
        {
            // This call is required by the Windows Form Designer. 
            InitializeComponent();
            // Add any initialization after the InitializeComponent() call. 
        }

        public void InitializeWithDateRange(ref DateRange DateRange)
        {
            mDateRange = DateRange;

            {
                BeginDateTimePicker.CustomFormat = "MMM dd, yyyy";
                //BeginDateTimePicker.MinDate = DateSerial(1991, 1, 1);
                BeginDateTimePicker.MinDate = DateTime.Parse("1991, 1, 1");
            }

            {
                EndDateTimePicker.CustomFormat = "MMM dd, yyyy";
                //EndDateTimePicker.MaxDate = DateSerial(2048, 12, 31);
                EndDateTimePicker.MaxDate = DateTime.Parse("2048, 12, 31");
            }

            if (mDateRange.BeginDate.Date == DateTime.Today.Date)
            {
                BeginDateTimePicker.Value = DateTime.Today.AddDays(1);
                BeginDateTimePicker.Enabled = false;
                rbTodayStartDate.Checked = true;
                rbFixedStartDate.Checked = false;
            }
            else
            {
                if (mDateRange.BeginDate.Date == System.DateTime.MinValue.Date)
                {
                    BeginDateTimePicker.Value = System.DateTime.Today;
                    rbTodayStartDate.Checked = true;
                    rbFixedStartDate.Checked = false;
                }
                else
                {
                    BeginDateTimePicker.Value = mDateRange.BeginDate.Date;
                    rbTodayStartDate.Checked = false;
                    rbFixedStartDate.Checked = true;
                }
                BeginDateTimePicker.Enabled = true;
            }

            if (mDateRange.EndDate.Date == DateTime.MaxValue.Date)
            {
                EndDateTimePicker.Value = DateTime.Today.AddDays(1);
                EndDateTimePicker.Enabled = false;
                rbOpenEndDate.Checked = true;
                rbFixedEndDate.Checked = false;
            }
            else
            {
                EndDateTimePicker.Value = mDateRange.EndDate.Date;
                EndDateTimePicker.Enabled = true;
                rbOpenEndDate.Checked = false;
                rbFixedEndDate.Checked = true;
            }

        }

        public bool IsDirty()
        {
            return IsDirtyTest();
        }

        private void rbOpenEndDate_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbOpenEndDate.Checked)
            {
                EndDateTimePicker.Enabled = false;
                EndDateTimePicker.Visible = false;
                rbFixedEndDate.Text = "set end date";
            }
            else
            {
                EndDateTimePicker.Enabled = true;
                EndDateTimePicker.Visible = true;
                rbFixedEndDate.Text = "";
            }
        }

        private void rbTodayStartDate_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbTodayStartDate.Checked)
            {
                BeginDateTimePicker.Enabled = false;
                BeginDateTimePicker.Visible = false;
                rbFixedStartDate.Text = "set begin date";
            }
            else
            {
                BeginDateTimePicker.Enabled = true;
                BeginDateTimePicker.Visible = true;
                rbFixedStartDate.Text = "";
            }
        }

        public bool UpdateDateRange(ref DateRange AnyDateRange, ref string ErrorMessage)
        {
            DateTime BeginDate;
            DateTime EndDate;
            bool NoErrors;

            NoErrors = false;

            if (rbTodayStartDate.Checked)
            {
                BeginDate = DateTime.Today;
            }
            else
            {
                BeginDate = BeginDateTimePicker.Value;
            }

            if (rbOpenEndDate.Checked)
            {
                EndDate = DateTime.MaxValue.Date;
            }
            else
            {
                EndDate = EndDateTimePicker.Value;
            }

            if (EndDate < BeginDate)
            {
                ErrorMessage = "End date cannot be before begin date.";
                return NoErrors;
            }

            {
                AnyDateRange.BeginDate = BeginDate;
                AnyDateRange.EndDate = EndDate;
            }

            NoErrors = true;

            mDateRange.IsDirty = true;

            return NoErrors;

        }

        private bool IsDirtyTest()
        {
            DateRange LatentDateRange;
            bool IsDirty;
            string Message;

            LatentDateRange = new DateRange();
            Message = "";
            IsDirty = false;

            if (!UpdateDateRange(ref LatentDateRange, ref Message))
            {
                MessageBox.Show("Errors found in Date Range." + "\r\n" + Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return IsDirty;
            }

            {
                if (LatentDateRange.BeginDate.Date != mDateRange.BeginDate.Date)
                    IsDirty = true;
                if (LatentDateRange.EndDate.Date != mDateRange.EndDate.Date)
                    IsDirty = true;
            }

            return IsDirty;

        }

    }
}