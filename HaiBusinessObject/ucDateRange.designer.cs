﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;


namespace MyNamespace
{
    public partial class ucDateRange : UserControl
    {

        //Required by the Windows Form Designer 
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //NOTE: The following procedure is required by the Windows Form Designer 
        //It can be modified using the Windows Form Designer. 
        //Do not modify it using the code editor. 
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.rbFixedEndDate = new System.Windows.Forms.RadioButton();
            this.rbOpenEndDate = new System.Windows.Forms.RadioButton();
            this.EndDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.BeginDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.rbFixedStartDate = new System.Windows.Forms.RadioButton();
            this.rbTodayStartDate = new System.Windows.Forms.RadioButton();
            this.GroupBox1.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.rbFixedEndDate);
            this.GroupBox1.Controls.Add(this.rbOpenEndDate);
            this.GroupBox1.Controls.Add(this.EndDateTimePicker);
            this.GroupBox1.Location = new System.Drawing.Point(143, 3);
            this.GroupBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GroupBox1.Size = new System.Drawing.Size(128, 77);
            this.GroupBox1.TabIndex = 10;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "End date";
            // 
            // rbFixedEndDate
            // 
            this.rbFixedEndDate.AutoSize = true;
            this.rbFixedEndDate.Location = new System.Drawing.Point(4, 46);
            this.rbFixedEndDate.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.rbFixedEndDate.Name = "rbFixedEndDate";
            this.rbFixedEndDate.Size = new System.Drawing.Size(14, 13);
            this.rbFixedEndDate.TabIndex = 12;
            // 
            // rbOpenEndDate
            // 
            this.rbOpenEndDate.AutoSize = true;
            this.rbOpenEndDate.Location = new System.Drawing.Point(4, 19);
            this.rbOpenEndDate.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.rbOpenEndDate.Name = "rbOpenEndDate";
            this.rbOpenEndDate.Size = new System.Drawing.Size(94, 17);
            this.rbOpenEndDate.TabIndex = 11;
            this.rbOpenEndDate.Text = "open end date";
            // 
            // EndDateTimePicker
            // 
            this.EndDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.EndDateTimePicker.Location = new System.Drawing.Point(20, 43);
            this.EndDateTimePicker.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.EndDateTimePicker.Name = "EndDateTimePicker";
            this.EndDateTimePicker.Size = new System.Drawing.Size(82, 20);
            this.EndDateTimePicker.TabIndex = 10;
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.BeginDateTimePicker);
            this.GroupBox2.Controls.Add(this.rbFixedStartDate);
            this.GroupBox2.Controls.Add(this.rbTodayStartDate);
            this.GroupBox2.Location = new System.Drawing.Point(2, 3);
            this.GroupBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GroupBox2.Size = new System.Drawing.Size(128, 77);
            this.GroupBox2.TabIndex = 12;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Start date";
            // 
            // BeginDateTimePicker
            // 
            this.BeginDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.BeginDateTimePicker.Location = new System.Drawing.Point(20, 43);
            this.BeginDateTimePicker.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.BeginDateTimePicker.Name = "BeginDateTimePicker";
            this.BeginDateTimePicker.Size = new System.Drawing.Size(82, 20);
            this.BeginDateTimePicker.TabIndex = 13;
            // 
            // rbFixedStartDate
            // 
            this.rbFixedStartDate.AutoSize = true;
            this.rbFixedStartDate.Location = new System.Drawing.Point(4, 46);
            this.rbFixedStartDate.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.rbFixedStartDate.Name = "rbFixedStartDate";
            this.rbFixedStartDate.Size = new System.Drawing.Size(14, 13);
            this.rbFixedStartDate.TabIndex = 12;
            // 
            // rbTodayStartDate
            // 
            this.rbTodayStartDate.AutoSize = true;
            this.rbTodayStartDate.Location = new System.Drawing.Point(4, 19);
            this.rbTodayStartDate.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.rbTodayStartDate.Name = "rbTodayStartDate";
            this.rbTodayStartDate.Size = new System.Drawing.Size(51, 17);
            this.rbTodayStartDate.TabIndex = 11;
            this.rbTodayStartDate.Text = "today";
            // 
            // ucDateRange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 98);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.GroupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "ucDateRange";
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.RadioButton rbFixedEndDate;
        internal System.Windows.Forms.RadioButton rbOpenEndDate;
        internal System.Windows.Forms.DateTimePicker EndDateTimePicker;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.RadioButton rbFixedStartDate;
        internal System.Windows.Forms.RadioButton rbTodayStartDate;
        internal System.Windows.Forms.DateTimePicker BeginDateTimePicker;

    }
}