﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using HaiBusinessObject;

namespace HaiInterfaces
{
    public interface IDateRange
    {
        HaiBusinessObject.DateRange DateRange
        {
            get;
            set;
        }

        DateTime BeginDate
        {
            get;
            set;
        }

        DateTime EndDate
        {
            get;
            set;
        }

        string UniqueIdentifier
        {
            get;
        }

        string[] KindredPropertyNames
        {
            get;
        }
    }

    public interface ISet
    {
        [Browsable(false)]
        int SetKey
        {
            get;
        }

        [Browsable(false)]
        string SetName
        {
            get;
        }

        [Browsable(false)]
        string SetAbbreviation
        {
            get;
        }
    }

    public interface IItem
    {
        [Browsable(false)]
        int ItemKey
        {
            get;
        }

        [Browsable(false)]
        string ItemName
        {
            get;
        }

        [Browsable(false)]
        string ItemAbbreviation
        {
            get;
        }
    }

    public interface IMember
    {
        int ItemKey
        {
            get;
            set;
        }

        int SetKey
        {
            get;
            set;
        }

        string ItemAbbreviation
        {
            get;
            set;
        }

        string ItemName
        {
            get;
            set;
        }

        string SetAbbreviation
        {
            get;
            set;
        }

        string SetName
        {
            get;
            set;
        }

        HaiBusinessObject.DateRange DateRange
        {
            get;
        }
    }
}
