﻿using System.Configuration;

using HaiBusinessObject;

namespace HaiMetaDataDAL
{
    public class DataServiceParameters
    {
        public string UserName
        {
            get
            {
                return Utilities.GetUserName();
            }
        }

        public string ApplicationContext
        {
            get
            {
                return Utilities.GetApplicationContext();
            }
        }

        public string ConnectionString
        {
            get
            {
                string connectionString;
                connectionString = @"Data Source=" + Utilities.GetServerName() + ";Initial Catalog=" + Utilities.GetDatabaseName() + ";Integrated Security=True";
                return connectionString;
            }
        }
    }
}
