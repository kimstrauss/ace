﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HaiBusinessObject;


namespace HaiMetaDataDAL
{
    public interface IMetaDataDS
    {
        DataAccessResult GetDataList(HaiBusinessObjectType objectType);
        DataAccessResult DataItemReGet(HaiBusinessObjectBase anyHaiBusinessObject);
        DataAccessResult Save(HaiBusinessObjectBase anyHaiBusinessObject);
        DataAccessResult DuplicateSetMembers(HaiBusinessObjectBase sourceSet, HaiBusinessObjectBase targetSet);

        DataAccessTestResult TestObjectForSaveConflicts(HaiBusinessObjectBase anyHaiBusinessObject);
    }
}
