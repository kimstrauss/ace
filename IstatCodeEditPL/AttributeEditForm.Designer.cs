﻿namespace IstatMetaDataPL
{
    partial class AttributeEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label averageOrTotalLabel;
            System.Windows.Forms.Label cHLMCDESCLabel;
            System.Windows.Forms.Label cHLMCMOSILabel;
            System.Windows.Forms.Label dataOrderLabel;
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label keyLabel;
            System.Windows.Forms.Label labelsLabel;
            System.Windows.Forms.Label measureNameLabel;
            System.Windows.Forms.Label modelOrderLabel;
            System.Windows.Forms.Label shortDescriptionLabel;
            System.Windows.Forms.Label subCntLabel;
            System.Windows.Forms.Label unitsDescriptionLabel;
            this.attributeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cHLMCDESCTextBox = new System.Windows.Forms.TextBox();
            this.cHLMCMOSITextBox = new System.Windows.Forms.TextBox();
            this.dataOrderTextBox = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.iDTextBox = new System.Windows.Forms.TextBox();
            this.keyTextBox = new System.Windows.Forms.TextBox();
            this.labelsTextBox = new System.Windows.Forms.TextBox();
            this.measureNameTextBox = new System.Windows.Forms.TextBox();
            this.modelOrderTextBox = new System.Windows.Forms.TextBox();
            this.shortDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.subCntTextBox = new System.Windows.Forms.TextBox();
            this.unitsDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.averageOrTotalComboBox = new System.Windows.Forms.ComboBox();
            averageOrTotalLabel = new System.Windows.Forms.Label();
            cHLMCDESCLabel = new System.Windows.Forms.Label();
            cHLMCMOSILabel = new System.Windows.Forms.Label();
            dataOrderLabel = new System.Windows.Forms.Label();
            descriptionLabel = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            keyLabel = new System.Windows.Forms.Label();
            labelsLabel = new System.Windows.Forms.Label();
            measureNameLabel = new System.Windows.Forms.Label();
            modelOrderLabel = new System.Windows.Forms.Label();
            shortDescriptionLabel = new System.Windows.Forms.Label();
            subCntLabel = new System.Windows.Forms.Label();
            unitsDescriptionLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.attributeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(370, 486);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(451, 486);
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 486);
            // 
            // averageOrTotalLabel
            // 
            averageOrTotalLabel.AutoSize = true;
            averageOrTotalLabel.Location = new System.Drawing.Point(13, 19);
            averageOrTotalLabel.Name = "averageOrTotalLabel";
            averageOrTotalLabel.Size = new System.Drawing.Size(91, 13);
            averageOrTotalLabel.TabIndex = 29;
            averageOrTotalLabel.Text = "Average Or Total:";
            // 
            // cHLMCDESCLabel
            // 
            cHLMCDESCLabel.AutoSize = true;
            cHLMCDESCLabel.Location = new System.Drawing.Point(13, 45);
            cHLMCDESCLabel.Name = "cHLMCDESCLabel";
            cHLMCDESCLabel.Size = new System.Drawing.Size(76, 13);
            cHLMCDESCLabel.TabIndex = 31;
            cHLMCDESCLabel.Text = "CHLMCDESC:";
            // 
            // cHLMCMOSILabel
            // 
            cHLMCMOSILabel.AutoSize = true;
            cHLMCMOSILabel.Location = new System.Drawing.Point(13, 71);
            cHLMCMOSILabel.Name = "cHLMCMOSILabel";
            cHLMCMOSILabel.Size = new System.Drawing.Size(74, 13);
            cHLMCMOSILabel.TabIndex = 33;
            cHLMCMOSILabel.Text = "CHLMCMOSI:";
            // 
            // dataOrderLabel
            // 
            dataOrderLabel.AutoSize = true;
            dataOrderLabel.Location = new System.Drawing.Point(13, 97);
            dataOrderLabel.Name = "dataOrderLabel";
            dataOrderLabel.Size = new System.Drawing.Size(62, 13);
            dataOrderLabel.TabIndex = 35;
            dataOrderLabel.Text = "Data Order:";
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Location = new System.Drawing.Point(13, 123);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(63, 13);
            descriptionLabel.TabIndex = 37;
            descriptionLabel.Text = "Description:";
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Location = new System.Drawing.Point(13, 149);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(21, 13);
            iDLabel.TabIndex = 39;
            iDLabel.Text = "ID:";
            // 
            // keyLabel
            // 
            keyLabel.AutoSize = true;
            keyLabel.Location = new System.Drawing.Point(13, 175);
            keyLabel.Name = "keyLabel";
            keyLabel.Size = new System.Drawing.Size(28, 13);
            keyLabel.TabIndex = 41;
            keyLabel.Text = "Key:";
            // 
            // labelsLabel
            // 
            labelsLabel.AutoSize = true;
            labelsLabel.Location = new System.Drawing.Point(13, 201);
            labelsLabel.Name = "labelsLabel";
            labelsLabel.Size = new System.Drawing.Size(41, 13);
            labelsLabel.TabIndex = 43;
            labelsLabel.Text = "Labels:";
            // 
            // measureNameLabel
            // 
            measureNameLabel.AutoSize = true;
            measureNameLabel.Location = new System.Drawing.Point(13, 227);
            measureNameLabel.Name = "measureNameLabel";
            measureNameLabel.Size = new System.Drawing.Size(82, 13);
            measureNameLabel.TabIndex = 45;
            measureNameLabel.Text = "Measure Name:";
            // 
            // modelOrderLabel
            // 
            modelOrderLabel.AutoSize = true;
            modelOrderLabel.Location = new System.Drawing.Point(13, 253);
            modelOrderLabel.Name = "modelOrderLabel";
            modelOrderLabel.Size = new System.Drawing.Size(68, 13);
            modelOrderLabel.TabIndex = 47;
            modelOrderLabel.Text = "Model Order:";
            // 
            // shortDescriptionLabel
            // 
            shortDescriptionLabel.AutoSize = true;
            shortDescriptionLabel.Location = new System.Drawing.Point(13, 279);
            shortDescriptionLabel.Name = "shortDescriptionLabel";
            shortDescriptionLabel.Size = new System.Drawing.Size(91, 13);
            shortDescriptionLabel.TabIndex = 49;
            shortDescriptionLabel.Text = "Short Description:";
            // 
            // subCntLabel
            // 
            subCntLabel.AutoSize = true;
            subCntLabel.Location = new System.Drawing.Point(13, 305);
            subCntLabel.Name = "subCntLabel";
            subCntLabel.Size = new System.Drawing.Size(48, 13);
            subCntLabel.TabIndex = 51;
            subCntLabel.Text = "Sub Cnt:";
            // 
            // unitsDescriptionLabel
            // 
            unitsDescriptionLabel.AutoSize = true;
            unitsDescriptionLabel.Location = new System.Drawing.Point(13, 331);
            unitsDescriptionLabel.Name = "unitsDescriptionLabel";
            unitsDescriptionLabel.Size = new System.Drawing.Size(90, 13);
            unitsDescriptionLabel.TabIndex = 53;
            unitsDescriptionLabel.Text = "Units Description:";
            // 
            // attributeBindingSource
            // 
            this.attributeBindingSource.DataSource = typeof(IstatMetaDataDAL.HaiAttribute);
            // 
            // cHLMCDESCTextBox
            // 
            this.cHLMCDESCTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "CHLMCDESC", true));
            this.cHLMCDESCTextBox.Location = new System.Drawing.Point(110, 42);
            this.cHLMCDESCTextBox.Name = "cHLMCDESCTextBox";
            this.cHLMCDESCTextBox.Size = new System.Drawing.Size(100, 20);
            this.cHLMCDESCTextBox.TabIndex = 32;
            // 
            // cHLMCMOSITextBox
            // 
            this.cHLMCMOSITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "CHLMCMOSI", true));
            this.cHLMCMOSITextBox.Location = new System.Drawing.Point(110, 68);
            this.cHLMCMOSITextBox.Name = "cHLMCMOSITextBox";
            this.cHLMCMOSITextBox.Size = new System.Drawing.Size(100, 20);
            this.cHLMCMOSITextBox.TabIndex = 34;
            // 
            // dataOrderTextBox
            // 
            this.dataOrderTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "DataOrder", true));
            this.dataOrderTextBox.Location = new System.Drawing.Point(110, 94);
            this.dataOrderTextBox.Name = "dataOrderTextBox";
            this.dataOrderTextBox.Size = new System.Drawing.Size(100, 20);
            this.dataOrderTextBox.TabIndex = 36;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "Description", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(110, 120);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.descriptionTextBox.TabIndex = 38;
            // 
            // iDTextBox
            // 
            this.iDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "ID", true));
            this.iDTextBox.Location = new System.Drawing.Point(110, 146);
            this.iDTextBox.Name = "iDTextBox";
            this.iDTextBox.Size = new System.Drawing.Size(100, 20);
            this.iDTextBox.TabIndex = 40;
            // 
            // keyTextBox
            // 
            this.keyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "Key", true));
            this.keyTextBox.Location = new System.Drawing.Point(110, 172);
            this.keyTextBox.Name = "keyTextBox";
            this.keyTextBox.Size = new System.Drawing.Size(100, 20);
            this.keyTextBox.TabIndex = 42;
            // 
            // labelsTextBox
            // 
            this.labelsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "Labels", true));
            this.labelsTextBox.Location = new System.Drawing.Point(110, 198);
            this.labelsTextBox.Name = "labelsTextBox";
            this.labelsTextBox.Size = new System.Drawing.Size(100, 20);
            this.labelsTextBox.TabIndex = 44;
            // 
            // measureNameTextBox
            // 
            this.measureNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "MeasureName", true));
            this.measureNameTextBox.Location = new System.Drawing.Point(110, 224);
            this.measureNameTextBox.Name = "measureNameTextBox";
            this.measureNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.measureNameTextBox.TabIndex = 46;
            // 
            // modelOrderTextBox
            // 
            this.modelOrderTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "ModelOrder", true));
            this.modelOrderTextBox.Location = new System.Drawing.Point(110, 250);
            this.modelOrderTextBox.Name = "modelOrderTextBox";
            this.modelOrderTextBox.Size = new System.Drawing.Size(100, 20);
            this.modelOrderTextBox.TabIndex = 48;
            // 
            // shortDescriptionTextBox
            // 
            this.shortDescriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "ShortDescription", true));
            this.shortDescriptionTextBox.Location = new System.Drawing.Point(110, 276);
            this.shortDescriptionTextBox.Name = "shortDescriptionTextBox";
            this.shortDescriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.shortDescriptionTextBox.TabIndex = 50;
            // 
            // subCntTextBox
            // 
            this.subCntTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "SubCnt", true));
            this.subCntTextBox.Location = new System.Drawing.Point(110, 302);
            this.subCntTextBox.Name = "subCntTextBox";
            this.subCntTextBox.Size = new System.Drawing.Size(100, 20);
            this.subCntTextBox.TabIndex = 52;
            // 
            // unitsDescriptionTextBox
            // 
            this.unitsDescriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.attributeBindingSource, "UnitsDescription", true));
            this.unitsDescriptionTextBox.Location = new System.Drawing.Point(110, 328);
            this.unitsDescriptionTextBox.Name = "unitsDescriptionTextBox";
            this.unitsDescriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.unitsDescriptionTextBox.TabIndex = 54;
            // 
            // averageOrTotalComboBox
            // 
            this.averageOrTotalComboBox.FormattingEnabled = true;
            this.averageOrTotalComboBox.Location = new System.Drawing.Point(110, 12);
            this.averageOrTotalComboBox.Name = "averageOrTotalComboBox";
            this.averageOrTotalComboBox.Size = new System.Drawing.Size(100, 21);
            this.averageOrTotalComboBox.TabIndex = 55;
            // 
            // AttributeEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 521);
            this.Controls.Add(this.averageOrTotalComboBox);
            this.Controls.Add(averageOrTotalLabel);
            this.Controls.Add(cHLMCDESCLabel);
            this.Controls.Add(this.cHLMCDESCTextBox);
            this.Controls.Add(cHLMCMOSILabel);
            this.Controls.Add(this.cHLMCMOSITextBox);
            this.Controls.Add(dataOrderLabel);
            this.Controls.Add(this.dataOrderTextBox);
            this.Controls.Add(descriptionLabel);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(iDLabel);
            this.Controls.Add(this.iDTextBox);
            this.Controls.Add(keyLabel);
            this.Controls.Add(this.keyTextBox);
            this.Controls.Add(labelsLabel);
            this.Controls.Add(this.labelsTextBox);
            this.Controls.Add(measureNameLabel);
            this.Controls.Add(this.measureNameTextBox);
            this.Controls.Add(modelOrderLabel);
            this.Controls.Add(this.modelOrderTextBox);
            this.Controls.Add(shortDescriptionLabel);
            this.Controls.Add(this.shortDescriptionTextBox);
            this.Controls.Add(subCntLabel);
            this.Controls.Add(this.subCntTextBox);
            this.Controls.Add(unitsDescriptionLabel);
            this.Controls.Add(this.unitsDescriptionTextBox);
            this.Name = "AttributeEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Attributes";
            this.Load += new System.EventHandler(this.AttributeEditForm_Load);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.unitsDescriptionTextBox, 0);
            this.Controls.SetChildIndex(unitsDescriptionLabel, 0);
            this.Controls.SetChildIndex(this.subCntTextBox, 0);
            this.Controls.SetChildIndex(subCntLabel, 0);
            this.Controls.SetChildIndex(this.shortDescriptionTextBox, 0);
            this.Controls.SetChildIndex(shortDescriptionLabel, 0);
            this.Controls.SetChildIndex(this.modelOrderTextBox, 0);
            this.Controls.SetChildIndex(modelOrderLabel, 0);
            this.Controls.SetChildIndex(this.measureNameTextBox, 0);
            this.Controls.SetChildIndex(measureNameLabel, 0);
            this.Controls.SetChildIndex(this.labelsTextBox, 0);
            this.Controls.SetChildIndex(labelsLabel, 0);
            this.Controls.SetChildIndex(this.keyTextBox, 0);
            this.Controls.SetChildIndex(keyLabel, 0);
            this.Controls.SetChildIndex(this.iDTextBox, 0);
            this.Controls.SetChildIndex(iDLabel, 0);
            this.Controls.SetChildIndex(this.descriptionTextBox, 0);
            this.Controls.SetChildIndex(descriptionLabel, 0);
            this.Controls.SetChildIndex(this.dataOrderTextBox, 0);
            this.Controls.SetChildIndex(dataOrderLabel, 0);
            this.Controls.SetChildIndex(this.cHLMCMOSITextBox, 0);
            this.Controls.SetChildIndex(cHLMCMOSILabel, 0);
            this.Controls.SetChildIndex(this.cHLMCDESCTextBox, 0);
            this.Controls.SetChildIndex(cHLMCDESCLabel, 0);
            this.Controls.SetChildIndex(averageOrTotalLabel, 0);
            this.Controls.SetChildIndex(this.averageOrTotalComboBox, 0);
            ((System.ComponentModel.ISupportInitialize)(this.attributeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource attributeBindingSource;
        private System.Windows.Forms.TextBox cHLMCDESCTextBox;
        private System.Windows.Forms.TextBox cHLMCMOSITextBox;
        private System.Windows.Forms.TextBox dataOrderTextBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.TextBox iDTextBox;
        private System.Windows.Forms.TextBox keyTextBox;
        private System.Windows.Forms.TextBox labelsTextBox;
        private System.Windows.Forms.TextBox measureNameTextBox;
        private System.Windows.Forms.TextBox modelOrderTextBox;
        private System.Windows.Forms.TextBox shortDescriptionTextBox;
        private System.Windows.Forms.TextBox subCntTextBox;
        private System.Windows.Forms.TextBox unitsDescriptionTextBox;
        private System.Windows.Forms.ComboBox averageOrTotalComboBox;
    }
}