namespace IstatMetaDataPL
{
    partial class CustomerTypeEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label keyLabel;
            System.Windows.Forms.Label sortTextLabel;
            this.customerTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.iDTextBox = new System.Windows.Forms.TextBox();
            this.keyTextBox = new System.Windows.Forms.TextBox();
            this.sortTextTextBox = new System.Windows.Forms.TextBox();
            descriptionLabel = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            keyLabel = new System.Windows.Forms.Label();
            sortTextLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.customerTypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(379, 464);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(460, 464);
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 464);
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Location = new System.Drawing.Point(12, 52);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(63, 13);
            descriptionLabel.TabIndex = 28;
            descriptionLabel.Text = "Description:";
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Location = new System.Drawing.Point(12, 78);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(21, 13);
            iDLabel.TabIndex = 30;
            iDLabel.Text = "ID:";
            // 
            // keyLabel
            // 
            keyLabel.AutoSize = true;
            keyLabel.Location = new System.Drawing.Point(12, 104);
            keyLabel.Name = "keyLabel";
            keyLabel.Size = new System.Drawing.Size(28, 13);
            keyLabel.TabIndex = 32;
            keyLabel.Text = "Key:";
            // 
            // sortTextLabel
            // 
            sortTextLabel.AutoSize = true;
            sortTextLabel.Location = new System.Drawing.Point(12, 130);
            sortTextLabel.Name = "sortTextLabel";
            sortTextLabel.Size = new System.Drawing.Size(53, 13);
            sortTextLabel.TabIndex = 34;
            sortTextLabel.Text = "Sort Text:";
            // 
            // customerTypeBindingSource
            // 
            this.customerTypeBindingSource.DataSource = typeof(IstatMetaDataDAL.CustomerType);
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customerTypeBindingSource, "Description", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(81, 49);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.descriptionTextBox.TabIndex = 29;
            // 
            // iDTextBox
            // 
            this.iDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customerTypeBindingSource, "ID", true));
            this.iDTextBox.Location = new System.Drawing.Point(81, 75);
            this.iDTextBox.Name = "iDTextBox";
            this.iDTextBox.Size = new System.Drawing.Size(100, 20);
            this.iDTextBox.TabIndex = 31;
            // 
            // keyTextBox
            // 
            this.keyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customerTypeBindingSource, "Key", true));
            this.keyTextBox.Location = new System.Drawing.Point(81, 101);
            this.keyTextBox.Name = "keyTextBox";
            this.keyTextBox.Size = new System.Drawing.Size(100, 20);
            this.keyTextBox.TabIndex = 33;
            // 
            // sortTextTextBox
            // 
            this.sortTextTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customerTypeBindingSource, "SortText", true));
            this.sortTextTextBox.Location = new System.Drawing.Point(81, 127);
            this.sortTextTextBox.Name = "sortTextTextBox";
            this.sortTextTextBox.Size = new System.Drawing.Size(100, 20);
            this.sortTextTextBox.TabIndex = 35;
            // 
            // CustomerTypeEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(547, 499);
            this.Controls.Add(descriptionLabel);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(iDLabel);
            this.Controls.Add(this.iDTextBox);
            this.Controls.Add(keyLabel);
            this.Controls.Add(this.keyTextBox);
            this.Controls.Add(sortTextLabel);
            this.Controls.Add(this.sortTextTextBox);
            this.Name = "CustomerTypeEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Customer Type";
            this.Load += new System.EventHandler(this.CustomerTypeEditForm_Load);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.sortTextTextBox, 0);
            this.Controls.SetChildIndex(sortTextLabel, 0);
            this.Controls.SetChildIndex(this.keyTextBox, 0);
            this.Controls.SetChildIndex(keyLabel, 0);
            this.Controls.SetChildIndex(this.iDTextBox, 0);
            this.Controls.SetChildIndex(iDLabel, 0);
            this.Controls.SetChildIndex(this.descriptionTextBox, 0);
            this.Controls.SetChildIndex(descriptionLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.customerTypeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource customerTypeBindingSource;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.TextBox iDTextBox;
        private System.Windows.Forms.TextBox keyTextBox;
        private System.Windows.Forms.TextBox sortTextTextBox;
    }
}
