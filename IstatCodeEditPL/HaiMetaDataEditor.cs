using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using HaiBusinessObject;
using IstatMetaDataDAL;
using HaiBusinessUI;

namespace IstatMetaDataPL
{
    public partial class HaiMetaDataEditor : Form
    {
        internal UserControl _currentGridControl;
        internal IUntypedReadonlyDatagridControl _untypedCurrentGridControl;
        internal IstatDataService _ds = new IstatDataService();
        internal DataGridView _datagrid;

        internal delegate void AddProcedure(IUntypedBindingList bindingList);
        internal Dictionary<HaiBusinessObjectType, AddProcedure> _callAddProcedure;
        internal delegate void EditProcedure();
        internal Dictionary<HaiBusinessObjectType, EditProcedure> _callEditProcedure;

        internal MenuStrip _mainMenu;       // make the Main Menu available to sub-classes

        public HaiMetaDataEditor()
        {
            InitializeComponent();

            KeyPreview = true;
            _mainMenu = menuMain;           // make the Main Menu available to sub-classes
        }

        #region FormEventHandlers

        private void MainForm_Load(object sender, EventArgs e)
        {
            // buttons are not enabled until data has been loaded and grid is visible
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            btnDuplicate.Enabled = false;
            btnPrint.Enabled = false;

            // "close" menu item not enabled until data has been loaded and grid is visible
            closeToolStripMenuItem.Enabled = false;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (_currentGridControl != null)
            _currentGridControl.Size = new Size(this.Width - 10, this.Height - 100);
        }

        #endregion

        #region DataGridView event handlers

        void datagrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!(e.RowIndex < 0))
            {
                _datagrid.Rows[e.RowIndex].Selected = true;
                EditItemsInTheGrid();
            }
        }

        #endregion

        #region Button event handlers

        // Edit button
        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditItemsInTheGrid();
        }

        // Add button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddAnItemToGrid();
        }

        // Delete button
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //ShowNothingImplemented();
            DataGridView datagrid = _untypedCurrentGridControl.DataGridView;

            HaiBindingList<HaiBusinessObjectBase> selectedObjects = _untypedCurrentGridControl.GetReadonlyBindingListOfSelectedItems();
            if (selectedObjects.Count==0)           // nothing selected
            {
                MessageBox.Show("Nothing selected.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                DialogResult yesNo = MessageBox.Show(selectedObjects.Count.ToString() + " item(s) will be deleted. \n\r Are you sure?", "", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (yesNo == DialogResult.No)
                    return;
            }

            IUntypedBindingList datalist = (IUntypedBindingList)((BindingSource)(datagrid.DataSource)).DataSource;
            foreach (HaiBusinessObjectBase haiObject in selectedObjects)
            {
                datalist.DeleteItem(haiObject);
                haiObject.MarkDeleted();
                DataAccessResult result = _ds.Save(haiObject);
                if (!result.Success)
                    MessageBox.Show("Delete failed. -- " + result.Message);
            }
        }

        // Copy button
        private void btnCopy_Click(object sender, EventArgs e)
        {
            ShowNothingImplemented();
        }

        // Print button
        private void btnPrint_Click(object sender, EventArgs e)
        {
            ShowNothingImplemented();
        }

        // Test button
        private void btnTest_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You're not supposed to push this button!\n\r\n\rHave a nice day.  ;-)", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }

        #endregion

        #region Utilities

        internal void CreateDataGridControl<T>(DataAccessResult dataFetchResult, List<string> columnList, string mainFormTitle)
        {
            Cursor = Cursors.WaitCursor;
            DateTime startTime = DateTime.Now;

            RemoveDataGridControl();

            menuMain.Items.Add(gridToolStripMenuItem);

            ReadonlyDatagridForm<T> dataGridControl = new ReadonlyDatagridForm<T>(dataFetchResult, columnList);
            _datagrid = dataGridControl.DataGridView;
            _datagrid.CellDoubleClick += new DataGridViewCellEventHandler(datagrid_CellDoubleClick);

            _currentGridControl = dataGridControl;
            _untypedCurrentGridControl = (IUntypedReadonlyDatagridControl)_currentGridControl;
            dataGridControl.Location = new Point(0, 25);
            dataGridControl.Size = new Size(this.Width - 10, this.Height - 100);
            this.Controls.Add(dataGridControl);
            dataGridControl.Show();

            ContextMenuStrip contextMenu = _untypedCurrentGridControl.GetContextMenu();
            gridToolStripMenuItem.DropDown = contextMenu;
            gridToolStripMenuItem.Enabled = true;

            btnAdd.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
            btnDuplicate.Enabled = true;
            btnPrint.Enabled = true;

            closeToolStripMenuItem.Enabled = true;

            TimeSpan createTime = (DateTime.Now).Subtract(startTime);
            Console.WriteLine("Seconds to create grid control: " + createTime.ToString() + " for data type " + typeof(T).ToString());
            this.Text = mainFormTitle;
            Cursor = Cursors.Default;
        }

        internal void RemoveDataGridControl()
        {
            if (_currentGridControl != null)
            {
                this.Controls.Remove(_currentGridControl);
                _currentGridControl.Dispose();
                _currentGridControl = null;
                _untypedCurrentGridControl = null;

                menuMain.Items.Remove(gridToolStripMenuItem);

                btnAdd.Enabled = false;
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
                btnDuplicate.Enabled = false;
                btnPrint.Enabled = false;

                closeToolStripMenuItem.Enabled = false;
            }
        }

        internal EditFormParameters CreateEditFormParametersForNew<T>(T newHaiObject, Dictionary<string, BrowsableProperty> bpList)
        {
            EditFormParameters formParameters = new EditFormParameters();   // container for return objects
            Dictionary<string, EditFormDisplayController> controllerDict = new Dictionary<string, EditFormDisplayController>(); // dict of items to control form display

            // iterate across the properties of all objects to edit
            // ... to determine if any are multivalued, etc.
            foreach (string propertyName in bpList.Keys)
            {
                BrowsableProperty bp = bpList[propertyName];                            // get the name of the property
                EditFormDisplayController controller = new EditFormDisplayController(); // create a controller for this property
                controller.PropertyName = propertyName;                                 // ... set the PropertyName
                controllerDict.Add(controller.PropertyName, controller);                // ... and add to the collection

                // set other properties needed by the controller
                controller.IsReadonly = bp.IsReadonly;
                controller.MustBeUnique = bp.MustBeUnique;
            }

            formParameters.EditObject = newHaiObject as HaiBusinessObjectBase;
            formParameters.ControllerDictionary = controllerDict;
            formParameters.BrowsablePropertyList = bpList;
            formParameters.NumberOfItemsToEdit = 0;

            return formParameters;
        }

        internal void AddAnItemToGrid()
        {
            IUntypedBindingList bindingList = (IUntypedBindingList)((BindingSource)_datagrid.DataSource).List;        // get the data collection
            _callAddProcedure[bindingList.TypeOfBussinessObjectsInList](bindingList);
            _datagrid.Refresh();
        }

        internal List<HaiBusinessObjectBase> GetObjectsSelectedForEdit<T>(DataGridView datagrid)
        {
            // verify that at least one item is selected for editing
            if (datagrid.SelectedRows.Count < 1)
            {
                throw new Exception("Cannot edit zero selected items.");
            }

            HaiBindingList<T> theBindingList = (HaiBindingList<T>)(((BindingSource)datagrid.DataSource).List);        // get the data collection

            // verify that the list contains objects derived from HaiBusinessObjectBase
            T firstObject = (T)theBindingList[datagrid.SelectedRows[0].Index];
            if (!(firstObject is HaiBusinessObjectBase))
            {
                throw new Exception("Edit object must be derived from HaiBusinessObjectBase");
            }

            // collect the objects that will be edited
            List<HaiBusinessObjectBase> objectsSelectedForEdit = new List<HaiBusinessObjectBase>();
            foreach (DataGridViewRow selectedRow in datagrid.SelectedRows)
            {
                objectsSelectedForEdit.Add(theBindingList[selectedRow.Index] as HaiBusinessObjectBase);
            }

            return objectsSelectedForEdit;
        }

        internal EditFormParameters CreateEditFormParametersForEdit(List<HaiBusinessObjectBase> objectsSelectedForEdit, Dictionary<string, BrowsableProperty> bpList)
        {
            EditFormParameters formParameters = new EditFormParameters();   // container for return objects
            HaiBusinessObjectBase anyHaiBusinessObjectToEdit;                                   // object with properties set by the edit
            Dictionary<string, EditFormDisplayController> controllerDict = new Dictionary<string, EditFormDisplayController>(); // dict of items to control form display

            // create a working object that will be used as the target of the edit
            anyHaiBusinessObjectToEdit = (HaiBusinessObjectBase)Activator.CreateInstance(objectsSelectedForEdit[0].GetType());

            // iterate across the properties of all objects to edit
            // ... to determine if any are multivalued, etc.
            foreach (string propertyName in bpList.Keys)
            {
                BrowsableProperty bp = bpList[propertyName];                            // get the name of the property
                EditFormDisplayController controller = new EditFormDisplayController(); // create a controller for this property
                controller.PropertyName = propertyName;                                 // ... set the PropertyName
                controllerDict.Add(controller.PropertyName, controller);                // ... and add to the collection

                // determine whether this property has more than one value among the objects to be editted.
                bool isMultivalued = false;
                object refValue = Utilities.CallByName(objectsSelectedForEdit[0], propertyName, CallType.Get);  // get the value of the first object as a reference

                // see if any objects have a value for this property that is different than the value for the reference
                foreach (HaiBusinessObjectBase anyHaiBusinessObject in objectsSelectedForEdit)
                {
                    if (refValue == null)
                    {
                        if (Utilities.CallByName(anyHaiBusinessObject, propertyName, CallType.Get)!=null)
                        {
                            isMultivalued = true;
                            break;
                        }
                    }
                    else
                    {
                        if (!refValue.Equals(Utilities.CallByName(anyHaiBusinessObject, propertyName, CallType.Get)))
                        {
                            isMultivalued = true;
                            break;
                        }
                    }
                }

                if (isMultivalued)
                {   // mark this property as having multiple values
                    controller.IsMultivalued = true;
                }
                else
                {   // mark this property as having a single value, and set the edit object's property to the common value
                    controller.IsMultivalued = false;
                    Utilities.CallByName(anyHaiBusinessObjectToEdit, propertyName, CallType.Set, refValue);
                }

                // set other properties needed by the controller
                controller.IsReadonly = bp.IsReadonly;
                controller.MustBeUnique = bp.MustBeUnique;
            }

            formParameters.EditObject = anyHaiBusinessObjectToEdit as HaiBusinessObjectBase;
            formParameters.ControllerDictionary = controllerDict;
            formParameters.BrowsablePropertyList = bpList;
            formParameters.NumberOfItemsToEdit = objectsSelectedForEdit.Count;

            return formParameters;
        }

        internal void EditItemsInTheGrid()
        {
            if (_datagrid.SelectedRows.Count == 0)
            {
                MessageBox.Show("Nothing selected.", "", MessageBoxButtons.OK);
                return;
            }

            IUntypedBindingList bindingList = (IUntypedBindingList)((BindingSource)_datagrid.DataSource).List;        // get the data collection
            _callEditProcedure[bindingList.TypeOfBussinessObjectsInList](); // call the edit procedure for the type of business object in the binding list
            _datagrid.Refresh();
        }

        internal bool ContinueAfterCheckForChangedItems(List<HaiBusinessObjectBase> items, Dictionary<string, BrowsableProperty> bpList)
        {
            // refresh the items that have been selected for editting
            // if any have changed, update the items (from the binding list)
            // and give the user an opportunity to cancel further action
            // if there are items that have changed.

            bool isChanged = false;

            foreach (HaiBusinessObjectBase anyHaiBusinessObject in items)
            {
                DataAccessResult result = _ds.DataItemReGet(anyHaiBusinessObject);
                if (!result.Success)
                {
                    MessageBox.Show(result.Message, "Data access error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (result.ItemIsChanged)
                {
                    isChanged = true;
                    HaiBusinessObjectBase updatedHaiBusinessObject = result.SingleItem;
                    foreach (string propertyName in bpList.Keys)
                    {
                        object newValue = Utilities.CallByName(updatedHaiBusinessObject, propertyName, CallType.Get);
                        Utilities.CallByName(anyHaiBusinessObject, propertyName, CallType.Set, newValue);
                    }
                }
            }

            if (isChanged)
            {
                DialogResult answer = MessageBox.Show("The item(s) selected for edit have changed.\n\rDo you want to continue?", "Changed items",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (answer == DialogResult.No)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }

        internal void UpdateBindingListFromEdit(List<HaiBusinessObjectBase> objectsSelectedForEdit, EditFormParameters parameters)
        {
            Dictionary<string, BrowsableProperty> bpList = parameters.BrowsablePropertyList;

            // set the changed properties for the selected items
            foreach (string propertyName in bpList.Keys)
            {
                EditFormDisplayController controller = parameters.ControllerDictionary[propertyName];
                foreach (HaiBusinessObjectBase anyHaiBusinessObject in objectsSelectedForEdit)
                {
                    if (controller.IsChangedByEdit)
                    {
                        object value = Utilities.CallByName(parameters.EditObject, propertyName, CallType.Get);
                        Utilities.CallByName(anyHaiBusinessObject, propertyName, CallType.Set, value);
                    }
                    anyHaiBusinessObject.MarkDirty();
                }
            }

            // for the selected items save changes to the database
            foreach (HaiBusinessObjectBase anyHaiBusinessObject in objectsSelectedForEdit)
            {
                DataAccessResult result = _ds.Save(anyHaiBusinessObject);
                if (!result.Success)
                    MessageBox.Show("Update failed. -- " + result.Message);
            }
        }

        internal void ShowNothingImplemented()
        {
            MessageBox.Show("Nothing implemented.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        #endregion

        #region Menu event handlers

        //Menu: File > Close
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveDataGridControl();
        }

        //Menu:  File > Exit
        private void menuMainFileExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Menu: Grid  --  set visibility of drop down items on Grid menu
        private void gridToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gridToolStripMenuItem.DropDown.Items["toolStripSeparatorColumnItems"].Visible = true;
            gridToolStripMenuItem.DropDown.Items["columnHeadingStyleToolStripMenuItem"].Visible = true;
            gridToolStripMenuItem.DropDown.Items["columnWidthToolStripMenuItem"].Visible = true;
        }

        #endregion

    }
}