﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using HaiBusinessObject;
using HaiBusinessUI;
using IstatMetaDataDAL;

namespace IstatMetaDataPL
{
    public partial class IstatMetaDataEditor : HaiMetaDataEditor
    {
        public IstatMetaDataEditor()
        {
            InitializeComponent();


            InitializeComponentSpecial();

            // set the form and the dictionaries of delegates for procedure calls based on business object type
            SetupForm();
            SetupAddProcedures();
            SetupEditProcedures();

            _ds = new IstatDataService();
        }

        #region Menu Event Handlers
        //Menu:  Codes > Dictionaries > Activities
        private void activitiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Activity_Istat);
            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Istat Code Editor: Activities");
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        //Menu:  Codes > Dictionaries > Attributes
        private void menuCodesDictionariesAttributes_Click(object sender, EventArgs e)
        {
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Attribute);
            if (result.Success)
            {
                string[] orderOfColumnsToShow = {"Description", "ShortDescription", "MeasureName", "UnitsDescription", "Labels", "AverageOrTotal", "SubCnt", "CHLMCDESC", "CHLMCMOSI", 
                    "DataOrder", "ModelOrder", "Key"};
                CreateDataGridControl<HaiBusinessObjectBase>(result, new List<string>(orderOfColumnsToShow), "Istat Code Editor: Attributes");
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        //Menu:  Codes > Dictionaries > Product
        private void menuCodesDictionariesProduct_Click(object sender, EventArgs e)
        {
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.Product_Istat);
            if (result.Success)
            {
                string[] orderOfColumnsToShow = {"PGPD", "ID", "ProgramName", "BatchPrint", "ShortDescription", "LongDescription", "ModelEditMOSIText", 
                    "ModelEditSizeValueText", "ColumnLabel", "ProgramKey", "ProductKey", "OldID1", "OldID2"};
                CreateDataGridControl<HaiBusinessObjectBase>(result, new List<string>(orderOfColumnsToShow), "Istat Code Editor: Product");
            }
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        //Menu:  Codes > Dictionaries > Customer type
        private void menuCodesDictionariesCustomerType_Click(object sender, EventArgs e)
        {
            DataAccessResult result = _ds.GetDataList(HaiBusinessObjectType.CustomerType);
            if (result.Success)
                CreateDataGridControl<HaiBusinessObjectBase>(result, null, "Istat Code Editor: Customer Type");
            else
                MessageBox.Show("Unable to load data: " + result.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        #endregion

        #region Setup Procedures
        private void SetupForm()
        {
            this.Text = "Istat Metadata Editor";
        }

        private void SetupEditProcedures()
        {
            _callEditProcedure = new Dictionary<HaiBusinessObjectType, EditProcedure>();
            _callEditProcedure.Add(HaiBusinessObjectType.Activity_Istat, new EditProcedure(ActivityEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.Attribute, new EditProcedure(AttributeEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.CustomerType, new EditProcedure(CustomerTypeEdit));
            _callEditProcedure.Add(HaiBusinessObjectType.Product_Istat, new EditProcedure(ProductEdit));
        }

        private void SetupAddProcedures()
        {
            _callAddProcedure = new Dictionary<HaiBusinessObjectType, AddProcedure>();
            _callAddProcedure.Add(HaiBusinessObjectType.Activity_Istat, ActivityAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.Attribute, AttributeAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.CustomerType, CustomerTypeAdd);
            _callAddProcedure.Add(HaiBusinessObjectType.Product_Istat, ProductAdd);
        }
        #endregion

        #region Edit Object Procedures

        private void ActivityEdit()
        {
            List<HaiBusinessObjectBase> activitiesToEdit = GetObjectsSelectedForEdit<Activity>(_datagrid);
            Dictionary<string, BrowsableProperty> bpList = Activity.GetBrowsablePropertyList();

            if (ContinueAfterCheckForChangedItems(activitiesToEdit, bpList))
            {
                _datagrid.Refresh();
                EditFormParameters editParameters = CreateEditFormParametersForEdit(activitiesToEdit, bpList);
                ActivityEditForm theForm = new ActivityEditForm(editParameters);
                theForm.ShowDialog();

                if (theForm.DialogResult == DialogResult.OK)
                    UpdateBindingListFromEdit(activitiesToEdit, editParameters);

                theForm.Dispose();
            }
        }

        private void AttributeEdit()
        {
            List<HaiBusinessObjectBase> attributesToEdit = GetObjectsSelectedForEdit<HaiAttribute>(_datagrid);
            Dictionary<string, BrowsableProperty> bpList = HaiAttribute.GetBrowsablePropertyList();

            if (ContinueAfterCheckForChangedItems(attributesToEdit, bpList))
            {
                _datagrid.Refresh();
                EditFormParameters editParameters = CreateEditFormParametersForEdit(attributesToEdit, HaiAttribute.GetBrowsablePropertyList());
                AttributeEditForm theForm = new AttributeEditForm(editParameters);
                theForm.ShowDialog();

                if (theForm.DialogResult == DialogResult.OK)
                    UpdateBindingListFromEdit(attributesToEdit, editParameters);

                theForm.Dispose();
            }
        }

        private void CustomerTypeEdit()
        {
            List<HaiBusinessObjectBase> customerTypesToEdit = GetObjectsSelectedForEdit<CustomerType>(_datagrid);
            Dictionary<string, BrowsableProperty> bpList = CustomerType.GetBrowsablePropertyList();

            if (ContinueAfterCheckForChangedItems(customerTypesToEdit, bpList))
            {
                _datagrid.Refresh();
                EditFormParameters editParameters = CreateEditFormParametersForEdit(customerTypesToEdit, CustomerType.GetBrowsablePropertyList());
                CustomerTypeEditForm theForm = new CustomerTypeEditForm(editParameters);
                theForm.ShowDialog();

                if (theForm.DialogResult == DialogResult.OK)
                    UpdateBindingListFromEdit(customerTypesToEdit, editParameters);

                theForm.Dispose();
            }
        }

        private void ProductEdit()
        {
            List<HaiBusinessObjectBase> productsToEdit = GetObjectsSelectedForEdit<Product>(_datagrid);
            Dictionary<string, BrowsableProperty> bpList = Product.GetBrowsablePropertyList();

            if (ContinueAfterCheckForChangedItems(productsToEdit, bpList))
            {
                _datagrid.Refresh();
                EditFormParameters editParameters = CreateEditFormParametersForEdit(productsToEdit, Product.GetBrowsablePropertyList());
                ProductEditForm theForm = new ProductEditForm(editParameters);
                theForm.ShowDialog();

                if (theForm.DialogResult == DialogResult.OK)
                    UpdateBindingListFromEdit(productsToEdit, editParameters);

                theForm.Dispose();
            }
        }

        #endregion

        #region New Object Add Procedures

        private void ActivityAdd(IUntypedBindingList bindingList)
        {
            EditFormParameters editParameters = CreateEditFormParametersForNew<Activity>(new Activity(), Activity.GetBrowsablePropertyList());
            Form theForm = new ActivityEditForm(editParameters);
            theForm.ShowDialog();

            if (theForm.DialogResult == DialogResult.OK)
            {
                DataAccessResult result = SaveNewHaiBusinessObject(editParameters);
                if (result.Success)
                    ((HaiBindingList<Activity>)bindingList).AddToList(((Activity)editParameters.EditObject));
                else
                    MessageBox.Show(result.Message, "Data save error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            theForm.Dispose();
        }

        private void AttributeAdd(IUntypedBindingList bindingList)
        {
            EditFormParameters editParameters = CreateEditFormParametersForNew<HaiAttribute>(new HaiAttribute(), HaiAttribute.GetBrowsablePropertyList());
            Form theForm = new AttributeEditForm(editParameters);
            theForm.ShowDialog();

            if (theForm.DialogResult == DialogResult.OK)
            {
                DataAccessResult result = SaveNewHaiBusinessObject(editParameters);
                if (result.Success)
                    ((HaiBindingList<HaiAttribute>)bindingList).AddToList(((HaiAttribute)editParameters.EditObject));
                else
                    MessageBox.Show(result.Message, "Data save error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            theForm.Dispose();
        }

        private void CustomerTypeAdd(IUntypedBindingList bindingList)
        {
            EditFormParameters editParameters = CreateEditFormParametersForNew<CustomerType>(new CustomerType(), CustomerType.GetBrowsablePropertyList());
            Form theForm = new CustomerTypeEditForm(editParameters);
            theForm.ShowDialog();

            if (theForm.DialogResult == DialogResult.OK)
            {
                DataAccessResult result = SaveNewHaiBusinessObject(editParameters);
                if (result.Success)
                    ((HaiBindingList<CustomerType>)bindingList).AddToList(((CustomerType)editParameters.EditObject));
                else
                    MessageBox.Show(result.Message, "Data save error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            theForm.Dispose();
        }

        private void ProductAdd(IUntypedBindingList bindingList)
        {
            EditFormParameters editParameters = CreateEditFormParametersForNew<Product>(new Product(), Product.GetBrowsablePropertyList());
            Form theForm = new ProductEditForm(editParameters);
            theForm.ShowDialog();

            if (theForm.DialogResult == DialogResult.OK)
            {
                DataAccessResult result = SaveNewHaiBusinessObject(editParameters);
                if (result.Success)
                    ((HaiBindingList<Product>)bindingList).AddToList(((Product)editParameters.EditObject));
                else
                    MessageBox.Show(result.Message, "Data save error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            theForm.Dispose();
        }

        #endregion


        #region SPECIAL: Designer-like code and declarations for this form

        private System.Windows.Forms.ToolStripMenuItem codesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dictionariesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attributesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem activitiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gridToolStripMenuItem;

        private void InitializeComponentSpecial()
        {

            this.codesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dictionariesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activitiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attributesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();

            // 
            // codesToolStripMenuItem
            // 
            this.codesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dictionariesToolStripMenuItem});
            this.codesToolStripMenuItem.Name = "codesToolStripMenuItem";
            this.codesToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.codesToolStripMenuItem.Text = "&Codes";
            // 
            // dictionariesToolStripMenuItem
            // 
            this.dictionariesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.activitiesToolStripMenuItem,
            this.attributesToolStripMenuItem,
            this.customerTypeToolStripMenuItem,
            this.productToolStripMenuItem});
            this.dictionariesToolStripMenuItem.Name = "dictionariesToolStripMenuItem";
            this.dictionariesToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.dictionariesToolStripMenuItem.Text = "&Dictionaries";
            // 
            // activitiesToolStripMenuItem
            // 
            this.activitiesToolStripMenuItem.Name = "activitiesToolStripMenuItem";
            this.activitiesToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.activitiesToolStripMenuItem.Text = "Ac&tivities";
            this.activitiesToolStripMenuItem.Click += new System.EventHandler(this.activitiesToolStripMenuItem_Click);
            // 
            // attributesToolStripMenuItem
            // 
            this.attributesToolStripMenuItem.Name = "attributesToolStripMenuItem";
            this.attributesToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.attributesToolStripMenuItem.Text = "&Attributes";
            this.attributesToolStripMenuItem.Click += new System.EventHandler(this.menuCodesDictionariesAttributes_Click);
            // 
            // customerTypeToolStripMenuItem
            // 
            this.customerTypeToolStripMenuItem.Name = "menuCodesDictionariesCustomerType";
            this.customerTypeToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.customerTypeToolStripMenuItem.Text = "&Customer type";
            this.customerTypeToolStripMenuItem.Click += new System.EventHandler(this.menuCodesDictionariesCustomerType_Click);
            // 
            // productToolStripMenuItem
            // 
            this.productToolStripMenuItem.Name = "menuCodesDictionariesProduct";
            this.productToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.productToolStripMenuItem.Text = "&Product";
            this.productToolStripMenuItem.Click += new System.EventHandler(this.menuCodesDictionariesProduct_Click);

            _mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.codesToolStripMenuItem});
        }
        #endregion
    }
}
