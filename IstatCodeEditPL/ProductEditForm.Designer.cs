namespace IstatMetaDataPL
{
    partial class ProductEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label batchPrintLabel;
            System.Windows.Forms.Label columnLabelLabel;
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label longDescriptionLabel;
            System.Windows.Forms.Label modelEditMOSITextLabel;
            System.Windows.Forms.Label modelEditSizeValueTextLabel;
            System.Windows.Forms.Label oldID1Label;
            System.Windows.Forms.Label oldID2Label;
            System.Windows.Forms.Label pGPDLabel;
            System.Windows.Forms.Label productKeyLabel;
            System.Windows.Forms.Label programKeyLabel;
            System.Windows.Forms.Label programNameLabel;
            System.Windows.Forms.Label shortDescriptionLabel;
            this.productBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.batchPrintCheckBox = new System.Windows.Forms.CheckBox();
            this.columnLabelTextBox = new System.Windows.Forms.TextBox();
            this.iDTextBox = new System.Windows.Forms.TextBox();
            this.longDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.modelEditMOSITextTextBox = new System.Windows.Forms.TextBox();
            this.modelEditSizeValueTextTextBox = new System.Windows.Forms.TextBox();
            this.oldID1TextBox = new System.Windows.Forms.TextBox();
            this.oldID2TextBox = new System.Windows.Forms.TextBox();
            this.pGPDTextBox = new System.Windows.Forms.TextBox();
            this.productKeyTextBox = new System.Windows.Forms.TextBox();
            this.programKeyTextBox = new System.Windows.Forms.TextBox();
            this.programNameComboBox = new System.Windows.Forms.ComboBox();
            this.shortDescriptionTextBox = new System.Windows.Forms.TextBox();
            batchPrintLabel = new System.Windows.Forms.Label();
            columnLabelLabel = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            longDescriptionLabel = new System.Windows.Forms.Label();
            modelEditMOSITextLabel = new System.Windows.Forms.Label();
            modelEditSizeValueTextLabel = new System.Windows.Forms.Label();
            oldID1Label = new System.Windows.Forms.Label();
            oldID2Label = new System.Windows.Forms.Label();
            pGPDLabel = new System.Windows.Forms.Label();
            productKeyLabel = new System.Windows.Forms.Label();
            programKeyLabel = new System.Windows.Forms.Label();
            programNameLabel = new System.Windows.Forms.Label();
            shortDescriptionLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(363, 468);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(444, 468);
            // 
            // btnInsertNull
            // 
            this.btnInsertNull.Location = new System.Drawing.Point(12, 468);
            // 
            // batchPrintLabel
            // 
            batchPrintLabel.AutoSize = true;
            batchPrintLabel.Location = new System.Drawing.Point(16, 27);
            batchPrintLabel.Name = "batchPrintLabel";
            batchPrintLabel.Size = new System.Drawing.Size(62, 13);
            batchPrintLabel.TabIndex = 28;
            batchPrintLabel.Text = "Batch Print:";
            // 
            // columnLabelLabel
            // 
            columnLabelLabel.AutoSize = true;
            columnLabelLabel.Location = new System.Drawing.Point(16, 55);
            columnLabelLabel.Name = "columnLabelLabel";
            columnLabelLabel.Size = new System.Drawing.Size(74, 13);
            columnLabelLabel.TabIndex = 30;
            columnLabelLabel.Text = "Column Label:";
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Location = new System.Drawing.Point(16, 81);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(21, 13);
            iDLabel.TabIndex = 32;
            iDLabel.Text = "ID:";
            // 
            // longDescriptionLabel
            // 
            longDescriptionLabel.AutoSize = true;
            longDescriptionLabel.Location = new System.Drawing.Point(16, 107);
            longDescriptionLabel.Name = "longDescriptionLabel";
            longDescriptionLabel.Size = new System.Drawing.Size(90, 13);
            longDescriptionLabel.TabIndex = 34;
            longDescriptionLabel.Text = "Long Description:";
            // 
            // modelEditMOSITextLabel
            // 
            modelEditMOSITextLabel.AutoSize = true;
            modelEditMOSITextLabel.Location = new System.Drawing.Point(16, 133);
            modelEditMOSITextLabel.Name = "modelEditMOSITextLabel";
            modelEditMOSITextLabel.Size = new System.Drawing.Size(111, 13);
            modelEditMOSITextLabel.TabIndex = 36;
            modelEditMOSITextLabel.Text = "Model Edit MOSIText:";
            // 
            // modelEditSizeValueTextLabel
            // 
            modelEditSizeValueTextLabel.AutoSize = true;
            modelEditSizeValueTextLabel.Location = new System.Drawing.Point(16, 159);
            modelEditSizeValueTextLabel.Name = "modelEditSizeValueTextLabel";
            modelEditSizeValueTextLabel.Size = new System.Drawing.Size(137, 13);
            modelEditSizeValueTextLabel.TabIndex = 38;
            modelEditSizeValueTextLabel.Text = "Model Edit Size Value Text:";
            // 
            // oldID1Label
            // 
            oldID1Label.AutoSize = true;
            oldID1Label.Location = new System.Drawing.Point(16, 185);
            oldID1Label.Name = "oldID1Label";
            oldID1Label.Size = new System.Drawing.Size(46, 13);
            oldID1Label.TabIndex = 40;
            oldID1Label.Text = "Old ID1:";
            // 
            // oldID2Label
            // 
            oldID2Label.AutoSize = true;
            oldID2Label.Location = new System.Drawing.Point(16, 211);
            oldID2Label.Name = "oldID2Label";
            oldID2Label.Size = new System.Drawing.Size(46, 13);
            oldID2Label.TabIndex = 42;
            oldID2Label.Text = "Old ID2:";
            // 
            // pGPDLabel
            // 
            pGPDLabel.AutoSize = true;
            pGPDLabel.Location = new System.Drawing.Point(16, 237);
            pGPDLabel.Name = "pGPDLabel";
            pGPDLabel.Size = new System.Drawing.Size(40, 13);
            pGPDLabel.TabIndex = 44;
            pGPDLabel.Text = "PGPD:";
            // 
            // productKeyLabel
            // 
            productKeyLabel.AutoSize = true;
            productKeyLabel.Location = new System.Drawing.Point(16, 263);
            productKeyLabel.Name = "productKeyLabel";
            productKeyLabel.Size = new System.Drawing.Size(68, 13);
            productKeyLabel.TabIndex = 46;
            productKeyLabel.Text = "Product Key:";
            // 
            // programKeyLabel
            // 
            programKeyLabel.AutoSize = true;
            programKeyLabel.Location = new System.Drawing.Point(16, 289);
            programKeyLabel.Name = "programKeyLabel";
            programKeyLabel.Size = new System.Drawing.Size(70, 13);
            programKeyLabel.TabIndex = 48;
            programKeyLabel.Text = "Program Key:";
            // 
            // programNameLabel
            // 
            programNameLabel.AutoSize = true;
            programNameLabel.Location = new System.Drawing.Point(16, 315);
            programNameLabel.Name = "programNameLabel";
            programNameLabel.Size = new System.Drawing.Size(80, 13);
            programNameLabel.TabIndex = 50;
            programNameLabel.Text = "Program Name:";
            // 
            // shortDescriptionLabel
            // 
            shortDescriptionLabel.AutoSize = true;
            shortDescriptionLabel.Location = new System.Drawing.Point(16, 342);
            shortDescriptionLabel.Name = "shortDescriptionLabel";
            shortDescriptionLabel.Size = new System.Drawing.Size(91, 13);
            shortDescriptionLabel.TabIndex = 52;
            shortDescriptionLabel.Text = "Short Description:";
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataSource = typeof(IstatMetaDataDAL.Product);
            // 
            // batchPrintCheckBox
            // 
            this.batchPrintCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.productBindingSource, "BatchPrint", true));
            this.batchPrintCheckBox.Location = new System.Drawing.Point(159, 22);
            this.batchPrintCheckBox.Name = "batchPrintCheckBox";
            this.batchPrintCheckBox.Size = new System.Drawing.Size(121, 24);
            this.batchPrintCheckBox.TabIndex = 29;
            this.batchPrintCheckBox.UseVisualStyleBackColor = true;
            // 
            // columnLabelTextBox
            // 
            this.columnLabelTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ColumnLabel", true));
            this.columnLabelTextBox.Location = new System.Drawing.Point(159, 52);
            this.columnLabelTextBox.Name = "columnLabelTextBox";
            this.columnLabelTextBox.Size = new System.Drawing.Size(121, 20);
            this.columnLabelTextBox.TabIndex = 31;
            // 
            // iDTextBox
            // 
            this.iDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ID", true));
            this.iDTextBox.Location = new System.Drawing.Point(159, 78);
            this.iDTextBox.Name = "iDTextBox";
            this.iDTextBox.Size = new System.Drawing.Size(121, 20);
            this.iDTextBox.TabIndex = 33;
            // 
            // longDescriptionTextBox
            // 
            this.longDescriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "LongDescription", true));
            this.longDescriptionTextBox.Location = new System.Drawing.Point(159, 104);
            this.longDescriptionTextBox.Name = "longDescriptionTextBox";
            this.longDescriptionTextBox.Size = new System.Drawing.Size(121, 20);
            this.longDescriptionTextBox.TabIndex = 35;
            // 
            // modelEditMOSITextTextBox
            // 
            this.modelEditMOSITextTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ModelEditMOSIText", true));
            this.modelEditMOSITextTextBox.Location = new System.Drawing.Point(159, 130);
            this.modelEditMOSITextTextBox.Name = "modelEditMOSITextTextBox";
            this.modelEditMOSITextTextBox.Size = new System.Drawing.Size(121, 20);
            this.modelEditMOSITextTextBox.TabIndex = 37;
            // 
            // modelEditSizeValueTextTextBox
            // 
            this.modelEditSizeValueTextTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ModelEditSizeValueText", true));
            this.modelEditSizeValueTextTextBox.Location = new System.Drawing.Point(159, 156);
            this.modelEditSizeValueTextTextBox.Name = "modelEditSizeValueTextTextBox";
            this.modelEditSizeValueTextTextBox.Size = new System.Drawing.Size(121, 20);
            this.modelEditSizeValueTextTextBox.TabIndex = 39;
            // 
            // oldID1TextBox
            // 
            this.oldID1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "OldID1", true));
            this.oldID1TextBox.Location = new System.Drawing.Point(159, 182);
            this.oldID1TextBox.Name = "oldID1TextBox";
            this.oldID1TextBox.Size = new System.Drawing.Size(121, 20);
            this.oldID1TextBox.TabIndex = 41;
            // 
            // oldID2TextBox
            // 
            this.oldID2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "OldID2", true));
            this.oldID2TextBox.Location = new System.Drawing.Point(159, 208);
            this.oldID2TextBox.Name = "oldID2TextBox";
            this.oldID2TextBox.Size = new System.Drawing.Size(121, 20);
            this.oldID2TextBox.TabIndex = 43;
            // 
            // pGPDTextBox
            // 
            this.pGPDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "PGPD", true));
            this.pGPDTextBox.Location = new System.Drawing.Point(159, 234);
            this.pGPDTextBox.Name = "pGPDTextBox";
            this.pGPDTextBox.Size = new System.Drawing.Size(121, 20);
            this.pGPDTextBox.TabIndex = 45;
            // 
            // productKeyTextBox
            // 
            this.productKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ProductKey", true));
            this.productKeyTextBox.Location = new System.Drawing.Point(159, 260);
            this.productKeyTextBox.Name = "productKeyTextBox";
            this.productKeyTextBox.Size = new System.Drawing.Size(121, 20);
            this.productKeyTextBox.TabIndex = 47;
            // 
            // programKeyTextBox
            // 
            this.programKeyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ProgramKey", true));
            this.programKeyTextBox.Location = new System.Drawing.Point(159, 286);
            this.programKeyTextBox.Name = "programKeyTextBox";
            this.programKeyTextBox.Size = new System.Drawing.Size(121, 20);
            this.programKeyTextBox.TabIndex = 49;
            // 
            // programNameComboBox
            // 
            this.programNameComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ProgramName", true));
            this.programNameComboBox.FormattingEnabled = true;
            this.programNameComboBox.Location = new System.Drawing.Point(159, 312);
            this.programNameComboBox.Name = "programNameComboBox";
            this.programNameComboBox.Size = new System.Drawing.Size(121, 21);
            this.programNameComboBox.TabIndex = 51;
            // 
            // shortDescriptionTextBox
            // 
            this.shortDescriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ShortDescription", true));
            this.shortDescriptionTextBox.Location = new System.Drawing.Point(159, 339);
            this.shortDescriptionTextBox.Name = "shortDescriptionTextBox";
            this.shortDescriptionTextBox.Size = new System.Drawing.Size(121, 20);
            this.shortDescriptionTextBox.TabIndex = 53;
            // 
            // ProductEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 501);
            this.Controls.Add(batchPrintLabel);
            this.Controls.Add(this.batchPrintCheckBox);
            this.Controls.Add(columnLabelLabel);
            this.Controls.Add(this.columnLabelTextBox);
            this.Controls.Add(iDLabel);
            this.Controls.Add(this.iDTextBox);
            this.Controls.Add(longDescriptionLabel);
            this.Controls.Add(this.longDescriptionTextBox);
            this.Controls.Add(modelEditMOSITextLabel);
            this.Controls.Add(this.modelEditMOSITextTextBox);
            this.Controls.Add(modelEditSizeValueTextLabel);
            this.Controls.Add(this.modelEditSizeValueTextTextBox);
            this.Controls.Add(oldID1Label);
            this.Controls.Add(this.oldID1TextBox);
            this.Controls.Add(oldID2Label);
            this.Controls.Add(this.oldID2TextBox);
            this.Controls.Add(pGPDLabel);
            this.Controls.Add(this.pGPDTextBox);
            this.Controls.Add(productKeyLabel);
            this.Controls.Add(this.productKeyTextBox);
            this.Controls.Add(programKeyLabel);
            this.Controls.Add(this.programKeyTextBox);
            this.Controls.Add(programNameLabel);
            this.Controls.Add(this.programNameComboBox);
            this.Controls.Add(shortDescriptionLabel);
            this.Controls.Add(this.shortDescriptionTextBox);
            this.Name = "ProductEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Product";
            this.Load += new System.EventHandler(this.ProductEditForm_Load);
            this.Controls.SetChildIndex(this.btnInsertNull, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.shortDescriptionTextBox, 0);
            this.Controls.SetChildIndex(shortDescriptionLabel, 0);
            this.Controls.SetChildIndex(this.programNameComboBox, 0);
            this.Controls.SetChildIndex(programNameLabel, 0);
            this.Controls.SetChildIndex(this.programKeyTextBox, 0);
            this.Controls.SetChildIndex(programKeyLabel, 0);
            this.Controls.SetChildIndex(this.productKeyTextBox, 0);
            this.Controls.SetChildIndex(productKeyLabel, 0);
            this.Controls.SetChildIndex(this.pGPDTextBox, 0);
            this.Controls.SetChildIndex(pGPDLabel, 0);
            this.Controls.SetChildIndex(this.oldID2TextBox, 0);
            this.Controls.SetChildIndex(oldID2Label, 0);
            this.Controls.SetChildIndex(this.oldID1TextBox, 0);
            this.Controls.SetChildIndex(oldID1Label, 0);
            this.Controls.SetChildIndex(this.modelEditSizeValueTextTextBox, 0);
            this.Controls.SetChildIndex(modelEditSizeValueTextLabel, 0);
            this.Controls.SetChildIndex(this.modelEditMOSITextTextBox, 0);
            this.Controls.SetChildIndex(modelEditMOSITextLabel, 0);
            this.Controls.SetChildIndex(this.longDescriptionTextBox, 0);
            this.Controls.SetChildIndex(longDescriptionLabel, 0);
            this.Controls.SetChildIndex(this.iDTextBox, 0);
            this.Controls.SetChildIndex(iDLabel, 0);
            this.Controls.SetChildIndex(this.columnLabelTextBox, 0);
            this.Controls.SetChildIndex(columnLabelLabel, 0);
            this.Controls.SetChildIndex(this.batchPrintCheckBox, 0);
            this.Controls.SetChildIndex(batchPrintLabel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource productBindingSource;
        private System.Windows.Forms.CheckBox batchPrintCheckBox;
        private System.Windows.Forms.TextBox columnLabelTextBox;
        private System.Windows.Forms.TextBox iDTextBox;
        private System.Windows.Forms.TextBox longDescriptionTextBox;
        private System.Windows.Forms.TextBox modelEditMOSITextTextBox;
        private System.Windows.Forms.TextBox modelEditSizeValueTextTextBox;
        private System.Windows.Forms.TextBox oldID1TextBox;
        private System.Windows.Forms.TextBox oldID2TextBox;
        private System.Windows.Forms.TextBox pGPDTextBox;
        private System.Windows.Forms.TextBox productKeyTextBox;
        private System.Windows.Forms.TextBox programKeyTextBox;
        private System.Windows.Forms.ComboBox programNameComboBox;
        private System.Windows.Forms.TextBox shortDescriptionTextBox;

    }
}