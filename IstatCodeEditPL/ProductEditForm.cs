using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using HaiBusinessObject;
using IstatMetaDataDAL;
using HaiBusinessUI;

namespace IstatMetaDataPL
{
    public partial class ProductEditForm : HaiObjectEditForm
    {
        private Product _productToEdit;
        private Product _workingProduct;

        private HaiBindingList<IstatMetaDataDAL.HaiProgram> _programList;
        private System.Collections.Hashtable _programHashTbl;

        public ProductEditForm(EditFormParameters parameters)
        {
            InitializeComponent();

            SetFormVariables(parameters);
            _productToEdit = (Product)parameters.EditObject;
            _workingProduct = new Product();

            // set the values of a working object to the values of the object that is the target of the edit
            SetPropertiesInAnyObject(_workingProduct, _productToEdit, _browsablePropertyList);

            productBindingSource.DataSource = _workingProduct;
            btnOK.Click+=new EventHandler(btnOK_Click);

        }

        private void ProductEditForm_Load(object sender, EventArgs e)
        {
            programNameComboBox.DropDownStyle = ComboBoxStyle.DropDownList;

            // assign ComboBox items source to the ComboBox.Tag
            IstatDataService ds = new IstatDataService();
            DataAccessResult result = ds.GetDataList(HaiBusinessObjectType.Program);

            if (result.Success)
            {
                _programList = (HaiBindingList<IstatMetaDataDAL.HaiProgram>)result.DataList;
                _programHashTbl = new System.Collections.Hashtable(_programList.ToDictionary(x => x.ShortDescription));
                programNameComboBox.Tag = _programHashTbl;
            }

            // if the edit item is multivalued then assign handler to lock/unlock the ComboBox
            // ... and set the background color to "readonly"
            EditFormDisplayController programNameController = _controllerList["ProgramName"];
            if (programNameController.IsMultivalued)
            {
                programNameComboBox.KeyDown += new KeyEventHandler(ComboBox_KeyDown);
                programNameComboBox.BackColor = SystemColors.Control;
            }
            else
            {
                SetListForComboBox(programNameComboBox);
                programNameComboBox.Text = _productToEdit.ProgramName;
                programNameComboBox.BackColor = SystemColors.Window;
            }

            PrepareTheFormToShow(_parameters);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // set the property values in the object that was the target of the edit
            HarvestInputValues(_workingProduct);
            SetPropertiesInAnyObject(_productToEdit, _workingProduct, _browsablePropertyList);

            // set the ProgramKey (not on form) based on the Program that was selected by name
            if (programNameComboBox.SelectedItem != null)
            {
                IstatMetaDataDAL.HaiProgram program = (IstatMetaDataDAL.HaiProgram)(_programHashTbl[programNameComboBox.SelectedItem.ToString()]);
                _productToEdit.ProgramKey = program.ProgramKey;
            }

            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}