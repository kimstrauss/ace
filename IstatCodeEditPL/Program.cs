using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace IstatMetaDataPL
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Application.Run(new HaiMetaDataEditor());

            Application.Run(new IstatMetaDataEditor());

        }
    }
}