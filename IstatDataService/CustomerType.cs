using System.Collections.Generic;

using HaiBusinessObject;

namespace IstatMetaDataDAL
{
    public class CustomerType : HaiBusinessObjectBase
    {
        protected static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {

            if (_browsablePropertyList == null)
            {
                _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            }
            if (_browsablePropertyList.Count == 0)
                BuildPropertyList();

            return _browsablePropertyList;
        }

        public CustomerType()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.CustomerType;
        }

        public int ID
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public override string UniqueIdentifier
        {
            get
            {
                return Description + " [" + ID.ToString() + "]";
            }
        }

        public string SortText
        {
            get;
            set;
        }

        public int Key
        {
            get;
            set;
        }

        private static void BuildPropertyList()
        {
            BrowsableProperty bp;
            bp = new BrowsableProperty("ID", "ID", "Identifier", "Identifier");
            bp.MustBeUnique = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Description", "DESC", "Customer description", "Description");
            bp.MaxStringLength = 36;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("SortText", "SORT", "Text to use for sorting", "Sort text");
            bp.MustBeUnique = true;
            bp.MaxStringLength = 12;
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("Key", "lcuidKey", "Primary key", "Primary key");
            bp.IsReadonly = true;
            bp.MustBeUnique = true;
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
    }
}
