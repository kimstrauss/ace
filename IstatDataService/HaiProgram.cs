using System;
using System.Collections.Generic;

using HaiBusinessObject;

namespace IstatMetaDataDAL
{
    public class HaiProgram : HaiBusinessObjectBase
    {
        protected static Dictionary<string, BrowsableProperty> _browsablePropertyList;

        public static Dictionary<string, BrowsableProperty> GetBrowsablePropertyList()
        {
            if (_browsablePropertyList == null)
            {
                _browsablePropertyList = new Dictionary<string, BrowsableProperty>();
            }
            if (_browsablePropertyList.Count == 0)
                BuildPropertyList();
            return _browsablePropertyList;
        }

        public HaiProgram()
            : base()
        {
            _haiBusinessObjectType = HaiBusinessObjectType.Program;
            _ID = string.Empty;
            _shortDescription = string.Empty;
            _longDescription = string.Empty;
            _oldID1 = string.Empty;
            _oldID2 = string.Empty;
        }

        private int _LPGSysid;
        public int ProgramKey
        {
            get
            {
                return _LPGSysid;
            }
            set
            {
                _LPGSysid = value;
            }
        }

        private string _ID;
        public string ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }

        private string _shortDescription;
        public string ShortDescription
        {
            get
            {
                return _shortDescription;
            }
            set
            {
                _shortDescription = value;
            }
        }

        private string _longDescription;
        public string LongDescription
        {
            get
            {
                return _longDescription;
            }
            set
            {
                _longDescription = value;
            }
        }

        public override string UniqueIdentifier
        {
            get
            {
                return LongDescription + " [" + ID.ToString() + "]";
            }
        }

        private string _oldID1;
        public string OldID1
        {
            get
            {
                return _oldID1;
            }
            set
            {
                _oldID1 = value;
            }
        }

        private string _oldID2;
        public string OldID2
        {
            get
            {
                return _oldID2;
            }
            set
            {
                _oldID2 = value;
            }
        }

        private static void BuildPropertyList()
        {
            BrowsableProperty bp;

            bp = new BrowsableProperty("ProgramKey", "LPGSysid", "Foreign key for program", "Key", TypeCode.Int32);
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ID", "ID", "ID", "ID", TypeCode.String);
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("ShortDescription", "SDESC", "Short description", "Short description", TypeCode.String);
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("LongDescription", "LDESC", "Long description", "Long description", TypeCode.String);
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("OldID1", "OID1", "Old ID 1", "Old ID 1", TypeCode.String);
            _browsablePropertyList.Add(bp.PropertyName, bp);

            bp = new BrowsableProperty("OldID2", "OID2", "Old ID 2", "Old ID 2", TypeCode.String);
            _browsablePropertyList.Add(bp.PropertyName, bp);
        }
    }
}
