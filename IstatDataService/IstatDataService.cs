using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

using HaiBusinessObject;
using HaiMetaDataDAL;

namespace IstatMetaDataDAL
{
    public class IstatDataService : IMetaDataDS
    {
        private string GetEmioperConnectionString()
        {
            return @"Data Source=sql0\qatr;Initial Catalog=EMIOPER;Integrated Security=True";
            //return @"Data Source=sql0\DevTr2;Initial Catalog=EMIOPER;Integrated Security=True";
        }

        public DataAccessResult GetDataList(HaiBusinessObjectType objectType)
        {
            switch (objectType)
            {
                case HaiBusinessObjectType.CustomerType :
                    return CustomerTypeListGet(null);
                case HaiBusinessObjectType.Activity_Istat :
                    return ActivityListGet(null);
                case HaiBusinessObjectType.Attribute :
                    return AttributeListGet(null);
                case HaiBusinessObjectType.Product_Istat :
                    return ProductListGet(null);
                case HaiBusinessObjectType.Program:
                    return ProgramListGet(null);
                default:
                    DataAccessResult errorResult = new DataAccessResult();
                    errorResult.Message = "Unsupported data type requested: " + objectType.ToString();
                    errorResult.Success = false;
                    return errorResult;
            }
        }

        public DataAccessResult DataItemReGet(HaiBusinessObjectBase anyHaiBusinessObject)
        {
            switch (anyHaiBusinessObject.ObjectType)
            {
                case HaiBusinessObjectType.Activity_Istat:
                    return ActivityGet((Activity)anyHaiBusinessObject);
                case HaiBusinessObjectType.Attribute:
                    return AttributeGet((HaiAttribute)anyHaiBusinessObject);
                case HaiBusinessObjectType.CustomerType:
                    return CustomerTypeGet((CustomerType)anyHaiBusinessObject);
                case HaiBusinessObjectType.Product_Istat:
                    return ProductGet((Product)anyHaiBusinessObject);
                case HaiBusinessObjectType.Program:
                    return ProgramGet((HaiProgram)anyHaiBusinessObject);
                default:
                    DataAccessResult errorResult = new DataAccessResult();
                    errorResult.Message = "Unsupported data type requested: " + anyHaiBusinessObject.ObjectType.ToString();
                    errorResult.Success = false;
                    return errorResult;
            }
        }

        public DataAccessResult Save(HaiBusinessObjectBase anyHaiBusinessObject)
        {
            switch (anyHaiBusinessObject.ObjectType)
            {
                case HaiBusinessObjectType.Attribute:
                    return AttributeSave((HaiAttribute)anyHaiBusinessObject);
                case HaiBusinessObjectType.Activity_Istat:
                    return ActivitySave((Activity)anyHaiBusinessObject);
                case HaiBusinessObjectType.CustomerType:
                    return CustomerTypeSave((CustomerType)anyHaiBusinessObject);
                case HaiBusinessObjectType.Product_Istat:
                    return ProductSave((Product)anyHaiBusinessObject);
                default:
                    DataAccessResult errorResult = new DataAccessResult();
                    errorResult.Message = "Unsupported data type requested: " + anyHaiBusinessObject.ObjectType.ToString();
                    errorResult.Success = false;
                    return errorResult;
            }
        }

        #region "Activity"
        private DataAccessResult ActivityListGet(Activity activityToGet)
        {
            DataAccessResult result = new DataAccessResult();
            string connectionString = GetEmioperConnectionString();
            HaiBindingList<Activity> activities = new HaiBindingList<Activity>();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(connectionString);
            

            try
            {
                List<LACID> LACIDs;
                if (activityToGet == null)
                    LACIDs = emiOper.LACIDs.ToList<LACID>();
                else
                {
                    LACIDs = new List<LACID>();
                    LACIDs.Add(emiOper.LACIDs.Single<LACID>(l => (l.ID == activityToGet.ID)));
                }

                foreach (LACID lacid in LACIDs)
                {

                    Activity activity = new Activity();

                    activity.Description = lacid.DESC.Trim();
                    activity.ID = lacid.ID;
                    activity.LACIDKey = lacid.LACIDKey;
                    if (lacid.MEAS.HasValue)
                        activity.Measure = lacid.MEAS.ToString().Trim();
                    activity.ParentID = lacid.ParentID;
                    activity.RollLevel = lacid.RollLevel;
                    activity.Sort = lacid.SORT.Trim();
                    activity.StockFlow = lacid.STKFLW.ToString().Trim();

                    activity.MarkOld();
                    activities.AddToList(activity);
                }

                result.DataList = activities;
                result.Success = true;
                result.BrowsablePropertyList = Activity.GetBrowsablePropertyList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult ActivityGet(Activity activityToGet)
        {
            DataAccessResult result = ActivityListGet(activityToGet);
            if (result.Success)
            {
                Activity activity = (Activity)(result.SingleItem);
                bool isChanged = false;
                if (activityToGet.Description != activity.Description) isChanged = true;
                if (activityToGet.ID != activity.ID) isChanged = true;
                if (activityToGet.LACIDKey != activity.LACIDKey) isChanged = true;
                if (activityToGet.Measure != activity.Measure) isChanged = true;
                if (activityToGet.ParentID != activity.ParentID) isChanged = true;
                if (activityToGet.RollLevel != activity.RollLevel) isChanged = true;
                if (activityToGet.Sort != activity.Sort) isChanged = true;
                if (activityToGet.StockFlow != activity.StockFlow) isChanged = true;

                activityToGet.MarkOld();
                result.ItemIsChanged = isChanged;
            }

            return result;
        }

        private DataAccessResult ActivityInsert(Activity activity)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());

            try
            {
                LACID lacid = new LACID();
                lacid.DESC = activity.Description;
                lacid.ID = (short)activity.ID;

                if (activity.Measure == null)
                {
                    lacid.MEAS = null;
                }
                else
                {
                    if (activity.Measure.Length == 0)
                    {
                        lacid.MEAS = " "[0];
                    }
                    else
                    {
                        lacid.MEAS = activity.Measure[0];
                    }
                }

                if (activity.ParentID.HasValue)
                    lacid.ParentID = (short)activity.ParentID;
                else
                    lacid.ParentID = null;

                if (activity.RollLevel.HasValue)
                    lacid.RollLevel = (short)activity.RollLevel;
                else
                    lacid.RollLevel = null;

                lacid.SORT = activity.Sort;

                lacid.STKFLW = (char)activity.StockFlow[0];

                emiOper.GetTable<LACID>().InsertOnSubmit(lacid);
                emiOper.SubmitChanges();

                activity.LACIDKey = lacid.LACIDKey;

                activity.MarkOld();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult ActivityUpdate(Activity activity)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());
            try
            {
                LACID lacid = emiOper.LACIDs.Single<LACID>(l => (l.ID == activity.ID));
                lacid.DESC = activity.Description;
                lacid.ID = (short)activity.ID;

                if (activity.Measure == null)
                    lacid.MEAS = null;
                else
                {
                    if (activity.Measure.Length == 0)
                        lacid.MEAS = " "[0];
                    else
                        lacid.MEAS = activity.Measure[0];
                }

                if (activity.ParentID.HasValue)
                    lacid.ParentID = (short)activity.ParentID;
                else
                    lacid.ParentID = null;

                if (activity.RollLevel.HasValue)
                    lacid.RollLevel = (short)activity.RollLevel;
                else
                    lacid.RollLevel = null;

                lacid.SORT = activity.Sort;
                lacid.STKFLW = (char)activity.StockFlow[0];

                emiOper.SubmitChanges();

                activity.LACIDKey = lacid.LACIDKey;
                
                emiOper.SubmitChanges();

                activity.MarkOld();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult ActivityDelete(Activity activity)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());

            try
            {
                LACID lacid = emiOper.LACIDs.Single<LACID>(l => (l.ID == activity.ID));
                emiOper.LACIDs.DeleteOnSubmit(lacid);
                emiOper.SubmitChanges();

                activity.MarkClean();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult ActivitySave(Activity activity)
        {
            DataAccessResult result;
            if (activity.IsSavable)
            {
                if (activity.IsDeleted)
                    result = ActivityDelete(activity);
                else if (activity.IsNew)
                    result = ActivityInsert(activity);
                else if (activity.IsDirty)
                    result = ActivityUpdate(activity);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        #endregion

        #region "Attribute"
        private DataAccessResult AttributeListGet(HaiAttribute attributeToGet)
        {
            DataAccessResult result = new DataAccessResult();
            string connectionString = GetEmioperConnectionString();
            HaiBindingList<HaiAttribute> attributes = new HaiBindingList<HaiAttribute>();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(connectionString);

            try
            {
                List<LATID> LATIDs;
                if (attributeToGet == null)
                    LATIDs = emiOper.LATIDs.ToList<LATID>();
                else
                {
                    LATIDs = new List<LATID>();
                    LATIDs.Add(emiOper.LATIDs.Single<LATID>(x => x.LATIDkey == attributeToGet.Key));
                }

                foreach (LATID latid in LATIDs)
                {
                    HaiAttribute attribute = new HaiAttribute();

                    attribute.AverageOrTotal = latid.AVGTOT.ToString().Trim();
                    if (latid.CHLMCDESC.HasValue)
                        attribute.CHLMCDESC = latid.CHLMCDESC.ToString().Trim();
                    if (latid.CHLMCMOSI.HasValue)
                        attribute.CHLMCMOSI = latid.CHLMCMOSI.ToString().Trim();
                    if (latid.DataOrder.HasValue)
                        attribute.DataOrder = (int)latid.DataOrder;
                    else
                        attribute.DataOrder = null;
                    attribute.Description = latid.DESC.Trim();
                    attribute.ID = latid.ID;
                    attribute.Labels = latid.LABELS.Trim();
                    attribute.Key = latid.LATIDkey;
                    if (latid.MeasureName != null)
                        attribute.MeasureName = latid.MeasureName.Trim();
                    if (latid.ModelOrder.HasValue)
                        attribute.ModelOrder = (int)latid.ModelOrder;
                    attribute.ShortDescription = latid.SDESC.Trim();
                    attribute.SubCnt = latid.SUBCNT.ToString().Trim();
                    if (latid.UDESC != null)
                        attribute.UnitsDescription = latid.UDESC.Trim();

                    attribute.MarkOld();
                    attributes.AddToList(attribute);
                }

                result.DataList = attributes;
                result.Success = true;
                result.BrowsablePropertyList = HaiAttribute.GetBrowsablePropertyList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult AttributeGet(HaiAttribute attributeToGet)
        {
            DataAccessResult result = AttributeListGet(attributeToGet);
            if (result.Success)
            {
                HaiAttribute attribute = (HaiAttribute)(result.SingleItem);
                bool isChanged = false;
                if (attributeToGet.AverageOrTotal != attribute.AverageOrTotal) isChanged = true;
                if (attributeToGet.CHLMCDESC != attribute.CHLMCDESC) isChanged = true;
                if (attributeToGet.CHLMCMOSI != attribute.CHLMCMOSI) isChanged = true;
                if (attributeToGet.DataOrder != attribute.DataOrder) isChanged = true;
                if (attributeToGet.Description != attribute.Description) isChanged = true;
                if (attributeToGet.ID != attribute.ID) isChanged = true;
                if (attributeToGet.Key != attribute.Key) isChanged = true;
                if (attributeToGet.Labels != attribute.Labels) isChanged = true;
                if (attributeToGet.MeasureName != attribute.MeasureName) isChanged = true;
                if (attributeToGet.ModelOrder != attribute.ModelOrder) isChanged = true;
                if (attributeToGet.ShortDescription != attribute.ShortDescription) isChanged = true;
                if (attribute.UnitsDescription != attribute.UnitsDescription) isChanged = true;

                attributeToGet.MarkOld();
                result.ItemIsChanged = isChanged;
            }

            return result;
        }

        private DataAccessResult AttributeInsert(HaiAttribute attribute)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());

            try
            {
                LATID latid = new LATID();

                if (attribute.AverageOrTotal.Length == 0)
                    latid.AVGTOT = " "[0];
                else
                    latid.AVGTOT = attribute.AverageOrTotal[0];
                if (attribute.CHLMCDESC == null)
                    latid.CHLMCDESC = null;
                else
                {
                    if (attribute.CHLMCDESC.Length == 0)
                        latid.CHLMCDESC = " "[0];
                    else
                        latid.CHLMCDESC = attribute.CHLMCDESC[0];
                }
                if (attribute.CHLMCMOSI == null)
                    latid.CHLMCMOSI = null;
                else
                    if (attribute.CHLMCMOSI.Length == 0)
                        latid.CHLMCMOSI = " "[0];
                    else
                        latid.CHLMCMOSI = attribute.CHLMCMOSI[0];
                if (attribute.DataOrder.HasValue)
                    latid.DataOrder = (short)attribute.DataOrder;
                else
                    latid.DataOrder = null;
                latid.DESC = attribute.Description;
                latid.ID = (short)attribute.ID;
                latid.LABELS = attribute.Labels;
                latid.MeasureName = attribute.MeasureName;
                if (attribute.ModelOrder.HasValue)
                    latid.ModelOrder = (short)attribute.ModelOrder;
                else
                    latid.ModelOrder = null;
                latid.SDESC = attribute.ShortDescription;
                if (attribute.SubCnt.Length > 0) latid.SUBCNT = (char)attribute.SubCnt[0];
                latid.UDESC = attribute.UnitsDescription;

                emiOper.GetTable<LATID>().InsertOnSubmit(latid);
                emiOper.SubmitChanges();

                attribute.Key = latid.LATIDkey;
                attribute.MarkOld();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult AttributeUpdate(HaiAttribute attribute)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());

            try
            {
                LATID latid = emiOper.LATIDs.Single<LATID>(x => x.LATIDkey == attribute.Key);

                if (attribute.AverageOrTotal.Length == 0)
                    latid.AVGTOT = " "[0];
                else
                    latid.AVGTOT = attribute.AverageOrTotal[0];
                if (attribute.CHLMCDESC == null)
                    latid.CHLMCDESC = null;
                else
                {
                    if (attribute.CHLMCDESC.Length == 0)
                        latid.CHLMCDESC = " "[0];
                    else
                        latid.CHLMCDESC = attribute.CHLMCDESC[0];
                }
                if (attribute.CHLMCMOSI == null)
                    latid.CHLMCMOSI = null;
                else
                    if (attribute.CHLMCMOSI.Length == 0)
                        latid.CHLMCMOSI = " "[0];
                    else
                        latid.CHLMCMOSI = attribute.CHLMCMOSI[0];
                if (attribute.DataOrder.HasValue)
                    latid.DataOrder = (short)attribute.DataOrder;
                else
                    latid.DataOrder = null;
                latid.DESC = attribute.Description;
                latid.ID = (short)attribute.ID;
                latid.LABELS = attribute.Labels;
                latid.MeasureName = attribute.MeasureName;
                if (attribute.ModelOrder.HasValue)
                    latid.ModelOrder = (short)attribute.ModelOrder;
                else
                    latid.ModelOrder = null;
                latid.SDESC = attribute.ShortDescription;
                if (attribute.SubCnt.Length>0) latid.SUBCNT = (char)attribute.SubCnt[0];
                latid.UDESC = attribute.UnitsDescription;

                emiOper.SubmitChanges();

                attribute.MarkOld();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult AttributeDelete(HaiAttribute attribute)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());
            try
            {
                LATID attributeToDelete = emiOper.LATIDs.Single<LATID>(x => x.LATIDkey == attribute.Key);
                emiOper.LATIDs.DeleteOnSubmit(attributeToDelete);
                emiOper.SubmitChanges();

                attribute.MarkClean();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult AttributeSave(HaiAttribute attribute)
        {
            DataAccessResult result;
            if (attribute.IsSavable)
            {
                if (attribute.IsDeleted)
                    result = AttributeDelete(attribute);

                else if (attribute.IsNew)
                    result = AttributeInsert(attribute);

                else if (attribute.IsDirty)
                    result = AttributeUpdate(attribute);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }
        #endregion

        #region "CustomerType"

        private DataAccessResult CustomerTypeListGet(CustomerType customerTypeToGet)
        {
            DataAccessResult result = new DataAccessResult();
            string connectionString = GetEmioperConnectionString();
            HaiBindingList<CustomerType> customerTypes = new HaiBindingList<CustomerType>();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(connectionString);

            try
            {
                List<LCUID> LCUIDs;
                if (customerTypeToGet == null)
                    LCUIDs = emiOper.LCUIDs.ToList<LCUID>();
                else
                {
                    LCUIDs = new List<LCUID>();
                    LCUIDs.Add(emiOper.LCUIDs.Single<LCUID>(x => x.ID == customerTypeToGet.ID));
                }

                foreach (LCUID lcuid in LCUIDs)
                {
                    CustomerType customerType = new CustomerType();

                    customerType.Description = lcuid.DESC;
                    customerType.ID = lcuid.ID;
                    customerType.SortText = lcuid.SORT;
                    customerType.Key = lcuid.lcuidKey;

                    customerType.MarkOld();
                    customerTypes.AddToList(customerType);
                }

                result.DataList = customerTypes;
                result.Success = true;
                result.BrowsablePropertyList = CustomerType.GetBrowsablePropertyList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult CustomerTypeGet(CustomerType customerTypeToGet)
        {
            DataAccessResult result = CustomerTypeListGet(customerTypeToGet);
            if (result.Success)
            {
                CustomerType customerType = (CustomerType)result.SingleItem;
                bool isChanged = false;
                if (customerTypeToGet.Description != customerType.Description) isChanged = true;
                if (customerTypeToGet.ID != customerType.ID) isChanged = true;
                if (customerTypeToGet.Key != customerType.Key) isChanged = true;
                if (customerTypeToGet.SortText != customerType.SortText) isChanged = true;

                customerType.MarkOld();
                result.ItemIsChanged = isChanged;
            }

            return result;
        }

        private DataAccessResult CustomerTypeInsert(CustomerType customerType)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());

            try
            {
                LCUID lcuid = new LCUID();

                lcuid.ID = (short)customerType.ID;
                lcuid.DESC = customerType.Description;
                lcuid.SORT = customerType.SortText;

                emiOper.GetTable<LCUID>().InsertOnSubmit(lcuid);
                emiOper.SubmitChanges();

                customerType.Key = lcuid.lcuidKey;

                customerType.MarkOld();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult CustomerTypeUpdate(CustomerType customerType)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());

            try
            {
                LCUID lcuid = emiOper.LCUIDs.Single<LCUID>(x => x.lcuidKey == customerType.Key);

                lcuid.ID = (short)customerType.ID;
                lcuid.DESC = customerType.Description;
                lcuid.SORT = customerType.SortText;
                lcuid.lcuidKey = customerType.Key;

                emiOper.SubmitChanges();

                customerType.MarkOld();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult CustomerTypeDelete(CustomerType customerType)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());

            try
            {
                LCUID lcuidToDelete = emiOper.LCUIDs.Single<LCUID>(x => x.ID == customerType.ID);
                emiOper.LCUIDs.DeleteOnSubmit(lcuidToDelete);
                emiOper.SubmitChanges();

                customerType.MarkClean();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult CustomerTypeSave(CustomerType customerType)
        {
            DataAccessResult result;
            if (customerType.IsSavable)
            {
                if (customerType.IsDeleted)
                    result = CustomerTypeDelete(customerType);
                else if (customerType.IsNew)
                    result = CustomerTypeInsert(customerType);
                else if (customerType.IsDirty)
                    result = CustomerTypeUpdate(customerType);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        #endregion

        #region "Product"

        private DataAccessResult ProductListGet(Product productToGet)
        {
            DataAccessResult result = new DataAccessResult();
            string connectionString = GetEmioperConnectionString();
            HaiBindingList<Product> products = new HaiBindingList<Product>();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(connectionString);

            Dictionary<int, HaiProgram> programDict;
            DataAccessResult programResult = ProgramListGet(null);
            if (programResult.Success)
            {
                HaiBindingList<HaiProgram> programs = (HaiBindingList<HaiProgram>)programResult.DataList;
                programDict = programs.ToDictionary<HaiProgram, int>(x => x.ProgramKey);
            }
            else
            {
                result.Success = false;
                result.Message = "Unable to retrieve program list.  Message: " + programResult.Message;
                return result;
            }

            try
            {
                List<LPROD> LPRODs;
                if (productToGet == null)
                    LPRODs = emiOper.LPRODs.ToList<LPROD>();
                else
                {
                    LPRODs = new List<LPROD>();
                    LPRODs.Add(emiOper.LPRODs.Single<LPROD>(x => x.LPDSYSID == productToGet.ProductKey));
                }

                foreach (LPROD lprod in LPRODs)
                {
                    Product product = new Product();

                    if (lprod.BP.HasValue)
                    {
                        if (lprod.BP == 0)
                            product.BatchPrint = false;
                        else
                            product.BatchPrint = true;
                    }
                    else
                    {
                        product.BatchPrint = null;
                    }
                    product.PGPD = lprod.PGPD;
                    product.ColumnLabel = lprod.Collabel;
                    product.ID = lprod.ID;
                    product.LongDescription = lprod.LDESC;
                    product.ModelEditMOSIText = lprod.ModelEditMOSIText;
                    product.ModelEditSizeValueText = lprod.ModelEditSizeValueText;
                    product.OldID1 = lprod.OID1;
                    product.OldID2 = lprod.OID2;
                    product.ProductKey = lprod.LPDSYSID;
                    product.ProgramKey = lprod.LPGSysid;
                    product.ShortDescription = lprod.SDESC;
                    product.ProgramName = programDict[lprod.LPGSysid].ShortDescription;

                    product.MarkOld();
                    products.AddToList(product);
                }

                result.DataList = products;
                result.Success = true;
                result.BrowsablePropertyList = Product.GetBrowsablePropertyList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult ProductGet(Product productToGet)
        {
            DataAccessResult result = ProductListGet(productToGet);
            if (result.Success)
            {
                Product product = (Product)result.SingleItem;
                bool isChanged = false;
                if (productToGet.BatchPrint != product.BatchPrint) isChanged = true;
                if (productToGet.ColumnLabel != product.ColumnLabel) isChanged = true;
                if (productToGet.ID != product.ID) isChanged = true;
                if (productToGet.LongDescription != product.LongDescription) isChanged = true;
                if (productToGet.ModelEditMOSIText != product.ModelEditMOSIText) isChanged = true;
                if (productToGet.ModelEditSizeValueText != product.ModelEditSizeValueText) isChanged = true;
                if (productToGet.OldID1 != product.OldID1) isChanged = true;
                if (productToGet.OldID2 != product.OldID2) isChanged = true;
                if (productToGet.PGPD != product.PGPD) isChanged = true;
                if (productToGet.ProductKey != product.ProductKey) isChanged = true;
                if (productToGet.ProgramKey != product.ProgramKey) isChanged = true;
                if (productToGet.ShortDescription != product.ShortDescription) isChanged = true;

                productToGet.MarkOld();
                result.ItemIsChanged = isChanged;
            }

            return result;
        }

        private DataAccessResult ProductInsert(Product product)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());

            try
            {
                LPROD lprod = new LPROD();

                if (product.BatchPrint.HasValue)
                {
                    if (product.BatchPrint.Value)
                        lprod.BP = 1;
                    else
                        lprod.BP = 0;
                }
                else
                    lprod.BP = null;
                lprod.Collabel = product.ColumnLabel;
                lprod.ID = product.ID;
                lprod.LDESC = product.LongDescription;
                lprod.LPGSysid = product.ProgramKey;
                lprod.ModelEditMOSIText = product.ModelEditMOSIText;
                lprod.ModelEditSizeValueText = product.ModelEditSizeValueText;
                lprod.OID1 = product.OldID1;
                lprod.OID2 = product.OldID2;
                lprod.PGPD = product.PGPD;
                lprod.SDESC = product.ShortDescription;

                emiOper.LPRODs.InsertOnSubmit(lprod);
                emiOper.SubmitChanges();

                product.ProductKey = lprod.LPDSYSID;
                product.MarkOld();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult ProductUpdate(Product product)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());

            try
            {
                LPROD lprod = emiOper.LPRODs.Single<LPROD>(x => x.LPDSYSID == product.ProductKey);

                if (product.BatchPrint.HasValue)
                {
                    if (product.BatchPrint.Value)
                        lprod.BP = 1;
                    else
                        lprod.BP = 0;
                }
                else
                    lprod.BP = null;

                lprod.Collabel = product.ColumnLabel;
                lprod.ID = product.ID;
                lprod.LDESC = product.LongDescription;
                lprod.LPGSysid = product.ProgramKey;
                lprod.ModelEditMOSIText = product.ModelEditMOSIText;
                lprod.ModelEditSizeValueText = product.ModelEditSizeValueText;
                lprod.OID1 = product.OldID1;
                lprod.OID2 = product.OldID2;
                lprod.PGPD = product.PGPD;
                lprod.SDESC = product.ShortDescription;

                emiOper.SubmitChanges();

                product.MarkOld();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;

        }

        private DataAccessResult ProductDelete(Product product)
        {
            DataAccessResult result = new DataAccessResult();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(GetEmioperConnectionString());

            try
            {
                LPROD lprod = emiOper.LPRODs.Single<LPROD>(x => x.LPDSYSID == product.ProductKey);
                emiOper.LPRODs.DeleteOnSubmit(lprod);
                emiOper.SubmitChanges();

                product.MarkClean();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult ProductSave(Product product)
        {
            DataAccessResult result;
            if (product.IsSavable)
            {
                if (product.IsDeleted)
                    result = ProductDelete(product);
                else if (product.IsNew)
                    result = ProductInsert(product);
                else if (product.IsDirty)
                    result = ProductUpdate(product);
                else
                    result = new DataAccessResult();
            }
            else
            {
                result = new DataAccessResult();
                result.Success = false;
                result.Message = "Marked as not savable.";
            }

            return result;
        }

        #endregion

        #region "Program  READ only"

        private DataAccessResult ProgramListGet(HaiProgram programToGet)
        {
            DataAccessResult result = new DataAccessResult();
            string connectionString = GetEmioperConnectionString();
            HaiBindingList<HaiProgram> programs = new HaiBindingList<HaiProgram>();
            EmioperClassesDataContext emiOper = new EmioperClassesDataContext(connectionString);

            try
            {
                List<LPGID> LPGIDs;
                if (programToGet == null)
                    LPGIDs = emiOper.LPGIDs.ToList<LPGID>();
                else
                {
                    LPGIDs = new List<LPGID>();
                    LPGIDs.Add(emiOper.LPGIDs.Single<LPGID>(x => x.LPGSysid == programToGet.ProgramKey));
                }

                foreach (LPGID lpgid in LPGIDs)
                {
                    HaiProgram program = new HaiProgram();

                    program.ID = lpgid.ID;
                    program.ShortDescription = lpgid.SDESC;
                    program.LongDescription = lpgid.LDESC;
                    program.OldID1 = lpgid.OID1;
                    program.OldID2 = lpgid.OID2;
                    program.ProgramKey = lpgid.LPGSysid;

                    program.MarkOld();
                    programs.AddToList(program);
                }

                result.DataList = programs;
                result.Success = true;
                result.BrowsablePropertyList = HaiProgram.GetBrowsablePropertyList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            emiOper.Dispose();
            return result;
        }

        private DataAccessResult ProgramGet(HaiProgram programToGet)
        {
            DataAccessResult result = ProgramListGet(programToGet);
            if (result.Success)
            {
                HaiProgram program = (HaiProgram)result.SingleItem;
                bool isChanged = false;
                if (programToGet.ID != program.ID) isChanged = true;
                if (programToGet.LongDescription != program.LongDescription) isChanged = true;
                if (programToGet.OldID1 != program.OldID1) isChanged = true;
                if (programToGet.OldID2 != program.OldID2) isChanged = true;
                if (programToGet.ProgramKey != program.ProgramKey) isChanged = true;
                if (programToGet.ShortDescription != program.ShortDescription) isChanged = true;
                result.ItemIsChanged = isChanged;
            }

            return result;
        }

        #endregion

        #region "Utilities"
        public DataAccessResult GetStronglyTypedList<ObjectType>(string tableName, Dictionary<string, BrowsableProperty> bpList)
        {
            DataAccessResult result = new DataAccessResult();
            HaiBindingList<ObjectType> list = new HaiBindingList<ObjectType>();

            SqlCommand command;
            SqlConnection connection;
            SqlDataReader reader;
            string query;
            string connectionString = GetEmioperConnectionString();

            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    query = @"Select * from " + tableName;
                    connection.Open();
                    command = new SqlCommand(query, connection);
                    reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        int fieldCount = reader.FieldCount;
                        while (reader.Read())
                        {
                            ObjectType item = Activator.CreateInstance<ObjectType>();
                            foreach (string key in bpList.Keys)
                            {
                                BrowsableProperty bp = bpList[key];
                                object value = reader[bp.DbColumnName];
                                if (value.GetType() == typeof(String))
                                {
                                    value = ((string)value).Trim();
                                }
                                Utilities.CallByName(item, bp.PropertyName, CallType.Set, value);
                            }
                            list.AddToList(item);
                        }
                    }

                    reader.Close();
                    connection.Close();

                    result.DataList = list;
                    result.BrowsablePropertyList = bpList;
                    result.Success = true;
                    result.Message = string.Empty;
                }
            }
            catch (Exception ex)
            {
                result.DataList = null;
                result.BrowsablePropertyList = bpList;
                result.Success = false;
                result.Message = ex.Message;
            }


            return result;
        }

        public DataAccessResult GetTestingTypeList()
        {
            DataAccessResult result = new DataAccessResult();

            try
            {
                HaiBindingList<TestingType> testingTypeList = new HaiBindingList<TestingType>();

                TestingType tt;

                tt = new TestingType();
                tt.Key = 1;
                tt.Description = "A";
                tt.SortText = "X";
                tt.AnyDate = DateTime.Today;
                tt.TrueOrFalse = true;
                testingTypeList.AddToList(tt);

                tt = new TestingType();
                tt.Key = 2;
                tt.Description = "A";
                tt.SortText = "X";
                tt.AnyDate = new DateTime(1993, 4, 17);
                tt.TrueOrFalse = true;
                testingTypeList.AddToList(tt);

                tt = new TestingType();
                tt.Key = 3;
                tt.Description = "A";
                tt.SortText = "Y";
                tt.AnyDate = new DateTime(1947, 4, 8);
                tt.TrueOrFalse = false;
                testingTypeList.AddToList(tt);

                tt = new TestingType();
                tt.Key = 4;
                tt.Description = "B";
                tt.SortText = "Y";
                tt.AnyDate = new DateTime(1941, 12, 7);
                tt.TrueOrFalse = true;
                testingTypeList.AddToList(tt);

                tt = new TestingType();
                tt.Key = 5;
                tt.Description = "B";
                tt.SortText = "Z";
                tt.AnyDate = new DateTime(1776, 7, 4);
                tt.TrueOrFalse = true;
                testingTypeList.AddToList(tt);

                tt = new TestingType();
                tt.Key = 6;
                tt.Description = "B";
                tt.SortText = "Z";
                tt.AnyDate = new DateTime(1991, 3, 5);
                tt.TrueOrFalse = false;
                testingTypeList.AddToList(tt);


                result.Success = true;
                result.DataList =  testingTypeList;
                result.BrowsablePropertyList = TestingType.GetBrowsablePropertyList();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }
        #endregion
    }
}
