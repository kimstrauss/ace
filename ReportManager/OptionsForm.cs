﻿using System;
using System.Windows.Forms;

using HaiBusinessObject;

public enum Database
{
    Aham,
    Opei
}

namespace ReportManager
{
    public partial class OptionsForm : Form
    {
        private Database _database;

        public OptionsForm(Database database)
        {
            InitializeComponent();

            _database = database;
        }

        private void OptionsForm_Load(object sender, EventArgs e)
        {
            switch (_database)
            {
                case Database.Aham:
                    rbAhamOper.Checked = true;
                    break;
                case Database.Opei:
                    rbOpeiDev.Checked = true;
                    break;
                default:
                    break;
            }
        }

        public Database Database
        {
            get
            {
                return _database;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (rbAhamOper.Checked) _database = Database.Aham;
            if (rbOpeiDev.Checked) _database = Database.Opei;

            DialogResult = DialogResult.OK;

            this.Close();
        }
    }
}
