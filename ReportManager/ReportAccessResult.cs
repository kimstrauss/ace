﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HaiBusinessObject;

namespace ReportManager
{
    public class ReportAccessResult
    {
        public bool Success
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public Dictionary<string, ReportSpecification> ReportSpecificationList
        {
            get;
            set;
        }
    }
}
