﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;

using HaiBusinessObject;

namespace ReportManager
{
    public class ReportDataAccess
    {
        private string _userName;
        private string _connectionString;

        public ReportDataAccess()
        {
            _userName = Utilities.GetUserName();
            _connectionString = @"Data Source=" + Utilities.GetReportServerName() + ";Initial Catalog=" + Utilities.GetDatabaseName() + ";Integrated Security=True";
        }

        public ReportDataAccess(string connectionString)
        {
            _userName = Utilities.GetUserName();
            _connectionString = connectionString;
        }

        public ReportAccessResult GetReportList(HaiBusinessObjectType haiObjectType)
        {
            ReportAccessResult result = new ReportAccessResult();

            using (ReportManagerDataDataContext reportData = new ReportManagerDataDataContext(_connectionString))
            {
                try
                {
                    List<HaiBusinessObject> reports;
                    Dictionary<string, ReportSpecification> reportList = new Dictionary<string, ReportSpecification>(); // create dictionary for results
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<HaiBusinessObject>(x => x.HaiDataSources);
                    reportData.LoadOptions = dlo;

                    if (haiObjectType == HaiBusinessObjectType.AnyAhamObject)                        // retrieve all of the data
                        reports = reportData.HaiBusinessObjects.ToList<HaiBusinessObject>();
                    else                                                                            // retrieve only data for this type
                        reports = reportData.HaiBusinessObjects.Where<HaiBusinessObject>(x => x.HaiInternalClassId == (int)haiObjectType).ToList<HaiBusinessObject>();

                    // iterate the retrieved data
                    foreach (HaiBusinessObject anyReport in reports)
                    {
                        // if reports for a specific class are specified, then bypass those for other types
                        if (haiObjectType != HaiBusinessObjectType.AnyAhamObject)
                            if (anyReport.HaiInternalClassId != (int)haiObjectType) break;

                        // create the report specification and add it to the list
                        ReportSpecification specs = new ReportSpecification((HaiBusinessObjectType)anyReport.HaiInternalClassId, anyReport.BusinessObjectDescription, anyReport.BusinessObjectName, anyReport.BusinessObjectPath);
                        specs.SqlKey = anyReport.HaiBusinessObjectKey;

                        string key = "[" + anyReport.BusinessObjectName + "]![" + anyReport.HaiInternalClassId.ToString() + "]";
                        reportList.Add(key, specs);

                        // insert the data sources
                        foreach (HaiDataSource dataSource in anyReport.HaiDataSources)
                        {
                            specs.DataSourceNames.Add(dataSource.DataSourceName);
                        }
                    }

                    result.Success = true;
                    result.ReportSpecificationList = reportList;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }
                
            }

            return result;
        }

        public ReportAccessResult InsertReportSpecification(ReportSpecification ReportSpecs)
        {
            ReportAccessResult result = new ReportAccessResult();

            using (ReportManagerDataDataContext reportData = new ReportManagerDataDataContext(_connectionString))
            {
                try
                {
                    HaiBusinessObject report = new HaiBusinessObject();

                    // set the properties of the report object
                    report.HaiInternalClassId = (int)ReportSpecs.HaiObjectType;
                    report.BusinessObjectDescription = ReportSpecs.Description;
                    report.BusinessObjectName = ReportSpecs.Name;
                    report.BusinessObjectPath = ReportSpecs.ReportFileName;
                    report.EntryDtm = DateTime.Now;
                    report.EntryUid = _userName;

                    reportData.HaiBusinessObjects.InsertOnSubmit(report);
                    reportData.SubmitChanges();
                    int key = report.HaiBusinessObjectKey;

                    // create new data source records for names in ReportSpecs
                    List<HaiDataSource> dataSourcesToAdd = new List<HaiDataSource>();
                    foreach (string dataSourceName in ReportSpecs.DataSourceNames)
                    {
                        HaiDataSource dataSource = new HaiDataSource();
                        dataSource.DataSourceName = dataSourceName;
                        dataSource.HaiBusinessObjectKey = ReportSpecs.SqlKey;
                        dataSource.EntryDtm = DateTime.Now;
                        dataSource.EntryUid = _userName;
                        dataSource.HaiBusinessObjectKey = key;
                        dataSourcesToAdd.Add(dataSource);
                    }

                    // insert the new data source records
                    foreach (HaiDataSource dataSource in dataSourcesToAdd)
                    {
                        reportData.HaiDataSources.InsertOnSubmit(dataSource);
                    }

                    reportData.SubmitChanges();

                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }

            }
            return result;
        }

        public ReportAccessResult UpdateReportSpecification(ReportSpecification ReportSpecs)
        {
            ReportAccessResult result = new ReportAccessResult();

            int key = ReportSpecs.SqlKey;

            using (ReportManagerDataDataContext reportData = new ReportManagerDataDataContext(_connectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<HaiBusinessObject>(x => x.HaiDataSources);
                    reportData.LoadOptions = dlo;

                    // get the report object, including the collection of data sources objects
                    HaiBusinessObject report = reportData.HaiBusinessObjects.Single<HaiBusinessObject>(x => (x.HaiBusinessObjectKey == key));

                    // update the properties of the report object
                    report.HaiInternalClassId = (int)ReportSpecs.HaiObjectType;
                    report.BusinessObjectDescription = ReportSpecs.Description;
                    report.BusinessObjectName = ReportSpecs.Name;
                    report.BusinessObjectPath = ReportSpecs.ReportFileName;
                    report.EntryDtm = DateTime.Now;
                    report.EntryUid = _userName;

                    // find the data source records that need to be deleted because they are no longer in ReportSpecs
                    List<HaiDataSource> dataSourcesToDelete = new List<HaiDataSource>();
                    List<string> dataSourceNames = new List<string>();
                    foreach (HaiDataSource dataSource in report.HaiDataSources)
                    {
                        if (!ReportSpecs.DataSourceNames.Contains(dataSource.DataSourceName))
                        {
                            dataSource.EntryUid = _userName;
                            dataSource.EntryDtm = DateTime.Now;
                            dataSourcesToDelete.Add(dataSource);
                        }

                        dataSourceNames.Add(dataSource.DataSourceName);     // record the names of the data sources that will remain
                    }

                    // create new data source records for names in ReportSpecs that are not in the list of remaining names in SQL
                    List<HaiDataSource> dataSourcesToAdd = new List<HaiDataSource>();
                    foreach (string dataSourceName in ReportSpecs.DataSourceNames)
                    {
                        if (!dataSourceNames.Contains(dataSourceName))
                        {
                            HaiDataSource dataSource = new HaiDataSource();
                            dataSource.DataSourceName = dataSourceName;
                            dataSource.HaiBusinessObjectKey = ReportSpecs.SqlKey;
                            dataSource.EntryDtm = DateTime.Now;
                            dataSource.EntryUid = _userName;
                            dataSourcesToAdd.Add(dataSource);
                        }
                    }

                    // delete records for removed data sources
                    foreach (HaiDataSource dataSource in dataSourcesToDelete)
                    {
                        reportData.HaiDataSources.DeleteOnSubmit(dataSource);
                    }

                    // insert the new data source records
                    foreach (HaiDataSource dataSource in dataSourcesToAdd)
                    {
                        reportData.HaiDataSources.InsertOnSubmit(dataSource);
                    }

                    reportData.SubmitChanges();

                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }

            }
            return result;
        }

        public ReportAccessResult DeleteReportSpecification(ReportSpecification ReportSpecs)
        {
            ReportAccessResult result = new ReportAccessResult();

            int key = ReportSpecs.SqlKey;

            using (ReportManagerDataDataContext reportData = new ReportManagerDataDataContext(_connectionString))
            {
                try
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                    dlo.LoadWith<HaiBusinessObject>(x => x.HaiDataSources);
                    reportData.LoadOptions = dlo;

                    // get the report object, including the collection of data sources objects
                    HaiBusinessObject report = reportData.HaiBusinessObjects.Single<HaiBusinessObject>(x => (x.HaiBusinessObjectKey == key));

                    // delete all of the data source records for the report
                    foreach (HaiDataSource dataSource in report.HaiDataSources)
                    {
                        dataSource.EntryUid = _userName;
                        dataSource.EntryDtm = DateTime.Now;
                        reportData.HaiDataSources.DeleteOnSubmit(dataSource);
                    }

                    // delete the report
                    report.EntryDtm = DateTime.Now;
                    report.EntryUid = _userName;
                    reportData.HaiBusinessObjects.DeleteOnSubmit(report);

                    reportData.SubmitChanges();

                    result.Success = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = ex.Message;
                }

            }
            return result;
        }
    }
}
