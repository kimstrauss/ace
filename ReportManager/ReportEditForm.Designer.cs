﻿namespace ReportManager
{
    partial class ReportEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtReportName = new System.Windows.Forms.TextBox();
            this.txtReportDescription = new System.Windows.Forms.TextBox();
            this.txtReportFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.lbDataSourceNames = new System.Windows.Forms.ListBox();
            this.txtDataSourceName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAddDataSource = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.cbxReportTypes = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(565, 345);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(484, 345);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtReportName
            // 
            this.txtReportName.Location = new System.Drawing.Point(96, 50);
            this.txtReportName.MaxLength = 128;
            this.txtReportName.Name = "txtReportName";
            this.txtReportName.Size = new System.Drawing.Size(531, 20);
            this.txtReportName.TabIndex = 0;
            // 
            // txtReportDescription
            // 
            this.txtReportDescription.Location = new System.Drawing.Point(96, 76);
            this.txtReportDescription.MaxLength = 1024;
            this.txtReportDescription.Name = "txtReportDescription";
            this.txtReportDescription.Size = new System.Drawing.Size(531, 20);
            this.txtReportDescription.TabIndex = 1;
            // 
            // txtReportFileName
            // 
            this.txtReportFileName.Location = new System.Drawing.Point(96, 102);
            this.txtReportFileName.MaxLength = 1024;
            this.txtReportFileName.Name = "txtReportFileName";
            this.txtReportFileName.Size = new System.Drawing.Size(531, 20);
            this.txtReportFileName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Description";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "File name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(534, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Key";
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(565, 12);
            this.txtKey.MaxLength = 1024;
            this.txtKey.Name = "txtKey";
            this.txtKey.ReadOnly = true;
            this.txtKey.Size = new System.Drawing.Size(62, 20);
            this.txtKey.TabIndex = 9;
            // 
            // lbDataSourceNames
            // 
            this.lbDataSourceNames.FormattingEnabled = true;
            this.lbDataSourceNames.Location = new System.Drawing.Point(96, 211);
            this.lbDataSourceNames.Name = "lbDataSourceNames";
            this.lbDataSourceNames.Size = new System.Drawing.Size(443, 108);
            this.lbDataSourceNames.TabIndex = 5;
            // 
            // txtDataSourceName
            // 
            this.txtDataSourceName.AcceptsReturn = true;
            this.txtDataSourceName.Location = new System.Drawing.Point(190, 185);
            this.txtDataSourceName.MaxLength = 1024;
            this.txtDataSourceName.Name = "txtDataSourceName";
            this.txtDataSourceName.Size = new System.Drawing.Size(349, 20);
            this.txtDataSourceName.TabIndex = 3;
            this.txtDataSourceName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDataSourceName_KeyDown);
            this.txtDataSourceName.Leave += new System.EventHandler(this.txtDataSourceName_Leave);
            this.txtDataSourceName.Enter += new System.EventHandler(this.txtDataSourceName_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(93, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Data souce name";
            // 
            // btnAddDataSource
            // 
            this.btnAddDataSource.Location = new System.Drawing.Point(552, 183);
            this.btnAddDataSource.Name = "btnAddDataSource";
            this.btnAddDataSource.Size = new System.Drawing.Size(75, 23);
            this.btnAddDataSource.TabIndex = 4;
            this.btnAddDataSource.Text = "Add";
            this.btnAddDataSource.UseVisualStyleBackColor = true;
            this.btnAddDataSource.Click += new System.EventHandler(this.btnAddDataSource_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(552, 212);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 6;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // cbxReportTypes
            // 
            this.cbxReportTypes.FormattingEnabled = true;
            this.cbxReportTypes.Location = new System.Drawing.Point(98, 12);
            this.cbxReportTypes.Name = "cbxReportTypes";
            this.cbxReportTypes.Size = new System.Drawing.Size(410, 21);
            this.cbxReportTypes.TabIndex = 9;
            this.cbxReportTypes.SelectedIndexChanged += new System.EventHandler(this.cbxReportTypes_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Report type";
            // 
            // ReportEditForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(652, 381);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbxReportTypes);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAddDataSource);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDataSourceName);
            this.Controls.Add(this.lbDataSourceNames);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtReportFileName);
            this.Controls.Add(this.txtReportDescription);
            this.Controls.Add(this.txtReportName);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReportEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit report";
            this.Load += new System.EventHandler(this.ReportEditForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox txtReportName;
        private System.Windows.Forms.TextBox txtReportDescription;
        private System.Windows.Forms.TextBox txtReportFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.ListBox lbDataSourceNames;
        private System.Windows.Forms.TextBox txtDataSourceName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAddDataSource;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.ComboBox cbxReportTypes;
        private System.Windows.Forms.Label label6;
    }
}