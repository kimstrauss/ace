﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using HaiBusinessObject;

namespace ReportManager
{
    public partial class ReportEditForm : Form
    {
        private ReportSpecification _reportSpecs;
        private HaiBusinessObjectType _selectedType;
        private Array _types;

        public ReportEditForm(ReportSpecification reportSpecs)
        {
            InitializeComponent();

            _reportSpecs = reportSpecs;
        }

        private void ReportEditForm_Load(object sender, EventArgs e)
        {
            btnRemove.Enabled = false;

            string[] typeNames = Enum.GetNames(typeof(HaiBusinessObjectType));
            _types = Enum.GetValues(typeof(HaiBusinessObjectType));
            cbxReportTypes.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxReportTypes.Items.AddRange(typeNames);

            foreach (string typeName in cbxReportTypes.Items)
            {
                if (_reportSpecs.HaiObjectType.ToString() == typeName)
                {
                    cbxReportTypes.Text = typeName;
                    break;
                }
            }

            txtReportName.Text = _reportSpecs.Name;
            txtReportDescription.Text = _reportSpecs.Description;
            txtReportFileName.Text = _reportSpecs.ReportFileName;
            if (_reportSpecs.SqlKey != 0)
                txtKey.Text = _reportSpecs.SqlKey.ToString();

            foreach (string dataSourceName in _reportSpecs.DataSourceNames)
            {
                lbDataSourceNames.Items.Add(dataSourceName);
                btnRemove.Enabled = true;
            }
        }

        private void cbxReportTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            _selectedType = (HaiBusinessObjectType)(_types.GetValue(cbxReportTypes.SelectedIndex));
        }

        private void btnAddDataSource_Click(object sender, EventArgs e)
        {
            string dataSourceName = txtDataSourceName.Text.Trim();
            if (dataSourceName == string.Empty)
                return;

            if (lbDataSourceNames.Items.Contains(dataSourceName))
            {
                MessageBox.Show(dataSourceName + " is already in list.", "Invalid entry", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            lbDataSourceNames.Items.Add(dataSourceName);
            lbDataSourceNames.SelectedIndex = lbDataSourceNames.Items.Count - 1;
            txtDataSourceName.Text = "";
            btnRemove.Enabled = true;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            int selectedIndex = lbDataSourceNames.SelectedIndex;
            if (selectedIndex == -1)
                return;

            lbDataSourceNames.Items.RemoveAt(selectedIndex);

            if (lbDataSourceNames.Items.Count == 0)
            {
                btnRemove.Enabled = false;
                return;
            }

            if (selectedIndex > 0)
                    selectedIndex -= 1;

            lbDataSourceNames.SelectedIndex = selectedIndex;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (_selectedType == HaiBusinessObjectType.UnknownHaiBusinessObjectType)
            {
                MessageBox.Show("Type cannot be unknown.", "Invalid selection", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (txtReportName.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A report name is required.", "Invalid entry", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (txtReportDescription.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A description is required.", "Invalid entry", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (txtReportFileName.Text.Trim() == string.Empty)
            {
                MessageBox.Show("A file name is required.", "Invalid entry", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (lbDataSourceNames.Items.Count == 0)
            {
                MessageBox.Show("At least one data source name is required.", "Invalid entry", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            _reportSpecs.HaiObjectType = _selectedType;
            _reportSpecs.DataSourceNames.Add(txtDataSourceName.Text.Trim());
            _reportSpecs.Description = txtReportDescription.Text.Trim();
            _reportSpecs.ReportFileName = txtReportFileName.Text.Trim();
            _reportSpecs.Name = txtReportName.Text.Trim();
            _reportSpecs.DataSourceNames.Clear();
            foreach (string dataSourceName in lbDataSourceNames.Items)
            {
                _reportSpecs.DataSourceNames.Add(dataSourceName);
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void txtDataSourceName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Return)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                btnAddDataSource_Click(null, null);
            }
        }

        private void txtDataSourceName_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = null;
        }

        private void txtDataSourceName_Leave(object sender, EventArgs e)
        {
            this.AcceptButton = btnOK;
        }

    }
}
