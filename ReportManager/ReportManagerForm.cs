﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Configuration;

using HaiBusinessObject;

namespace ReportManager
{
    public partial class ReportManagerForm : Form
    {
        private HaiBusinessObjectType _filterType = HaiBusinessObjectType.AnyAhamObject;
        private Dictionary<string, ReportSpecification> _reportList;
        private Database _database;

        public ReportManagerForm()
        {
            InitializeComponent();
            _database = Database.Aham;
        }

        private void ReportManagerForm_Load(object sender, EventArgs e)
        {
            lvReportList.Columns.Add("Name", 150);
            lvReportList.Columns.Add("Description", 300);
            lvReportList.Columns.Add("File name", 100);
            lvReportList.Columns.Add("Class ID", 100);
            lvReportList.Columns.Add("First data source name", 150);
            lvReportList.Columns.Add("Source count", 100, HorizontalAlignment.Right);
            lvReportList.Columns.Add("Key", 50, HorizontalAlignment.Right);

            lvReportList.View = View.Details;
            lvReportList.FullRowSelect = true;
            lvReportList.MultiSelect = false;

            cbxReportTypes.DropDownStyle = ComboBoxStyle.DropDownList;

            // fill the ComboBox with the object enumeration names
            int ahamIndex = 0;
            Array types = Enum.GetValues(typeof(HaiBusinessObjectType));
            for (int i = 0; i < types.Length; i++)
            {
                HaiBusinessObjectType objType = (HaiBusinessObjectType)types.GetValue(i);
                cbxReportTypes.Items.Add(objType);
                if (objType == _filterType)
                    ahamIndex = i;
            }

            cbxReportTypes.SelectedIndex = ahamIndex;   // setting the SelectedIndex forces a call to FillControls();

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddReport_Click(object sender, EventArgs e)
        {
            // create the new object that will be added
            ReportSpecification reportSpecs = new ReportSpecification(HaiBusinessObjectType.UnknownHaiBusinessObjectType, "", "", "");

            // instantiate the form with the new object and open the form
            ReportEditForm editForm = new ReportEditForm(reportSpecs);
            editForm.ShowDialog();

            if (editForm.DialogResult == DialogResult.OK)
            {
                string connectionString = GetConnectionString(_database);
                ReportDataAccess rda = new ReportDataAccess(connectionString);

                ReportAccessResult result = rda.InsertReportSpecification(reportSpecs);    // add the new object to the DB
                if (!result.Success)
                    MessageBox.Show("Adding report failed\n\n\r" + result.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            editForm.Dispose();

            FillControls();     // redisplay the form with the updated list
        }

        private void btnEditReport_Click(object sender, EventArgs e)
        {
            if (lvReportList.SelectedItems.Count == 0)
            {
                MessageBox.Show("Nothing selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // get the item to be editted
            ListViewItem item = lvReportList.SelectedItems[0];
            ReportSpecification reportSpecs = (ReportSpecification)item.Tag;

            // instantiate the form with the edit object and open the form
            ReportEditForm editForm = new ReportEditForm(reportSpecs);
            editForm.ShowDialog();

            if (editForm.DialogResult == DialogResult.OK)
            {
                string connectionString = GetConnectionString(_database);
                ReportDataAccess rda = new ReportDataAccess(connectionString);

                ReportAccessResult result = rda.UpdateReportSpecification(reportSpecs);    // update the DB
                if (!result.Success)
                    MessageBox.Show("Updating report failed\n\n\r" + result.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            editForm.Dispose();

            FillControls();     // redisplay the form with the updated list
        }

        private void btnDeleteReport_Click(object sender, EventArgs e)
        {
            if (lvReportList.SelectedItems.Count == 0)
            {
                MessageBox.Show("Nothing selected.", "Selection error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // get the item to be deleted
            ListViewItem item = lvReportList.SelectedItems[0];
            ReportSpecification reportSpecs = (ReportSpecification)item.Tag;

            string connectionString = GetConnectionString(_database);
            ReportDataAccess rda = new ReportDataAccess(connectionString);

            ReportAccessResult result = rda.DeleteReportSpecification(reportSpecs);
            if (!result.Success)
                    MessageBox.Show("Deleting report failed\n\n\r" + result.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            FillControls();     // redisplay the form with the updated list
        }

        private void FillControls()
        {
            Cursor = Cursors.WaitCursor;

            lvReportList.Items.Clear();

            // get the list of report objects for this HaiBusinessObjectType
            string connectionString = GetConnectionString(_database);
            ReportDataAccess rda = new ReportDataAccess(connectionString);

            ReportAccessResult result = rda.GetReportList(_filterType);
            if (result.Success)
            {
                _reportList = result.ReportSpecificationList;
            }
            else
            {
                string message = "Retrieval of report list failed.\n\n\r" + result.Message;
                MessageBox.Show(message, "Data retrieval error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // populate the listview with the report list
            lvReportList.BeginUpdate();
            foreach (string key in _reportList.Keys)
            {
                ReportSpecification specs = _reportList[key];

                ListViewItem item = new ListViewItem(specs.Name);
                item.Tag = specs;
                item.SubItems.Add(specs.Description);
                item.SubItems.Add(specs.ReportFileName);
                item.SubItems.Add(specs.HaiObjectType.ToString());
                if (specs.DataSourceNames.Count > 0)
                    item.SubItems.Add(specs.DataSourceNames[0]);
                else
                    item.SubItems.Add("*** no data sources ***");

                item.SubItems.Add(specs.DataSourceNames.Count.ToString());
                item.SubItems.Add(specs.SqlKey.ToString());

                lvReportList.Items.Add(item);
            }
            lvReportList.EndUpdate();

            this.Text = "Report Manager: " + _database.ToString();

            Cursor = Cursors.Default;
        }

        private void lvReportList_DoubleClick(object sender, EventArgs e)
        {
            btnEditReport_Click(null, null);
        }

        private void cbxReportTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            _filterType = (HaiBusinessObjectType)cbxReportTypes.Items[cbxReportTypes.SelectedIndex];
            FillControls();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OptionsForm theForm = new OptionsForm(_database);
            theForm.ShowDialog();

            if (theForm.DialogResult == DialogResult.OK)
            {
                _database = theForm.Database;
                cbxReportTypes.SelectedIndex = 0;           // setting the SelectedIndex forces a call to FillControls();
            }

            theForm.Dispose();
        }

        private string GetConnectionString(Database database)
        {
            string databaseName=string.Empty;

            switch (database)
            {
                case Database.Aham:
                    databaseName = ConfigurationManager.AppSettings["AhamDatabaseName"].Trim();
                    break;
                case Database.Opei:
                    databaseName = ConfigurationManager.AppSettings["OpeiDatabaseName"].Trim();
                    break;
                default:
                    break;
            }
            
            return @"Data Source=" + Utilities.GetReportServerName() + ";Initial Catalog=" + databaseName + ";Integrated Security=True";
        }
    }
}
